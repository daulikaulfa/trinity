package jp.co.blueship.tri.fw.um.domainx.wiki;

import java.io.File;
import java.io.IOException;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.status.UmActStatusId;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.um.UmDesignBusinessRuleUtils;
import jp.co.blueship.tri.fw.um.dao.hist.eb.HistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.IWikiAttachedFileEntity;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.IWikiEntity;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.IWikiHistEntity;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.IWikiTagEntity;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.WikiAttachedFileCondition;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.WikiAttachedFileEntity;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.WikiCondition;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.WikiEntity;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.WikiHistEntity;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.WikiTagCondition;
import jp.co.blueship.tri.fw.um.domainx.wiki.dto.FlowWikiRemovalServiceBean;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 */
public class FlowWikiRemovalService implements IDomain<FlowWikiRemovalServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IUmFinderSupport umSupport = null;

	public void setUmSupport(IUmFinderSupport umSupport) {
		this.umSupport = umSupport;
	}

	@Override
	public IServiceDto<FlowWikiRemovalServiceBean> execute(IServiceDto<FlowWikiRemovalServiceBean> serviceDto) {

		FlowWikiRemovalServiceBean paramBean = serviceDto.getServiceBean();

		try{
			String wikiId = paramBean.getParam().getSelectedWikiId();

			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");
			PreConditions.assertOf(TriStringUtils.isNotEmpty(wikiId), "SelectedWikiId is not specified");

			if(RequestType.submitChanges.equals(paramBean.getParam().getRequestType())){
				paramBean.getResult().setCompleted(false);

				String pageNm = this.getWikiPageNm(wikiId);

				this.removalWiki(wikiId);
				this.removalWikiTag(wikiId);
				this.removalWikiHist(wikiId);
				this.removalWikiAttachedFile(wikiId);
				this.removalWikiUpdateHist(wikiId);

				paramBean.getMessageInfo().addFlashTranslatable(UmMessageId.UM003068I,pageNm);
				paramBean.getResult().setCompleted(true);
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(UmMessageId.UM005057S, e, paramBean.getFlowAction());
		}
	}

	private String getWikiPageNm( String wikiId ){
		WikiCondition  condition = new WikiCondition();
		condition.setWikiId(wikiId);

		IWikiEntity entity = this.umSupport.getWikiDao().findByPrimaryKey(condition.getCondition());

		return entity.getPageNm();
	}


	private void removalWiki( String wikiId ){
		IWikiEntity entity = new WikiEntity();
		entity.setWikiId( wikiId );
		entity.setDelStsId( StatusFlg.on );

		this.umSupport.getWikiDao().update(entity);
	}


	private void removalWikiTag( String wikiId ){
		WikiTagCondition tagCondition = new WikiTagCondition();
		tagCondition.setWikiId( wikiId);
		List<IWikiTagEntity> tagEntityList = this.umSupport.getWikiTagDao().find(tagCondition.getCondition());

		for(IWikiTagEntity tagEntity : tagEntityList){
			tagEntity.setDelStsId( StatusFlg.on );
			this.umSupport.getWikiTagDao().update(tagEntity);
		}
	}


	private void removalWikiHist( String wikiId ){
		IWikiHistEntity histEntity = new WikiHistEntity();
		histEntity.setWikiId(wikiId);
		histEntity.setDelStsId( StatusFlg.on );
		this.umSupport.getWikiHistDao().update(histEntity);
	}

	private void removalWikiUpdateHist( String wikiId ){
		IHistEntity histEntity = new HistEntity();
		histEntity.setActSts(UmActStatusId.Remove.getStatusId());

		WikiCondition  condition = new WikiCondition();
		condition.setWikiId(wikiId);
		condition.setDelStsId( StatusFlg.on );

		IWikiEntity entity = this.umSupport.getWikiDao().findByPrimaryKey(condition.getCondition());

		this.umSupport.getWikiUpdateHistDao().insert( histEntity , entity);
	}


	private void removalWikiAttachedFile(String wikiId){
		// 指定Wikiの添付ファイルを全削除
		WikiAttachedFileCondition condition = new WikiAttachedFileCondition();
		condition.setWikiId(wikiId);
		condition.setDelStsId(StatusFlg.off);
		List<IWikiAttachedFileEntity> attachedEntities = this.umSupport.getWikiAttachedFileDao().find(condition.getCondition());
		for( IWikiAttachedFileEntity attachedEntity : attachedEntities ) {
			File attachmentFilePath = UmDesignBusinessRuleUtils.getAttachmentFile(wikiId, attachedEntity.getAttachedFileSeqNo(), attachedEntity.getFilePath());
			try {
				TriFileUtils.delete(attachmentFilePath);
				File path = new File( attachmentFilePath.getParent() );
				if( path.isDirectory() ) {
					TriFileUtils.delete(path);
				}
			} catch (IOException e) {
				LogHandler.fatal(log, e);
				ExceptionUtils.reThrowIfTrinityException(e);
				throw new TriSystemException(SmMessageId.SM004181F, e, attachmentFilePath.toString());
			}
		}

		// DBから削除
		IWikiAttachedFileEntity entity = new WikiAttachedFileEntity();
		entity.setWikiId(wikiId);
		entity.setDelStsId( StatusFlg.on );
		this.umSupport.getWikiAttachedFileDao().update(entity);
	}
}
