package jp.co.blueship.tri.fw.um.domainx.project.beans;

import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

import jp.co.blueship.tri.am.dao.areq.eb.AreqCondition;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotCondition;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.PjtCondition;
import jp.co.blueship.tri.am.support.IAmFinderSupport;
import jp.co.blueship.tri.bm.dao.bp.eb.BpCondition;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.bm.dao.constants.BmTables;
import jp.co.blueship.tri.bm.support.IBmFinderSupport;
import jp.co.blueship.tri.dcm.constants.DcmReportType;
import jp.co.blueship.tri.dcm.dao.constants.DcmTables;
import jp.co.blueship.tri.dcm.dao.rep.eb.IRepEntity;
import jp.co.blueship.tri.dcm.dao.rep.eb.RepCondition;
import jp.co.blueship.tri.dm.dao.constants.DmTables;
import jp.co.blueship.tri.dm.dao.dmdo.eb.DmDoCondition;
import jp.co.blueship.tri.dm.dao.dmdo.eb.IDmDoEntity;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.constants.status.UmActStatusId;
import jp.co.blueship.tri.fw.dao.orm.FinderSupport;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.um.UmRecentUpdatesUtils;
import jp.co.blueship.tri.fw.um.constants.DataAttribute;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.CtgCondition;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.ICtgEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.IMstoneEntity;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.MstoneCondition;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.IWikiEntity;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.WikiCondition;
import jp.co.blueship.tri.fw.um.domainx.project.beans.dto.RecentUpdateViewBean;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;
import jp.co.blueship.tri.rm.dao.constants.RmTables;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.RaCondition;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.RpCondition;
import jp.co.blueship.tri.rm.support.IRmFinderSupport;


/**
 * Create the information of "Recent updata".<br>
 * <br>
 * 「最近の更新」に表示する情報を用意するクラスです。<br>
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 */
public class RecentUpdateService {

	private IUmFinderSupport umSupport;
	private IAmFinderSupport amSupport;
	private IBmFinderSupport bmSupport;
	private IRmFinderSupport rmSupport;


	public void setAmSupport(IAmFinderSupport amSupport) {
		this.amSupport = amSupport;
		this.umSupport = amSupport.getUmFinderSupport();
	}
	public void setBmSupport(IBmFinderSupport bmSupport) {
		this.bmSupport = bmSupport;
	}
	public void setRmSupport(IRmFinderSupport rmSupport) {
		this.rmSupport = rmSupport;
	}


	/**
	 *
	 * Creatd RecentUpdateViewList using argument.<br>
	 * <br>
	 * 引数で指定したIHistEntityからRecentUpdateViewを作成し、<br>
	 * それをリストにして返します。<br>
	 *
	 * @param histEntityList
	 * @param langage
	 * @param timeZone
	 * @return RecentUpdateViewList
	 */
	public List<RecentUpdateViewBean> getRecentUpdateViewList( List<IHistEntity> histEntityList, String langage, TimeZone timeZone ){

		if(histEntityList == null)
			return null;

		List<RecentUpdateViewBean> recentUpdateViewList = new ArrayList<RecentUpdateViewBean>();

		for( IHistEntity histEntity : histEntityList )
			recentUpdateViewList.add(this.getRecentUpdateView( histEntity, langage, timeZone ));

		return recentUpdateViewList;
	}


	/**
	 * Creatd RecentUpdateView using argument.<br>
	 * <br>
	 * 引数で指定したHistEntityからRecentUpdateViewを作成します。<br>
	 *
	 * @param histEntity
	 * @param langage
	 * @param timeZone
	 * @return RecentUpdateView
	 */
	public RecentUpdateViewBean getRecentUpdateView( IHistEntity histEntity, String langage, TimeZone timeZone){

		RecentUpdateViewBean recentUpdateView = new RecentUpdateViewBean();

		String lotId = histEntity.getLotId();

		LotCondition lotCondition = new LotCondition();
		lotCondition.setLotId(lotId);
		lotCondition.setDelStsId((StatusFlg)null);

		ILotEntity lotEntity = this.amSupport.getLotDao().findByPrimaryKey(lotCondition.getCondition());
		if ( null == lotEntity ) {
			throw new TriSystemException(AmMessageId.AM005146S, lotId);
		}

		String dataCtgCd = histEntity.getDataCtgCd();
		boolean isUnread = ( histEntity.getNoticeViewTimestamp() == null )? true : false;

		recentUpdateView.setHistId				( histEntity.getHistId() )
						.setSubmitterId			( histEntity.getRegUserId() )
						.setSubmitterNm			( histEntity.getRegUserNm() )
						.setSubmitterIconPath	( this.umSupport.getIconPath(histEntity.getRegUserId()) )
						.setDataAttribute		( DataAttribute.none )
						.setActStsId			( histEntity.getActSts() )
						.setLotId				( lotId )
						.setDataSubject			( "" )
						.setLotNm				( lotEntity.getLotNm() )
						.setDataId				( histEntity.getDataId() )
						.setElapsedTime			( UmRecentUpdatesUtils.stringOf(langage , histEntity.getRegTimestamp()) )
						.setUpdTime				( TriDateUtils.convertViewDateFormat(histEntity.getRegTimestamp(),
														TriDateUtils.getHMDateFormat( timeZone )))
						.setStsId				( lotEntity.getProcStsId() )
						.setIsUnread			( isUnread )
						;

		if(AmTables.AM_LOT.name().equals(dataCtgCd))
			this.getRecentUpdateViewOfLot( recentUpdateView, histEntity, lotEntity);

		else if(AmTables.AM_PJT.name().equals(dataCtgCd))
			this.getRecentUpdateViewOfPjt( recentUpdateView, histEntity);

		else if(AmTables.AM_AREQ.name().equals(dataCtgCd))
			this.getRecentUpdateViewOfAreq( recentUpdateView, histEntity);

		else if(BmTables.BM_BP.name().equals(dataCtgCd))
			this.getRecentUpdateViewOfBp( recentUpdateView, histEntity);

		else if(RmTables.RM_RA.name().equals(dataCtgCd))
			this.getRecentUpdateViewOfRa( recentUpdateView, histEntity);

		else if(RmTables.RM_RP.name().equals(dataCtgCd))
			this.getRecentUpdateViewOfRp( recentUpdateView, histEntity);

		else if(DmTables.DM_DO.name().equals(dataCtgCd))
			this.getRecentUpdateViewOfDmDo( recentUpdateView, histEntity);

		else if(DcmTables.DCM_REP.name().equals(dataCtgCd))
			this.getRecentUpdateViewOfReport( recentUpdateView, histEntity);

		else if(UmTables.UM_CTG.name().equals(dataCtgCd))
			this.getRecentUpdateViewOfCtg( recentUpdateView, histEntity);

		else if(UmTables.UM_MSTONE.name().equals(dataCtgCd))
			this.getRecentUpdateViewOfMstone( recentUpdateView, histEntity);

		else if(UmTables.UM_WIKI.name().equals(dataCtgCd))
			this.getRecentUpdateViewOfWiki( recentUpdateView, histEntity);

		return recentUpdateView;
	}


	private void getRecentUpdateViewOfLot(RecentUpdateViewBean view, IHistEntity entity, ILotEntity lotEntity){

		String comment = null;
		String actStsId = view.getActStsId();

		if(UmActStatusId.Remove.equals(actStsId))
			comment = lotEntity.getDelCmt();

		else if(UmActStatusId.Close.equals(actStsId) || UmActStatusId.MultipleClose.equals(actStsId))
			comment = lotEntity.getCloseCmt();

		view.setDataSubject		( lotEntity.getSummary() )
			.setComment			( comment )
			.setDataAttribute	( DataAttribute.Lot );
	}


	private void getRecentUpdateViewOfPjt(RecentUpdateViewBean view , IHistEntity entity){
		PjtCondition condition = new PjtCondition();
		condition.setDelStsId( (StatusFlg)null );
		condition.setPjtId( view.getDataId() );

		IPjtEntity pjtEntity = this.amSupport.getPjtDao().findByPrimaryKey(condition.getCondition());
		if(pjtEntity == null) return;

		String comment = null;
		String actStsId = view.getActStsId();

		if(UmActStatusId.Remove.equals(actStsId))
			comment = pjtEntity.getDelCmt();

		else if(UmActStatusId.Close.equals(actStsId) || UmActStatusId.MultipleClose.equals(actStsId))
			comment = pjtEntity.getCloseCmt();

		view.setDataSubject	 ( pjtEntity.getChgFactorNo() )
			.setComment		 ( comment )
			.setCtgNm		 ( pjtEntity.getCtgNm() )
			.setMstoneNm	 ( pjtEntity.getMstoneNm() )
			.setDataAttribute( DataAttribute.ChangeProperty )
			;
	}


	private void getRecentUpdateViewOfAreq(RecentUpdateViewBean view , IHistEntity entity){
		AreqCondition condition = new AreqCondition();
		condition.setDelStsId( (StatusFlg)null );
		condition.setAreqId( view.getDataId() );

		IAreqEntity areqEntity = this.amSupport.getAreqDao().findByPrimaryKey(condition.getCondition());
		if(areqEntity == null) return;

		String comment = null;
		DataAttribute dataAttribute = DataAttribute.none;
		String actStsId = view.getActStsId();
		//String areqCtgCd = this.getAreqCtgCd(entity.getHistData());
		String areqCtgCd = umSupport.getAreqHistDao().unmarshall( entity.getHistData() ).getAreqCtgCd();

		if(AreqCtgCd.LendingRequest.equals( areqCtgCd ))
			dataAttribute = DataAttribute.CheckOutRequest;

		else if(AreqCtgCd.ReturningRequest.equals( areqCtgCd ))
			dataAttribute = DataAttribute.CheckInRequest;

		else if(AreqCtgCd.RemovalRequest.equals( areqCtgCd ))
			dataAttribute = DataAttribute.RemovalRequest;

		if(UmActStatusId.Remove.equals(actStsId))
			comment = areqEntity.getDelCmt();

		else if(UmActStatusId.Approve.equals(actStsId) ||
				UmActStatusId.MultipleApprove.equals(actStsId))
			comment =
					amSupport.findPjtAvlEntity(areqEntity.getPjtId(), areqEntity.getPjtAvlSeqNo()).getPjtAvlCmt();

		view.setDataSubject	 ( areqEntity.getSummary() )
			.setComment		 ( comment )
			.setCtgNm		 ( areqEntity.getCtgNm() )
			.setMstoneNm	 ( areqEntity.getMstoneNm() )
			.setDataAttribute( dataAttribute )
			;
	}


	private void getRecentUpdateViewOfBp(RecentUpdateViewBean view, IHistEntity entity){
		BpCondition condition = new BpCondition();
		condition.setDelStsId( (StatusFlg)null );
		condition.setBpId( view.getDataId() );

		IBpEntity bpEntity = this.bmSupport.getBpDao().findByPrimaryKey(condition.getCondition());
		if(bpEntity == null) return;

		String comment = null;
		String actStsId = view.getActStsId();

		if(UmActStatusId.Remove.equals(actStsId))
			comment = bpEntity.getDelCmt();

		else if(UmActStatusId.Close.equals(actStsId) || UmActStatusId.MultipleClose.equals(actStsId))
			comment = bpEntity.getCloseCmt();

		view.setDataSubject	 ( bpEntity.getBpNm() )
			.setComment		 ( comment )
			.setCtgNm		 ( bpEntity.getCtgNm() )
			.setMstoneNm	 ( bpEntity.getMstoneNm() )
			.setDataAttribute( DataAttribute.BuildPackage )
			;
	}


	private void getRecentUpdateViewOfRa(RecentUpdateViewBean view , IHistEntity entity){
		RaCondition condition = new RaCondition();
		condition.setDelStsId( (StatusFlg)null );
		condition.setRaId( view.getDataId() );

		IRaEntity raEntity = this.rmSupport.getRaDao().findByPrimaryKey(condition.getCondition());
		if(raEntity == null) return;

		String comment = null;
		String actStsId = view.getActStsId();

		if(UmActStatusId.Remove.equals(actStsId))
			comment = raEntity.getDelCmt();

		else if(UmActStatusId.Close.equals(actStsId))
			comment = raEntity.getCloseCmt();

		view.setDataSubject	 ( raEntity.getBldEnvNm() )
			.setComment		 ( comment )
			.setDataAttribute( DataAttribute.ReleaseRequest )
			;
	}


	private void getRecentUpdateViewOfRp(RecentUpdateViewBean view , IHistEntity entity){
		RpCondition condition = new RpCondition();
		condition.setDelStsId( (StatusFlg)null );
		condition.setRpId( view.getDataId() );

		IRpEntity rpEntity = this.rmSupport.getRpDao().findByPrimaryKey(condition.getCondition());
		if(rpEntity == null) return;

		String comment = null;
		String actStsId = view.getActStsId();

		if(UmActStatusId.Remove.equals(actStsId))
			comment = rpEntity.getDelCmt();

		else if(UmActStatusId.Close.equals(actStsId) || UmActStatusId.MultipleClose.equals(actStsId))
			comment = rpEntity.getCloseCmt();

		view.setDataSubject	 ( rpEntity.getRpNm() )
			.setComment		 ( comment )
			.setCtgNm		 ( rpEntity.getCtgNm() )
			.setMstoneNm	 ( rpEntity.getMstoneNm() )
			.setDataAttribute( DataAttribute.ReleasePackage )
			;
	}


	private void getRecentUpdateViewOfDmDo(RecentUpdateViewBean view , IHistEntity entity){
		DmDoCondition condition = new DmDoCondition();
		condition.setDelStsId( (StatusFlg)null );
		condition.setMgtVer(view.getDataId());

		IDmDoEntity dmDoEntity = this.rmSupport.getDmDoDao().findByPrimaryKey(condition.getCondition());
		if(dmDoEntity == null) return;

		String comment = null;

		if(UmActStatusId.Remove.equals( view.getActStsId() ))
			comment = dmDoEntity.getDelCmt();

		view.setDataSubject	 ( dmDoEntity.getSummary() )
			.setComment		 ( comment )
			.setCtgNm		 ( dmDoEntity.getCtgNm() )
			.setMstoneNm	 ( dmDoEntity.getMstoneNm() )
			.setDataAttribute( DataAttribute.DeploymentJob )
			;
	}


	private void getRecentUpdateViewOfReport(RecentUpdateViewBean view , IHistEntity entity){
		RepCondition condition = new RepCondition();
		condition.setDelStsId( (StatusFlg)null );
		condition.setRepId( view.getDataId() );

		IRepEntity repEntity = ((FinderSupport)rmSupport).getRepDao().findByPrimaryKey(condition.getCondition());
		if(repEntity ==  null ) return ;

		DcmReportType dcmReportType = DcmReportType.value(repEntity.getRepCtgCd());

		view.setDataSubject		( dcmReportType.wordKey() )
			.setDataAttribute	( DataAttribute.Report )
			;
	}


	private void getRecentUpdateViewOfCtg(RecentUpdateViewBean view , IHistEntity entity){
		CtgCondition condition = new CtgCondition();
		condition.setDelStsId( (StatusFlg)null );
		condition.setCtgId(view.getDataId());

		ICtgEntity ctgEntity = this.umSupport.getCtgDao().findByPrimaryKey(condition.getCondition());

		view.setDataSubject		( ctgEntity.getCtgNm() )
			.setDataAttribute	( DataAttribute.Category )
		;
	}


	private void getRecentUpdateViewOfMstone(RecentUpdateViewBean view , IHistEntity entity){
		MstoneCondition condition = new MstoneCondition();
		condition.setDelStsId( (StatusFlg)null );
		condition.setMstoneId(view.getDataId());

		IMstoneEntity mstoneEntity = this.umSupport.getMstoneDao().findByPrimaryKey(condition.getCondition());

		view.setDataSubject		( mstoneEntity.getMstoneNm() )
			.setDataAttribute	( DataAttribute.Milestone )
		;
	}

	private void getRecentUpdateViewOfWiki(RecentUpdateViewBean view , IHistEntity entity){
		WikiCondition condition = new WikiCondition();
		condition.setDelStsId( (StatusFlg)null );
		condition.setWikiId(view.getDataId());

		IWikiEntity wikiHistEntity = this.umSupport.getWikiDao().findByPrimaryKey(condition.getCondition());

		view.setDataSubject		( wikiHistEntity.getPageNm() )
			.setDataAttribute	( DataAttribute.Wiki )
		;
	}

}
