package jp.co.blueship.tri.fw.um.domainx.project.beans.dto;

import jp.co.blueship.tri.fw.um.constants.DataAttribute;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author Akahoshi
 */
public class RecentUpdateViewBean {
	private String histId;
	private String submitterId;
	private String submitterNm;
	private String submitterIconPath;
	private DataAttribute dataAttribute;
	private String actStsId;
	private String lotId;
	private String lotNm;
	private String dataId;
	private String dataSubject;
	private String comment;
	private String mstoneNm;
	private String ctgNm;
	private String elapsedTime;
	private String updTime;
	private String stsId;
	private boolean isUnread = true;

	public String getHistId() {
		return histId;
	}

	public RecentUpdateViewBean setHistId(String histId) {
		this.histId = histId;
		return this;
	}

	public String getSubmitterId() {
		return submitterId;
	}

	public RecentUpdateViewBean setSubmitterId(String submitterId) {
		this.submitterId = submitterId;
		return this;
	}

	public String getSubmitterNm() {
		return submitterNm;
	}

	public RecentUpdateViewBean setSubmitterNm(String submitterNm) {
		this.submitterNm = submitterNm;
		return this;
	}

	public String getSubmitterIconPath() {
		return submitterIconPath;
	}

	public RecentUpdateViewBean setSubmitterIconPath(String submitterIconPath) {
		this.submitterIconPath = submitterIconPath;
		return this;
	}

	public DataAttribute getDataAttribute() {
		return dataAttribute;
	}

	public RecentUpdateViewBean setDataAttribute(DataAttribute dataAttribute) {
		this.dataAttribute = dataAttribute;
		return this;
	}

	public String getActStsId() {
		return actStsId;
	}

	public RecentUpdateViewBean setActStsId(String actStsId) {
		this.actStsId = actStsId;
		return this;
	}

	public String getLotId() {
		return lotId;
	}

	public RecentUpdateViewBean setLotId(String lotId) {
		this.lotId = lotId;
		return this;
	}

	public String getLotNm() {
		return lotNm;
	}

	public RecentUpdateViewBean setLotNm(String lotNm) {
		this.lotNm = lotNm;
		return this;
	}

	public String getDataId() {
		return dataId;
	}

	public RecentUpdateViewBean setDataId(String dataId) {
		this.dataId = dataId;
		return this;
	}

	public String getDataSubject() {
		return dataSubject;
	}

	public RecentUpdateViewBean setDataSubject(String dataSubject) {
		this.dataSubject = dataSubject;
		return this;
	}

	public String getComment() {
		return comment;
	}

	public RecentUpdateViewBean setComment(String comment) {
		this.comment = comment;
		return this;
	}

	public String getMstoneNm() {
		return mstoneNm;
	}

	public RecentUpdateViewBean setMstoneNm(String mstoneNm) {
		this.mstoneNm = mstoneNm;
		return this;
	}

	public String getCtgNm() {
		return ctgNm;
	}

	public RecentUpdateViewBean setCtgNm(String ctgNm) {
		this.ctgNm = ctgNm;
		return this;
	}

	public String getElapsedTime() {
		return elapsedTime;
	}

	public RecentUpdateViewBean setElapsedTime(String elapsedTime) {
		this.elapsedTime = elapsedTime;
		return this;
	}

	public String getUpdTime() {
		return updTime;
	}

	public RecentUpdateViewBean setUpdTime(String updTime) {
		this.updTime = updTime;
		return this;
	}

	public String getStsId() {
		return stsId;
	}
	public RecentUpdateViewBean setStsId(String stsId) {
		this.stsId = stsId;
		return this;
	}

	public boolean isUnread(){
		return isUnread;
	}
	public RecentUpdateViewBean setIsUnread( boolean isUnread ){
		this.isUnread = isUnread;
		return this;
	}

}
