package jp.co.blueship.tri.fw.um.domainx.admin.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.um.domainx.admin.beans.dto.DepartmentEditInputBean;
import jp.co.blueship.tri.fw.um.domainx.admin.beans.dto.DepartmentUserViewBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class FlowDepartmentCreationServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private RequestsCompletion result = new RequestsCompletion();
	private List<DepartmentUserViewBean> userViews = new ArrayList<DepartmentUserViewBean>();

	public RequestsCompletion getResult() {
		return result;
	}

	public FlowDepartmentCreationServiceBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}
	public RequestParam getParam() {
		return param;
	}

	public List<DepartmentUserViewBean> getUserViews() {
		return userViews;
	}
	public FlowDepartmentCreationServiceBean setUserViews(List<DepartmentUserViewBean> userViews) {
		this.userViews = userViews;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private DepartmentEditInputBean inputInfo = new DepartmentEditInputBean();

		public DepartmentEditInputBean getInputInfo() {
			return inputInfo;
		}
		public RequestParam setInputInfo(DepartmentEditInputBean inputInfo) {
			this.inputInfo = inputInfo;
			return this;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private boolean completed = false;
		private String deptId;

		public String getDeptId() {
			return deptId;
		}
		public RequestsCompletion setDeptId(String deptId) {
			this.deptId = deptId;
			return this;
		}

		public boolean isCompleted() {
			return completed;
		}
		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}
}
