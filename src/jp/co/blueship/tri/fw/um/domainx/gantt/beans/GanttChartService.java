package jp.co.blueship.tri.fw.um.domainx.gantt.beans;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import jp.co.blueship.tri.am.dao.areq.constants.AreqItems;
import jp.co.blueship.tri.am.dao.areq.eb.AreqCondition;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.baseline.constants.HeadBlItems;
import jp.co.blueship.tri.am.dao.baseline.constants.LotBlItems;
import jp.co.blueship.tri.am.dao.baseline.eb.HeadBlCondition;
import jp.co.blueship.tri.am.dao.baseline.eb.IHeadBlDto;
import jp.co.blueship.tri.am.dao.baseline.eb.IHeadBlEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.IHeadLotBlLnkEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.LotBlCondition;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.support.AmDaoFinder;
import jp.co.blueship.tri.am.support.IAmFinderSupport;
import jp.co.blueship.tri.bm.dao.bp.constants.BpItems;
import jp.co.blueship.tri.bm.dao.bp.eb.BpCondition;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.bm.support.IBmFinderSupport;
import jp.co.blueship.tri.dm.dao.constants.DmDoItems;
import jp.co.blueship.tri.dm.dao.dmdo.eb.DmDoCondition;
import jp.co.blueship.tri.dm.dao.dmdo.eb.IDmDoEntity;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusId;
import jp.co.blueship.tri.fw.constants.status.AmHeadBlMergeStatusCode;
import jp.co.blueship.tri.fw.constants.status.AmLotBlStatusId;
import jp.co.blueship.tri.fw.constants.status.BmBpStatusId;
import jp.co.blueship.tri.fw.constants.status.DmDoStatusId;
import jp.co.blueship.tri.fw.constants.status.RmRaStatusId;
import jp.co.blueship.tri.fw.constants.status.RmRpStatusId;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.um.constants.DataAttribute;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.IMstoneEntity;
import jp.co.blueship.tri.fw.um.domainx.gantt.beans.dto.GanttViewBean;
import jp.co.blueship.tri.fw.um.domainx.gantt.beans.dto.GanttViewBean.GanttContent;
import jp.co.blueship.tri.fw.um.domainx.gantt.beans.dto.GanttViewBean.GanttDay;
import jp.co.blueship.tri.fw.um.domainx.gantt.beans.dto.GanttViewBean.GanttHeader;
import jp.co.blueship.tri.fw.um.domainx.gantt.beans.dto.GanttViewBean.GanttPeriod;
import jp.co.blueship.tri.fw.um.domainx.gantt.beans.dto.GanttViewBean.GroupingDataType;
import jp.co.blueship.tri.fw.um.domainx.gantt.beans.dto.GanttViewBean.Status;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;
import jp.co.blueship.tri.rm.dao.ra.constants.RaItems;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.RaCondition;
import jp.co.blueship.tri.rm.dao.rp.constants.RpItems;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.RpCondition;
import jp.co.blueship.tri.rm.support.IRmFinderSupport;

/**
 * @version V4.00.00
 * @author Akahoshi
 *
 * @version V4.03.00
 * @author Cuong Nguyen
 *
 */
public class GanttChartService {
	private IAmFinderSupport amSupport = null;
	private IBmFinderSupport bmSupport = null;
	private IRmFinderSupport rmSupport = null;
	private IUmFinderSupport umSupport = null;

	public void setAmSupport(IAmFinderSupport amSupport) {
		this.amSupport = amSupport;
	}
	public void setBmSupport(IBmFinderSupport bmSupport) {
		this.bmSupport = bmSupport;
	}
	public void setRmSupport(IRmFinderSupport rmSupport) {
		this.rmSupport = rmSupport;
	}

	public void setUmSupport(IUmFinderSupport umSupport) {
		this.umSupport = umSupport;
	}

	/**
	 * Get the GanttContent of all kind.<br>
	 * <br>
	 * 全ての種類のganttContentを取得します。<br>
	 *
	 * @param timeZone
	 * @param language
	 * @param searchStatus
	 * @param lotId
	 * @param mstoneId
	 * @param ctgId
	 * @param assigneeId
	 * @param ganttStartDate
	 * @param ganttEndDate
	 * @return ganttContentList
	 */
	public List<GanttContent> getGanttContentListOfAllDataType( TimeZone timeZone,
																String language,
																Status searchStatus,
																String lotId,
																String mstoneId,
																String ctgId,
																String assigneeId,
																Timestamp ganttStartDate,
																Timestamp ganttEndDate){

		List<GanttContent> ganttContentList = new ArrayList<GanttContent>();

		AreqCondition areqCondition = new AreqCondition();
		if( lotId != null )		 areqCondition.setLotId			( lotId );
		if( mstoneId != null )	 areqCondition.setMstoneId		( mstoneId );
		if( ctgId != null )		 areqCondition.setCtgId			( ctgId );
		if( assigneeId != null ) areqCondition.setAssigneeIds	( new String[]{assigneeId} );

		ganttContentList.addAll(this.getAreqGanttContentList( language,
															  timeZone,
															  searchStatus,
															  areqCondition,
															  ganttStartDate,
															  ganttEndDate ));

		BpCondition bpCondition = new BpCondition();
		if( lotId != null )		 bpCondition.setLotId		( lotId );
		if( mstoneId != null )	 bpCondition.setMstoneId	( mstoneId );
		if( ctgId != null )		 bpCondition.setCtgId		( ctgId );
		if( assigneeId != null ) bpCondition.setRegUserId	( assigneeId );

		ganttContentList.addAll(this.getBpGanttContentList(  language,
				  											 timeZone,
				  											 searchStatus,
				  											 bpCondition,
				  											 ganttStartDate,
				  											 ganttEndDate ));

		RaCondition raCondition = new RaCondition();
		if( lotId != null )		 raCondition.setLotId		( lotId );
		if( mstoneId != null )	 raCondition.setMstoneId	( mstoneId );
		if( ctgId != null )		 raCondition.setCtgId		( ctgId );
		if( assigneeId != null ) raCondition.setReqUserId	( assigneeId );

		ganttContentList.addAll(this.getRaGanttContentList( language,
															timeZone,
															searchStatus,
															raCondition,
															ganttStartDate,
															ganttEndDate ));

		RpCondition rpCondition = new RpCondition();
		if( lotId != null )		 rpCondition.setLotId		( lotId );
		if( mstoneId != null )	 rpCondition.setMstoneId	( mstoneId );
		if( ctgId != null )		 rpCondition.setCtgId		( ctgId );
		if( assigneeId != null ) rpCondition.setExecUserId	( assigneeId );

		ganttContentList.addAll(this.getRpGanttContentList( language,
															timeZone,
															searchStatus,
															rpCondition,
															ganttStartDate,
															ganttEndDate ));

		bpCondition = new BpCondition();
		if( lotId != null )		 bpCondition.setLotId		( lotId );
		if( mstoneId != null )	 bpCondition.setMstoneId	( mstoneId );
		if( ctgId != null )		 bpCondition.setCtgId		( ctgId );
		if( assigneeId != null ) bpCondition.setRegUserId	( assigneeId );

		ganttContentList.addAll(this.getBpCloseGanttContentList( language,
																 timeZone,
																 searchStatus,
																 bpCondition,
																 ganttStartDate,
																 ganttEndDate ));

		DmDoCondition dmDoCondition = new DmDoCondition();
		dmDoCondition.setStsId( DmDoStatusId.JobRegistered.getStatusId() );
		if( mstoneId != null )	 dmDoCondition.setMstoneId	( mstoneId );
		if( ctgId != null )		 dmDoCondition.setCtgId		( ctgId );
		if( assigneeId != null ) dmDoCondition.setRegUserId	( assigneeId );

		ganttContentList.addAll(this.getDeploymentGanttContentList( language,
																	timeZone,
																	searchStatus,
																	lotId,
																	dmDoCondition,
																	ganttStartDate,
																	ganttEndDate ));

		dmDoCondition = new DmDoCondition();
		dmDoCondition.setStsId( DmDoStatusId.TimerConfigured.getStatusId() );
		if( mstoneId != null )	 dmDoCondition.setMstoneId	( mstoneId );
		if( ctgId != null )		 dmDoCondition.setCtgId		( ctgId );
		if( assigneeId != null ) dmDoCondition.setRegUserId	( assigneeId );

		ganttContentList.addAll(this.getDeploymentGanttContentList( language,
																	timeZone,
																	searchStatus,
																	lotId,
																	dmDoCondition,
																	ganttStartDate,
																	ganttEndDate ));


		HeadBlCondition headBlCondition = new HeadBlCondition();
		if( lotId != null )		 headBlCondition.setLotId		  ( lotId );
		if( assigneeId != null ) headBlCondition.setMergechkUserId( assigneeId );

		if( ctgId == null && mstoneId == null )
			ganttContentList.addAll(this.getConflictCheckGanttContentList( language,
																		   timeZone,
																		   searchStatus,
																		   headBlCondition,
																		   ganttStartDate,
																		   ganttEndDate ));

		headBlCondition = new HeadBlCondition();
		if( lotId != null )		 headBlCondition.setLotId	   ( lotId );
		if( assigneeId != null ) headBlCondition.setMergeUserId( assigneeId );

		if( ctgId == null && mstoneId == null )
			ganttContentList.addAll(this.getMergeGanttContentList( language,
																   timeZone,
																   searchStatus,
																   headBlCondition,
																   ganttStartDate,
																   ganttEndDate ));

		return ganttContentList;
	}


	/**
	 * Get the GanttContent of ChangeRequest.<br>
	 * <br>
	 * 貸出/返却/削除申請のGanttContentを取得します。<br>
	 *
	 * @param language
	 * @param timeZone
	 * @param searchStatus
	 * @param condition
	 * @param ganttStartDate
	 * @param ganttEndDate
	 * @return ganttContentList
	 */
	public List<GanttContent> getAreqGanttContentList( String language,
													   TimeZone timeZone,
													   Status searchStatus,
													   AreqCondition condition,
													   Timestamp ganttStartDate,
													   Timestamp ganttEndDate){

		String[] areqStsIds ={ AmAreqStatusId.CheckoutRequested.getStatusId(),
							   AmAreqStatusId.CheckinRequested.getStatusId(), AmAreqStatusId.CheckinRequestApproved.getStatusId(),
							   AmAreqStatusId.RemovalRequested.getStatusId(), AmAreqStatusId.RemovalRequestApproved.getStatusId(),
							   AmAreqStatusId.Merged.getStatusId(),
							   AmAreqStatusId.BuildPackageClosed.getStatusId()};
		condition.setStsIds		( areqStsIds );

		ISqlSort sort = new SortBuilder();
		sort.setElement(AreqItems.reqTimestamp, TriSortOrder.Asc, 1);
		List<IAreqEntity> areqEntityList = amSupport.getAreqDao().find(condition.getCondition(), sort);

		List<GanttContent> ganttContentList = new ArrayList<GanttContent>();

		for(IAreqEntity areqEntity : areqEntityList ){
			IPjtEntity pjtEntity = amSupport.findPjtEntity( areqEntity.getPjtId() );

			DataAttribute dataAttribute = DataAttribute.none;
			GroupingDataType groupingDataType = GroupingDataType.none;
			Status status = Status.InProgress;
			Timestamp startDate = areqEntity.getReqTimestamp();
			Timestamp endDate = startDate;
			String areqCtgCd = areqEntity.getAreqCtgCd();

			String stsId = areqEntity.getStsId();
			if( AmAreqStatusId.BuildPackageClosed.equals( stsId ) ||
				AmAreqStatusId.Merged.equals( stsId ))
				status = Status.Closed;

			else if( AmAreqStatusId.CheckinRequestApproved.equals(stsId) ||
					 AmAreqStatusId.RemovalRequestApproved.equals(stsId) )
				status = Status.Completed;


			if( AreqCtgCd.LendingRequest.equals( areqCtgCd ) ){
				dataAttribute = DataAttribute.CheckOutRequest;
				groupingDataType = GroupingDataType.CheckOutRequest;

				if( TriStringUtils.isNotEmpty( areqEntity.getRtnReqDueDate() ) )
					endDate = this.convertStringToTimestampWithDateTime(areqEntity.getRtnReqDueDate(),timeZone);

			} else if( AreqCtgCd.ReturningRequest.equals( areqCtgCd ) ){
				dataAttribute = DataAttribute.CheckInRequest;
				groupingDataType = GroupingDataType.CheckInRequest;

			if(!Status.InProgress.equals(status))
				endDate = pjtEntity.getPjtAvlTimestamp();

			} else if ( AreqCtgCd.RemovalRequest.equals( areqCtgCd ) ){
				dataAttribute = DataAttribute.RemovalRequest;
				groupingDataType = GroupingDataType.RemovalRequest;

				if(!Status.InProgress.equals(status))
					endDate = pjtEntity.getPjtAvlTimestamp();
			}

			int seq       = this.getSeq( ganttStartDate, startDate );
			int countDays = this.getCountDays( ganttStartDate, ganttEndDate, endDate, seq );

			if(this.getDiffDay( ganttEndDate, startDate ) > 0)
				continue;

			if(!this.isHitSearchStatus( searchStatus, status ))
				continue;

			if( countDays < 1 )
				continue;

			GanttContent content = new GanttViewBean().new GanttContent()
					.setGroupingDataType(groupingDataType)
					.setDataAttribute	( dataAttribute )
					.setDataId			( areqEntity.getAreqId() )
					.setSubject			( pjtEntity.getChgFactorNo() )
					.setAssigneeNm		( areqEntity.getAssigneeNm() )
					.setAssigneeIconPath( this.umSupport.getIconPath(areqEntity.getAssigneeId()) )
					.setStartDate		( TriDateUtils.convertViewDateFormat( startDate, TriDateUtils.getYMDDateFormat( language, timeZone)) )
					.setEndDate			( TriDateUtils.convertViewDateFormat(endDate, TriDateUtils.getYMDDateFormat( language, timeZone)) )
					.setSeq				( seq )
					.setCountDays		( countDays )
					.setStatus			( status )
					;
			ganttContentList.add(content);
		}
		return ganttContentList;
	}

	/**
	 * Get the GanttContent of BuildPackage.<br>
	 * <br>
	 * ビルドパッケージのGanttContentを取得します。<br>
	 *
	 * @param language
	 * @param timeZone
	 * @param searchStatus
	 * @param condition
	 * @param ganttStartDate
	 * @param ganttEndDate
	 * @return ganttContentList
	 */
	public List<GanttContent> getBpGanttContentList( String language,
													 TimeZone timeZone,
													 Status searchStatus,
													 BpCondition condition,
													 Timestamp ganttStartDate,
													 Timestamp ganttEndDate){

		String[] stsIds = { BmBpStatusId.BuildPackageCreated.getStatusId(),
							BmBpStatusId.BuildPackageClosed.getStatusId()};
		condition.setStsIds				( stsIds );
		condition.setProcStTimeFromTo	((String)null, TriDateUtils.convertTimezone( ganttEndDate, TriDateUtils.getDefaultTimeZone() ));

		ISqlSort sort = new SortBuilder();
		sort.setElement( BpItems.regTimestamp, TriSortOrder.Asc, 1 );
		List<IBpEntity> bpEntityList = bmSupport.getBpDao().find( condition.getCondition(), sort );

		List<GanttContent> ganttContentList = new ArrayList<GanttContent>();

		for(IBpEntity bpEntity : bpEntityList){
			Status status = BmBpStatusId.BuildPackageClosed.equals( bpEntity.getStsId() )? Status.Closed : Status.Completed;

			int seq       = this.getSeq( ganttStartDate, bpEntity.getProcStTimestamp() );
			int countDays = this.getCountDays( ganttStartDate, ganttEndDate, bpEntity.getProcEndTimestamp(), seq );

			if(!this.isHitSearchStatus( searchStatus, status ))
				continue;

			if( countDays < 1 )
				continue;

			GanttContent content = new GanttViewBean().new GanttContent()
					.setGroupingDataType( GroupingDataType.BuildPackage )
					.setDataAttribute	( DataAttribute.BuildPackage )
					.setDataId			( bpEntity.getBpId() )
					.setSubject			( bpEntity.getBpNm() )
					.setAssigneeNm		( bpEntity.getRegUserNm() )
					.setAssigneeIconPath( this.umSupport.getIconPath(bpEntity.getRegUserId()) )
					.setStartDate		( TriDateUtils.convertViewDateFormat(bpEntity.getProcStTimestamp(), TriDateUtils.getYMDDateFormat( language, timeZone)) )
					.setEndDate			( TriDateUtils.convertViewDateFormat(bpEntity.getProcEndTimestamp(), TriDateUtils.getYMDDateFormat( language, timeZone)) )
					.setSeq				( seq )
					.setCountDays		( countDays )
					.setStatus			( status )
					;
			ganttContentList.add( content );
		}
		return ganttContentList;
	}

	/**
	 * Get the GanttContent of ReleaseRequest.<br>
	 * <br>
	 * リリース申請のGanttContentを取得します。<br>
	 *
	 * @param language
	 * @param timeZone
	 * @param searchStatus
	 * @param condition
	 * @param ganttStartDate
	 * @param ganttEndDate
	 * @return ganttContentList
	 */
	public List<GanttContent> getRaGanttContentList( String language,
													 TimeZone timeZone,
													 Status searchStatus,
													 RaCondition condition,
													 Timestamp ganttStartDate,
													 Timestamp ganttEndDate) {


		String[] stsIds = { RmRaStatusId.ReleaseRequested.getStatusId(),
							RmRaStatusId.PendingReleaseRequest.getStatusId(),
							RmRaStatusId.ReleaseRequestApproved.getStatusId(),
							RmRaStatusId.ReleaseRequestClosed.getStatusId(),
							RmRaStatusId.ReleasePackageCreated.getStatusId()};
		condition.setStsIds( stsIds );

		ISqlSort sort = new SortBuilder();
		sort.setElement( RaItems.reqTimestamp, TriSortOrder.Asc, 1 );
		List<IRaEntity> raEntityList = rmSupport.getRaDao().find( condition.getCondition(), sort );

		List<GanttContent> ganttContentList = new ArrayList<GanttContent>();

		for( IRaEntity raEntity : raEntityList ){
			Timestamp startDate = raEntity.getReqTimestamp();
			Timestamp endDate = this.convertStringToTimestampWithDateTime(raEntity.getPreferredRelDate(), timeZone);

			int seq       = this.getSeq( ganttStartDate, startDate );
			int countDays = this.getCountDays( ganttStartDate, ganttEndDate, endDate, seq );

			Status status = Status.Closed;

			if( RmRaStatusId.ReleaseRequested.equals( raEntity.getStsId() ))
				status = Status.InProgress;

			else if( RmRaStatusId.ReleaseRequestApproved.equals( raEntity.getStsId() ))
				status = Status.Completed;

			if(this.getDiffDay(ganttEndDate, startDate) > 0)
				continue;

			if(!this.isHitSearchStatus( searchStatus, status ))
				continue;

			if( countDays < 1 )
				continue;

			GanttContent content = new GanttViewBean().new GanttContent()
					.setGroupingDataType( GroupingDataType.ReleaseRequest )
					.setDataAttribute	( DataAttribute.ReleaseRequest )
					.setDataId			( raEntity.getRaId() )
					.setSubject			( raEntity.getBldEnvNm() )
					.setAssigneeNm		( raEntity.getReqUser() )
					.setAssigneeIconPath( this.umSupport.getIconPath(raEntity.getReqUserId()) )
					.setStartDate		( TriDateUtils.convertViewDateFormat(startDate, TriDateUtils.getYMDDateFormat( language, timeZone)) )
					.setEndDate			( TriDateUtils.convertViewDateFormat(endDate, TriDateUtils.getYMDDateFormat( language, timeZone)) )
					.setSeq				( seq )
					.setCountDays		( countDays )
					.setStatus			( status )
					;
			ganttContentList.add( content );
		}
		return ganttContentList;
	}

	/**
	 * Get the GanttContent of ReleasePackage.<br>
	 * <br>
	 * リリースパッケージのGanttContentを取得します。<br>
	 *
	 * @param language
	 * @param timeZone
	 * @param searchStatus
	 * @param condition
	 * @param ganttStartDate
	 * @param ganttEndDate
	 * @return GanttContentList
	 */
	public List<GanttContent> getRpGanttContentList( String language,
													 TimeZone timeZone,
													 Status searchStatus,
													 RpCondition condition,
													 Timestamp ganttStartDate,
													 Timestamp ganttEndDate){

		String[] stsIds = {RmRpStatusId.ReleasePackageCreated.getStatusId(), RmRpStatusId.ReleasePackageClosed.getStatusId()};
		condition.setStsIds				( stsIds );
		condition.setProcStTimeFromTo	((String)null, TriDateUtils.convertTimezone( ganttEndDate, TriDateUtils.getDefaultTimeZone() ));

		ISqlSort sort = new SortBuilder();
		sort.setElement( RpItems.procStTimestamp, TriSortOrder.Asc, 1 );
		List<IRpEntity> rpEntityList = rmSupport.getRpDao().find( condition.getCondition(), sort );

		List<GanttContent> ganttContentList = new ArrayList<GanttContent>();

		for(IRpEntity rpEntity : rpEntityList ){
			Status status = RmRpStatusId.ReleasePackageClosed.equals( rpEntity.getStsId() )? Status.Closed : Status.Completed;

			int seq       = this.getSeq( ganttStartDate, rpEntity.getProcStTimestamp() );
			int countDays = this.getCountDays( ganttStartDate, ganttEndDate, rpEntity.getProcEndTimestamp(), seq );

			if(!this.isHitSearchStatus( searchStatus, status ))
				continue;

			if( countDays < 1 )
				continue;

			GanttContent content = new GanttViewBean().new GanttContent()
					.setGroupingDataType( GroupingDataType.ReleasePackage )
					.setDataAttribute	( DataAttribute.ReleasePackage)
					.setDataId			( rpEntity.getRpId() )
					.setSubject			( rpEntity.getSummary() )
					.setAssigneeNm		( rpEntity.getExecUserNm() )
					.setAssigneeIconPath( this.umSupport.getIconPath(rpEntity.getExecUserId()) )
					.setStartDate		( TriDateUtils.convertViewDateFormat(rpEntity.getProcStTimestamp(), TriDateUtils.getYMDDateFormat( language, timeZone)) )
					.setEndDate			( TriDateUtils.convertViewDateFormat(rpEntity.getProcEndTimestamp(), TriDateUtils.getYMDDateFormat( language, timeZone)) )
					.setSeq				( seq )
					.setCountDays		( countDays )
					.setStatus			( status )
					;
			ganttContentList.add(content);
		}
		return ganttContentList;
	}

	/**
	 * Get the GanttContent of BuildPackageClose.<br>
	 * <br>
	 * クローズ済みのビルドパッケージのGanttContentを取得します。<br>
	 *
	 * @param language
	 * @param timeZone
	 * @param searchStatus
	 * @param condition
	 * @param ganttStartDate
	 * @param ganttEndDate
	 * @return
	 */
	public List<GanttContent> getBpCloseGanttContentList( String language,
														  TimeZone timeZone,
														  Status searchStatus,
														  BpCondition condition,
														  Timestamp ganttStartDate,
														  Timestamp ganttEndDate){

		condition.setStsId			( BmBpStatusId.BuildPackageClosed.getStatusId() );
		condition.setCloseTimeFromTo((String)null, TriDateUtils.convertTimezone( ganttEndDate, TriDateUtils.getDefaultTimeZone() ) );

		ISqlSort sort = new SortBuilder();
		sort.setElement( BpItems.closeTimestamp, TriSortOrder.Asc, 1 );
		List<IBpEntity> bpEntityList = bmSupport.getBpDao().find( condition.getCondition(), sort );

		if( bpEntityList.size() == 0 )
			return new ArrayList<GanttContent>();

		String latestCloseBpId = this.getLatestCloseBpId( condition.getLotId() );

		List<GanttContent> ganttContentList = new ArrayList<GanttContent>();
		List<String> verTags = new ArrayList<String>();

		for( IBpEntity bpEntity : bpEntityList ){
			Status status = latestCloseBpId.equals( bpEntity.getBpId() )? Status.Completed : Status.Closed;

			int seq       = this.getSeq( ganttStartDate, bpEntity.getCloseTimestamp() );
			int countDays = this.getCountDays( ganttStartDate, ganttEndDate, bpEntity.getCloseTimestamp(), seq );

			if(!this.isHitSearchStatus( searchStatus, status ))
				continue;

			if( countDays < 1 )
				continue;

			if( verTags.contains( bpEntity.getBpCloseVerTag() ) )
				continue;
			verTags.add( bpEntity.getBpCloseVerTag() );

			GanttContent content = new GanttViewBean().new GanttContent()
					.setGroupingDataType( GroupingDataType.BuildPackageClose )
					.setDataAttribute	( DataAttribute.BuildPackage )
					.setDataId			( bpEntity.getBpId() )
					.setSubject			( bpEntity.getBpCloseBlTag() )
					.setAssigneeNm		( bpEntity.getCloseUserNm() )
					.setAssigneeIconPath( this.umSupport.getIconPath(bpEntity.getCloseUserId()) )
					.setStartDate		( TriDateUtils.convertViewDateFormat(bpEntity.getCloseTimestamp(), TriDateUtils.getYMDDateFormat( language, timeZone)) )
					.setEndDate			( TriDateUtils.convertViewDateFormat(bpEntity.getCloseTimestamp(), TriDateUtils.getYMDDateFormat( language, timeZone)) )
					.setSeq				( seq )
					.setCountDays		( countDays )
					.setStatus			( status )
					;
			ganttContentList.add(content);
		}
		return ganttContentList;
	}

	/**
	 * Get the GanttContent of DeploymentJob.<br>
	 * <br>
	 * リリース連携のGanttContentを取得します。<br>
	 *
	 * @param language
	 * @param timeZone
	 * @param searchStatus
	 * @param lotId
	 * @param condition
	 * @param ganttStartDate
	 * @param ganttEndDate
	 * @return
	 */
	public List<GanttContent> getDeploymentGanttContentList( String language,
															 TimeZone timeZone,
															 Status searchStatus,
															 String lotId,
															 DmDoCondition condition,
															 Timestamp ganttStartDate,
															 Timestamp ganttEndDate){

		RpCondition rpCondition = new RpCondition();
		rpCondition.setStsIds(new String[]{ RmRpStatusId.ReleasePackageCreated.getStatusId(),
											RmRpStatusId.ReleasePackageClosed.getStatusId()} );
		rpCondition.setLotId( lotId );

		ISqlSort sort = new SortBuilder();
		sort.setElement( RpItems.relTimestamp, TriSortOrder.Desc, 1 );
		List<IRpEntity> rpEntityList = rmSupport.getRpDao().find(rpCondition.getCondition(), sort);

		if(rpEntityList.size() == 0)
			return new ArrayList<GanttContent>();

		String latestRpId = rpEntityList.get(0).getRpId();
		List<String> rpIds = new ArrayList<String>();
		rpIds.add( "dummy" );

		for( IRpEntity rpEntity : rpEntityList )
			rpIds.add(rpEntity.getRpId());

		condition.setRpIds(rpIds.toArray(new String[0]));
		condition.setRegTimeFromTo( (String)null, TriDateUtils.convertTimezone( ganttEndDate, TriDateUtils.getDefaultTimeZone() ));

		sort = new SortBuilder();
		sort.setElement( DmDoItems.regTimestamp, TriSortOrder.Asc, 1 );
		List<IDmDoEntity> dmDoEntityList = rmSupport.getDmDoDao().find(condition.getCondition(), sort);

		List<GanttContent> ganttContentList = new ArrayList<GanttContent>();

		for(IDmDoEntity dmDoEntity : dmDoEntityList){
			if(DmDoStatusId.JobCancelled.getStatusId().equals(dmDoEntity.getStsId()))
				continue;

			GroupingDataType groupingDataType = (DmDoStatusId.TimerConfigured.equals(dmDoEntity.getStsId()))?
													GroupingDataType.DeploymentJob : GroupingDataType.DeploymentJobTimer;
			DataAttribute dataAttribute = DataAttribute.DeploymentJob;

			Status status = latestRpId.equals(dmDoEntity.getRpId())? Status.Completed : Status.Closed ;

			Timestamp startDate = dmDoEntity.getRegTimestamp();
			Timestamp endDate = startDate;

			if( DmDoStatusId.TimerConfigured.getStatusId().equals( dmDoEntity.getStsId() ) )
					endDate = this.convertStringToTimestampWithDateTime(dmDoEntity.getTimerSettingDate(), timeZone);

			int seq       = this.getSeq( ganttStartDate, startDate );
			int countDays = this.getCountDays( ganttStartDate, ganttEndDate, endDate, seq );

			if(!this.isHitSearchStatus( searchStatus, status ))
				continue;

			if( countDays < 1 )
				continue;

			GanttContent content = new GanttViewBean().new GanttContent()
					.setGroupingDataType(groupingDataType)
					.setDataAttribute	( dataAttribute )
					.setDataId			( dmDoEntity.getMgtVer() )
					.setSubject			( dmDoEntity.getSummary() )
					.setAssigneeNm		( dmDoEntity.getRegUserNm() )
					.setAssigneeIconPath( this.umSupport.getIconPath(dmDoEntity.getRegUserId()) )
					.setStartDate		( TriDateUtils.convertViewDateFormat(startDate, TriDateUtils.getYMDDateFormat( language, timeZone)) )
					.setEndDate			( TriDateUtils.convertViewDateFormat(endDate, TriDateUtils.getYMDDateFormat( language, timeZone)) )
					.setSeq				( seq )
					.setCountDays		( countDays )
					.setStatus			( status )
					;
			ganttContentList.add(content);
		}
		return ganttContentList;
	}

	/**
	 * Get the GanttContent of conflictCheck.<br>
	 * <br>
	 * コンフリクトチェックのGanttContentを取得します。<br>
	 *
	 * @param language
	 * @param timeZone
	 * @param searchStatus
	 * @param condition
	 * @param ganttStartDate
	 * @param ganttEndDate
	 * @return
	 */
	public List<GanttContent> getConflictCheckGanttContentList( String language,
																TimeZone timeZone,
																Status searchStatus,
																HeadBlCondition condition,
																Timestamp ganttStartDate,
																Timestamp ganttEndDate){

		String[] stsCds = { AmHeadBlMergeStatusCode.LOT_MERGE_CHECK_SUCCESS.getStatusCode(),
							AmHeadBlMergeStatusCode.LOT_MERGE_CHECK_RESOURCE_WARNING.getStatusCode(),
							AmHeadBlMergeStatusCode.LOT_MERGE_SUCCESS.getStatusCode(),
							AmHeadBlMergeStatusCode.LOT_MERGE_RESOURCE_WARNING.getStatusCode()};
		condition.setMergeStsCds			( stsCds );
		condition.setMergechkStTimeFromTo	((String)null, TriDateUtils.convertTimezone( ganttEndDate, TriDateUtils.getDefaultTimeZone() ));

		ISqlSort sort = new SortBuilder();
		sort.setElement( HeadBlItems.mergechkStTimestamp, TriSortOrder.Asc, 1 );
		List<IHeadBlEntity> headBlEntityList = ((AmDaoFinder)amSupport).getHeadBlDao().find(condition.getCondition(), sort);

		List<GanttContent> ganttContentList = new ArrayList<GanttContent>();

		for( IHeadBlEntity headBlEntity : headBlEntityList ){
			Status status = Status.Completed;

			int seq       = this.getSeq( ganttStartDate, headBlEntity.getMergechkStTimestamp() );
			int countDays = this.getCountDays( ganttStartDate, ganttEndDate, headBlEntity.getMergechkEndTimestamp(), seq );

			if( AmHeadBlMergeStatusCode.LOT_MERGE_SUCCESS.equals(headBlEntity.getMergeStsCd()) ||
				AmHeadBlMergeStatusCode.LOT_MERGE_RESOURCE_WARNING.equals(headBlEntity.getMergeStsCd()))
				status = Status.Closed;

			if(!this.isHitSearchStatus( searchStatus, status ))
				continue;

			if( countDays < 1 )
				continue;
			
			ILotBlEntity lotBlDto= getLotBlEntityByMergeEnableAtFirst(headBlEntity.getLotId(),headBlEntity.getHeadBlId());

			GanttContent content = new GanttViewBean().new GanttContent()
					.setGroupingDataType( GroupingDataType.ConflictCheck )
					.setDataAttribute	( DataAttribute.Lot )
					.setDataId			( headBlEntity.getLotId() )
					.setSubject			( lotBlDto.getLotBlTag() )
					.setAssigneeNm		( headBlEntity.getMergechkUserNm() )
					.setAssigneeIconPath( this.umSupport.getIconPath(headBlEntity.getMergechkUserId()) )
					.setStartDate		( TriDateUtils.convertViewDateFormat(headBlEntity.getMergechkStTimestamp(), TriDateUtils.getYMDDateFormat( language, timeZone)) )
					.setEndDate			( TriDateUtils.convertViewDateFormat(headBlEntity.getMergechkEndTimestamp(), TriDateUtils.getYMDDateFormat( language, timeZone)) )
					.setSeq				( seq )
					.setCountDays		( countDays )
					.setStatus			( status )
					;
			ganttContentList.add(content);
		}
		return ganttContentList;
	}

	/**
	 * Get the GanttContent of merge.<br>
	 * <br>
	 * マージのGanttContentを取得します。<br>
	 *
	 * @param language
	 * @param timeZone
	 * @param searchStatus
	 * @param condition
	 * @param ganttStartDate
	 * @param ganttEndDate
	 * @return
	 */
	public List<GanttContent> getMergeGanttContentList( String language,
														TimeZone timeZone,
														Status searchStatus,
														HeadBlCondition condition,
														Timestamp ganttStartDate,
														Timestamp ganttEndDate){

		String[] stsCds = { AmHeadBlMergeStatusCode.LOT_MERGE_SUCCESS.getStatusCode(),
							AmHeadBlMergeStatusCode.LOT_MERGE_RESOURCE_WARNING.getStatusCode()};
		condition.setMergeStsCds		( stsCds );
		condition.setMergeStTimeFromTo	((String)null, TriDateUtils.convertTimezone( ganttEndDate, TriDateUtils.getDefaultTimeZone() ));

		ISqlSort sort = new SortBuilder();
		sort.setElement( HeadBlItems.mergeStTimestamp, TriSortOrder.Asc, 1 );
		List<IHeadBlEntity> headBlEntityList = ((AmDaoFinder)amSupport).getHeadBlDao().find( condition.getCondition(), sort );

		if( headBlEntityList.size() == 0 )
			return new ArrayList<GanttContent>();

		String latestMergeDataId = this.getLatestMergeDataId( condition.getLotId());

		List<GanttContent> ganttContentList = new ArrayList<GanttContent>();

		for(IHeadBlEntity headBlEntity : headBlEntityList){
			Status status = latestMergeDataId.equals(headBlEntity.getHeadBlId())? Status.Completed : Status.Closed;

			int seq       = this.getSeq( ganttStartDate, headBlEntity.getMergeStTimestamp() );
			int countDays = this.getCountDays( ganttStartDate, ganttEndDate, headBlEntity.getMergeEndTimestamp(), seq );

			if(!this.isHitSearchStatus( searchStatus, status ))
				continue;

			if( countDays < 1 )
				continue;

			GanttContent content = new GanttViewBean().new GanttContent()
					.setGroupingDataType( GroupingDataType.Merge )
					.setDataAttribute	( DataAttribute.Lot )
					.setDataId			( headBlEntity.getLotId() )
					.setSubject			( headBlEntity.getHeadVerTag() )
					.setAssigneeNm		( headBlEntity.getMergeUserNm() )
					.setAssigneeIconPath( this.umSupport.getIconPath(headBlEntity.getMergeUserId()) )
					.setStartDate		( TriDateUtils.convertViewDateFormat(headBlEntity.getMergeStTimestamp(), TriDateUtils.getYMDDateFormat( language, timeZone)) )
					.setEndDate			( TriDateUtils.convertViewDateFormat(headBlEntity.getMergeEndTimestamp(), TriDateUtils.getYMDDateFormat( language, timeZone)) )
					.setSeq				( seq )
					.setCountDays		( countDays )
					.setStatus			( status )
					;
			ganttContentList.add(content);
		}
		return ganttContentList;
	}

	/**
	 * Get the GanttHaeder.<br>
	 * <br>
	 * GanttHeaderを取得します。<br>
	 *
	 * @param language
	 * @param timeZone
	 * @param startTime
	 * @param span
	 * @param entityList
	 * @return GanttHeader
	 */
	public GanttHeader getGanttHeader( String language,
									   TimeZone timeZone,
									   Timestamp startTime,
									   int span,
									   List<IMstoneEntity> entityList){
		Calendar todayCal = Calendar.getInstance( timeZone );
		todayCal.setTime(new Date( TriDateUtils.getSystemTimestamp().getTime() ));

		Calendar endDayCal = Calendar.getInstance( timeZone );
		endDayCal.setTime(new Date( startTime.getTime() ));
		endDayCal.add( Calendar.MONTH, span );
		endDayCal.add( Calendar.DATE, 1 );

		Calendar cal = Calendar.getInstance( timeZone );
		cal.setTime(new Date( startTime.getTime() ));

		int seqCount =1;
		List<GanttPeriod> periods = new ArrayList<GanttPeriod>();

		while(true){
			GanttPeriod ganttPeriod = new GanttViewBean().new GanttPeriod();
			List<GanttDay> days = new ArrayList<GanttDay>();
			String period =  TriDateUtils.convertViewDateFormat(new Timestamp( cal.getTime().getTime() ),
								TriDateUtils.getYMDateFormat( language, timeZone));
			int yesterday = cal.get( Calendar.DATE );

			while(true){
				if( yesterday > cal.get( Calendar.DATE )) break;
				if( this.isEqualDay( cal, endDayCal ))	  break;

				int		thisDayOfWeek	= cal.get( Calendar.DAY_OF_WEEK );
				boolean holiday			= ( thisDayOfWeek == Calendar.SATURDAY ) || ( thisDayOfWeek == Calendar.SUNDAY );
				boolean weekBeginning	= thisDayOfWeek == Calendar.MONDAY;
				boolean today			= this.isEqualDay( cal, todayCal );
				boolean flag			= false;
				String	flagNm			= null;

				for( IMstoneEntity entity : entityList ){
					if( TriStringUtils.isEmpty( entity.getMstoneEndDate() ) )
						continue;

					String thisDay = cal.get( Calendar.YEAR ) + "/" + (cal.get( Calendar.MONTH ) + 1) + "/" + cal.get( Calendar.DATE );
					if( entity.getMstoneEndDate().equals( TriDateUtils.fillYMD( thisDay ))){
						flag = true;
						flagNm = entity.getMstoneNm();
					}
				}
				GanttDay day = new GanttViewBean().new GanttDay()
								.setSeq				( seqCount )
								.setDay				( cal.get(Calendar.DATE)+"" )
								.setToday			( today )
								.setHoliday			( holiday )
								.setWeekBeginning	( weekBeginning )
								.setFlag			( flag )
								.setFlagNm			( flagNm )
								;
				days.add( day );

				yesterday = cal.get( Calendar.DATE );

				seqCount++;
				cal.add( Calendar.DATE, +1 );
			}
			ganttPeriod.setDays		( days )
					   .setPeriod	( period );
			periods.add( ganttPeriod );

			if( this.isEqualDay( cal, endDayCal ) ) break;
		}
		return new GanttViewBean().new GanttHeader().setPeriods( periods );
	}


	private String getLatestCloseBpId( String lotId ){
		BpCondition condition = new BpCondition();
		condition.setStsId( BmBpStatusId.BuildPackageClosed.getStatusId() );
		condition.setLotId( lotId );

		ISqlSort sort = new SortBuilder();
		sort.setElement( BpItems.closeTimestamp, TriSortOrder.Desc, 1 );

		IBpEntity entity = bmSupport.getBpDao().find( condition.getCondition(), sort ).get(0);

		return entity.getBpId();
	}


	private String getLatestMergeDataId( String lotId ){
		String[] stsCds = { AmHeadBlMergeStatusCode.LOT_MERGE_SUCCESS.getStatusCode(),
							AmHeadBlMergeStatusCode.LOT_MERGE_RESOURCE_WARNING.getStatusCode()};

		HeadBlCondition latestMergeCondition = new HeadBlCondition();
		latestMergeCondition.setMergeStsCds( stsCds );
		latestMergeCondition.setDelStsId( (StatusFlg)null );
		latestMergeCondition.setLotId( lotId );

		ISqlSort sort = new SortBuilder();
		sort.setElement(HeadBlItems.mergeEndTimestamp, TriSortOrder.Desc, 1);

		IHeadBlEntity entity = ((AmDaoFinder)amSupport).getHeadBlDao().find( latestMergeCondition.getCondition(), sort ).get(0);

		return entity.getHeadBlId();
	}


	private int getSeq( Timestamp ganttStartDate, Timestamp startDate ) {
		int seq = this.getDiffDay( ganttStartDate, startDate );

		if( seq < 1 ) seq = 1;

		return seq;
	}


	private int getCountDays( Timestamp ganttStartDate, Timestamp ganttEndDate, Timestamp endDate, int seq){
		int countDays = this.getDiffDay( ganttStartDate, endDate ) - seq +1;

		if(this.getDiffDay( ganttEndDate, endDate ) > 0)
			countDays = countDays - this.getDiffDay( ganttEndDate, endDate );

		return countDays;
	}


	private boolean isHitSearchStatus( Status searchStatus , Status status ){

		if( Status.All.equals( searchStatus ) )
			return true;

		if( Status.NotClosed.equals( searchStatus ) ){
			return this.isHitSearchStatus( Status.InProgress, status ) ||
				   this.isHitSearchStatus( Status.Completed, status );
		}
		if( searchStatus.equals( status ) )
			return true;

		return false;
	}


	private int getDiffDay( Timestamp dateFrom, Timestamp dateTo ){
		SimpleDateFormat format = TriDateUtils.getYMDDateFormat();

		String stringDateFrom = TriDateUtils.convertViewDateFormat( dateFrom, format );
		String stringDateTo = TriDateUtils.convertViewDateFormat( dateTo, format );

		Timestamp from = null;
		Timestamp to = null;

		from = this.convertStringToTimestampWithDateTime(stringDateFrom, null);
		to   = this.convertStringToTimestampWithDateTime(stringDateTo, null);

		long diff = ( to.getTime() - from.getTime()) / (1000 * 60 * 60 * 24 );
		return (int)diff +1;
	}


	private Timestamp convertStringToTimestampWithDateTime(String DateString, TimeZone timeZone){
		Timestamp date = TriDateUtils.convertStringToTimestamp(
											TriDateUtils.convertDateFormat(DateString, null ), timeZone);

		return date;
	}

	private boolean isEqualDay( Calendar left, Calendar right ){
		boolean equalDay = true;
		equalDay = left.get( Calendar.YEAR ) == right.get( Calendar.YEAR ) && equalDay;
		equalDay = left.get( Calendar.MONTH ) == right.get( Calendar.MONTH ) && equalDay;
		equalDay = left.get( Calendar.DATE ) == right.get( Calendar.DATE ) && equalDay;
		return equalDay;
	}
	
	private final ILotBlEntity getLotBlEntityByMergeEnableAtFirst( String lotId , String headBlId) {
		IHeadBlDto headBlDto = amSupport.findHeadBlDto(headBlId);
		List<IHeadLotBlLnkEntity> headLotBlLink= headBlDto.getHeadLotBlLnkEntities();
		List<String> lotBlIds = new ArrayList<String>();
			for (IHeadLotBlLnkEntity lotHeadLink : headLotBlLink) {
				String lotBlId = lotHeadLink.getLotBlId();
				if ( TriStringUtils.isNotEmpty(lotBlId) ){
					lotBlIds.add(lotBlId);
				}
			}
		return this.getLotBlEntitiesByMergeEnable( lotId, null, TriSortOrder.Desc, lotBlIds  ).get(0);
	}
	
	public List<ILotBlEntity> getLotBlEntitiesByMergeEnable(
			String lotId,
			Timestamp lotCheckInTimestamp,
			TriSortOrder order,
			List<String> lotBlIds) {

		LotBlCondition condition = new LotBlCondition() ;

		condition.setLotId( lotId ) ;
		condition.setStsIds( new String[]{
				AmLotBlStatusId.CommittedToLot.getStatusId(),
				AmLotBlStatusId.Merged.getStatusId()} );

		if( null != lotCheckInTimestamp ) {
			condition.setLotChkinTimestamp( null, lotCheckInTimestamp  ) ;
		}
		
		if ( TriCollectionUtils.isNotEmpty(lotBlIds) ){
			condition.setLotBlIds(lotBlIds.toArray(new String[0]));
		}

		ISqlSort sort = null;

		if ( null != order ) {
			sort = new SortBuilder();
			sort.setElement(LotBlItems.lotChkinTimestamp, order, 0);
		}

		return ((AmDaoFinder)amSupport).getLotBlDao().find( condition.getCondition(), sort );
	}

}
