package jp.co.blueship.tri.fw.um.domainx.lotsettings.beans.dto;

/**
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 */
public class MilestoneEditInputBean {

	private String subject;
	private String startDate;
	private String dueDate;
	private String description;

	public String getSubject() {
		return subject;
	}
	public MilestoneEditInputBean setSubject(String subject) {
		this.subject = subject;
		return this;
	}
	public String getStartDate() {
		return startDate;
	}
	public MilestoneEditInputBean setStartDate(String startDate) {
		this.startDate = startDate;
		return this;
	}
	public String getDueDate() {
		return dueDate;
	}
	public MilestoneEditInputBean setDueDate(String dueDate) {
		this.dueDate = dueDate;
		return this;
	}
	public String getDescription() {
		return description;
	}
	public MilestoneEditInputBean setDescription(String description) {
		this.description = description;
		return this;
	}

}
