package jp.co.blueship.tri.fw.um.domainx.project;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.support.IFwFinderSupport;
import jp.co.blueship.tri.fw.um.UmDesignBusinessRuleUtils;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserCondition;
import jp.co.blueship.tri.fw.um.domainx.project.dto.FlowMemberListServiceBean;
import jp.co.blueship.tri.fw.um.domainx.project.dto.FlowMemberListServiceBean.MemberView;
import jp.co.blueship.tri.fw.um.domainx.project.dto.FlowMemberListServiceBean.SearchCondition;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class FlowMemberListService implements IDomain<FlowMemberListServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IFwFinderSupport fwSupport;
	private IUmFinderSupport support;

	public void setSupport(IFwFinderSupport support) {
		this.fwSupport = support;
		this.support = this.fwSupport.getUmFinderSupport();
	}

	@Override
	public IServiceDto<FlowMemberListServiceBean> execute(IServiceDto<FlowMemberListServiceBean> serviceDto) {
		FlowMemberListServiceBean paramBean = serviceDto.getServiceBean();

		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");

			if (RequestType.init.equals(paramBean.getParam().getRequestType())) {
				this.onChange(paramBean);
			}

			if (RequestType.onChange.equals(paramBean.getParam().getRequestType())) {

				this.onChange(paramBean);
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(UmMessageId.UM005057S, e, paramBean.getFlowAction());
		}
	}

	private void onChange( FlowMemberListServiceBean paramBean ) {
		List<MemberView> memberViews = new ArrayList<MemberView>();

		List<IUserEntity> userSameGroup = this.support.findUsersInSameGroupWithCurrentUser( paramBean.getUserId() );
		String[] users = new String[userSameGroup.size()];
		int pos = 0;
		for (IUserEntity userEntity : userSameGroup) {
			users[pos] = userEntity.getUserId();
			pos++;
		}

		SearchCondition searchCondition = paramBean.getParam().getSearchCondition();
		UserCondition condition = new UserCondition();
		condition.setUserIds(users);
		if (TriStringUtils.isNotEmpty(searchCondition.getKeyword())) {

			condition.setContainsByKeyword(searchCondition.getKeyword());
		}

		List<IUserEntity> userEntities = this.support.getUserDao().find(condition.getCondition());
		SimpleDateFormat formatYMDHM = TriDateUtils.getYMDHMDateFormat( paramBean.getLanguage(), paramBean.getTimeZone() );

		for (IUserEntity userEntity : userEntities) {
			MemberView bean = paramBean.new MemberView()
					.setUserId( userEntity.getUserId())
					.setUserNm(userEntity.getUserNm())
					.setIconPath( UmDesignBusinessRuleUtils.getUserIconSharePath(userEntity) )
					.setLastLogInTime( TriDateUtils.convertViewDateFormat(userEntity.getLastLoginTimestamp(), formatYMDHM) )
					.setCurrentTime( TriDateUtils.convertViewDateFormat(TriDateUtils.getSystemTimestamp(), formatYMDHM ) )
			;
			
			if (userEntity.getUserId().equals(paramBean.getUserId())) {
				memberViews.add(0, bean);
			} else {
				memberViews.add(bean);
			}
			
		}

		paramBean.setMemberViews(memberViews);
	}

}
