package jp.co.blueship.tri.fw.um.domainx.admin;

import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.session.LoginSession;
import jp.co.blueship.tri.fw.sm.dao.login.eb.UserLoginCondition;
import jp.co.blueship.tri.fw.sm.support.ISmFinderSupport;
import jp.co.blueship.tri.fw.support.IFwFinderSupport;
import jp.co.blueship.tri.fw.um.UmItemChkUtils;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserCondition;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserUrlCondition;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowUserRemovalServiceBean;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class FlowUserRemovalService implements IDomain<FlowUserRemovalServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IFwFinderSupport fwSupport;
	private IUmFinderSupport umSupport;
	private ISmFinderSupport smSupport;

	public void setSupport( IFwFinderSupport support ) {
		this.fwSupport = support;
		this.umSupport = this.fwSupport.getUmFinderSupport();
		this.smSupport = this.fwSupport.getSmFinderSupport();
	}

	private LoginSession loginSession;

	public void setLoginSession(LoginSession loginSession) {
		this.loginSession = loginSession;
	}

	@Override
	public IServiceDto<FlowUserRemovalServiceBean> execute(
			IServiceDto<FlowUserRemovalServiceBean> serviceDto) {
		FlowUserRemovalServiceBean paramBean = serviceDto.getServiceBean();
		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");
			String userId = paramBean.getParam().getSelectedUserId();
			PreConditions.assertOf(TriStringUtils.isNotEmpty(userId), "SelectedUserId is not specified");
			paramBean.getResult().setCompleted(false);

			if (RequestType.submitChanges.equals(paramBean.getParam().getRequestType())) {
				UserCondition condition = new UserCondition();
				condition.setUserId(userId);
				if (0 < umSupport.getUserDao().count(condition.getCondition())) {
					this.removeUser(paramBean, userId);
				}
			}
			return serviceDto;
		} catch (Exception e) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(UmMessageId.UM005057S, e, paramBean.getFlowAction());
		}
	}

	private void removeUser(FlowUserRemovalServiceBean paramBean, String userId) {

		validateRemovalUser(userId, paramBean.getUserId());
		// Get userEntity
		IUserEntity userEntity = umSupport.findUserByUserId(userId);

		// Delete um_user
		deleteUser( userId );

		// Delete um_user_role_lnk
		umSupport.cleaningUserRoleLnk(userId, null);

		// Delete um_grp_use_lnk
		umSupport.cleaningGrpUserLnk(null, userId);

		// Delete sm_user_login
		deleteUserLogin(userId);

		// Delete um_user_url
		deleteUserUrl(userId);

		paramBean.getResult().setCompleted(true);
		paramBean.getMessageInfo().addFlashTranslatable( UmMessageId.UM003072I, userEntity.getUserNm() );

	}

	private void validateRemovalUser(String userId, String loggedInUserId) {

		// Can't delete admin user
		UmItemChkUtils.checkUcfAdminUser(userId);

		// Can't delete current login user
		UmItemChkUtils.checkUcfLoginUser(userId, loggedInUserId);

		// Can't delete logged in users
		if (loginSession.isLoggedInUser(userId)) {
			throw new ContinuableBusinessException( UmMessageId.UM001029E);
		}
	}

	private void deleteUser( String userId ) {
		UserCondition condition = new UserCondition();
		condition.setUserId( userId );

		umSupport.getUserDao().delete( condition.getCondition() );
	}

	private void deleteUserLogin( String userId ) {
		UserLoginCondition userLoginCondition = new UserLoginCondition();
		userLoginCondition.setUserId(userId);

		smSupport.getUserLoginDao().delete(userLoginCondition.getCondition());
	}

	private void deleteUserUrl( String userId ) {
		UserUrlCondition userUrlCondition = new UserUrlCondition();
		userUrlCondition.setUserId( userId );

		umSupport.getUserUrlDao().delete(userUrlCondition.getCondition());

	}
}
