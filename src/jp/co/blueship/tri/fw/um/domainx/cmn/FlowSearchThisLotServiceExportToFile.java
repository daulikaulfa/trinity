package jp.co.blueship.tri.fw.um.domainx.cmn;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.dcm.DcmExportToFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.um.constants.DataAttribute;
import jp.co.blueship.tri.fw.um.constants.ExportToFileSubmitOption;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowSearchThisLotServiceExportToFileBean;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowSearchThisSiteServiceBean;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowSearchThisSiteServiceBean.SearchCondition;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowSearchThisSiteServiceBean.SearchServiceType;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowSearchThisSiteServiceBean.SearchThisSiteView;

public class FlowSearchThisLotServiceExportToFile implements IDomain<FlowSearchThisLotServiceExportToFileBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IDomain<IGeneralServiceBean> searchThisSite = null;

	public void setSearchThisSite(IDomain<IGeneralServiceBean> searchThisSite) {
		this.searchThisSite = searchThisSite;
	}

	@Override
	public IServiceDto<FlowSearchThisLotServiceExportToFileBean> execute(
			IServiceDto<FlowSearchThisLotServiceExportToFileBean> serviceDto) {
		FlowSearchThisLotServiceExportToFileBean paramBean = serviceDto.getServiceBean();
		FlowSearchThisSiteServiceBean innerServiceBean = paramBean.getInnerService();

		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");

			if ( paramBean.getParam().getRequestType() == RequestType.onChange
					|| paramBean.getParam().getRequestType() == RequestType.init ) {
				this.onChange(paramBean);
			}

			if ( paramBean.getParam().getRequestType() == RequestType.submitChanges ) {
				this.submitChanges(paramBean);
			}
			return serviceDto;
		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(UmMessageId.UM005057S, e, paramBean.getFlowAction());
		} finally {
			ExceptionUtils.copyToParent(paramBean, innerServiceBean);
		}

	}

	private void submitChanges(FlowSearchThisLotServiceExportToFileBean paramBean) {
		File file = null;
		ExportToFileSubmitOption submitOption = paramBean.getParam().getSubmitOption();

		if (ExportToFileSubmitOption.ExportToExcel.equals(submitOption)) {
			file = DcmExportToFileUtils.createExcel(paramBean, paramBean.getParam().getExportBean());
		} else if (ExportToFileSubmitOption.ExportToCsv.equals(submitOption)) {
			file = DcmExportToFileUtils.createCSV(paramBean, paramBean.getParam().getExportBean());
		}

		paramBean.getFileResponse().setFile(file);
	}

	private void onChange(FlowSearchThisLotServiceExportToFileBean paramBean) {
		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowSearchThisSiteServiceBean serviceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(serviceBean, paramBean);
		dto.setServiceBean(serviceBean);

		{
			serviceBean.getParam().setRequestType(RequestType.init);
			serviceBean.getParam().setSearchServiceType(SearchServiceType.SearchSite);
			searchThisSite.execute(dto);
		}

		serviceBean.getParam().setRequestType(RequestType.onChange);
		this.beforeExecution(paramBean, serviceBean);
		searchThisSite.execute(dto);
		this.afterExecution(serviceBean, paramBean);

	}

	private void beforeExecution(FlowSearchThisLotServiceExportToFileBean src, FlowSearchThisSiteServiceBean dest) {
		SearchCondition srcSearchConditon = src.getParam().getSearchCondition();
		SearchCondition destSearchCondition = dest.getParam().getSearchCondition();
		if ( src.getParam().getRequestType() == RequestType.init ) {
			destSearchCondition.setKeyword(srcSearchConditon.getKeyword());
		} else if ( src.getParam().getRequestType() == RequestType.onChange ) {
			dest.getParam().setRequestType(RequestType.onChange);
			dest.getParam().setSearchServiceType(SearchServiceType.SearchLot);

			destSearchCondition.setLotIds(srcSearchConditon.getLotIds());
			destSearchCondition.setMstoneStartDate(srcSearchConditon.getMstoneStartDate());
			destSearchCondition.setMstoneDueDate(srcSearchConditon.getMstoneDueDate());
			if (TriStringUtils.isNotEmpty(srcSearchConditon.getDataNm())) {
				destSearchCondition.setTableAttribute(DataAttribute.value(srcSearchConditon.getDataNm()));
			} else {
				destSearchCondition.setTableAttribute(DataAttribute.none);
			}
			destSearchCondition.setAssigneeIds(srcSearchConditon.getAssigneeIds());
			destSearchCondition.setGroupIds(srcSearchConditon.getGroupIds());
			destSearchCondition.setStatusIds(srcSearchConditon.getStatusIds());
			destSearchCondition.setCtgIds(srcSearchConditon.getCtgIds());
			destSearchCondition.setMstoneIds(srcSearchConditon.getMstoneIds());
			destSearchCondition.setKeyword(srcSearchConditon.getKeyword());
			destSearchCondition.setAttachedFile(srcSearchConditon.isAttachedFile());
			destSearchCondition.setSelectedPageNo(1);
			dest.getParam().setLinesPerPage(0);
		}
	}

	private void afterExecution(FlowSearchThisSiteServiceBean src, FlowSearchThisLotServiceExportToFileBean dest) {
		List<SearchThisSiteView> srcSearchSiteViews = src.getSearchSiteViews();
		List<SearchThisSiteView> destSearchSiteViews = new ArrayList<SearchThisSiteView>();

		if (RequestType.onChange.equals(src.getParam().getRequestType())) {
			for (int i = 0; i < srcSearchSiteViews.size(); i++) {
				SearchThisSiteView view = srcSearchSiteViews.get(i);
				destSearchSiteViews.add(view);
			}
			dest.setSearchSiteViews(destSearchSiteViews);
		}
	}
}
