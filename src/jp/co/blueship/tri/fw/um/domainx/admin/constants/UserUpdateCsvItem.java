package jp.co.blueship.tri.fw.um.domainx.admin.constants;

import jp.co.blueship.tri.fw.constants.LanguageType;

/**
 *
 * @version V4.03.00
 * @author Cuong Nguyen
 *
 */
public enum UserUpdateCsvItem {
	command("command", "コマンド", 0),
	userId("userId","ユーザID", 1),
	userName("userName","ユーザ名", 2),
	language("language","言語", 3),
	timezone("timezone","タイムゾーン", 4),
	password("password","パスワード", 5),
	passwordExpiration("passwordExpiration","パスワード期限", 6),
	emailAddress("email","メールアドレス", 7),
	deptId("departmentId","部署ID", 8),
	groupIds("groupId","グループID", 9),
	icon("icon","アイコン", 10),
	;

	UserUpdateCsvItem(String enName, String jaName, int index){
		this.enName = enName;
		this.jaName = jaName;
		this.index = index;
	}

	private String enName;
	private String jaName;
	private int index;

	public String getName(String language) {
		if(LanguageType.EN.getValue().equals(language))
			return enName;
		return jaName;
	}
	public int getIndex() {
		return index;
	}

	public static String getHeader(String language){
		String header = "";
		for (UserUpdateCsvItem item : UserUpdateCsvItem.values()) {
				header += item.getName(language) + ",";
		}
		return header.substring(0, header.length() - 1);
	}
}