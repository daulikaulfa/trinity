package jp.co.blueship.tri.fw.um.domainx.project.dto;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.um.domainx.project.beans.dto.ProfileDetailsGanttChartBean;
import jp.co.blueship.tri.fw.um.domainx.project.beans.dto.ProfileDetailsLatestActivityBean;
import jp.co.blueship.tri.fw.um.domainx.project.beans.dto.ProfileDetailsViewBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public class FlowProfileDetailsServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private ProfileDetailsViewBean detailsView = new ProfileDetailsViewBean();
	private ProfileDetailsLatestActivityBean latestActivityView = new ProfileDetailsLatestActivityBean();
	private ProfileDetailsGanttChartBean ganttChartView = new ProfileDetailsGanttChartBean();

	public RequestParam getParam() {
		return param;
	}

	public ProfileDetailsViewBean getDetailsView() {
		return detailsView;
	}
	public FlowProfileDetailsServiceBean setDetailsView(ProfileDetailsViewBean detailsView) {
		this.detailsView = detailsView;
		return this;
	}

	public ProfileDetailsLatestActivityBean getLatestActivityView() {
		return latestActivityView;
	}
	public FlowProfileDetailsServiceBean setLatestActivityView(ProfileDetailsLatestActivityBean latestActivityView) {
		this.latestActivityView = latestActivityView;
		return this;
	}

	public ProfileDetailsGanttChartBean getGanttChartView() {
		return ganttChartView;
	}
	public FlowProfileDetailsServiceBean setGanttChartView(ProfileDetailsGanttChartBean ganttChartView) {
		this.ganttChartView = ganttChartView;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String logInUserId;
		private String userId;
		private RequestOption requestOption = RequestOption.none;

		public String getLogInUserId() {
			return logInUserId;
		}
		public RequestParam setLogInUserId(String logInUserId) {
			this.logInUserId = logInUserId;
			return this;
		}

		public String getSelectedUserId() {
			return userId;
		}
		public RequestParam setSelectedUserId(String userId) {
			this.userId = userId;
			return this;
		}

		public RequestOption getRequestOption() {
			return requestOption;
		}
		public RequestParam setRequestOption(RequestOption requestOption) {
			this.requestOption = requestOption;
			return this;
		}
	}


	public enum RequestOption {
		none			( "" ),
		profile			( "profile" ),
		latestActivity	( "latestActivity" ),
		ganttChart		( "ganttChart" ),
		;

		private String value = null;

		private RequestOption( String value) {
			this.value = value;
		}

		public boolean equals( String value ) {
			RequestOption type = value( value );

			if ( null == type ) return false;
			if ( ! this.equals(type) ) return false;

			return true;
		}

		public String value() {
			return this.value;
		}

		public static RequestOption value( String value ) {
			if ( TriStringUtils.isEmpty( value ) ) {
				return null;
			}

			for ( RequestOption type : values() ) {
				if ( type.value().equals( value ) ) {
					return type;
				}
			}

			return none;
		}
	}
}
