package jp.co.blueship.tri.fw.um.domainx.wiki.dto;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author Sharov.Maksym
 */
public class FlowWikiAttachmentUploadServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();

	public RequestParam getParam() {
		return param;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String wikiId;
		private AppendFileBean appendFile = new AppendFileBean();
		private RequestOption requestOption = RequestOption.none;

		public String getSelectedWikiId() {
			return wikiId;
		}
		public RequestParam setSelectedWikiId(String wikiId) {
			this.wikiId = wikiId;
			return this;
		}
		
		public AppendFileBean getAppendFile() {
			return appendFile;
		}
		public RequestParam setAppendFile(AppendFileBean appendFile) {
			this.appendFile = appendFile;
			return this;
		}
		
		public RequestOption getRequestOption() {
			return requestOption;
		}
		public RequestParam setRequestOption(RequestOption requestOption) {
			this.requestOption = requestOption;
			return this;
		}
	}
	
	public class AppendFileBean {
		private String appendFileNm;
		private String seqNo;
		private byte[] appendFileInputStreamBytes;

		public String getAppendFileNm() {
			return appendFileNm;
		}

		public AppendFileBean setAppendFileNm(String appendFileNm) {
			this.appendFileNm = appendFileNm;
			return this;
		}

		public String getSeqNo() {
			return seqNo;
		}
		public AppendFileBean setSeqNo(String seqNo) {
			this.seqNo = seqNo;
			return this;
		}

		public byte[] getAppendFileInputStreamBytes() {
			return appendFileInputStreamBytes;
		}

		public AppendFileBean setAppendFileInputStreamBytes(byte[] appendFileInputStreamBytes) {
			this.appendFileInputStreamBytes = appendFileInputStreamBytes;
			return this;
		}
	}
	
	/**
	 * This is the class of enumeration types for 'onChange request'.
	 */
	public enum RequestOption {
		none( "" ),
		fileUpload( "fileUpload" ),
		deleteUplodedFile( "deleteFile" ),
		;

		private String value = null;

		private RequestOption( String value) {
			this.value = value;
		}

		public boolean equals( String value ) {
			RequestOption type = value( value );

			if ( null == type ) return false;
			if ( ! this.equals(type) ) return false;

			return true;
		}

		public String value() {
			return this.value;
		}

		public static RequestOption value( String value ) {
			if ( TriStringUtils.isEmpty( value ) ) {
				return null;
			}

			for ( RequestOption type : values() ) {
				if ( type.value().equals( value ) ) {
					return type;
				}
			}

			return none;
		}

	}
}
