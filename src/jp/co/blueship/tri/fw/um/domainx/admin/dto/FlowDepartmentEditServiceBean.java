package jp.co.blueship.tri.fw.um.domainx.admin.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.um.domainx.admin.beans.dto.DepartmentEditInputBean;
import jp.co.blueship.tri.fw.um.domainx.admin.beans.dto.DepartmentUserViewBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class FlowDepartmentEditServiceBean extends DomainServiceBean {
	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private RequestsCompletion result = new RequestsCompletion();
	private List<DepartmentUserViewBean> userViews = new ArrayList<DepartmentUserViewBean>();

	public RequestsCompletion getResult() {
		return result;
	}

	public FlowDepartmentEditServiceBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}
	public RequestParam getParam() {
		return param;
	}

	public List<DepartmentUserViewBean> getUserViews() {
		return userViews;
	}
	public FlowDepartmentEditServiceBean setUserViews(List<DepartmentUserViewBean> userViews) {
		this.userViews = userViews;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {

		private String deptId = null;
		private DepartmentRequestOption requestOption = DepartmentRequestOption.Default;
		private DepartmentEditInputBean inputInfo = new DepartmentEditInputBean();

		public String getSelectedDeptId() {
			return deptId;
		}
		public RequestParam setSelectedDeptId(String deptId) {
			this.deptId = deptId;
			return this;
		}

		public DepartmentRequestOption getRequestOption() {
			return requestOption;
		}
		public RequestParam setRequestOption(DepartmentRequestOption requestOption) {
			this.requestOption = requestOption;
			return this;
		}

		public DepartmentEditInputBean getInputInfo() {
			return inputInfo;
		}
		public RequestParam setInputInfo(DepartmentEditInputBean inputInfo) {
			this.inputInfo = inputInfo;
			return this;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private boolean completed = false;

		public boolean isCompleted() {
			return completed;
		}
		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}

	/**
	 * This is the class of all DepartmentRequest enumeration types.
	 */
	public enum DepartmentRequestOption {
		Default( "default" ),
		ExcludeUser( "excludeUser" );

		private String value = null;

		private DepartmentRequestOption( String value) {
			this.value = value;
		}

		public boolean equals( String value ) {
			DepartmentRequestOption type = value( value );

			if ( null == type ) return false;
			if ( ! this.equals(type) ) return false;

			return true;
		}

		public String value() {
			return this.value;
		}

		public static DepartmentRequestOption value( String value ) {
			if ( TriStringUtils.isEmpty( value ) ) {
				return null;
			}

			for ( DepartmentRequestOption type : values() ) {
				if ( type.value().equals( value ) ) {
					return type;
				}
			}

			return Default;
		}
	}

}
