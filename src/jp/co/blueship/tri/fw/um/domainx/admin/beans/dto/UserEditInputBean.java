package jp.co.blueship.tri.fw.um.domainx.admin.beans.dto;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;
import jp.co.blueship.tri.fw.um.constants.IconSelectionOption;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class UserEditInputBean {
	private String userId;
	private String userNm;
	private String mailAddress;
	private String deptId;
	private List<ItemLabelsBean> deptViews = new ArrayList<ItemLabelsBean>();
	private IconSelectionOption iconOption = IconSelectionOption.DefaultImage;
	private String iconPath;
	private List<ItemLabelsBean> iconViews = new ArrayList<ItemLabelsBean>();
	private boolean receiveMail;
	/**
	 * for upload custom icon
	 */
	private String iconFileNm;
	private byte[] iconInputStreamBytes;

	private String password;
	private String confirmPassword;
	private String passwordExpiration;
	private String originalPasswordExpiration;
	private List<ItemLabelsBean> passwordExpirationViews = new ArrayList<ItemLabelsBean>();

	private Set<String> groupIdSet = new LinkedHashSet<String>();
	private List<GroupView> groupViews = new ArrayList<GroupView>();

	private String language;
	private List<ItemLabelsBean> languageViews = new ArrayList<ItemLabelsBean>();
	private String timeZone;
	private List<ItemLabelsBean> timeZoneViews = new ArrayList<ItemLabelsBean>();

	public String getUserId() {
		return userId;
	}
	public UserEditInputBean setUserId(String userId) {
		this.userId = userId;
		return this;
	}

	public String getUserNm() {
		return userNm;
	}
	public UserEditInputBean setUserNm(String userNm) {
		this.userNm = userNm;
		return this;
	}

	public String getMailAddress() {
		return mailAddress;
	}
	public UserEditInputBean setMailAddress(String mailAddress) {
		this.mailAddress = mailAddress;
		return this;
	}

	public String getDeptId() {
		return deptId;
	}
	public UserEditInputBean setDeptId(String deptId) {
		this.deptId = deptId;
		return this;
	}

	public List<ItemLabelsBean> getDeptViews() {
		return deptViews;
	}
	public UserEditInputBean setDeptViews(List<ItemLabelsBean> deptViews) {
		this.deptViews = deptViews;
		return this;
	}

	public IconSelectionOption getIconOption() {
		return iconOption;
	}
	public UserEditInputBean setIconOption(IconSelectionOption iconOption) {
		this.iconOption = iconOption;
		return this;
	}

	public String getIconPath() {
		return iconPath;
	}
	public UserEditInputBean setIconPath(String iconPath) {
		this.iconPath = iconPath;
		return this;
	}

	public List<ItemLabelsBean> getIconViews() {
		return iconViews;
	}
	public UserEditInputBean setIconViews(List<ItemLabelsBean> iconViews) {
		this.iconViews = iconViews;
		return this;
	}

	public boolean isReceiveMail() {
		return receiveMail;
	}
	public UserEditInputBean setReceiveMail(boolean receiveMail) {
		this.receiveMail = receiveMail;
		return this;
	}

	public String getPassword() {
		return password;
	}
	public UserEditInputBean setPassword(String password) {
		this.password = password;
		return this;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}
	public UserEditInputBean setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
		return this;
	}

	public String getPasswordExpiration() {
		return passwordExpiration;
	}
	public UserEditInputBean setPasswordExpiration(String passwordExpiration) {
		this.passwordExpiration = passwordExpiration;
		return this;
	}

	public List<ItemLabelsBean> getPasswordExpirationViews() {
		return passwordExpirationViews;
	}
	public UserEditInputBean setPasswordExpirationViews(List<ItemLabelsBean> passwordExpirationViews) {
		this.passwordExpirationViews = passwordExpirationViews;
		return this;
	}

	public Set<String> getGroupIdSet() {
		return groupIdSet;
	}
	public UserEditInputBean setGroupIdSet(Set<String> groupSet) {
		this.groupIdSet = groupSet;
		return this;
	}
	public UserEditInputBean setGroupIds(String... groupIds) {
		groupIdSet.clear();
		groupIdSet.addAll( FluentList.from(groupIds).asList() );
		return this;
	}

	public List<GroupView> getGroupViews() {
		return groupViews;
	}
	public UserEditInputBean setGroupViews(List<GroupView> groupViews) {
		this.groupViews = groupViews;
		return this;
	}

	public String getLanguage() {
		return language;
	}
	public UserEditInputBean setLanguage(String language) {
		this.language = language;
		return this;
	}

	public List<ItemLabelsBean> getLanguageViews() {
		return languageViews;
	}
	public UserEditInputBean setLanguageViews(List<ItemLabelsBean> languageViews) {
		this.languageViews = languageViews;
		return this;
	}

	public String getTimeZone() {
		return timeZone;
	}
	public UserEditInputBean setTimeZone(String timeZone) {
		this.timeZone = timeZone;
		return this;
	}

	public List<ItemLabelsBean> getTimeZoneViews() {
		return timeZoneViews;
	}
	public UserEditInputBean setTimeZoneViews(List<ItemLabelsBean> timeZoneViews) {
		this.timeZoneViews = timeZoneViews;
		return this;
	}

	public String getIconFileNm() {
		return iconFileNm;
	}
	public UserEditInputBean setIconFileNm(String iconFileNm) {
		this.iconFileNm = iconFileNm;
		return this;
	}

	public byte[] getIconInputStreamBytes() {
		return iconInputStreamBytes;
	}
	public UserEditInputBean setIconInputStreamBytes(byte[] iconInputStreamBytes) {
		this.iconInputStreamBytes = iconInputStreamBytes;
		return this;
	}

	public String getOriginalPasswordExpiration() {
		return originalPasswordExpiration;
	}

	public void setOriginalPasswordExpiration(String originalPasswordExpiration) {
		this.originalPasswordExpiration = originalPasswordExpiration;
	}

	/**
	 * Group View
	 */
	public class GroupView {
		private String groupId;
		private String groupNm;
		private int count  = 0;

		public String getGroupId() {
			return groupId;
		}
		public GroupView setGroupId(String groupId) {
			this.groupId = groupId;
			return this;
		}

		public String getGroupNm() {
			return groupNm;
		}
		public GroupView setGroupNm(String groupNm) {
			this.groupNm = groupNm;
			return this;
		}

		public int getCount() {
			return count;
		}
		public GroupView setCount(int count) {
			this.count = count;
			return this;
		}
	}

}
