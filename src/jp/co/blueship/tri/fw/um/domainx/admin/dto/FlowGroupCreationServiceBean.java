package jp.co.blueship.tri.fw.um.domainx.admin.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.um.domainx.admin.beans.dto.GroupEditInputBean;
import jp.co.blueship.tri.fw.um.domainx.admin.beans.dto.GroupUserLinkViewBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class FlowGroupCreationServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private RequestsCompletion result = new RequestsCompletion();
	private List<GroupUserLinkViewBean> userViews = new ArrayList<GroupUserLinkViewBean>();

	public RequestParam getParam() {
		return param;
	}

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowGroupCreationServiceBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	public List<GroupUserLinkViewBean> getUserViews() {
		return userViews;
	}
	public FlowGroupCreationServiceBean setUserViews(List<GroupUserLinkViewBean> userViews) {
		this.userViews = userViews;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private GroupEditInputBean inputInfo = new GroupEditInputBean();

		public GroupEditInputBean getInputInfo() {
			return inputInfo;
		}
		public RequestParam setInputInfo(GroupEditInputBean inputInfo) {
			this.inputInfo = inputInfo;
			return this;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private String groupId;
		private boolean completed = false;

		public String getGroupId() {
			return groupId;
		}
		public RequestsCompletion setGroupId(String groupId) {
			this.groupId = groupId;
			return this;
		}

		public boolean isCompleted() {
			return completed;
		}
		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}
}
