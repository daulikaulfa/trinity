package jp.co.blueship.tri.fw.um.domainx.cmn;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotCondition;
import jp.co.blueship.tri.am.support.IAmFinderSupport;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.ProcessStatus;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.status.SmProcMgtStatusId;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.MessageParameter;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.sm.SmProgressNotificationUtils;
import jp.co.blueship.tri.fw.sm.dao.procmgt.constants.ProcMgtItems;
import jp.co.blueship.tri.fw.sm.dao.procmgt.eb.IProcMgtEntity;
import jp.co.blueship.tri.fw.sm.dao.procmgt.eb.ProcMgtCondition;
import jp.co.blueship.tri.fw.sm.support.ISmFinderSupport;
import jp.co.blueship.tri.fw.svc.beans.dto.IProgressBar;
import jp.co.blueship.tri.fw.svc.beans.dto.ProgressBar;
import jp.co.blueship.tri.fw.um.dao.user.constants.UserProcNoticeItems;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserProcNoticeEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserProcNoticeCondition;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserProcNoticeEntity;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowProgressNotificationServiceBean;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowProgressNotificationServiceBean.Notification;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowProgressNotificationServiceBean.RequestOption;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

public class FlowProgressNotificationService implements IDomain<FlowProgressNotificationServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private IUmFinderSupport umSupport;
	private ISmFinderSupport smSupport;
	private IAmFinderSupport amSupport;

	public void setAmSupport(IAmFinderSupport amSupport) {
		this.amSupport = amSupport;
		umSupport = amSupport.getUmFinderSupport();
		smSupport = amSupport.getSmFinderSupport();
	}


	@Override
	public IServiceDto<FlowProgressNotificationServiceBean> execute(
			IServiceDto<FlowProgressNotificationServiceBean> serviceDto) {

		FlowProgressNotificationServiceBean paramBean = serviceDto.getServiceBean();

		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");

			if ( RequestType.init.equals(paramBean.getParam().getRequestType()) )
				this.init(paramBean);

			if( RequestType.onChange.equals(paramBean.getParam().getRequestType()) )
				this.onChange(paramBean);

			return serviceDto;
		} catch (Exception e) {

			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(UmMessageId.UM005057S, e, paramBean.getFlowAction());

		}
	}


	private void init(FlowProgressNotificationServiceBean paramBean){
		this.onChange(paramBean);
	}

	private void onChange(FlowProgressNotificationServiceBean paramBean){

		RequestOption requestOption = paramBean.getParam().getRequestOption();

		if(requestOption.equals(RequestOption.count))
			this.setCountOfUnread(paramBean);

		else if(requestOption.equals(RequestOption.views))
			this.setNotificationView(paramBean);

		else if(requestOption.equals(RequestOption.refreshViews))
			this.refreshNotificationViews(paramBean);
	}


	private void setCountOfUnread(FlowProgressNotificationServiceBean paramBean){
 		int countOfUnread =0;

		UserProcNoticeCondition condition = new UserProcNoticeCondition();
		condition.setUserId(paramBean.getUserId());
		condition.setIsRead(StatusFlg.off);

		countOfUnread= this.umSupport.getUserProcNoticeDao().count(condition.getCondition() );

		paramBean.setCountOfUnread( countOfUnread );
	}


	private void setNotificationView(FlowProgressNotificationServiceBean paramBean){

		ProcMgtCondition procMgtCondition = new ProcMgtCondition();
		procMgtCondition.setProcIds( this.getNoticeProcIds( paramBean.getUserId(), paramBean.getParam().getLinesPerPage() ) );
		procMgtCondition.setDelStsId( (StatusFlg)null );

		ISqlSort sort = new SortBuilder();
		sort.setElement(ProcMgtItems.updTimestamp, TriSortOrder.Desc, 1);

		List<IProcMgtEntity> procMgtEntityList = this.smSupport.getProcMgtDao().find(procMgtCondition.getCondition() , sort);

		int countOfRead = 0;
		List<Notification> views = new ArrayList<Notification>();

		for(IProcMgtEntity procMgtEntity : procMgtEntityList){

			UserProcNoticeCondition noticCondition = new UserProcNoticeCondition();
			noticCondition.setProcId(procMgtEntity.getProcId());
			noticCondition.setUserId(paramBean.getUserId());

			IUserProcNoticeEntity procNoticeEntity =
					this.umSupport.getUserProcNoticeDao().findByPrimaryKey(noticCondition.getCondition());

			LotCondition lotCondition = new LotCondition();
			lotCondition.setLotId( procMgtEntity.getLotId() );

			if( this.amSupport.getLotDao().findByPrimaryKey( lotCondition.getCondition() ) == null ){
				continue;
			}

			if(NoticeService.none.equals( NoticeService.value(procMgtEntity.getSvcId())) ) {
				continue;
			}

			if (procNoticeEntity == null) {
				continue;
			}

			boolean isRead = procNoticeEntity.getIsRead().parseBoolean();
			if ( isRead ) {
				if( countOfRead < paramBean.getParam().getLinesPerPage() ) {
					countOfRead++;
				} else {
					continue;
				}
			}

			Notification view = this.getNotificationView(paramBean, procNoticeEntity , procMgtEntity);
			views.add(view);
		}

		IUserProcNoticeEntity procNoticeEntity = new UserProcNoticeEntity();
		procNoticeEntity.setUserId(paramBean.getUserId());
		procNoticeEntity.setIsRead(StatusFlg.on);

		this.umSupport.getUserProcNoticeDao().update(procNoticeEntity);

		paramBean.setNotificationViews(views);
	}


	private void refreshNotificationViews( FlowProgressNotificationServiceBean paramBean){

		List<Notification> views = new ArrayList<Notification>();

		for(Notification notification : paramBean.getNotificationViews() ){

			String procId = notification.getProcId();

			UserProcNoticeCondition noticCondition = new UserProcNoticeCondition();
			noticCondition.setProcId( procId );
			noticCondition.setUserId( paramBean.getUserId() );

			IUserProcNoticeEntity procNoticeEntity =
					this.umSupport.getUserProcNoticeDao().findByPrimaryKey(noticCondition.getCondition());

			ProcMgtCondition procMgtCondition = new ProcMgtCondition();
			procMgtCondition.setProcId( procId );
			procMgtCondition.setDelStsId( (StatusFlg)null );

			IProcMgtEntity procMgtEntity = this.smSupport.getProcMgtDao().findByPrimaryKey(procMgtCondition.getCondition());

			if(procNoticeEntity == null || procMgtEntity == null) {
				continue;
			}

			if(NoticeService.none.equals( NoticeService.value(procMgtEntity.getSvcId())) ) {
				continue;
			}

			Notification view = this.getNotificationView(paramBean, procNoticeEntity , procMgtEntity);
			views.add(view);

			procNoticeEntity.setIsRead(StatusFlg.on);
			this.umSupport.getUserProcNoticeDao().update(procNoticeEntity);
		}

		paramBean.setNotificationViews(views);
	}


	private Notification getNotificationView( FlowProgressNotificationServiceBean paramBean ,
											  IUserProcNoticeEntity procNoticeEntity ,
											  IProcMgtEntity procMgtEntity ){

		String lotId = procMgtEntity.getLotId();
		ILotEntity lotEntity = this.amSupport.findLotEntity(lotId);

		String procId = procMgtEntity.getProcId();
		String serviceId = procMgtEntity.getSvcId();
		String stsId = procMgtEntity.getStsId();
		String language = paramBean.getLanguage();

		NoticeService noticeService = NoticeService.value(serviceId);
		if(noticeService == null) noticeService = NoticeService.none;

		String infoMessage = this.createInfoMessage( language , noticeService , stsId );
		String subject = this.createSubject( lotEntity.getLotNm() );

		long predictionProcessTime = (SmProcMgtStatusId.Processing.equals(stsId))?
										this.getPredictionProcessTime(noticeService) : 0;
		long elapsedTime = TriDateUtils.getSystemTimestamp().getTime() - procMgtEntity.getProcStTimestamp().getTime();

		if(predictionProcessTime <= elapsedTime) predictionProcessTime = elapsedTime + 1000;

		IProgressBar progressBar = (SmProcMgtStatusId.Processing.equals(stsId))?
										this.getProgressBar((int)(predictionProcessTime/1000),(int)(elapsedTime/1000)):
										this.getProgressBar(1, 1);

		String resultMessage = this.createResultMessage( language ,  stsId , progressBar);
		String remainingTime = SmProgressNotificationUtils.stringOf( language , predictionProcessTime - elapsedTime);

		Notification view = paramBean.new Notification()
				.setProcId			( procId )
				.setInfoMessage		( infoMessage )
				.setSubject			( subject )
				.setResultMessage	( resultMessage )
				.setResult			( this.getProcessStatus( stsId ) )
				.setBar				( progressBar )
				.setRemainingTime	( remainingTime )
				.setStartDate		( TriDateUtils.convertViewDateFormat(
										procMgtEntity.getProcStTimestamp(),
										TriDateUtils.getYMDDateFormat(paramBean.getLanguage(), paramBean.getTimeZone())
										).toString() )
				.setEndDate			( TriDateUtils.convertViewDateFormat(
										procMgtEntity.getProcEndTimestamp(),
										TriDateUtils.getYMDDateFormat(paramBean.getLanguage(), paramBean.getTimeZone())
										).toString())

				.setServiceId		( noticeService.getLinkServiceId() )
				.setLotId			( lotId )
				;

		return view;
	}


	private String[] getNoticeProcIds(String userId, int linesPerPage){

		List<IUserProcNoticeEntity> entityList = this.umSupport.findUserProcNoticeByUserId(userId);

		List<String> procIds = new ArrayList<String>();

		for(IUserProcNoticeEntity entity : entityList) {
			procIds.add(entity.getProcId());
		}

		return procIds.toArray(new String[0]);
	}


	private String createInfoMessage(String language , NoticeService service , String stsId){

		IContextAdapter contextAdapter = ContextAdapterFactory.getContextAdapter();


		if(service == null) return null;

		switch(service){
		case lotCreate:
			if(SmProcMgtStatusId.Success.equals(stsId))
				return contextAdapter.getMessage(language , false , UmMessageId.UM003018I);

			else if(SmProcMgtStatusId.Processing.equals(stsId))
				return contextAdapter.getMessage(language , false , UmMessageId.UM003016I);

			else if(SmProcMgtStatusId.Warning.equals(stsId))
				return contextAdapter.getMessage(language , false , UmMessageId.UM003018I);

			else if(SmProcMgtStatusId.Error.equals(stsId))
				return contextAdapter.getMessage(language , false , UmMessageId.UM003017I);
			break;

		case createBuildPackage:
			if(SmProcMgtStatusId.Success.equals(stsId))
				return contextAdapter.getMessage(language , false , UmMessageId.UM003021I);

			else if(SmProcMgtStatusId.Processing.equals(stsId))
				return contextAdapter.getMessage(language , false , UmMessageId.UM003019I);

			else if(SmProcMgtStatusId.Warning.equals(stsId))
				return contextAdapter.getMessage(language , false , UmMessageId.UM003021I);

			else if(SmProcMgtStatusId.Error.equals(stsId))
				return contextAdapter.getMessage(language , false , UmMessageId.UM003020I);
			break;

		case closeBuildPackage:
			if(SmProcMgtStatusId.Success.equals(stsId))
				return contextAdapter.getMessage(language , false , UmMessageId.UM003024I);

			else if(SmProcMgtStatusId.Processing.equals(stsId))
				return contextAdapter.getMessage(language , false , UmMessageId.UM003022I);

			else if(SmProcMgtStatusId.Warning.equals(stsId))
				return contextAdapter.getMessage(language , false , UmMessageId.UM003024I);

			else if(SmProcMgtStatusId.Error.equals(stsId))
				return contextAdapter.getMessage(language , false , UmMessageId.UM003023I);
			break;

		case createReleasePackage:
			if(SmProcMgtStatusId.Success.equals(stsId))
				return contextAdapter.getMessage(language , false , UmMessageId.UM003027I);

			else if(SmProcMgtStatusId.Processing.equals(stsId))
				return contextAdapter.getMessage(language , false , UmMessageId.UM003025I);

			else if(SmProcMgtStatusId.Warning.equals(stsId))
				return contextAdapter.getMessage(language , false , UmMessageId.UM003027I);

			else if(SmProcMgtStatusId.Error.equals(stsId))
				return contextAdapter.getMessage(language , false , UmMessageId.UM003026I);
			break;

		case conflictCheck:
			if(SmProcMgtStatusId.Success.equals(stsId))
				return contextAdapter.getMessage(language , false , UmMessageId.UM003030I);

			else if(SmProcMgtStatusId.Processing.equals(stsId))
				return contextAdapter.getMessage(language , false , UmMessageId.UM003028I);

			else if(SmProcMgtStatusId.Warning.equals(stsId))
				return contextAdapter.getMessage(language , false , UmMessageId.UM003030I);

			else if(SmProcMgtStatusId.Error.equals(stsId))
				return contextAdapter.getMessage(language , false , UmMessageId.UM003029I);
			break;

		case merge:
			if(SmProcMgtStatusId.Success.equals(stsId))
				return contextAdapter.getMessage(language , false , UmMessageId.UM003034I);

			else if(SmProcMgtStatusId.Processing.equals(stsId))
				return contextAdapter.getMessage(language , false , UmMessageId.UM003032I);

			else if(SmProcMgtStatusId.Warning.equals(stsId))
				return contextAdapter.getMessage(language , false , UmMessageId.UM003034I);

			else if(SmProcMgtStatusId.Error.equals(stsId))
				return contextAdapter.getMessage(language , false , UmMessageId.UM003033I);
			break;

		default:
			break;
		}
		return null;
	}


	private String createSubject(String lotNm){
		return lotNm;
	}


	private String createResultMessage(String language , String stsId , IProgressBar progressBar){

		IContextAdapter contextAdapter = ContextAdapterFactory.getContextAdapter();
		String resultMessage = "";

		if(SmProcMgtStatusId.Processing.equals(stsId)){
			String percentComplete = (int)(progressBar.getPercentComplete() * 100) + "";
			resultMessage = contextAdapter.getValue(MessageParameter.ProgressNotificationResultMessage1.getKey() , new String[]{percentComplete}, language );

		}else if(SmProcMgtStatusId.Success.equals(stsId))
			resultMessage = contextAdapter.getValue(MessageParameter.ProgressNotificationResultMessage2.getKey(), (String[])null, language);

		else if(SmProcMgtStatusId.Warning.equals(stsId))
			resultMessage = contextAdapter.getValue(MessageParameter.ProgressNotificationResultMessage3.getKey(), (String[])null, language);

		else if(SmProcMgtStatusId.Error.equals(stsId))
			resultMessage = contextAdapter.getValue(MessageParameter.ProgressNotificationResultMessage4.getKey(), (String[])null, language);

		return resultMessage;
	}


	private ProcessStatus getProcessStatus(String stsId){

		ProcessStatus result = ProcessStatus.none;

		if(SmProcMgtStatusId.Success.equals(stsId))
			result = ProcessStatus.Completed;

		else if(SmProcMgtStatusId.Processing.equals(stsId))
			result = ProcessStatus.Active;

		else if(SmProcMgtStatusId.Warning.equals(stsId))
			result = ProcessStatus.Warning;

		else if(SmProcMgtStatusId.Error.equals(stsId))
			result = ProcessStatus.Error;

		return result;
	}


	private IProgressBar getProgressBar(int maximum , int minimum){
		IProgressBar progressBar = new ProgressBar()
				.setMaximum(maximum)
				.setMinimum(minimum)
				;

		progressBar.setValue((int)(progressBar.getPercentComplete()*100));

		return progressBar;
	}


	private long getPredictionProcessTime(NoticeService service){

		ProcMgtCondition condition = new ProcMgtCondition();
		List<String> serviceIdList = new ArrayList<>();
		if ( TriStringUtils.isNotEmpty(service.value()) ) {
			serviceIdList.add(service.value());
		}
		if ( TriStringUtils.isNotEmpty(service.valueOfV3()) ) {
			serviceIdList.add(service.valueOfV3());
		}
		
		String[] serviceIds = serviceIdList.toArray(new String[0]);
		condition.setServiceIds(serviceIds);
		condition.setStsId(SmProcMgtStatusId.Success.getStatusId());
		condition.setDelStsId( (StatusFlg)null );

		ISqlSort sort = new SortBuilder();
		sort.setElement(UserProcNoticeItems.updTimestamp, TriSortOrder.Desc, 1);

		IEntityLimit<IProcMgtEntity> entityLimit = this.smSupport.getProcMgtDao().find(condition.getCondition() , sort , 1 , 5);

		List<IProcMgtEntity> entityList = entityLimit.getEntities();

		if(entityList.size() <= 0) return 0;

		long sumTime =0;
		for(IProcMgtEntity entity : entityList)
			sumTime += entity.getProcEndTimestamp().getTime() - entity.getProcStTimestamp().getTime();

		return sumTime / entityList.size();
	}



	/**
	 * 通知するサービスの列挙型です<br>
	 * getLinkServiceId()でその処理の状況紹介画面又はリスト画面のServiceIDを取得できます
	 */
	private enum NoticeService{
		none ("","",""),
		lotCreate			( ServiceId.AmLotCreationService.value(),
							  ServiceId.AmLotCreationService.valueOfV3(),
							  ServiceId.UmDashBoardService.value()),

		createBuildPackage	( ServiceId.BmBuildPackageCreationService.value() ,
							  ServiceId.BmBuildPackageCreationService.valueOfV3(),
							  ServiceId.BmBuildPackageCreationProcessingStatusService.value()),

		closeBuildPackage	( ServiceId.BmBuildPackageCloseService.value() ,
							  ServiceId.BmBuildPackageCloseService.valueOfV3(),
							  ServiceId.BmBuildPackageListService.value()),

		createReleasePackage( ServiceId.RmReleasePackageCreationService.value() ,
							  ServiceId.RmReleasePackageCreationService.valueOfV3(),
							  ServiceId.RmReleasePackageCreationProcessingStatusService.value()),

		conflictCheck		( ServiceId.AmConflictCheckService.value(),
							  ServiceId.AmConflictCheckService.valueOfV3(),
							  ServiceId.AmMergeService.value()),

		merge				( ServiceId.AmMergeService.value(),
							  ServiceId.AmMergeService.valueOfV3(),
							  ServiceId.AmMergeService.value()),
		;

		private String value = null;
		private String valueOfV3 = null;
		private String linkServiceId = null;

		private NoticeService(String serviceId ,  String v3ServiceId , String linkServiceId){
			this.value = serviceId;
			this.valueOfV3 = v3ServiceId;
			this.linkServiceId = linkServiceId;
		}

		private String value(){
			return this.value;
		}
		private String valueOfV3(){
			return this.valueOfV3;
		}
		private String getLinkServiceId(){
			return this.linkServiceId;
		}

		private static NoticeService value( String serviceId){

			if( TriStringUtils.isEmpty( serviceId ) ) return null;

			for ( NoticeService value : values() ) {
				if ( value.value().equals( serviceId ) ||
					 ((value.valueOfV3() == null)? false : value.valueOfV3().equals( serviceId )))
					return value;
			}

			return none;
		}
	}
}
