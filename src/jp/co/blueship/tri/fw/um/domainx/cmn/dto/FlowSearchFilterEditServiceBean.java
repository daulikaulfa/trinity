package jp.co.blueship.tri.fw.um.domainx.cmn.dto;

import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class FlowSearchFilterEditServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private RequestsCompletion result = new RequestsCompletion();

	public RequestsCompletion getResult() {
		return result;
	}

	public FlowSearchFilterEditServiceBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}
	@Override
	public RequestParam getParam() {
		return param;
	}

	/**
	 *
	 * Request Param
	 *
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String filterId = null;
		private SearchFilterEditInput inputInfo = new SearchFilterEditInput();

		public String getFilterId() {
			return filterId;
		}
		public RequestParam setFilterId(String filterId) {
			this.filterId = filterId;
			return this;
		}

		public SearchFilterEditInput getInputInfo() {
			return inputInfo;
		}
		public RequestParam setInputInfo(SearchFilterEditInput inputInfo) {
			this.inputInfo = inputInfo;
			return this;
		}

	}

	/**
	 * Input Information
	 */
	public class SearchFilterEditInput {
		private String filterNm = null;

		public String getFilterNm() {
			return filterNm;
		}
		public SearchFilterEditInput setFilterNm(String filterNm) {
			this.filterNm = filterNm;
			return this;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private boolean completed = false;

		public boolean isCompleted() {
			return completed;
		}
		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}
}
