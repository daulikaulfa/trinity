package jp.co.blueship.tri.fw.um.domainx.admin.beans.dto;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class DepartmentEditInputBean {

	private String deptNm;
	private String excludeUserId;
	private Set<String> userIdSet = new LinkedHashSet<String>();
	private List<DepartmentUserViewBean> userViews = new ArrayList<DepartmentUserViewBean>();

	public String getDeptNm() {
		return deptNm;
	}
	public DepartmentEditInputBean setDeptNm(String deptNm) {
		this.deptNm = deptNm;
		return this;
	}

	public Set<String> getUserIdSet() {
		return userIdSet;
	}
	public DepartmentEditInputBean setUserIdSet(Set<String> userIdSet) {
		this.userIdSet = userIdSet;
		return this;
	}
	public DepartmentEditInputBean setUserIds(String... userIds) {
		userIdSet.clear();
		userIdSet.addAll( FluentList.from(userIds).asList() );
		return this;
	}

	public String getExcludeUserId() {
		return excludeUserId;
	}
	public DepartmentEditInputBean setExcludeUserId(String excludeUserId) {
		this.excludeUserId = excludeUserId;
		return this;
	}

	public List<DepartmentUserViewBean> getUserViews() {
		return userViews;
	}
	public DepartmentEditInputBean setUserViews(List<DepartmentUserViewBean> userViews) {
		this.userViews = userViews;
		return this;
	}

}
