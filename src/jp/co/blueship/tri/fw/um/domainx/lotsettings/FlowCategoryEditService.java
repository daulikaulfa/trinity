package jp.co.blueship.tri.fw.um.domainx.lotsettings;

import jp.co.blueship.tri.fw.cmn.utils.DesignSheetUtils;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.status.UmActStatusId;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.support.IFwFinderSupport;
import jp.co.blueship.tri.fw.um.UmItemChkUtils;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.CtgCondition;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.CtgEntity;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.ICtgEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.HistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;
import jp.co.blueship.tri.fw.um.domainx.lotsettings.beans.dto.CategoryEditInputBean;
import jp.co.blueship.tri.fw.um.domainx.lotsettings.dto.FlowCategoryEditServiceBean;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class FlowCategoryEditService implements IDomain<FlowCategoryEditServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IFwFinderSupport fwSupport;
	private IUmFinderSupport support;

	public void setSupport( IFwFinderSupport support ) {
		this.fwSupport = support;
		this.support = this.fwSupport.getUmFinderSupport();
	}

	@Override
	public IServiceDto<FlowCategoryEditServiceBean> execute(
			IServiceDto<FlowCategoryEditServiceBean> serviceDto) {

		FlowCategoryEditServiceBean paramBean = null;

		try {
			paramBean = serviceDto.getServiceBean();
			String ctgId = paramBean.getParam().getSelectedCategoryId();
			String lotId = paramBean.getParam().getSelectedLotId();
			paramBean.getResult().setCompleted(false);

			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");
			PreConditions.assertOf(TriStringUtils.isNotEmpty(ctgId), "SelectedCategoryId is not specified");
			PreConditions.assertOf(TriStringUtils.isNotEmpty(lotId), "SelectedLotId is not specified");

			if (RequestType.init.equals(paramBean.getParam().getRequestType())) {
				ICtgEntity ctgEntity = support.findCtgByPrimaryKey(ctgId);

				CategoryEditInputBean inputInfo = new CategoryEditInputBean();
				inputInfo.setSubject(ctgEntity.getCtgNm());
				paramBean.getParam().setInputInfo(inputInfo);
			}

			if (RequestType.submitChanges.equals(paramBean.getParam().getRequestType())) {
				CategoryEditInputBean inputInfo = paramBean.getParam().getInputInfo();
				int count = this.countCategoryByName(lotId, ctgId, inputInfo.getSubject());

				UmItemChkUtils.checkInputCategory(inputInfo, count);

				updateCategory(inputInfo, ctgId , lotId);
				paramBean.getResult().setCompleted(true);
				paramBean.getMessageInfo().addFlashTranslatable(UmMessageId.UM003062I,inputInfo.getSubject());
			}


			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(UmMessageId.UM005057S, e, paramBean.getFlowAction());
		}
	}

	private void updateCategory(CategoryEditInputBean bean, String ctgId , String lotId) {
		ICtgEntity ctgEntity = new CtgEntity();
		ctgEntity.setCtgId(ctgId);
		ctgEntity.setCtgNm(bean.getSubject());
		support.getCtgDao().update(ctgEntity);

		if ( DesignSheetUtils.isRecord() ) {
			ctgEntity.setLotId(lotId);
			IHistEntity histEntity = new HistEntity();
			histEntity.setActSts(UmActStatusId.Edit.getStatusId());
			support.getCtgHistDao().insert( histEntity , ctgEntity);
		}
	}

	private int countCategoryByName(String lotId, String ctgIdByNotEquals, String ctgSubject) {
		if ( TriStringUtils.isEmpty(ctgSubject) ) {
			return 0;
		}

		CtgCondition condition = new CtgCondition();
		condition.setLotId(lotId);
		condition.setCtgIdsByNotEquals(ctgIdByNotEquals);
		condition.setCtgNm(ctgSubject);

		return support.getCtgDao().count(condition.getCondition());
	}

}
