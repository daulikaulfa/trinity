package jp.co.blueship.tri.fw.um.domainx.wiki.beans;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.um.dao.wiki.eb.IViewWikiTagsEntity;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.IWikiEntity;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.ViewWikiTagsCondition;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.WikiCondition;
import jp.co.blueship.tri.fw.um.domainx.wiki.beans.dto.WikiTagViewBean;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 *
 * @author Akahoshi
 *
 */
public class WikiTagListService {

	private IUmFinderSupport umSupport = null;

	public void setUmSupport(IUmFinderSupport umSupport) {
		this.umSupport = umSupport;
	}

	/**
	 * Get wikiTagView List by wikId.<br>
	 * Return null if wiki does not exist.
	 * <br>
	 * wikiIdを引数にタグリストを取得します.<br>
	 * 指定されたWikiIdのWikiが存在しない場合はnullを返します。<br>
	 *
	 * @param wikiId
	 * @return WikiTagViewList
	 */
	public List<WikiTagViewBean> getWikiTagViewListByWikiId(String wikiId){

		WikiCondition condition = new WikiCondition();
		condition.setWikiId(wikiId);

		IWikiEntity entity = this.umSupport.getWikiDao().findByPrimaryKey(condition.getCondition());

		if( entity == null) return null;

		return this.getWikiTagViewList( entity.getLotId() );
	}

	/**
	 * Get wikiTagView List.<br>
	 * <br>
	 * タグリストを取得します.<br>
	 *
	 * @param lotId
	 * @return wikiTagViewBeanList
	 */
	public List<WikiTagViewBean> getWikiTagViewList(String lotId){

		ViewWikiTagsCondition condition = new ViewWikiTagsCondition();
		condition.setLotId( lotId );

		List<IViewWikiTagsEntity> entityList = this.umSupport.getViewWikiTagsDao().find(condition);
		List<WikiTagViewBean> tagViewList = new ArrayList<WikiTagViewBean>();

		for(IViewWikiTagsEntity entity : entityList){

			WikiTagViewBean view = new WikiTagViewBean()
					.setTagNm		( entity.getTagNm() )
					.setQuantity	( entity.count() )
					;
			tagViewList.add( view );
		}
		return tagViewList;
	}


}
