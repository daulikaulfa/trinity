package jp.co.blueship.tri.fw.um.domainx.admin.beans.dto;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class GroupUserLinkViewBean {
	private String userId = null;
	private String userNm = null;
	private String iconPath = null;
	private String mailAddress = null;

	public String getUserId() {
		return userId;
	}
	public GroupUserLinkViewBean setUserId(String userId) {
		this.userId = userId;
		return this;
	}
	public String getUserNm() {
		return userNm;
	}
	public GroupUserLinkViewBean setUserNm(String userNm) {
		this.userNm = userNm;
		return this;
	}
	public String getIconPath() {
		return iconPath;
	}
	public GroupUserLinkViewBean setIconPath(String iconPath) {
		this.iconPath = iconPath;
		return this;
	}
	public String getMailAddress() {
		return mailAddress;
	}
	public GroupUserLinkViewBean setMailAddress(String mailAddress) {
		this.mailAddress = mailAddress;
		return this;
	}

}
