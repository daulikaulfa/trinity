package jp.co.blueship.tri.fw.um.domainx.admin.dto;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;
import jp.co.blueship.tri.fw.um.constants.IconSelectionOption;
import jp.co.blueship.tri.fw.um.constants.LookAndFeel;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 * @version V4.02.00
 * @author Akahoshi
 *
 */
public class FlowProjectSettingsEditServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private RequestsCompletion result = new RequestsCompletion();

	public RequestParam getParam() {
		return param;
	}

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowProjectSettingsEditServiceBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private Set<String> requestOptions = new HashSet<String>();
		private ProjectSettingEditInputBean projectInputInfo = new ProjectSettingEditInputBean();
		private LanguageAndTimezoneEditInputBean languageTimezoneInputInfo = new LanguageAndTimezoneEditInputBean();
		private LookAndFeelEditInputBean lookAndFeelEditInputInfo = new LookAndFeelEditInputBean();
		private NotificationEditInputBean notificationInputInfo = new NotificationEditInputBean();

		public boolean isRequestOptionOf( RequestOption option ) {
			if ( null == option )
				return false;

			return requestOptions.contains(option.value);
		}
		public RequestParam setRequestOptions(RequestOption... requestOptions) {
			if ( null == requestOptions )
				return this;

			this.requestOptions = new HashSet<String>();
			for ( RequestOption option: requestOptions ) {
				this.requestOptions.add( option.value );
			}

			return this;
		}

		public ProjectSettingEditInputBean getProjectInputInfo() {
			return projectInputInfo;
		}
		public RequestParam setProjectInputInfo(ProjectSettingEditInputBean projectInputInfo) {
			this.projectInputInfo = projectInputInfo;
			return this;
		}

		public LookAndFeelEditInputBean getLookAndFeelEditInputInfo() {
			return lookAndFeelEditInputInfo;
		}
		public RequestParam setLookAndFeelEditInputInfo(LookAndFeelEditInputBean lookAndFeelEditInputInfo) {
			this.lookAndFeelEditInputInfo = lookAndFeelEditInputInfo;
			return this;
		}

		public LanguageAndTimezoneEditInputBean getLanguageTimezoneInputInfo() {
			return languageTimezoneInputInfo;
		}
		public RequestParam setLanguageTimezoneInputInfo(LanguageAndTimezoneEditInputBean languageTimezoneInputInfo) {
			this.languageTimezoneInputInfo = languageTimezoneInputInfo;
			return this;
		}

		public NotificationEditInputBean getNotificationInputInfo() {
			return notificationInputInfo;
		}
		public RequestParam setNotificationInputInfo(NotificationEditInputBean notificationInputInfo) {
			this.notificationInputInfo = notificationInputInfo;
			return this;
		}
	}

	/**
	 * This is the class of all ProjectSettings enumeration types.
	 */
	public enum RequestOption {
		ProjectChanges( "projectChanges" ),
		LanguageAndTimezoneChanges( "languageAndTimezoneChanges" ),
		LookAndFeelChanges( "lookAndFeelChanges" ),
		NotificationChanges( "notificationChanges" ),
		;

		private String value = null;

		private RequestOption( String value) {
			this.value = value;
		}

		public String value() {
			return this.value;
		}
	}

	/**
	 * This is the project information to be used in the case of RequestOption = {@link RequestOption#ProjectChanges}
	 */
	public class ProjectSettingEditInputBean {
		private String projectNm;
		private List<ItemLabelsBean> iconViews = new ArrayList<ItemLabelsBean>();
		private IconSelectionOption iconOption = IconSelectionOption.DefaultImage;
		/**
		 * for default
		 */
		private String iconPath;
		/**
		 * for custom
		 */
		private String iconFilePath;
		private String iconFileNm;
		private byte[] iconInputStreamBytes;

		public String getProjectNm() {
			return projectNm;
		}
		public ProjectSettingEditInputBean setProjectNm(String projectNm) {
			this.projectNm = projectNm;
			return this;
		}
		public List<ItemLabelsBean> getIconViews() {
			return iconViews;
		}
		public ProjectSettingEditInputBean setIconViews(List<ItemLabelsBean> iconViews) {
			this.iconViews = iconViews;
			return this;
		}
		public IconSelectionOption getIconOption() {
			return iconOption;
		}
		public ProjectSettingEditInputBean setIconOption(IconSelectionOption iconOption) {
			this.iconOption = iconOption;
			return this;
		}
		public String getIconPath() {
			return iconPath;
		}
		public ProjectSettingEditInputBean setIconPath(String iconPath) {
			this.iconPath = iconPath;
			return this;
		}
		public String getIconFilePath() {
			return iconFilePath;
		}
		public ProjectSettingEditInputBean setIconFilePath(String iconFilePath) {
			this.iconFilePath = iconFilePath;
			return this;
		}
		public String getIconFileNm() {
			return iconFileNm;
		}
		public ProjectSettingEditInputBean setIconFileNm(String iconFileNm) {
			this.iconFileNm = iconFileNm;
			return this;
		}
		public byte[] getIconInputStreamBytes() {
			return iconInputStreamBytes;
		}
		public ProjectSettingEditInputBean setIconInputStreamBytes(byte[] iconInputStreamBytes) {
			this.iconInputStreamBytes = iconInputStreamBytes;
			return this;
		}
	}

	/**
	 * This is the language and timezone to be used in the case of RequestOption = {@link RequestOption#LanguageAndTimezoneChanges}
	 */
	public class LanguageAndTimezoneEditInputBean {
		private String language;
		private String timezone;
		private List<ItemLabelsBean> languageViews = new ArrayList<ItemLabelsBean>();
		private List<ItemLabelsBean> timezoneViews = new ArrayList<ItemLabelsBean>();

		public String getLanguage() {
			return language;
		}
		public LanguageAndTimezoneEditInputBean setLanguage(String language) {
			this.language = language;
			return this;
		}
		public String getTimezone() {
			return timezone;
		}
		public LanguageAndTimezoneEditInputBean setTimezone(String timezone) {
			this.timezone = timezone;
			return this;
		}
		public List<ItemLabelsBean> getLanguageViews() {
			return languageViews;
		}
		public LanguageAndTimezoneEditInputBean setLanguageViews(List<ItemLabelsBean> languageViews) {
			this.languageViews = languageViews;
			return this;
		}
		public List<ItemLabelsBean> getTimezoneViews() {
			return timezoneViews;
		}
		public LanguageAndTimezoneEditInputBean setTimezoneViews(List<ItemLabelsBean> timezoneViews) {
			this.timezoneViews = timezoneViews;
			return this;
		}
	}

	/**
	 * This is the LookAndFeel to be used in the case of RequestOption = {@link RequestOption#LookAndFeelChanges}
	 */
	public class LookAndFeelEditInputBean {
		private LookAndFeel lookAndFeel = LookAndFeel.Trinity;
		private List<ItemLabelsBean> lookAndFeelViews = new ArrayList<ItemLabelsBean>();

		public LookAndFeel getLookAndFeel() {
			return lookAndFeel;
		}
		public LookAndFeelEditInputBean setLookAndFeel(LookAndFeel lookAndFeel) {
			this.lookAndFeel = lookAndFeel;
			return this;
		}

		public List<ItemLabelsBean> getLookAndFeelViews() {
			return lookAndFeelViews;
		}
		public LookAndFeelEditInputBean setLookAndFeelViews(List<ItemLabelsBean> lookAndFeelViews) {
			this.lookAndFeelViews = lookAndFeelViews;
			return this;
		}
	}

	/**
	 * This is the notification to be used in the case of RequestOption = {@link RequestOption#Notification}
	 */
	public class NotificationEditInputBean {
		private String notification;

		public String getNotification() {
			return notification;
		}
		public NotificationEditInputBean setNotification(String notification) {
			this.notification = notification;
			return this;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {

		private boolean completed = false;
		private String projectIconPath = null;
		private String projectNm = null;

		public boolean isCompleted() {
			return completed;
		}
		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}

		public String getProjectIconPath() {
			return projectIconPath;
		}
		public RequestsCompletion setProjectIconPath(String projectIconPath) {
			this.projectIconPath = projectIconPath;
			return this;
		}

		public String getProjectNm() {
			return projectNm;
		}
		public RequestsCompletion setProjectNm(String projectNm) {
			this.projectNm = projectNm;
			return this;
		}
	}

}
