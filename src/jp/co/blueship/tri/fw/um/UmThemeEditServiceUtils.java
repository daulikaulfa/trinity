package jp.co.blueship.tri.fw.um;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.UmDesignBeanId;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;
import jp.co.blueship.tri.fw.um.constants.IconSelectionOption;

/**
 *
 * @version V4.00.00
 * @author Sharov.Maksym
 */
public class UmThemeEditServiceUtils {
	
	public static List<ItemLabelsBean> getLotIconViews(String lotId) {
		List<ItemLabelsBean> iconList = new ArrayList<ItemLabelsBean>();
		Map<String, String> termMap = DesignSheetFactory.getDesignSheet().getKeyMap(UmDesignBeanId.lotIcons);
		
		for (Map.Entry<String, String> entry : termMap.entrySet()) {
			
			ItemLabelsBean userIcon = new ItemLabelsBean();
			userIcon.setLabel(entry.getValue());
			String defaultLotIconPath = UmDesignBusinessRuleUtils.getLotIconParentSharePath(lotId, IconSelectionOption.DefaultImage);
			String iconPath = TriStringUtils.linkPathBySlash(defaultLotIconPath, entry.getKey());
			userIcon.setValue(iconPath);
			
			iconList.add(userIcon);
		}
		
		return iconList;
	}
}
