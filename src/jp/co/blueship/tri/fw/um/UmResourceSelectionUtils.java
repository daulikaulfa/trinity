package jp.co.blueship.tri.fw.um;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.svc.beans.dto.ITreeFolderViewBean;
import jp.co.blueship.tri.fw.svc.beans.dto.TreeFolderViewBean;
import jp.co.blueship.tri.fw.um.dao.svcctg.eb.ISvcCtgEntity;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class UmResourceSelectionUtils {

	/**
	 * Convert the set entities in the parameter to ITreeFolderViewBean.
	 *
	 * @param entities resource entities.
	 * @return
	 */
	public static ITreeFolderViewBean getTreeActionView( List<ISvcCtgEntity> entities ) {
		List<String> servicePath = new ArrayList<String>();

		for (ISvcCtgEntity entity : entities) {
			servicePath.add(entity.getSvcCtgPath());
		}

		ITreeFolderViewBean tree = TreeFolderViewBean.toBean( servicePath );

		return tree;
	}

	public static void convertToLocale( ITreeFolderViewBean tree, String lang ){
		if( null == tree ) return;
		String name = tree.getName();
		if( TriStringUtils.isNotEmpty(name) ){
			tree.setName(ContextAdapterFactory.getContextAdapter().getValue("Role."+name+".text",lang));
		}
		for (ITreeFolderViewBean bean: tree.getFolders()) {
			convertToLocale( bean, lang );
		}
	}
}
