package jp.co.blueship.tri.fw.um.dao.user.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the user entity.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public interface IUserEntity extends IEntityFooter {

	public String getUserId();
	public void setUserId(String userId);

	public String getUserPass();
	public void setUserPass(String userPass);

	public Timestamp getPassTimeLimit();
	public void setPassTimeLimit(Timestamp passTimeLimit);

	public String getUserNm();
	public void setUserNm(String userNm);

	public String getEmailAddr();
	public void setEmailAddr(String emailAddr);

	public String getDeptId();
	public void setDeptId(String deptId);

	public String getDeptNm();
	public void setDeptNm(String deptNm);

	public String getLang();
	public void setLang(String lang);

	public String getTimeZone();
	public void setTimeZone(String id);

	public String getUserLocation();
	public void setUserLocation(String location);

	public String getBiography();
	public void setBiography(String biography);

	public String getIconTyp();
	public void setIconTyp(String iconTyp);

	public String getDefaultIconPath();
	public void setDefaultIconPath(String defaultIconPath);

	public String getCustomIconPath();
	public void setCustomIconPath(String customIconPath);

	public String getGenderTyp();
	public void setGenderTyp(String genderTyp);

	public StatusFlg getIsReceiveMail();
	public void setIsReceiveMail(StatusFlg isReceiveMail);

	public Timestamp getLastLoginTimestamp();
	public void setLastLoginTimestamp(Timestamp lastLoginTimestamp);
	
	public String getLookAndFeel();
	public void setLookAndFeel(String lookAndFeel);

}
