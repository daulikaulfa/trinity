package jp.co.blueship.tri.fw.um.dao.wiki.constants;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;

public enum WikiHistItems implements ITableItem {
	wikiId			("wiki_id"),
	wikiVerNo		("wiki_ver_no"),
	pageNm			("page_nm"),
	content			("content"),
	delStsId		("del_sts_id"),
	regUserId		("reg_user_id"),
	regUserNm		("reg_user_nm"),
	regTimestamp	("reg_timestamp"),
	updUserId		("upd_user_id"),
	updUserNm		("upd_user_nm"),
	updTimestamp	("upd_timestamp"),
	;

	private String element = null;

	private WikiHistItems( String element) {
		this.element = element;
	}

	public String getItemName() {
		return this.element;
	}
}
