package jp.co.blueship.tri.fw.um.dao.mstone.eb;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.dao.orm.psql.SqlFormatUtils;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.fw.um.dao.ctg.constants.CtgLnkItems;
import jp.co.blueship.tri.fw.um.dao.mstone.constants.MstoneLnkItems;

/**
 * The SQL condition of the Milestone Link entity
 *
 * @author Hai Thach
 *
 */
public class MstoneLnkCondition extends ConditionSupport{

	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute milestone = UmTables.UM_MSTONE_LNK;

	/**
	 * milestone-Id
	 */
	public String mstoneId = null;
	/**
	 * data category cd
	 */
	public String dataCtgCd = null;
	/**
	 * data-id
	 */
	public String dataId = null;
	/**
	 * milestone-ID's
	 */
	public String[] mstoneIds = null;
	/**
	 * data category cd's
	 */
	public String[] dataCtgCds = null;
	/**
	 * data id's
	 */
	public String[] dataIds = null;
	/**
	 * delete-status-id
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		super.append(MstoneLnkItems.delStsId, StatusFlg.off.parseBoolean() );
	}

	public MstoneLnkCondition() {
		super(milestone);
	}

	/**
	 * milestone-id
	 * @return milestone-id
	 */
	public String getMstoneId() {
		return mstoneId;
	}

	/**
	 * milestone-id
	 * @param milestoneId milestone-ID
	 */
	public void setMstoneId(String mstoneId) {
		this.mstoneId = mstoneId;
		super.append(MstoneLnkItems.mstoneId, mstoneId);
	}

	/**
	 * data category cd
	 * @return data category cd
	 */
	public String getDataCtgCd() {
		return dataCtgCd;
	}

	/**
	 * data category cd
	 * @param dataCtgCd data category cd
	 */
	public void setDataCtgCd(String dataCtgCd) {
		this.dataCtgCd = dataCtgCd;
		super.append(CtgLnkItems.dataCtgCd, dataCtgCd);
	}

	/**
	 * data-id
	 * @return data-id
	 */
	public String getDataId() {
		return dataId;
	}

	/**
	 * data-id
	 * @param dataId data-id
	 */
	public void setDataId(String dataId) {
		this.dataId = dataId;
		super.append(CtgLnkItems.dataId, dataId);
	}

	/**
	 * milestone-id's
	 * @return milestone-id's
	 */
	public String[] getMstoneIds() {
		return mstoneIds;
	}

	/**
	 * milestone-id's
	 * @param mstoneIds milestone-ID's
	 */
	public void setMstoneIds(String... mstoneIds) {
		this.mstoneIds = mstoneIds;
		super.append( MstoneLnkItems.mstoneId.getItemName() + "[]",
				SqlFormatUtils.getCondition(mstoneIds, MstoneLnkItems.mstoneId.getItemName(), false, true, false) );
	}

	/**
	 * data category cd's
	 * @return data category cd's
	 */
	public String[] getDataCtgCds() {
		return dataCtgCds;
	}

	/**
	 * data category cd's
	 * @param dataCtgCds data category cd's
	 */
	public void setDataCtgCds(String... dataCtgCds) {
		this.dataCtgCds = dataCtgCds;
		super.append( CtgLnkItems.dataCtgCd.getItemName() + "[]",
				SqlFormatUtils.getCondition(dataCtgCds, CtgLnkItems.dataCtgCd.getItemName(), false, true, false) );
	}

	/**
	 * data-id's
	 * @return data-id's
	 */
	public String[] getDataIds() {
		return dataIds;
	}

	/**
	 * data-id's
	 * @param dataIds data-ID's
	 */
	public void setDataIds(String... dataIds) {
		this.dataIds = dataIds;
		super.append( CtgLnkItems.dataId.getItemName() + "[]",
				SqlFormatUtils.getCondition(dataIds, CtgLnkItems.dataId.getItemName(), false, true, false) );
	}

}
