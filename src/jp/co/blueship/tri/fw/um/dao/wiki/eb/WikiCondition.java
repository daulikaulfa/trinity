package jp.co.blueship.tri.fw.um.dao.wiki.eb;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.constants.JoinType;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.dao.orm.psql.SqlFormatUtils;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.fw.um.dao.wiki.constants.WikiItems;

/**
 * The SQL condition of the wiki entity.
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 * @version V4.03.00
 * @author Cuong Nguyen
 *
 */
public class WikiCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute wiki = UmTables.UM_WIKI;

	public WikiCondition() {
		super(wiki);
		super.setJoinExecData( true );
		super.setKeyByJoinExecData( WikiItems.wikiId );
	}
	
	@Override
	public WikiCondition setJoinAccsHist( boolean isJoinAccsHist ) {
		super.setJoinAccsHist(isJoinAccsHist);
		return this;
	}

	@Override
	public WikiCondition setJoinAccsHist( boolean isJoinAccsHist, JoinType joinType ) {
		super.setJoinAccsHist(isJoinAccsHist, joinType);
		return this;
	}

	public String wikiId = null;
	public String[] wikiIds = null;
	public String lotId = null;
	public String[] lotIds = null;
	public String pageNm = null;
	public StatusFlg isWikiHome = null;
	public String keyword = null;
	public StatusFlg delStsId = StatusFlg.off;
	{
		super.append(WikiItems.delStsId, StatusFlg.off.parseBoolean() );
	}

	public String getWikiId(){
		return this.wikiId;
	}
	public void setWikiId(String wikiId){
		this.wikiId = wikiId;
		super.append( WikiItems.wikiId, wikiId);
	}

	public String[] getWikiIds(){
		return this.wikiIds;
	}
	public void setWikiIds(String[] wikiIds){
		this.wikiIds = wikiIds;
		super.append( WikiItems.wikiId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(wikiIds, WikiItems.wikiId.getItemName(), false, true, false) );
	}

	public String getLotId(){
		return this.lotId;
	}
	public void setLotId(String lotId){
		this.lotId = lotId;
		super.append( WikiItems.lotId, lotId);
	}
	
	public String[] getLotIds(){
		return this.lotIds;
	}
	public void setLotIds(String[] lotIds){
		this.lotIds = lotIds;
		super.append( WikiItems.lotId.getItemName() + "[]",
				SqlFormatUtils.getCondition(lotIds, WikiItems.lotId.getItemName(), false, true, false) );
	}

	public String getPageNm(){
		return this.pageNm;
	}
	public void setPageNm(String pageNm){
		this.pageNm = pageNm;
		super.append( WikiItems.pageNm + "Other", SqlFormatUtils.getCondition( this.pageNm, WikiItems.pageNm.getItemName(), true, false, false));
	}

	public StatusFlg getIsWikiHome(){
		return this.isWikiHome;
	}
	public void setIsWikiHome(StatusFlg isWikiHome){
		this.isWikiHome = isWikiHome;
		super.append( WikiItems.isWikiHome, (null == isWikiHome)? null :isWikiHome.parseBoolean());
	}

	public String getKeyword(){
		return this.keyword;
	}
	public void setKeyword(String keyword){
		this.keyword = keyword;
		super.append( WikiItems.pageNm + "Other",
					  SqlFormatUtils.joinCondition( true,
							new String[] {
								SqlFormatUtils.getCondition( this.keyword, WikiItems.pageNm.getItemName(), true, false, false),
								SqlFormatUtils.getCondition( this.keyword, WikiItems.content.getItemName(), true, false, false),
							}
					  )
		);
	}

	public StatusFlg getDelStsId(){
		return this.delStsId;
	}
	public void setDelStsId(StatusFlg delStsId){
		this.delStsId = delStsId;
		super.append( WikiItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean());
	}
}
