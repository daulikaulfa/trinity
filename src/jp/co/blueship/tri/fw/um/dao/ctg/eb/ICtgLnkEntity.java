package jp.co.blueship.tri.fw.um.dao.ctg.eb;

import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the category link entity.
 *
 * @version V4.00.00
 * @author Hai Thach
 */
public interface ICtgLnkEntity extends IEntityFooter {
	/**
	 * category-ID
	 * @return category-ID
	 */
	public String getCtgId();
	/**
	 * category-ID
	 * @param ctgId category-ID
	 */
	public void setCtgId(String ctgId);
	/**
	 * data-category-cd
	 * @return data-category-cd
	 */
	public String getDataCtgCd();
	/**
	 * data-category-cd
	 * @param dataCtgCd data-category-cd
	 */
	public void setDataCtgCd(String dataCtgCd);
	/**
	 * data-ID
	 * @return data-ID
	 */
	public String getDataId();
	/**
	 * data-ID
	 * @param dataId data-ID
	 */
	public void setDataId(String dataId);

}
