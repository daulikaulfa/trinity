package jp.co.blueship.tri.fw.um.dao.grp.eb;

import jp.co.blueship.tri.fw.dao.orm.EntityFooter;


/**
 * group entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 * 
 * @version V4.00.00
 * @author le.thixuan
 */
public class GrpEntity extends EntityFooter implements IGrpEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * grp-ID
	 */
	public String grpId = null;
	/**
	 * group name
	 */
	public String grpNm = null;
	
	/**
	 * sort order
	 */
	public Integer sortOdr = null;

	/**
	 * grp-IDを取得します。
	 * @return grp-ID
	 */
	public String getGrpId() {
	    return grpId;
	}
	/**
	 * grp-IDを設定します。
	 * @param grpId grp-ID
	 */
	public void setGrpId(String grpId) {
	    this.grpId = grpId;
	}
	/**
	 * group nameを取得します。
	 * @return group name
	 */
	public String getGrpNm() {
	    return grpNm;
	}
	/**
	 * group nameを設定します。
	 * @param grpNm group name
	 */
	public void setGrpNm(String grpNm) {
	    this.grpNm = grpNm;
	}
	
	@Override
	public Integer getSortOdr() {
		return sortOdr;
	}
	
	@Override
	public void setSortOdr(Integer sortOdr) {
		this.sortOdr = sortOdr;
	}

}