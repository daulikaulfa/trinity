package jp.co.blueship.tri.fw.um.dao.wiki;

import jp.co.blueship.tri.fw.um.dao.hist.IHistDao;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.IWikiEntity;

/**
 * The interface of the wikiHist DAO.
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 */
public interface IWikiUpdateHistDao extends IHistDao {

	public int insert(IHistEntity entity , IWikiEntity wikiEntity);
}
