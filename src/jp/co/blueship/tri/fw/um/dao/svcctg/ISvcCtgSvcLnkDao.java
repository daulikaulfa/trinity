package jp.co.blueship.tri.fw.um.dao.svcctg;

import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;
import jp.co.blueship.tri.fw.um.dao.svcctg.eb.ISvcCtgSvcLnkEntity;


/**
 * The interface of the service catalog service link DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface ISvcCtgSvcLnkDao extends IJdbcDao<ISvcCtgSvcLnkEntity> {

}
