package jp.co.blueship.tri.fw.um.dao.wiki;

import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.IWikiAttachedFileEntity;

/**
 * The interface of the wikiAtatchedFileDao DAO.
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 */
public interface IWikiAttachedFileDao extends IJdbcDao<IWikiAttachedFileEntity> {

}
