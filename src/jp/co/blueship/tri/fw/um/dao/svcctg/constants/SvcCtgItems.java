package jp.co.blueship.tri.fw.um.dao.svcctg.constants;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;


/**
 * The items of the service catalog entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public enum SvcCtgItems implements ITableItem {
	svcCtgId("svc_ctg_id"),
	svcCtgNm("svc_ctg_nm"),
	svcCtgContent("svc_ctg_content"),
	svcCtgPath("svc_ctg_path"),
	sortOdr("sort_odr"),
	delStsId("del_sts_id"),
	regUserId("reg_user_id"),
	regUserNm("reg_user_nm"),
	regTimestamp("reg_timestamp"),
	updUserId("upd_user_id"),
	updUserNm("upd_user_nm"),
	updTimestamp("upd_timestamp");

	private String element = null;

	private SvcCtgItems( String element) {
		this.element = element;
	}

	public String getItemName() {
		return this.element;
	}

}
