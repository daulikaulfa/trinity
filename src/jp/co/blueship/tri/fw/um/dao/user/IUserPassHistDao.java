package jp.co.blueship.tri.fw.um.dao.user;

import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserPassHistEntity;


/**
 * The interface of the user password histry DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IUserPassHistDao extends IJdbcDao<IUserPassHistEntity> {

}
