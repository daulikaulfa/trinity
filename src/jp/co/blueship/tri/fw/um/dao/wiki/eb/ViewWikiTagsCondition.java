package jp.co.blueship.tri.fw.um.dao.wiki.eb;

/**
 * The SQL condition of the asset request entity.
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public class ViewWikiTagsCondition {
	/**
	 * The interface of the table properties.
	 */
	/*
	private static final ITableAttribute attr = UmTables.UM_WIKI_TAG;

	public ViewWikiTagsCondition(){
		super(attr);
	}
	*/

	private String tagNm = null;
	private String lotId = null;


	public String getTagNm() {
		return this.tagNm;
	}
	public void setTagNm(String tagNm) {
		this.tagNm = tagNm;
	}

	public String getLotId() {
		return this.lotId;
	}
	public void setLotId(String lotId) {
		this.lotId = lotId;
	}
}
