package jp.co.blueship.tri.fw.um.dao.role;

import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;
import jp.co.blueship.tri.fw.um.dao.role.eb.IRoleSvcCtgLnkEntity;


/**
 * The interface of the role service catalog link DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IRoleSvcCtgLnkDao extends IJdbcDao<IRoleSvcCtgLnkEntity> {

}
