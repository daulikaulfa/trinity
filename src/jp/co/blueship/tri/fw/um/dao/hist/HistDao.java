package jp.co.blueship.tri.fw.um.dao.hist;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.apache.xmlbeans.XmlOptions;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.INumberingDao;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;
import jp.co.blueship.tri.fw.um.dao.accshist.constants.AccsHistItems;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.fw.um.dao.hist.constants.HistItems;
import jp.co.blueship.tri.fw.um.dao.hist.eb.HistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;

/**
 * The implements of the history DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version V4.00.00
 * @author Yohei Tanaka
 */
public class HistDao extends JdbcBaseDao<IHistEntity> implements IHistDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	public INumberingDao histNumberingDao;

	private Map<String, String> xmlNamespaceMap = new HashMap<String, String>();
	private XmlOptions opt = new XmlOptions(); /* http://xmlbeans.apache.org/docs/2.0.0/reference/constant-values.html#org.apache.xmlbeans.XmlOptions.COMPILE_MDEF_NAMESPACES */

	/**
	 * 自動採番インタフェースがインスタンス生成時に自動的に設定されます。
	 * @param histNumberingDao 自動採番インタフェース
	 */
	public final void setHistNumberingDao( INumberingDao histNumberingDao ) {
		this.histNumberingDao = histNumberingDao;
	}

	/**
	 * 自動採番インタフェースを取得します。
	 * @return numbering 自動採番インタフェース
	 */
	protected final INumberingDao getHistNumberingDao() {
		return this.histNumberingDao;
	}

	@Override
	protected final ITableAttribute getTableAttribute() {
		return UmTables.UM_HIST;
	}

	@Override
	protected final ISqlBuilder append( ISqlBuilder builder, IHistEntity entity ) {
		builder
			.append(HistItems.histId, entity.getHistId(), true)
			.append(HistItems.dataCtgCd, entity.getDataCtgCd())
			.append(HistItems.dataId, entity.getDataId())
			.append(HistItems.lotId, entity.getLotId())
			.append(HistItems.histData, entity.getHistData())
			.append(HistItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(HistItems.regTimestamp, entity.getRegTimestamp())
			.append(HistItems.regUserId, entity.getRegUserId())
			.append(HistItems.regUserNm, entity.getRegUserNm())
			.append(HistItems.updTimestamp, entity.getUpdTimestamp())
			.append(HistItems.updUserId, entity.getUpdUserId())
			.append(HistItems.updUserNm, entity.getUpdUserNm())
			.append(HistItems.stsId, entity.getStsId())
			.append(HistItems.actStsId, entity.getActSts())
			;

		return builder;
	}

	@Override
	protected final IHistEntity entityMapping( ResultSet rs, int row ) throws SQLException {
		HistEntity entity = new HistEntity();

		entity.setHistId( rs.getString(HistItems.histId.getItemName()) );
		entity.setDataCtgCd( rs.getString(HistItems.dataCtgCd.getItemName()) );
		entity.setDataId( rs.getString(HistItems.dataId.getItemName()) );
		entity.setLotId( rs.getString(HistItems.lotId.getItemName()) );
		entity.setHistData( rs.getString(HistItems.histData.getItemName()) );
		entity.setStsId( rs.getString(HistItems.stsId.getItemName()) );
		entity.setActSts( rs.getString(HistItems.actStsId.getItemName()) );
		entity.setDelStsId( StatusFlg.value(rs.getBoolean(HistItems.delStsId.getItemName())) );
		entity.setRegTimestamp( rs.getTimestamp(HistItems.regTimestamp.getItemName()) );
		entity.setRegUserId( rs.getString(HistItems.regUserId.getItemName()) );
		entity.setRegUserNm( rs.getString(HistItems.regUserNm.getItemName()) );
		entity.setUpdTimestamp( rs.getTimestamp(HistItems.updTimestamp.getItemName()) );
		entity.setUpdUserId( rs.getString(HistItems.updUserId.getItemName()) );
		entity.setUpdUserNm( rs.getString(HistItems.updUserNm.getItemName()) );

		entity.setAccsTimestamp( rs.getTimestamp( AccsHistItems.accsTimestamp.getItemName() ) );
		entity.setNoticeViewTimestamp( rs.getTimestamp( AccsHistItems.noticeViewTimestamp.getItemName() ) );

		return entity;
	}

	/**
	 * XML名前空間を指定します。
	 * @param key キー値
	 * @param nameSpace 名前空間
	 */
	protected void addNameSpace( String key, String nameSpace ) {
		xmlNamespaceMap.put(key, nameSpace);
		opt.setLoadSubstituteNamespaces( xmlNamespaceMap );
	}

    /**
     * 名前空間を示すパラメータを取得します。
     *
     * @return 名前空間を示すパラメータを戻します
     */
    protected final XmlOptions getXmlOptions() {
    	return opt;
    }

}
