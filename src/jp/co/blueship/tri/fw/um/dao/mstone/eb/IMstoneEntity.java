/**
 *
 */
package jp.co.blueship.tri.fw.um.dao.mstone.eb;

import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the milestone entity.
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 */
public interface IMstoneEntity extends IEntityFooter {
	/**
	 * milestone-ID
	 * @return milestone-ID
	 */
	public String getMstoneId();
	/**
	 * milestone-ID
	 * @param milestoneId milestone-ID
	 */
	public void setMstoneId(String mstoneId);
	/**
	 * milestone name
	 * @return milestone name
	 */
	public String getMstoneNm();
	/**
	 * milestone name
	 * @param milestoneNm milestone name
	 */
	public void setMstoneNm(String mstoneNm);
	/**
	 * milestone start date
	 * @return milestone start date
	 */
	public String getMstoneStDate();
	/**
	 * milestone start date
	 * @param mstoneStDate milestone start date
	 */
	public void setMstoneStDate(String mstoneStDate);
	/**
	 * milestone end date
	 * @return milestone end date
	 */
	public String getMstoneEndDate();
	/**
	 * milestone end date
	 * @param mstoneEndDate milestone end date
	 */
	public void setMstoneEndDate(String mstoneEndDate);
	/**
	 * content
	 * @return content
	 */
	public String getContent();
	/**
	 * content
	 * @param content content
	 */
	public void setContent(String content);
	/**
	 * lot id
	 * @return lot Id
	 */
	public String getLotId();
	/**
	 * lot id
	 * @param lotId lot id
	 */
	public void setLotId(String lotId);
	/**
	 * sort order
	 * @return sort order
	 */
	public Integer getSortOdr();
	/**
	 * sort order
	 * @param sortOdr sort order
	 */
	public void setSortOdr(Integer sortOdr);
}
