package jp.co.blueship.tri.fw.um.dao.grp.constants;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;


/**
 * The items of the group entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 * 
 * @version V4.00.00
 * @author le.thixuan
 */
public enum GrpItems implements ITableItem {
	grpId("grp_id"),
	grpNm("grp_nm"),
	sortOdr("sort_odr"),
	delStsId("del_sts_id"),
	regUserId("reg_user_id"),
	regUserNm("reg_user_nm"),
	regTimestamp("reg_timestamp"),
	updUserId("upd_user_id"),
	updUserNm("upd_user_nm"),
	updTimestamp("upd_timestamp");

	private String element = null;

	private GrpItems( String element) {
		this.element = element;
	}

	public String getItemName() {
		return this.element;
	}

}
