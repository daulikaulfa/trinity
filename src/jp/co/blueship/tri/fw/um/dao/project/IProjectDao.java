package jp.co.blueship.tri.fw.um.dao.project;

import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;
import jp.co.blueship.tri.fw.um.dao.project.eb.IProjectEntity;

/**
 * The interface of the project DAO.
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 */
public interface IProjectDao extends IJdbcDao<IProjectEntity>{

}
