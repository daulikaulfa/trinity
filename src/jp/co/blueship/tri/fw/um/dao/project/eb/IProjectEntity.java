package jp.co.blueship.tri.fw.um.dao.project.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the project entity.
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 */
public interface IProjectEntity extends IEntityFooter {

	public String getProductId();
	public void setProductId(String productId);

	public String getProjectNm();
	public void setProjectNm(String projectNm);

	public String getIconTyp();
	public void setIconTyp(String iconTyp);

	public String getDefaultIconPath();
	public void setDefaultIconPath(String defaultIconPath);

	public String getCustomIconPath();
	public void setCustomIconPath(String customIconPath);

	public String getLanguage();
	public void setLanguage(String language);

	public String getTimezone();
	public void setTimezone(String timezone);

	public String getNotice();
	public void setNotice(String notice);
	
	public Timestamp getNoticeTimestamp();
	public void setNoticeTimestamp(Timestamp noticeTimestamp);
	
	public String getLookAndFeel();
	public void setLookAndFeel(String lookAndFeel);

}
