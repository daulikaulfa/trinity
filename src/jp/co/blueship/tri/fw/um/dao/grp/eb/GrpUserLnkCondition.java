package jp.co.blueship.tri.fw.um.dao.grp.eb;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.dao.orm.psql.SqlFormatUtils;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.fw.um.dao.grp.constants.GrpUserLnkItems;

/**
 * The SQL condition of the group role link entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class GrpUserLnkCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute grp = UmTables.UM_GRP_USER_LNK;

	/**
	 * grp-ID
	 */
	public String grpId = null;
	/**
	 * grp-ID's
	 */
	public String[] grpIds = null;
	/**
	 * user-ID
	 */
	public String userId = null;
	/**
	 * user-ID's
	 */
	public String[] userIds = null;
	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		   super.append(GrpUserLnkItems.delStsId, StatusFlg.off.parseBoolean() );
	}

	public GrpUserLnkCondition() {
		super(grp);
	}

	/**
	 * grp-IDを取得します。
	 * @return grp-ID
	 */
	public String getGrpId() {
	    return grpId;
	}

	/**
	 * grp-IDを設定します。
	 * @param grpId grp-ID
	 */
	public void setGrpId(String grpId) {
	    this.grpId = grpId;
	    super.append(GrpUserLnkItems.grpId, grpId );
	}

	/**
	 * grp-ID'sを取得します。
	 * @return grp-ID's
	 */
	public String[] getGrpIds() {
	    return grpIds;
	}

	/**
	 * grp-ID'sを設定します。
	 * @param grpIds grp-ID's
	 */
	public void setGrpIds(String[] grpIds) {
	    this.grpIds = grpIds;
	    super.append( GrpUserLnkItems.grpId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(grpIds, GrpUserLnkItems.grpId.getItemName(), false, true, false) );
	}

	/**
	 * user-IDを取得します。
	 * @return user-ID
	 */
	public String getUserId() {
	    return userId;
	}

	/**
	 * role-IDを設定します。
	 * @param userId user-ID
	 */
	public void setUserId(String userId) {
	    this.userId = userId;
	    super.append(GrpUserLnkItems.userId, userId );
	}

	/**
	 * user-ID'sを取得します。
	 * @return user-ID's
	 */
	public String[] getUserIds() {
	    return userIds;
	}

	/**
	 * user-ID'sを設定します。
	 * @param userIds user-ID's
	 */
	public void setUserIds(String[] userIds) {
	    this.userIds = userIds;
	    super.append( GrpUserLnkItems.userId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(userIds, GrpUserLnkItems.userId.getItemName(), false, true, false) );
	}

	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	    super.append( GrpUserLnkItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}
}