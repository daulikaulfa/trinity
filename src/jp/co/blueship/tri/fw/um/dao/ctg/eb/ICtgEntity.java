package jp.co.blueship.tri.fw.um.dao.ctg.eb;

import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the category entity.
 *
 * @version V4.00.00
 * @author Hai Thach
 */
public interface ICtgEntity extends IEntityFooter {
	/**
	 * category-ID
	 * @return category-ID
	 */
	public String getCtgId();
	/**
	 * category-ID
	 * @param ctgId category-ID
	 */
	public void setCtgId(String ctgId);
	/**
	 * category name
	 * @return category name
	 */
	public String getCtgNm();
	/**
	 * category name
	 * @param ctgNm category name
	 */
	public void setCtgNm(String ctgNm);
	/**
	 * lot-Id
	 * @return lotId
	 */
	public String getLotId();
	/**
	 * lotId
	 * @param lotId lot-Id
	 */
	public void setLotId(String lotId);
	/**
	 * sort order
	 * @return sort order
	 */
	public Integer getSortOdr();
	/**
	 * sort order
	 * @param sortOdr sort order
	 */
	public void setSortOdr(Integer sortOdr);

}
