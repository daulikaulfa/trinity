package jp.co.blueship.tri.fw.um.dao.ctg.eb;

/**
 * The implementation of category DTO.
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 */

public class CtgDto implements ICtgDto {

	/**
	 * Category Entity
	 */
	public ICtgEntity ctgEntity = null;

	@Override
	public ICtgEntity getCtgEntity() {
		return ctgEntity;
	}

	@Override
	public void setCtgEntity(ICtgEntity ctgEntity) {
		this.ctgEntity = ctgEntity;
	}

}
