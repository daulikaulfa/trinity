package jp.co.blueship.tri.fw.um.dao.hist.utils;

import java.util.List;

import jp.co.blueship.tri.am.dao.areq.eb.IAreqAttachedFileEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqBinaryFileEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqFileEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotGrpLnkEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotMailGrpLnkEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotMdlLnkEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotRelEnvLnkEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpAreqLnkEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpDto;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpMdlLnkEntity;
import jp.co.blueship.tri.dcm.dao.rep.eb.IRepEntity;
import jp.co.blueship.tri.dm.dao.dmdo.eb.IDmDoEntity;
import jp.co.blueship.tri.fw.dao.oxm.utils.TriXmlMappingUtils;
import jp.co.blueship.tri.fw.schema.beans.areq.AreqBinaryFileType;
import jp.co.blueship.tri.fw.schema.beans.areq.AreqFileType;
import jp.co.blueship.tri.fw.schema.beans.areq.AreqType;
import jp.co.blueship.tri.fw.schema.beans.areq.AreqType.Link;
import jp.co.blueship.tri.fw.schema.beans.areq.AreqType.Link.AreqAttachedFile;
import jp.co.blueship.tri.fw.schema.beans.bp.BpType;
import jp.co.blueship.tri.fw.schema.beans.bp.BpType.Link.BpAreq;
import jp.co.blueship.tri.fw.schema.beans.bp.BpType.Link.Mdl;
import jp.co.blueship.tri.fw.schema.beans.lot.GrpType;
import jp.co.blueship.tri.fw.schema.beans.lot.LotType;
import jp.co.blueship.tri.fw.schema.beans.lot.LotType.Link.RelEnv;
import jp.co.blueship.tri.fw.schema.beans.ra.RaType;
import jp.co.blueship.tri.fw.schema.beans.ra.RaType.Link.Bp;
import jp.co.blueship.tri.fw.schema.beans.ra.RaType.Link.RaAttachedFile;
import jp.co.blueship.tri.fw.schema.beans.ra.RaType.Link.Rp;
import jp.co.blueship.tri.fw.schema.beans.rep.RepType;
import jp.co.blueship.tri.fw.schema.beans.rp.DoType;
import jp.co.blueship.tri.fw.schema.beans.rp.RpType;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaAttachedFileEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaBpLnkEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaDto;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaRpLnkEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpBpLnkEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpDto;

/**
 * XMLBeansのEntityとDataBaseのEntity、双方向のマッピングを行います。
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version V3L12.01
 * @author Yukihiro Eguchi
 */
public class HistMappingUtils extends TriXmlMappingUtils {
	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param d 複写元のDBのAreqDto
	 * @param x 複写先のXMLBeans
	 * @return マッピング結果を戻します。
	 */
	public final AreqType mapDB2XMLBeans( IAreqDto d, AreqType x ) {

		copyProperties(d.getAreqEntity(), x);

		if( ( d.getAreqFileEntities()!=null && d.getAreqFileEntities().size()>0 ) ||
			( d.getAreqBinaryFileEntities()!=null && d.getAreqBinaryFileEntities().size()>0 ) ||
			( d.getAreqAttachedFileEntities()!=null && d.getAreqAttachedFileEntities().size()>0 )
			) {
			Link link = x.addNewLink();
			// AreqFile
			if( d.getAreqFileEntities()!=null && d.getAreqFileEntities().size()>0 ) {
				for ( IAreqFileEntity areqFile : d.getAreqFileEntities() ) {
					AreqFileType areqFileType = link.addNewAreqFile();
					copyProperties(areqFile,areqFileType);
				}
			}
			// AreqBinaryFile
			if( d.getAreqBinaryFileEntities()!=null && d.getAreqBinaryFileEntities().size()>0 ) {
				for ( IAreqBinaryFileEntity areqFile : d.getAreqBinaryFileEntities() ) {
					AreqBinaryFileType areqBinaryFileType = link.addNewAreqBinaryFile();
					copyProperties(areqFile,areqBinaryFileType);
				}
			}
			// AreqAttachedFile
			if( d.getAreqAttachedFileEntities()!=null && d.getAreqAttachedFileEntities().size()>0 ) {
				for ( IAreqAttachedFileEntity areqFile : d.getAreqAttachedFileEntities() ) {
					AreqAttachedFile areqAttachedFileType = link.addNewAreqAttachedFile();
					copyProperties(areqFile,areqAttachedFileType);
				}
			}
		}
		return x;
	}
	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param d 複写元のDBのLotDto
	 * @param x 複写先のXMLBeans
	 * @return マッピング結果を戻します。
	 */
	public final LotType mapDB2XMLBeans( ILotDto d, LotType x ) {

		copyProperties(d.getLotEntity(), x);

		if ( null != d.getLotEntity().isUseMerge() ) {
			x.setUseMerge( d.getLotEntity().isUseMerge().parseBoolean() );
		}

		if( ( d.getLotMdlLnkEntities()!=null && d.getLotMdlLnkEntities().size()>0 ) ||
			( d.getLotRelEnvLnkEntities()!=null && d.getLotRelEnvLnkEntities().size()>0 ) ||
			( d.getLotGrpLnkEntities()!=null && d.getLotGrpLnkEntities().size()>0 ) ||
			( d.getLotMailGrpLnkEntities()!=null && d.getLotMailGrpLnkEntities().size()>0 )
			) {
			jp.co.blueship.tri.fw.schema.beans.lot.LotType.Link link = x.addNewLink();
			// MdlLnk
			if( d.getLotMdlLnkEntities()!=null && d.getLotMdlLnkEntities().size()>0 ) {
				for ( ILotMdlLnkEntity lotMdlEntity : d.getLotMdlLnkEntities() ) {
					jp.co.blueship.tri.fw.schema.beans.lot.LotType.Link.Mdl mdlType = link.addNewMdl();
					copyProperties(lotMdlEntity,mdlType);
				}
			}
			// RelEnvLnk
			if( d.getLotRelEnvLnkEntities()!=null && d.getLotRelEnvLnkEntities().size()>0 ) {
				for ( ILotRelEnvLnkEntity lotRelEntity : d.getLotRelEnvLnkEntities() ) {
					RelEnv relEnvType = link.addNewRelEnv();
					copyProperties(lotRelEntity,relEnvType);
				}
			}
			// GrpLnk
			if( d.getLotGrpLnkEntities()!=null && d.getLotGrpLnkEntities().size()>0 ) {
				for ( ILotGrpLnkEntity grpEntity : d.getLotGrpLnkEntities() ) {
					GrpType grpType = link.addNewGrp();
					copyProperties(grpEntity,grpType);
				}
			}
			// MailGrpLnk
			if( d.getLotMailGrpLnkEntities()!=null && d.getLotMailGrpLnkEntities().size()>0 ) {
				for ( ILotMailGrpLnkEntity mailGrpEntity : d.getLotMailGrpLnkEntities() ) {
					GrpType grpType = link.addNewMailGrp();
					copyProperties(mailGrpEntity,grpType);
				}
			}
		}

		return x;
	}
	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param d 複写元のDBのBpDto
	 * @param x 複写先のXMLBeans
	 * @return マッピング結果を戻します。
	 */
	public final BpType mapDB2XMLBeans( IBpDto d, BpType x ) {

		copyProperties(d.getBpEntity(), x);

		if( ( d.getBpAreqLnkEntities()!=null && d.getBpAreqLnkEntities().size()>0 ) ||
			( d.getBpMdlLnkEntities()!=null && d.getBpMdlLnkEntities().size()>0 )
			) {
			jp.co.blueship.tri.fw.schema.beans.bp.BpType.Link link = x.addNewLink();
			// BpAreqLnk
			if( d.getBpAreqLnkEntities()!=null && d.getBpAreqLnkEntities().size()>0 ) {
				for ( IBpAreqLnkEntity bpAreqEntity : d.getBpAreqLnkEntities() ) {
					BpAreq bpAreqLink = link.addNewBpAreq();
					copyProperties(bpAreqEntity,bpAreqLink);
				}
			}
			// BpMdlLnk
			if( d.getBpMdlLnkEntities()!=null && d.getBpMdlLnkEntities().size()>0 ) {
				for ( IBpMdlLnkEntity bpMdlEntity : d.getBpMdlLnkEntities() ) {
					Mdl bpMdlLink = link.addNewMdl();
					copyProperties(bpMdlEntity,bpMdlLink);
				}
			}
		}
			return x;
	}
	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param d 複写元のDBのRaDto
	 * @param x 複写先のXMLBeans
	 * @return マッピング結果を戻します。
	 */
	public final RaType mapDB2XMLBeans( IRaDto d, RaType x ) {

		copyProperties(d.getRaEntity(), x);

		if( ( d.getRaBpLnkEntities() != null && d.getRaBpLnkEntities().size() > 0 ) ||
				( d.getRaRpLnkEntities() != null && d.getRaRpLnkEntities().size()>0 ) ||
				( d.getRaAttachedFileEntities()!=null && d.getRaAttachedFileEntities().size()>0 )
				) {
			jp.co.blueship.tri.fw.schema.beans.ra.RaType.Link link = x.addNewLink();
			// RaBpLnk
			if( d.getRaBpLnkEntities() != null && d.getRaBpLnkEntities().size() > 0 ) {
				for ( IRaBpLnkEntity raBpLnkEntity : d.getRaBpLnkEntities() ) {
					Bp raBpLink = link.addNewBp();
					copyProperties( raBpLnkEntity, raBpLink );
				}
			}
			// RaRpLnk
			if( d.getRaRpLnkEntities() != null && d.getRaRpLnkEntities().size() > 0 ) {
				for ( IRaRpLnkEntity raRpEntity : d.getRaRpLnkEntities() ) {
					Rp raRpLink = link.addNewRp();
					copyProperties(raRpEntity,raRpLink);
				}
			}
			// RaAttachedFile
			if( d.getRaAttachedFileEntities()!=null && d.getRaAttachedFileEntities().size()>0 ) {
				for ( IRaAttachedFileEntity raAttachedFileEntity : d.getRaAttachedFileEntities() ) {
					RaAttachedFile raAttachedFile = link.addNewRaAttachedFile();
					copyProperties(raAttachedFileEntity,raAttachedFile);
				}
			}
		}
		return x;
	}
	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param d 複写元のDBのRpDto
	 * @param doEntities 複写元のDBのDoテーブル情報
	 * @param x 複写先のXMLBeans
	 * @return マッピング結果を戻します。
	 */
	public final RpType mapDB2XMLBeans( IRpDto d, List<IDmDoEntity> doEntities, List<String> raIds, RpType x ) {

		copyProperties(d.getRpEntity(), x);

		if( ( d.getRpBpLnkEntities()!=null && d.getRpBpLnkEntities().size()>0 ) ||
			( doEntities!=null && doEntities.size()>0 ) ||
			( raIds!=null && raIds.size()>0 )
			) {
			jp.co.blueship.tri.fw.schema.beans.rp.RpType.Link link = x.addNewLink();
			// RpBpLnk
			if( d.getRpBpLnkEntities()!=null && d.getRpBpLnkEntities().size()>0 ) {
				for ( IRpBpLnkEntity rpBpEntity : d.getRpBpLnkEntities() ) {
					jp.co.blueship.tri.fw.schema.beans.rp.BpType rpBpType = link.addNewBp();
					copyProperties(rpBpEntity, rpBpType);
				}
			}
			// RpDo
			if( doEntities!=null && doEntities.size()>0 ) {
				for ( IDmDoEntity doEntity : doEntities ) {
					DoType doType = link.addNewDo();
					copyProperties(doEntity,doType);
					doType.setTimerContent(doEntity.getTimerSettingContent());
					doType.setTimerSummary(doEntity.getTimerSettingSummary());
					doType.setTimerDate(doEntity.getTimerSettingDate());
					doType.setTimerTime(doEntity.getTimerSettingTime());
				}
			}
			// RpRa
			if( raIds!=null && raIds.size()>0 ) {
				for ( String raId : raIds ) {
					jp.co.blueship.tri.fw.schema.beans.rp.RaType raType = link.addNewRa();
					raType.setRaId( raId );
				}
			}
		}

		return x;
	}
	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param d 複写元のDBのRepEntity
	 * @param x 複写先のXMLBeans
	 * @return マッピング結果を戻します。
	 */
	public final RepType mapDB2XMLBeans( IRepEntity d, RepType x ) {

		copyProperties(d, x);

		return x;
	}
}

