package jp.co.blueship.tri.fw.um.dao.wiki.eb;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the wiki entity.
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 */
public interface IWikiEntity extends IEntityFooter {

	public String getWikiId();
	public void setWikiId(String wikiId);

	public String getLotId();
	public void setLotId(String lotId);

	public Integer getWikiVerNo();
	public void setWikiVerNo(Integer wikiVerNo);

	public String getPageNm();
	public void setPageNm(String pageNm);

	public String getContent();
	public void setContent(String content);

	public StatusFlg isWikiHome();
	public void setIsWikiHome(StatusFlg isWikiHome);
}
