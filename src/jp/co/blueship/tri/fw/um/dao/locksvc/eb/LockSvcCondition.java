package jp.co.blueship.tri.fw.um.dao.locksvc.eb;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.dao.orm.psql.SqlFormatUtils;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.fw.um.dao.locksvc.constants.LockSvcItems;

/**
 * The SQL condition of the lock service entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class LockSvcCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute locksvc = UmTables.UM_LOCK_SVC;

	/**
	 * svc-ID
	 */
	public String svcId = null;
	/**
	 * svc-ID's
	 */
	public String[] svcIds = null;
	/**
	 * lock svc-ID
	 */
	public String lockSvcId = null;
	/**
	 * lock svc-ID's
	 */
	public String[] lockSvcIds = null;
	/**
	 * lock for lot
	 */
	public StatusFlg lockForLot = StatusFlg.off;
	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		   super.append(LockSvcItems.delStsId, StatusFlg.off.parseBoolean() );
	}

	public LockSvcCondition(){
		super(locksvc);
	}

	/**
	 * svc-IDを取得します。
	 * @return svc-ID
	 */
	public String getSvcId() {
	    return svcId;
	}

	/**
	 * svc-IDを設定します。
	 * @param svcId svc-ID
	 */
	public void setSvcId(String svcId) {
	    this.svcId = svcId;
	    super.append(LockSvcItems.svcId, svcId );
	}

	/**
	 * svc-ID'sを取得します。
	 * @return locksvc-ID's
	 */
	public String[] getSvcIds() {
	    return svcIds;
	}

	/**
	 * svc-ID'sを設定します。
	 * @param svcIds svc-ID's
	 */
	public void setSvcIds(String[] svcIds) {
	    this.svcIds = svcIds;
	    super.append( LockSvcItems.svcId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(svcIds, LockSvcItems.svcId.getItemName(), false, true, false) );
	}

	/**
	 * lock svc-IDを取得します。
	 * @return lock svc-ID
	 */
	public String getLockSvcId() {
	    return lockSvcId;
	}

	/**
	 * svc-IDを設定します。
	 * @param lockSvcId lock svc-ID
	 */
	public void setLockSvcId(String lockSvcId) {
	    this.lockSvcId = lockSvcId;
	    super.append(LockSvcItems.lockSvcId, lockSvcId );
	}

	/**
	 * svc-ID'sを取得します。
	 * @return locksvc-ID's
	 */
	public String[] getLockSvcIds() {
	    return lockSvcIds;
	}

	/**
	 * svc-ID'sを設定します。
	 * @param lockSvcId locksvc-ID's
	 */
	public void setLockSvcIds(String[] lockSvcIds) {
	    this.lockSvcIds = lockSvcIds;
	    super.append( LockSvcItems.lockSvcId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(lockSvcIds, LockSvcItems.lockSvcId.getItemName(), false, true, false) );
	}

	/**
	 * lock for lotを取得します。
	 * @return lock for lot
	 */
	public StatusFlg getLockForLot() {
	    return lockForLot;
	}

	/**
	 * lock for lotを設定します。
	 * @param lockForLot lock for lot
	 */
	public void setLockForLot(StatusFlg lockForLot) {
	    this.lockForLot = lockForLot;
	    super.append( LockSvcItems.lockForLot, (null == lockForLot)? StatusFlg.off.parseBoolean(): lockForLot.parseBoolean() );
	}

	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	    super.append( LockSvcItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}
}