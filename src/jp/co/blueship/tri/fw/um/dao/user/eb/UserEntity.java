package jp.co.blueship.tri.fw.um.dao.user.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.EntityFooter;

/**
 * user entity.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public class UserEntity extends EntityFooter implements IUserEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * user-ID
	 */
	public String userId = null;
	/**
	 * user password
	 */
	public String userPass = null;
	/**
	 * password time limit
	 */
	public Timestamp passTimeLimit = null;
	/**
	 * user name
	 */
	public String userNm = null;
	/**
	 * e-mail address
	 */
	public String emailAddr = null;
	/**
	 * dept-ID
	 */
	public String deptId = null;
	/**
	 * dept. name
	 */
	public String deptNm = null;
	/**
	 * language
	 */
	public String lang = null;
	/**
	 * timezone
	 */
	public String timezone = null;

	private String userLocation = null;

	private String biography = null;

	private String iconTyp = null;

	private String defaultIconPath = null;

	private String customIconPath = null;

	private String genderTyp = null;

	public StatusFlg isReceiveMail = StatusFlg.off;

	private Timestamp lastLoginTimestamp = null;
	
	private String lookAndFeel = null;

	public String getUserId() {
	    return userId;
	}
	public void setUserId(String userId) {
	    this.userId = userId;
	}
	public String getUserPass() {
	    return userPass;
	}
	public void setUserPass(String userPass) {
	    this.userPass = userPass;
	}
	public Timestamp getPassTimeLimit() {
	    return passTimeLimit;
	}
	public void setPassTimeLimit(Timestamp passTimeLimit) {
	    this.passTimeLimit = passTimeLimit;
	}
	public String getUserNm() {
	    return userNm;
	}
	public void setUserNm(String userNm) {
	    this.userNm = userNm;
	}
	public String getEmailAddr() {
	    return emailAddr;
	}
	public void setEmailAddr(String emailAddr) {
	    this.emailAddr = emailAddr;
	}
	public String getDeptId() {
	    return deptId;
	}
	public void setDeptId(String deptId) {
	    this.deptId = deptId;
	}
	public String getDeptNm() {
	    return deptNm;
	}
	public void setDeptNm(String deptNm) {
	    this.deptNm = deptNm;
	}

	public String getLang() {
		return lang;
	}
	public void setLang(String lang) {
		this.lang = lang;
	}

	public String getTimeZone() {
		return timezone;
	}
	public void setTimeZone(String id) {
		this.timezone = id;
	}

	public String getUserLocation() {
		return userLocation;
	}

	public void setUserLocation(String location) {
		this.userLocation = location;
	}

	public String getBiography() {
		return biography;
	}

	public void setBiography(String biography) {
		this.biography = biography;
	}

	public String getIconTyp() {
		return iconTyp;
	}

	public void setIconTyp(String iconTyp) {
		this.iconTyp = iconTyp;
	}

	public String getDefaultIconPath() {
		return defaultIconPath;
	}

	public void setDefaultIconPath(String defaultIconPath) {
		this.defaultIconPath = defaultIconPath;
	}

	public String getCustomIconPath() {
		return customIconPath;
	}

	public void setCustomIconPath(String customIconPath) {
		this.customIconPath = customIconPath;
	}

	public String getGenderTyp() {
		return genderTyp;
	}

	public void setGenderTyp(String genderTyp) {
		this.genderTyp = genderTyp;
	}

	public StatusFlg getIsReceiveMail() {
		return isReceiveMail;
	}

	public void setIsReceiveMail(StatusFlg isReceiveMail) {
		this.isReceiveMail = isReceiveMail;
	}

	public Timestamp getLastLoginTimestamp() {
		return lastLoginTimestamp;
	}

	public void setLastLoginTimestamp(Timestamp lastLoginTimestamp) {
		this.lastLoginTimestamp = lastLoginTimestamp;
	}
	
	public String getLookAndFeel() {
		return lookAndFeel;
	}

	public void setLookAndFeel(String lookAndFeel) {
		this.lookAndFeel = lookAndFeel;
	}
}
