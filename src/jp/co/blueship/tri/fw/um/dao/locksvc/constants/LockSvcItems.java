package jp.co.blueship.tri.fw.um.dao.locksvc.constants;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;


/**
 * The items of the lock service entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public enum LockSvcItems implements ITableItem {
	svcId("svc_id"),
	lockSvcId("lock_svc_id"),
	lockForLot("lock_for_lot"),
	delStsId("del_sts_id"),
	regUserId("reg_user_id"),
	regUserNm("reg_user_nm"),
	regTimestamp("reg_timestamp"),
	updUserId("upd_user_id"),
	updUserNm("upd_user_nm"),
	updTimestamp("upd_timestamp");

	private String element = null;

	private LockSvcItems( String element) {
		this.element = element;
	}

	public String getItemName() {
		return this.element;
	}

}
