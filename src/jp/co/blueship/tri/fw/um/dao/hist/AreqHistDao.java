package jp.co.blueship.tri.fw.um.dao.hist;

import jp.co.blueship.tri.am.dao.areq.eb.AreqEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.dao.orm.ex.TriJdbcDaoException;
import jp.co.blueship.tri.fw.dao.oxm.utils.TriXmlUtils;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.schema.beans.areq.AreqRecDocument;
import jp.co.blueship.tri.fw.schema.beans.areq.AreqRecDocument.AreqRec;
import jp.co.blueship.tri.fw.schema.beans.areq.AreqType;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.utils.AreqHistMappingUtils;
import jp.co.blueship.tri.fw.um.dao.hist.utils.HistMappingUtils;

/**
 * The implements of the areq history DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 */
public class AreqHistDao extends HistDao implements IAreqHistDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	private static final String LS_NAMESPACES_KEY = "";
    private static final String LS_NAMESPACES_VAL = "http://www.blueship.co.jp/tri/fw/schema/beans/areq";

	public AreqHistDao() {
		super();
		this.addNameSpace(LS_NAMESPACES_KEY, LS_NAMESPACES_VAL);
	}

	public int insert(IHistEntity entity , IAreqDto areqDto){

		AreqRecDocument areqDoc = AreqRecDocument.Factory.newInstance();
		AreqType areqXml = areqDoc.addNewAreqRec();
		areqXml = new HistMappingUtils().mapDB2XMLBeans(areqDto,areqXml);

		entity.setHistId( this.getHistNumberingDao().nextval() );
		entity.setDataCtgCd( AmTables.AM_AREQ.name() );
		entity.setDataId( areqDto.getAreqEntity().getAreqId() );
		entity.setHistData( TriXmlUtils.replaceExcessString( areqDoc ) );
		entity.setDelStsId( StatusFlg.off );
		entity.setLotId( areqDto.getAreqEntity().getLotId() );
		entity.setStsId( areqDto.getAreqEntity().getStsId() );

		return super.insert( entity );
	}

	@Override
	public final IAreqEntity unmarshall( String xmlDate ) {
		try {
			IAreqEntity entity = new AreqEntity();

			if ( TriStringUtils.isEmpty(xmlDate) ) {
				return entity;
			}

			AreqRecDocument doc = AreqRecDocument.Factory.parse( xmlDate, this.getXmlOptions() );
			AreqRec xml = doc.getAreqRec();

			entity = new AreqHistMappingUtils().mapXMLBeans2DB(xml, entity);

			return entity;

		} catch (Exception e) {
			throw new TriJdbcDaoException(SmMessageId.SM005004S, e, this.getTableAttribute().name());
		}
	}

}