package jp.co.blueship.tri.fw.um.dao.hist;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.ICtgEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;


/**
 * @version V4.00.00
 * @author Akahoshi
 *
 */
public class CtgHistDao extends HistDao implements ICtgHistDao{

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	public int insert(IHistEntity entity, ICtgEntity ctgEntity) {

		entity.setHistId( this.getHistNumberingDao().nextval() );
		entity.setDataCtgCd( UmTables.UM_CTG.name() );
		entity.setDataId( ctgEntity.getCtgId() );
		entity.setHistData( "" );
		entity.setDelStsId( StatusFlg.off );
		entity.setLotId( ctgEntity.getLotId() );
		entity.setStsId( "" );

		return super.insert(entity);
	}

}
