package jp.co.blueship.tri.fw.um.dao.hist;

import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaDto;

/**
 * The implements of the Ra History DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 */
public interface IRaHistDao extends IHistDao {

	/**
	 * 登録処理を行います
	 *
	 * @param entity
	 * @return the number of rows affected1
	 */
	public int insert(IHistEntity entity , IRaDto raDto);
}
