package jp.co.blueship.tri.fw.um.dao.svcctg;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.fw.um.dao.svcctg.constants.SvcCtgSvcLnkItems;
import jp.co.blueship.tri.fw.um.dao.svcctg.eb.ISvcCtgSvcLnkEntity;
import jp.co.blueship.tri.fw.um.dao.svcctg.eb.SvcCtgSvcLnkEntity;

/**
 * The implements of the service catalog service link DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 */
public class SvcCtgSvcLnkDao extends JdbcBaseDao<ISvcCtgSvcLnkEntity> implements ISvcCtgSvcLnkDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected final ITableAttribute getTableAttribute() {
		return UmTables.UM_SVC_CTG_SVC_LNK;
	}

	@Override
	protected final ISqlBuilder append( ISqlBuilder builder, ISvcCtgSvcLnkEntity entity ) {
		builder
			.append(SvcCtgSvcLnkItems.svcId, entity.getSvcId(), true)
			.append(SvcCtgSvcLnkItems.svcCtgId, entity.getSvcCtgId(), true)
			.append(SvcCtgSvcLnkItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(SvcCtgSvcLnkItems.regTimestamp, entity.getRegTimestamp())
			.append(SvcCtgSvcLnkItems.regUserId, entity.getRegUserId())
			.append(SvcCtgSvcLnkItems.regUserNm, entity.getRegUserNm())
			.append(SvcCtgSvcLnkItems.updTimestamp, entity.getUpdTimestamp())
			.append(SvcCtgSvcLnkItems.updUserId, entity.getUpdUserId())
			.append(SvcCtgSvcLnkItems.updUserNm, entity.getUpdUserNm())
			;

		return builder;
	}

	@Override
	protected final ISvcCtgSvcLnkEntity entityMapping( ResultSet rs, int row ) throws SQLException {
  	  ISvcCtgSvcLnkEntity entity = new SvcCtgSvcLnkEntity();

  	  entity.setSvcId( rs.getString(SvcCtgSvcLnkItems.svcId.getItemName()) );
  	  entity.setSvcCtgId( rs.getString(SvcCtgSvcLnkItems.svcCtgId.getItemName()) );
  	  entity.setDelStsId( StatusFlg.value(rs.getBoolean(SvcCtgSvcLnkItems.delStsId.getItemName())) );
  	  entity.setRegTimestamp( rs.getTimestamp(SvcCtgSvcLnkItems.regTimestamp.getItemName()) );
  	  entity.setRegUserId( rs.getString(SvcCtgSvcLnkItems.regUserId.getItemName()) );
  	  entity.setRegUserNm( rs.getString(SvcCtgSvcLnkItems.regUserNm.getItemName()) );
  	  entity.setUpdTimestamp( rs.getTimestamp(SvcCtgSvcLnkItems.updTimestamp.getItemName()) );
  	  entity.setUpdUserId( rs.getString(SvcCtgSvcLnkItems.updUserId.getItemName()) );
  	  entity.setUpdUserNm( rs.getString(SvcCtgSvcLnkItems.updUserNm.getItemName()) );

  	  return entity;
	}

}
