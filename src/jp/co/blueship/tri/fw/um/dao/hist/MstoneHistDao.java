package jp.co.blueship.tri.fw.um.dao.hist;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.IMstoneEntity;


/**
 * @version V4.00.00
 * @author Akahoshi
 */
public class MstoneHistDao extends HistDao implements IMstoneHistDao{

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	public int insert(IHistEntity entity, IMstoneEntity mstoneEntity) {

		entity.setHistId( this.getHistNumberingDao().nextval() );
		entity.setDataCtgCd( UmTables.UM_MSTONE.name() );
		entity.setDataId( mstoneEntity.getMstoneId() );
		entity.setHistData( "" );
		entity.setDelStsId( StatusFlg.off );
		entity.setLotId( mstoneEntity.getLotId() );
		entity.setStsId( "" );

		return super.insert(entity);
	}

}
