package jp.co.blueship.tri.fw.um.dao.grp.eb;

import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the group role link entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IGrpRoleLnkEntity extends IEntityFooter {

	public String getGrpId();
	public void setGrpId(String grpId);

	public String getRoleId();
	public void setRoleId(String roleId);
}
