package jp.co.blueship.tri.fw.um.dao.hist.utils;

import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.fw.dao.oxm.utils.TriXmlMappingUtils;
import jp.co.blueship.tri.fw.schema.beans.areq.AreqRecDocument.AreqRec;
import jp.co.blueship.tri.fw.schema.beans.areq.AreqType;

/**
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class AreqHistMappingUtils extends TriXmlMappingUtils {

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param x 複写元のXMLBeans
	 * @param d 複写先のDBのEntity
	 * @return マッピング結果を戻します。
	 */
	public final IAreqEntity mapXMLBeans2DB( AreqRec x , IAreqEntity d ) {
		return this.mapXMLBeans2DB( (AreqType)x, d );

	}

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param x 複写元のXMLBeans
	 * @param d 複写先のDBのEntity
	 * @return マッピング結果を戻します。
	 */
	public final IAreqEntity mapXMLBeans2DB( AreqType x , IAreqEntity d ) {
		copyPropertiesOfHistData(x, d);

		return d;
	}

}
