package jp.co.blueship.tri.fw.um.dao.svcctg;

import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;
import jp.co.blueship.tri.fw.um.dao.svcctg.eb.ISvcCtgEntity;


/**
 * The interface of the service catalog DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface ISvcCtgDao extends IJdbcDao<ISvcCtgEntity> {

}
