package jp.co.blueship.tri.fw.um.dao.user;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.fw.um.dao.user.constants.UserRoleLnkItems;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserRoleLnkEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserRoleLnkEntity;

/**
 * The implements of the user role link DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 */
public class UserRoleLnkDao extends JdbcBaseDao<IUserRoleLnkEntity> implements IUserRoleLnkDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected final ITableAttribute getTableAttribute() {
		return UmTables.UM_USER_ROLE_LNK;
	}

	@Override
	protected final ISqlBuilder append( ISqlBuilder builder, IUserRoleLnkEntity entity ) {
		builder
			.append(UserRoleLnkItems.userId, entity.getUserId(), true)
			.append(UserRoleLnkItems.roleId, entity.getRoleId(), true)
			.append(UserRoleLnkItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(UserRoleLnkItems.regTimestamp, entity.getRegTimestamp())
			.append(UserRoleLnkItems.regUserId, entity.getRegUserId())
			.append(UserRoleLnkItems.regUserNm, entity.getRegUserNm())
			.append(UserRoleLnkItems.updTimestamp, entity.getUpdTimestamp())
			.append(UserRoleLnkItems.updUserId, entity.getUpdUserId())
			.append(UserRoleLnkItems.updUserNm, entity.getUpdUserNm())
			;

		return builder;
	}

	@Override
	protected final IUserRoleLnkEntity entityMapping( ResultSet rs, int row ) throws SQLException {
  	  IUserRoleLnkEntity entity = new UserRoleLnkEntity();

  	  entity.setUserId( rs.getString(UserRoleLnkItems.userId.getItemName()) );
  	  entity.setRoleId( rs.getString(UserRoleLnkItems.roleId.getItemName()) );
  	  entity.setDelStsId( StatusFlg.value(rs.getBoolean(UserRoleLnkItems.delStsId.getItemName())) );
  	  entity.setRegTimestamp( rs.getTimestamp(UserRoleLnkItems.regTimestamp.getItemName()) );
  	  entity.setRegUserId( rs.getString(UserRoleLnkItems.regUserId.getItemName()) );
  	  entity.setRegUserNm( rs.getString(UserRoleLnkItems.regUserNm.getItemName()) );
  	  entity.setUpdTimestamp( rs.getTimestamp(UserRoleLnkItems.updTimestamp.getItemName()) );
  	  entity.setUpdUserId( rs.getString(UserRoleLnkItems.updUserId.getItemName()) );
  	  entity.setUpdUserNm( rs.getString(UserRoleLnkItems.updUserNm.getItemName()) );

  	  return entity;
	}

}
