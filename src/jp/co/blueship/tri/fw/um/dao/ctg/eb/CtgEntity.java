package jp.co.blueship.tri.fw.um.dao.ctg.eb;

import jp.co.blueship.tri.fw.dao.orm.EntityFooter;

/**
 * category entity.
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 */
public class CtgEntity extends EntityFooter implements ICtgEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * category-id
	 */
	public String ctgId = null;

	/**
	 * category name
	 */
	public String ctgNm = null;

	/**
	 * lot-id
	 */
	public String lotId = null;

	/**
	 * sort order
	 */
	public Integer sortOdr = null;

	@Override
	public String getCtgId() {
		return ctgId;
	}
	@Override
	public void setCtgId(String ctgId) {
		this.ctgId = ctgId;
	}

	@Override
	public String getCtgNm() {
		return ctgNm;
	}
	@Override
	public void setCtgNm(String ctgNm) {
		this.ctgNm = ctgNm;
	}

	@Override
	public String getLotId() {
		return lotId;
	}
	@Override
	public void setLotId(String lotId) {
		this.lotId = lotId;
	}

	@Override
	public Integer getSortOdr() {
		return sortOdr;
	}
	@Override
	public void setSortOdr(Integer sortOdr) {
		this.sortOdr = sortOdr;
	}

}
