package jp.co.blueship.tri.fw.um.dao.hist;

import jp.co.blueship.tri.dm.dao.dmdo.eb.IDmDoDto;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;

public interface IDmDoHistDao extends IHistDao {


	/**
	 * 登録処理を行います
	 *
	 * @param entity
	 * @return the number of rows affected1
	 */
	public int insert(IHistEntity entity , IDmDoDto dmDoDto);
}
