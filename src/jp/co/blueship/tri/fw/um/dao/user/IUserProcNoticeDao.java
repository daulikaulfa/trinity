package jp.co.blueship.tri.fw.um.dao.user;

import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserProcNoticeEntity;


/**
 * The interface of the user proc notice DAO.
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 */
public interface IUserProcNoticeDao extends IJdbcDao<IUserProcNoticeEntity> {

}