package jp.co.blueship.tri.fw.um.dao.filter;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.fw.um.dao.filter.constants.FilterItems;
import jp.co.blueship.tri.fw.um.dao.filter.eb.FilterEntity;
import jp.co.blueship.tri.fw.um.dao.filter.eb.IFilterEntity;

/**
 * The interface of the filter DAO.
 *
 * @version V4.00.00
 * @author Yohei Tanaka
 */
public class FilterDao extends JdbcBaseDao<IFilterEntity> implements IFilterDao {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected ITableAttribute getTableAttribute() {
		return UmTables.UM_FILTER;
	}

	@Override
	protected final ISqlBuilder append( ISqlBuilder builder, IFilterEntity entity ) {

		builder
		.append(FilterItems.filterId, entity.getFilterId(), true)
		.append(FilterItems.filterNm, entity.getFilterNm())
		.append(FilterItems.filterData, entity.getFilterData())
		.append(FilterItems.lotId, entity.getLotId())
		.append(FilterItems.userId, entity.getUserId())
		.append(FilterItems.svcId, entity.getSvcId())
		.append(FilterItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
		.append(FilterItems.regTimestamp, entity.getRegTimestamp())
		.append(FilterItems.regUserId, entity.getRegUserId())
		.append(FilterItems.regUserNm, entity.getRegUserNm())
		.append(FilterItems.updTimestamp, entity.getUpdTimestamp())
		.append(FilterItems.updUserId, entity.getUpdUserId())
		.append(FilterItems.updUserNm, entity.getUpdUserNm())
		;

		return builder;
	}

	@Override
	protected final IFilterEntity entityMapping( ResultSet rs, int row ) throws SQLException {
		FilterEntity entity = new FilterEntity();

		entity.setFilterId( rs.getString(FilterItems.filterId.getItemName()) );
		entity.setFilterNm( rs.getString(FilterItems.filterNm.getItemName()) );
		entity.setFilterData( rs.getString(FilterItems.filterData.getItemName()) );
		entity.setLotId( rs.getString(FilterItems.lotId.getItemName()) );
		entity.setUserId( rs.getString(FilterItems.userId.getItemName()) );
		entity.setSvcId( rs.getString(FilterItems.svcId.getItemName()) );
		entity.setDelStsId( StatusFlg.value(rs.getBoolean(FilterItems.delStsId.getItemName())) );
		entity.setRegUserId( rs.getString(FilterItems.regUserId.getItemName()) );
		entity.setRegUserNm( rs.getString(FilterItems.regUserNm.getItemName()) );
		entity.setRegTimestamp( rs.getTimestamp(FilterItems.regTimestamp.getItemName()) );
		entity.setUpdUserId( rs.getString(FilterItems.updUserId.getItemName()) );
		entity.setUpdUserNm( rs.getString(FilterItems.updUserNm.getItemName()) );
		entity.setUpdTimestamp( rs.getTimestamp(FilterItems.updTimestamp.getItemName()) );

		return entity;
	}

}