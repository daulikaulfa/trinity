package jp.co.blueship.tri.fw.um.dao.mstone.constants;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;
/**
 * The items of the milestone link entity.
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 */
public enum MstoneLnkItems implements ITableItem {
	mstoneId("mstone_id"),
	dataCtgCd("data_ctg_cd"),
	dataId("data_id"),
	delStsId("del_sts_id"),
	regUserId("reg_user_id"),
	regUserNm("reg_user_nm"),
	regTimestamp("reg_timestamp"),
	updUserId("upd_user_id"),
	updUserNm("upd_user_nm"),
	updTimestamp("upd_timestamp");

	private String element = null;

	private MstoneLnkItems( String element ) {
		this.element = element;
	}

	@Override
	public String getItemName() {
		return element;
	}

}
