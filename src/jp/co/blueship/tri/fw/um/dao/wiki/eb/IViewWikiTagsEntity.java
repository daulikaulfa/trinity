package jp.co.blueship.tri.fw.um.dao.wiki.eb;

import jp.co.blueship.tri.fw.dao.orm.IEntity;

public interface IViewWikiTagsEntity extends IEntity {
	/**
	 * Gets the string of tag name.
	 * <br>
	 * <br>タグ名を取得します.
	 *
	 * @return Tag Name
	 */
	public String getTagNm();
	public void setTagNm(String tagNm);
	/**
	 * Gets the string of lot id.<br>
	 * <br>
	 * ロットIDを取得します
	 *
	 * @return Lot Id
	 */
	public String getLotId();
	public void setLotId(String lotId);
	/**
	 * Gets the number of tabs that are used.
	 * <br>
	 * <br>使用されているタグ数を取得します.
	 *
	 * @return Count of tags
	 */
	public Integer count();
	public void setCount(Integer count);
}
