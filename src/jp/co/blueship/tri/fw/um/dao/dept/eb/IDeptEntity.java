package jp.co.blueship.tri.fw.um.dao.dept.eb;

import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the department entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 * 
 * @version V4.00.00
 * @author le.thixuan
 */
public interface IDeptEntity extends IEntityFooter {

	public String getDeptId();
	public void setDeptId(String deptId);

	public String getDeptNm();
	public void setDeptNm(String deptNm);
	
	/**
	 * sort order
	 * @return sort order
	 */
	public Integer getSortOdr();
	/**
	 * sort order
	 * @param sortOdr sort order
	 */
	public void setSortOdr(Integer sortOdr);

}
