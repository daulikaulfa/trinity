package jp.co.blueship.tri.fw.um.dao.project.constants;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;

/**
 * The interface of the project entity.
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 */
public enum ProjectItems implements ITableItem {
	productId("product_id"),
	projectNm("project_nm"),
	iconTyp("icon_typ"),
	defaultIconPath("default_icon_path"),
	customIconPath("custom_icon_path"),
	lang("lang"),
	timezone("timezone"),
	notice("notice"),
	noticeTimestamp("notice_timestamp"),
	delStsId("del_sts_id"),
	regUserId("reg_user_id"),
	regUserNm("reg_user_nm"),
	regTimestamp("reg_timestamp"),
	updUserId("upd_user_id"),
	updUserNm("upd_user_nm"),
	updTimestamp("upd_timestamp"),
	lookAndFeel("look_and_feel"),
	;

	private String element = null;

	private ProjectItems (String element) {
		this.element = element;
	}

	public String getItemName() {
		return this.element;
	}

}
