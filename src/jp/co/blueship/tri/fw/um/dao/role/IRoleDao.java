package jp.co.blueship.tri.fw.um.dao.role;

import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;
import jp.co.blueship.tri.fw.um.dao.role.eb.IRoleEntity;


/**
 * The interface of the role DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IRoleDao extends IJdbcDao<IRoleEntity> {

}
