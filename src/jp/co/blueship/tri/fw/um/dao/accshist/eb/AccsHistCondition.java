package jp.co.blueship.tri.fw.um.dao.accshist.eb;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.um.dao.accshist.constants.AccsHistItems;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;

/**
 * The SQL condition of the access history entity.
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 */
public class AccsHistCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute hist = UmTables.UM_ACCS_HIST;

	/**
	 * user-ID
	 */
	public String userId = null;
	/**
	 * user name
	 */
	public String userNm = null;
	/**
	 * data catalog code
	 */
	public String dataCtgCd = null;
	/**
	 * data-ID
	 */
	public String dataId = null;
	/**
	 * history data
	 */
	public String histData = null;
	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		   super.append(AccsHistItems.delStsId, StatusFlg.off.parseBoolean() );
	}

	public AccsHistCondition(){
		super(hist);
	}

	public String getUserId() {
	    return userId;
	}
	public void setUserId(String userId) {
	    this.userId = userId;
	    super.append( AccsHistItems.userId, userId );
	}

	public String getUserNm() {
	    return userNm;
	}
	public void setUserNm(String userNm) {
	    this.userNm = userNm;
	    super.append( AccsHistItems.userNm, userNm );
	}

	public String getDataCtgCd() {
	    return dataCtgCd;
	}

	public void setDataCtgCd(String dataCtgCd) {
	    this.dataCtgCd = dataCtgCd;
	    super.append(AccsHistItems.dataCtgCd, dataCtgCd );
	}

	public String getDataId() {
	    return dataId;
	}

	public void setDataId(String dataId) {
	    this.dataId = dataId;
	    super.append(AccsHistItems.dataId, dataId );
	}

	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	    super.append( AccsHistItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}
}