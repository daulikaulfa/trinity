package jp.co.blueship.tri.fw.um.dao.user.eb;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.dao.orm.psql.SqlFormatUtils;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.fw.um.dao.user.constants.UserPassHistItems;

/**
 * The SQL condition of the user password history entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class UserPassHistCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute userpasshist = UmTables.UM_USER_PASS_HIST;

	/**
	 * user-ID
	 */
	public String userId = null;
	/**
	 * user-ID's
	 */
	public String[] userIds = null;
	/**
	 * password history sequential number
	 */
	public String passHistSeqNo = null;
	/**
	 * user password
	 */
	public String userPass = null;
	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		   super.append(UserPassHistItems.delStsId, StatusFlg.off.parseBoolean() );
	}

	public UserPassHistCondition(){
		super(userpasshist);
	}

	/**
	 * user-IDを取得します。
	 * @return user-ID
	 */
	public String getUserId() {
	    return userId;
	}

	/**
	 * user-IDを設定します。
	 * @param userId user-ID
	 */
	public void setUserId(String userId) {
	    this.userId = userId;
	    super.append(UserPassHistItems.userId, userId );
	}

	/**
	 * user-ID'sを取得します。
	 * @return user-ID's
	 */
	public String[] getUserIds() {
	    return userIds;
	}

	/**
	 * user-ID'sを設定します。
	 * @param userIds user-ID's
	 */
	public void setUserIds(String[] userIds) {
	    this.userIds = userIds;
	    super.append( UserPassHistItems.userId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(userIds, UserPassHistItems.userId.getItemName(), false, true, false) );
	}

	/**
	 * password history sequential numberを取得します。
	 * @return password history sequential number
	 */
	public String getPassHistSeqNo() {
	    return passHistSeqNo;
	}

	/**
	 * password history sequential numberを設定します。
	 * @param passHistSeqNo password-history-sequential-number
	 */
	public void setPassHistSeqNo(String passHistSeqNo) {
	    this.passHistSeqNo = passHistSeqNo;
	    super.append(UserPassHistItems.passHistSeqNo, passHistSeqNo );
	}

	/**
	 * user passwordを取得します。
	 * @return user password
	 */
	public String getUserPass() {
	    return userPass;
	}

	/**
	 * user passwordを設定します。
	 * @param userPass user-password
	 */
	public void setUserPass(String userPass) {
	    this.userPass = userPass;
	    super.append(UserPassHistItems.userPass, userPass );
	}

	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	    super.append( UserPassHistItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}
}