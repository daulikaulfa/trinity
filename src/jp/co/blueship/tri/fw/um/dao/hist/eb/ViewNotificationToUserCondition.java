package jp.co.blueship.tri.fw.um.dao.hist.eb;

import java.sql.Timestamp;

/**
 * The SQL condition.
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 */
public class ViewNotificationToUserCondition {


	String histId = null;
	String lotId = null;
	String actStsId = null;
	String stsId = null;
	String dataCtgCd = null;

	String[] lotIds = null;
	String[] actStsIds = null;
	String[] stsIds = null;

	Timestamp regTimestampFrom = null;
	Timestamp regTimestampTo = null;

	public String getHistId(){
		return this.histId;
	}
	public void setHistId(String histId){
		this.histId = histId;
	}

	public String getLotId(){
		return this.lotId;
	}
	public void setLotId(String lotId){
		this.lotId = lotId;
	}

	public String getActStsId(){
		return this.actStsId;
	}
	public void setActStsId(String actStsId){
		this.actStsId = actStsId;
	}

	public String getStsId(){
		return this.stsId;
	}
	public void setStsId(String stsId){
		this.stsId = stsId;
	}

	public String getDataCtgCd(){
		return this.dataCtgCd;
	}
	public void setDataCtgCd(String dataCtgCd){
		this.dataCtgCd = dataCtgCd;
	}


	public String[] getLotIds(){
		return this.lotIds;
	}
	public void setLotIds(String[] lotIds){
		this.lotIds = lotIds;
	}

	public String[] getActStsIds(){
		return this.actStsIds;
	}
	public void setActStsIds(String[] actStsIds){
		this.actStsIds = actStsIds;
	}

	public String[] getStsIds(){
		return this.stsIds;
	}
	public void setStsIds(String[] stsIds){
		this.stsIds = stsIds;
	}


	public boolean isRegTimestampFromToSearch(){
		return (this.regTimestampFrom != null) || (this.regTimestampTo != null);
	}
	public Timestamp getRegTimestampFrom(){
		return this.regTimestampFrom;
	}
	public Timestamp getRegTimestampTo(){
		return this.regTimestampTo;
	}
	public void setRegTimestampFromTo(Timestamp from, Timestamp to){
		this.regTimestampFrom = from;
		this.regTimestampTo = to;
	}
}
