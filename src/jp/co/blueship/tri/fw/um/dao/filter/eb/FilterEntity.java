package jp.co.blueship.tri.fw.um.dao.filter.eb;

import jp.co.blueship.tri.fw.dao.orm.EntityFooter;

/**
 * filter entity.
 *
 * @version V4.00.00
 * @author Yohei Tanaka
 *
 */
public class FilterEntity extends EntityFooter implements IFilterEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * filter id
	 */
	public String filterId = null;

	/**
	 * filter nm
	 */
	public String filterNm = null;

	/**
	 * filter data
	 */
	public String filterData = null;

	/**
	 * lot id
	 */
	public String lotId = null;

	/**
	 * user id
	 */
	public String userId = null;

	/**
	 * svc id
	 */
	public String svcId = null;

	@Override
	public String getFilterId() {
		return filterId;
	}
	@Override
	public void setFilterId(String filterId) {
		this.filterId = filterId;
	}
	@Override
	public String getFilterNm() {
		return filterNm;
	}
	@Override
	public void setFilterNm(String filterNm) {
		this.filterNm = filterNm;
	}
	@Override
	public String getFilterData() {
		return filterData;
	}
	@Override
	public void setFilterData(String filterData) {
		this.filterData = filterData;
	}
	@Override
	public String getLotId() {
		return lotId;
	}
	@Override
	public void setLotId(String lotId) {
		this.lotId = lotId;
	}
	@Override
	public String getUserId() {
		return userId;
	}
	@Override
	public void setUserId(String userId) {
		this.userId = userId;
	}
	@Override
	public String getSvcId() {
		return svcId;
	}
	@Override
	public void setSvcId(String svcId) {
		this.svcId = svcId;
	}
}
