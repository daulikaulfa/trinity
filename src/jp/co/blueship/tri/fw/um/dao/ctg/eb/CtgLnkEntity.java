package jp.co.blueship.tri.fw.um.dao.ctg.eb;

import jp.co.blueship.tri.fw.dao.orm.EntityFooter;

/**
 * category link entity.
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 */
public class CtgLnkEntity extends EntityFooter implements ICtgLnkEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * category-id
	 */
	public String ctgId = null;

	/**
	 * data category cd
	 */
	public String dataCtgCd = null;

	/**
	 * data id
	 */
	public String dataId = null;

	@Override
	public String getCtgId() {
		return ctgId;
	}
	@Override
	public void setCtgId(String categoryId) {
		this.ctgId = categoryId;
	}

	@Override
	public String getDataCtgCd() {
		return dataCtgCd;
	}
	@Override
	public void setDataCtgCd(String dataCtgCd) {
		this.dataCtgCd = dataCtgCd;
	}

	@Override
	public String getDataId() {
		return this.dataId;
	}
	@Override
	public void setDataId(String dataId) {
		this.dataId = dataId;
	}

}
