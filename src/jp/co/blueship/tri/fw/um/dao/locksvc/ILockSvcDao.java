package jp.co.blueship.tri.fw.um.dao.locksvc;

import java.util.List;

import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;
import jp.co.blueship.tri.fw.um.dao.locksvc.eb.ILockSvcEntity;


/**
 * The interface of the lock service DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface ILockSvcDao extends IJdbcDao<ILockSvcEntity> {

    /**
     * 指定されたサービスIDに対応する「同時実行を禁止するサービス」に行ロックを行います。
     *
     * @param svcId サービスID
     * @return
     */
    List<ILockSvcEntity> lock( String svcId );

}
