package jp.co.blueship.tri.fw.um.dao.hist;

import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.IMstoneEntity;

/**
 * @version V4.00.00
 * @author Akahoshi
 *
 */
public interface IMstoneHistDao extends IHistDao {

	/**
	 * 登録処理を行います
	 *
	 * @param entity
	 * @param mstoneEntity
	 * @return the number of rows affected1
	 */
	public int insert(IHistEntity entity , IMstoneEntity mstoneEntity);
}
