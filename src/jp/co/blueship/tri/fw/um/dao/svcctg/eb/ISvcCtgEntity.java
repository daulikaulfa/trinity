package jp.co.blueship.tri.fw.um.dao.svcctg.eb;

import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the service catalog entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public interface ISvcCtgEntity extends IEntityFooter {

	public String getSvcCtgId();
	public void setSvcCtgId(String svcCtgId);

	public String getSvcCtgNm();
	public void setSvcCtgNm(String svcCtgNm);

	public String getSvcCtgContent();
	public void setSvcCtgContent(String svcCtgContent);

	public String getSvcCtgPath();
	public void setSvcCtgPath(String svcCtgPath);

	public Integer getSortOdr();
	public void setSortOdr(Integer sortOdr);

}
