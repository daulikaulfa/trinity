package jp.co.blueship.tri.fw.um.dao.ctg;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.fw.um.dao.ctg.constants.CtgLnkItems;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.CtgLnkEntity;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.ICtgLnkEntity;

/**
 * The implements of the category link DAO.
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 */
public class CtgLnkDao extends JdbcBaseDao<ICtgLnkEntity> implements ICtgLnkDao {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected ITableAttribute getTableAttribute() {
		return UmTables.UM_CTG_LNK;
	}

	@Override
	protected ISqlBuilder append(ISqlBuilder builder, ICtgLnkEntity entity) {
		builder
			.append(CtgLnkItems.ctgId, entity.getCtgId())
			.append(CtgLnkItems.dataCtgCd, entity.getDataCtgCd(), true)
			.append(CtgLnkItems.dataId, entity.getDataId(), true)
			.append(CtgLnkItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(CtgLnkItems.regTimestamp, entity.getRegTimestamp())
			.append(CtgLnkItems.regUserId, entity.getRegUserId())
			.append(CtgLnkItems.regUserNm, entity.getRegUserNm())
			.append(CtgLnkItems.updTimestamp, entity.getUpdTimestamp())
			.append(CtgLnkItems.updUserId, entity.getUpdUserId())
			.append(CtgLnkItems.updUserNm, entity.getUpdUserNm())
			;
		return builder;
	}

	@Override
	protected ICtgLnkEntity entityMapping(ResultSet rs, int row) throws SQLException {
		CtgLnkEntity entity = new CtgLnkEntity();

		entity.setCtgId( rs.getString(CtgLnkItems.ctgId.getItemName()) );
		entity.setDataCtgCd( rs.getString(CtgLnkItems.dataCtgCd.getItemName()) );
		entity.setDataId( rs.getString(CtgLnkItems.dataId.getItemName()) );
		entity.setDelStsId( StatusFlg.value(rs.getBoolean(CtgLnkItems.delStsId.getItemName())) );
		entity.setRegUserId( rs.getString(CtgLnkItems.regUserId.getItemName()) );
		entity.setRegUserNm( rs.getString(CtgLnkItems.regUserNm.getItemName()) );
		entity.setRegTimestamp( rs.getTimestamp(CtgLnkItems.regTimestamp.getItemName()) );
		entity.setUpdUserId( rs.getString(CtgLnkItems.updUserId.getItemName()) );
		entity.setUpdUserNm( rs.getString(CtgLnkItems.updUserNm.getItemName()) );
		entity.setUpdTimestamp( rs.getTimestamp(CtgLnkItems.updTimestamp.getItemName()) );

		return entity;
	}

}
