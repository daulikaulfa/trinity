package jp.co.blueship.tri.fw.um.dao.dept.eb;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.dao.orm.psql.SqlFormatUtils;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.fw.um.dao.dept.constants.DeptItems;

/**
 * The SQL condition of the department entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class DeptCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute dept = UmTables.UM_DEPT;

	/**
	 * dept-ID
	 */
	public String deptId = null;
	/**
	 * dept-ID's
	 */
	public String[] deptIds = null;
	/**
	 * dept. name
	 */
	public String deptNm = null;
	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		   super.append(DeptItems.delStsId, StatusFlg.off.parseBoolean() );
	}

	public DeptCondition(){
		super(dept);
	}

	/**
	 * dept-IDを取得します。
	 * @return dept-ID
	 */
	public String getDeptId() {
	    return deptId;
	}

	/**
	 * dept-IDを設定します。
	 * @param deptId dept-ID
	 */
	public void setDeptId(String deptId) {
	    this.deptId = deptId;
	    super.append(DeptItems.deptId, deptId );
	}

	/**
	 * dept-ID'sを取得します。
	 * @return dept-ID's
	 */
	public String[] getDeptIds() {
	    return deptIds;
	}

	/**
	 * dept-ID'sを設定します。
	 * @param deptIds dept-ID's
	 */
	public void setDeptIds(String[] deptIds) {
	    this.deptIds = deptIds;
	    super.append( DeptItems.deptId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(deptIds, DeptItems.deptId.getItemName(), false, true, false) );
	}

	/**
	 * dept. nameを取得します。
	 * @return dept. name
	 */
	public String getDeptNm() {
	    return deptNm;
	}

	/**
	 * dept. nameを設定します。
	 * @param deptNm dept. name
	 */
	public void setDeptNm(String deptNm) {
	    this.deptNm = deptNm;
	    super.append( DeptItems.deptNm, deptNm );
	}

	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	    super.append( DeptItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}

	public String[] getDeptIdsByNotEquals() {
		return deptIds;
	}

	public void setDeptIdsByNotEquals(String... deptIds) {
		this.deptIds = deptIds;
		super.append(DeptItems.deptId.getItemName() + "[]",
				SqlFormatUtils.getCondition(deptIds, DeptItems.deptId.getItemName(), false, false, true));
	}
}