package jp.co.blueship.tri.fw.um.dao.user.eb;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.dao.orm.psql.SqlFormatUtils;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.fw.um.dao.user.constants.UserRoleLnkItems;

/**
 * The SQL condition of the user role link entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class UserRoleLnkCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute user = UmTables.UM_USER_ROLE_LNK;

	/**
	 * user-ID
	 */
	public String userId = null;
	/**
	 * user-ID's
	 */
	public String[] userIds = null;
	/**
	 * role-ID
	 */
	public String roleId = null;
	/**
	 * role-ID's
	 */
	public String[] roleIds = null;
	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		   super.append(UserRoleLnkItems.delStsId, StatusFlg.off.parseBoolean() );
	}

	public UserRoleLnkCondition(){
		super(user);
	}

	/**
	 * user-IDを取得します。
	 * @return user-ID
	 */
	public String getUserId() {
	    return userId;
	}

	/**
	 * user-IDを設定します。
	 * @param userId user-ID
	 */
	public void setUserId(String userId) {
	    this.userId = userId;
	    super.append(UserRoleLnkItems.userId, userId );
	}

	/**
	 * user-ID'sを取得します。
	 * @return user-ID's
	 */
	public String[] getUserIds() {
	    return userIds;
	}

	/**
	 * user-ID'sを設定します。
	 * @param userIds user-ID's
	 */
	public void setUserIds(String[] userIds) {
	    this.userIds = userIds;
	    super.append( UserRoleLnkItems.userId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(userIds, UserRoleLnkItems.userId.getItemName(), false, true, false) );
	}

	/**
	 * role-IDを取得します。
	 * @return role-ID
	 */
	public String getRoleId() {
	    return roleId;
	}

	/**
	 * role-IDを設定します。
	 * @param roleId role-ID
	 */
	public void setRoleId(String roleId) {
	    this.roleId = roleId;
	    super.append(UserRoleLnkItems.roleId, roleId );
	}

	/**
	 * role-ID'sを取得します。
	 * @return role-ID's
	 */
	public String[] getRoleIds() {
	    return roleIds;
	}

	/**
	 * role-ID'sを設定します。
	 * @param roleIds role-ID's
	 */
	public void setRoleIds(String[] roleIds) {
	    this.roleIds = roleIds;
	    super.append( UserRoleLnkItems.roleId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(roleIds, UserRoleLnkItems.roleId.getItemName(), false, true, false) );
	}

	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	    super.append( UserRoleLnkItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}
}