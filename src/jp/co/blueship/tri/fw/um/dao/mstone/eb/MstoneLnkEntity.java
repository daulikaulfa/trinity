package jp.co.blueship.tri.fw.um.dao.mstone.eb;

import jp.co.blueship.tri.fw.dao.orm.EntityFooter;

/**
 * milestone link entity.
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 */
public class MstoneLnkEntity extends EntityFooter implements IMstoneLnkEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * milestone-id
	 */
	public String mstoneId = null;

	/**
	 * data category cd
	 */
	public String dataCtgCd = null;

	/**
	 * data id
	 */
	public String dataId = null;

	@Override
	public String getMstoneId() {
		return mstoneId;
	}
	@Override
	public void setMstoneId(String mstoneId) {
		this.mstoneId = mstoneId;
	}
	@Override
	public String getDataCtgCd() {
		return dataCtgCd;
	}
	@Override
	public void setDataCtgCd(String dataCtgCd) {
		this.dataCtgCd = dataCtgCd;
	}
	@Override
	public String getDataId() {
		return this.dataId;
	}
	@Override
	public void setDataId(String dataId) {
		this.dataId = dataId;
	}

}
