package jp.co.blueship.tri.fw.um.dao.filter.eb;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.dao.orm.psql.SqlFormatUtils;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.fw.um.dao.filter.constants.FilterItems;

/**
 *
 * @version V4.00.00
 * @author Yuhei Suzuki
 *
 */
public class FilterCondition extends ConditionSupport{

	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute filter = UmTables.UM_FILTER;

	public String filterId = null;
	public String filterNm = null;
	public String filterData = null;
	public String userId = null;
	public String svcId = null;
	public StatusFlg delStsId = StatusFlg.off;
	public String keyword = null;
	{
		super.append(FilterItems.delStsId, StatusFlg.off.parseBoolean());
	}
	public String regUserId = null;

	public FilterCondition() {
		super(filter);
	}

	public String getFilterId() {
		return filterId;
	}
	public void setFilterId(String filterId) {
		this.filterId = filterId;
		super.append(FilterItems.filterId, filterId);
	}

	public String getFilterNm() {
		return filterNm;
	}
	public void setFilterNm(String filterNm) {
		this.filterNm = filterNm;
		super.append(FilterItems.filterNm, filterNm);
	}

	public String getFilterData() {
		return filterData;
	}
	public void setFilterData(String filterData) {
		this.filterData = filterData;
		super.append(FilterItems.filterData, filterData);
	}

	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
		super.append(FilterItems.userId, userId);
	}

	public String getSvcId() {
		return svcId;
	}
	public void setSvcId(String svcId) {
		this.svcId = svcId;
		super.append(FilterItems.svcId, svcId);
	}

	public String getRegUserId() {
		return regUserId;
	}
	public void setRegUserId(String regUserId) {
		this.regUserId = regUserId;
		super.append(FilterItems.regUserId, regUserId);
	}

	public void setContainsByKeyword(String keyword){
		if ( TriStringUtils.isEmpty(keyword) ){
			return;
		}

		this.keyword = keyword;
		super.append(FilterItems.filterNm.getItemName() + "Other",
				SqlFormatUtils.joinCondition( true,
						new String[]{
								SqlFormatUtils.getCondition(this.keyword, FilterItems.filterNm.getItemName(), true, false, false),
						}));
	}

}
