package jp.co.blueship.tri.fw.um.dao.dept;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.fw.um.dao.dept.constants.DeptItems;
import jp.co.blueship.tri.fw.um.dao.dept.eb.DeptEntity;
import jp.co.blueship.tri.fw.um.dao.dept.eb.IDeptEntity;

/**
 * The implements of the department DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 */
public class DeptDao extends JdbcBaseDao<IDeptEntity> implements IDeptDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected final ITableAttribute getTableAttribute() {
		return UmTables.UM_DEPT;
	}

	@Override
	protected final ISqlBuilder append( ISqlBuilder builder, IDeptEntity entity ) {
		builder
			.append(DeptItems.deptId, entity.getDeptId(), true)
			.append(DeptItems.deptNm, entity.getDeptNm())
			.append(DeptItems.sortOdr, entity.getSortOdr())
			.append(DeptItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(DeptItems.regTimestamp, entity.getRegTimestamp())
			.append(DeptItems.regUserId, entity.getRegUserId())
			.append(DeptItems.regUserNm, entity.getRegUserNm())
			.append(DeptItems.updTimestamp, entity.getUpdTimestamp())
			.append(DeptItems.updUserId, entity.getUpdUserId())
			.append(DeptItems.updUserNm, entity.getUpdUserNm())
			;

		return builder;
	}

	@Override
	protected final IDeptEntity entityMapping( ResultSet rs, int row ) throws SQLException {
  	  IDeptEntity entity = new DeptEntity();

  	  entity.setDeptId( rs.getString(DeptItems.deptId.getItemName()) );
  	  entity.setDeptNm( rs.getString(DeptItems.deptNm.getItemName()) );
  	  entity.setSortOdr( rs.getInt(DeptItems.sortOdr.getItemName()) );
  	  entity.setDelStsId( StatusFlg.value(rs.getBoolean(DeptItems.delStsId.getItemName())) );
  	  entity.setRegTimestamp( rs.getTimestamp(DeptItems.regTimestamp.getItemName()) );
  	  entity.setRegUserId( rs.getString(DeptItems.regUserId.getItemName()) );
  	  entity.setRegUserNm( rs.getString(DeptItems.regUserNm.getItemName()) );
  	  entity.setUpdTimestamp( rs.getTimestamp(DeptItems.updTimestamp.getItemName()) );
  	  entity.setUpdUserId( rs.getString(DeptItems.updUserId.getItemName()) );
  	  entity.setUpdUserNm( rs.getString(DeptItems.updUserNm.getItemName()) );

  	  return entity;
	}

}
