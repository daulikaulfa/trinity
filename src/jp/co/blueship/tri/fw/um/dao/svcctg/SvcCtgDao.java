package jp.co.blueship.tri.fw.um.dao.svcctg;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.fw.um.dao.svcctg.constants.SvcCtgItems;
import jp.co.blueship.tri.fw.um.dao.svcctg.eb.ISvcCtgEntity;
import jp.co.blueship.tri.fw.um.dao.svcctg.eb.SvcCtgEntity;

/**
 * The implements of the service catalog DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class SvcCtgDao extends JdbcBaseDao<ISvcCtgEntity> implements ISvcCtgDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected final ITableAttribute getTableAttribute() {
		return UmTables.UM_SVC_CTG;
	}

	@Override
	protected final ISqlBuilder append( ISqlBuilder builder, ISvcCtgEntity entity ) {
		builder
			.append(SvcCtgItems.svcCtgId, entity.getSvcCtgId(), true)
			.append(SvcCtgItems.svcCtgNm, entity.getSvcCtgNm())
			.append(SvcCtgItems.svcCtgContent, entity.getSvcCtgContent())
			.append(SvcCtgItems.svcCtgPath, entity.getSvcCtgPath())
			.append(SvcCtgItems.sortOdr, entity.getSortOdr())
			.append(SvcCtgItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(SvcCtgItems.regTimestamp, entity.getRegTimestamp())
			.append(SvcCtgItems.regUserId, entity.getRegUserId())
			.append(SvcCtgItems.regUserNm, entity.getRegUserNm())
			.append(SvcCtgItems.updTimestamp, entity.getUpdTimestamp())
			.append(SvcCtgItems.updUserId, entity.getUpdUserId())
			.append(SvcCtgItems.updUserNm, entity.getUpdUserNm())
			;

		return builder;
	}

	@Override
	protected final ISvcCtgEntity entityMapping( ResultSet rs, int row ) throws SQLException {
		ISvcCtgEntity entity = new SvcCtgEntity();

		entity.setSvcCtgId( rs.getString(SvcCtgItems.svcCtgId.getItemName()) );
		entity.setSvcCtgNm( rs.getString(SvcCtgItems.svcCtgNm.getItemName()) );
		entity.setSvcCtgContent( rs.getString(SvcCtgItems.svcCtgContent.getItemName()) );
		entity.setSvcCtgPath( rs.getString(SvcCtgItems.svcCtgPath.getItemName()) );
		entity.setSortOdr( rs.getInt(SvcCtgItems.sortOdr.getItemName()) );
		entity.setDelStsId( StatusFlg.value(rs.getBoolean(SvcCtgItems.delStsId.getItemName())) );
		entity.setRegTimestamp( rs.getTimestamp(SvcCtgItems.regTimestamp.getItemName()) );
		entity.setRegUserId( rs.getString(SvcCtgItems.regUserId.getItemName()) );
		entity.setRegUserNm( rs.getString(SvcCtgItems.regUserNm.getItemName()) );
		entity.setUpdTimestamp( rs.getTimestamp(SvcCtgItems.updTimestamp.getItemName()) );
		entity.setUpdUserId( rs.getString(SvcCtgItems.updUserId.getItemName()) );
		entity.setUpdUserNm( rs.getString(SvcCtgItems.updUserNm.getItemName()) );

		return entity;
	}

}
