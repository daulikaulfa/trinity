package jp.co.blueship.tri.fw.um.dao.ctg.eb;

/**
 * The implementation of category link DTO.
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 */
public class CtgLnkDto implements ICtgLnkDto {

	/**
	 * Category Link Entity
	 */
	public ICtgLnkEntity ctgLnkEntity = null;

	@Override
	public ICtgLnkEntity getCtgLnkEntity() {
		return ctgLnkEntity;
	}

	@Override
	public void setCtgLnkEntity(ICtgLnkEntity ctgLnkEntity) {
		this.ctgLnkEntity = ctgLnkEntity;
	}

}
