package jp.co.blueship.tri.fw.um.dao.filter.eb;

import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the Filter entity.
 *
 * @version V4.00.00
 * @author Yohei Tanaka
 */
public interface IFilterEntity extends IEntityFooter {

	/**
	 * Filter ID
	 * @return Filter-ID
	 */
	public String getFilterId();
	/**
	 * Filter ID
	 * @param filterId Filter-ID
	 */
	public void setFilterId(String filterId);
	/**
	 * Filter name
	 * @return Filter name
	 */
	public String getFilterNm();
	/**
	 * Filter name
	 * @param filterName Filter name
	 */
	public void setFilterNm(String filterName);
	/**
	 * Filter data
	 * @return Filter data
	 */
	public String getFilterData();
	/**
	 * Filter data
	 * @param filterData Filter data
	 */
	public void setFilterData(String filterData);

	/**
	 * Lot id
	 * @param lotId Lot id
	 */
	public String getLotId();

	/**
	 * Lot id
	 * @param lotId Lot id
	 */
	public void setLotId(String lotId);

	/**
	 * User id
	 * @param userId User id
	 */
	public String getUserId();

	/**
	 * User id
	 * @param userId User id
	 */
	public void setUserId(String userId);

	/**
	 * Service ID
	 * @return service ID
	 */
	public String getSvcId();
	/**
	 * Service Id
	 * @param serviceId Service ID
	 */
	public void setSvcId(String svcId);

}
