package jp.co.blueship.tri.fw.um.dao.hist;

import jp.co.blueship.tri.dcm.dao.constants.DcmTables;
import jp.co.blueship.tri.dcm.dao.rep.eb.IRepEntity;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.oxm.utils.TriXmlUtils;
import jp.co.blueship.tri.fw.schema.beans.rep.RepRecDocument;
import jp.co.blueship.tri.fw.schema.beans.rep.RepType;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.utils.HistMappingUtils;

/**
 * The implements of the rep history DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 */
public class RepHistDao extends HistDao implements IRepHistDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	public int insert(IHistEntity entity , IRepEntity repEntity){

		RepRecDocument repDoc = RepRecDocument.Factory.newInstance();
		RepType repXml = repDoc.addNewRepRec();
		repXml = new HistMappingUtils().mapDB2XMLBeans( repEntity, repXml );

		entity.setHistId( this.getHistNumberingDao().nextval() );
		entity.setDataCtgCd( DcmTables.DCM_REP.name() );
		entity.setDataId( repEntity.getRepId() );
		entity.setHistData( TriXmlUtils.replaceExcessString( repDoc ) );
		entity.setDelStsId( StatusFlg.off );
		entity.setLotId( repEntity.getLotId() );
		entity.setStsId( repEntity.getStsId() );

		return super.insert( entity );
	}
}