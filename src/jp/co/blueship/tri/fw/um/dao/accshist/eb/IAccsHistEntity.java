package jp.co.blueship.tri.fw.um.dao.accshist.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the access history entity.
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 */
public interface IAccsHistEntity extends IEntityFooter {
	public String getUserId();
	public void setUserId(String userId);

	public String getDataCtgCd();
	public void setDataCtgCd(String dataCtgCd);

	public String getDataId();
	public void setDataId(String dataId);

	public Timestamp getAccsTimestamp();
	public void setAccsTimestamp(Timestamp accsTimestamp);

	public Timestamp getNoticeViewTimestamp();
	public void setNoticeViewTimestamp(Timestamp noticeViewTimestamp);

	public String getUserNm();
	public void setUserNm(String userNm);
}
