package jp.co.blueship.tri.fw.um.dao.grp.eb;

import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the group entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 * 
 * @version V4.00.00
 * @author le.thixuan
 */
public interface IGrpEntity extends IEntityFooter {

	public String getGrpId();
	public void setGrpId(String grpId);

	public String getGrpNm();
	public void setGrpNm(String grpNm);
	
	/**
	 * sort order
	 * @return sort order
	 */
	public Integer getSortOdr();
	/**
	 * sort order
	 * @param sortOdr sort order
	 */
	public void setSortOdr(Integer sortOdr);
}
