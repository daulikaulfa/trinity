package jp.co.blueship.tri.fw.um.dao.wiki.eb;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.EntityFooter;


/**
 *
 * @V4.00.00
 * @author Akahoshi
 *
 */
public class WikiEntity extends EntityFooter implements IWikiEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	public String wikiId = null;
	public String lotId = null;
	public Integer wikiVerNo = null;
	public String pageNm = null;
	public String content = null;
	public StatusFlg isWikiHome = null;


	@Override
	public String getWikiId() {
		return this.wikiId;
	}

	@Override
	public void setWikiId(String wikiId) {
		this.wikiId = wikiId;
	}

	@Override
	public String getLotId() {
		return this.lotId;
	}

	@Override
	public void setLotId(String lotId) {
		this.lotId = lotId;
	}

	@Override
	public Integer getWikiVerNo() {
		return this.wikiVerNo;
	}

	@Override
	public void setWikiVerNo(Integer wikiVerNo) {
		this.wikiVerNo = wikiVerNo;
	}

	@Override
	public String getPageNm() {
		return this.pageNm;
	}

	@Override
	public void setPageNm(String pageNm) {
		this.pageNm = pageNm;
	}

	@Override
	public String getContent() {
		return this.content;
	}

	@Override
	public void setContent(String content) {
		this.content = content;
	}

	@Override
	public StatusFlg isWikiHome() {
		return this.isWikiHome;
	}

	@Override
	public void setIsWikiHome(StatusFlg isWikiHome) {
		this.isWikiHome = isWikiHome;
	}
}
