package jp.co.blueship.tri.fw.um.dao.project.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.fw.dao.orm.EntityFooter;
import jp.co.blueship.tri.fw.um.constants.IconSelectionOption;

/**
 * project entity.
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 */
public class ProjectEntity extends EntityFooter implements IProjectEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Product Id
	 */
	public String productId = null;
	
	/**
	 * Project Name
	 */
	public String projectNm = null;

	/**
	 * Icon Type
	 */
	public String iconTyp = null;
	
	/**
	 * Default Icon Path
	 */
	public String defaultIconPath = null;
	
	/**
	 * Custom Icon Path
	 */
	public String customIconPath = null;
	
	/**
	 * Language
	 */
	public String language = null;
	
	/**
	 * Timezone
	 */
	public String timezone = null;
	
	/**
	 * Notice
	 */
	public String notice = null;
	
	/**
	 * Notice Timestamp
	 */
	public Timestamp noticeTimestamp = null;
	
	private String lookAndFeel = null;
	
	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProjectNm() {
		return projectNm;
	}

	public void setProjectNm(String projectNm) {
		this.projectNm = projectNm;
	}

	public String getIconTyp() {
		if (null == iconTyp) {
			iconTyp = IconSelectionOption.DefaultImage.value();
		}
		return iconTyp;
	}

	public void setIconTyp(String iconTyp) {
		this.iconTyp = iconTyp;
	}

	public String getDefaultIconPath() {
		return defaultIconPath;
	}

	public void setDefaultIconPath(String defaultIconPath) {
		this.defaultIconPath = defaultIconPath;
	}

	public String getCustomIconPath() {
		return customIconPath;
	}

	public void setCustomIconPath(String customIconPath) {
		this.customIconPath = customIconPath;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public String getNotice() {
		return notice;
	}

	public void setNotice(String notice) {
		this.notice = notice;
	}

	public Timestamp getNoticeTimestamp() {
		return noticeTimestamp;
	}

	public void setNoticeTimestamp(Timestamp noticeTimestamp) {
		this.noticeTimestamp = noticeTimestamp;
	}

	public String getLookAndFeel() {
		return lookAndFeel;
	}

	public void setLookAndFeel(String lookAndFeel) {
		this.lookAndFeel = lookAndFeel;
	}
}
