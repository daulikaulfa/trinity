package jp.co.blueship.tri.fw.um.dao.svcctg.eb;

import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the service catalog service link entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface ISvcCtgSvcLnkEntity extends IEntityFooter {

	public String getSvcId();
	public void setSvcId(String svcId);

	public String getSvcCtgId();
	public void setSvcCtgId(String svcCtgId);


}
