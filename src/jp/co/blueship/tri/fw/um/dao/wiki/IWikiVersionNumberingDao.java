package jp.co.blueship.tri.fw.um.dao.wiki;

import jp.co.blueship.tri.fw.dao.orm.IDao;
import jp.co.blueship.tri.fw.dao.orm.ex.TriJdbcDaoException;

/**
 * The access interface for automatic numbering.
 * <br>
 * <br>自動採番を行うためのアクセスインタフェースです。
 *
 */
public interface IWikiVersionNumberingDao extends IDao {
	/**
	 * increments the sequence and returns the next value.
	 *
	 * @param id Wiki ID
	 * @return returns the next value.
	 */
	public Integer nextval( String id ) throws TriJdbcDaoException;

}
