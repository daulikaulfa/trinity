package jp.co.blueship.tri.fw.um.dao.user;

import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserUrlEntity;

/**
 * 
 * @version V4.00.00
 * @author le.thixuan
 */
public interface IUserUrlDao extends IJdbcDao<IUserUrlEntity> {

}
