package jp.co.blueship.tri.fw.um.dao.wiki;

import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.IWikiTagEntity;

/**
 * The interface of the wikiTag DAO.
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 */
public interface IWikiTagDao extends IJdbcDao<IWikiTagEntity> {

}
