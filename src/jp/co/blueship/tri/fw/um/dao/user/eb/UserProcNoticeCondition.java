package jp.co.blueship.tri.fw.um.dao.user.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.dao.orm.psql.SqlFormatUtils;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.fw.um.dao.user.constants.UserProcNoticeItems;

/**
 * The SQL condition of the user proc notice entity.
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 */
public class UserProcNoticeCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute userprocnotice = UmTables.UM_USER_PROC_NOTICE;

	public String procId = null;
	public String userId = null;
	public String[] userIds = null;
	public StatusFlg isRead = StatusFlg.off;


	public UserProcNoticeCondition() {
		super(userprocnotice);
	}

	public String getProcId(){
		return this.procId;
	}
	public void setProcId(String procId){
		this.procId =  procId;
		super.append(UserProcNoticeItems.procId, procId );
	}

	public String getUserId(){
		return this.userId;
	}
	public void setUserId(String userId){
		this.userId =  userId;
		super.append(UserProcNoticeItems.userId, userId );
	}

	public String[] getUserIds(){
		return this.userIds;
	}
	public void setUserIds(String[] userIds){
		this.userIds =  userIds;
	}

	public StatusFlg getIsRead(){
		return this.isRead;
	}
	public void setIsRead(StatusFlg isRead){
		this.isRead = isRead;
		 super.append( UserProcNoticeItems.isRead, (null == isRead)? null :isRead.parseBoolean() );
	}

	/**
	 * FromToで検索範囲を設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setFromToRegTimestamp(Timestamp from, Timestamp to ){

		super.append(UserProcNoticeItems.regTimestamp.getItemName() ,
				SqlFormatUtils.getFromTo(from, to, UserProcNoticeItems.regTimestamp.getItemName()));
	}

}
