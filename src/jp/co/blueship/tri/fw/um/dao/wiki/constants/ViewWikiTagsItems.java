package jp.co.blueship.tri.fw.um.dao.wiki.constants;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;

/**
 * The items of the asset register entity.
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public enum ViewWikiTagsItems implements ITableItem {
	tagNm("tag_nm"),
	lotId("lot_id"),
	count("count"),
	;

	private String element = null;

	private ViewWikiTagsItems( String element) {
		this.element = element;
	}

	public String getItemName() {
		return this.element;
	}

}
