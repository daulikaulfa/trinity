package jp.co.blueship.tri.fw.um.dao.user.eb;

/**
 * The SQL condition of the user access history entity.
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class ViewUserAccsHistCondition {

	private String[] keywords = null;

	public String[] getKeywords() {
		return keywords;
	}

	public void setKeywords(String... keywords) {
		this.keywords = keywords;
	}
	
}
