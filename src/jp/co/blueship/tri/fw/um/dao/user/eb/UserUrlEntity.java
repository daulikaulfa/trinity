package jp.co.blueship.tri.fw.um.dao.user.eb;

import jp.co.blueship.tri.fw.dao.orm.EntityFooter;

/**
 * 
 * @version V4.00.00
 * @author le.thixuan
 */
public class UserUrlEntity extends EntityFooter implements IUserUrlEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	private String userId = null;
	private String urlSeqNo = null;
	private String url = null;
	
	public String getUserId() {
		return userId;
	}
	
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public String getUrlSeqNo() {
		return urlSeqNo;
	}
	
	public void setUrlSeqNo(String urlSeqNo) {
		this.urlSeqNo = urlSeqNo;
	}
	
	public String getUrl() {
		return url;
	}
	
	public void setUrl(String url) {
		this.url = url;
	}
}
