package jp.co.blueship.tri.fw.um.dao.wiki.eb;

import jp.co.blueship.tri.fw.dao.orm.EntityFooter;

/**
 *
 * @V4.00.00
 * @author Akahoshi
 *
 */
public class WikiTagEntity extends EntityFooter implements IWikiTagEntity {

	public String wikiId = null;
	public String tagNm = null;

	@Override
	public String getWikiId() {
		return this.wikiId;
	}

	@Override
	public void setWikiId(String wikiId) {
		this.wikiId = wikiId;
	}

	@Override
	public String getTagNm() {
		return this.tagNm;
	}

	@Override
	public void setTagNm(String tagNm) {
		this.tagNm = tagNm;
	}

}
