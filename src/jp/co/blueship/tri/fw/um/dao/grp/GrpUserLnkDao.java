package jp.co.blueship.tri.fw.um.dao.grp;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.fw.um.dao.grp.constants.GrpUserLnkItems;
import jp.co.blueship.tri.fw.um.dao.grp.eb.GrpUserLnkEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;

/**
 * The implements of the group user link DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 */
public class GrpUserLnkDao extends JdbcBaseDao<IGrpUserLnkEntity> implements IGrpUserLnkDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected final ITableAttribute getTableAttribute() {
		return UmTables.UM_GRP_USER_LNK;
	}

	@Override
	protected final ISqlBuilder append( ISqlBuilder builder, IGrpUserLnkEntity entity ) {
		builder
			.append(GrpUserLnkItems.grpId, entity.getGrpId(), true)
			.append(GrpUserLnkItems.userId, entity.getUserId(), true)
			.append(GrpUserLnkItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(GrpUserLnkItems.regTimestamp, entity.getRegTimestamp())
			.append(GrpUserLnkItems.regUserId, entity.getRegUserId())
			.append(GrpUserLnkItems.regUserNm, entity.getRegUserNm())
			.append(GrpUserLnkItems.updTimestamp, entity.getUpdTimestamp())
			.append(GrpUserLnkItems.updUserId, entity.getUpdUserId())
			.append(GrpUserLnkItems.updUserNm, entity.getUpdUserNm())
			;

		return builder;
	}

	@Override
	protected final IGrpUserLnkEntity entityMapping( ResultSet rs, int row ) throws SQLException {
  	  IGrpUserLnkEntity entity = new GrpUserLnkEntity();

  	  entity.setGrpId( rs.getString(GrpUserLnkItems.grpId.getItemName()) );
  	  entity.setUserId( rs.getString(GrpUserLnkItems.userId.getItemName()) );
  	  entity.setDelStsId( StatusFlg.value(rs.getBoolean(GrpUserLnkItems.delStsId.getItemName())) );
  	  entity.setRegTimestamp( rs.getTimestamp(GrpUserLnkItems.regTimestamp.getItemName()) );
  	  entity.setRegUserId( rs.getString(GrpUserLnkItems.regUserId.getItemName()) );
  	  entity.setRegUserNm( rs.getString(GrpUserLnkItems.regUserNm.getItemName()) );
  	  entity.setUpdTimestamp( rs.getTimestamp(GrpUserLnkItems.updTimestamp.getItemName()) );
  	  entity.setUpdUserId( rs.getString(GrpUserLnkItems.updUserId.getItemName()) );
  	  entity.setUpdUserNm( rs.getString(GrpUserLnkItems.updUserNm.getItemName()) );

  	  return entity;
	}

}
