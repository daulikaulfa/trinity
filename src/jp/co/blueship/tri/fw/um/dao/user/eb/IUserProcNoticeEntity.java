package jp.co.blueship.tri.fw.um.dao.user.eb;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the user proc notice entity.
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 */
public interface IUserProcNoticeEntity extends IEntityFooter{

	public String getProcId();
	public void setProcId(String procId);

	public String getUserId();
	public void setUserId(String userId);

	public StatusFlg getIsRead();
	public void setIsRead(StatusFlg isRead);
}
