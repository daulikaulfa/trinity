package jp.co.blueship.tri.fw.um.dao.hist;

import java.util.List;

import jp.co.blueship.tri.dm.dao.dmdo.eb.IDmDoEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpDto;

/**
 * The implements of the Rp History DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 */
public interface IRpHistDao extends IHistDao {

	/**
	 * 登録処理を行います
	 *
	 * @param entity
	 * @return the number of rows affected1
	 */
	public int insert(IHistEntity entity , IRpDto rpDto , List<IDmDoEntity> raRpEntities , List<String> raIds);
}
