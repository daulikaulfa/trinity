package jp.co.blueship.tri.fw.um.dao.hist;

import jp.co.blueship.tri.bm.dao.bp.eb.IBpDto;
import jp.co.blueship.tri.bm.dao.constants.BmTables;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.oxm.utils.TriXmlUtils;
import jp.co.blueship.tri.fw.schema.beans.bp.BpRecDocument;
import jp.co.blueship.tri.fw.schema.beans.bp.BpType;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.utils.HistMappingUtils;

/**
 * The implements of the bp history DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 */
public class BpHistDao extends HistDao implements IBpHistDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	public int insert(IHistEntity entity , IBpDto bpDto){

		BpRecDocument bpDoc = BpRecDocument.Factory.newInstance();
		BpType bpXml = bpDoc.addNewBpRec();
		bpXml = new HistMappingUtils().mapDB2XMLBeans( bpDto, bpXml );

		entity.setHistId( this.getHistNumberingDao().nextval() );
		entity.setDataCtgCd( BmTables.BM_BP.name() );
		entity.setDataId( bpDto.getBpEntity().getBpId() );
		entity.setHistData( TriXmlUtils.replaceExcessString( bpDoc ) );
		entity.setDelStsId( StatusFlg.off );
		entity.setLotId( bpDto.getBpEntity().getLotId() );
		entity.setStsId( bpDto.getBpEntity().getStsId() );

		return super.insert( entity );
	}
}