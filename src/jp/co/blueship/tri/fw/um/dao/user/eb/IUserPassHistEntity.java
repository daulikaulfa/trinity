package jp.co.blueship.tri.fw.um.dao.user.eb;

import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the user password history entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IUserPassHistEntity extends IEntityFooter {

	public String getUserId();
	public void setUserId(String userId);

	public String getPassHistSeqNo();
	public void setPassHistSeqNo(String passHistSeqNo);

	public String getUserPass();
	public void setUserPass(String userPass);
}
