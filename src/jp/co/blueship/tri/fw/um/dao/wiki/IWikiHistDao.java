package jp.co.blueship.tri.fw.um.dao.wiki;

import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.IWikiHistEntity;

/**
 * The interface of the wikiHist DAO.
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 */
public interface IWikiHistDao extends IJdbcDao<IWikiHistEntity> {

}
