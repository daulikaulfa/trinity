package jp.co.blueship.tri.fw.um.dao.ctg.eb;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.dao.orm.psql.SqlFormatUtils;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.fw.um.dao.ctg.constants.CtgLnkItems;

/**
 * The SQL condition of the Category Link entity
 *
 * @author Hai Thach
 *
 */
public class CtgLnkCondition extends ConditionSupport{

	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute category = UmTables.UM_CTG_LNK;

	/**
	 * category-Id
	 */
	public String ctgId = null;
	/**
	 * data category cd
	 */
	public String dataCtgCd = null;
	/**
	 * data-id
	 */
	public String dataId = null;
	/**
	 * category-ID's
	 */
	public String[] ctgIds = null;
	/**
	 * data category cd's
	 */
	public String[] dataCtgCds = null;
	/**
	 * data id's
	 */
	public String[] dataIds = null;
	/**
	 * delete-status-id
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		super.append(CtgLnkItems.delStsId, StatusFlg.off.parseBoolean() );
	}

	public CtgLnkCondition() {
		super(category);
	}

	/**
	 * category-id
	 * @return category-id
	 */
	public String getCtgId() {
		return ctgId;
	}

	/**
	 * category-id
	 * @param ctgId category-ID
	 */
	public void setCtgId(String ctgId) {
		this.ctgId = ctgId;
		super.append(CtgLnkItems.ctgId, ctgId);
	}

	/**
	 * data category cd
	 * @return data category cd
	 */
	public String getDataCtgCd() {
		return dataCtgCd;
	}

	/**
	 * data category cd
	 * @param dataCtgCd data category cd
	 */
	public void setDataCtgCd(String dataCtgCd) {
		this.dataCtgCd = dataCtgCd;
		super.append(CtgLnkItems.dataCtgCd, dataCtgCd);
	}

	/**
	 * data-id
	 * @return data-id
	 */
	public String getDataId() {
		return dataId;
	}

	/**
	 * data-id
	 * @param dataId data-id
	 */
	public void setDataId(String dataId) {
		this.dataId = dataId;
		super.append(CtgLnkItems.dataId, dataId);
	}

	/**
	 * category-id's
	 * @return category-id's
	 */
	public String[] getCtgIds() {
		return ctgIds;
	}

	/**
	 * category-id's
	 * @param ctgIds category-ID's
	 */
	public void setCtgIds(String... ctgIds) {
		this.ctgIds = ctgIds;
		super.append( CtgLnkItems.ctgId.getItemName() + "[]",
				SqlFormatUtils.getCondition(ctgIds, CtgLnkItems.ctgId.getItemName(), false, true, false) );
	}

	/**
	 * data category cd's
	 * @return data category cd's
	 */
	public String[] getDataCtgCds() {
		return dataCtgCds;
	}

	/**
	 * data category cd's
	 * @param dataCtgCds data category cd's
	 */
	public void setDataCtgCds(String... dataCtgCds) {
		this.dataCtgCds = dataCtgCds;
		super.append( CtgLnkItems.dataCtgCd.getItemName() + "[]",
				SqlFormatUtils.getCondition(dataCtgCds, CtgLnkItems.dataCtgCd.getItemName(), false, true, false) );
	}

	/**
	 * data-id's
	 * @return data-id's
	 */
	public String[] getDataIds() {
		return dataIds;
	}

	/**
	 * data-id's
	 * @param dataIds data-ID's
	 */
	public void setDataIds(String... dataIds) {
		this.dataIds = dataIds;
		super.append( CtgLnkItems.dataId.getItemName() + "[]",
				SqlFormatUtils.getCondition(dataIds, CtgLnkItems.dataId.getItemName(), false, true, false) );
	}

}
