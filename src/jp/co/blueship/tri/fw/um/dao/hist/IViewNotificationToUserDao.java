package jp.co.blueship.tri.fw.um.dao.hist;

import java.util.List;

import jp.co.blueship.tri.fw.dao.orm.IDao;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.ex.TriJdbcDaoException;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.ViewNotificationToUserCondition;

/**
 * @version V4.00.00
 * @author Akahoshi
 */
public interface IViewNotificationToUserDao  extends IDao {

	public List<IHistEntity> find( ViewNotificationToUserCondition condition,
			   ISqlSort sort
			   ) throws TriJdbcDaoException;
}
