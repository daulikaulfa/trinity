package jp.co.blueship.tri.fw.um.dao.project.eb;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.dao.orm.psql.SqlFormatUtils;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.fw.um.dao.project.constants.ProjectItems;

/**
 * The SQL condition of the project entity.
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 */
public class ProjectCondition extends ConditionSupport {
	private static final ITableAttribute project = UmTables.UM_PROJECT;
	
	public ProjectCondition() {
		super(project);
	}
	
	/**
	 * product Id
	 */
	public String productId = null;
	/**
	 * project name
	 */
	public String projectNm = null;
	/**
	 * keyword
	 */
	public String[] keywords = null;
	/**
	 * delete status id
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		super.append(ProjectItems.delStsId, StatusFlg.off.parseBoolean());
	}
	
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
		super.append(ProjectItems.productId, productId);
	}
	
	public String getProjectNm() {
		return projectNm;
	}
	public void setProjectNm(String projectNm) {
		this.projectNm = projectNm;
		super.append(ProjectItems.projectNm, projectNm);
	}
	
	public String[] getKeywords() {
		return keywords;
	}
	public void setContainsByKeywords(String... keywords) {
		if (TriStringUtils.isEmpty(keywords)) {
			return;
		}
		
		this.keywords = keywords;
		super.append(ProjectItems.productId.getItemName() + "Other", 
				SqlFormatUtils.joinCondition( true, 
						new String[] {
							SqlFormatUtils.getCondition(this.keywords, ProjectItems.productId.getItemName(), true, false, false),
							SqlFormatUtils.getCondition(this.keywords, ProjectItems.projectNm.getItemName(), true, false, false),
						}));
	}
	
	public StatusFlg getDelStsId() {
		return delStsId;
	}
	public void setDelStsId(StatusFlg delStsId) {
		this.delStsId = delStsId;
		super.append(ProjectItems.delStsId, (null == delStsId) ? null : delStsId.parseBoolean());
	}
	
	
}
