package jp.co.blueship.tri.fw.um.dao.user;

import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserRoleLnkEntity;


/**
 * The interface of the user role link DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IUserRoleLnkDao extends IJdbcDao<IUserRoleLnkEntity> {

}
