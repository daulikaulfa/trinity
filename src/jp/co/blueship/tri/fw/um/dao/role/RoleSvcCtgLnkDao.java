package jp.co.blueship.tri.fw.um.dao.role;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.fw.um.dao.role.constants.RoleSvcCtgLnkItems;
import jp.co.blueship.tri.fw.um.dao.role.eb.IRoleSvcCtgLnkEntity;
import jp.co.blueship.tri.fw.um.dao.role.eb.RoleSvcCtgLnkEntity;

/**
 * The implements of the role service catalog link DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 */
public class RoleSvcCtgLnkDao extends JdbcBaseDao<IRoleSvcCtgLnkEntity> implements IRoleSvcCtgLnkDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected final ITableAttribute getTableAttribute() {
		return UmTables.UM_ROLE_SVC_CTG_LNK;
	}

	@Override
	protected final ISqlBuilder append( ISqlBuilder builder, IRoleSvcCtgLnkEntity entity ) {
		builder
			.append(RoleSvcCtgLnkItems.roleId, entity.getRoleId(), true)
			.append(RoleSvcCtgLnkItems.svcCtgId, entity.getSvcCtgId())
			.append(RoleSvcCtgLnkItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(RoleSvcCtgLnkItems.regTimestamp, entity.getRegTimestamp())
			.append(RoleSvcCtgLnkItems.regUserId, entity.getRegUserId())
			.append(RoleSvcCtgLnkItems.regUserNm, entity.getRegUserNm())
			.append(RoleSvcCtgLnkItems.updTimestamp, entity.getUpdTimestamp())
			.append(RoleSvcCtgLnkItems.updUserId, entity.getUpdUserId())
			.append(RoleSvcCtgLnkItems.updUserNm, entity.getUpdUserNm())
			;

		return builder;
	}

	@Override
	protected final IRoleSvcCtgLnkEntity entityMapping( ResultSet rs, int row ) throws SQLException {
  	  IRoleSvcCtgLnkEntity entity = new RoleSvcCtgLnkEntity();

  	  entity.setRoleId( rs.getString(RoleSvcCtgLnkItems.roleId.getItemName()) );
  	  entity.setSvcCtgId( rs.getString(RoleSvcCtgLnkItems.svcCtgId.getItemName()) );
  	  entity.setDelStsId( StatusFlg.value(rs.getBoolean(RoleSvcCtgLnkItems.delStsId.getItemName())) );
  	  entity.setRegTimestamp( rs.getTimestamp(RoleSvcCtgLnkItems.regTimestamp.getItemName()) );
  	  entity.setRegUserId( rs.getString(RoleSvcCtgLnkItems.regUserId.getItemName()) );
  	  entity.setRegUserNm( rs.getString(RoleSvcCtgLnkItems.regUserNm.getItemName()) );
  	  entity.setUpdTimestamp( rs.getTimestamp(RoleSvcCtgLnkItems.updTimestamp.getItemName()) );
  	  entity.setUpdUserId( rs.getString(RoleSvcCtgLnkItems.updUserId.getItemName()) );
  	  entity.setUpdUserNm( rs.getString(RoleSvcCtgLnkItems.updUserNm.getItemName()) );

  	  return entity;
	}

}
