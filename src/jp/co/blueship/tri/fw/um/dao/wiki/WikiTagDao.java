package jp.co.blueship.tri.fw.um.dao.wiki;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.fw.um.dao.wiki.constants.WikiTagItems;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.IWikiTagEntity;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.WikiTagEntity;


/**
 * The implements of the wikiTag DAO.
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 */
public class WikiTagDao extends JdbcBaseDao<IWikiTagEntity> implements IWikiTagDao {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected ITableAttribute getTableAttribute() {
		return UmTables.UM_WIKI_TAG;
	}

	@Override
	protected ISqlBuilder append(ISqlBuilder builder, IWikiTagEntity entity) {
		builder
			.append(WikiTagItems.wikiId, entity.getWikiId(), true)
			.append(WikiTagItems.tagNm, entity.getTagNm(), true)
			.append(WikiTagItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(WikiTagItems.regTimestamp, entity.getRegTimestamp())
			.append(WikiTagItems.regUserId, entity.getRegUserId())
			.append(WikiTagItems.regUserNm, entity.getRegUserNm())
			.append(WikiTagItems.updTimestamp, entity.getUpdTimestamp())
			.append(WikiTagItems.updUserId, entity.getUpdUserId())
			.append(WikiTagItems.updUserNm, entity.getUpdUserNm())
			;
		return builder;
	}

	@Override
	protected IWikiTagEntity entityMapping(ResultSet rs, int row) throws SQLException {
		IWikiTagEntity entity = new WikiTagEntity();

		entity.setWikiId		( rs.getString(WikiTagItems.wikiId.getItemName()) );
		entity.setTagNm			( rs.getString(WikiTagItems.tagNm.getItemName()) );
		entity.setDelStsId		( StatusFlg.value(rs.getBoolean(WikiTagItems.delStsId.getItemName())) );
		entity.setRegTimestamp	( rs.getTimestamp(WikiTagItems.regTimestamp.getItemName()) );
		entity.setRegUserId		( rs.getString(WikiTagItems.regUserId.getItemName()) );
		entity.setRegUserNm		( rs.getString(WikiTagItems.regUserNm.getItemName()) );
		entity.setUpdTimestamp	( rs.getTimestamp(WikiTagItems.updTimestamp.getItemName()) );
		entity.setUpdUserId		( rs.getString(WikiTagItems.updUserId.getItemName()) );
		entity.setUpdUserNm		( rs.getString(WikiTagItems.updUserNm.getItemName()) );

		return entity;
	}
}
