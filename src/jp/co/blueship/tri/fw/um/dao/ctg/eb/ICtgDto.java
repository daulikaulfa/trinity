package jp.co.blueship.tri.fw.um.dao.ctg.eb;

/**
 * The interface of the category DTO.
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 */
public interface ICtgDto {
	/**
	 * get a category entity
	 *
	 * @return Category Entity
	 */
	public ICtgEntity getCtgEntity();

	/**
	 * set a category entity
	 *
	 * @param ctgEntity
	 */
	public void setCtgEntity(ICtgEntity ctgEntity);
}
