package jp.co.blueship.tri.fw.um.dao.wiki;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.ex.TriJdbcDaoException;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;
import jp.co.blueship.tri.fw.dao.orm.psql.SqlFormatUtils;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.fw.um.dao.wiki.constants.ViewWikiTagsItems;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.IViewWikiTagsEntity;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.ViewWikiTagsCondition;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.ViewWikiTagsEntity;

public class ViewWikiTagsDao extends JdbcBaseDao<IViewWikiTagsEntity> implements IViewWikiTagsDao{

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	private static final String SQL_WIKI_TAG_QUERY =  "SELECT "
													 +" R.TAG_NM, R.LOT_ID, COUNT(*) AS COUNT"
													 +" FROM "
													 +" 	(SELECT"
													 +" 	  T.WIKI_ID, T.TAG_NM, W.LOT_ID, T.DEL_STS_ID "
													 +" 	  FROM UM_WIKI_TAG T "
													 +" 		LEFT JOIN UM_WIKI W "
													 +" 		ON T.WIKI_ID = W.WIKI_ID) R"
													 +" WHERE R.DEL_STS_ID = FALSE"
													 +" GROUP BY R.TAG_NM, R.LOT_ID ";

	@Override
	protected ITableAttribute getTableAttribute() {
		return UmTables.UM_WIKI_TAG;
	}

	@Override
	@Deprecated
	protected ISqlBuilder append(ISqlBuilder builder, IViewWikiTagsEntity entity) {
		return null;
	}

	@Override
	protected IViewWikiTagsEntity entityMapping(ResultSet rs, int row) throws SQLException {
		IViewWikiTagsEntity entity = new ViewWikiTagsEntity();

		entity.setTagNm( rs.getString(ViewWikiTagsItems.tagNm.getItemName()) );
		entity.setLotId( rs.getString(ViewWikiTagsItems.lotId.getItemName()) );
		entity.setCount( rs.getInt(ViewWikiTagsItems.count.getItemName()) );

		return entity;
	}


	@Override
	public List<IViewWikiTagsEntity> find(ViewWikiTagsCondition condition) throws TriJdbcDaoException {

		try{
			StringBuffer buf = new StringBuffer();
			String sql = null;

			String tagNm = (condition.getTagNm() != null)? " " + SqlFormatUtils.getCondition( condition.getTagNm(),"R." + ViewWikiTagsItems.tagNm.getItemName(), true, false, false): null;
			String lotId = (condition.getLotId() != null)? " " + SqlFormatUtils.getCondition( condition.getLotId(),"R." + ViewWikiTagsItems.lotId.getItemName(), true, false, false): null;

			buf.append( SQL_WIKI_TAG_QUERY );

			if( tagNm != null || lotId != null )
				buf.append( " HAVING " );

			if( tagNm != null )
				buf.append( tagNm );

			if( lotId != null ){
				if( tagNm != null )
					buf.append( " AND " );

				buf.append( lotId );
			}

			sql = buf.toString();

			List<IViewWikiTagsEntity> rows = super.query(sql, new ParameterizedRowMapper<IViewWikiTagsEntity>() {

				@Override
				public IViewWikiTagsEntity mapRow(ResultSet resultset, int i) throws SQLException {
					return entityMapping(resultset, i);
				}
			});

			return rows;

		} catch (Exception e) {
			throw new TriJdbcDaoException(SmMessageId.SM005004S, e, this.getTableAttribute().name());
		}
	}
}
