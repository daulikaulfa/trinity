package jp.co.blueship.tri.fw.um.dao.user.eb;

import jp.co.blueship.tri.fw.dao.orm.EntityFooter;


/**
 * user role link entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class UserRoleLnkEntity extends EntityFooter implements IUserRoleLnkEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * user-ID
	 */
	public String userId = null;
	/**
	 * role-ID
	 */
	public String roleId = null;

	/**
	 * user-IDを取得します。
	 * @return user-ID
	 */
	public String getUserId() {
	    return userId;
	}
	/**
	 * user-IDを設定します。
	 * @param userId user-ID
	 */
	public void setUserId(String userId) {
	    this.userId = userId;
	}
	/**
	 * role-IDを取得します。
	 * @return role-ID
	 */
	public String getRoleId() {
	    return roleId;
	}
	/**
	 * role-IDを設定します。
	 * @param roleId role-ID
	 */
	public void setRoleId(String roleId) {
	    this.roleId = roleId;
	}

}