package jp.co.blueship.tri.fw.um.dao.mstone.eb;

/**
 * The implementation of milestone DTO.
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 */

public class MstoneDto implements IMstoneDto {

	/**
	 * Milestone Entity
	 */
	public IMstoneEntity mstoneEntity = null;

	@Override
	public IMstoneEntity getMstoneEntity() {
		return mstoneEntity;
	}

	@Override
	public void setMstoneEntity(IMstoneEntity mstoneEntity) {
		this.mstoneEntity = mstoneEntity;
	}

}
