package jp.co.blueship.tri.fw.um.dao.hist;

import jp.co.blueship.tri.dm.dao.constants.DmTables;
import jp.co.blueship.tri.dm.dao.dmdo.eb.IDmDoDto;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;

public class DmDoHistDao extends HistDao implements IDmDoHistDao{

	public int insert(IHistEntity entity , IDmDoDto dmDoDto){
		entity.setHistId( this.getHistNumberingDao().nextval() );
		entity.setDataCtgCd( DmTables.DM_DO.name() );
		entity.setDataId( dmDoDto.getDmDoEntity().getMgtVer() );
		entity.setHistData( "" );
		entity.setDelStsId( StatusFlg.off );
		entity.setLotId( dmDoDto.getRpEntity().getLotId() );
		entity.setStsId( dmDoDto.getDmDoEntity().getStsId() );
		return super.insert( entity );
	}
//ILotDto lotDto =support.findLotDto( entity.getLotId() );
}
