package jp.co.blueship.tri.fw.um.dao.wiki;

import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.IWikiEntity;

/**
 * The interface of the wiki DAO.
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 */
public interface IWikiDao extends IJdbcDao<IWikiEntity> {

}
