package jp.co.blueship.tri.fw.um.dao.ctg.constants;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;

/**
 * The items of the category link entity.
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 */
public enum CtgLnkItems implements ITableItem {
	ctgId("ctg_id"),
	dataCtgCd("data_ctg_cd"),
	dataId("data_id"),
	delStsId("del_sts_id"),
	regUserId("reg_user_id"),
	regUserNm("reg_user_nm"),
	regTimestamp("reg_timestamp"),
	updUserId("upd_user_id"),
	updUserNm("upd_user_nm"),
	updTimestamp("upd_timestamp");

	private String element = null;

	private CtgLnkItems( String element ) {
		this.element = element;
	}

	@Override
	public String getItemName() {
		return element;
	}

}
