package jp.co.blueship.tri.fw.um.dao.user.eb;

import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the user role link entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IUserRoleLnkEntity extends IEntityFooter {

	public String getUserId();
	public void setUserId(String userId);

	public String getRoleId();
	public void setRoleId(String roleId);

}
