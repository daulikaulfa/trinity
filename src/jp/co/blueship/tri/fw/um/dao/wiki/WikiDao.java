package jp.co.blueship.tri.fw.um.dao.wiki;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.fw.um.dao.wiki.constants.WikiItems;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.IWikiEntity;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.WikiEntity;

/**
 * The implements of the wiki DAO.
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 */
public class WikiDao extends JdbcBaseDao<IWikiEntity> implements IWikiDao {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	public IWikiVersionNumberingDao wikiVersionNumberingDao;

	public final void setWikiVersionNumberingDao( IWikiVersionNumberingDao wikiVersionNumberingDao ){
		this.wikiVersionNumberingDao = wikiVersionNumberingDao;
	}

	@Override
	protected ITableAttribute getTableAttribute() {
		return UmTables.UM_WIKI;
	}

	@Override
	protected ISqlBuilder append(ISqlBuilder builder, IWikiEntity entity) {
		builder
			.append(WikiItems.wikiId, entity.getWikiId(), true)
			.append(WikiItems.lotId, entity.getLotId())
			.append(WikiItems.wikiVerNo, wikiVersionNumberingDao.nextval(entity.getWikiId() ))
			.append(WikiItems.pageNm, entity.getPageNm())
			.append(WikiItems.content, entity.getContent())
			.append(WikiItems.isWikiHome, (null == entity.isWikiHome())? StatusFlg.off.parseBoolean() : entity.isWikiHome().parseBoolean())
			.append(WikiItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(WikiItems.regTimestamp, entity.getRegTimestamp())
			.append(WikiItems.regUserId, entity.getRegUserId())
			.append(WikiItems.regUserNm, entity.getRegUserNm())
			.append(WikiItems.updTimestamp, entity.getUpdTimestamp())
			.append(WikiItems.updUserId, entity.getUpdUserId())
			.append(WikiItems.updUserNm, entity.getUpdUserNm())
			;

		return builder;
	}

	@Override
	protected IWikiEntity entityMapping(ResultSet rs, int row) throws SQLException {
		IWikiEntity entity = new WikiEntity();

		entity.setWikiId		( rs.getString(WikiItems.wikiId.getItemName()) );
		entity.setLotId			( rs.getString(WikiItems.lotId.getItemName()) );
		entity.setWikiVerNo		( rs.getInt(WikiItems.wikiVerNo.getItemName()) );
		entity.setPageNm		( rs.getString(WikiItems.pageNm.getItemName()) );
		entity.setContent		( rs.getString(WikiItems.content.getItemName()) );
		entity.setIsWikiHome	( StatusFlg.value(rs.getBoolean(WikiItems.isWikiHome.getItemName())) );
		entity.setDelStsId		( StatusFlg.value(rs.getBoolean(WikiItems.delStsId.getItemName())) );
		entity.setRegTimestamp	( rs.getTimestamp(WikiItems.regTimestamp.getItemName()) );
		entity.setRegUserId		( rs.getString(WikiItems.regUserId.getItemName()) );
		entity.setRegUserNm		( rs.getString(WikiItems.regUserNm.getItemName()) );
		entity.setUpdTimestamp	( rs.getTimestamp(WikiItems.updTimestamp.getItemName()) );
		entity.setUpdUserId		( rs.getString(WikiItems.updUserId.getItemName()) );
		entity.setUpdUserNm		( rs.getString(WikiItems.updUserNm.getItemName()) );

		return entity;
	}
}
