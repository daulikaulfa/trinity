package jp.co.blueship.tri.fw.um.dao.hist;

import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.oxm.utils.TriXmlUtils;
import jp.co.blueship.tri.fw.schema.beans.lot.LotRecDocument;
import jp.co.blueship.tri.fw.schema.beans.lot.LotType;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.utils.HistMappingUtils;

/**
 * The implements of the lot history DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 */
public class LotHistDao extends HistDao implements ILotHistDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	public int insert(IHistEntity entity, ILotDto lotDto){

		LotRecDocument lotDoc = LotRecDocument.Factory.newInstance();
		LotType lotXml = lotDoc.addNewLotRec();
		lotXml = new HistMappingUtils().mapDB2XMLBeans( lotDto,lotXml );

		entity.setHistId( this.getHistNumberingDao().nextval() );
		entity.setDataCtgCd( AmTables.AM_LOT.name() );
		entity.setDataId( lotDto.getLotEntity().getLotId() );
		entity.setHistData( TriXmlUtils.replaceExcessString( lotDoc ) );
		entity.setDelStsId( StatusFlg.off );
		entity.setLotId( lotDto.getLotEntity().getLotId() );
		entity.setStsId( lotDto.getLotEntity().getStsId() );

		return super.insert( entity );
	}
}
