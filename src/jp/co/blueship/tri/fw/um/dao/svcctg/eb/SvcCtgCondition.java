package jp.co.blueship.tri.fw.um.dao.svcctg.eb;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.dao.orm.psql.SqlFormatUtils;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.fw.um.dao.svcctg.constants.SvcCtgItems;

/**
 * The SQL condition of the service catalog entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class SvcCtgCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute svcctg = UmTables.UM_SVC_CTG;

	/**
	 * service ctg-ID
	 */
	public String svcCtgId = null;
	/**
	 * service ctg-ID's
	 */
	public String[] svcCtgIds = null;
	/**
	 * service catalog name
	 */
	public String svcCtgNm = null;
	/**
	 * service catalog content
	 */
	public String svcCtgContent = null;
	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		   super.append(SvcCtgItems.delStsId, StatusFlg.off.parseBoolean() );
	}

	public SvcCtgCondition(){
		super(svcctg);
	}

	/**
	 * service ctg-IDを取得します。
	 * @return service ctg-ID
	 */
	public String getSvcCtgId() {
	    return svcCtgId;
	}

	/**
	 * service ctg-IDを設定します。
	 * @param svcId service ctg-ID
	 */
	public void setSvcCtgId(String svcCtgId) {
	    this.svcCtgId = svcCtgId;
	    super.append(SvcCtgItems.svcCtgId, svcCtgId );
	}

	/**
	 * service ctg-ID'sを取得します。
	 * @return svcctg-ID's
	 */
	public String[] getSvcCtgIds() {
	    return svcCtgIds;
	}

	/**
	 * service ctg-ID'sを設定します。
	 * @param svcIds svcctg-ID's
	 */
	public void setSvcCtgIds(String[] svcCtgIds) {
	    this.svcCtgIds = svcCtgIds;
	    super.append( SvcCtgItems.svcCtgId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(svcCtgIds, SvcCtgItems.svcCtgId.getItemName(), false, true, false) );
	}

	/**
	 * service catalog nameを取得します。
	 * @return service catalog name
	 */
	public String getSvcCtgNm() {
	    return svcCtgNm;
	}

	/**
	 * service catalog nameを設定します。
	 * @param svcCtgNm service catalog name
	 */
	public void setSvcCtgNm(String svcCtgNm) {
	    this.svcCtgNm = svcCtgNm;
	    super.append(SvcCtgItems.svcCtgNm, svcCtgNm );
	}

	/**
	 * service catalog contentを取得します。
	 * @return service catalog content
	 */
	public String getSvcCtgContent() {
	    return svcCtgContent;
	}

	/**
	 * service catalog contentを設定します。
	 * @param svcCtgContent service catalog content
	 */
	public void setSvcCtgContent(String svcCtgContent) {
	    this.svcCtgContent = svcCtgContent;
	    super.append(SvcCtgItems.svcCtgContent, svcCtgContent );
	}

	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	    super.append( SvcCtgItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}
}