package jp.co.blueship.tri.fw.um.dao.project;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.fw.um.dao.project.constants.ProjectItems;
import jp.co.blueship.tri.fw.um.dao.project.eb.IProjectEntity;
import jp.co.blueship.tri.fw.um.dao.project.eb.ProjectEntity;

/**
 * The implements of the project DAO.
 *
 * @version V4.00.00
 * @author Hai Thach
 */
public class ProjectDao extends JdbcBaseDao<IProjectEntity> implements IProjectDao{
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected final ITableAttribute getTableAttribute() {
		return UmTables.UM_PROJECT;
	}
	
	@Override
	protected ISqlBuilder append (ISqlBuilder builder, IProjectEntity entity) {
		builder
			.append(ProjectItems.productId, entity.getProductId(), true)
			.append(ProjectItems.projectNm, entity.getProjectNm())
			.append(ProjectItems.iconTyp, entity.getIconTyp())
			.append(ProjectItems.defaultIconPath, entity.getDefaultIconPath())
			.append(ProjectItems.customIconPath, entity.getCustomIconPath())
			.append(ProjectItems.lang, entity.getLanguage())
			.append(ProjectItems.timezone, entity.getTimezone())
			.append(ProjectItems.notice, entity.getNotice())
			.append(ProjectItems.delStsId, (null == entity.getDelStsId()) ? null : entity.getDelStsId().parseBoolean())
			.append(ProjectItems.regUserId, entity.getRegUserId())
			.append(ProjectItems.regUserNm, entity.getRegUserNm())
			.append(ProjectItems.regTimestamp, entity.getRegTimestamp())
			.append(ProjectItems.updUserId, entity.getUpdUserId())
			.append(ProjectItems.updUserNm, entity.getUpdUserNm())
			.append(ProjectItems.updTimestamp, entity.getUpdTimestamp())
			.append(ProjectItems.noticeTimestamp, entity.getNoticeTimestamp())
			.append(ProjectItems.lookAndFeel, entity.getLookAndFeel())
		;
		
		return builder;
	}
	
	@Override
	protected final IProjectEntity entityMapping ( ResultSet rs, int row ) throws SQLException {
		IProjectEntity entity = new ProjectEntity();
		
		entity.setProductId(rs.getString(ProjectItems.productId.getItemName()));
		entity.setProjectNm(rs.getString(ProjectItems.projectNm.getItemName()));
		entity.setIconTyp(rs.getString(ProjectItems.iconTyp.getItemName()));
		entity.setDefaultIconPath(rs.getString(ProjectItems.defaultIconPath.getItemName()));
		entity.setCustomIconPath(rs.getString(ProjectItems.customIconPath.getItemName()));
		entity.setLanguage(rs.getString(ProjectItems.lang.getItemName()));
		entity.setTimezone(rs.getString(ProjectItems.timezone.getItemName()));
		entity.setNotice(rs.getString(ProjectItems.notice.getItemName()));
		entity.setDelStsId(StatusFlg.value(rs.getBoolean(ProjectItems.delStsId.getItemName())));
		entity.setRegTimestamp(rs.getTimestamp(ProjectItems.regTimestamp.getItemName()));
		entity.setRegUserId(rs.getString(ProjectItems.regUserId.getItemName()));
		entity.setRegUserNm(rs.getString(ProjectItems.regUserNm.getItemName()));
		entity.setUpdTimestamp(rs.getTimestamp(ProjectItems.updTimestamp.getItemName()));
		entity.setUpdUserId(rs.getString(ProjectItems.updUserId.getItemName()));
		entity.setUpdUserNm(rs.getString(ProjectItems.updUserNm.getItemName()));
		entity.setNoticeTimestamp(rs.getTimestamp(ProjectItems.noticeTimestamp.getItemName()));
		entity.setLookAndFeel(rs.getString(ProjectItems.lookAndFeel.getItemName()));
		
		return entity;
	}
}
