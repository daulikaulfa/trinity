package jp.co.blueship.tri.fw.um.dao.mstone.eb;

import jp.co.blueship.tri.fw.dao.orm.EntityFooter;

/**
 * milestone entity.
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 */
public class MstoneEntity extends EntityFooter implements IMstoneEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * milestone-id
	 */
	public String mstoneId = null;

	/**
	 * milestone name
	 */
	public String mstoneNm = null;

	/**
	 * milestone start date
	 */
	public String mstoneStDate = null;

	/**
	 * milestone end date
	 */
	public String mstoneEndDate = null;

	/**
	 * content
	 */
	public String content = null;

	/**
	 * lot id
	 */
	public String lotId = null;

	/**
	 * sort order
	 */
	public Integer sortOdr = null;

	/**
	 * process status-ID
	 */
	public String procStsId = null;

	@Override
	public String getMstoneId() {
		return mstoneId;
	}
	@Override
	public void setMstoneId(String mstoneId) {
		this.mstoneId = mstoneId;
	}

	@Override
	public String getMstoneNm() {
		return mstoneNm;
	}
	@Override
	public void setMstoneNm(String mstoneNm) {
		this.mstoneNm = mstoneNm;
	}

	@Override
	public String getMstoneStDate() {
		return mstoneStDate;
	}
	@Override
	public void setMstoneStDate(String mstoneStDate) {
		this.mstoneStDate = mstoneStDate;
	}

	@Override
	public String getMstoneEndDate() {
		return mstoneEndDate;
	}
	@Override
	public void setMstoneEndDate(String mstoneEndDate) {
		this.mstoneEndDate = mstoneEndDate;
	}

	@Override
	public String getContent() {
		return content;
	}
	@Override
	public void setContent(String content) {
		this.content = content;
	}

	@Override
	public String getLotId() {
		return lotId;
	}
	@Override
	public void setLotId(String lotId) {
		this.lotId = lotId;
	}

	@Override
	public Integer getSortOdr() {
		return sortOdr;
	}
	@Override
	public void setSortOdr(Integer sortOdr) {
		this.sortOdr = sortOdr;
	}

}
