package jp.co.blueship.tri.fw.um.dao.dept.eb;

import jp.co.blueship.tri.fw.dao.orm.EntityFooter;

/**
 * user department entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 * 
 * @version V4.00.00
 * @author le.thixuan
 */
public class DeptEntity extends EntityFooter implements IDeptEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * dept-ID
	 */
	public String deptId = null;
	/**
	 * dept. name
	 */
	public String deptNm = null;
	
	/**
	 * sort order
	 */
	public Integer sortOdr = null;

	/**
	 * dept-IDを取得します。
	 * @return dept-ID
	 */
	public String getDeptId() {
	    return deptId;
	}
	/**
	 * dept-IDを設定します。
	 * @param deptId dept-ID
	 */
	public void setDeptId(String deptId) {
	    this.deptId = deptId;
	}
	/**
	 * dept. nameを取得します。
	 * @return dept. name
	 */
	public String getDeptNm() {
	    return deptNm;
	}
	/**
	 * dept. nameを設定します。
	 * @param deptName dept. name
	 */
	public void setDeptNm(String deptNm) {
	    this.deptNm = deptNm;
	}
	
	@Override
	public Integer getSortOdr() {
		return sortOdr;
	}
	@Override
	public void setSortOdr(Integer sortOdr) {
		this.sortOdr = sortOdr;
	}

}
