package jp.co.blueship.tri.fw.um.dao.user.eb;

import jp.co.blueship.tri.fw.dao.orm.EntityFooter;

/**
 * user password history entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class UserPassHistEntity extends EntityFooter implements IUserPassHistEntity{

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * user-ID
	 */
	public String userId = null;
	/**
	 * pass history sequential number
	 */
	public String passHistSeqNo = null;
	/**
	 * user password
	 */
	public String userPass = null;
	/**
	 * user-IDを取得します。
	 * @return user-ID
	 */
	public String getUserId() {
	    return userId;
	}
	/**
	 * user-IDを設定します。
	 * @param userId user-ID
	 */
	public void setUserId(String userId) {
	    this.userId = userId;
	}
	/**
	 * pass history sequential numberを取得します。
	 * @return pass  history sequential number no
	 */
	public String getPassHistSeqNo() {
	    return passHistSeqNo;
	}
	/**
	 * pass  history sequential number noを設定します。
	 * @param passHistSeqNo pass  history sequential number no
	 */
	public void setPassHistSeqNo(String passHistSeqNo) {
	    this.passHistSeqNo = passHistSeqNo;
	}
	/**
	 * user passwordを取得します。
	 * @return user password
	 */
	public String getUserPass() {
	    return userPass;
	}
	/**
	 * user passwordを設定します。
	 * @param userPass user password
	 */
	public void setUserPass(String userPass) {
	    this.userPass = userPass;
	}

}