package jp.co.blueship.tri.fw.um.dao.grp;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.fw.um.dao.grp.constants.GrpRoleLnkItems;
import jp.co.blueship.tri.fw.um.dao.grp.eb.GrpRoleLnkEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpRoleLnkEntity;

/**
 * The implements of the group role link DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 */
public class GrpRoleLnkDao extends JdbcBaseDao<IGrpRoleLnkEntity> implements IGrpRoleLnkDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected final ITableAttribute getTableAttribute() {
		return UmTables.UM_GRP_ROLE_LNK;
	}

	@Override
	protected final ISqlBuilder append( ISqlBuilder builder, IGrpRoleLnkEntity entity ) {
		builder
			.append(GrpRoleLnkItems.grpId, entity.getGrpId(), true)
			.append(GrpRoleLnkItems.roleId, entity.getRoleId())
			.append(GrpRoleLnkItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(GrpRoleLnkItems.regTimestamp, entity.getRegTimestamp())
			.append(GrpRoleLnkItems.regUserId, entity.getRegUserId())
			.append(GrpRoleLnkItems.regUserNm, entity.getRegUserNm())
			.append(GrpRoleLnkItems.updTimestamp, entity.getUpdTimestamp())
			.append(GrpRoleLnkItems.updUserId, entity.getUpdUserId())
			.append(GrpRoleLnkItems.updUserNm, entity.getUpdUserNm())
			;

		return builder;
	}

	@Override
	protected final IGrpRoleLnkEntity entityMapping( ResultSet rs, int row ) throws SQLException {
  	  IGrpRoleLnkEntity entity = new GrpRoleLnkEntity();

  	  entity.setGrpId( rs.getString(GrpRoleLnkItems.grpId.getItemName()) );
  	  entity.setRoleId( rs.getString(GrpRoleLnkItems.roleId.getItemName()) );
  	  entity.setDelStsId( StatusFlg.value(rs.getBoolean(GrpRoleLnkItems.delStsId.getItemName())) );
  	  entity.setRegTimestamp( rs.getTimestamp(GrpRoleLnkItems.regTimestamp.getItemName()) );
  	  entity.setRegUserId( rs.getString(GrpRoleLnkItems.regUserId.getItemName()) );
  	  entity.setRegUserNm( rs.getString(GrpRoleLnkItems.regUserNm.getItemName()) );
  	  entity.setUpdTimestamp( rs.getTimestamp(GrpRoleLnkItems.updTimestamp.getItemName()) );
  	  entity.setUpdUserId( rs.getString(GrpRoleLnkItems.updUserId.getItemName()) );
  	  entity.setUpdUserNm( rs.getString(GrpRoleLnkItems.updUserNm.getItemName()) );

  	  return entity;
	}

}
