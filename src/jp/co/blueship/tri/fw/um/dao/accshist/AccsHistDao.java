package jp.co.blueship.tri.fw.um.dao.accshist;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;
import jp.co.blueship.tri.fw.um.dao.accshist.constants.AccsHistItems;
import jp.co.blueship.tri.fw.um.dao.accshist.eb.AccsHistEntity;
import jp.co.blueship.tri.fw.um.dao.accshist.eb.IAccsHistEntity;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;

/**
 * The implements of the access history DAO.
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 */
public class AccsHistDao extends JdbcBaseDao<IAccsHistEntity> implements IAccsHistDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected final ITableAttribute getTableAttribute() {
		return UmTables.UM_ACCS_HIST;
	}

	@Override
	protected final ISqlBuilder append( ISqlBuilder builder, IAccsHistEntity entity ) {
		builder
			.append(AccsHistItems.userId, entity.getUserId(), true)
			.append(AccsHistItems.dataCtgCd, entity.getDataCtgCd(), true)
			.append(AccsHistItems.dataId, entity.getDataId(), true)
			.append(AccsHistItems.accsTimestamp, entity.getAccsTimestamp())
			.append(AccsHistItems.noticeViewTimestamp, entity.getNoticeViewTimestamp())
			.append(AccsHistItems.userNm, entity.getUserNm())
			.append(AccsHistItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(AccsHistItems.regTimestamp, entity.getRegTimestamp())
			.append(AccsHistItems.regUserId, entity.getRegUserId())
			.append(AccsHistItems.regUserNm, entity.getRegUserNm())
			.append(AccsHistItems.updTimestamp, entity.getUpdTimestamp())
			.append(AccsHistItems.updUserId, entity.getUpdUserId())
			.append(AccsHistItems.updUserNm, entity.getUpdUserNm())
			;

		return builder;
	}

	@Override
	protected final IAccsHistEntity entityMapping( ResultSet rs, int row ) throws SQLException {
  	  IAccsHistEntity entity = new AccsHistEntity();

  	  entity.setUserId( rs.getString(AccsHistItems.userId.getItemName()) );
  	  entity.setDataCtgCd( rs.getString(AccsHistItems.dataCtgCd.getItemName()) );
  	  entity.setDataId( rs.getString(AccsHistItems.dataId.getItemName()) );
  	  entity.setAccsTimestamp( rs.getTimestamp(AccsHistItems.accsTimestamp.getItemName()) );
  	  entity.setNoticeViewTimestamp( rs.getTimestamp(AccsHistItems.noticeViewTimestamp.getItemName()) );
  	  entity.setUserNm( rs.getString(AccsHistItems.userNm.getItemName()) );
  	  entity.setDelStsId( StatusFlg.value(rs.getBoolean(AccsHistItems.delStsId.getItemName())) );
  	  entity.setRegTimestamp( rs.getTimestamp(AccsHistItems.regTimestamp.getItemName()) );
  	  entity.setRegUserId( rs.getString(AccsHistItems.regUserId.getItemName()) );
  	  entity.setRegUserNm( rs.getString(AccsHistItems.regUserNm.getItemName()) );
  	  entity.setUpdTimestamp( rs.getTimestamp(AccsHistItems.updTimestamp.getItemName()) );
  	  entity.setUpdUserId( rs.getString(AccsHistItems.updUserId.getItemName()) );
  	  entity.setUpdUserNm( rs.getString(AccsHistItems.updUserNm.getItemName()) );

  	  return entity;
	}

}
