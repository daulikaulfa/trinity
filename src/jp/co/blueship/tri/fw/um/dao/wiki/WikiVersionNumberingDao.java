package jp.co.blueship.tri.fw.um.dao.wiki;

import jp.co.blueship.tri.fw.dao.orm.ex.TriJdbcDaoException;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.sm.dao.autonumbering.IAutoNumberingDao;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;

/**
 * The implements of the Lot numbering DAO.
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class WikiVersionNumberingDao implements IWikiVersionNumberingDao {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	private IAutoNumberingDao numberingDao = null;

	/**
	 * インスタンス生成時に自動的に設定されます。
	 * @param numberingDao アクセスインタフェース
	 */
	public void setAutoNumberingDao( IAutoNumberingDao numberingDao ) {
		this.numberingDao = numberingDao;
	}

	@Override
	public Integer nextval( String id ) {
		String key = UmTables.UM_WIKI_HIST + "_" + id;
		try {
			return Integer.parseInt( numberingDao.nextval( key, "0000/00/00", "0" ) );
		} catch ( Exception e ) {
			throw new TriJdbcDaoException( SmMessageId.SM005008S, e, key );
		}
	}

}
