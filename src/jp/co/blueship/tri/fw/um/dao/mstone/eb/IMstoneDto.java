package jp.co.blueship.tri.fw.um.dao.mstone.eb;

/**
 * The interface of the milestone DTO.
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 */
public interface IMstoneDto {
	/**
	 * get a milestone entity
	 *
	 * @return Milestone Entity
	 */
	public IMstoneEntity getMstoneEntity();

	/**
	 * set a milestone entity
	 *
	 * @param mstoneEntity
	 */
	public void setMstoneEntity(IMstoneEntity mstoneEntity);
}
