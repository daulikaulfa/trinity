package jp.co.blueship.tri.fw.um.dao.svcctg.eb;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.dao.orm.psql.SqlFormatUtils;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.fw.um.dao.svcctg.constants.SvcCtgSvcLnkItems;

/**
 * The SQL condition of the service catalog service link entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class SvcCtgSvcLnkCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute svcctg = UmTables.UM_SVC_CTG_SVC_LNK;

	/**
	 * svc-ID
	 */
	public String svcId = null;
	/**
	 * svc-ID's
	 */
	public String[] svcIds = null;
	/**
	 * svc-ctg-ID
	 */
	public String svcCtgId = null;
	/**
	 * svc-ctg-ID's
	 */
	public String[] svcCtgIds = null;
	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		   super.append(SvcCtgSvcLnkItems.delStsId, StatusFlg.off.parseBoolean() );
	}

	public SvcCtgSvcLnkCondition(){
		super(svcctg);
	}

	/**
	 * svc-IDを取得します。
	 * @return svc-ID
	 */
	public String getSvcId() {
	    return svcId;
	}

	/**
	 * svc-IDを設定します。
	 * @param svcId svc-ID
	 */
	public void setSvcId(String svcId) {
	    this.svcId = svcId;
	    super.append(SvcCtgSvcLnkItems.svcId, svcId );
	}

	/**
	 * svc-ID'sを取得します。
	 * @return svcctg-ID's
	 */
	public String[] getSvcIds() {
	    return svcIds;
	}

	/**
	 * svc-ID'sを設定します。
	 * @param svcIds svc-ID's
	 */
	public void setSvcIds(String[] svcIds) {
	    this.svcIds = svcIds;
	    super.append( SvcCtgSvcLnkItems.svcId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(svcIds, SvcCtgSvcLnkItems.svcId.getItemName(), false, true, false) );
	}

	/**
	 * svc-ctg-IDを取得します。
	 * @return svc-ctg-ID
	 */
	public String getSvcCtgId() {
	    return svcCtgId;
	}

	/**
	 * svc-ctg-IDを設定します。
	 * @param svcCtgId svc-ctg-ID
	 */
	public void setSvcCtgId(String svcCtgId) {
	    this.svcCtgId = svcCtgId;
	    super.append(SvcCtgSvcLnkItems.svcCtgId, svcCtgId );
	}

	/**
	 * svc-ctg-ID'sを取得します。
	 * @return svc-ctg-ID's
	 */
	public String[] getSvcCtgIds() {
	    return svcCtgIds;
	}

	/**
	 * svc-ctg-ID'sを設定します。
	 * @param svcCtgIds svc-ctg-ID's
	 */
	public void setSvcCtgIds(String[] svcCtgIds) {
	    this.svcCtgIds = svcCtgIds;
	    super.append( SvcCtgSvcLnkItems.svcCtgId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(svcCtgIds, SvcCtgSvcLnkItems.svcCtgId.getItemName(), false, true, false) );
	}

	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	    super.append( SvcCtgSvcLnkItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}
}