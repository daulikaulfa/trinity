package jp.co.blueship.tri.fw.um.dao.filter;

import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;
import jp.co.blueship.tri.fw.um.dao.filter.eb.IFilterEntity;

/**
 * The interface of the filter DAO.
 *
 * @version V4.00.00
 * @author Yohei Tanaka
 */
public interface IFilterDao extends IJdbcDao<IFilterEntity> {

}