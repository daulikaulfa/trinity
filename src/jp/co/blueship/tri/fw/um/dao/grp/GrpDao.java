package jp.co.blueship.tri.fw.um.dao.grp;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.fw.um.dao.grp.constants.GrpItems;
import jp.co.blueship.tri.fw.um.dao.grp.eb.GrpEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;

/**
 * The implements of the group DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 * 
 * @version V4.00.00
 * @author le.thixuan
 */
public class GrpDao extends JdbcBaseDao<IGrpEntity> implements IGrpDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected final ITableAttribute getTableAttribute() {
		return UmTables.UM_GRP;
	}

	@Override
	protected final ISqlBuilder append( ISqlBuilder builder, IGrpEntity entity ) {
		builder
			.append(GrpItems.grpId, entity.getGrpId(), true)
			.append(GrpItems.grpNm, entity.getGrpNm())
			.append(GrpItems.sortOdr, entity.getSortOdr())
			.append(GrpItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(GrpItems.regTimestamp, entity.getRegTimestamp())
			.append(GrpItems.regUserId, entity.getRegUserId())
			.append(GrpItems.regUserNm, entity.getRegUserNm())
			.append(GrpItems.updTimestamp, entity.getUpdTimestamp())
			.append(GrpItems.updUserId, entity.getUpdUserId())
			.append(GrpItems.updUserNm, entity.getUpdUserNm())
			;

		return builder;
	}

	@Override
	protected final IGrpEntity entityMapping( ResultSet rs, int row ) throws SQLException {
  	  IGrpEntity entity = new GrpEntity();

  	  entity.setGrpId( rs.getString(GrpItems.grpId.getItemName()) );
  	  entity.setGrpNm( rs.getString(GrpItems.grpNm.getItemName()) );
  	  entity.setSortOdr( rs.getInt(GrpItems.sortOdr.getItemName()) );
  	  entity.setDelStsId( StatusFlg.value(rs.getBoolean(GrpItems.delStsId.getItemName())) );
  	  entity.setRegTimestamp( rs.getTimestamp(GrpItems.regTimestamp.getItemName()) );
  	  entity.setRegUserId( rs.getString(GrpItems.regUserId.getItemName()) );
  	  entity.setRegUserNm( rs.getString(GrpItems.regUserNm.getItemName()) );
  	  entity.setUpdTimestamp( rs.getTimestamp(GrpItems.updTimestamp.getItemName()) );
  	  entity.setUpdUserId( rs.getString(GrpItems.updUserId.getItemName()) );
  	  entity.setUpdUserNm( rs.getString(GrpItems.updUserNm.getItemName()) );

  	  return entity;
	}

}
