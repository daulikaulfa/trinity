package jp.co.blueship.tri.fw.um.dao.user;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.fw.um.dao.user.constants.UserPassHistItems;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserPassHistEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserPassHistEntity;

/**
 * The implements of the user password histry DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 */
public class UserPassHistDao extends JdbcBaseDao<IUserPassHistEntity> implements IUserPassHistDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected final ITableAttribute getTableAttribute() {
		return UmTables.UM_USER_PASS_HIST;
	}

	@Override
	protected final ISqlBuilder append( ISqlBuilder builder, IUserPassHistEntity entity ) {
		builder
			.append(UserPassHistItems.userId, entity.getUserId(), true)
			.append(UserPassHistItems.passHistSeqNo, entity.getPassHistSeqNo(), true)
			.append(UserPassHistItems.userPass, entity.getUserPass())
			.append(UserPassHistItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(UserPassHistItems.regTimestamp, entity.getRegTimestamp())
			.append(UserPassHistItems.regUserId, entity.getRegUserId())
			.append(UserPassHistItems.regUserNm, entity.getRegUserNm())
			.append(UserPassHistItems.updTimestamp, entity.getUpdTimestamp())
			.append(UserPassHistItems.updUserId, entity.getUpdUserId())
			.append(UserPassHistItems.updUserNm, entity.getUpdUserNm())
			;

		return builder;
	}

	@Override
	protected final IUserPassHistEntity entityMapping( ResultSet rs, int row ) throws SQLException {
  	  IUserPassHistEntity entity = new UserPassHistEntity();

  	  entity.setUserId( rs.getString(UserPassHistItems.userId.getItemName()) );
  	  entity.setPassHistSeqNo( rs.getString(UserPassHistItems.passHistSeqNo.getItemName()) );
  	  entity.setUserPass( rs.getString(UserPassHistItems.userPass.getItemName()) );
  	  entity.setDelStsId( StatusFlg.value(rs.getBoolean(UserPassHistItems.delStsId.getItemName())) );
  	  entity.setRegTimestamp( rs.getTimestamp(UserPassHistItems.regTimestamp.getItemName()) );
  	  entity.setRegUserId( rs.getString(UserPassHistItems.regUserId.getItemName()) );
  	  entity.setRegUserNm( rs.getString(UserPassHistItems.regUserNm.getItemName()) );
  	  entity.setUpdTimestamp( rs.getTimestamp(UserPassHistItems.updTimestamp.getItemName()) );
  	  entity.setUpdUserId( rs.getString(UserPassHistItems.updUserId.getItemName()) );
  	  entity.setUpdUserNm( rs.getString(UserPassHistItems.updUserNm.getItemName()) );

  	  return entity;
	}

}
