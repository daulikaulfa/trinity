package jp.co.blueship.tri.fw.um.dao.accshist.constants;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;


/**
 * The items of the access history entity.
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public enum AccsHistItems implements ITableItem {
	userId("user_Id"),
	dataCtgCd("data_ctg_cd"),
	dataId("data_id"),
	accsTimestamp("accs_timestamp"),
	noticeViewTimestamp("notice_view_timestamp"),
	userNm("user_nm"),
	delStsId("del_sts_id"),
	regUserId("reg_user_id"),
	regUserNm("reg_user_nm"),
	regTimestamp("reg_timestamp"),
	updUserId("upd_user_id"),
	updUserNm("upd_user_nm"),
	updTimestamp("upd_timestamp");

	private String element = null;

	private AccsHistItems( String element) {
		this.element = element;
	}

	public String getItemName() {
		return this.element;
	}

}
