package jp.co.blueship.tri.fw.um.dao.user.eb;

import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * 
 * @version V4.00.00
 * @author le.thixuan
 */
public interface IUserUrlEntity extends IEntityFooter{
	
	public String getUserId();
	public void setUserId(String userId);
	
	public String getUrlSeqNo();
	public void setUrlSeqNo(String urlSeqNo);
	
	public String getUrl();
	public void setUrl(String url);
}
