package jp.co.blueship.tri.fw.um.dao.mstone.eb;

/**
 * The interface of the milestone link DTO.
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 */
public interface IMstoneLnkDto {
	/**
	 * get a milestone link entity
	 *
	 * @return Milestone Link Entity
	 */
	public IMstoneLnkEntity getMstoneLnkEntity();

	/**
	 * set a milestone link entity
	 *
	 * @param mstoneLnkEntity
	 */
	public void setMstoneLnkEntity(IMstoneLnkEntity mstoneLnkEntity);
}
