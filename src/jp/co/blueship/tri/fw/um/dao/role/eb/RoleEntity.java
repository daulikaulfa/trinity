package jp.co.blueship.tri.fw.um.dao.role.eb;

import jp.co.blueship.tri.fw.dao.orm.EntityFooter;

/**
 * role entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 * 
 * @version V4.00.00
 * @author le.thixuan
 */
public class RoleEntity extends EntityFooter implements IRoleEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * role-ID
	 */
	public String roleId = null;
	/**
	 * role name
	 */
	public String roleName = null;
	
	/**
	 * sort order
	 */
	public Integer sortOdr = null;


	/**
	 * role-IDを取得します。
	 * @return role-ID
	 */
	public String getRoleId() {
	    return roleId;
	}
	/**
	 * role-IDを設定します。
	 * @param roleId role-ID
	 */
	public void setRoleId(String roleId) {
	    this.roleId = roleId;
	}
	/**
	 * role nameを取得します。
	 * @return role name
	 */
	public String getRoleName() {
	    return roleName;
	}
	/**
	 * role nameを設定します。
	 * @param roleName role name
	 */
	public void setRoleName(String roleName) {
	    this.roleName = roleName;
	}
	
	@Override
	public Integer getSortOdr() {
		return sortOdr;
	}
	
	@Override
	public void setSortOdr(Integer sortOdr) {
		this.sortOdr = sortOdr;
	}

}