package jp.co.blueship.tri.fw.um.dao.wiki;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.fw.um.dao.hist.HistDao;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.IWikiEntity;

/**
 * The implements of the wikiHist DAO.
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 */
public class WikiUpdateHistDao extends HistDao implements IWikiUpdateHistDao {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public int insert(IHistEntity entity, IWikiEntity wikiEntity) {
		entity.setHistId( this.getHistNumberingDao().nextval() );
		entity.setDataCtgCd( UmTables.UM_WIKI.name() );
		entity.setDataId( wikiEntity.getWikiId() );
		entity.setHistData( "" );
		entity.setDelStsId( StatusFlg.off );
		entity.setLotId( wikiEntity.getLotId() );
		entity.setStsId( "" );

		return super.insert(entity);
	}



}
