package jp.co.blueship.tri.fw.um.dao.user;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.fw.um.dao.user.constants.UserItems;
import jp.co.blueship.tri.fw.um.dao.user.constants.UserUrlItems;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserUrlEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserUrlEntity;

/**
 * 
 * @version V4.00.00
 * @author le.thixuan
 */
public class UserUrlDao extends JdbcBaseDao<IUserUrlEntity> implements IUserUrlDao {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected ITableAttribute getTableAttribute() {
		return UmTables.UM_USER_URL;
	}

	@Override
	protected ISqlBuilder append(ISqlBuilder builder, IUserUrlEntity entity) {
		builder
		.append(UserUrlItems.userId, entity.getUserId(), true)
		.append(UserUrlItems.urlSeqNo, entity.getUrlSeqNo())
		.append(UserUrlItems.url, entity.getUrl())
		.append(UserUrlItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
		.append(UserUrlItems.regTimestamp, entity.getRegTimestamp())
		.append(UserUrlItems.regUserId, entity.getRegUserId())
		.append(UserUrlItems.regUserNm, entity.getRegUserNm())
		.append(UserUrlItems.updTimestamp, entity.getUpdTimestamp())
		.append(UserUrlItems.updUserId, entity.getUpdUserId())
		.append(UserUrlItems.updUserNm, entity.getUpdUserNm());
	return builder;
	}

	@Override
	protected IUserUrlEntity entityMapping(ResultSet rs, int row)
			throws SQLException {
		UserUrlEntity entity = new UserUrlEntity();
		
		entity.setUserId( rs.getString(UserUrlItems.userId.getItemName()) );
		entity.setUrlSeqNo( rs.getString(UserUrlItems.urlSeqNo.getItemName()) );
		entity.setUrl( rs.getString(UserUrlItems.url.getItemName()) );
		entity.setDelStsId( StatusFlg.value(rs.getBoolean(UserItems.delStsId.getItemName())) );
		entity.setRegTimestamp( rs.getTimestamp(UserItems.regTimestamp.getItemName()) );
	  	entity.setRegUserId( rs.getString(UserItems.regUserId.getItemName()) );
	  	entity.setRegUserNm( rs.getString(UserItems.regUserNm.getItemName()) );
	  	entity.setUpdTimestamp( rs.getTimestamp(UserItems.updTimestamp.getItemName()) );
	  	entity.setUpdUserId( rs.getString(UserItems.updUserId.getItemName()) );
	  	entity.setUpdUserNm( rs.getString(UserItems.updUserNm.getItemName()) );
	  	
		return entity;
	}

}
