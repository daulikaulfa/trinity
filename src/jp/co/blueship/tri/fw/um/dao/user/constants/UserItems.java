package jp.co.blueship.tri.fw.um.dao.user.constants;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;


/**
 * The items of the user entity.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public enum UserItems implements ITableItem {
	userId("user_Id"),
	userPass("user_pass"),
	passTimeLimit("pass_time_limit"),
	userNm("user_nm"),
	emailAddr("email_addr"),
	deptId("dept_id"),
	deptNm("dept_nm"),
	lang("lang"),
	timezone("timezone"),
	userLocaltion("user_location"),
	biography("biography"),
	iconTyp("icon_typ"),
	defaultIconPath("default_icon_path"),
	customIconPath("custom_icon_path"),
	genderTyp("gender_typ"),
	isReceiveMail("is_receive_mail"),
	delStsId("del_sts_id"),
	regUserId("reg_user_id"),
	regUserNm("reg_user_nm"),
	regTimestamp("reg_timestamp"),
	updUserId("upd_user_id"),
	updUserNm("upd_user_nm"),
	updTimestamp("upd_timestamp"),
	lastLoginTimestamp("last_login_timestamp"),
	lookAndFeel("look_and_feel");

	private String element = null;

	private UserItems( String element) {
		this.element = element;
	}

	public String getItemName() {
		return this.element;
	}

}
