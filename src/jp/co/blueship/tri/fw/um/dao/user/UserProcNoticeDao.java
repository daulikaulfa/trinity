package jp.co.blueship.tri.fw.um.dao.user;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.fw.um.dao.user.constants.UserProcNoticeItems;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserProcNoticeEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserProcNoticeEntity;

/**
 * The implements of the user proc notice DAO.
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 */
public class UserProcNoticeDao extends JdbcBaseDao<IUserProcNoticeEntity> implements IUserProcNoticeDao{

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected ITableAttribute getTableAttribute() {
		return UmTables.UM_USER_PROC_NOTICE;
	}

	@Override
	protected ISqlBuilder append(ISqlBuilder builder, IUserProcNoticeEntity entity) {
		builder
			.append(UserProcNoticeItems.procId, entity.getProcId() , true)
			.append(UserProcNoticeItems.userId, entity.getUserId() ,  true)
			.append(UserProcNoticeItems.isRead, (null == entity.getIsRead())? null: entity.getIsRead().parseBoolean())
			.append(UserProcNoticeItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(UserProcNoticeItems.regTimestamp, entity.getRegTimestamp())
			.append(UserProcNoticeItems.regUserId, entity.getRegUserId())
			.append(UserProcNoticeItems.regUserNm, entity.getRegUserNm())
			.append(UserProcNoticeItems.updTimestamp, entity.getUpdTimestamp())
			.append(UserProcNoticeItems.updUserId, entity.getUpdUserId())
			.append(UserProcNoticeItems.updUserNm, entity.getUpdUserNm())
		;

		return builder;
	}

	@Override
	protected IUserProcNoticeEntity entityMapping(ResultSet rs, int row) throws SQLException {
		IUserProcNoticeEntity entity = new UserProcNoticeEntity();

		entity.setProcId( rs.getString(UserProcNoticeItems.procId.getItemName()) );
		entity.setUserId( rs.getString(UserProcNoticeItems.userId.getItemName()) );
		entity.setIsRead( StatusFlg.value(rs.getBoolean(UserProcNoticeItems.isRead.getItemName())) );
		entity.setDelStsId( StatusFlg.value(rs.getBoolean(UserProcNoticeItems.delStsId.getItemName())) );
		entity.setRegTimestamp( rs.getTimestamp(UserProcNoticeItems.regTimestamp.getItemName()) );
		entity.setRegUserId( rs.getString(UserProcNoticeItems.regUserId.getItemName()) );
		entity.setRegUserNm( rs.getString(UserProcNoticeItems.regUserNm.getItemName()) );
		entity.setUpdTimestamp( rs.getTimestamp(UserProcNoticeItems.updTimestamp.getItemName()) );
		entity.setUpdUserId( rs.getString(UserProcNoticeItems.updUserId.getItemName()) );
		entity.setUpdUserNm( rs.getString(UserProcNoticeItems.updUserNm.getItemName()) );

		return entity;
	}

}
