package jp.co.blueship.tri.fw.um.dao.hist;

import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;

/**
 * The implements of the Lot History DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 */
public interface ILotHistDao extends IHistDao {

	/**
	 * 登録処理を行います
	 *
	 * @param entity
	 * @return the number of rows affected1
	 */
	public int insert(IHistEntity entity , ILotDto lotDto);
}