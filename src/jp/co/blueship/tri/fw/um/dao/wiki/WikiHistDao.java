package jp.co.blueship.tri.fw.um.dao.wiki;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.fw.um.dao.wiki.constants.WikiHistItems;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.IWikiHistEntity;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.WikiHistEntity;

/**
 * The implements of the wikiHist DAO.
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 */
public class WikiHistDao extends JdbcBaseDao<IWikiHistEntity> implements IWikiHistDao {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected ITableAttribute getTableAttribute() {
		return UmTables.UM_WIKI_HIST;
	}

	@Override
	protected ISqlBuilder append(ISqlBuilder builder, IWikiHistEntity entity) {
		builder
			.append(WikiHistItems.wikiId, entity.getWikiId(), true)
			.append(WikiHistItems.wikiVerNo, entity.getWikiVerNo(), true)
			.append(WikiHistItems.pageNm, entity.getPageNm())
			.append(WikiHistItems.content, entity.getContent())
			.append(WikiHistItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(WikiHistItems.regTimestamp, entity.getRegTimestamp())
			.append(WikiHistItems.regUserId, entity.getRegUserId())
			.append(WikiHistItems.regUserNm, entity.getRegUserNm())
			.append(WikiHistItems.updTimestamp, entity.getUpdTimestamp())
			.append(WikiHistItems.updUserId, entity.getUpdUserId())
			.append(WikiHistItems.updUserNm, entity.getUpdUserNm())
			;

		return builder;
	}

	@Override
	protected IWikiHistEntity entityMapping(ResultSet rs, int row) throws SQLException {
		IWikiHistEntity entity = new WikiHistEntity();

		entity.setWikiId		( rs.getString(WikiHistItems.wikiId.getItemName()) );
		entity.setWikiVerNo		( rs.getInt(WikiHistItems.wikiVerNo.getItemName()) );
		entity.setPageNm		( rs.getString(WikiHistItems.pageNm.getItemName()) );
		entity.setContent		( rs.getString(WikiHistItems.content.getItemName()) );
		entity.setDelStsId		( StatusFlg.value(rs.getBoolean(WikiHistItems.delStsId.getItemName())) );
		entity.setRegTimestamp	( rs.getTimestamp(WikiHistItems.regTimestamp.getItemName()) );
		entity.setRegUserId		( rs.getString(WikiHistItems.regUserId.getItemName()) );
		entity.setRegUserNm		( rs.getString(WikiHistItems.regUserNm.getItemName()) );
		entity.setUpdTimestamp	( rs.getTimestamp(WikiHistItems.updTimestamp.getItemName()) );
		entity.setUpdUserId		( rs.getString(WikiHistItems.updUserId.getItemName()) );
		entity.setUpdUserNm		( rs.getString(WikiHistItems.updUserNm.getItemName()) );

		return entity;
	}

}
