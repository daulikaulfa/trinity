package jp.co.blueship.tri.fw.um.dao.grp.eb;

import jp.co.blueship.tri.fw.dao.orm.EntityFooter;


/**
 * group user link entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class GrpUserLnkEntity extends EntityFooter implements IGrpUserLnkEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * grp-ID
	 */
	public String grpId = null;
	/**
	 * user-ID
	 */
	public String userId = null;

	/**
	 * grp-IDを取得します。
	 * @return grp-ID
	 */
	public String getGrpId() {
	    return grpId;
	}
	/**
	 * grp-IDを設定します。
	 * @param grpId grp-ID
	 */
	public void setGrpId(String grpId) {
	    this.grpId = grpId;
	}
	/**
	 * user-IDを取得します。
	 * @return user-ID
	 */
	public String getUserId() {
	    return userId;
	}
	/**
	 * user-IDを設定します。
	 * @param userId user-ID
	 */
	public void setUserId(String userId) {
	    this.userId = userId;
	}

}