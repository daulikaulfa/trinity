package jp.co.blueship.tri.fw.um.dao.grp.eb;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.dao.orm.psql.SqlFormatUtils;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.fw.um.dao.grp.constants.GrpRoleLnkItems;

/**
 * The SQL condition of the group role link entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class GrpRoleLnkCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute grp = UmTables.UM_GRP_ROLE_LNK;

	/**
	 * grp-ID
	 */
	public String grpId = null;
	/**
	 * grp-ID's
	 */
	public String[] grpIds = null;
	/**
	 * role-ID
	 */
	public String roleId = null;
	/**
	 * role-ID's
	 */
	public String[] roleIds = null;
	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		   super.append(GrpRoleLnkItems.delStsId, StatusFlg.off.parseBoolean() );
	}

	public GrpRoleLnkCondition(){
		super(grp);
	}

	/**
	 * grp-IDを取得します。
	 * @return grp-ID
	 */
	public String getGrpId() {
	    return grpId;
	}

	/**
	 * grp-IDを設定します。
	 * @param grpId grp-ID
	 */
	public void setGrpId(String grpId) {
	    this.grpId = grpId;
	    super.append(GrpRoleLnkItems.grpId, grpId );
	}

	/**
	 * grp-ID'sを取得します。
	 * @return grp-ID's
	 */
	public String[] getGrpIds() {
	    return grpIds;
	}

	/**
	 * grp-ID'sを設定します。
	 * @param grpIds grp-ID's
	 */
	public void setGrpIds(String[] grpIds) {
	    this.grpIds = grpIds;
	    super.append( GrpRoleLnkItems.grpId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(grpIds, GrpRoleLnkItems.grpId.getItemName(), false, true, false) );
	}

	/**
	 * role-IDを取得します。
	 * @return role-ID
	 */
	public String getRoleId() {
	    return roleId;
	}

	/**
	 * role-IDを設定します。
	 * @param roleId role-ID
	 */
	public void setRoleId(String roleId) {
	    this.roleId = roleId;
	    super.append(GrpRoleLnkItems.roleId, roleId );
	}

	/**
	 * role-ID'sを取得します。
	 * @return role-ID's
	 */
	public String[] getRoleIds() {
	    return roleIds;
	}

	/**
	 * role-ID'sを設定します。
	 * @param roleIds role-ID's
	 */
	public void setRoleIds(String[] roleIds) {
	    this.roleIds = roleIds;
	    super.append( GrpRoleLnkItems.roleId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(roleIds, GrpRoleLnkItems.roleId.getItemName(), false, true, false) );
	}

	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	    super.append( GrpRoleLnkItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}
}