package jp.co.blueship.tri.fw.um.dao.hist;

import jp.co.blueship.tri.fw.um.dao.ctg.eb.ICtgEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;

/**
 * @version V4.00.00
 * @author Akahoshi
 *
 */
public interface ICtgHistDao extends IHistDao {

	/**
	 * 登録処理を行います
	 *
	 * @param entity
	 * @param ctgEntity
	 * @return the number of rows affected1
	 */
	public int insert(IHistEntity entity , ICtgEntity ctgEntity);
}
