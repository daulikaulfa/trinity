package jp.co.blueship.tri.fw.um.dao.constants;

import org.apache.commons.lang.StringUtils;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;

/**
 * The enum of the tables.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public enum UmTables implements ITableAttribute {

	UM_PROJECT( "UPJ" ),
	UM_GRP( "UG" ),
	UM_GRP_ROLE_LNK( "GRL" ),
	UM_GRP_USER_LNK( "GUL" ),
	UM_ROLE( "UR" ),
	UM_ROLE_SVC_CTG_LNK( "URS" ),
	UM_SVC_CTG( "US" ),
	UM_SVC_CTG_SVC_LNK( "USL" ),
	UM_DEPT( "UD" ),
	UM_USER( "UU" ),
	UM_USER_ROLE_LNK( "URL" ),
	UM_USER_PASS_HIST( "UP" ),
	UM_USER_PROC_NOTICE( "UPN" ),
	UM_LOCK_SVC( "UL" ),
	UM_HIST( "UH" ),
	UM_ACCS_HIST( "UAH" ),
	UM_CTG( "UC" ),
	UM_CTG_LNK( "UCL" ),
	UM_MSTONE( "UM" ),
	UM_MSTONE_LNK( "UML" ),
	UM_USER_URL( "UUU" ) ,
	UM_FILTER( "UF" ),
	UM_WIKI( "UW" ),
	UM_WIKI_HIST( "UWH" ),
	UM_WIKI_TAG( "UWT" ),
	UM_WIKI_ATTACHED_FILE( "UWAF" ),
	;

	private String alias = null;

	private UmTables( String alias) {
		this.alias = alias;
	}

	/**
	 * Alias付きのテーブル名を取得します。
	 *
	 * @return 文字列
	 */
	public String nameWithAlias() {
		return this.name() + " " + this.alias() ;
	}

	/**
	 * テーブルごとに一意となるエイリアスを取得します。
	 *
	 * @return 文字列
	 */
	public String alias() {
		return this.alias;
	}

	/**
	 * 指定された名前に対応する列挙型を取得します。
	 *
	 * @param name 名前
	 * @return 対応する列挙型
	 */
	public static UmTables name( String name ) {
		if ( StringUtils.isBlank( name ) ) {
			return null;
		}

		for ( UmTables table : values() ) {
			if ( table.name().equalsIgnoreCase( name ) ) {
				return table;
			}
		}

		return null;
	}

}
