package jp.co.blueship.tri.fw.um.dao.wiki.eb;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.fw.um.dao.wiki.constants.WikiItems;
import jp.co.blueship.tri.fw.um.dao.wiki.constants.WikiTagItems;

public class WikiTagCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute wikiTag = UmTables.UM_WIKI_TAG;

	public WikiTagCondition() {
		super(wikiTag);
	}

	public String wikiId = null;
	public String tagNm = null;
	public StatusFlg delStsId = StatusFlg.off;
	{
		super.append(WikiItems.delStsId, StatusFlg.off.parseBoolean() );
	}

	public String getWikiId(){
		return this.wikiId;
	}
	public void setWikiId(String wikiId){
		this.wikiId = wikiId;
		super.append( WikiTagItems.wikiId, wikiId);
	}

	public String getTagNm(){
		return this.tagNm;
	}
	public void setTagNm(String tagNm){
		this.tagNm = tagNm;
		super.append( WikiTagItems.tagNm, tagNm);
	}

	public StatusFlg getDelStsId(){
		return this.delStsId;
	}
	public void setDelStsId(StatusFlg delStsId){
		this.delStsId = delStsId;
		super.append( WikiTagItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean());
	}
}
