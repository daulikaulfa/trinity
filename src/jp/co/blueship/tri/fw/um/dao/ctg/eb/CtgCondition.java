package jp.co.blueship.tri.fw.um.dao.ctg.eb;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.dao.orm.psql.SqlFormatUtils;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.fw.um.dao.ctg.constants.CtgItems;

public class CtgCondition extends ConditionSupport{

	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute category = UmTables.UM_CTG;

	public String ctgId = null;
	public String lotId = null;
	public String[] lotIds = null;
	public String ctgNm = null;
	public String[] ctgIds = null;
	public StatusFlg delStsId = StatusFlg.off;
	{
		   super.append(CtgItems.delStsId, StatusFlg.off.parseBoolean() );
	}

	public CtgCondition() {
		super(category);
	}

	public String getCtgId() {
		return ctgId;
	}
	public void setCtgId(String ctgId) {
		this.ctgId = ctgId;
		super.append(CtgItems.ctgId, ctgId);
	}

	public String getLotId() {
		return lotId;
	}
	public void setLotId(String lotId) {
		this.lotId = lotId;
		super.append(CtgItems.lotId, lotId);
	}

	public String[] getLotIds() {
		return lotIds;
	}

	public void setLotIds(String[] lotIds) {
		this.lotIds = lotIds;
		super.append( CtgItems.lotId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(lotIds, CtgItems.lotId.getItemName(), false, true, false) );
	}

	public String getCtgNm() {
		return ctgNm;
	}
	public void setCtgNm(String ctgNm) {
		this.ctgNm = ctgNm;
		super.append(CtgItems.ctgNm, ctgNm);
	}

	public String[] getCtgIds() {
		return ctgIds;
	}
	public void setCtgIds(String... ctgIds) {
		this.ctgIds = ctgIds;
		super.append( CtgItems.ctgId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(ctgIds, CtgItems.ctgId.getItemName(), false, true, false) );
	}

	public String[] getCtgIdsByNotEquals() {
		return ctgIds;
	}
	public void setCtgIdsByNotEquals(String... ctgIds) {
		this.ctgIds = ctgIds;
		super.append( CtgItems.ctgId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(ctgIds, CtgItems.ctgId.getItemName(), false, false, true) );
	}

	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	public void setDelStsId(StatusFlg delStsId){
	    this.delStsId = delStsId;
	    super.append( CtgItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}

}
