package jp.co.blueship.tri.fw.um.dao.hist;

import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;

/**
 * The implements of the Areq History DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public interface IAreqHistDao extends IHistDao {

	/**
	 * 登録処理を行います
	 *
	 * @param entity
	 * @return the number of rows affected1
	 */
	public int insert(IHistEntity entity , IAreqDto areqDto);

	/**
	 * Unmarshalling from a Xml document.
	 * <br>XML文書を任意のオブジェクトにマッピング（Unmarshall）します。
	 *
	 * @param xml history date
	 * @return the newly created object. マッピングしたオブジェクトを戻します
	 */
	public IAreqEntity unmarshall( String xmlDate );

}
