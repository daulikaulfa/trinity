package jp.co.blueship.tri.fw.um.dao.locksvc.eb;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.EntityFooter;

/**
 * lock service entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class LockSvcEntity extends EntityFooter implements ILockSvcEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * svc-ID
	 */
	public String svcId = null;
	/**
	 * lock svc-ID
	 */
	public String lockSvcId = null;
	/**
	 * lock for lot
	 */
	public StatusFlg lockForLot = StatusFlg.off;
	/**
	 * svc-IDを取得します。
	 * @return svc-ID
	 */
	public String getSvcId() {
	    return svcId;
	}
	/**
	 * svc-IDを設定します。
	 * @param svcId svc-ID
	 */
	public void setSvcId(String svcId) {
	    this.svcId = svcId;
	}
	/**
	 * lock svc-IDを取得します。
	 * @return lock svc-ID
	 */
	public String getLockSvcId() {
	    return lockSvcId;
	}
	/**
	 * lock svc-IDを設定します。
	 * @param lockSvcId lock svc-ID
	 */
	public void setLockSvcId(String lockSvcId) {
	    this.lockSvcId = lockSvcId;
	}
	/**
	 * lock for lotを取得します。
	 * @return lock for lot
	 */
	public StatusFlg getLockForLot() {
	    return lockForLot;
	}
	/**
	 * lock for lotを設定します。
	 * @param lockForLot lock for lot
	 */
	public void setLockForLot(StatusFlg lockForLot) {
	    this.lockForLot = lockForLot;
	}

}