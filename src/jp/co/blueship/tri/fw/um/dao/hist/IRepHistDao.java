package jp.co.blueship.tri.fw.um.dao.hist;

import jp.co.blueship.tri.dcm.dao.rep.eb.IRepEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;

/**
 * The implements of the Rep History DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 */
public interface IRepHistDao extends IHistDao {

	/**
	 * 登録処理を行います
	 *
	 * @param entity
	 * @return the number of rows affected1
	 */
	public int insert(IHistEntity entity , IRepEntity repEntity);

}
