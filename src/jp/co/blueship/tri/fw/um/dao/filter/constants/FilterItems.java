package jp.co.blueship.tri.fw.um.dao.filter.constants;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;

/**
 * The items of the filter entity.
 *
 * @version V4.00.00
 * @author Yohei Tanaka
 *
 */
public enum FilterItems implements ITableItem{
	filterId("filter_id"),
	filterNm("filter_nm"),
	filterData("filter_data"),
	lotId("lot_id"),
	userId("user_id"),
	svcId("svc_id"),
	delStsId("del_sts_id"),
	regUserId("reg_user_id"),
	regUserNm("reg_user_nm"),
	regTimestamp("reg_timestamp"),
	updUserId("upd_user_id"),
	updUserNm("upd_user_nm"),
	updTimestamp("upd_timestamp");

	private String element = null;

	private FilterItems( String element ) {
		this.element = element;
	}
	public String getItemName() {
		return element;
	}

}
