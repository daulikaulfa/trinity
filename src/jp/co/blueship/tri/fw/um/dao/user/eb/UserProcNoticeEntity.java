package jp.co.blueship.tri.fw.um.dao.user.eb;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.EntityFooter;

/**
 * user proc notice entity.
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 */
public class UserProcNoticeEntity extends EntityFooter implements IUserProcNoticeEntity{

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	public String procId = null;
	public String userId = null;
	public StatusFlg isRead = StatusFlg.off;


	public String getProcId() {
		return procId;
	}
	public void setProcId(String procId) {
		this.procId = procId;
	}

	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}

	public StatusFlg getIsRead() {
		return isRead;
	}
	public void setIsRead(StatusFlg isRead) {
		this.isRead = isRead;
	}
}
