package jp.co.blueship.tri.fw.um.dao.hist.eb;

import jp.co.blueship.tri.fw.dao.orm.IEntityAccsHist;
import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the history entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version V4.00.00
 * @author Yohei Tanaka
 *
 */
public interface IHistEntity extends IEntityFooter, IEntityAccsHist {

	public String getHistId();
	public void setHistId(String histId);

	public String getLotId();
	public void setLotId(String lotId);

	public String getDataCtgCd();
	public void setDataCtgCd(String dataCtgCd);

	public String getDataId();
	public void setDataId(String dataId);

	public String getHistData();
	public void setHistData(String histData);

	public String getStsId();
	public void setStsId(String stsId);

	public String getActSts();
	public void setActSts(String actSts);

}
