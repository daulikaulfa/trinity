package jp.co.blueship.tri.fw.um.dao.svcctg.eb;

import jp.co.blueship.tri.fw.dao.orm.EntityFooter;

/**
 * service catalog service link entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class SvcCtgSvcLnkEntity extends EntityFooter implements ISvcCtgSvcLnkEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * svc-ID
	 */
	public String svcId = null;
	/**
	 * svc-ctg-ID
	 */
	public String svcCtgId = null;


	/**
	 * svc-IDを取得します。
	 * @return svc-ID
	 */
	public String getSvcId() {
	    return svcId;
	}
	/**
	 * svc-IDを設定します。
	 * @param svcId svc-ID
	 */
	public void setSvcId(String svcId) {
	    this.svcId = svcId;
	}
	/**
	 * svc-ctg-IDを取得します。
	 * @return svc-ctg-ID
	 */
	public String getSvcCtgId() {
	    return svcCtgId;
	}
	/**
	 * svc-ctg-IDを設定します。
	 * @param svcCtgId svc-ctg-ID
	 */
	public void setSvcCtgId(String svcCtgId) {
	    this.svcCtgId = svcCtgId;
	}

}