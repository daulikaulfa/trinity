package jp.co.blueship.tri.fw.um.dao.mstone;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.fw.um.dao.mstone.constants.MstoneItems;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.IMstoneEntity;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.MstoneEntity;

/**
 * The implements of the milestone DAO.
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 */
public class MstoneDao extends JdbcBaseDao<IMstoneEntity> implements IMstoneDao {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected ITableAttribute getTableAttribute() {
		return UmTables.UM_MSTONE;
	}

	@Override
	protected ISqlBuilder append(ISqlBuilder builder, IMstoneEntity entity) {
		builder
			.append(MstoneItems.mstoneId, entity.getMstoneId(), true)
			.append(MstoneItems.mstoneNm, entity.getMstoneNm())
			.append(MstoneItems.mstoneStDate, entity.getMstoneStDate())
			.append(MstoneItems.mstoneEndDate, entity.getMstoneEndDate())
			.append(MstoneItems.content, entity.getContent())
			.append(MstoneItems.lotId, entity.getLotId())
			.append(MstoneItems.sortOdr, entity.getSortOdr())
			.append(MstoneItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(MstoneItems.regTimestamp, entity.getRegTimestamp())
			.append(MstoneItems.regUserId, entity.getRegUserId())
			.append(MstoneItems.regUserNm, entity.getRegUserNm())
			.append(MstoneItems.updTimestamp, entity.getUpdTimestamp())
			.append(MstoneItems.updUserId, entity.getUpdUserId())
			.append(MstoneItems.updUserNm, entity.getUpdUserNm())
			;

		return builder;
	}

	@Override
	protected IMstoneEntity entityMapping(ResultSet rs, int row) throws SQLException {
		MstoneEntity entity = new MstoneEntity();

		entity.setMstoneId( rs.getString(MstoneItems.mstoneId.getItemName()) );
		entity.setMstoneNm( rs.getString(MstoneItems.mstoneNm.getItemName()) );
		entity.setMstoneStDate( rs.getString(MstoneItems.mstoneStDate.getItemName()) );
		entity.setMstoneEndDate( rs.getString(MstoneItems.mstoneEndDate.getItemName()) );
		entity.setContent( rs.getString(MstoneItems.content.getItemName()) );
		entity.setLotId( rs.getString(MstoneItems.lotId.getItemName()) );
		entity.setSortOdr( rs.getInt(MstoneItems.sortOdr.getItemName()) );
		entity.setDelStsId( StatusFlg.value(rs.getBoolean(MstoneItems.delStsId.getItemName())) );
		entity.setRegUserId( rs.getString(MstoneItems.regUserId.getItemName()) );
		entity.setRegUserNm( rs.getString(MstoneItems.regUserNm.getItemName()) );
		entity.setRegTimestamp( rs.getTimestamp(MstoneItems.regTimestamp.getItemName()) );
		entity.setUpdUserId( rs.getString(MstoneItems.updUserId.getItemName()) );
		entity.setUpdUserNm( rs.getString(MstoneItems.updUserNm.getItemName()) );
		entity.setUpdTimestamp( rs.getTimestamp(MstoneItems.updTimestamp.getItemName()) );

		return entity;
	}

}
