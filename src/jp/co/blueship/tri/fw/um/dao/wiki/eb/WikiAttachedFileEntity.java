package jp.co.blueship.tri.fw.um.dao.wiki.eb;

import jp.co.blueship.tri.fw.dao.orm.EntityFooter;

public class WikiAttachedFileEntity extends EntityFooter implements IWikiAttachedFileEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	public String wikiId = null;
	public String attachedFileSeqNo = null;
	public String filePath = null;

	@Override
	public String getWikiId() {
		return this.wikiId;
	}
	@Override
	public void setWikiId(String wikiId) {
		this.wikiId = wikiId;
	}

	@Override
	public String getAttachedFileSeqNo() {
		return this.attachedFileSeqNo;
	}
	@Override
	public void setAttachedFileSeqNo(String attachedFileSeqNo) {
		this.attachedFileSeqNo = attachedFileSeqNo;
	}

	@Override
	public String getFilePath() {
		return this.filePath;
	}
	@Override
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

}
