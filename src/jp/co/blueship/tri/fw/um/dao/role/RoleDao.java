package jp.co.blueship.tri.fw.um.dao.role;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.fw.um.dao.role.constants.RoleItems;
import jp.co.blueship.tri.fw.um.dao.role.eb.IRoleEntity;
import jp.co.blueship.tri.fw.um.dao.role.eb.RoleEntity;

/**
 * The implements of the role DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 * 
 * @version V4.00.00
 * @author le.thixuan
 */
public class RoleDao extends JdbcBaseDao<IRoleEntity> implements IRoleDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected final ITableAttribute getTableAttribute() {
		return UmTables.UM_ROLE;
	}

	@Override
	protected final ISqlBuilder append( ISqlBuilder builder, IRoleEntity entity ) {
		builder
			.append(RoleItems.roleId, entity.getRoleId(), true)
			.append(RoleItems.roleName, entity.getRoleName())
			.append(RoleItems.sortOdr, entity.getSortOdr())
			.append(RoleItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(RoleItems.regTimestamp, entity.getRegTimestamp())
			.append(RoleItems.regUserId, entity.getRegUserId())
			.append(RoleItems.regUserNm, entity.getRegUserNm())
			.append(RoleItems.updTimestamp, entity.getUpdTimestamp())
			.append(RoleItems.updUserId, entity.getUpdUserId())
			.append(RoleItems.updUserNm, entity.getUpdUserNm())
			;

		return builder;
	}

	@Override
	protected final IRoleEntity entityMapping( ResultSet rs, int row ) throws SQLException {
  	  IRoleEntity entity = new RoleEntity();

  	  entity.setRoleId( rs.getString(RoleItems.roleId.getItemName()) );
  	  entity.setRoleName( rs.getString(RoleItems.roleName.getItemName()) );
  	  entity.setSortOdr( rs.getInt(RoleItems.sortOdr.getItemName()) );
  	  entity.setDelStsId( StatusFlg.value(rs.getBoolean(RoleItems.delStsId.getItemName())) );
  	  entity.setRegTimestamp( rs.getTimestamp(RoleItems.regTimestamp.getItemName()) );
  	  entity.setRegUserId( rs.getString(RoleItems.regUserId.getItemName()) );
  	  entity.setRegUserNm( rs.getString(RoleItems.regUserNm.getItemName()) );
  	  entity.setUpdTimestamp( rs.getTimestamp(RoleItems.updTimestamp.getItemName()) );
  	  entity.setUpdUserId( rs.getString(RoleItems.updUserId.getItemName()) );
  	  entity.setUpdUserNm( rs.getString(RoleItems.updUserNm.getItemName()) );

  	  return entity;
	}

}
