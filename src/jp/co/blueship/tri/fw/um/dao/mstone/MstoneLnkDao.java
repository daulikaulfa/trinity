package jp.co.blueship.tri.fw.um.dao.mstone;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.fw.um.dao.mstone.constants.MstoneLnkItems;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.IMstoneLnkEntity;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.MstoneLnkEntity;

/**
 * The implements of the milestone link DAO.
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 */
public class MstoneLnkDao extends JdbcBaseDao<IMstoneLnkEntity> implements IMstoneLnkDao {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected ITableAttribute getTableAttribute() {
		return UmTables.UM_MSTONE_LNK;
	}

	@Override
	protected ISqlBuilder append(ISqlBuilder builder, IMstoneLnkEntity entity) {
		builder
			.append(MstoneLnkItems.mstoneId, entity.getMstoneId())
			.append(MstoneLnkItems.dataCtgCd, entity.getDataCtgCd(), true)
			.append(MstoneLnkItems.dataId, entity.getDataId(), true)
			.append(MstoneLnkItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(MstoneLnkItems.regTimestamp, entity.getRegTimestamp())
			.append(MstoneLnkItems.regUserId, entity.getRegUserId())
			.append(MstoneLnkItems.regUserNm, entity.getRegUserNm())
			.append(MstoneLnkItems.updTimestamp, entity.getUpdTimestamp())
			.append(MstoneLnkItems.updUserId, entity.getUpdUserId())
			.append(MstoneLnkItems.updUserNm, entity.getUpdUserNm())
			;
		return builder;
	}

	@Override
	protected IMstoneLnkEntity entityMapping(ResultSet rs, int row) throws SQLException {
		MstoneLnkEntity entity = new MstoneLnkEntity();

		entity.setMstoneId( rs.getString(MstoneLnkItems.mstoneId.getItemName()) );
		entity.setDataCtgCd( rs.getString(MstoneLnkItems.dataCtgCd.getItemName()) );
		entity.setDataId( rs.getString(MstoneLnkItems.dataId.getItemName()) );
		entity.setDelStsId( StatusFlg.value(rs.getBoolean(MstoneLnkItems.delStsId.getItemName())) );
		entity.setRegUserId( rs.getString(MstoneLnkItems.regUserId.getItemName()) );
		entity.setRegUserNm( rs.getString(MstoneLnkItems.regUserNm.getItemName()) );
		entity.setRegTimestamp( rs.getTimestamp(MstoneLnkItems.regTimestamp.getItemName()) );
		entity.setUpdUserId( rs.getString(MstoneLnkItems.updUserId.getItemName()) );
		entity.setUpdUserNm( rs.getString(MstoneLnkItems.updUserNm.getItemName()) );
		entity.setUpdTimestamp( rs.getTimestamp(MstoneLnkItems.updTimestamp.getItemName()) );

		return entity;
	}

}
