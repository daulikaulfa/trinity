package jp.co.blueship.tri.fw.um.dao.accshist.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.fw.dao.orm.EntityFooter;


/**
 * Access history entity.
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 */
public class AccsHistEntity extends EntityFooter implements IAccsHistEntity {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * user-ID
	 */
	public String userId = null;
	/**
	 * data catalog code
	 */
	public String dataCtgCd = null;
	/**
	 * data-ID
	 */
	public String dataId = null;
	/**
	 * access time stamp
	 */
	public Timestamp accsTimestamp = null;
	/**
	 * notice view time stamp
	 */
	public Timestamp noticeViewTimestamp = null;
	/**
	 * user name
	 */
	public String userNm = null;

	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getDataCtgCd() {
		return dataCtgCd;
	}
	public void setDataCtgCd(String dataCtgCd) {
		this.dataCtgCd = dataCtgCd;
	}
	public String getDataId() {
		return dataId;
	}
	public void setDataId(String dataId) {
		this.dataId = dataId;
	}
	public Timestamp getAccsTimestamp() {
		return accsTimestamp;
	}
	public void setAccsTimestamp(Timestamp accsTimestamp) {
		this.accsTimestamp = accsTimestamp;
	}
	public Timestamp getNoticeViewTimestamp() {
		return noticeViewTimestamp;
	}
	public void setNoticeViewTimestamp(Timestamp noticeViewTimestamp) {
		this.noticeViewTimestamp = noticeViewTimestamp;
	}
	public String getUserNm() {
		return userNm;
	}
	public void setUserNm(String userNm) {
		this.userNm = userNm;
	}

}