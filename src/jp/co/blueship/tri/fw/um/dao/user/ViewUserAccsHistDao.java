package jp.co.blueship.tri.fw.um.dao.user;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.ex.TriJdbcDaoException;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;
import jp.co.blueship.tri.fw.dao.orm.psql.SqlFormatUtils;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.fw.um.dao.user.constants.UserItems;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.ViewUserAccsHistCondition;

/**
 * The implements of the user access history DAO.
 * 
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class ViewUserAccsHistDao extends JdbcBaseDao<IUserEntity> implements IViewUserAccsHistDao {
	
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	private static final String SQL_RECENTLY_QUERY = " SELECT "
													+ " U.*, H.ACCS_TIMESTAMP "
													+ " FROM UM_USER U"
													+ "		INNER JOIN "
													+ "			(SELECT MAX(ACCS_TIMESTAMP) AS ACCS_TIMESTAMP, USER_ID, DEL_STS_ID "
													+ " 		FROM UM_ACCS_HIST "
													+ "			GROUP BY USER_ID, DEL_STS_ID) H "
													+ "		ON H.USER_ID = U.USER_ID AND H.DEL_STS_ID = U.DEL_STS_ID";
	@Override
	protected ITableAttribute getTableAttribute() {
		return UmTables.UM_USER;
	}

	@Override
	@Deprecated
	protected ISqlBuilder append(ISqlBuilder builder, IUserEntity entity) {
		return null;
	}

	@Override
	protected IUserEntity entityMapping(ResultSet rs, int row) throws SQLException {
		IUserEntity entity = new UserEntity();
		
		entity.setUserId( rs.getString(UserItems.userId.getItemName()) );
		entity.setUserPass( rs.getString(UserItems.userPass.getItemName()) );
  	  	entity.setPassTimeLimit( rs.getTimestamp(UserItems.passTimeLimit.getItemName()) );
  	  	entity.setUserNm( rs.getString(UserItems.userNm.getItemName()) );
  	  	entity.setDeptId( rs.getString(UserItems.deptId.getItemName()) );
  	  	entity.setDeptNm( rs.getString(UserItems.deptNm.getItemName()) );
  	  	entity.setEmailAddr( rs.getString(UserItems.emailAddr.getItemName()) );
  	  	entity.setLang( rs.getString(UserItems.lang.getItemName()) );
  	  	entity.setTimeZone( rs.getString(UserItems.timezone.getItemName()) );
  	  	entity.setUserLocation( rs.getString(UserItems.userLocaltion.getItemName()) );
  	  	entity.setBiography( rs.getString(UserItems.biography.getItemName()) );
  	  	entity.setIconTyp( rs.getString(UserItems.iconTyp.getItemName()) );
  	  	entity.setDefaultIconPath( rs.getString(UserItems.defaultIconPath.getItemName()) );
  	  	entity.setCustomIconPath( rs.getString(UserItems.customIconPath.getItemName()) );
  	  	entity.setGenderTyp( rs.getString(UserItems.genderTyp.getItemName()) );
  	  	entity.setIsReceiveMail( StatusFlg.value(rs.getBoolean(UserItems.isReceiveMail.getItemName())) );
  	  	entity.setDelStsId( StatusFlg.value(rs.getBoolean(UserItems.delStsId.getItemName())) );
  	  	entity.setRegTimestamp( rs.getTimestamp(UserItems.regTimestamp.getItemName()) );
  	  	entity.setRegUserId( rs.getString(UserItems.regUserId.getItemName()) );
  	  	entity.setRegUserNm( rs.getString(UserItems.regUserNm.getItemName()) );
  	  	entity.setUpdTimestamp( rs.getTimestamp(UserItems.updTimestamp.getItemName()) );
  	  	entity.setUpdUserId( rs.getString(UserItems.updUserId.getItemName()) );
  	  	entity.setUpdUserNm( rs.getString(UserItems.updUserNm.getItemName()) );
		
		return entity;
	}
	
	@Override
	public List<IUserEntity> find(ViewUserAccsHistCondition condition, ISqlSort sort, int viewRows) throws TriJdbcDaoException {
		try {
			StringBuffer buf = new StringBuffer();
			String sql = null;
			
			String userId = SqlFormatUtils.getCondition( condition.getKeywords(), "U." + UserItems.userId.getItemName(), true, false, false);
			String userNm = SqlFormatUtils.getCondition(condition.getKeywords(), "U." + UserItems.userNm.getItemName(), true, false, false);
			String emailAddr = SqlFormatUtils.getCondition(condition.getKeywords(), "U." + UserItems.emailAddr.getItemName(), true, false, false);
			
			buf
				.append( SQL_RECENTLY_QUERY )
				.append( " WHERE ")
				.append( userId )
				.append( " OR ")
				.append( userNm )
				.append( " OR ")
				.append( emailAddr );
			
			sql = buf.toString() + SqlFormatUtils.toSortString(sort) + " OFFSET 0 LIMIT " + viewRows;
			
			List<IUserEntity> rows = super.query(sql, new ParameterizedRowMapper<IUserEntity>() {
				
				@Override
				public IUserEntity mapRow(ResultSet resultset, int i) throws SQLException {
					return entityMapping(resultset, i);
				}
			});
			
			return rows;
		} catch (Exception e) {
			throw new TriJdbcDaoException(SmMessageId.SM005004S, e, this.getTableAttribute().name());
		}
	}
	


}
