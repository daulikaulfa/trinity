package jp.co.blueship.tri.fw.um.dao.mstone.eb;

/**
 * The implementation of milestone link DTO.
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 */
public class MstoneLnkDto implements IMstoneLnkDto {

	/**
	 * Milestone Link Entity
	 */
	public IMstoneLnkEntity mstoneLnkEntity = null;

	@Override
	public IMstoneLnkEntity getMstoneLnkEntity() {
		return mstoneLnkEntity;
	}

	@Override
	public void setMstoneLnkEntity(IMstoneLnkEntity mstoneLnkEntity) {
		this.mstoneLnkEntity = mstoneLnkEntity;
	}

}
