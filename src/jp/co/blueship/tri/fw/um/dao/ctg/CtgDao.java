package jp.co.blueship.tri.fw.um.dao.ctg;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.fw.um.dao.ctg.constants.CtgItems;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.CtgEntity;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.ICtgEntity;

/**
 * The implements of the category DAO.
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 */
public class CtgDao extends JdbcBaseDao<ICtgEntity> implements ICtgDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected ITableAttribute getTableAttribute() {
		return UmTables.UM_CTG;
	}

	@Override
	protected final ISqlBuilder append( ISqlBuilder builder, ICtgEntity entity ) {
		builder
			.append(CtgItems.ctgId, entity.getCtgId(), true)
			.append(CtgItems.ctgNm, entity.getCtgNm())
			.append(CtgItems.lotId, entity.getLotId())
			.append(CtgItems.sortOdr, entity.getSortOdr())
			.append(CtgItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(CtgItems.regTimestamp, entity.getRegTimestamp())
			.append(CtgItems.regUserId, entity.getRegUserId())
			.append(CtgItems.regUserNm, entity.getRegUserNm())
			.append(CtgItems.updTimestamp, entity.getUpdTimestamp())
			.append(CtgItems.updUserId, entity.getUpdUserId())
			.append(CtgItems.updUserNm, entity.getUpdUserNm())
			;

		return builder;
	}

	@Override
	protected final ICtgEntity entityMapping( ResultSet rs, int row ) throws SQLException {
		CtgEntity entity = new CtgEntity();

		entity.setCtgId( rs.getString(CtgItems.ctgId.getItemName()) );
		entity.setCtgNm( rs.getString(CtgItems.ctgNm.getItemName()) );
		entity.setLotId( rs.getString(CtgItems.lotId.getItemName()) );
		entity.setSortOdr( rs.getInt(CtgItems.sortOdr.getItemName()) );
		entity.setDelStsId( StatusFlg.value(rs.getBoolean(CtgItems.delStsId.getItemName())) );
		entity.setRegUserId( rs.getString(CtgItems.regUserId.getItemName()) );
		entity.setRegUserNm( rs.getString(CtgItems.regUserNm.getItemName()) );
		entity.setRegTimestamp( rs.getTimestamp(CtgItems.regTimestamp.getItemName()) );
		entity.setUpdUserId( rs.getString(CtgItems.updUserId.getItemName()) );
		entity.setUpdUserNm( rs.getString(CtgItems.updUserNm.getItemName()) );
		entity.setUpdTimestamp( rs.getTimestamp(CtgItems.updTimestamp.getItemName()) );

		return entity;
	}

}
