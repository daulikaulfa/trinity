package jp.co.blueship.tri.fw.um.dao.hist.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.dao.orm.psql.SqlFormatUtils;
import jp.co.blueship.tri.fw.sm.dao.procmgt.constants.ProcMgtItems;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.fw.um.dao.hist.constants.HistItems;

/**
 * The SQL condition of the history entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class HistCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute hist = UmTables.UM_HIST;

	/**
	 * hist-ID
	 */
	public String histId = null;
	/**
	 * hist-ID's
	 */
	public String[] histIds = null;
	/**
	 * data catalog code
	 */
	public String dataCtgCd = null;
	/**
	 * data catalog code's
	 */
	public String[] dataCtgCds = null;
	/**
	 * data-ID
	 */
	public String dataId = null;
	/**
	 * history data
	 */
	public String histData = null;
	/**
	 * ActionStatusId
	 */
	public String actStsId = null;
	/**
	 * ActionStatusId's
	 */
	public String[] actStsIds = null;
	/**
	 * LotId
	 */
	public String lotId = null;
	/**
	 * LotId's
	 */
	public String[] lotIds = null;
	/**
	 * regUserId
	 */
	public String regUserId = null;
	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		   super.append(HistItems.delStsId, StatusFlg.off.parseBoolean() );
	}

	public HistCondition(){
		super(hist);
		super.setJoinExecData( true );
		super.setKeyByJoinExecData( HistItems.histId );
		super.setJoinCtg(true);
		super.setJoinMstone(true);
		super.setJoinAccsHist(true);
	}

	@Override
	public ConditionSupport setJoinAccsHist( boolean isJoinAccsHist ) {
		super.setJoinAccsHist(isJoinAccsHist);
		return this;
	}

	/**
	 * hist-IDを取得します。
	 * @return hist-ID
	 */
	public String getHistId() {
	    return histId;
	}

	/**
	 * hist-IDを設定します。
	 * @param histId hist-ID
	 */
	public void setHistId(String histId) {
	    this.histId = histId;
	    super.append(HistItems.histId, histId );
	}

	/**
	 * hist-ID'sを取得します。
	 * @return hist-ID's
	 */
	public String[] getHistIds() {
	    return histIds;
	}

	/**
	 * hist-ID'sを設定します。
	 * @param histIds hist-ID's
	 */
	public void setHistIds(String[] histIds) {
	    this.histIds = histIds;
	    super.append( HistItems.histId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(histIds, HistItems.histId.getItemName(), false, true, false) );
	}

	/**
	 * data catalog code'sを取得します。
	 * @return data catalog's code
	 */
	public String[] getDataCtgCds() {
	    return dataCtgCds;
	}

	/**
	 * data catalog code'sを設定します。
	 * @param dataCtgCd data catalog code's
	 */
	public void setDataCtgCds(String[] dataCtgCds) {
	    this.dataCtgCds = dataCtgCds;
	    super.append( HistItems.dataCtgCd.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(dataCtgCds, HistItems.dataCtgCd.getItemName(), false, true, false) );
	}

	/**
	 * data catalog codeを取得します。
	 * @return data catalog code
	 */
	public String getDataCtgCd() {
	    return dataCtgCd;
	}

	/**
	 * data catalog codeを設定します。
	 * @param dataCtgCd data catalog code
	 */
	public void setDataCtgCd(String dataCtgCd) {
	    this.dataCtgCd = dataCtgCd;
	    super.append(HistItems.dataCtgCd, dataCtgCd );
	}

	/**
	 * data-IDを取得します。
	 * @return data-ID
	 */
	public String getDataId() {
	    return dataId;
	}

	/**
	 * data-IDを設定します。
	 * @param dataId data-ID
	 */
	public void setDataId(String dataId) {
	    this.dataId = dataId;
	    super.append(HistItems.dataId, dataId );
	}

	/**
	 * history dataを取得します。
	 * @return history data
	 */
	public String getHistData() {
	    return histData;
	}

	/**
	 * history dataを設定します。
	 * @param histData history data
	 */
	public void setHistData(String histData) {
	    this.histData = histData;
	    super.append(HistItems.histData, histData );
	}

	/**
	 * ActionStatusIdを取得します。<br>
	 * get the ActionStatusId
	 * @return ActionStatusId
	 */
	public String getActStsId(){
		return this.actStsId;
	}
	/**
	 * ActionStatusIdを設定します。<br>
	 * Setting the ActionStatus
	 */
	public void setActStsId(String actStsId){
		this.actStsId = actStsId;
		super.append(HistItems.actStsId, actStsId );
	}

	/**
	 * ActionStatusId'sを取得します。<br>
	 * get the ActionStatusId's
	 * @return ActionStatusId's
	 */
	public String[] getActStsIds(){
		return this.actStsIds;
	}
	/**
	 * ActionStatusId'sを設定します。<br>
	 * Setting the ActionStatus's
	 */
	public void setActStsIds(String[] actStsIds){
		this.actStsIds = actStsIds;
	    super.append( HistItems.actStsId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(actStsIds, HistItems.actStsId.getItemName(), false, true, false) );
	}

	/**
	 * LotIdを取得します。<br>
	 * get the LotId
	 * @return LotId
	 */
	public String getLotId(){
		return this.lotId;
	}
	/**
	 * LotIdを設定します。<br>
	 * Setting the LotId
	 */
	public void setLotId(String lotId){
		this.lotId = lotId;
		super.append(HistItems.lotId, lotId );
	}

	/**
	 * LotId'sを取得します。<br>
	 * get the LotId's
	 * @return LotId's
	 */
	public String[] getLotIds(){
		return this.lotIds;
	}
	/**
	 * LotId'sを設定します。<br>
	 * Setting the Lot's
	 */
	public void setLotIds(String[] lotIds){
		this.lotIds = lotIds;
	    super.append( HistItems.lotId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(lotIds, HistItems.lotId.getItemName(), false, true, false) );
	}

	/**
	 * RegUserIdを取得します。<br>
	 * get the RegUserId
	 * @return RegUserId
	 */
	public String getRegUserId(){
		return this.regUserId;
	}
	/**
	 * RegUserIdを設定します。<br>
	 * Setting the RegUserId
	 */
	public void setRegUserId(String regUserId){
		this.regUserId = regUserId;
		super.append(HistItems.regUserId, regUserId );
	}

	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	    super.append( HistItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}

	/**
	 * 登録日時での検索範囲を設定します<br>
	 */
	public void setRegTimestampFromTo(Timestamp from , Timestamp to){

		super.append(ProcMgtItems.regTimestamp.getItemName() ,
				SqlFormatUtils.getFromTo(from, to, ProcMgtItems.regTimestamp.getItemName()));
	}

	/**
	 * 登録日時での検索範囲を設定します<br>
	 */
	public void setRegTimestampFromTo(String from , String to){

		super.append(ProcMgtItems.regTimestamp.getItemName() ,
				SqlFormatUtils.getFromTo(from, to, ProcMgtItems.regTimestamp.getItemName(), true));
	}
}