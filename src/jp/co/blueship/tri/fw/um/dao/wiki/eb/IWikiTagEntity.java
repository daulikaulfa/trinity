package jp.co.blueship.tri.fw.um.dao.wiki.eb;

import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the wikiTag entity.
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 */
public interface IWikiTagEntity extends IEntityFooter {

	public String getWikiId();
	public void setWikiId(String wikiId);

	public String getTagNm();
	public void setTagNm(String tagNm);

}
