package jp.co.blueship.tri.fw.um.dao.user;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.IDaoTemplate;
import jp.co.blueship.tri.fw.dao.orm.IDaoTemplateCallback;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.ISqlCondition;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.DaoTemplate;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;
import jp.co.blueship.tri.fw.sm.dao.constants.SmTables;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.fw.um.dao.user.constants.UserItems;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserEntity;

/**
 * The implements of the user DAO.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class UserDao extends JdbcBaseDao<IUserEntity> implements IUserDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected final ITableAttribute getTableAttribute() {
		return UmTables.UM_USER;
	}

	@Override
	public void setDaoTemplate( IDaoTemplate daoTemplate ) {
		super.setDaoTemplate( new CustomTemplate( new CustomCallback() ) );
	}

	@Override
	protected final  ISqlBuilder append( ISqlBuilder builder, IUserEntity entity ) {
		builder
			.append(UserItems.userId, entity.getUserId(), true)
			.append(UserItems.userPass, entity.getUserPass())
			.append(UserItems.passTimeLimit, entity.getPassTimeLimit())
			.append(UserItems.userNm, entity.getUserNm())
			.append(UserItems.deptId, entity.getDeptId())
			.append(UserItems.deptNm, entity.getDeptNm())
			.append(UserItems.lang, entity.getLang())
			.append(UserItems.timezone, entity.getTimeZone())
			.append(UserItems.userLocaltion, entity.getUserLocation())
			.append(UserItems.biography, entity.getBiography())
			.append(UserItems.iconTyp, entity.getIconTyp())
			.append(UserItems.defaultIconPath, entity.getDefaultIconPath())
			.append(UserItems.customIconPath, entity.getCustomIconPath())
			.append(UserItems.genderTyp, entity.getGenderTyp())
			.append(UserItems.isReceiveMail, (null == entity.getIsReceiveMail())? StatusFlg.off.parseBoolean(): entity.getIsReceiveMail().parseBoolean())
			.append(UserItems.emailAddr, entity.getEmailAddr())
			.append(UserItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(UserItems.regTimestamp, entity.getRegTimestamp())
			.append(UserItems.regUserId, entity.getRegUserId())
			.append(UserItems.regUserNm, entity.getRegUserNm())
			.append(UserItems.updTimestamp, entity.getUpdTimestamp())
			.append(UserItems.updUserId, entity.getUpdUserId())
			.append(UserItems.updUserNm, entity.getUpdUserNm())
			.append(UserItems.lookAndFeel, entity.getLookAndFeel())
			;

		return builder;
	}

	@Override
	protected final IUserEntity entityMapping( ResultSet rs, int row ) throws SQLException {
  	  IUserEntity entity = new UserEntity();

  	  entity.setUserId( rs.getString(UserItems.userId.getItemName()) );
  	  entity.setUserPass( rs.getString(UserItems.userPass.getItemName()) );
  	  entity.setPassTimeLimit( rs.getTimestamp(UserItems.passTimeLimit.getItemName()) );
  	  entity.setUserNm( rs.getString(UserItems.userNm.getItemName()) );
  	  entity.setDeptId( rs.getString(UserItems.deptId.getItemName()) );
  	  entity.setDeptNm( rs.getString(UserItems.deptNm.getItemName()) );
  	  entity.setEmailAddr( rs.getString(UserItems.emailAddr.getItemName()) );
  	  entity.setLang( rs.getString(UserItems.lang.getItemName()) );
  	  entity.setTimeZone( rs.getString(UserItems.timezone.getItemName()) );
  	  entity.setUserLocation( rs.getString(UserItems.userLocaltion.getItemName()) );
	  entity.setBiography( rs.getString(UserItems.biography.getItemName()) );
	  entity.setIconTyp( rs.getString(UserItems.iconTyp.getItemName()) );
	  entity.setDefaultIconPath( rs.getString(UserItems.defaultIconPath.getItemName()) );
	  entity.setCustomIconPath( rs.getString(UserItems.customIconPath.getItemName()) );
	  entity.setGenderTyp( rs.getString(UserItems.genderTyp.getItemName()) );
	  entity.setIsReceiveMail( StatusFlg.value(rs.getBoolean(UserItems.isReceiveMail.getItemName())) );
  	  entity.setDelStsId( StatusFlg.value(rs.getBoolean(UserItems.delStsId.getItemName())) );
  	  entity.setRegTimestamp( rs.getTimestamp(UserItems.regTimestamp.getItemName()) );
  	  entity.setRegUserId( rs.getString(UserItems.regUserId.getItemName()) );
  	  entity.setRegUserNm( rs.getString(UserItems.regUserNm.getItemName()) );
  	  entity.setUpdTimestamp( rs.getTimestamp(UserItems.updTimestamp.getItemName()) );
  	  entity.setUpdUserId( rs.getString(UserItems.updUserId.getItemName()) );
  	  entity.setUpdUserNm( rs.getString(UserItems.updUserNm.getItemName()) );
  	  entity.setLastLoginTimestamp( rs.getTimestamp(UserItems.lastLoginTimestamp.getItemName()) );
  	  entity.setLookAndFeel( rs.getString(UserItems.lookAndFeel.getItemName()) );

  	  return entity;
	}

	private class CustomTemplate extends DaoTemplate {
		protected CustomTemplate( IDaoTemplateCallback callback ) {
			super.setDaoTemplateCallback(callback);
		}
	}

	private class CustomCallback implements IDaoTemplateCallback {
		@Override
		public String appendOfSelectClause(ISqlCondition condition, boolean isCountMode) {
			StringBuffer buf = new StringBuffer()
				.append("  ," + SmTables.SM_USER_LOGIN.alias() + ".LAST_LOGIN_TIMESTAMP");

			return buf.toString();
		}

		@Override
		public String appendOfFromClause(ISqlCondition condition, boolean isCountMode) {
			String alias = SmTables.SM_USER_LOGIN.alias();

			StringBuffer buf = new StringBuffer()
				.append("  LEFT JOIN (")
				.append("    SELECT USER_ID AS L_USER_ID, LAST_LOGIN_TIMESTAMP")
				.append("    FROM SM_USER_LOGIN")
				.append("    WHERE DEL_STS_ID = false")
				.append("  ) " + alias + " ON " + alias + ".L_USER_ID = USER_ID");

			return buf.toString();
		}
	}

}
