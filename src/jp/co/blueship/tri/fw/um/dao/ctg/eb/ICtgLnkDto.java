package jp.co.blueship.tri.fw.um.dao.ctg.eb;

/**
 * The interface of the category link DTO.
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 */
public interface ICtgLnkDto {
	/**
	 * get a category link entity
	 *
	 * @return Category Link Entity
	 */
	public ICtgLnkEntity getCtgLnkEntity();

	/**
	 * set a category link entity
	 *
	 * @param ctgLnkEntity
	 */
	public void setCtgLnkEntity(ICtgLnkEntity ctgLnkEntity);
}
