package jp.co.blueship.tri.fw.um.dao.mstone.eb;

import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the milestone link entity.
 *
 * @version V4.00.00
 * @author Hai Thach
 */
public interface IMstoneLnkEntity extends IEntityFooter {
	/**
	 * milestone-ID
	 * @return milestone-ID
	 */
	public String getMstoneId();
	/**
	 * milestone-ID
	 * @param milestoneId milestone-ID
	 */
	public void setMstoneId(String mstoneId);
	/**
	 * data-category-cd
	 * @return data-category-cd
	 */
	public String getDataCtgCd();
	/**
	 * data-category-cd
	 * @param dataCtgCd data-category-cd
	 */
	public void setDataCtgCd(String dataCtgCd);
	/**
	 * data-ID
	 * @return data-ID
	 */
	public String getDataId();
	/**
	 * data-ID
	 * @param dataId data-ID
	 */
	public void setDataId(String dataId);

}
