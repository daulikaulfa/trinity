package jp.co.blueship.tri.fw.um.dao.locksvc.eb;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the lock service entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface ILockSvcEntity extends IEntityFooter {

	public String getSvcId();
	public void setSvcId(String svcId);

	public String getLockSvcId();
	public void setLockSvcId(String lockSvcId);

	public StatusFlg getLockForLot();
	public void setLockForLot(StatusFlg lockForLot);

}
