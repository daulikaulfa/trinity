package jp.co.blueship.tri.fw.um.dao.hist;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.ex.TriJdbcDaoException;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;
import jp.co.blueship.tri.fw.dao.orm.psql.SqlFormatUtils;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.fw.um.dao.hist.constants.HistItems;
import jp.co.blueship.tri.fw.um.dao.hist.eb.HistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.ViewNotificationToUserCondition;
import jp.co.blueship.tri.rm.dao.constants.RmTables;

/**
 * @version V4.00.00
 * @author Akahoshi
 */
public class ViewNotificationToUserDao extends JdbcBaseDao<IHistEntity> implements IViewNotificationToUserDao{

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	private final String SQL_QUERY =  "SELECT "
									+ "		H.* "
									+ "FROM "
									+ "		(SELECT "
									+ "			MAX( REG_TIMESTAMP ) AS REG_TIMESTAMP, "
									+ "			DATA_ID"
									+ "		 FROM UM_HIST "
									+ "		 WHERE (DATA_CTG_CD = '" + AmTables.AM_AREQ.name() + "' "
									+ "			OR  DATA_CTG_CD = '" + RmTables.RM_RA.name()   + "' )"
									+ "		 GROUP BY DATA_ID) L "
									+ "LEFT JOIN UM_HIST H ON L.REG_TIMESTAMP = H.REG_TIMESTAMP "
									+ "					  AND L.DATA_ID = H.DATA_ID "
									+ "WHERE DEL_STS_ID = " + StatusFlg.off.parseBoolean();


	@Override
	protected ITableAttribute getTableAttribute() {
		return UmTables.UM_HIST;
	}


	@Override
	@Deprecated
	protected ISqlBuilder append(ISqlBuilder builder, IHistEntity entity) {
		return null;
	}


	@Override
	protected IHistEntity entityMapping(ResultSet rs, int row) throws SQLException {
		IHistEntity entity = new HistEntity();

		entity.setHistId		( rs.getString( HistItems.histId.getItemName()) );
		entity.setDataCtgCd		( rs.getString( HistItems.dataCtgCd.getItemName()) );
		entity.setDataId		( rs.getString( HistItems.dataId.getItemName()) );
		entity.setLotId			( rs.getString( HistItems.lotId.getItemName()) );
		entity.setHistData		( rs.getString( HistItems.histData.getItemName()) );
		entity.setStsId			( rs.getString( HistItems.stsId.getItemName()) );
		entity.setActSts		( rs.getString( HistItems.actStsId.getItemName()) );
		entity.setDelStsId		( StatusFlg.value( rs.getBoolean( HistItems.delStsId.getItemName())) );
		entity.setRegTimestamp	( rs.getTimestamp( HistItems.regTimestamp.getItemName()) );
		entity.setRegUserId		( rs.getString( HistItems.regUserId.getItemName()) );
		entity.setRegUserNm		( rs.getString( HistItems.regUserNm.getItemName()) );
		entity.setUpdTimestamp	( rs.getTimestamp( HistItems.updTimestamp.getItemName()) );
		entity.setUpdUserId		( rs.getString( HistItems.updUserId.getItemName()) );
		entity.setUpdUserNm		( rs.getString( HistItems.updUserNm.getItemName()) );

		return entity;
	}


	@Override
	public List<IHistEntity> find( ViewNotificationToUserCondition condition,
								   ISqlSort sort
								   ) throws TriJdbcDaoException{
		try{

			StringBuffer buf = new StringBuffer();
			String sql = null;

			String histId	= (condition.getHistId() != null)?	 " AND " + SqlFormatUtils.getCondition( condition.getHistId(),	 "H." + HistItems.histId.getItemName(),		true, false, false) : "";
			String lotId	= (condition.getLotId() != null)?	 " AND " + SqlFormatUtils.getCondition( condition.getLotId(),	 "H." + HistItems.lotId.getItemName(),		true, false, false) : "";
			String actStsId = (condition.getActStsId() != null)? " AND " + SqlFormatUtils.getCondition( condition.getActStsId(), "H." + HistItems.actStsId.getItemName(),	true, false, false) : "";
			String stsId	= (condition.getStsId() != null)?	 " AND " + SqlFormatUtils.getCondition( condition.getStsId(),	 "H." + HistItems.stsId.getItemName(),		true, false, false) : "";
			String dataCtgCd= (condition.getDataCtgCd() != null)?" AND " + SqlFormatUtils.getCondition( condition.getDataCtgCd(),"H." + HistItems.dataCtgCd.getItemName(),	true, false, false) : "";

			String lotIds	= (condition.getLotIds() != null)?	 " AND " + SqlFormatUtils.getCondition( condition.getLotIds(),	 "H." + HistItems.lotId.getItemName(),		false, true, false) : "";
			String actStsIds= (condition.getActStsIds() != null)?" AND " + SqlFormatUtils.getCondition( condition.getActStsIds(),"H." + HistItems.actStsId.getItemName(),	false, true, false) : "";
			String stsIds	= (condition.getStsIds() != null)?	 " AND " + SqlFormatUtils.getCondition( condition.getStsIds(),	 "H." + HistItems.stsId.getItemName(),		false, true, false) : "";

			String fromTo 	= (condition.isRegTimestampFromToSearch())?
								" AND " + SqlFormatUtils.getFromTo(condition.getRegTimestampFrom(), condition.getRegTimestampTo(), "H." + HistItems.regTimestamp.getItemName()):"";

			buf .append( SQL_QUERY )
				.append( histId )
				.append( lotId )
				.append( actStsId )
				.append( stsId )
				.append( dataCtgCd )
				.append( lotIds )
				.append( actStsIds )
				.append( stsIds )
				.append( fromTo )
				;

			sql = buf.toString() + SqlFormatUtils.toSortString(sort) ;

			List<IHistEntity> rows = super.query(sql, new ParameterizedRowMapper<IHistEntity>() {

				@Override
				public IHistEntity mapRow(ResultSet resultset, int i) throws SQLException {
					return entityMapping(resultset, i);
				}
			});

			return rows;

		} catch (Exception e) {
			throw new TriJdbcDaoException(SmMessageId.SM005004S, e, this.getTableAttribute().name());
		}
	}
}
