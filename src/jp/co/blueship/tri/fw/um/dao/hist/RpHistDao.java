package jp.co.blueship.tri.fw.um.dao.hist;

import java.util.List;

import jp.co.blueship.tri.dm.dao.dmdo.eb.IDmDoEntity;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.oxm.utils.TriXmlUtils;
import jp.co.blueship.tri.fw.schema.beans.rp.RpRecDocument;
import jp.co.blueship.tri.fw.schema.beans.rp.RpType;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.utils.HistMappingUtils;
import jp.co.blueship.tri.rm.dao.constants.RmTables;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpDto;

/**
 * The implements of the rp history DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 */
public class RpHistDao extends HistDao implements IRpHistDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	public int insert(IHistEntity entity , IRpDto rpDto , List<IDmDoEntity> doEntities , List<String> raIds){

		RpRecDocument rpDoc = RpRecDocument.Factory.newInstance();
		RpType rpXml = rpDoc.addNewRpRec();
		rpXml = new HistMappingUtils().mapDB2XMLBeans( rpDto,doEntities,raIds,rpXml );

		entity.setHistId( this.getHistNumberingDao().nextval() );
		entity.setDataCtgCd( RmTables.RM_RP.name() );
		entity.setDataId( rpDto.getRpEntity().getRpId() );
		entity.setHistData( TriXmlUtils.replaceExcessString( rpDoc ) );
		entity.setDelStsId(StatusFlg.off);
		entity.setLotId( rpDto.getRpEntity().getLotId() );
		entity.setStsId( rpDto.getRpEntity().getStsId() );

		return super.insert( entity );
	}

}