package jp.co.blueship.tri.fw.um.dao.dept;

import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;
import jp.co.blueship.tri.fw.um.dao.dept.eb.IDeptEntity;


/**
 * The interface of the department DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IDeptDao extends IJdbcDao<IDeptEntity> {

}
