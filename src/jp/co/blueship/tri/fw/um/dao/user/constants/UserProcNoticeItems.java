package jp.co.blueship.tri.fw.um.dao.user.constants;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;

/**
 * The items of the user proc notice entity.
 *
 * @version V4.00.00
 * @author Satoshi Sasaki
 *
 */
public enum UserProcNoticeItems implements ITableItem {
	procId		 ( "proc_id" ),
	userId		 ( "user_id" ),
	isRead		 ( "is_read" ),
	delStsId	 ( "del_sts_id" ),
	regUserId	 ( "reg_user_id" ),
	regUserNm	 ( "reg_user_nm" ),
	regTimestamp ( "reg_timestamp" ),
	updUserId	 ( "upd_user_id" ),
	updUserNm	 ( "upd_user_nm" ),
	updTimestamp ( "upd_timestamp" ),
	;

	private String element = null;

	private UserProcNoticeItems( String element){
		this.element = element;
	}

	public String getItemName() {
		return this.element;
	}
}
