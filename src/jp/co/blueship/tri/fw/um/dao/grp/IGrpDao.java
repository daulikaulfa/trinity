package jp.co.blueship.tri.fw.um.dao.grp;

import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;


/**
 * The interface of the group DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IGrpDao extends IJdbcDao<IGrpEntity> {

}
