package jp.co.blueship.tri.fw.um.dao.wiki;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.fw.um.dao.wiki.constants.WikiAttachedFileItems;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.IWikiAttachedFileEntity;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.WikiAttachedFileEntity;

/**
 * The implements of the wikiAttachedFile DAO.
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 */
public class WikiAttachedFileDao extends JdbcBaseDao<IWikiAttachedFileEntity> implements IWikiAttachedFileDao {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected ITableAttribute getTableAttribute() {
		return UmTables.UM_WIKI_ATTACHED_FILE;
	}

	@Override
	protected ISqlBuilder append(ISqlBuilder builder, IWikiAttachedFileEntity entity) {
		builder
			.append(WikiAttachedFileItems.wikiId, entity.getWikiId(), true)
			.append(WikiAttachedFileItems.attachedFileSeqNo, entity.getAttachedFileSeqNo())
			.append(WikiAttachedFileItems.filePath, entity.getFilePath())
			.append(WikiAttachedFileItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(WikiAttachedFileItems.regTimestamp, entity.getRegTimestamp())
			.append(WikiAttachedFileItems.regUserId, entity.getRegUserId())
			.append(WikiAttachedFileItems.regUserNm, entity.getRegUserNm())
			.append(WikiAttachedFileItems.updTimestamp, entity.getUpdTimestamp())
			.append(WikiAttachedFileItems.updUserId, entity.getUpdUserId())
			.append(WikiAttachedFileItems.updUserNm, entity.getUpdUserNm())
			;
		return builder;
	}

	@Override
	protected IWikiAttachedFileEntity entityMapping(ResultSet rs, int row) throws SQLException {
		IWikiAttachedFileEntity entity = new WikiAttachedFileEntity();

		entity.setWikiId		( rs.getString(WikiAttachedFileItems.wikiId.getItemName()) );
		entity.setAttachedFileSeqNo
								( rs.getString(WikiAttachedFileItems.attachedFileSeqNo.getItemName()) );
		entity.setFilePath		( rs.getString(WikiAttachedFileItems.filePath.getItemName()) );
		entity.setDelStsId		( StatusFlg.value(rs.getBoolean(WikiAttachedFileItems.delStsId.getItemName())) );
		entity.setRegTimestamp	( rs.getTimestamp(WikiAttachedFileItems.regTimestamp.getItemName()) );
		entity.setRegUserId		( rs.getString(WikiAttachedFileItems.regUserId.getItemName()) );
		entity.setRegUserNm		( rs.getString(WikiAttachedFileItems.regUserNm.getItemName()) );
		entity.setUpdTimestamp	( rs.getTimestamp(WikiAttachedFileItems.updTimestamp.getItemName()) );
		entity.setUpdUserId		( rs.getString(WikiAttachedFileItems.updUserId.getItemName()) );
		entity.setUpdUserNm		( rs.getString(WikiAttachedFileItems.updUserNm.getItemName()) );

		return entity;
	}

}
