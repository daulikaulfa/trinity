package jp.co.blueship.tri.fw.um.dao.role.eb;

import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the role service catalog link entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IRoleSvcCtgLnkEntity extends IEntityFooter {

	public String getRoleId();
	public void setRoleId(String roleId);

	public String getSvcCtgId();
	public void setSvcCtgId(String svcCtgId);

}
