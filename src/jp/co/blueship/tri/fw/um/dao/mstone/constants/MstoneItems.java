package jp.co.blueship.tri.fw.um.dao.mstone.constants;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;

/**
 * The items of the milestone entity.
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 */
public enum MstoneItems implements ITableItem {
	mstoneId("mstone_id"),
	mstoneNm("mstone_nm"),
	mstoneStDate("mstone_st_date"),
	mstoneEndDate("mstone_end_date"),
	content("content"),
	lotId("lot_id"),
	sortOdr("sort_odr"),
	delStsId("del_sts_id"),
	regUserId("reg_user_id"),
	regUserNm("reg_user_nm"),
	regTimestamp("reg_timestamp"),
	updUserId("upd_user_id"),
	updUserNm("upd_user_nm"),
	updTimestamp("upd_timestamp");

	private String element = null;

	private MstoneItems( String element ) {
		this.element = element;
	}

	public String getItemName() {
		return element;
	}

}
