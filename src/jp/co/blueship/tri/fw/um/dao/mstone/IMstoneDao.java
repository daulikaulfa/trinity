package jp.co.blueship.tri.fw.um.dao.mstone;

import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.IMstoneEntity;

/**
 * The interface of the milestone DAO.
 *
 * @version V4.00.00
 * @author Hai Thach
 */
public interface IMstoneDao extends IJdbcDao<IMstoneEntity> {

}
