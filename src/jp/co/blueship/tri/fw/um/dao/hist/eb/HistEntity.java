package jp.co.blueship.tri.fw.um.dao.hist.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.fw.dao.orm.EntityFooter;


/**
 * history entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version V4.00.00
 * @author Yohei Tanaka
 *
 */
public class HistEntity extends EntityFooter implements IHistEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * hist-ID
	 */
	public String histId = null;
	/**
	 * lot id
	 */
	public String lotId = null;
	/**
	 * data catalog code
	 */
	public String dataCtgCd = null;
	/**
	 * data-ID
	 */
	public String dataId = null;
	/**
	 * history data
	 */
	public String histData = null;
	/**
	 * Status id
	 */
	public String stsId = null;
	/**
	 * action status
	 */
	public String actSts = null;
	/**
	 * accs time stamp
	 */
	public Timestamp accsTimestamp = null;
	/**
	 * notice view time stamp
	 */
	public Timestamp noticeViewTimestamp = null;

	/**
	 * hist-IDを取得します。
	 * @return hist-ID
	 */
	public String getHistId() {
	    return histId;
	}
	/**
	 * hist-IDを設定します。
	 * @param histId hist-ID
	 */
	public void setHistId(String histId) {
	    this.histId = histId;
	}
	/**
	 * Lot Idを取得します
	 * @return Lot Id
	 */
	public String getLotId(){
		return this.lotId;
	}
	/**
	 * Lot Idを設定します
	 * @param Lot Id
	 */
	public void setLotId(String lotId){
		this.lotId = lotId;
	}
	/**
	 * data catalog codeを取得します。
	 * @return data catalog code
	 */
	public String getDataCtgCd() {
	    return dataCtgCd;
	}
	/**
	 * data catalog codeを設定します。
	 * @param dataCtgCd data catalog code
	 */
	public void setDataCtgCd(String dataCtgCd) {
	    this.dataCtgCd = dataCtgCd;
	}
	/**
	 * data-IDを取得します。
	 * @return data-ID
	 */
	public String getDataId() {
	    return dataId;
	}
	/**
	 * data-IDを設定します。
	 * @param dataId data-ID
	 */
	public void setDataId(String dataId) {
	    this.dataId = dataId;
	}
	/**
	 * history dataを取得します。
	 * @return history data
	 */
	public String getHistData() {
	    return histData;
	}
	/**
	 * history dataを設定します。
	 * @param histData history data
	 */
	public void setHistData(String histData) {
	    this.histData = histData;
	}
	/**
	 * Status Idを取得します
	 * @return Status Id
	 */
	public String getStsId(){
		return this.stsId;
	}
	/**
	 * Status Idを設定します
	 * @param Status Id
	 */
	public void setStsId(String stsId){
		this.stsId = stsId;
	}
	/**
	 * Action Statusを取得します
	 * @return Action Status
	 */
	public String getActSts(){
		return this.actSts;
	}
	/**
	 * Action Statusを設定します
	 * @param Action Status
	 */
	public void setActSts(String actSts){
		this.actSts = actSts;
	}

	/**
	 * set the accs timestmp
	 * @param accs timesstamp
	 */
	public void setAccsTimestamp( Timestamp accsTimestamp ){
		this.accsTimestamp = accsTimestamp;
	}
	@Override
	public Timestamp getAccsTimestamp() {
	    return accsTimestamp;
	}

	/**
	 * set the notice view timestmp
	 * @param notice view timesstamp
	 */
	public void setNoticeViewTimestamp( Timestamp noticeViewTimestamp ){
		this.noticeViewTimestamp = noticeViewTimestamp;
	}
	@Override
	public Timestamp getNoticeViewTimestamp() {
	    return noticeViewTimestamp;
	}
}