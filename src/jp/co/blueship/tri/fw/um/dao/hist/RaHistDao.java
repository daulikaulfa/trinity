package jp.co.blueship.tri.fw.um.dao.hist;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.oxm.utils.TriXmlUtils;
import jp.co.blueship.tri.fw.schema.beans.ra.RaRecDocument;
import jp.co.blueship.tri.fw.schema.beans.ra.RaType;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.utils.HistMappingUtils;
import jp.co.blueship.tri.rm.dao.constants.RmTables;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaDto;

/**
 * The implements of the ra history DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 */
public class RaHistDao extends HistDao implements IRaHistDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	public int insert(IHistEntity entity , IRaDto raDto){

		RaRecDocument raDoc = RaRecDocument.Factory.newInstance();
		RaType raXml = raDoc.addNewRaRec();
		raXml = new HistMappingUtils().mapDB2XMLBeans( raDto, raXml );

		entity.setHistId( this.getHistNumberingDao().nextval() );
		entity.setDataCtgCd( RmTables.RM_RA.name() );
		entity.setDataId( raDto.getRaEntity().getRaId() );
		entity.setHistData( TriXmlUtils.replaceExcessString( raDoc ) );
		entity.setDelStsId( StatusFlg.off );
		entity.setLotId( raDto.getRaEntity().getLotId() );
		entity.setStsId( raDto.getRaEntity().getStsId() );

		return super.insert( entity );
	}
}