package jp.co.blueship.tri.fw.um.dao.user;

import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;


/**
 * The interface of the user DAO.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public interface IUserDao extends IJdbcDao<IUserEntity> {

}
