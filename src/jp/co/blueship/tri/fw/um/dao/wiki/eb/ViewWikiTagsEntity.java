package jp.co.blueship.tri.fw.um.dao.wiki.eb;

public class ViewWikiTagsEntity implements IViewWikiTagsEntity{

	private String tagNm = null;
	private String lotId = null;
	private Integer count = null;

	@Override
	public String getTagNm() {
		return this.tagNm;
	}
	@Override
	public void setTagNm(String tagNm) {
		this.tagNm = tagNm;
	}

	@Override
	public String getLotId() {
		return this.lotId;
	}
	@Override
	public void setLotId(String lotId) {
		this.lotId = lotId;
	}

	@Override
	public Integer count() {
		return this.count;
	}
	@Override
	public void setCount(Integer count) {
		this.count = count;
	}

}
