package jp.co.blueship.tri.fw.um.dao.mstone.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.dao.orm.psql.SqlFormatUtils;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.fw.um.dao.mstone.constants.MstoneItems;
/**
 * The SQL condition of the milestone entity.
 *
 * @version V4.00.00
 * @author Hai Thach
 */
public class MstoneCondition extends ConditionSupport{
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute milestone = UmTables.UM_MSTONE;

	/**
	 * milestone-id
	 */
	public String mstoneId = null;
	/**
	 * milestone-id's
	 */
	public String[] mstoneIds = null;
	/**
	 * milestone name
	 */
	public String mstoneNm = null;
	/**
	 * lot ID
	 */
	public String lotId = null;
	/**
	 * lot ID's
	 */
	public String[] lotIds = null;
	/**
	 * milestone start date
	 */
	public String mstoneStDate = null;
	/**
	 * milestone end date
	 */
	public String mstoneEndDate = null;
	/**
	 * content
	 */
	public String content = null;
	/**
	/**
	 * delete status Id
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		   super.append(MstoneItems.delStsId, StatusFlg.off.parseBoolean() );
	}

	public MstoneCondition() {
		super(milestone);
	}

	/**
	 * milestone-Id
	 * @return milestone-Id
	 */
	public String getMstoneId() {
		return mstoneId;
	}

	/**
	 * milestone-Id
	 * @param mstoneId milestone-Id
	 */
	public void setMstoneId(String mstoneId) {
		this.mstoneId = mstoneId;
		super.append(MstoneItems.mstoneId, mstoneId);
	}

	/**
	 * milestone-Id's
	 * @return milestone-Id's
	 */
	public String[] getMstoneIds() {
		return mstoneIds;
	}

	/**
	 * milestone-Id's
	 * @param mstoneId milestone-Id's
	 */
	public void setMstoneIds(String... mstoneIds) {
		this.mstoneIds = mstoneIds;
		super.append( MstoneItems.mstoneId.getItemName() + "[]",
				SqlFormatUtils.getCondition(mstoneIds, MstoneItems.mstoneId.getItemName(), false, true, false) );
	}

	/**
	 * @return
	 */
	public String[] getMstoneIdsByNotEquals() {
		return mstoneIds;
	}

	/**
	 * @param mstoneIds
	 */
	public void setMstoneIdsByNotEquals(String... mstoneIds) {
		this.mstoneIds = mstoneIds;
		super.append( MstoneItems.mstoneId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(mstoneIds, MstoneItems.mstoneId.getItemName(), false, false, true) );
	}

	/**
	 * milestone name
	 * @return milestone name
	 */
	public String getMstoneNm() {
		return mstoneNm;
	}

	/**
	 * milestone name
	 * @param mstoneNm milestone name
	 */
	public void setMstoneNm(String mstoneNm) {
		this.mstoneNm = mstoneNm;
		super.append(MstoneItems.mstoneNm, mstoneNm);
	}

	/**
	 * lot id
	 * @return lot id
	 */
	public String getLotId() {
		return lotId;
	}
	/**
	 * lot id
	 * @param lotId lot id
	 */
	public void setLotId(String lotId) {
		this.lotId = lotId;
		super.append(MstoneItems.lotId, lotId);
	}

	/**
	 * @return
	 */
	public String[] getLotIds() {
		return lotIds;
	}
	/**
	 * @param lotIds
	 */
	public void setLotIds(String[] lotIds) {
		this.lotIds = lotIds;
		super.append( MstoneItems.lotId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(lotIds, MstoneItems.lotId.getItemName(), false, true, false) );
	}

	/**
	 * milestone start date
	 * @return milestone start date
	 */
	public String getMstoneStDate() {
		return mstoneStDate;
	}

	/**
	 * milestone start date
	 * @param mstoneStDate milestone start date
	 */
	public void setMstoneStDate(String mstoneStDate) {
		this.mstoneStDate = mstoneStDate;
		super.append(MstoneItems.mstoneStDate, mstoneStDate);
	}

	/**
	 * milestone end date
	 * @return milestone end date
	 */
	public String getMstoneEndDate() {
		return mstoneEndDate;
	}

	/**
	 * milestone end date
	 * @param mstoneEndDate milestone end date
	 */
	public void setMstoneEndDate(String mstoneEndDate) {
		this.mstoneEndDate = mstoneEndDate;
		super.append(MstoneItems.mstoneEndDate, mstoneEndDate);
	}

	public void setFromToEndDate(Timestamp from, Timestamp to){
		super.append(MstoneItems.mstoneEndDate.getItemName() ,
				SqlFormatUtils.getFromTo(from, to, MstoneItems.mstoneEndDate.getItemName()));
	}

	/**
	 * content
	 * @return content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * content
	 * @param content content
	 */
	public void setContent(String content) {
		this.content = content;
		super.append(MstoneItems.content, content);
	}

	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	public void setDelStsId(StatusFlg delStsId){
	    this.delStsId = delStsId;
	    super.append( MstoneItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}

}
