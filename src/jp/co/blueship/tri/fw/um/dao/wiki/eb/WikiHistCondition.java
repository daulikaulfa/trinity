package jp.co.blueship.tri.fw.um.dao.wiki.eb;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.fw.um.dao.wiki.constants.WikiHistItems;

/**
 * The SQL condition of the wikiHist entity.
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 */
public class WikiHistCondition extends ConditionSupport {

	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute wikiHist = UmTables.UM_WIKI_HIST;

	public WikiHistCondition() {
		super(wikiHist);
	}

	public String wikiId = null;
	public Integer wikiVerNo = null;
	public StatusFlg delStsId = StatusFlg.off;
	{
		super.append(WikiHistItems.delStsId, StatusFlg.off.parseBoolean() );
	}

	public String getWikiId(){
		return this.wikiId;
	}
	public void setWikiId(String wikiId){
		this.wikiId = wikiId;
		super.append( WikiHistItems.wikiId, wikiId);
	}

	public Integer getWikiVerNo(){
		return this.wikiVerNo;
	}
	public void setWikiVerNo(Integer wikiVerNo){
		this.wikiVerNo = wikiVerNo;
		super.append( WikiHistItems.wikiVerNo, wikiVerNo);
	}

	public StatusFlg getDelStsId(){
		return this.delStsId;
	}
	public void setDelStsId(StatusFlg delStsId){
		this.delStsId = delStsId;
		super.append( WikiHistItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean());
	}
}
