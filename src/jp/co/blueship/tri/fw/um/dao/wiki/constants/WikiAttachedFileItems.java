package jp.co.blueship.tri.fw.um.dao.wiki.constants;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;

public enum WikiAttachedFileItems implements ITableItem {
	wikiId			("wiki_id"),
	attachedFileSeqNo
					("attached_file_seq_no"),
	filePath		("file_path"),
	delStsId		("del_sts_id"),
	regUserId		("reg_user_id"),
	regUserNm		("reg_user_nm"),
	regTimestamp	("reg_timestamp"),
	updUserId		("upd_user_id"),
	updUserNm		("upd_user_nm"),
	updTimestamp	("upd_timestamp"),
	;

	private String element = null;

	private WikiAttachedFileItems( String element) {
		this.element = element;
	}

	public String getItemName() {
		return this.element;
	}
}
