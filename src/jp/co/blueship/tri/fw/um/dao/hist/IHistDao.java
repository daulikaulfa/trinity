package jp.co.blueship.tri.fw.um.dao.hist;

import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;


/**
 * The interface of the history DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IHistDao extends IJdbcDao<IHistEntity> {

}
