package jp.co.blueship.tri.fw.um.dao.wiki;

import java.util.List;

import jp.co.blueship.tri.fw.dao.orm.IDao;
import jp.co.blueship.tri.fw.dao.orm.ex.TriJdbcDaoException;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.IViewWikiTagsEntity;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.ViewWikiTagsCondition;

/**
 * The interface of the asset register view.
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public interface IViewWikiTagsDao extends IDao {

	/**
	 * 指定された検索条件で検索を行います。
	 *
     * @param condition 検索条件
	 * @return 検索結果を戻します。
	 * @throws TriJdbcDaoException
	 */
	public List<IViewWikiTagsEntity> find( ViewWikiTagsCondition condition ) throws TriJdbcDaoException;

}
