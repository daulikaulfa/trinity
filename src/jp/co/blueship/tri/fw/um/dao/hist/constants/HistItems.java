package jp.co.blueship.tri.fw.um.dao.hist.constants;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;


/**
 * The items of the history entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public enum HistItems implements ITableItem {
	histId("hist_id"),
	dataCtgCd("data_ctg_cd"),
	dataId("data_id"),
	lotId("lot_id"),
	histData("hist_data"),
	stsId("sts_id"),
	actStsId("act_sts_id"),
	delStsId("del_sts_id"),
	regUserId("reg_user_id"),
	regUserNm("reg_user_nm"),
	regTimestamp("reg_timestamp"),
	updUserId("upd_user_id"),
	updUserNm("upd_user_nm"),
	updTimestamp("upd_timestamp"),
	;


	private String element = null;

	private HistItems( String element) {
		this.element = element;
	}

	public String getItemName() {
		return this.element;
	}

}
