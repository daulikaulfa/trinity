package jp.co.blueship.tri.fw.um.dao.user.eb;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.dao.orm.psql.SqlFormatUtils;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.fw.um.dao.user.constants.UserItems;

/**
 * The SQL condition of the user entity.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public class UserCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute user = UmTables.UM_USER;

	public UserCondition() {
		super(user);
	}

	/**
	 * user-ID
	 */
	public String userId = null;
	/**
	 * user-ID's
	 */
	public String[] userIds = null;
	/**
	 * user name
	 */
	public String userNm = null;
	/**
	 * dept-ID
	 */
	public String deptId = null;
	
	/**
	 * keyword
	 */
	public String[] keywords = null;
	
	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		   super.append(UserItems.delStsId, StatusFlg.off.parseBoolean() );
	}

	/**
	 * user-IDを取得します。
	 * @return user-ID
	 */
	public String getUserId() {
	    return userId;
	}
	/**
	 * user-IDを設定します。
	 * @param userId user-ID
	 */
	public void setUserId(String userId) {
	    this.userId = userId;
	    super.append( UserItems.userId, userId );
	}

	/**
	 * user-ID'sを取得します。
	 * @return user-ID's
	 */
	public String[] getUserIds() {
	    return userIds;
	}
	/**
	 * user-ID'sを設定します。
	 * @param userId user-ID's
	 */
	public void setUserIds(String[] userIds) {
	    this.userIds = userIds;
	    super.append( UserItems.userId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(userIds, UserItems.userId.getItemName(), false, true, false) );
	}
	/**
	 * user nameを取得します。
	 * @return user name
	 */
	public String getUserNm() {
	    return userNm;
	}
	/**
	 * user nameを設定します。
	 * @param userNm user name
	 */
	public void setUserNm(String userNm) {
	    this.userNm = userNm;
	    super.append( UserItems.userNm, userNm );
	}
	/**
	 * dept-IDを取得します。
	 * @return dept-ID
	 */
	public String getDeptId() {
	    return deptId;
	}
	/**
	 * dept-IDを設定します。
	 * @param deptId dept-ID
	 */
	public void setDeptId(String deptId) {
	    this.deptId = deptId;
	    super.append( UserItems.deptId, deptId );
	}

	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	    super.append( UserItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}
	
	public String[] getUserIdsByNotEquals() {
		return userIds;
	}
	
	public void setUserIdsByNotEquals(String... userIds) {
		this.userIds = userIds;
		super.append(UserItems.userId.getItemName() + "[]", 
				SqlFormatUtils.getCondition(userIds, UserItems.userId.getItemName(), false, false, true));
	}
	
	/**
	 * keyword を設定します。（中間一致）
	 * @param keyword
	 */
	public void setContainsByKeyword(String... keywords) {
		if ( TriStringUtils.isEmpty(keywords) ) {
			return;
		}

	    this.keywords = keywords;
	    super.append( UserItems.userId.getItemName() + "Other",
	    		SqlFormatUtils.joinCondition( true,
	    			new String[]{
	    			SqlFormatUtils.getCondition( this.keywords, UserItems.userId.getItemName(), true, false, false),
	    			SqlFormatUtils.getCondition( this.keywords, UserItems.userNm.getItemName(), true, false, false),
	    			SqlFormatUtils.getCondition( this.keywords, UserItems.emailAddr.getItemName(), true, false, false)
	    			}));
	}
	
	public String[] getKeywords() {
		return keywords;
	}
	
}
