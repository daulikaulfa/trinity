package jp.co.blueship.tri.fw.um.dao.grp.eb;

import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the group user link entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IGrpUserLnkEntity extends IEntityFooter {

	public String getGrpId();
	public void setGrpId(String grpId);

	public String getUserId();
	public void setUserId(String userId);
}
