package jp.co.blueship.tri.fw.um.dao.role.eb;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.dao.orm.psql.SqlFormatUtils;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.fw.um.dao.role.constants.RoleSvcCtgLnkItems;

/**
 * The SQL condition of the role service catalog link entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class RoleSvcCtgLnkCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute role = UmTables.UM_ROLE_SVC_CTG_LNK;

	/**
	 * role-ID
	 */
	public String roleId = null;
	/**
	 * role-ID's
	 */
	public String[] roleIds = null;
	/**
	 * svc-ctg-ID
	 */
	public String svcCtgId = null;
	/**
	 * svc-ctg-ID's
	 */
	public String[] svcCtgIds = null;
	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		   super.append(RoleSvcCtgLnkItems.delStsId, StatusFlg.off.parseBoolean() );
	}

	public RoleSvcCtgLnkCondition(){
		super(role);
	}

	/**
	 * role-IDを取得します。
	 * @return role-ID
	 */
	public String getRoleId() {
	    return roleId;
	}

	/**
	 * role-IDを設定します。
	 * @param roleId role-ID
	 */
	public void setRoleId(String roleId) {
	    this.roleId = roleId;
	    super.append(RoleSvcCtgLnkItems.roleId, roleId );
	}

	/**
	 * role-ID'sを取得します。
	 * @return role-ID's
	 */
	public String[] getRoleIds() {
	    return roleIds;
	}

	/**
	 * role-ID'sを設定します。
	 * @param roleIds role-ID's
	 */
	public void setRoleIds(String[] roleIds) {
	    this.roleIds = roleIds;
	    super.append( RoleSvcCtgLnkItems.roleId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(roleIds, RoleSvcCtgLnkItems.roleId.getItemName(), false, true, false) );
	}

	/**
	 * svc-ctg-IDを取得します。
	 * @return svc-ctg-ID
	 */
	public String getSvcCtgId() {
	    return svcCtgId;
	}

	/**
	 * svc-ctg-IDを設定します。
	 * @param svcCtgId svc-ctg-ID
	 */
	public void setSvcCtgId(String svcCtgId) {
	    this.svcCtgId = svcCtgId;
	    super.append(RoleSvcCtgLnkItems.svcCtgId, svcCtgId );
	}

	/**
	 * svc-ctg-ID'sを取得します。
	 * @return svc-ctg-ID's
	 */
	public String[] getSvcCtgIds() {
	    return svcCtgIds;
	}

	/**
	 * svc-ctg-ID'sを設定します。
	 * @param roleIds svc-ctg-ID's
	 */
	public void setSvcCtgIds(String[] svcCtgIds) {
	    this.svcCtgIds = svcCtgIds;
	    super.append( RoleSvcCtgLnkItems.svcCtgId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(svcCtgIds, RoleSvcCtgLnkItems.svcCtgId.getItemName(), false, true, false) );
	}

	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	    super.append( RoleSvcCtgLnkItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}
}