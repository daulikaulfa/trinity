package jp.co.blueship.tri.fw.um.dao.ctg;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

import jp.co.blueship.tri.fw.dao.orm.INumberingDao;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.ex.TriJdbcDaoException;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;

public class CtgNumberingDao extends JdbcBaseDao<String> implements INumberingDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;
	private final String SEQ_NAME = "UM_CTG_ID_SEQ";
	private final String SQL_SELECT_QUERY = "select (to_char(now(), 'YYMM') || ltrim(to_char(nextval('" + SEQ_NAME + "'), '0000' ))) as nextval";

	@Override
	public String nextval() throws TriJdbcDaoException {
		try {
			List<?> rows = super.query(
					SQL_SELECT_QUERY,
					new ParameterizedRowMapper<String>() {
						@Override
						public String mapRow(ResultSet resultset, int i) throws SQLException {
							return entityMapping( resultset, i );
						}
					});

			return (String)rows.get(0);
		} catch ( Exception e ) {
			throw new TriJdbcDaoException( SmMessageId.SM005009S, e, SEQ_NAME );
		}
	}

	@Override
	protected ITableAttribute getTableAttribute() {
		return UmTables.UM_CTG;
	}

	@Override
	protected ISqlBuilder append(ISqlBuilder builder, String entity) {
		return builder;
	}

	@Override
	protected String entityMapping(ResultSet resultSet, int row) throws SQLException {
		return resultSet.getString("nextval");
	}

}
