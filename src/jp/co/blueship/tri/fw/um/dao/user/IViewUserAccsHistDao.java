package jp.co.blueship.tri.fw.um.dao.user;

import java.util.List;

import jp.co.blueship.tri.fw.dao.orm.IDao;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.ex.TriJdbcDaoException;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.ViewUserAccsHistCondition;

/**
 * The interface of the user access history view.
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public interface IViewUserAccsHistDao extends IDao {
	
	public List<IUserEntity> find( ViewUserAccsHistCondition condition, ISqlSort sort, int viewRows ) throws TriJdbcDaoException;
}
