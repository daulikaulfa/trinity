package jp.co.blueship.tri.fw.um.dao.role.eb;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.dao.orm.psql.SqlFormatUtils;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.fw.um.dao.role.constants.RoleItems;

/**
 * The SQL condition of the group entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class RoleCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute role = UmTables.UM_ROLE;

	/**
	 * role-ID
	 */
	public String roleId = null;
	/**
	 * role-ID's
	 */
	public String[] roleIds = null;
	/**
	 * role. name
	 */
	public String roleName = null;
	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		   super.append(RoleItems.delStsId, StatusFlg.off.parseBoolean() );
	}

	public RoleCondition(){
		super(role);
	}

	/**
	 * role-IDを取得します。
	 * @return role-ID
	 */
	public String getRoleId() {
	    return roleId;
	}

	/**
	 * role-IDを設定します。
	 * @param roleId role-ID
	 */
	public void setRoleId(String roleId) {
	    this.roleId = roleId;
	    super.append(RoleItems.roleId, roleId );
	}

	/**
	 * role-ID'sを取得します。
	 * @return role-ID's
	 */
	public String[] getRoleIds() {
	    return roleIds;
	}

	/**
	 * role-ID'sを設定します。
	 * @param roleIds role-ID's
	 */
	public void setRoleIds(String[] roleIds) {
	    this.roleIds = roleIds;
	    super.append( RoleItems.roleId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(roleIds, RoleItems.roleId.getItemName(), false, true, false) );
	}

	/**
	 * role. nameを取得します。
	 * @return role. name
	 */
	public String getRoleName() {
	    return roleName;
	}

	/**
	 * role. nameを設定します。
	 * @param roleNm role. name
	 */
	public void setRoleName(String roleName) {
	    this.roleName = roleName;
	    super.append( RoleItems.roleName, roleName );
	}

	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	    super.append( RoleItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}

	public String[] getRoleIdsByNotEquals() {
		return roleIds;
	}

	public void setRoleIdsByNotEquals(String... roleIds) {
		this.roleIds = roleIds;
		super.append( RoleItems.roleId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(roleIds, RoleItems.roleId.getItemName(), false, false, true) );
	}
}