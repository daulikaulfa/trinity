package jp.co.blueship.tri.fw.um.dao.role.eb;

import jp.co.blueship.tri.fw.dao.orm.EntityFooter;

/**
 * role service catalog link entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class RoleSvcCtgLnkEntity extends EntityFooter implements IRoleSvcCtgLnkEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * role-ID
	 */
	public String roleId = null;
	/**
	 * svc ctg-ID
	 */
	public String svcCtgId = null;

	/**
	 * role-IDを取得します。
	 * @return role-ID
	 */
	public String getRoleId() {
	    return roleId;
	}
	/**
	 * role-IDを設定します。
	 * @param roleId role-ID
	 */
	public void setRoleId(String roleId) {
	    this.roleId = roleId;
	}
	/**
	 * svc ctg-IDを取得します。
	 * @return svc ctg-ID
	 */
	public String getSvcCtgId() {
	    return svcCtgId;
	}
	/**
	 * svc ctg-IDを設定します。
	 * @param svcCtgId svc ctg-ID
	 */
	public void setSvcCtgId(String svcCtgId) {
	    this.svcCtgId = svcCtgId;
	}

}