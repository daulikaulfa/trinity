package jp.co.blueship.tri.fw.um.dao.wiki.eb;

import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

public interface IWikiAttachedFileEntity extends IEntityFooter {

	public String getWikiId();
	public void setWikiId(String wikiId);

	public String getAttachedFileSeqNo();
	public void setAttachedFileSeqNo(String attachedFileSeqNo);

	public String getFilePath();
	public void setFilePath(String filePath);
}
