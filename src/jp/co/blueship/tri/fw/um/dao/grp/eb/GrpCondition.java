package jp.co.blueship.tri.fw.um.dao.grp.eb;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.dao.orm.psql.SqlFormatUtils;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.fw.um.dao.grp.constants.GrpItems;

/**
 * The SQL condition of the group entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class GrpCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute grp = UmTables.UM_GRP;

	/**
	 * grp-ID
	 */
	public String grpId = null;
	/**
	 * grp-ID's
	 */
	public String[] grpIds = null;
	/**
	 * grp. name
	 */
	public String grpNm = null;
	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		   super.append(GrpItems.delStsId, StatusFlg.off.parseBoolean() );
	}

	public GrpCondition(){
		super(grp);
	}

	/**
	 * grp-IDを取得します。
	 * @return grp-ID
	 */
	public String getGrpId() {
	    return grpId;
	}

	/**
	 * grp-IDを設定します。
	 * @param grpId grp-ID
	 */
	public void setGrpId(String grpId) {
	    this.grpId = grpId;
	    super.append(GrpItems.grpId, grpId );
	}

	/**
	 * grp-ID'sを取得します。
	 * @return grp-ID's
	 */
	public String[] getGrpIds() {
	    return grpIds;
	}

	/**
	 * grp-ID'sを設定します。
	 * @param grpIds grp-ID's
	 */
	public void setGrpIds(String[] grpIds) {
	    this.grpIds = grpIds;
	    super.append( GrpItems.grpId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(grpIds, GrpItems.grpId.getItemName(), false, true, false) );
	}

	/**
	 * grp. nameを取得します。
	 * @return grp. name
	 */
	public String getGrpNm() {
	    return grpNm;
	}

	/**
	 * grp. nameを設定します。
	 * @param grpNm grp. name
	 */
	public void setGrpNm(String grpNm) {
	    this.grpNm = grpNm;
	    super.append( GrpItems.grpNm, grpNm );
	}

	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	    super.append( GrpItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}

	public String[] getGrpIdsByNotEquals() {
		return grpIds;
	}

	public void setGrpIdsByNotEquals(String... grpIds) {
		this.grpIds = grpIds;
		super.append( GrpItems.grpId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(grpIds, GrpItems.grpId.getItemName(), false, false, true) );
	}
}