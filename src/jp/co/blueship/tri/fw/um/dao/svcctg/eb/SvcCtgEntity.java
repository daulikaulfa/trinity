package jp.co.blueship.tri.fw.um.dao.svcctg.eb;

import jp.co.blueship.tri.fw.dao.orm.EntityFooter;

/**
 * service catalog entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public class SvcCtgEntity extends EntityFooter implements ISvcCtgEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * service ctg-ID
	 */
	public String svcCtgId = null;
	/**
	 * service category name
	 */
	public String svcCtgNm = null;
	/**
	 * service category content
	 */
	public String svcCtgContent = null;
	/**
	 * service category path
	 */
	public String svcCtgPath = null;

	/**
	 * sort order
	 */
	public Integer sortOdr = null;

	/**
	 * service ctg-IDを取得します。
	 * @return service ctg-ID
	 */
	public String getSvcCtgId() {
		return svcCtgId;
	}
	/**
	 * service ctg-IDを設定します。
	 * @param svcCtgId service ctg-ID
	 */
	public void setSvcCtgId(String svcCtgId) {
		this.svcCtgId = svcCtgId;
	}
	/**
	 * service catalog nameを取得します。
	 * @return service catalog name
	 */
	public String getSvcCtgNm() {
		return svcCtgNm;
	}
	/**
	 * service catalog nameを設定します。
	 * @param svcCtgNm service catalog name
	 */
	public void setSvcCtgNm(String svcCtgNm) {
		this.svcCtgNm = svcCtgNm;
	}
	/**
	 * service catalog contentを取得します。
	 * @return service catalog content
	 */
	public String getSvcCtgContent() {
		return svcCtgContent;
	}
	/**
	 * service catalog contentを設定します。
	 * @param svcCtgContent service catalog content
	 */
	public void setSvcCtgContent(String svcCtgContent) {
		this.svcCtgContent = svcCtgContent;
	}

	@Override
	public String getSvcCtgPath() {
		return svcCtgPath;
	}
	@Override
	public void setSvcCtgPath(String svcCtgPath) {
		this.svcCtgPath = svcCtgPath;
	}

	public Integer getSortOdr() {
		return sortOdr;
	}
	public void setSortOdr(Integer sortOdr) {
		this.sortOdr = sortOdr;
	}
}