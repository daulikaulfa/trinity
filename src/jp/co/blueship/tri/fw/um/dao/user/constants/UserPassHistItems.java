package jp.co.blueship.tri.fw.um.dao.user.constants;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;


/**
 * The items of the user password histry entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public enum UserPassHistItems implements ITableItem {
	userId("user_id"),
	passHistSeqNo("pass_hist_seq_no"),
	userPass("user_pass"),
	delStsId("del_sts_id"),
	regUserId("reg_user_id"),
	regUserNm("reg_user_nm"),
	regTimestamp("reg_timestamp"),
	updUserId("upd_user_id"),
	updUserNm("upd_user_nm"),
	updTimestamp("upd_timestamp");

	private String element = null;

	private UserPassHistItems( String element) {
		this.element = element;
	}

	public String getItemName() {
		return this.element;
	}

}
