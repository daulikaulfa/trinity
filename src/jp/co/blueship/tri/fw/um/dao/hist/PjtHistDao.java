package jp.co.blueship.tri.fw.um.dao.hist;

import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;

public class PjtHistDao extends HistDao implements IPjtHistDao{

	public int insert(IHistEntity entity , IPjtEntity pjtEntity){
		entity.setHistId( this.getHistNumberingDao().nextval() );
		entity.setDataCtgCd( AmTables.AM_PJT.name() );
		entity.setDataId( pjtEntity.getPjtId() );
		entity.setHistData( "" );
		entity.setDelStsId( StatusFlg.off );
		entity.setLotId( pjtEntity.getLotId() );
		entity.setStsId( pjtEntity.getStsId() );

		return super.insert( entity );
	}
}
