package jp.co.blueship.tri.fw.um.dao.accshist;

import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;
import jp.co.blueship.tri.fw.um.dao.accshist.eb.IAccsHistEntity;


/**
 * The interface of the access history DAO.
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 */
public interface IAccsHistDao extends IJdbcDao<IAccsHistEntity> {

}
