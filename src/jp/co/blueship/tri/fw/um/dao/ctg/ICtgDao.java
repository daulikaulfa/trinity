package jp.co.blueship.tri.fw.um.dao.ctg;

import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.ICtgEntity;

/**
 * The interface of the category DAO.
 *
 * @version V4.00.00
 * @author Hai Thach
 */
public interface ICtgDao extends IJdbcDao<ICtgEntity> {

}
