package jp.co.blueship.tri.fw.um.dao.locksvc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.ISqlCondition;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.fw.um.dao.locksvc.constants.LockSvcItems;
import jp.co.blueship.tri.fw.um.dao.locksvc.eb.ILockSvcEntity;
import jp.co.blueship.tri.fw.um.dao.locksvc.eb.LockSvcCondition;
import jp.co.blueship.tri.fw.um.dao.locksvc.eb.LockSvcEntity;

/**
 * The implements of the lock service DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 */
public class LockSvcDao extends JdbcBaseDao<ILockSvcEntity> implements ILockSvcDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	private String toLockQuery( ISqlCondition condition ) {
		StringBuffer buf = new StringBuffer();

		String sql = null;

		buf
		.append("SELECT A.*")
		.append(" FROM UM_LOCK_SVC A")
		.append(" WHERE A.LOCK_SVC_ID IN (")
		.append(" SELECT LOCK_SVC_ID FROM UM_LOCK_SVC")
		.append( condition.toQueryString() )
		.append(" ) FOR UPDATE")
		;
		sql = buf.toString();

		return sql;
	}

	public final List<ILockSvcEntity> lock( String svcId ) {
		LockSvcCondition condition = new LockSvcCondition();
		condition.setSvcId( svcId );

		String sql = this.toLockQuery(condition.getCondition());

		{
			//行レベルでロック
			super.query(sql, new ParameterizedRowMapper<ResultSet>() {
				@Override
				public ResultSet mapRow(ResultSet resultset, int i) throws SQLException {
					return resultset;
				}
			}, condition.getCondition());
		}

		SortBuilder sort = new SortBuilder();
		sort.setElement(LockSvcItems.lockSvcId, TriSortOrder.Asc, 1);

		return this.find(condition.getCondition(), sort);
	}

	@Override
	protected final ITableAttribute getTableAttribute() {
		return UmTables.UM_LOCK_SVC;
	}

	@Override
	protected final ISqlBuilder append( ISqlBuilder builder, ILockSvcEntity entity ) {
		builder
			.append(LockSvcItems.svcId, entity.getSvcId(), true)
			.append(LockSvcItems.lockSvcId, entity.getLockSvcId(), true)
			.append(LockSvcItems.lockForLot, (null == entity.getLockForLot())? StatusFlg.off.parseBoolean(): entity.getLockForLot().parseBoolean())
			.append(LockSvcItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(LockSvcItems.regTimestamp, entity.getRegTimestamp())
			.append(LockSvcItems.regUserId, entity.getRegUserId())
			.append(LockSvcItems.regUserNm, entity.getRegUserNm())
			.append(LockSvcItems.updTimestamp, entity.getUpdTimestamp())
			.append(LockSvcItems.updUserId, entity.getUpdUserId())
			.append(LockSvcItems.updUserNm, entity.getUpdUserNm())
			;

		return builder;
	}

	@Override
	protected final ILockSvcEntity entityMapping( ResultSet rs, int row ) throws SQLException {
  	  ILockSvcEntity entity = new LockSvcEntity();

  	  entity.setSvcId( rs.getString(LockSvcItems.svcId.getItemName()) );
  	  entity.setLockSvcId( rs.getString(LockSvcItems.lockSvcId.getItemName()) );
  	  entity.setLockForLot( StatusFlg.value(rs.getBoolean(LockSvcItems.lockForLot.getItemName())) );
  	  entity.setDelStsId( StatusFlg.value(rs.getBoolean(LockSvcItems.delStsId.getItemName())) );
  	  entity.setRegTimestamp( rs.getTimestamp(LockSvcItems.regTimestamp.getItemName()) );
  	  entity.setRegUserId( rs.getString(LockSvcItems.regUserId.getItemName()) );
  	  entity.setRegUserNm( rs.getString(LockSvcItems.regUserNm.getItemName()) );
  	  entity.setUpdTimestamp( rs.getTimestamp(LockSvcItems.updTimestamp.getItemName()) );
  	  entity.setUpdUserId( rs.getString(LockSvcItems.updUserId.getItemName()) );
  	  entity.setUpdUserNm( rs.getString(LockSvcItems.updUserNm.getItemName()) );

  	  return entity;
	}

}
