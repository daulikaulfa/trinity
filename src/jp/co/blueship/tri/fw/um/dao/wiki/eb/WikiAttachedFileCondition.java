package jp.co.blueship.tri.fw.um.dao.wiki.eb;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.fw.um.dao.wiki.constants.WikiAttachedFileItems;


/**
 * The SQL condition of the wikiAttachedFile entity.
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 */
public class WikiAttachedFileCondition extends ConditionSupport {

	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute wiki = UmTables.UM_WIKI_ATTACHED_FILE;

	public WikiAttachedFileCondition() {
		super(wiki);
	}

	public String wikiId = null;
	public String filePath = null;
	public String attachedFileSeqNo = null;
	public StatusFlg delStsId = null;

	public String getWikiId(){
		return this.wikiId;
	}
	public void setWikiId(String wikiId){
		this.wikiId = wikiId;
		super.append( WikiAttachedFileItems.wikiId, wikiId);
	}
	
	public String getFilePath(){
		return this.filePath;
	}
	public void setFilePath(String filePath){
		this.filePath = filePath;
		super.append( WikiAttachedFileItems.filePath, filePath);
	}
	
	public String getAttachedFileSeqNo() {
		return attachedFileSeqNo;
	}
	public void setAttachedFileSeqNo(String attachedFileSeqNo) {
		this.attachedFileSeqNo = attachedFileSeqNo;
		super.append( WikiAttachedFileItems.attachedFileSeqNo, attachedFileSeqNo);
	}
	
	public StatusFlg getDelStsId() {
		return delStsId;
	}
	public void setDelStsId(StatusFlg delStsId) {
		this.delStsId = delStsId;
		super.append(WikiAttachedFileItems.delStsId, delStsId.parseBoolean() );
	}
}
