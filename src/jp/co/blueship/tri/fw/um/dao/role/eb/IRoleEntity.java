package jp.co.blueship.tri.fw.um.dao.role.eb;

import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the role entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 * 
 * @version V4.00.00
 * @author le.thixuan
 */
public interface IRoleEntity extends IEntityFooter {

	public String getRoleId();
	public void setRoleId(String roleId);

	public String getRoleName();
	public void setRoleName(String roleName);

	/**
	 * sort order
	 * @return sort order
	 */
	public Integer getSortOdr();
	/**
	 * sort order
	 * @param sortOdr sort order
	 */
	public void setSortOdr(Integer sortOdr);
}
