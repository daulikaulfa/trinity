package jp.co.blueship.tri.fw.um;

import jp.co.blueship.tri.fw.cmn.utils.function.TriFunction;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.ICtgEntity;
import jp.co.blueship.tri.fw.um.dao.dept.eb.IDeptEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpRoleLnkEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.IMstoneEntity;
import jp.co.blueship.tri.fw.um.dao.role.eb.IRoleEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaDto;

/**
 * {@link jp.co.blueship.tri.fw.cmn.utils.collections.FluentList}のサポートUtils
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class UmFluentFunctionUtils {

	public static final TriFunction<ICtgEntity, ItemLabelsBean> toItemLabelsFromCtgEntity = new TriFunction<ICtgEntity, ItemLabelsBean>() {
		@Override
		public ItemLabelsBean apply(ICtgEntity input) {
			return new ItemLabelsBean(input.getCtgNm(), input.getCtgId());
		}
	};

	public static final TriFunction<IMstoneEntity, ItemLabelsBean> toItemLabelsFromMstoneEntity = new TriFunction<IMstoneEntity, ItemLabelsBean>() {
		@Override
		public ItemLabelsBean apply(IMstoneEntity input) {
			return new ItemLabelsBean(input.getMstoneNm(), input.getMstoneId());
		}
	};

	public static final TriFunction<IUserEntity, ItemLabelsBean> toItemLabelsFromUserEntity = new TriFunction<IUserEntity, ItemLabelsBean>() {
		@Override
		public ItemLabelsBean apply(IUserEntity input) {
			return new ItemLabelsBean(input.getUserNm(), input.getUserId());
		}
	};

	public static final TriFunction<IGrpEntity, ItemLabelsBean> toItemLabelsFromGrpEntity = new TriFunction<IGrpEntity, ItemLabelsBean>() {
		@Override
		public ItemLabelsBean apply(IGrpEntity input) {
			return new ItemLabelsBean(input.getGrpNm(), input.getGrpId());
		}
	};
	
	public static final TriFunction<IGrpUserLnkEntity, String> toUserIdFromGrpUserLnkEntity = new TriFunction<IGrpUserLnkEntity, String>() {
		@Override
		public String apply(IGrpUserLnkEntity entity) {
			return entity.getUserId();
		}
	};
	
	public static final TriFunction<IGrpUserLnkEntity, String> toGrpIdFromGrpUserLnkEntity = new TriFunction<IGrpUserLnkEntity, String>() {
		@Override
		public String apply(IGrpUserLnkEntity entity) {
			return entity.getGrpId();
		}
	};
	
	public static final TriFunction<IGrpRoleLnkEntity, String> toRoleFromGrpRoleLnkEntity = new TriFunction<IGrpRoleLnkEntity, String>() {
		@Override
		public String apply(IGrpRoleLnkEntity entity) {
			return entity.getRoleId();
		}
	};
	
	public static final TriFunction<IRoleEntity, ItemLabelsBean> toItemLabelsFromRoleEntity = new TriFunction<IRoleEntity, ItemLabelsBean>() {
		@Override
		public ItemLabelsBean apply(IRoleEntity input) {
			return new ItemLabelsBean(input.getRoleName(), input.getRoleId());
		}
	};

	public static final TriFunction<IDeptEntity, ItemLabelsBean> toItemLabelsFromDeptEntity = new TriFunction<IDeptEntity, ItemLabelsBean>() {
		@Override
		public ItemLabelsBean apply(IDeptEntity input) {
			return new ItemLabelsBean(input.getDeptNm(), input.getDeptId());
		}
	};
	
	public static final TriFunction<IGrpEntity, String> toGroupNmFromGrpUserLnkEntity = new TriFunction<IGrpEntity, String>() {
		@Override
		public String apply(IGrpEntity entity) {
			return entity.getGrpNm();
		}
	};
	
	public static final TriFunction<IGrpEntity, String> toGroupIdFromGrpEntity = new TriFunction<IGrpEntity, String>() {
		@Override
		public String apply(IGrpEntity entity) {
			return entity.getGrpId();
		}
	};

	public static final TriFunction<ICtgEntity, ItemLabelsBean> toItemLabelsFromCtgEntityWithSubValue = new TriFunction<ICtgEntity, ItemLabelsBean>() {
		@Override
		public ItemLabelsBean apply(ICtgEntity input) {
			return new ItemLabelsBean(input.getCtgNm(), input.getCtgId(), input.getLotId());
		}
	};

	public static final TriFunction<IMstoneEntity, ItemLabelsBean> toItemLabelsFromMstoneEntityViewSubValue = new TriFunction<IMstoneEntity, ItemLabelsBean>() {
		@Override
		public ItemLabelsBean apply(IMstoneEntity input) {
			return new ItemLabelsBean(input.getMstoneNm(), input.getMstoneId(), input.getLotId());
		}
	};
	
	public static final TriFunction<IRaDto, ItemLabelsBean> toItemLabelsFromRaDto = new TriFunction<IRaDto, ItemLabelsBean>() {
		@Override
		public ItemLabelsBean apply(IRaDto input) {
			return new ItemLabelsBean(input.getRaEntity().getRaId(), input.getRaEntity().getRaId());
		}
	};
	
}
