package jp.co.blueship.tri.fw.um;

import java.sql.Timestamp;

import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.msg.MessageParameter;

public class UmRecentUpdatesUtils {

	/**
	 *
	 * 経過時間を文字列で返します。<br>
	 * 経過時間が1年以上の場合は年のみの表示になります。<br>
	 * 経過時間が1年未満1ヵ月以上の場合は月のみの表示になります。<br>
	 * 経過時間が1ヵ月未満1日以上の場合は日のみの表示になります。<br>
	 * 経過時間が1日未満1時間以上の場合は時間のみの表示になります。<br>
	 * 経過時間が1時間未満1分以上の場合は分のみの表示になります。<br>
	 * 経過時間が1分未満の場合は秒のみの表示になります。<br>
	 * <br>
	 * 1年を365日,1ヵ月を30日として計算を行います。<br>
	 * <br>
	 * 引数で渡す時間は必ずシステム時間にしてください。<br>
	 * <br>
	 *
	 * Get the elapsedTime of String.<br>
	 * <br>
	 * Dealing with 1 year as 365 days .
	 * Dealing with 1 month as 30 days.<br>
	 * <br>
	 * The argument is only the SystemTime.
	 *
	 * @param language
	 * @param time  only the SystemTime
	 * @return elapsedTime
	 */
	public static String stringOf(String language , Timestamp time){

		IContextAdapter contextAdapter = ContextAdapterFactory.getContextAdapter();

		String elapsedTimeStr = "";

		long elapsedTime = TriDateUtils.getSystemTimestamp().getTime() - time.getTime();
		if(elapsedTime < 0) elapsedTime = 0 ;

		long elapsedSec  = (elapsedTime / 1000) %60;
		elapsedTime = (elapsedTime / 1000) /60;

		long elapsedMin  = elapsedTime % 60;
		elapsedTime = elapsedTime / 60;

		long elapsedHour = elapsedTime % 24;
		elapsedTime = elapsedTime / 24;

		long elapsedDate = elapsedTime % 30;
		long elapsedMonth = ( elapsedTime % 365 ) / 30;
		long elapsedYear = elapsedTime / 365;

		if(elapsedYear > 0) {
			elapsedTimeStr = contextAdapter.getValue( MessageParameter.RecentUpdatesTime1.getKey(), new String[]{ String.valueOf(elapsedYear) }, language );
		} else if (elapsedMonth > 0) {
			elapsedTimeStr = contextAdapter.getValue( MessageParameter.RecentUpdatesTime2.getKey(), new String[]{ String.valueOf(elapsedMonth) }, language );
		} else if(elapsedDate > 0) {
			elapsedTimeStr = contextAdapter.getValue( MessageParameter.RecentUpdatesTime3.getKey(), new String[]{ String.valueOf(elapsedDate) }, language );
		} else if(elapsedHour > 0) {
			elapsedTimeStr = contextAdapter.getValue( MessageParameter.RecentUpdatesTime4.getKey(), new String[]{ String.valueOf(elapsedHour) }, language );
		} else if(elapsedMin > 0) {
			elapsedTimeStr = contextAdapter.getValue( MessageParameter.RecentUpdatesTime5.getKey(), new String[]{ String.valueOf(elapsedMin) }, language );
		} else {
			elapsedTimeStr = contextAdapter.getValue( MessageParameter.RecentUpdatesTime6.getKey(), new String[]{ String.valueOf(elapsedSec) }, language );
		}

		return elapsedTimeStr;
	}
}
