package jp.co.blueship.tri.fw.um.uix.cmn;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowGlobalSearchServiceBean;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowGlobalSearchServiceBean.SearchCondition;

@Controller
@RequestMapping("/project/globalSearch")
public class GlobalSearchController extends TriControllerSupport<FlowGlobalSearchServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.UmGlobalSearchService;
	}

	@Override
	protected FlowGlobalSearchServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowGlobalSearchServiceBean bean = new FlowGlobalSearchServiceBean();
		return bean;
	}

	@RequestMapping(value = {"", "/"})
	public String search(FlowGlobalSearchServiceBean bean, TriModel model) {
		String view = TriView.GlobalSearchResult.value();

		try {
			this.updateMapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel().addAttribute("result", bean);
		return view;
	}

	private void updateMapping(FlowGlobalSearchServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();
		SearchCondition searchCondition = bean.getParam().getSearchCondition();
		searchCondition.setKeyword(requestInfo.getParameter("keyword"));
	}

}
