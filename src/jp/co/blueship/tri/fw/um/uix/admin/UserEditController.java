package jp.co.blueship.tri.fw.um.uix.admin;

import java.io.IOException;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.sm.contants.PasswordComplexityLevel;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.constants.IconSelectionOption;
import jp.co.blueship.tri.fw.um.domainx.admin.beans.dto.UserEditInputBean;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowUserEditServiceBean;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowUserEditServiceBean.RequestOption;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 */
@Controller
@RequestMapping("/admin/user")
public class UserEditController extends TriControllerSupport<FlowUserEditServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.UmUserEditService;
	}

	@Override
	protected FlowUserEditServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowUserEditServiceBean bean = new FlowUserEditServiceBean();
		return bean;
	}

	@RequestMapping(value = "/edit")
	public String insert(FlowUserEditServiceBean bean, TriModel model) {
		String view = TriTemplateView.AdminSettings.value();

		try {

			this.mapping(bean, model);
			this.execute(getServiceId(), bean , model);
			if (bean.getResult().isCompleted()) {
				bean.getParam().setRequestType(RequestType.init);
				model.getRedirectAttributes().addFlashAttribute("result", bean);
				view = "redirect:/admin/user/redirect";
			}

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.UserEdit.value())
			.addAttribute("selectedMenu"  , "userList")
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}

	@RequestMapping("/edit/validate")
	public String validatePassword(
			FlowUserEditServiceBean bean,
			TriModel model) {
		String view = TriView.UserEdit.value();

		try {
			this.mapping(bean, model);
			this.execute( getServiceId(), bean, model );

			PasswordComplexityLevel level = bean.getDetailsView().getPassword().getPasswordComplexityLevel();

			if( level == PasswordComplexityLevel.none ){

				model.getModel().addAttribute( "levelClass", "level00" );

			} else if( level == PasswordComplexityLevel.Low ){

				model.getModel().addAttribute( "levelClass", "level01" );

			} else if( level == PasswordComplexityLevel.Middle ){

				model.getModel().addAttribute( "levelClass", "level02" );

			} else if( level == PasswordComplexityLevel.High ){

				model.getModel().addAttribute( "levelClass", "level03" );

			}

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		} finally {

		}
		model.getModel().addAttribute( "result", bean );
		return view;
	}
	
	@RequestMapping(value = "/edit/upload", method = RequestMethod.POST)
	public @ResponseBody String uploadIcon(@RequestParam("icon") MultipartFile customIconFile,
			FlowUserEditServiceBean bean, TriModel model) {
		String imagePath = "";

		try {
			this.uploadCustomIconMapping(customIconFile, bean, model);
			this.execute(getServiceId(), bean, model);
			imagePath = bean.getParam().getInputInfo().getIconPath();
		} catch (Exception e) {
			ExceptionUtils.printStackTrace(e);
			imagePath = "fail";
		}

		return imagePath;
	}

	private void mapping(FlowUserEditServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();

		if (RequestType.init.equals(bean.getParam().getRequestType())) {
			if( requestInfo.getParameter("inUserId") != null ){
				bean.getParam().getInputInfo().setUserId( requestInfo.getParameter("inUserId") );
			}

		} else if ( RequestType.submitChanges.equals(bean.getParam().getRequestType()) ) {
			bean.getParam()
				.setRequestOptions(new RequestOption[] {RequestOption.PasswordChanges, RequestOption.UserChanges})
				.getInputInfo()
					.setUserId(requestInfo.getParameter("inUserId"))
					.setUserNm( requestInfo.getParameter("inUserName"))
					.setMailAddress( requestInfo.getParameter("mailAddress"))
					.setDeptId( requestInfo.getParameter("deptId"))
					.setIconPath( requestInfo.getParameter("iconPath"))
					.setIconOption(IconSelectionOption.value(requestInfo.getParameter("iconOption")))
					.setReceiveMail(null != requestInfo.getParameter("isReceiveMail"))
					.setPassword( requestInfo.getParameter("password"))
					.setConfirmPassword( requestInfo.getParameter("confirmPassword"))
					.setPasswordExpiration( requestInfo.getParameter("passwordExpiration"))
					.setGroupIds(requestInfo.getParameterValues("groupIds"))
					.setLanguage( requestInfo.getParameter("userLanguage"))
					.setTimeZone( requestInfo.getParameter("timeZone"));

		} else if ( RequestType.onChange.equals(bean.getParam().getRequestType())) {
			bean.getParam()
			.setRequestOptions(new RequestOption[] {RequestOption.PasswordChanges, RequestOption.UserChanges})
			.getInputInfo()
				.setPassword( requestInfo.getParameter("password") );
		}

	}
	
	private void uploadCustomIconMapping(MultipartFile customIconFile, FlowUserEditServiceBean bean,
			TriModel model) throws IOException {
		UserEditInputBean userInfo = bean.getParam().getInputInfo();
		FlowUserEditServiceBean.RequestParam param = bean.getParam();

		param.setRequestOptions(RequestOption.UserChanges);
		param.setRequestType(RequestType.onChange);
		byte[] iconBytes = new byte[] {};
		if (!customIconFile.isEmpty()) {
			iconBytes = customIconFile.getBytes();
			userInfo.setIconOption(IconSelectionOption.CustomImage);
			userInfo.setIconInputStreamBytes(iconBytes);
			userInfo.setIconFileNm(customIconFile.getOriginalFilename());
		}
	}
}
