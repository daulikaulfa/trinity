package jp.co.blueship.tri.fw.um.uix.project;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.constants.GenderType;
import jp.co.blueship.tri.fw.um.domainx.project.dto.FlowProfileEditServiceBean;
import jp.co.blueship.tri.fw.um.domainx.project.dto.FlowProfileEditServiceBean.ProfileEditInputBean;

/**
 * @version V4.00.00
 * @author thang.vu
 *
 */
@Controller
@RequestMapping(value = { "/project/profile/edit" })
public class ProfileEditController extends TriControllerSupport<FlowProfileEditServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.UmProfileEditService;
	}

	@Override
	protected FlowProfileEditServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowProfileEditServiceBean bean = new FlowProfileEditServiceBean();
		return bean;
	}

	@RequestMapping(value = { "", "/" })
	public String index(FlowProfileEditServiceBean bean, TriModel model) {
		String view = TriView.Profile.value();

		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);
			if (bean.getResult().isCompleted()) {
				model.getRedirectAttributes().addFlashAttribute("result", bean);
				return "redirect:/project/profile/details/redirect";
			}

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel().addAttribute("result", bean);
		
		setPrev(model);
		return view;
	}

	private void mapping(FlowProfileEditServiceBean bean, TriModel model) {
		FlowProfileEditServiceBean.RequestParam param = bean.getParam();
		IRequestInfo requestInfo = model.getRequestInfo();
		if ( requestInfo.getParameter("userId") != null ){
			param.setSelectedUserId(requestInfo.getParameter("userId"));
		}

		ProfileEditInputBean info = param.getInputInfo();

		if ( requestInfo.getParameter("biography") != null ) {
			info.setBiography(requestInfo.getParameter("biography"));
		}
		if ( requestInfo.getParameter("gender") != null ) {
			info.setGenderType(GenderType.value(requestInfo.getParameter("gender")));
		}
		if ( requestInfo.getParameterValues("urls") != null ) {
			info.setUrls(requestInfo.getParameterValues("urls"));
		}
		if ( requestInfo.getParameter("userLocation") != null ) {
			info.setUserLocation(requestInfo.getParameter("userLocation"));
		}

		if (RequestType.init.equals(param.getRequestType())) {
		}

		if (RequestType.onChange.equals(param.getRequestType())) {
		}
	}
}
