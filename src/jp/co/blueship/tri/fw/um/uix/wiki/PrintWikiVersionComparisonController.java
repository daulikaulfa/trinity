package jp.co.blueship.tri.fw.um.uix.wiki;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.domainx.wiki.dto.FlowWikiVersionComparisonServiceBean;

/**
 *
 * @version V4.00.00
 * @author thang.vu
 */
@Controller
@RequestMapping("/lot/wiki/diff/print")
public class PrintWikiVersionComparisonController extends TriControllerSupport<FlowWikiVersionComparisonServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.UmPrintWikiVersionComparisonService;
	}

	@Override
	protected FlowWikiVersionComparisonServiceBean getServiceBean(ISessionInfo sesInfo) {
		return new FlowWikiVersionComparisonServiceBean();
	}

	@RequestMapping
	public String diff(FlowWikiVersionComparisonServiceBean bean, TriModel model) {
		String view = TriTemplateView.PrintTemplate.value();

		try {
			this.mapping(bean, model);
			this.execute(this.getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.PrintWikiPageDifferences.value())
			.addAttribute("selectedMenu", "wikiMenu")
			.addAttribute("result", bean)
		;

		setPrev(model);

		return view;
	}

	private void mapping(FlowWikiVersionComparisonServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();
		bean.getParam().setSelectedWikiId(requestInfo.getParameter("wikiId"));
		bean.getParam().setSrcVersion(Integer.parseInt(requestInfo.getParameter("srcVerNo")));
		bean.getParam().setDestVersion(Integer.parseInt(requestInfo.getParameter("destVerNo")));
	}
}
