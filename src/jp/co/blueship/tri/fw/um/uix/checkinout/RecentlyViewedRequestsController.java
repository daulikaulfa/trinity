package jp.co.blueship.tri.fw.um.uix.checkinout;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckInOutRequestListServiceBean;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckInOutRequestListServiceBean.SearchCondition;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriView;

/**
 * @version V4.00.00
 * @author Chung
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 */
@Controller
public class RecentlyViewedRequestsController extends TriControllerSupport<FlowCheckInOutRequestListServiceBean> {
	private static int LINES_PER_PAGE_FOR_RECENTLY_VIEW_REQUEST = 8;

	@Override
	public ServiceId getServiceId() {
		return ServiceId.UmRecentlyViewedRequestsService;
	}

	@Override
	protected FlowCheckInOutRequestListServiceBean getServiceBean(ISessionInfo sesInfo) {
		return new FlowCheckInOutRequestListServiceBean();
	}

	@RequestMapping("/project/recently/requests")
	public String requestList(FlowCheckInOutRequestListServiceBean bean, TriModel model) {

		this.mapping(bean, model);
		this.execute( this.getServiceId(), bean, model);

		model.getModel().addAttribute("result", bean);
		model.getModel().addAttribute("pagination", bean.getPage());

		return TriView.GlobalHeaderViewRequest.value() + "::GlobalHeader_RecentlyViewedRequests";
	}

	private void mapping(FlowCheckInOutRequestListServiceBean bean, TriModel model){
		IRequestInfo requestInfo = model.getRequestInfo();
		SearchCondition searchCondition = bean.getParam().getSearchCondition();

		bean.getParam().setLinesPerPage(LINES_PER_PAGE_FOR_RECENTLY_VIEW_REQUEST);
		bean.getParam().getOrderBy().setAccsTimestamp	( TriSortOrder.Desc )
									.setPjtId			( null )
									.setAreqId			( null )
									.setStatus			( null )
									;
		String selectPage = requestInfo.getParameter("selectedPageNo");

		if( TriStringUtils.isNotEmpty(selectPage) ){
			searchCondition.setSelectedPageNo( Integer.parseInt(selectPage) );

		}else{
			searchCondition.setSelectedPageNo(1);
		}

		searchCondition.setKeyword( requestInfo.getParameter("requestSearch") );
	}

}