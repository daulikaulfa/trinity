package jp.co.blueship.tri.fw.um.uix.wiki;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.domainx.wiki.dto.FlowWikiVersionDetailsServiceBean;
import jp.co.blueship.tri.fw.um.domainx.wiki.dto.FlowWikiVersionDetailsServiceBean.RequestOption;

@Controller
@RequestMapping("/lot/wiki")
public class WikiVersionDetailsController extends TriControllerSupport<FlowWikiVersionDetailsServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.UmWikiVersionDetailsService;
	}

	@Override
	protected FlowWikiVersionDetailsServiceBean getServiceBean(ISessionInfo sesInfo) {
		return new FlowWikiVersionDetailsServiceBean();
	}

	@RequestMapping("/version")
	public String verDetails(FlowWikiVersionDetailsServiceBean bean, TriModel model){
		String view = TriTemplateView.MainTemplate.value();

		try {
			this.mapping(bean, model);
			this.execute(this.getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.WikiVersionDetails.value())
			.addAttribute("selectedMenu", "wikiMenu")
			.addAttribute("result", bean)
		;

		setPrev(model);

		return view;
	}


	private void mapping(FlowWikiVersionDetailsServiceBean bean, TriModel model){
		IRequestInfo requestInfo = model.getRequestInfo();

		if( RequestType.init.equals(bean.getParam().getRequestType()) ){
			if ( requestInfo.getParameter("wikiVerNo") != null ){
				bean.getParam().setSelectedWikiId( requestInfo.getParameter("wikiId") );
			}
			if ( requestInfo.getParameter("wikiVerNo") != null ){
				bean.getParam().setWikiVerNo( Integer.parseInt(requestInfo.getParameter("wikiVerNo")));
			}

		}

		if( RequestType.onChange.equals(bean.getParam().getRequestType()) ){
			bean.getParam().setRequestOption( RequestOption.value( requestInfo.getParameter("requestOption") ) );
			bean.getParam().setSearchPageNm( requestInfo.getParameter("searchPageNm") );
		}
	}
}
