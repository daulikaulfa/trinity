package jp.co.blueship.tri.fw.um.uix.admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.domainx.admin.beans.dto.GroupEditInputBean;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowGroupEditServiceBean;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowGroupEditServiceBean.GroupRequestOption;

@Controller
@RequestMapping("/admin/group/edit")
public class GroupEditController extends TriControllerSupport<FlowGroupEditServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.UmGroupEditService;
	}

	@Override
	protected FlowGroupEditServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowGroupEditServiceBean bean = new FlowGroupEditServiceBean();
		return bean;
	}

	@RequestMapping
	public String update(FlowGroupEditServiceBean bean, TriModel model) {
		String view = TriTemplateView.AdminSettings.value();

		try {
			this.updateMapping(bean, model);
			this.execute(getServiceId(), bean, model);
			if (RequestType.submitChanges.equals(bean.getParam().getRequestType())) {
				if(bean.getResult().isCompleted()) {
					model.getSessionInfo().setAttribute( "groupId", bean.getParam().getSelectedGroupId());
					model.getRedirectAttributes().addFlashAttribute("result", bean);
					view = "redirect:/admin/group/redirect";
				}
			}

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		Gson gson = new Gson();


		model.getModel()
			.addAttribute("userViews", gson.toJson(bean.getParam().getInputInfo().getUserViews()))
			.addAttribute("groupId", bean.getParam().getSelectedGroupId())
			.addAttribute("view", TriView.GroupEdit.value())
			.addAttribute("selectedMenu", "groupList")
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}

	@RequestMapping("/excludeuser")
	public String excludeUserId( FlowGroupEditServiceBean bean, TriModel model) {
		String view = TriView.Messages.value() + "::message";
		try {
			this.excludeUserMapping(bean, model );
			this.execute(getServiceId(), bean, model);
		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}
		model.getModel().addAttribute("result", bean);
		return view;
	}

	private void excludeUserMapping(FlowGroupEditServiceBean bean, TriModel model ) {

		IRequestInfo requestInfo = model.getRequestInfo();
		String excludeUserId = requestInfo.getParameter("excludeUserId");
		String groupId = requestInfo.getParameter("groupId");

		bean.getParam().setRequestType(RequestType.onChange);
		bean.getParam().setRequestOption(GroupRequestOption.ExcludeUser);
		bean.getParam().setSelectedGroupId(groupId);
		bean.getParam().getInputInfo().setExcludeUserId(excludeUserId);
	}

	private void updateMapping(FlowGroupEditServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();

		GroupEditInputBean info = bean.getParam().getInputInfo();
		FlowGroupEditServiceBean.RequestParam param = bean.getParam();


		if(requestInfo.getParameter("groupId") != null) {
			param.setSelectedGroupId(requestInfo.getParameter("groupId"));
		}

		if (RequestType.submitChanges.equals(bean.getParam().getRequestType())) {
			info.setGroupNm(requestInfo.getParameter("groupNm"))
				.setRoleIds(requestInfo.getParameterValues("roleIds"))
				.setUserIds(requestInfo.getParameterValues("userIds"));
		}

	}

}
