package jp.co.blueship.tri.fw.um.uix.wiki;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.domainx.wiki.dto.FlowWikiDetailsServiceBean;
import jp.co.blueship.tri.fw.um.domainx.wiki.dto.FlowWikiDetailsServiceBean.RequestOption;

@Controller
@RequestMapping("/lot/wiki")
public class WikiDetailsController extends TriControllerSupport<FlowWikiDetailsServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.UmWikiDetailsService;
	}

	@Override
	protected FlowWikiDetailsServiceBean getServiceBean(ISessionInfo sesInfo) {
		return new FlowWikiDetailsServiceBean();
	}

	@RequestMapping(value = "/details/redirect")
	public String redirect( @ModelAttribute("wikiId") String wikiId,
							@ModelAttribute("result") DomainServiceBean prevBean,
							FlowWikiDetailsServiceBean bean,
							TriModel model ){
		bean.getParam().setSelectedWikiId( wikiId );
		bean.getMessageInfo().addFlashMessages( prevBean.getMessageInfo().getFlashMessages() );

		return detils( bean , model.setRedirect(true) );
	}

	@RequestMapping(value = {"","/"})
	public String detils(FlowWikiDetailsServiceBean bean, TriModel model){

		String view = TriTemplateView.MainTemplate.value();

		try {
			bean.getMessageInfo().isEmptyFlashMessages();
			bean.getMessageInfo().getFlashMessages();

			this.mapping(bean, model);
			this.execute(this.getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.WikiDetails.value())
			.addAttribute("selectedMenu", "wikiMenu")
			.addAttribute("result", bean)
		;

		return view;
	}

	private void mapping(FlowWikiDetailsServiceBean bean, TriModel model){
		IRequestInfo requestInfo = model.getRequestInfo();
		ISessionInfo sesInfo = model.getSessionInfo();

		if( TriStringUtils.isEmpty( bean.getParam().getSelectedWikiId() ) ) {
			bean.getParam().setSelectedWikiId( requestInfo.getParameter("wikiId") );
		}

		if( RequestType.init.equals(bean.getParam().getRequestType()) ){
			bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
		}

		if( RequestType.onChange.equals(bean.getParam().getRequestType()) ){
			bean.getParam().setRequestOption( RequestOption.value( requestInfo.getParameter("requestOption") ) );
			bean.getParam().setSearchPageNm( requestInfo.getParameter("searchPageNm") );
		}

	}
}
