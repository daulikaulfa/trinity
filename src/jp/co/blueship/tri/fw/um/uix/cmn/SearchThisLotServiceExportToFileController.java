package jp.co.blueship.tri.fw.um.uix.cmn;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.dcm.DcmExportToFileUtils;
import jp.co.blueship.tri.fw.cmn.io.StreamUtils;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.io.constants.Charset;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.constants.DataAttribute;
import jp.co.blueship.tri.fw.um.constants.ExportToFileSubmitOption;
import jp.co.blueship.tri.fw.um.domainx.cmn.beans.dto.ExportToFileBean;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowSearchThisLotServiceExportToFileBean;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowSearchThisLotServiceExportToFileBean.RequestParam;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowSearchThisSiteServiceBean.SearchCondition;


@Controller
@RequestMapping("/lot/search/export")
public class SearchThisLotServiceExportToFileController
		extends TriControllerSupport<FlowSearchThisLotServiceExportToFileBean> {
	@Override
	public ServiceId getServiceId() {
		return ServiceId.UmSearchThisLotServiceExportToFile;
	}

	@Override
	protected FlowSearchThisLotServiceExportToFileBean getServiceBean(ISessionInfo sesInfo) {
		FlowSearchThisLotServiceExportToFileBean bean = new FlowSearchThisLotServiceExportToFileBean();
		return bean;
	}

	@RequestMapping
	public String export(FlowSearchThisLotServiceExportToFileBean bean, TriModel model) {

		String view = TriView.LotSearch.value() + "::resultTable";

		try {
			this.insertMapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		} finally {
			this.removeAttribute(model, bean.getHeader().getWindowsId());
		}

		model.getModel().addAttribute("result", bean);
		return view;
	}

	@RequestMapping("/file")
	public void exportFile(FlowSearchThisLotServiceExportToFileBean bean, TriModel model,
			HttpServletResponse response) {
		try {
			this.insertMapping(bean, model);
			this.execute(getServiceId(), bean, model);

			String newFileName =
					DcmExportToFileUtils.getDownloadFileName(
							getServiceId(),
							bean.getParam().getSubmitOption().extention(),
							bean.getLanguage(),
							bean.getTimeZone());

			StreamUtils.download(
					response,
					bean.getFileResponse().getFile(),
					newFileName );

			TriFileUtils.delete(bean.getFileResponse().getFile());

		} catch (Exception e) {
			bean.getSearchSiteViews().clear();
			ExceptionUtils.printStackTrace(e);
		} finally {
			this.removeAttribute(model, bean.getHeader().getWindowsId());
		}
		return;
	}

	private void insertMapping(FlowSearchThisLotServiceExportToFileBean bean, TriModel model)
			throws UnsupportedEncodingException {
		RequestParam param = bean.getParam();
		SearchCondition searchCondition = param.getSearchCondition();
		IRequestInfo requestInfo = model.getRequestInfo();

		String lotId = this.getSessionSelectedLot(model.getSessionInfo(), bean);
		searchCondition.setLotIds(lotId);

		if ((RequestType.onChange).equals(param.getRequestType())) {
			String dataAttribute = requestInfo.getParameter("dataProcessing");

			if (TriStringUtils.isNotEmpty(dataAttribute)) {
				searchCondition.setTableAttribute(DataAttribute.value(dataAttribute));
			} else {
				searchCondition.setTableAttribute(DataAttribute.none);
			}

			searchCondition
				.setMstoneStartDate	( requestInfo.getParameter("dateStart") )
				.setMstoneDueDate	( requestInfo.getParameter("dateEnd") )
				.setAssigneeIds		( requestInfo.getParameterValues("contact") )
				.setGroupIds		( requestInfo.getParameterValues("group") )
				.setStatusIds		( requestInfo.getParameterValues("status") )
				.setCtgIds			( requestInfo.getParameterValues("category") )
				.setMstoneIds		( requestInfo.getParameterValues("milestone") )
				.setKeyword			( requestInfo.getParameter("keyword") )
				.setAttachedFile	( "true".equals(requestInfo.getParameter("attachment")) )
			;

		} else if ((RequestType.submitChanges).equals(param.getRequestType())) {
			String htmlTable = URLDecoder.decode(model.getRequestInfo().getParameter("table"), Charset.UTF_8.value());
			ExportToFileBean fileBean = createExportToFileBean(htmlTable);
			param.setExportBean(fileBean);
			ExportToFileSubmitOption option = ExportToFileSubmitOption.valueOf(requestInfo.getParameter("submitOption"));
			param.setSubmitOption(option);
		}
	}

	private ExportToFileBean createExportToFileBean(String htmlTable) {
		ExportToFileBean fileBean = new ExportToFileBean();
		String parseHtmlTable = "<html><body><table>" + htmlTable + "</table></body></html>";

		Document doc = Jsoup.parse(parseHtmlTable);
		Iterator<Element> iteHeader = doc.select("th").iterator();

		while (iteHeader.hasNext()) {
			fileBean.getTitleHeader().add(iteHeader.next().text());
		}
		int columnCount = fileBean.getTitleHeader().size();

		Iterator<Element> iteBody = doc.select("td").iterator();
		List<String> row = new ArrayList<String>();
		while (iteBody.hasNext()) {
			if (row.size() == columnCount) {
				fileBean.addRow(row);
				row = new ArrayList<String>();
			}
			Element element = iteBody.next();

			if ("attachment".equals(element.attr("data-column"))) {
				if (element.children() != null && element.children().get(0).hasClass("fa-paperclip")) {
					row.add("yes");
				} else {
					row.add("");
				}
			} else {
				row.add(element.text());
			}
		}
		fileBean.addRow(row);
		return fileBean;
	}
}
