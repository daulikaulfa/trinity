package jp.co.blueship.tri.fw.um.uix.admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowRoleCreationServiceBean;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 */
@Controller
@RequestMapping("/admin/role")
public class RoleCreationController  extends TriControllerSupport<FlowRoleCreationServiceBean> {

	public ServiceId getServiceId() {
		return ServiceId.UmRoleCreationService;
	}

	@Override
	protected FlowRoleCreationServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowRoleCreationServiceBean bean = new FlowRoleCreationServiceBean();
		return bean;
	}

	@RequestMapping(value = "/create")
	public String insert(
			FlowRoleCreationServiceBean bean,
            TriModel model) {
		String view = TriTemplateView.AdminSettings.value();

		try {

			this.insertMapping(bean, model);
			this.execute(getServiceId(), bean , model);
			if (bean.getResult().isCompleted()) {
				bean.getParam().setRequestType(RequestType.init);
				model.getRedirectAttributes().addFlashAttribute("result",bean);
				view = "redirect:/admin/role/redirect";
			}

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.RoleCreation.value())
			.addAttribute("selectedMenu"  , "roleList")
			.addAttribute("result", bean)
			.addAttribute("actionSelection", bean.getActionSelectionView().toJsonFromActionSelection())
		;

		setPrev(model);
		return view;
	}

	private void insertMapping(FlowRoleCreationServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();

		if ( RequestType.submitChanges.equals(bean.getParam().getRequestType()) ) {
			bean.getParam().getInputInfo()
				.setRoleNm(requestInfo.getParameter("roleSubject"))
				.setActionIds(requestInfo.getParameterValues("actionIds"))
				.setGroupIds(requestInfo.getParameterValues("groupIds"));
		}
	}

}
