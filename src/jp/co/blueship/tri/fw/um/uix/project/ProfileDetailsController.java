package jp.co.blueship.tri.fw.um.uix.project;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.domainx.gantt.beans.dto.GanttViewBean.Grouping;
import jp.co.blueship.tri.fw.um.domainx.gantt.beans.dto.GanttViewBean.Status;
import jp.co.blueship.tri.fw.um.domainx.project.beans.dto.ProfileDetailsGanttChartBean;
import jp.co.blueship.tri.fw.um.domainx.project.beans.dto.ProfileDetailsGanttChartBean.GanttChartRequestOption;
import jp.co.blueship.tri.fw.um.domainx.project.beans.dto.ProfileDetailsGanttChartBean.Span;
import jp.co.blueship.tri.fw.um.domainx.project.beans.dto.ProfileDetailsLatestActivityBean.LatestActivityRequestOption;
import jp.co.blueship.tri.fw.um.domainx.project.dto.FlowProfileDetailsServiceBean;
import jp.co.blueship.tri.fw.um.domainx.project.dto.FlowProfileDetailsServiceBean.RequestOption;
import jp.co.blueship.tri.fw.um.domainx.project.dto.FlowProfileEditServiceBean;

import java.util.HashMap;
import java.util.Map;

/**
 * @version V4.00.00
 * @author thang.vu
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 * @version V4.03.00
 * @author Cuong Nguyen
 *
 */

@Controller
@RequestMapping(value = { "/project/profile/details" })
public class ProfileDetailsController extends TriControllerSupport<FlowProfileDetailsServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.UmProfileDetailsService;
	}

	@Override
	protected FlowProfileDetailsServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowProfileDetailsServiceBean bean = new FlowProfileDetailsServiceBean();
		return bean;
	}

	@RequestMapping(value = "/redirect")
	public String redirect(FlowProfileDetailsServiceBean bean, TriModel model) {
		bean.getParam().setRequestType(RequestType.init);
		return this.index(bean, model.setRedirect(true));
	}

	@RequestMapping(value = { "", "/" })
	public String index(FlowProfileDetailsServiceBean bean, TriModel model) {
		String view = TriTemplateView.ProjectTemplate.value();

		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);
		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("result", bean)
			.addAttribute("defaultSelectTab", bean.getParam().getRequestOption().value())
			.addAttribute("view", TriView.Profile.value())
		;

		return view;
	}

	private void mapping(FlowProfileDetailsServiceBean bean, TriModel model) {
		FlowProfileDetailsServiceBean.RequestParam param = bean.getParam();
		IRequestInfo requestInfo = model.getRequestInfo();

		if( TriStringUtils.isNotEmpty( requestInfo.getParameter("requestOption") ) ) {
			bean.getParam().setRequestOption(RequestOption.value(requestInfo.getParameter("requestOption")));
		}

		if ( requestInfo.getParameter("memberId") != null ) {
			param.setSelectedUserId(requestInfo.getParameter("memberId"));

		} else if ( model.isRedirect() ) {
			if ( model.getModel().asMap().get("result") instanceof FlowProfileEditServiceBean) {
				FlowProfileEditServiceBean profileEdit = (FlowProfileEditServiceBean) model.getModel().asMap().get("result");
				param.setSelectedUserId( profileEdit.getUserId() );
			}
		}

		if (RequestType.init.equals(param.getRequestType())) {
			ProfileDetailsGanttChartBean ganttChart = bean.getGanttChartView();

			ganttChart.setSearchCondition(ganttChart.new SearchCondition());
		}

		if (RequestType.onChange.equals(param.getRequestType())) {
			if( RequestOption.latestActivity.equals( bean.getParam().getRequestOption() ) )
				bean.getLatestActivityView().setRequestOption( LatestActivityRequestOption.value( requestInfo.getParameter("latestActivityRequestOption") ) );

			if( RequestOption.ganttChart.equals( bean.getParam().getRequestOption() ) ){
				ProfileDetailsGanttChartBean ganttChart = bean.getGanttChartView();

				int ganttSts_span = 1;
				if ( null != requestInfo.getParameter("ganttSts_span")) {
					ganttSts_span = Integer.valueOf( requestInfo.getParameter("ganttSts_span"));
				}

				ganttChart.setRequestOption( GanttChartRequestOption.value( requestInfo.getParameter("ganttChartRequestOption") ) );
				ganttChart.getSearchCondition().setStartDate	( requestInfo.getParameter("ganttSts_startDate"))
											   .setSpan			( Span.value( ganttSts_span ))
											   .setGrouping		( Grouping.value( requestInfo.getParameter("ganttSts_grouping")) )
											   .setStatus		( Status.value( requestInfo.getParameter("ganttSts_status")))
											   ;
			}
		}
	}
}
