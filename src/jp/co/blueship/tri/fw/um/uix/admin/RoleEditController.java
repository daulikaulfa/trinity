package jp.co.blueship.tri.fw.um.uix.admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowRoleEditServiceBean;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 */
@Controller
@RequestMapping("/admin/role")
public class RoleEditController extends TriControllerSupport<FlowRoleEditServiceBean> {

	public ServiceId getServiceId() {
		return ServiceId.UmRoleEditService;
	}

	@Override
	protected FlowRoleEditServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowRoleEditServiceBean bean = new FlowRoleEditServiceBean();
		return bean;
	}

	@RequestMapping(value = "/edit")
	public String insert(
			FlowRoleEditServiceBean bean,
            TriModel model) {
		String view = TriTemplateView.AdminSettings.value();

		try {

			this.editMapping(bean, model);
			this.execute(getServiceId(), bean, model);
			if (bean.getResult().isCompleted()) {
				bean.getParam().setRequestType(RequestType.init);
				model.getRedirectAttributes().addFlashAttribute("result", bean);
				model.getSessionInfo().setAttribute( "roleId", bean.getParam().getSelectedRoleId());
				view = "redirect:/admin/role/redirect";
			}

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.RoleEdit.value())
			.addAttribute("selectedMenu"  , "roleList")
			.addAttribute("actionSelection", bean.getActionSelectionView().toJsonFromActionSelection())
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}

	private void editMapping(FlowRoleEditServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();

		if (RequestType.init.equals(bean.getParam().getRequestType())) {

			if(requestInfo.getParameter("roleId") != null) {
				bean.getParam().setSelectedRoleId(requestInfo.getParameter("roleId"));
			}

		} else if ( RequestType.submitChanges.equals(bean.getParam().getRequestType()) ) {

			bean.getParam().setSelectedRoleId( requestInfo.getParameter("roleId"))
				.getInputInfo()
				.setRoleNm( requestInfo.getParameter("roleSubject") )
				.setActionIds( requestInfo.getParameterValues("actionIds") )
				.setGroupIds( requestInfo.getParameterValues("groupIds") );

		}

	}
}
