package jp.co.blueship.tri.fw.um.uix.wiki;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.domainx.wiki.dto.FlowWikiCreationServiceBean;

@Controller
@RequestMapping("/lot/wiki")
public class WikiCreationController extends TriControllerSupport<FlowWikiCreationServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.UmWikiCreationService;
	}

	@Override
	protected FlowWikiCreationServiceBean getServiceBean(ISessionInfo sesInfo) {
		return new FlowWikiCreationServiceBean();
	}

	@RequestMapping("/create")
	public String create(FlowWikiCreationServiceBean bean, TriModel model){
		String view = TriTemplateView.MainTemplate.value();

		try {
			this.mapping(bean, model);
			this.execute(this.getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		if( bean.getResult().isCompleted() ){
			model.getRedirectAttributes().addFlashAttribute("wikiId", bean.getResult().getWikiId());
			model.getRedirectAttributes().addFlashAttribute( "result", bean );
			bean.getParam().setRequestType(RequestType.init);

			view = "redirect:/lot/wiki/details/redirect";
		}

		model.getModel()
			.addAttribute("view", TriView.WikiPageCreation.value())
			.addAttribute("selectedMenu", "wikiMenu")
			.addAttribute("result", bean)
		;

		setPrev(model);

		return view;
	}


	private void mapping(FlowWikiCreationServiceBean bean, TriModel model){
		IRequestInfo requestInfo = model.getRequestInfo();
		ISessionInfo sesInfo = model.getSessionInfo();

		if( RequestType.init.equals(bean.getParam().getRequestType()) ){
			bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
			if ( requestInfo.getParameter("copyWikiId") != null )
			bean.getParam().setCopyWikiId( requestInfo.getParameter("copyWikiId") );
		}

		if( RequestType.submitChanges.equals(bean.getParam().getRequestType()) ){
			bean.getParam().getInputInfo().setPageNm	( requestInfo.getParameter("pageNm") )
										  .setContent	( requestInfo.getParameter("content") )
										  ;
		}
	}
}
