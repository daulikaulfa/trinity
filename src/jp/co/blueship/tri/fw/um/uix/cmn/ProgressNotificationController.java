package jp.co.blueship.tri.fw.um.uix.cmn;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.constants.TriSessionAttributes;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowProgressNotificationServiceBean;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowProgressNotificationServiceBean.RequestOption;

/**
 * @version V4.00.00
 * @author Yuhei Suzuki
 */
@Controller
@RequestMapping("/notification")
public class ProgressNotificationController extends TriControllerSupport<FlowProgressNotificationServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.UmProgressNotificationService;
	}

	@Override
	protected FlowProgressNotificationServiceBean getServiceBean(ISessionInfo sesInfo) {
		return new FlowProgressNotificationServiceBean();
	}

	@RequestMapping(value = { "", "/" })
	public String progressNotification(FlowProgressNotificationServiceBean bean, TriModel model) {
		String view = TriView.ProgressNotification.value();

		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel().addAttribute("result", bean);

		return view;
	}


	@RequestMapping(value = { "/count" })
	@ResponseBody
	public String progressNotificationCount(FlowProgressNotificationServiceBean bean, TriModel model) {

		try {
			bean.getParam().setRequestOption(RequestOption.value(model.getRequestInfo().getParameter("requestOption")));
			this.execute(getServiceId(), bean, model);
			return String.valueOf(bean.getCountOfUnread());
		} catch (Exception e) {
			ExceptionUtils.printStackTrace(e);
		}

		return "";
	}


	private void mapping(FlowProgressNotificationServiceBean bean, TriModel model) {

		// onChange
		if (RequestType.onChange.equals(bean.getParam().getRequestType())) {
			bean.getParam().setRequestOption(RequestOption.value(model.getRequestInfo().getParameter("requestOption")));
		}

	}


	@RequestMapping("/link")
	public String link(FlowProgressNotificationServiceBean bean, TriModel model) {

		String lotId = model.getRequestInfo().getParameter("lotId");
		String serviceId = model.getRequestInfo().getParameter("serviceId");

		PreConditions.assertOf(lotId != null, "lotId is not specified");
		PreConditions.assertOf(serviceId != null, "serviceId is not specified");

		if (lotId != null) {
			model.getSessionInfo().setAttribute(this.getSessionKey(bean.getHeader().getWindowsId(), TriSessionAttributes.SelectedLotId.value()), lotId);			
		}

		bean.getParam().setRequestType(RequestType.init);
		model.getRedirectAttributes().addFlashAttribute("result", bean);

		if (serviceId.equals(ServiceId.UmDashBoardService.value())) {
			return "redirect:/dashboard";
		} else if (serviceId.equals(ServiceId.BmBuildPackageCreationProcessingStatusService.value())) {
			return "redirect:/build/progress";
		} else if (serviceId.equals(ServiceId.BmBuildPackageListService.value())) {
			return "redirect:/build";
		} else if (serviceId.equals(ServiceId.RmReleasePackageCreationProcessingStatusService.value())) {
			return "redirect:/release/progress";
		} else if (serviceId.equals(ServiceId.AmMergeService.value())) {
			return "redirect:/merge";
		}

		return "redirect:/dashboard";
	}


}
