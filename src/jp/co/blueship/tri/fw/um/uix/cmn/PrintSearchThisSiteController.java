package jp.co.blueship.tri.fw.um.uix.cmn;

import java.io.UnsupportedEncodingException;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.constants.DataAttribute;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowSearchThisSiteServiceBean;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowSearchThisSiteServiceBean.RequestParam;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowSearchThisSiteServiceBean.SearchCondition;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowSearchThisSiteServiceBean.SearchServiceType;

@Controller
@RequestMapping("/project/search/print")
public class PrintSearchThisSiteController
		extends TriControllerSupport<FlowSearchThisSiteServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.UmPrintSearchThisSiteService;
	}

	@Override
	protected FlowSearchThisSiteServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowSearchThisSiteServiceBean bean = new FlowSearchThisSiteServiceBean();
		return bean;
	}

	@RequestMapping
	public String export(FlowSearchThisSiteServiceBean bean, TriModel model) {
		String view = TriTemplateView.PrintTemplate.value();

		try {
			this.insertMapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.PrintSearchThisProject.value())
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}

	private void insertMapping(FlowSearchThisSiteServiceBean bean, TriModel model)
			throws UnsupportedEncodingException {
		RequestParam param = bean.getParam();
		SearchCondition searchCondition = param.getSearchCondition();
		IRequestInfo requestInfo = model.getRequestInfo();

		if ((RequestType.onChange).equals(param.getRequestType())) {
			String dataAttribute = requestInfo.getParameter("dataProcessing");
			if (TriStringUtils.isNotEmpty(dataAttribute)) {
				searchCondition.setTableAttribute(DataAttribute.value(dataAttribute));
			} else {
				searchCondition.setTableAttribute(DataAttribute.none);
			}

			searchCondition
				.setLotIds			( requestInfo.getParameterValues("filterLot") )
				.setMstoneStartDate	( requestInfo.getParameter("dateStart") )
				.setMstoneDueDate	( requestInfo.getParameter("dateEnd") )
				.setAssigneeIds		( requestInfo.getParameterValues("contact") )
				.setGroupIds		( requestInfo.getParameterValues("group") )
				.setStatusIds		( requestInfo.getParameterValues("status") )
				.setCtgIds			( requestInfo.getParameterValues("category") )
				.setMstoneIds		( requestInfo.getParameterValues("milestone") )
				.setKeyword			( requestInfo.getParameter("keyword") )
				.setAttachedFile	( "true".equals(requestInfo.getParameter("attachment")) )
				.setSelectedPageNo(1)
			;
			param.setLinesPerPage(0);
			param.setSearchServiceType(SearchServiceType.SearchSite);
		}
	}
}
