package jp.co.blueship.tri.fw.um.uix.admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowRoleRemovalServiceBean;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 */
@Controller
@RequestMapping("/admin/role")
public class RoleRemovalController extends TriControllerSupport<FlowRoleRemovalServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.UmRoleRemovalService;
	}

	@Override
	protected FlowRoleRemovalServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowRoleRemovalServiceBean bean = new FlowRoleRemovalServiceBean();
		return bean;
	}

	@RequestMapping(value = "/remove")
	public String removal(FlowRoleRemovalServiceBean bean, TriModel model) {
		String view = "redirect:/admin/role/refresh";

		try {
			bean.getParam().setRequestType(RequestType.submitChanges);
			this.removalMapping(bean, model);
			this.execute(getServiceId(), bean , model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getRedirectAttributes().addFlashAttribute( "result", bean );
		return view;
	}

	private void removalMapping(FlowRoleRemovalServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();
		if ( requestInfo.getParameter("roleId") != null )
		bean.getParam().setSelectedRoleId(requestInfo.getParameter("roleId"));
	}

}
