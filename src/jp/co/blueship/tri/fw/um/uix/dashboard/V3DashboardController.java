package jp.co.blueship.tri.fw.um.uix.dashboard;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.domainx.dashboard.dto.FlowV3DashboardServiceBean;

/**
 *
 * @version V4.02.00
 * @author Chung
 */
@Controller
@RequestMapping("/dashboardV3")
public class V3DashboardController extends TriControllerSupport<FlowV3DashboardServiceBean> {
	@Override
	public ServiceId getServiceId() {
		return ServiceId.UmV3DashBoardService;
	}

	@Override
	protected FlowV3DashboardServiceBean getServiceBean(ISessionInfo sesInfo) {
		return new FlowV3DashboardServiceBean();
	}

	@RequestMapping
	public String index(FlowV3DashboardServiceBean bean, TriModel model) {
		try {
			bean.getParam().setRequestType(RequestType.init);
			this.execute(getServiceId(), bean, model);

		} catch (BaseBusinessException bbe) {
			if (bean.isAuthError()) {
				model.getRedirectAttributes().addFlashAttribute("result", bean);
				return "redirect:/login/error";
			}
		}
		
		model.getModel()
			.addAttribute("view", TriView.DashboardForV3.value())
			.addAttribute("result", bean);

		setPrev(model);

		return TriTemplateView.ProjectTemplateV3.value();
	}
}
