package jp.co.blueship.tri.fw.um.uix.cmn;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowSearchFilterEditServiceBean;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowSearchFilterEditServiceBean.SearchFilterEditInput;

/**
 *
 * @version V4.00.00
 * @author Yuhei Suzuki
 *
 */
@Controller
@RequestMapping("/searchfilter/edit")
public class SearchFilterEditController extends TriControllerSupport<FlowSearchFilterEditServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.UmSearchFilterEditService;
	}

	@Override
	protected FlowSearchFilterEditServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowSearchFilterEditServiceBean bean = new FlowSearchFilterEditServiceBean();
		return bean;
	}

	@RequestMapping
	public String edit(FlowSearchFilterEditServiceBean bean, TriModel model) {
		String view = TriView.BandPopup.value();

		try {
			bean.getParam().setFilterId(model.getRequestInfo().getParameter("filterId"));
			SearchFilterEditInput inputInfo = bean.getParam().getInputInfo();
			inputInfo.setFilterNm(model.getRequestInfo().getParameter("filterNm"));
			bean.getParam().setInputInfo(inputInfo);

			this.execute(getServiceId(), bean , model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel().addAttribute("result", bean);

		return view;
	}


}