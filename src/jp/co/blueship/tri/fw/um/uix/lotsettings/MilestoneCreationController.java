package jp.co.blueship.tri.fw.um.uix.lotsettings;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.domainx.lotsettings.beans.dto.MilestoneEditInputBean;
import jp.co.blueship.tri.fw.um.domainx.lotsettings.dto.FlowMilestoneCreationServiceBean;

@Controller
@RequestMapping("/lot/settings/milestone")
public class MilestoneCreationController extends TriControllerSupport<FlowMilestoneCreationServiceBean> {

	public ServiceId getServiceId() {
		return ServiceId.UmMilestoneCreationService;
	}

	@Override
	protected FlowMilestoneCreationServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowMilestoneCreationServiceBean bean = new FlowMilestoneCreationServiceBean();
		return bean;
	}

	@RequestMapping(value = "/create")
	public String insert(
			FlowMilestoneCreationServiceBean bean,
			TriModel model) {
		String view = TriTemplateView.LotSettings.value();

		if (RequestType.submitChanges.equals(bean.getParam().getRequestType())) {
			try {
				String redirectUrl = this.insertMapping(bean, model);
				this.execute(getServiceId(), bean , model);

				if( bean.getResult().isCompleted() ){
					model.getRedirectAttributes().addFlashAttribute( "result", bean );

					if (TriStringUtils.isEmpty(redirectUrl)) {
						model.getRedirectAttributes().addFlashAttribute("result", bean);
						view = "redirect:/lot/settings/milestone/redirect";
					} else {
						view = redirectUrl;
						String sessionKey = this.getSessionKey(bean.getHeader().getWindowsId(), bean.getParam().getReferer());
						Object sessionBeanTemp = model.getSessionInfo().getAttribute(sessionKey);
						model.getSessionInfo().setAttribute("flowReleaseRequestBeanTemp", sessionBeanTemp);
						model.getRedirectAttributes().addFlashAttribute("mstoneId", bean.getResult().getMstoneId());
						model.getRedirectAttributes().addFlashAttribute("result", bean);
					}
				}

			} catch (Exception e) {
				if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
					return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
				}
			}
		}

		model.getModel().addAttribute("view", TriView.MilestoneCreation.value());
		model.getModel().addAttribute("selectedSideMenu"  , "milestone");
		model.getModel().addAttribute("result", bean);
		setPrev(model);
		return view;
	}

	private String insertMapping(FlowMilestoneCreationServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();
		ISessionInfo sesInfo = model.getSessionInfo();
		String redirectUrl = requestInfo.getParameter("redirectUrl");
		bean.getParam().setReferer(requestInfo.getParameter("referer"));
		MilestoneEditInputBean info = bean.getParam().getInputInfo();
		if ( RequestType.submitChanges.equals(bean.getParam().getRequestType()) ) {
			bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
			info.setSubject(requestInfo.getParameter("milestoneSubject"))
				.setStartDate(requestInfo.getParameter("startDate"))
				.setDueDate(requestInfo.getParameter("dueDate"))
				.setDescription(requestInfo.getParameter("milestoneDescription"));
		}

		return redirectUrl;
	}
}
