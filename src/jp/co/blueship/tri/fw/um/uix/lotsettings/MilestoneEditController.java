package jp.co.blueship.tri.fw.um.uix.lotsettings;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.domainx.lotsettings.beans.dto.MilestoneEditInputBean;
import jp.co.blueship.tri.fw.um.domainx.lotsettings.dto.FlowMilestoneEditServiceBean;

@Controller
@RequestMapping("/lot/settings/milestone")
public class MilestoneEditController extends TriControllerSupport<FlowMilestoneEditServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.UmMilestoneEditService;
	}

	@Override
	protected FlowMilestoneEditServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowMilestoneEditServiceBean bean = new FlowMilestoneEditServiceBean();
		return bean;
	}

	@RequestMapping(value = "/edit")
	public String update(FlowMilestoneEditServiceBean bean,
			TriModel model) {
		String view = TriTemplateView.LotSettings.value();

		try {
			this.updateMapping(bean, model);
			this.execute(getServiceId(), bean , model);
			if (RequestType.submitChanges.equals(bean.getParam().getRequestType())) {

				if( bean.getResult().isCompleted() ){
					model.getRedirectAttributes().addFlashAttribute( "result", bean );
					view = "redirect:/lot/settings/milestone/redirect";
				}
			}

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel().addAttribute("view", TriView.MilestoneEditing.value());
		model.getModel().addAttribute("selectedSideMenu"  , "milestone");
		model.getModel().addAttribute("result"   , bean);
		setPrev(model);
		return view;
	}

	private void updateMapping(FlowMilestoneEditServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();
		ISessionInfo sesInfo = model.getSessionInfo();

		MilestoneEditInputBean info = bean.getParam().getInputInfo();
		FlowMilestoneEditServiceBean.RequestParam param = bean.getParam();

		bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));

		if (RequestType.init.equals(bean.getParam().getRequestType())) {
			if ( requestInfo.getParameter("milestoneId") != null){
				param.setSelectedMstoneId( requestInfo.getParameter("milestoneId") );
			}
		}
		if ( RequestType.submitChanges.equals(bean.getParam().getRequestType()) ) {
			param.setSelectedMstoneId( requestInfo.getParameter("milestoneId") );
			info.setSubject( requestInfo.getParameter("milestoneSubject") )
				.setStartDate(requestInfo.getParameter("startDate"))
				.setDueDate(requestInfo.getParameter("dueDate"))
				.setDescription(requestInfo.getParameter("milestoneDescription"));
		}
	}
}
