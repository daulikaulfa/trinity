package jp.co.blueship.tri.fw.um.uix.wiki;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.domainx.wiki.dto.FlowWikiListServiceBean;
import jp.co.blueship.tri.fw.um.domainx.wiki.dto.FlowWikiListServiceBean.RequestOption;


@Controller
@RequestMapping("/lot/wiki")
public class WikiListController extends TriControllerSupport<FlowWikiListServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.UmWikiListService;
	}

	@Override
	protected FlowWikiListServiceBean getServiceBean(ISessionInfo sesInfo) {
		return new FlowWikiListServiceBean();
	}


	@RequestMapping("/list")
	public String list(FlowWikiListServiceBean bean, TriModel model){
		String view = TriTemplateView.MainTemplate.value();

		try {
			this.mapping(bean, model);
			this.execute(this.getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.WikiSearch.value())
			.addAttribute("selectedMenu", "wikiMenu")
			.addAttribute("result", bean)
		;

		setPrev(model);

		return view;
	}


	private void mapping(FlowWikiListServiceBean bean, TriModel model){
		IRequestInfo requestInfo = model.getRequestInfo();
		ISessionInfo sesInfo = model.getSessionInfo();

		if( RequestType.init.equals(bean.getParam().getRequestType()) ){
			bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
			bean.getParam().getSearchCondition().setTagNm	( requestInfo.getParameter("tagNm") )
												.setKeyword	( requestInfo.getParameter("keyword"))
												;
		}

		if( RequestType.onChange.equals(bean.getParam().getRequestType()) ){
			bean.getParam().setRequestOption( RequestOption.value( requestInfo.getParameter("requestOption") ) );
			bean.getParam().setSearchPageNm( requestInfo.getParameter("searchPageNm") );
		}
	}
}
