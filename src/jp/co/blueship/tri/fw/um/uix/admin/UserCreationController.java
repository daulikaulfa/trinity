package jp.co.blueship.tri.fw.um.uix.admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.sm.contants.PasswordComplexityLevel;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowUserCreationServiceBean;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowUserCreationServiceBean.RequestOption;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 */
@Controller
@RequestMapping("/admin/user")
public class UserCreationController extends TriControllerSupport<FlowUserCreationServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.UmUserCreationService;
	}

	@Override
	protected FlowUserCreationServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowUserCreationServiceBean bean = new FlowUserCreationServiceBean();
		return bean;
	}

	@RequestMapping(value = "/create")
	public String insert(FlowUserCreationServiceBean bean, TriModel model) {

		String view = TriTemplateView.AdminSettings.value();

		try {

			this.mapping(bean, model);
			this.execute(getServiceId(), bean , model);
			if (bean.getResult().isCompleted()) {
				bean.getParam().setRequestType(RequestType.init);
				model.getRedirectAttributes().addFlashAttribute("result", bean);
				view = "redirect:/admin/user/redirect";
			}

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.UserCreation.value())
			.addAttribute("selectedMenu"  , "userList")
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}

	@RequestMapping("/create/validate")
	public String validatePassword(
			FlowUserCreationServiceBean bean,
			TriModel model) {
		String view = TriView.UserCreation.value();

		try {
			this.mapping(bean, model);
			this.execute( getServiceId(), bean, model );

			PasswordComplexityLevel level = bean.getDetailsView().getPassword().getPasswordComplexityLevel();

			if( level == PasswordComplexityLevel.none ){

				model.getModel().addAttribute( "levelClass", "level00" );

			} else if( level == PasswordComplexityLevel.Low ){

				model.getModel().addAttribute( "levelClass", "level01" );

			} else if( level == PasswordComplexityLevel.Middle ){

				model.getModel().addAttribute( "levelClass", "level02" );

			} else if( level == PasswordComplexityLevel.High ){

				model.getModel().addAttribute( "levelClass", "level03" );

			}

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		} finally {

		}
		model.getModel().addAttribute( "result", bean );
		return view;
	}
	private void mapping(FlowUserCreationServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();

		if ( RequestType.submitChanges.equals(bean.getParam().getRequestType()) ) {
			bean.getParam()
				.setRequestOptions(new RequestOption[] {RequestOption.PasswordChanges, RequestOption.UserChanges})
				.getInputInfo()
					.setUserId(requestInfo.getParameter("inUserId"))
					.setUserNm( requestInfo.getParameter("inUserName"))
					.setMailAddress( requestInfo.getParameter("mailAddress"))
					.setDeptId( requestInfo.getParameter("deptId"))
					.setIconPath( requestInfo.getParameter("iconPath"))
					.setReceiveMail(null != requestInfo.getParameter("isReceiveMail"))
					.setPassword( requestInfo.getParameter("password"))
					.setConfirmPassword( requestInfo.getParameter("confirmPassword"))
					.setPasswordExpiration( requestInfo.getParameter("passwordExpiration"))
					.setGroupIds(requestInfo.getParameterValues("groupIds"))
					.setLanguage( requestInfo.getParameter("userLanguage"))
					.setTimeZone( requestInfo.getParameter("userTimeZone"));

		} else if ( RequestType.onChange.equals(bean.getParam().getRequestType())) {
			bean.getParam()
			.setRequestOptions(new RequestOption[] {RequestOption.PasswordChanges, RequestOption.UserChanges})
			.getInputInfo()
				.setPassword( requestInfo.getParameter("password") );
		}
	}

}
