package jp.co.blueship.tri.fw.um.uix.cmn;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriModelAttributes;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.constants.DataAttribute;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowSearchFilterCreationServiceBean;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowSearchThisSiteServiceBean;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowSearchThisSiteServiceBean.RequestParam;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowSearchThisSiteServiceBean.SearchCondition;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowSearchThisSiteServiceBean.SearchServiceType;

@Controller
@RequestMapping("/project/search")
public class SearchThisSiteController extends TriControllerSupport<FlowSearchThisSiteServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.UmSearchThisSiteService;
	}

	@Override
	protected FlowSearchThisSiteServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowSearchThisSiteServiceBean bean = new FlowSearchThisSiteServiceBean();
		return bean;
	}

	@RequestMapping
	public String search(FlowSearchThisSiteServiceBean bean, TriModel model) {

		String view = TriTemplateView.ProjectTemplate.value();

		try {
			this.insertMapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("pagination", bean.getPage())
			.addAttribute("view", TriView.SearchThisSite.value())
			.addAttribute("selectedMenu", "globalSearch")
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}
	
	@RequestMapping(value="/filter/save")
	public String save(FlowSearchThisSiteServiceBean bean, TriModel model) {
		String view = "redirect:/searchfilter/create";
		
		try {
			FlowSearchFilterCreationServiceBean searchFilterBean = new FlowSearchFilterCreationServiceBean();
			String mStoneStartDate = bean.getParam().getSearchCondition().getMstoneStartDate();
			String mStoneDueDate = bean.getParam().getSearchCondition().getMstoneDueDate();
			if ( TriStringUtils.isNotEmpty(mStoneStartDate) && TriStringUtils.isNotEmpty(mStoneDueDate)
					&& !TriDateUtils.before(mStoneStartDate, mStoneDueDate) ) {
				throw new ContinuableBusinessException( UmMessageId.UM001063E );
			}
			searchFilterBean.getParam().getInputInfo()
				.setFilterNm( model.getRequestInfo().getParameter("filterNm") )
				.setServiceId( getServiceId().value() )
				.setSearchFilter( bean.getParam().getSearchCondition() );

			model.getRedirectAttributes().addFlashAttribute( TriModelAttributes.RedirectFilter.value() , searchFilterBean );

		} catch (Exception e) {
			ExceptionUtils.printStackTrace(e);
		}
		
		return view;
	}
	
	@RequestMapping(value = "/filter/search" )
	public String receive(FlowSearchThisSiteServiceBean bean, TriModel model ){
		String view = TriTemplateView.ProjectTemplate.value();

		try {
			model.setRedirect(true);
			String json = null;

			if ( model.isRedirect() && model.containsKey(TriModelAttributes.RedirectFilter) ) {
				json = model.valueOf(TriModelAttributes.RedirectFilter);
//				ISessionInfo sesInfo = model.getSessionInfo();
				bean.getParam().setRequestType(RequestType.init);
			}

			Gson gson = new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create();
			FlowSearchThisSiteServiceBean.SearchCondition condition = gson.fromJson(json, FlowSearchThisSiteServiceBean.SearchCondition.class);
			bean.getParam().setSearchCondition( condition );
			bean.getParam().getSearchCondition().setSelectedPageNo(1);
			bean.getParam().setSearchServiceType(SearchServiceType.SearchSite);
			bean.getParam().setLinesPerPage(20);

			this.execute(this.getServiceId(), bean, model);

		}catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("pagination", bean.getPage())
			.addAttribute("view", TriView.SearchThisSite.value())
			.addAttribute("selectedMenu", "globalSearch")
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;


	}

	private void insertMapping(FlowSearchThisSiteServiceBean bean, TriModel model) {
		RequestParam param = bean.getParam();
		SearchCondition searchCondition = param.getSearchCondition();
		IRequestInfo requestInfo = model.getRequestInfo();

		param.setSearchServiceType(SearchServiceType.SearchSite);

		if(param.getRequestType().equals(RequestType.onChange)) {
			
			
			searchCondition.setLotIds(requestInfo.getParameterValues("filterLot"));
			searchCondition.setMstoneStartDate(requestInfo.getParameter("dateStart"));
			searchCondition.setMstoneDueDate(requestInfo.getParameter("dateEnd"));

			String dataAttribute = requestInfo.getParameter("dataProcessing");

			if(TriStringUtils.isNotEmpty(dataAttribute)) {
				searchCondition.setTableAttribute(DataAttribute.value(dataAttribute));
			} else {			
				searchCondition.setTableAttribute(DataAttribute.none);
			}

			searchCondition.setAssigneeIds(requestInfo.getParameterValues("contact"));
			searchCondition.setGroupIds(requestInfo.getParameterValues("group"));
			searchCondition.setStatusIds(requestInfo.getParameterValues("status"));
			searchCondition.setCtgIds(requestInfo.getParameterValues("category"));
			searchCondition.setMstoneIds(requestInfo.getParameterValues("milestone"));
			searchCondition.setKeyword(requestInfo.getParameter("keyword"));
			searchCondition.setAttachedFile("true".equals(requestInfo.getParameter("attachment")));

			String selectedPageNo = requestInfo.getParameter("selectedPageNo");
			if(TriStringUtils.isNotEmpty(selectedPageNo)) {
				searchCondition.setSelectedPageNo(Integer.parseInt(selectedPageNo));
			} else {
				searchCondition.setSelectedPageNo(1);
			}
			String linesPerPage = requestInfo.getParameter("linesPerPage");
			if(TriStringUtils.isNotEmpty(linesPerPage)) {
				param.setLinesPerPage(Integer.parseInt(linesPerPage));
			}
		}
	}

}
