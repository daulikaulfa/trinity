package jp.co.blueship.tri.fw.um.uix.wiki;

import java.io.IOException;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.JsonObject;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.um.domainx.wiki.dto.FlowWikiAttachmentUploadServiceBean;
import jp.co.blueship.tri.fw.um.domainx.wiki.dto.FlowWikiAttachmentUploadServiceBean.AppendFileBean;
import jp.co.blueship.tri.fw.um.domainx.wiki.dto.FlowWikiAttachmentUploadServiceBean.RequestOption;

/**
 * @version V4.00.00
 * @author Sharov.Maksym
 */
@Controller
@RequestMapping("/lot/wiki/upload")
public class WikiAttachmentUploadController extends TriControllerSupport<FlowWikiAttachmentUploadServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.UmWikiAttachmentUploadService;
	}

	@Override
	protected FlowWikiAttachmentUploadServiceBean getServiceBean(ISessionInfo sesInfo) {
		return new FlowWikiAttachmentUploadServiceBean();
	}

	@RequestMapping
	public @ResponseBody String upload(@RequestParam("userFile") MultipartFile userFile,
			FlowWikiAttachmentUploadServiceBean bean, TriModel model) {

		try {
			bean.getParam().setRequestOption(RequestOption.fileUpload);
			this.uploadFile(userFile, bean, model);
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);
		} catch (Exception e) {
			ExceptionUtils.printStackTrace(e);
		}

		JsonObject json = new JsonObject();
		json.addProperty("seqNo", bean.getParam().getAppendFile().getSeqNo());
		return json.toString();
	}

	@RequestMapping("/delete")
	public @ResponseBody String delete(FlowWikiAttachmentUploadServiceBean bean, TriModel model) {
		String message = "success";

		try {
			bean.getParam().setRequestOption(RequestOption.deleteUplodedFile);
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);
		} catch (Exception e) {
			message = "fail";
			ExceptionUtils.printStackTrace(e);
		}
		return message;
	}

	private void uploadFile(MultipartFile userFile, FlowWikiAttachmentUploadServiceBean bean, TriModel model)
			throws IOException {
		AppendFileBean fileBean = bean.getParam().getAppendFile();
		if (!userFile.isEmpty()) {
			byte[] fileBytes = userFile.getBytes();
			fileBean.setAppendFileInputStreamBytes(fileBytes);
			fileBean.setAppendFileNm(userFile.getOriginalFilename());
		}
	}

	private void mapping(FlowWikiAttachmentUploadServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();

		if (RequestType.init.equals(bean.getParam().getRequestType())) {
			bean.getParam().setSelectedWikiId(requestInfo.getParameter("wikiId"));

			if (RequestOption.deleteUplodedFile.equals(bean.getParam().getRequestOption())) {
				AppendFileBean appendFile = bean.new AppendFileBean();
				appendFile.setAppendFileNm(requestInfo.getParameter("deletedFileNm"));
				appendFile.setSeqNo(requestInfo.getParameter("seqNo"));
				bean.getParam().setAppendFile(appendFile);
			}
		}
	}
}
