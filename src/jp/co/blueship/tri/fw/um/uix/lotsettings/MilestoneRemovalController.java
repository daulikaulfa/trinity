package jp.co.blueship.tri.fw.um.uix.lotsettings;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.um.domainx.lotsettings.dto.FlowMilestoneRemovalServiceBean;

@Controller
@RequestMapping("/lot/settings/milestone/remove")
public class MilestoneRemovalController extends TriControllerSupport<FlowMilestoneRemovalServiceBean>{


	@Override
	public ServiceId getServiceId() {
		return ServiceId.UmMilestoneRemovalService;
	}

	@Override
	protected FlowMilestoneRemovalServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowMilestoneRemovalServiceBean bean = new FlowMilestoneRemovalServiceBean();
		return bean;
	}


	@RequestMapping
	public String delete(FlowMilestoneRemovalServiceBean bean,TriModel model) {
		String view = "redirect:/lot/settings/milestone/refresh";

		try {
			bean.getParam().setRequestType(RequestType.submitChanges);
			this.mapping(bean, model);
			this.execute(getServiceId(), bean , model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getRedirectAttributes().addFlashAttribute( "result", bean );

		return view;
	}


	private void mapping(FlowMilestoneRemovalServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();
		FlowMilestoneRemovalServiceBean.RequestParam param = bean.getParam();

		if ( requestInfo.getParameter("mstoneId") != null )
		param.setSelectedMstoneId(requestInfo.getParameter("mstoneId"));
	}
}
