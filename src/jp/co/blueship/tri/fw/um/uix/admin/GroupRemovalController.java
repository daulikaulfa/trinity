package jp.co.blueship.tri.fw.um.uix.admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowGroupRemovalServiceBean;

@Controller
@RequestMapping("/admin/group")
public class GroupRemovalController extends TriControllerSupport<FlowGroupRemovalServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.UmGroupRemovalService;
	}

	@Override
	protected FlowGroupRemovalServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowGroupRemovalServiceBean bean = new FlowGroupRemovalServiceBean();
		return bean;
	}

	@RequestMapping(value = "/remove")
	public String removal( FlowGroupRemovalServiceBean bean, TriModel model) {
		String view = "redirect:/admin/group/redirect";
		
		try {
			this.removalMapping(bean, model);
			this.execute(getServiceId(), bean, model);
		} catch (Exception e) {
			ExceptionUtils.printStackTrace(e);
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}
		model.getRedirectAttributes().addFlashAttribute("result", bean);
		return view;
	}

	private void removalMapping(FlowGroupRemovalServiceBean bean, TriModel model ) {
		
		IRequestInfo requestInfo = model.getRequestInfo();
		String deletedGroupId = requestInfo.getParameter("deletedGroupId");
		
		bean.getParam().setRequestType(RequestType.submitChanges);
		bean.getParam().setSelectedGroupId(deletedGroupId);
	}
}
