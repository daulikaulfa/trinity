package jp.co.blueship.tri.fw.um.uix.admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.google.gson.Gson;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.domainx.admin.beans.dto.DepartmentEditInputBean;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowDepartmentEditServiceBean;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowDepartmentEditServiceBean.DepartmentRequestOption;

@Controller
@RequestMapping("/admin/department/edit")
public class DepartmentEditController extends TriControllerSupport<FlowDepartmentEditServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.UmDepartmentEditService;
	}

	@Override
	protected FlowDepartmentEditServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowDepartmentEditServiceBean bean = new FlowDepartmentEditServiceBean();
		return bean;
	}

	@RequestMapping
	public String update(FlowDepartmentEditServiceBean bean, TriModel model) {
		String view = TriTemplateView.AdminSettings.value();

		try {
			this.updateMapping(bean, model);
			this.execute(getServiceId(), bean, model);
			if (RequestType.submitChanges.equals(bean.getParam().getRequestType())) {
				if(bean.getResult().isCompleted()) {
					model.getSessionInfo().setAttribute( "departmentId", bean.getParam().getSelectedDeptId());
					model.getRedirectAttributes().addFlashAttribute("result", bean);
					view = "redirect:/admin/department/redirect";
				}
			}

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		Gson gson = new Gson();

		model.getModel()
			.addAttribute("userViews", gson.toJson(bean.getParam().getInputInfo().getUserViews()))
			.addAttribute("departmentId", bean.getParam().getSelectedDeptId())
			.addAttribute("view", TriView.DepartmentEdit.value())
			.addAttribute("selectedMenu", "departList")
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}

	@RequestMapping("/excludeuser")
	public String excludeUserId( FlowDepartmentEditServiceBean bean, TriModel model) {
		String view = TriView.Messages.value();

		try {
			this.excludeUserMapping(bean,model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel().addAttribute("result", bean);
		return view;
	}

	private void excludeUserMapping(FlowDepartmentEditServiceBean bean, TriModel model ) {

		IRequestInfo requestInfo = model.getRequestInfo();
		String excludeUserId = requestInfo.getParameter("excludeUserId");
		String deptId = requestInfo.getParameter("departmentId");

		bean.getParam().setRequestType(RequestType.onChange);
		bean.getParam().setRequestOption(DepartmentRequestOption.ExcludeUser);
		bean.getParam().setSelectedDeptId(deptId);
		bean.getParam().getInputInfo().setExcludeUserId(excludeUserId);
	}

	private void updateMapping(FlowDepartmentEditServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();

		DepartmentEditInputBean info = bean.getParam().getInputInfo();
		FlowDepartmentEditServiceBean.RequestParam param = bean.getParam();

		if(requestInfo.getParameter("departmentId") != null) {
			param.setSelectedDeptId(requestInfo.getParameter("departmentId"));
		}
		if (RequestType.submitChanges.equals(bean.getParam().getRequestType())) {
			info.setDeptNm(requestInfo.getParameter("deptNm"))
				.setUserIds(requestInfo.getParameterValues("userIds"));
		}
	}

}
