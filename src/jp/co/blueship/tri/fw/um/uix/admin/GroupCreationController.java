package jp.co.blueship.tri.fw.um.uix.admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.domainx.admin.beans.dto.GroupEditInputBean;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowGroupCreationServiceBean;

@Controller
@RequestMapping("/admin/group")
public class GroupCreationController extends TriControllerSupport<FlowGroupCreationServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.UmGroupCreationService;
	}

	@Override
	protected FlowGroupCreationServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowGroupCreationServiceBean bean = new FlowGroupCreationServiceBean();
		return bean;
	}

	@RequestMapping("/create")
	public String insert(FlowGroupCreationServiceBean bean, TriModel model) {
		String view = TriTemplateView.AdminSettings.value();

		try {
			this.insertMapping(bean, model);
			this.execute(getServiceId(), bean, model);
			if(bean.getResult().isCompleted()) {
				model.getRedirectAttributes().addFlashAttribute("result", bean);
				view = "redirect:/admin/group/redirect";
			}

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.GroupCreation.value())
			.addAttribute("selectedMenu", "groupList")
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}

	private void insertMapping(FlowGroupCreationServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();
		GroupEditInputBean info = bean.getParam().getInputInfo();
		if(RequestType.submitChanges.equals(bean.getParam().getRequestType())) {
			info.setGroupNm(requestInfo.getParameter("groupNm"))
				.setRoleIds(requestInfo.getParameterValues("roleIds"))
				.setUserIds(requestInfo.getParameterValues("userIds"));
		}
	}
}
