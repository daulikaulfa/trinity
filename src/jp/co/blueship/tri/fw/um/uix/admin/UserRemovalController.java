package jp.co.blueship.tri.fw.um.uix.admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowUserRemovalServiceBean;

@Controller
@RequestMapping("/admin/user")
public class UserRemovalController extends TriControllerSupport<FlowUserRemovalServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.UmUserRemovalService;
	}

	@Override
	protected FlowUserRemovalServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowUserRemovalServiceBean bean = new FlowUserRemovalServiceBean();
		return bean;
	}

	@RequestMapping(value = "/remove")
	public String removal(FlowUserRemovalServiceBean bean,TriModel model) {
		String view = "redirect:/admin/user/redirect";

		try {
			this.removalMapping(bean, model);
			this.execute(getServiceId(), bean, model);
		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException( e, this, bean, model, view );
			}
		}

		model.getRedirectAttributes().addFlashAttribute( "result", bean );
		return view;
	}

	private void removalMapping(FlowUserRemovalServiceBean bean, TriModel model) {

		IRequestInfo requestInfo = model.getRequestInfo();
		String deletedUserId = requestInfo.getParameter("deletedUserId");

		bean.getParam().setRequestType(RequestType.submitChanges);
		bean.getParam().setSelectedUserId(deletedUserId);
	}
}