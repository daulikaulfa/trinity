package jp.co.blueship.tri.fw.um.uix.lotsettings;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.domainx.lotsettings.beans.dto.CategoryEditInputBean;
import jp.co.blueship.tri.fw.um.domainx.lotsettings.dto.FlowCategoryEditServiceBean;

@Controller
@RequestMapping("/lot/settings/category")
public class CategoryEditController extends TriControllerSupport<FlowCategoryEditServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.UmCategoryEditService;
	}

	@Override
	protected FlowCategoryEditServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowCategoryEditServiceBean bean = new FlowCategoryEditServiceBean();
		return bean;
	}

	@RequestMapping(value = "/edit")
	public String update(FlowCategoryEditServiceBean bean,
			TriModel model) {
		String view = TriTemplateView.LotSettings.value();

		try {
			this.updateMapping(bean, model);
			this.execute(getServiceId(), bean,model);
			if (RequestType.submitChanges.equals(bean.getParam().getRequestType())) {

				if( bean.getResult().isCompleted() ){
					model.getRedirectAttributes().addFlashAttribute( "result", bean );
					view = "redirect:/lot/settings/category/redirect";
				}
			}

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel().addAttribute("view", TriView.CategoryEdit.value());
		model.getModel().addAttribute("selectedSideMenu"  ,"category");
		model.getModel().addAttribute("result"   , bean);
		setPrev(model);
		return view;
	}

	private void updateMapping(FlowCategoryEditServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();
		ISessionInfo sesInfo = model.getSessionInfo();

		CategoryEditInputBean info = bean.getParam().getInputInfo();
		FlowCategoryEditServiceBean.RequestParam param = bean.getParam();
		bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
		if (RequestType.init.equals(bean.getParam().getRequestType())) {
			if ( requestInfo.getParameter("categoryId") != null )
			param.setSelectedCategoryId( requestInfo.getParameter("categoryId") );
		}
		if ( RequestType.submitChanges.equals(bean.getParam().getRequestType()) ) {
			info.setSubject(requestInfo.getParameter("categorySubject"));
			param.setSelectedCategoryId( requestInfo.getParameter("categoryId") );
		}
	}

}
