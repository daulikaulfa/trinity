package jp.co.blueship.tri.fw.um.uix.wiki;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.domainx.wiki.dto.FlowWikiEditServiceBean;

@Controller
@RequestMapping("/lot/wiki")
public class WikiEditController extends TriControllerSupport<FlowWikiEditServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.UmWikiEditService;
	}

	@Override
	protected FlowWikiEditServiceBean getServiceBean(ISessionInfo sesInfo) {
		return new FlowWikiEditServiceBean();
	}


	@RequestMapping("/edit")
	public String edit(FlowWikiEditServiceBean bean, TriModel model){
		String view = TriTemplateView.MainTemplate.value();

		try {
			this.mapping(bean, model);
			this.execute(this.getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		if( bean.getResult().isCompleted() ){
			model.getRedirectAttributes().addFlashAttribute("wikiId", bean.getResult().getWikiId());
			model.getRedirectAttributes().addFlashAttribute( "result", bean );
			bean.getParam().setRequestType(RequestType.init);

			view = "redirect:/lot/wiki/details/redirect";
		}

		model.getModel()
			.addAttribute("view", TriView.WikiPageEdit.value())
			.addAttribute("selectedMenu", "wikiMenu")
			.addAttribute("result", bean)
		;

		setPrev(model);

		return view;
	}

	public void mapping(FlowWikiEditServiceBean bean, TriModel model){
		IRequestInfo requestInfo = model.getRequestInfo();

		if( RequestType.init.equals(bean.getParam().getRequestType()) ){
			if ( requestInfo.getParameter("wikiId") != null )
			bean.getParam().setSelectedWikiId( requestInfo.getParameter("wikiId") );
		}

		if( RequestType.submitChanges.equals(bean.getParam().getRequestType()) ){
			String wikiId = requestInfo.getParameter("wikiId");

			if( TriStringUtils.isNotEmpty( wikiId ) )
				bean.getParam().setSelectedWikiId(wikiId);

			bean.getParam().getInputInfo().setPageNm	( requestInfo.getParameter("pageNm") )
										  .setContent	( requestInfo.getParameter("content") )
										  ;
		}
	}
}
