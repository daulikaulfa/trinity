package jp.co.blueship.tri.fw.um.uix.cmn;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriModelAttributes;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowSearchFilterCreationServiceBean;

/**
 *
 * @version V4.00.00
 * @author Yohei Tanaka
 *
 */
@Controller
@RequestMapping("/searchfilter/create")
public class SearchFilterCreationController extends TriControllerSupport<FlowSearchFilterCreationServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.UmSearchFilterCreationService;
	}

	@Override
	protected FlowSearchFilterCreationServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowSearchFilterCreationServiceBean bean = new FlowSearchFilterCreationServiceBean();
		return bean;
	}

	@RequestMapping
	public String redirectCreate( FlowSearchFilterCreationServiceBean bean, TriModel model) {

		String view = TriView.BandPopup.value();

		model.setRedirect(true);
		try {
			FlowSearchFilterCreationServiceBean saveBean = null;
			if ( model.isRedirect() && model.containsKey(TriModelAttributes.RedirectFilter) ) {
				saveBean = model.valueOf(TriModelAttributes.RedirectFilter);
			}

			if( null != saveBean ){
				bean.getParam().setRequestType(RequestType.submitChanges);
				bean.getParam().setInputInfo( saveBean.getParam().getInputInfo() );
				this.execute(getServiceId(), bean , model);
			}

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel().addAttribute("result", bean);
		return view;
	}

}