package jp.co.blueship.tri.fw.um.uix.admin;

import jp.co.blueship.tri.fw.cmn.io.StreamUtils;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowUserBatchUpdateServiceBean;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 *
 * @version V4.00.00
 * @author thang.vu
 *
 * @version V4.03.00
 * @author Cuong Nguyen
 */
@Controller
@RequestMapping("/admin/user/batchupdate")
public class UserBatchUpdateController extends TriControllerSupport<FlowUserBatchUpdateServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.UmUserBatchUpdateService;
	}

	@Override
	protected FlowUserBatchUpdateServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowUserBatchUpdateServiceBean bean = new FlowUserBatchUpdateServiceBean();
		return bean;
	}

	@RequestMapping(value = {"", "/"})
	public String insert(FlowUserBatchUpdateServiceBean bean, TriModel model) {
		String view = TriTemplateView.AdminSettings.value();

		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);
			if (bean.getResult().isCompleted()) {
				bean.getParam().setRequestType(RequestType.init);
				model.getRedirectAttributes().addFlashAttribute("result", bean);
				view = "redirect:/admin/user/redirect";
			}
		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.UserMultipleCreation.value())
			.addAttribute("selectedMenu", "userList")
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}

	@RequestMapping(value="/upload")
	public String upload(@RequestParam("userFile") MultipartFile userFile, FlowUserBatchUpdateServiceBean bean, TriModel model) {
		String view = TriTemplateView.AdminSettings.value();
		try {
			this.uploadFile(userFile, bean, model);
			this.execute(getServiceId(), bean, model);
		} catch (Exception e) {
			ExceptionUtils.printStackTrace(e);
		}

		model.getModel()
				.addAttribute("result", bean)
				.addAttribute("selectedMenu", "userList")
				.addAttribute("view", TriView.UserMultipleCreation.value());
		return view;
	}

	@RequestMapping(value = { "/download/template"})
	public void downloadTemplate(FlowUserBatchUpdateServiceBean bean, TriModel model, HttpServletResponse response) throws IOException {
		bean.getParam().setRequestType(RequestType.onChange);
		bean.getParam().setRequestOption(FlowUserBatchUpdateServiceBean.RequestOption.downloadTemplate);
		this.execute(getServiceId(), bean, model);
		StreamUtils.download(response, bean.getFileView().getTemplateFile());
	}

	private void uploadFile(MultipartFile userFile, FlowUserBatchUpdateServiceBean bean, TriModel model) throws IOException {
		FlowUserBatchUpdateServiceBean.RequestParam param = bean.getParam();

		param.setRequestOption(FlowUserBatchUpdateServiceBean.RequestOption.fileUpload);
		if (!userFile.isEmpty()) {
			byte[] fileBytes = userFile.getBytes();
			param.getInputInfo().setCsvInputStreamBytes(fileBytes);
			param.getInputInfo().setCsvFileNm(userFile.getOriginalFilename());
		}
	}

	private void mapping(FlowUserBatchUpdateServiceBean bean, TriModel model) {
		if (RequestType.init.equals(bean.getParam().getRequestType())) {

		} else if (RequestType.submitChanges.equals(bean.getParam().getRequestType())) {

		} else if (RequestType.onChange.equals(bean.getParam().getRequestType())) {

		}
	}
}
