package jp.co.blueship.tri.fw.um.uix.lotsettings;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.domainx.lotsettings.beans.dto.CategoryListServiceSortOrderEditBean;
import jp.co.blueship.tri.fw.um.domainx.lotsettings.dto.FlowCategoryListServiceBean;

@Controller
@RequestMapping("/lot/settings/category")
public class CategoryListController extends TriControllerSupport<FlowCategoryListServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.UmCategoryListService;
	}

	@Override
	protected FlowCategoryListServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowCategoryListServiceBean bean = new FlowCategoryListServiceBean();
		return bean;
	}

	@RequestMapping(value = "/redirect")
	public String redirect( FlowCategoryListServiceBean bean, TriModel model ){
		return index( bean , model.setRedirect(true) );
	}

	@RequestMapping(value = "/refresh")
	public String refresh(FlowCategoryListServiceBean bean, TriModel model){

		String view = TriTemplateView.LotSettings.value();
		try{
			bean.getParam().setRequestType(RequestType.submitChanges);
			this.execute(getServiceId(), bean , model.setRedirect(true) );

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.CategoryList.value())
			.addAttribute("selectedSideMenu"  ,"category")
			.addAttribute("result"   , bean)
		;

		setPrev(model);
		return view;
	}

	@RequestMapping(value = {"","/"})
	public String index(FlowCategoryListServiceBean bean, TriModel model) {
		String view = TriTemplateView.LotSettings.value();

		try {
			ISessionInfo sesInfo = model.getSessionInfo();
			bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
			this.execute(getServiceId(), bean, model );

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.CategoryList.value())
			.addAttribute("selectedSideMenu"  ,"category")
			.addAttribute("result"   , bean)
		;

		setPrev(model);
		return view;
	}

	@RequestMapping(value = "/changesort")
	public String changeSortOrder(FlowCategoryListServiceBean bean,
			TriModel model) {
		String view = TriView.CategoryList.value() + "::categoryList";

		try {
			ISessionInfo sesInfo = model.getSessionInfo();
			bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
			this.changeSortOrderMapping(bean, model);
			this.execute(getServiceId(), bean , model );

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel().addAttribute("result", bean);

		return view;
	}

	private void changeSortOrderMapping(FlowCategoryListServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();

		FlowCategoryListServiceBean.RequestParam param = bean.getParam();
		param.setRequestType(RequestType.onChange);
		CategoryListServiceSortOrderEditBean sortOrderBean = param.getSortOrderEdit();

		sortOrderBean.setCategoryId(requestInfo.getParameter("selectedCategoryId"))
					.setNewSortOrder(Integer.parseInt(requestInfo.getParameter("newSortOrder")));
	}

}
