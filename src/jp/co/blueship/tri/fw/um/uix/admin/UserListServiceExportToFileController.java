package jp.co.blueship.tri.fw.um.uix.admin;

import jp.co.blueship.tri.dcm.DcmExportToFileUtils;
import jp.co.blueship.tri.fw.cmn.io.StreamUtils;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.constants.ExportToFileSubmitOption;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowUserListServiceExportToFileBean;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;

/**
 * @author Cuong Nguyen
 * @version 4.03.00
 */
@Controller
@RequestMapping("/admin/user/export")
public class UserListServiceExportToFileController extends TriControllerSupport<FlowUserListServiceExportToFileBean> {
	@Override
	public ServiceId getServiceId() {
		return ServiceId.UmUserListServiceExportToFile;
	}

	@Override
	protected FlowUserListServiceExportToFileBean getServiceBean(ISessionInfo sesInfo) {
		FlowUserListServiceExportToFileBean bean = new FlowUserListServiceExportToFileBean();
		return bean;
	}

	@RequestMapping("/file")
	public void exportFile(FlowUserListServiceExportToFileBean bean, TriModel model,
						   HttpServletResponse response) {
		String view = TriView.Messages.value() + "::message";
		try {
			this.execute(getServiceId(), bean, model);

			String newFileName =
					DcmExportToFileUtils.getDownloadFileName(
							getServiceId(),
							ExportToFileSubmitOption.ExportToCsv.extention(),
							bean.getLanguage(),
							bean.getTimeZone());

			StreamUtils.download(
					response,
					bean.getFileResponse().getFile(),
					newFileName);

			TriFileUtils.delete(bean.getFileResponse().getFile());

		} catch (Exception e) {
			ExceptionUtils.printStackTrace(e);
		}
	}

	@RequestMapping("/validate")
	public String validateExportFile(FlowUserListServiceExportToFileBean bean, TriModel model,
						   HttpServletResponse response) {
		String view = TriView.Messages.value() + "::message";
		try {
			this.execute(getServiceId(), bean, model);
		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}
		model.getModel().addAttribute("result", bean);
		return view;
	}
}
