package jp.co.blueship.tri.fw.um.uix.cmn;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowSearchFilterServiceBean;

/**
 *
 * @version V4.00.00
 * @author Yuhei Suzuki
 *
 */
@Controller
@RequestMapping("/searchfilter")
public class SearchFilterController extends TriControllerSupport<FlowSearchFilterServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.UmSearchFilterService;
	}

	@Override
	protected FlowSearchFilterServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowSearchFilterServiceBean bean = new FlowSearchFilterServiceBean();
		return bean;
	}

	@RequestMapping
	public String list(FlowSearchFilterServiceBean bean, TriModel model) {
		String view = TriView.GlobalHeaderSearchFilter.value();

		bean.getParam().setSelectedUserId(bean.getUserId());

		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean , model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel().addAttribute("result", bean);
		model.getModel().addAttribute("pagination",bean.getPage());

		return view;
	}

	private void mapping(FlowSearchFilterServiceBean bean, TriModel model) {

		IRequestInfo requestInfo = model.getRequestInfo();
		FlowSearchFilterServiceBean.RequestParam param = bean.getParam();

		if (RequestType.init.equals(bean.getParam().getRequestType())) {

			bean.getPage().setSelectPageNo(1);
			bean.getParam().setRequestType(RequestType.onChange);

		} else if (RequestType.onChange.equals(param.getRequestType())) {

			bean.getParam().getSearchCondition().setSelectedPageNo(Integer.parseInt(requestInfo.getParameter("selectedPageNo")));
			bean.getParam().getSearchCondition().setKeyword(requestInfo.getParameter("keyword"));
		}

	}
}