package jp.co.blueship.tri.fw.um.uix.lotsettings;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.domainx.lotsettings.beans.dto.CategoryEditInputBean;
import jp.co.blueship.tri.fw.um.domainx.lotsettings.dto.FlowCategoryCreationServiceBean;

@Controller
@RequestMapping("/lot/settings/category")
public class CategoryCreationController extends TriControllerSupport<FlowCategoryCreationServiceBean> {

	public ServiceId getServiceId() {
		return ServiceId.UmCategoryCreationService;
	}

	@Override
	protected FlowCategoryCreationServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowCategoryCreationServiceBean bean = new FlowCategoryCreationServiceBean();
		return bean;
	}

	@RequestMapping(value = "/create")
	public String insert(
			FlowCategoryCreationServiceBean bean,
			TriModel model) {
		String view = TriTemplateView.LotSettings.value();

		try {
			String redirectUrl = this.insertMapping(bean, model);
			this.execute(getServiceId(), bean , model );
			if( bean.getResult().isCompleted() ){
				model.getRedirectAttributes().addFlashAttribute( "result", bean );
				if (TriStringUtils.isEmpty(redirectUrl)) {
					view = "redirect:/lot/settings/category/redirect";
					model.getRedirectAttributes().addFlashAttribute("result", bean);
				} else {
					view = redirectUrl;
					String sessionKey = this.getSessionKey(bean.getHeader().getWindowsId(), bean.getParam().getReferer());
					Object temp = model.getSessionInfo().getAttribute(sessionKey);
					model.getSessionInfo().setAttribute("flowReleaseRequestBeanTemp", temp);
					model.getRedirectAttributes().addFlashAttribute("ctgId", bean.getResult().getCtgId());
					model.getRedirectAttributes().addFlashAttribute("result", bean);
				}
			}

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel().addAttribute("view", TriView.CategoryCreation.value());
		model.getModel().addAttribute("selectedSideMenu"  , "category");
		model.getModel().addAttribute("result"   , bean);
		setPrev(model);
		return view;
	}

	private String insertMapping(FlowCategoryCreationServiceBean bean, TriModel model) {

		IRequestInfo requestInfo = model.getRequestInfo();
		ISessionInfo sesInfo = model.getSessionInfo();
		String redirectUrl = requestInfo.getParameter("redirectUrl");
		bean.getParam().setReferer(requestInfo.getParameter("referer"));
		bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
		if (RequestType.submitChanges.equals(bean.getParam().getRequestType()) ) {
		CategoryEditInputBean info = bean.getParam().getInputInfo();
			info.setSubject(requestInfo.getParameter("categorySubject"));
		}

		return redirectUrl;
	}

}
