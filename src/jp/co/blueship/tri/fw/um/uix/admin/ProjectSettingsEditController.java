package jp.co.blueship.tri.fw.um.uix.admin;

import java.io.IOException;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.constants.TriSessionAttributes;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.constants.IconSelectionOption;
import jp.co.blueship.tri.fw.um.constants.LookAndFeel;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowProjectSettingsEditServiceBean;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowProjectSettingsEditServiceBean.ProjectSettingEditInputBean;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowProjectSettingsEditServiceBean.RequestOption;

/**
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 */
@Controller
@RequestMapping("/admin")
public class ProjectSettingsEditController extends TriControllerSupport<FlowProjectSettingsEditServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.UmProjectSettingsEditService;
	}

	@Override
	protected FlowProjectSettingsEditServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowProjectSettingsEditServiceBean bean = new FlowProjectSettingsEditServiceBean();
		return bean;
	}

	@RequestMapping(value = {"", "/", "/profile"})
	public String edit(FlowProjectSettingsEditServiceBean bean, TriModel model) {
		String view = TriTemplateView.AdminSettings.value();

		try {
			bean.getParam().setRequestOptions(RequestOption.ProjectChanges);
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

			if (bean.getResult().isCompleted()
					&& RequestType.submitChanges.equals(bean.getParam().getRequestType())
					&& bean.getParam().isRequestOptionOf(RequestOption.ProjectChanges)) {

				ProjectSettingEditInputBean inputInfo = bean.getParam().getProjectInputInfo();
				model.getSessionInfo().setAttribute(TriSessionAttributes.ProjectName.value(), inputInfo.getProjectNm());

				if (IconSelectionOption.CustomImage.equals(inputInfo.getIconOption().value())) {
					model.getSessionInfo().setAttribute(TriSessionAttributes.ProjectIconPath.value(), inputInfo.getIconFilePath());
				} else {
					model.getSessionInfo().setAttribute(TriSessionAttributes.ProjectIconPath.value(), inputInfo.getIconPath());
				}
			}
		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.ProjectEdit.value())
			.addAttribute("selectedMenu", "projectEdit")
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}

	@RequestMapping(value = {"/locale"})
	public String languageTimezone(FlowProjectSettingsEditServiceBean bean, TriModel model) {
		String view = TriTemplateView.AdminSettings.value();

		try {
			bean.getParam().setRequestOptions(RequestOption.LanguageAndTimezoneChanges);
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.AdminSettingsLanguageTimezone.value())
			.addAttribute("selectedMenu", "langTime")
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}

	@RequestMapping(value = {"/lookandfeel"})
	public String lookAndFeel(FlowProjectSettingsEditServiceBean bean, TriModel model) {
		String view = TriTemplateView.AdminSettings.value();

		try {
			bean.getParam().setRequestOptions(RequestOption.LookAndFeelChanges);
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);
		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.AdminSettingsUICompatibility.value())
			.addAttribute("selectedMenu", "lookAndFeel")
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}

	@RequestMapping(value = {"/lookandfeelV3"})
	public String lookAndFeelV3(FlowProjectSettingsEditServiceBean bean, TriModel model) {
		String view = TriTemplateView.AdminSettingsV3.value();

		try {
			bean.getParam().setRequestOptions(RequestOption.LookAndFeelChanges);
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);
		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.AdminSettingsUIcompatibilityforV3.value())
			.addAttribute("selectedMenu", "lookAndFeel")
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}

	@RequestMapping(value = {"/notify"})
	public String notification(FlowProjectSettingsEditServiceBean bean, TriModel model) {
		String view = TriTemplateView.AdminSettings.value();

		try {
			bean.getParam().setRequestOptions(RequestOption.NotificationChanges);
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.ProjectNotification.value())
			.addAttribute("selectedMenu", "projectNoti")
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}

	@RequestMapping(value = {"/profile/upload"}, method = RequestMethod.POST)
	public @ResponseBody String uploadFile( @RequestParam("icon") MultipartFile customIconFile,
			FlowProjectSettingsEditServiceBean bean, TriModel model) {
		String imagePath = "";

		try {
			this.uploadFileMapping(customIconFile, bean, model);
			this.execute(getServiceId(), bean, model);
			imagePath = bean.getParam().getProjectInputInfo().getIconFilePath();

		} catch (Exception e) {
			ExceptionUtils.printStackTrace(e);
			imagePath = "fail";
		}

		return imagePath;
	}


	private void mapping (FlowProjectSettingsEditServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();
		String requestType = requestInfo.getParameter("RequestType");
		if ( TriStringUtils.isNotEmpty(requestType) ) {
			bean.getParam().setRequestType(RequestType.value(requestType));
		}

		if (RequestType.submitChanges.equals(RequestType.value(requestType))) {

			if (bean.getParam().isRequestOptionOf(RequestOption.ProjectChanges)) {
				bean.getParam().getProjectInputInfo().setProjectNm(requestInfo.getParameter("projectName"));

				String iconType = requestInfo.getParameter("iconOption");
				bean.getParam().getProjectInputInfo().setIconOption(IconSelectionOption.value(iconType));

				if (IconSelectionOption.CustomImage.equals(iconType)) {
					bean.getParam().getProjectInputInfo().setIconFilePath(requestInfo.getParameter("iconPath"));
				} else if (IconSelectionOption.DefaultImage.equals(iconType)) {
					bean.getParam().getProjectInputInfo().setIconPath(requestInfo.getParameter("iconPath"));
				}

			} else if (bean.getParam().isRequestOptionOf(RequestOption.LanguageAndTimezoneChanges)) {
				bean.getParam().getLanguageTimezoneInputInfo().setLanguage(requestInfo.getParameter("language"));
				bean.getParam().getLanguageTimezoneInputInfo().setTimezone(requestInfo.getParameter("timezone"));

			} else if (bean.getParam().isRequestOptionOf(RequestOption.NotificationChanges)) {
				bean.getParam().getNotificationInputInfo().setNotification(requestInfo.getParameter("notification"));
			} else if (bean.getParam().isRequestOptionOf(RequestOption.LookAndFeelChanges)) {
				bean.getParam().getLookAndFeelEditInputInfo().setLookAndFeel(LookAndFeel.value(requestInfo.getParameter("lookAndFeel")));
			}
		}
	}

	private void uploadFileMapping(MultipartFile customFile, FlowProjectSettingsEditServiceBean bean, TriModel model)
		throws IOException {
		FlowProjectSettingsEditServiceBean.RequestParam param = bean.getParam();
		ProjectSettingEditInputBean projectInfo = param.getProjectInputInfo();

		param.setRequestOptions(RequestOption.ProjectChanges);
		byte[] file = new byte[] {};

		if  ( !customFile.isEmpty() ) {
			file = customFile.getBytes();
			projectInfo.setIconOption(IconSelectionOption.CustomImage);
			projectInfo.setIconInputStreamBytes(file);
			projectInfo.setIconFileNm(customFile.getOriginalFilename());
		}
	}
}
