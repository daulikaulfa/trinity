package jp.co.blueship.tri.fw.um.uix.cmn;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowSearchFilterRemovalServiceBean;

/**
 *
 * @version V4.00.00
 * @author Yuhei Suzuki
 *
 */
@Controller
@RequestMapping("/searchfilter/remove")
public class SearchFilterRemovalController extends TriControllerSupport<FlowSearchFilterRemovalServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.UmSearchFilterRemovalService;
	}

	@Override
	protected FlowSearchFilterRemovalServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowSearchFilterRemovalServiceBean bean = new FlowSearchFilterRemovalServiceBean();
		return bean;
	}

	@RequestMapping
	public String remove(FlowSearchFilterRemovalServiceBean bean, TriModel model) {
		String view = TriView.BandPopup.value();

		try {
			bean.getParam().setSelectedFilterId(model.getRequestInfo().getParameter("filterId"));
			this.execute(getServiceId(), bean , model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel().addAttribute("result", bean);

		return view;
	}




}