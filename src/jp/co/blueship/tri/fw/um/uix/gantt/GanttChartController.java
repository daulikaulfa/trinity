package jp.co.blueship.tri.fw.um.uix.gantt;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.domainx.gantt.beans.dto.GanttViewBean.Grouping;
import jp.co.blueship.tri.fw.um.domainx.gantt.beans.dto.GanttViewBean.Status;
import jp.co.blueship.tri.fw.um.domainx.gantt.dto.FlowGanttChartServiceBean;
import jp.co.blueship.tri.fw.um.domainx.gantt.dto.FlowGanttChartServiceBean.RequestOption;
import jp.co.blueship.tri.fw.um.domainx.gantt.dto.FlowGanttChartServiceBean.RequestParam;
import jp.co.blueship.tri.fw.um.domainx.gantt.dto.FlowGanttChartServiceBean.Span;

/**
 * @version V4.00.00
 * @author Yuhei Suzuki
 *
 * @version V4.03.00
 * @author Cuong Nguyen
 */

@Controller
@RequestMapping("/lot/gantt")
public class GanttChartController extends TriControllerSupport<FlowGanttChartServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.UmGanttChartService;
	}

	@Override
	protected FlowGanttChartServiceBean getServiceBean(ISessionInfo sesInfo) {
		return new FlowGanttChartServiceBean();
	}

	@RequestMapping(value = { "", "/" })
	public String ganttChart(FlowGanttChartServiceBean bean, TriModel model) {
		String view = TriTemplateView.MainTemplate.value();

		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel().addAttribute("view", TriView.GanttChart.value());
		model.getModel().addAttribute("selectedMenu", "calendarMenu");
		model.getModel().addAttribute("result", bean);
		setPrev(model);
		return view;
	}


	private void mapping(FlowGanttChartServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();
		ISessionInfo sesInfo = model.getSessionInfo();

		RequestParam param = bean.getParam();
		if (RequestType.init.equals(param.getRequestType())) {
			if( TriStringUtils.isNotEmpty(bean.getParam().getSelectedLotId()) &&
					!this.getSessionSelectedLot(sesInfo, bean).equals(bean.getParam().getSelectedLotId())){
				bean.getParam().setSearchCondition(bean.new SearchCondition() );
			}
			param.setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
		}

		if (RequestType.onChange.equals(param.getRequestType()) ){
			if (requestInfo.getParameter("condSpan") != null) {
				bean.getParam().setRequestOption(RequestOption.value( requestInfo.getParameter("requestOption") ));
	
				bean.getParam().getSearchCondition()
					.setStartDate(requestInfo.getParameter("startDate"))
					.setSpan(Span.value(Integer.valueOf(requestInfo.getParameter("condSpan"))))
					.setGrouping(Grouping.value(requestInfo.getParameter("condGrouping")))
					.setStatus(Status.value(requestInfo.getParameter("condStatus")))
					;
			}
		}
	}
}

