package jp.co.blueship.tri.fw.um.uix.admin;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.domainx.admin.beans.dto.GroupListServiceSortOrderEditBean;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowGroupListServiceBean;

@Controller
@RequestMapping("/admin/group")
public class GroupListController extends TriControllerSupport<FlowGroupListServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.UmGroupListService;
	}

	@Override
	protected FlowGroupListServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowGroupListServiceBean bean = new FlowGroupListServiceBean();
		return bean;
	}

	@RequestMapping(value = "/redirect")
	public String redirect( FlowGroupListServiceBean bean, TriModel model, HttpServletResponse response ){
		return listView( bean , model.setRedirect(true) );
	}

	@RequestMapping(value = {"", "/"})
	public String listView(FlowGroupListServiceBean bean, TriModel model) {
		String view = TriTemplateView.AdminSettings.value();

		try {
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.GroupList.value())
			.addAttribute("selectedMenu", "groupList")
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}

	@RequestMapping(value="/changesort")
	public String changeSortOrder(FlowGroupListServiceBean bean, TriModel model) {
		String view = TriView.GroupList.value() + "::groupList";

		try {
			this.changeSortOrderMapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel().addAttribute("result", bean);
		return view;
	}

	private void changeSortOrderMapping(FlowGroupListServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();

		FlowGroupListServiceBean.RequestParam param = bean.getParam();
		param.setRequestType(RequestType.submitChanges);
		GroupListServiceSortOrderEditBean sortOrderBean = param.getSortOrderEdit();
		sortOrderBean.setGroupId(requestInfo.getParameter("selectedGroupId"))
					 .setNewSortOrder(Integer.parseInt(requestInfo.getParameter("newSortOrder")));
	}
}
