package jp.co.blueship.tri.fw.um.uix.project;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.domainx.project.dto.FlowMemberListServiceBean;


@Controller
@RequestMapping("/project/member")
public class MemberListController extends TriControllerSupport<FlowMemberListServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.UmMemberListService;
	}
	@Override
	protected FlowMemberListServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowMemberListServiceBean bean = new FlowMemberListServiceBean();
		return bean;
	}

	@RequestMapping(value = {"","/"})
	public String index(FlowMemberListServiceBean bean,
			TriModel model) {
		String view = TriTemplateView.ProjectTemplate.value();

		try {

			this.mapping(bean, model);
			this.execute(getServiceId(), bean , model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("result", bean)
			.addAttribute("view", TriView.Member.value());

		setPrev(model);
		return view;
	}

	@RequestMapping(value = "/search")
	public String search(FlowMemberListServiceBean bean, TriModel model ){
		String view = TriView.Member.value();

		try{

			this.mapping(bean, model);
			this.execute(getServiceId(), bean , model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel().addAttribute("result", bean);
		setPrev(model);
		return view;
	}


	private void mapping(
		FlowMemberListServiceBean bean, TriModel model) {

			FlowMemberListServiceBean.RequestParam param = bean.getParam();
			if (RequestType.init.equals(param.getRequestType())) {
			}

			if (RequestType.onChange.equals(param.getRequestType()) ){
				IRequestInfo requestInfo = model.getRequestInfo();
				bean.getParam().getSearchCondition().setKeyword(requestInfo.getParameter("searchKeyword"));
			}
	}
}
