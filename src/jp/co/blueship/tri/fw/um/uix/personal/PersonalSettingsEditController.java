package jp.co.blueship.tri.fw.um.uix.personal;

import java.io.IOException;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.constants.TriSessionAttributes;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.sm.contants.PasswordComplexityLevel;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.constants.GenderType;
import jp.co.blueship.tri.fw.um.constants.IconSelectionOption;
import jp.co.blueship.tri.fw.um.constants.LookAndFeel;
import jp.co.blueship.tri.fw.um.domainx.personal.dto.FlowPersonalSettingsEditServiceBean;
import jp.co.blueship.tri.fw.um.domainx.personal.dto.FlowPersonalSettingsEditServiceBean.LanguageAndTimezoneEditInputBean;
import jp.co.blueship.tri.fw.um.domainx.personal.dto.FlowPersonalSettingsEditServiceBean.PasswordEditInputBean;
import jp.co.blueship.tri.fw.um.domainx.personal.dto.FlowPersonalSettingsEditServiceBean.RequestOption;
import jp.co.blueship.tri.fw.um.domainx.personal.dto.FlowPersonalSettingsEditServiceBean.UserEditInputBean;

/**
 *
 * @version V4.00.00
 * @author Maksym
 */
@Controller
@RequestMapping("/personal")
public class PersonalSettingsEditController extends TriControllerSupport<FlowPersonalSettingsEditServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.UmPersonalSettingsEditService;
	}

	@Override
	protected FlowPersonalSettingsEditServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowPersonalSettingsEditServiceBean bean = new FlowPersonalSettingsEditServiceBean();
		return bean;
	}

	@RequestMapping(value = { "", "/" })
	public String updateUser(FlowPersonalSettingsEditServiceBean bean, TriModel model) {
		String view = TriTemplateView.PersonalSettings.value();

		try {
			this.updateUserMapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.PersonalSettingsUserProfile.value())
			.addAttribute("selectedMenu", "user")
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}

	@RequestMapping(value = "/locale")
	public String updateLocale(FlowPersonalSettingsEditServiceBean bean, TriModel model) {
		String view = TriTemplateView.PersonalSettings.value();
		try {
			this.updateLocaleMapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.PersonalSettingsLanguageTimezone.value())
			.addAttribute("selectedMenu", "langTime")
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}

	@RequestMapping(value = "/lookandfeel")
	public String lookAndFeel(FlowPersonalSettingsEditServiceBean bean, TriModel model) {
		String view = TriTemplateView.PersonalSettings.value();
		try {
			this.lookAndFeelMapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.PersonalSettingsUICompatibility.value())
			.addAttribute("selectedMenu", "lookAndFeel")
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}

	@RequestMapping(value = "/lookandfeelV3")
	public String lookAndFeelV3(FlowPersonalSettingsEditServiceBean bean, TriModel model) {
		String view = TriTemplateView.PersonalSettingsV3.value();
		try {
			this.lookAndFeelMapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.PersonalSettingsUIcompatibilityforV3.value())
			.addAttribute("selectedMenu", "lookAndFeel")
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}

	@RequestMapping(value = "/password")
	public String updatePassword(FlowPersonalSettingsEditServiceBean bean, TriModel model) {
		String view = TriTemplateView.PersonalSettings.value();

		try {
			this.updatePasswordMapping(bean, model);
			this.execute(getServiceId(), bean, model);
		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.PersonalsettingsPassword.value())
			.addAttribute("selectedMenu", "password")
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}

	@RequestMapping(value = "/profile")
	public String editUser(FlowPersonalSettingsEditServiceBean bean, TriModel model) {
		String view = TriTemplateView.PersonalSettings.value();

		try {
			this.updateUserMapping(bean, model);
			this.execute(getServiceId(), bean, model);

			if (bean.getResult().isCompleted()) {
				model.getSessionInfo().setAttribute(TriSessionAttributes.UserIconPath.value(),
						bean.getHeader().getUserIconPath());
				model.getSessionInfo().setAttribute(TriSessionAttributes.UserName.value(),
						bean.getUserName());
			}
		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.PersonalSettingsUserProfile.value())
			.addAttribute("selectedMenu", "user")
			.addAttribute("result", bean)
		;


		setPrev(model);
		return view;
	}

	@RequestMapping(value = "/profile/upload", method = RequestMethod.POST)
	public @ResponseBody String uploadIcon(@RequestParam("icon") MultipartFile customIconFile,
			FlowPersonalSettingsEditServiceBean bean, TriModel model) {
		String imagePath = "";

		try {
			this.uploadCustomIconMapping(customIconFile, bean, model);
			this.execute(getServiceId(), bean, model);
			imagePath = bean.getParam().getUserInputInfo().getIconFilePath();
		} catch (Exception e) {
			ExceptionUtils.printStackTrace(e);
			imagePath = "fail";
		}

		return imagePath;
	}

	@RequestMapping("/password/validate")
	public String validatePassword(FlowPersonalSettingsEditServiceBean bean, TriModel model) {
		String view = TriView.PersonalsettingsPassword.value();

		try {
			this.updatePasswordMapping(bean, model);
			this.execute(getServiceId(), bean, model);

			PasswordComplexityLevel level = bean.getDetailsView().getPassword().getPasswordComplexityLevel();

			if (level == PasswordComplexityLevel.none) {

				model.getModel().addAttribute("levelClass", "level00");

			} else if (level == PasswordComplexityLevel.Low) {

				model.getModel().addAttribute("levelClass", "level01");

			} else if (level == PasswordComplexityLevel.Middle) {

				model.getModel().addAttribute("levelClass", "level02");

			} else if (level == PasswordComplexityLevel.High) {

				model.getModel().addAttribute("levelClass", "level03");

			}

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel().addAttribute("result", bean);
		return view;
	}

	private void updateUserMapping(FlowPersonalSettingsEditServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();
		UserEditInputBean userInfo = bean.getParam().getUserInputInfo();
		FlowPersonalSettingsEditServiceBean.RequestParam param = bean.getParam();

		param.setRequestOptions(RequestOption.UserChanges);
		if (RequestType.submitChanges.equals(bean.getParam().getRequestType())) {
			userInfo.setUserNm(requestInfo.getParameter("userNm"));
			userInfo.setMailAddress(requestInfo.getParameter("mailAddress"));
			userInfo.setGenderType(GenderType.value(requestInfo.getParameter("profileSex")));
			userInfo.setLocation(requestInfo.getParameter("location"));
			userInfo.setBiography(requestInfo.getParameter("biography"));
			userInfo.setUrl(requestInfo.getParameterValues("url"));

			userInfo.setIconOption(IconSelectionOption.value(requestInfo.getParameter("iconOption")));
			if (userInfo.getIconOption() == IconSelectionOption.CustomImage) {
				userInfo.setIconFilePath(requestInfo.getParameter("iconPath"));
			} else if (userInfo.getIconOption() == IconSelectionOption.DefaultImage) {
				userInfo.setIconPath(requestInfo.getParameter("iconPath"));
			}
		}
	}

	private void updateLocaleMapping(FlowPersonalSettingsEditServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();
		LanguageAndTimezoneEditInputBean localeInfo = bean.getParam().getLanguageTimezoneInputInfo();
		FlowPersonalSettingsEditServiceBean.RequestParam param = bean.getParam();

		param.setRequestOptions(RequestOption.LanguageAndTimezoneChanges);
		if (RequestType.submitChanges.equals(bean.getParam().getRequestType())) {
			localeInfo.setLanguage(requestInfo.getParameter("language"));
			localeInfo.setTimezone(requestInfo.getParameter("timezone"));
		}
	}

	private void lookAndFeelMapping(FlowPersonalSettingsEditServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();

		bean.getParam().setRequestOptions(RequestOption.LookAndFeelChanges);
		if (RequestType.submitChanges.equals(bean.getParam().getRequestType())) {
			bean.getParam().getLookAndFeelInputInfo().setLookAndFeel(LookAndFeel.value(requestInfo.getParameter("lookAndFeel")));
		}
	}

	private void updatePasswordMapping(FlowPersonalSettingsEditServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();
		PasswordEditInputBean passwordInfo = bean.getParam().getPasswordInputInfo();
		FlowPersonalSettingsEditServiceBean.RequestParam param = bean.getParam();

		param.setRequestOptions(RequestOption.PasswordChanges);
		if (RequestType.submitChanges.equals(bean.getParam().getRequestType())) {
			passwordInfo.setPasswordExpiration(requestInfo.getParameter("passwordExpiration"));
			passwordInfo.setCurrentPassword(requestInfo.getParameter("currentPassword"));
			passwordInfo.setNewPassword(requestInfo.getParameter("newPassword"));
			passwordInfo.setConfirmPassword(requestInfo.getParameter("confirmPassword"));
		} else if (RequestType.onChange.equals(bean.getParam().getRequestType())) {
			passwordInfo.setNewPassword(requestInfo.getParameter("newPassword"));
		}
	}

	private void uploadCustomIconMapping(MultipartFile customIconFile, FlowPersonalSettingsEditServiceBean bean,
			TriModel model) throws IOException {
		UserEditInputBean userInfo = bean.getParam().getUserInputInfo();
		FlowPersonalSettingsEditServiceBean.RequestParam param = bean.getParam();

		param.setRequestOptions(RequestOption.UserChanges);
		byte[] iconBytes = new byte[] {};
		if (!customIconFile.isEmpty()) {
			iconBytes = customIconFile.getBytes();
			userInfo.setIconOption(IconSelectionOption.CustomImage);
			userInfo.setIconInputStreamBytes(iconBytes);
			userInfo.setIconFileNm(customIconFile.getOriginalFilename());
		}
	}
}
