package jp.co.blueship.tri.fw.um.uix.lotsettings;

import java.io.IOException;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.constants.TriSessionAttributes;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.constants.IconSelectionOption;
import jp.co.blueship.tri.fw.um.constants.ThemeColor;
import jp.co.blueship.tri.fw.um.domainx.lotsettings.dto.FlowThemeSettingsServiceBean;
import jp.co.blueship.tri.fw.um.domainx.lotsettings.dto.FlowThemeSettingsServiceBean.ThemeSettingsInputInfo;

@Controller
@RequestMapping("/lot/settings")
public class ThemeSettingsController extends TriControllerSupport<FlowThemeSettingsServiceBean> {

	public ServiceId getServiceId() {
		return ServiceId.UmThemeSettingsService;
	}

	@Override
	protected FlowThemeSettingsServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowThemeSettingsServiceBean bean = new FlowThemeSettingsServiceBean();
		return bean;
	}

	@RequestMapping(value = { "", "/", "/theme" })
	public String index(FlowThemeSettingsServiceBean bean, TriModel model) {
		String view = TriTemplateView.LotSettings.value();

		try {
			this.insertMapping(bean, model);
			if (RequestType.init.equals(bean.getParam().getRequestType())) {
				this.execute(getServiceId(), bean, model);
			}
			if (RequestType.submitChanges.equals(bean.getParam().getRequestType())) {
				this.execute(getServiceId(), bean, model);

				if (bean.getResult().isCompleted()) {
					Integer windowId = bean.getHeader().getWindowsId();

					model.getSessionInfo().setAttribute(this.getSessionKey(windowId, TriSessionAttributes.SelectedLotThemeColor.value()),
							ThemeColor.id(bean.getResult().getLotThemeColor().id()).value());
					model.getSessionInfo().setAttribute(this.getSessionKey(windowId, TriSessionAttributes.SelectedLotIconPath.value()),
							bean.getResult().getLotIconPath());
					this.setHeader(bean, model.getSessionInfo(), windowId);
				}
			}

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.LotSettingsThemes.value())
			.addAttribute("selectedSideMenu", "theme")
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}

	/**
	 *
	 * @param bean
	 * @param model
	 * @return
	 */
	private void insertMapping(FlowThemeSettingsServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();
		ISessionInfo sesInfo = model.getSessionInfo();

		bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
		if (RequestType.submitChanges.value().equals(requestInfo.getParameter("RequestType"))) {
			ThemeSettingsInputInfo inputInfo = bean.getParam().getInputInfo();
			inputInfo.setThemeColor(ThemeColor.value(requestInfo.getParameter("themeColor")));
			inputInfo.setIconOption(IconSelectionOption.value(requestInfo.getParameter("iconOption")));
			if (inputInfo.getIconOption() == IconSelectionOption.DefaultImage) {
				inputInfo.setIconPath(requestInfo.getParameter("iconPath"));
			} else if (inputInfo.getIconOption() == IconSelectionOption.CustomImage) {
				inputInfo.setIconFilePath(requestInfo.getParameter("iconPath"));
			}
		}
	}

	/**
	 *
	 * @param customIconFile
	 * @param bean
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/theme/upload", method = RequestMethod.POST)
	public @ResponseBody String uploadIcon(@RequestParam("icon") MultipartFile customIconFile,
			FlowThemeSettingsServiceBean bean, TriModel model) {
		String imagePath = "";
		try {
			this.uploadCustomIconMapping(customIconFile, bean, model);
			this.execute(getServiceId(), bean, model);
			imagePath = bean.getParam().getInputInfo().getIconFilePath();
		} catch (Exception e) {
			ExceptionUtils.printStackTrace(e);
			imagePath = "fail";
		}

		return imagePath;
	}

	/**
	 *
	 * @param customIconFile
	 * @param bean
	 * @param model
	 * @throws IOException
	 */
	private void uploadCustomIconMapping(MultipartFile customIconFile, FlowThemeSettingsServiceBean bean,
			TriModel model) throws IOException {
		ThemeSettingsInputInfo userInfo = bean.getParam().getInputInfo();

		byte[] iconBytes = new byte[] {};
		if (!customIconFile.isEmpty()) {
			iconBytes = customIconFile.getBytes();
			userInfo.setIconOption(IconSelectionOption.CustomImage);
			userInfo.setIconInputStreamBytes(iconBytes);
			userInfo.setIconFileNm(customIconFile.getOriginalFilename());
		}
	}
}
