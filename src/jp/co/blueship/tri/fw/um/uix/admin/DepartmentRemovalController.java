package jp.co.blueship.tri.fw.um.uix.admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowDepartmentRemovalServiceBean;

@Controller
@RequestMapping("/admin/department")
public class DepartmentRemovalController extends TriControllerSupport<FlowDepartmentRemovalServiceBean>{
	@Override
	public ServiceId getServiceId() {
		return ServiceId.UmDepartmentRemovalService;
	}

	@Override
	protected FlowDepartmentRemovalServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowDepartmentRemovalServiceBean bean = new FlowDepartmentRemovalServiceBean();
		return bean;
	}

	@RequestMapping(value = "/remove")
	public String removal(FlowDepartmentRemovalServiceBean bean, TriModel model) {
		String view = "redirect:/admin/department/redirect";

		try {
			this.removalMapping(bean,model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getRedirectAttributes().addFlashAttribute( "result", bean );
		return view;
	}

	private void removalMapping(FlowDepartmentRemovalServiceBean bean, TriModel model ) {

		IRequestInfo requestInfo = model.getRequestInfo();
		String deletedDepartmentId = requestInfo.getParameter("deletedDepartmentId");

		bean.getParam().setRequestType(RequestType.submitChanges);
		bean.getParam().setSelectedDeptId(deletedDepartmentId);
	}
}
