package jp.co.blueship.tri.fw.um.uix.cmn;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.domainx.wiki.dto.FlowRecentlyViewedWikiServiceBean;


@Controller
public class RecentlyViewedWikiController extends TriControllerSupport<FlowRecentlyViewedWikiServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.UmRecentlyViewedWikiService;
	}

	@Override
	protected FlowRecentlyViewedWikiServiceBean getServiceBean(ISessionInfo sesInfo) {
		return new FlowRecentlyViewedWikiServiceBean();
	}

	@RequestMapping("/project/recently/wiki")
	public String list(FlowRecentlyViewedWikiServiceBean bean, TriModel model){
		this.mapping(bean, model);
		this.execute(this.getServiceId(), bean, model);
		model.getModel().addAttribute("result", bean);
		model.getModel().addAttribute("pagination", bean.getPage());
		
		return TriView.GlobalHeaderViewWiki.value() + "::GlobalHeader_RecentlyViewedWiki";
	}

	private void mapping(FlowRecentlyViewedWikiServiceBean bean, TriModel model) {
		FlowRecentlyViewedWikiServiceBean.RequestParam param = bean.getParam();
		IRequestInfo requestInfo = model.getRequestInfo();
		
		param.getSearchCondition().setSelectedPageNo(1);
		
		if(RequestType.onChange.equals(param.getRequestType())) {
			param.getSearchCondition().setKeyword(requestInfo.getParameter("keyword"));

			String selectedPageNo = requestInfo.getParameter("selectedPageNo"); 
			if (null != selectedPageNo) {
				param.getSearchCondition().setSelectedPageNo(Integer.parseInt(selectedPageNo));
			}
		}
	}
}
