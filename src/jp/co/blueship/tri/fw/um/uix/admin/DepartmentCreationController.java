package jp.co.blueship.tri.fw.um.uix.admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.domainx.admin.beans.dto.DepartmentEditInputBean;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowDepartmentCreationServiceBean;

@Controller
@RequestMapping("/admin/department")
public class DepartmentCreationController extends TriControllerSupport<FlowDepartmentCreationServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.UmDepartmentCreationService;
	}

	@Override
	protected FlowDepartmentCreationServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowDepartmentCreationServiceBean bean = new FlowDepartmentCreationServiceBean();
		return bean;
	}

	@RequestMapping(value = "/create")
    public String insert(
    		FlowDepartmentCreationServiceBean bean,
            TriModel model) throws Exception {
		String view = TriTemplateView.AdminSettings.value();

		try {
			this.insertMapping(bean, model);
			this.execute(getServiceId(), bean, model);
			if (bean.getResult().isCompleted()) {
				model.getRedirectAttributes().addFlashAttribute("result", bean);
				view = "redirect:/admin/department/redirect";
			}

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel().addAttribute("view", 	   TriView.DepartmentCreation.value());
		model.getModel().addAttribute("selectedMenu"  , "departList");
		model.getModel().addAttribute("result"   , bean);

		setPrev(model);
		return view;
	}

	private void insertMapping(FlowDepartmentCreationServiceBean bean, TriModel model) throws Exception {
		IRequestInfo requestInfo = model.getRequestInfo();
		DepartmentEditInputBean info = bean.getParam().getInputInfo();
		if (RequestType.submitChanges.equals(bean.getParam().getRequestType()) ) {
			info.setDeptNm(requestInfo.getParameter("deptNm"))
			.setUserIds(requestInfo.getParameterValues("userIds"));
		}
	}
}
