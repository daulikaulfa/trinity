package jp.co.blueship.tri.fw.um.uix.admin;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.domainx.admin.beans.dto.RoleListServiceSortOrderEditBean;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowRoleListServiceBean;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 */
@Controller
@RequestMapping("/admin/role")
public class RoleListController extends TriControllerSupport<FlowRoleListServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.UmRoleListService;
	}

	@Override
	protected FlowRoleListServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowRoleListServiceBean bean = new FlowRoleListServiceBean();
		return bean;
	}

	@RequestMapping(value = "/redirect")
	public String redirect( FlowRoleListServiceBean bean, TriModel model, HttpServletResponse response ){
		return index( bean , model.setRedirect(true) );
	}

	@RequestMapping(value = "/refresh")
	public String refresh(FlowRoleListServiceBean bean, TriModel model){
		String view = TriTemplateView.AdminSettings.value();

		try {
			bean.getParam().setRequestType(RequestType.submitChanges);
			this.execute(getServiceId(), bean, model.setRedirect(true) );

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.RoleList.value())
			.addAttribute("selectedMenu"  ,"roleList")
			.addAttribute("result"   , bean)
		;

		setPrev(model);
		return view;
	}


	@RequestMapping(value = {"","/"})
	public String index( FlowRoleListServiceBean bean, TriModel model) {
		String view = TriTemplateView.AdminSettings.value();
		try {

			this.execute(getServiceId(), bean, model );

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.RoleList.value())
			.addAttribute("selectedMenu"  ,"roleList")
			.addAttribute("result"   , bean)
		;

		setPrev(model);
		return view;
	}

	@RequestMapping(value = "/changesort")
	public String changeSortOrder(FlowRoleListServiceBean bean,
			TriModel model) {
		String view = TriView.RoleList.value() + "::roleList";
		try {

			this.changeSortOrderMapping(bean, model);
			this.execute(getServiceId(), bean , model );

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel().addAttribute("result", bean);
		return view;
	}

	private void changeSortOrderMapping(FlowRoleListServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();

		FlowRoleListServiceBean.RequestParam param = bean.getParam();
		param.setRequestType(RequestType.submitChanges);
		RoleListServiceSortOrderEditBean sortOrderBean = param.getSortOrderEdit();

		sortOrderBean.setRoleId(requestInfo.getParameter("selectedRoleId"))
					.setNewSortOrder(Integer.parseInt(requestInfo.getParameter("newSortOrder")));
	}

}
