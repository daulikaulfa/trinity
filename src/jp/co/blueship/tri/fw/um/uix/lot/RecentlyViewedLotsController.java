package jp.co.blueship.tri.fw.um.uix.lot;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.am.domainx.lot.dto.FlowDashboardServiceLotListBean;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.domainx.dashboard.dto.FlowDashboardServiceBean;

/**
 * @version V4.00.00
 * @author Chung
 */
@Controller
public class RecentlyViewedLotsController extends TriControllerSupport<FlowDashboardServiceBean> {
	private static int LINES_PER_PAGE_FOR_RECENTLY_VIEW_LOT = 8;

	@Override
	public ServiceId getServiceId() {
		return ServiceId.UmRecentlyViewedLotsService;
	}

	@Override
	protected FlowDashboardServiceBean getServiceBean(ISessionInfo sesInfo) {
		return new FlowDashboardServiceBean();
	}

	@RequestMapping("/project/recently/lots")
	public String lotList(FlowDashboardServiceBean bean, TriModel model) {

		IRequestInfo requestInfo = model.getRequestInfo();

		if( bean.getLotListBean() == null ){
			bean.setLotListBean(new FlowDashboardServiceLotListBean());
		}

		bean.getLotListBean().getParam().setIncrementalCondition(requestInfo.getParameter("lotSearch"));

		FlowDashboardServiceLotListBean.SearchCondition searchCond = bean.getLotListBean().getParam().getSearchCondition();

		searchCond.setSelectedLotInProgress(true);
		searchCond.setSelectedLotClosed(false);

		FlowDashboardServiceLotListBean.RequestParam param = bean.getLotListBean().getParam();

		if (TriStringUtils.isNotEmpty(requestInfo.getParameter("selectedPageNo"))) {
			param.getSearchCondition().setSelectedPageNo(Integer.parseInt(requestInfo.getParameter("selectedPageNo")));
		} else {
			param.getSearchCondition().setSelectedPageNo(1);
		}

		bean.getLotListBean().getParam().setLinesPerPage(LINES_PER_PAGE_FOR_RECENTLY_VIEW_LOT);

		bean.getLotListBean().getParam().setSearchCondition(searchCond);

		this.execute(getServiceId(), bean, model);

		model.getModel().addAttribute("result", bean);

		model.getModel().addAttribute("pagination", bean.getLotListBean().getPage());
		
		return TriView.GlobalHeaderViewLot.value() + "::GlobalHeader_RecentlyViewedLots";
	}
}