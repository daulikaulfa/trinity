package jp.co.blueship.tri.fw.um.uix.wiki;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.um.domainx.wiki.dto.FlowWikiRemovalServiceBean;

@Controller
@RequestMapping("/lot/wiki/remove")
public class WikiRemovalController extends TriControllerSupport<FlowWikiRemovalServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.UmWikiRemovalService;
	}

	@Override
	protected FlowWikiRemovalServiceBean getServiceBean(ISessionInfo sesInfo) {
		return new FlowWikiRemovalServiceBean();
	}

	@RequestMapping
	public String removal(FlowWikiRemovalServiceBean bean, TriModel model){
		try {
			this.mapping(bean, model);
			this.execute(this.getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, "") ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, "");
			}
		}

		if( bean.getResult().isCompleted() ){
			model.getRedirectAttributes().addFlashAttribute( "result", bean );
			bean.getParam().setRequestType(RequestType.init);
			return "redirect:/lot/wiki/details/redirect";
		}

		return "redirect:" + getPrevPath(model, bean.getHeader().getWindowsId());
	}

	private void mapping(FlowWikiRemovalServiceBean bean, TriModel model){
		IRequestInfo requestInfo = model.getRequestInfo();
		bean.getParam().setSelectedWikiId(requestInfo.getParameter("wikiId"));
	}
}
