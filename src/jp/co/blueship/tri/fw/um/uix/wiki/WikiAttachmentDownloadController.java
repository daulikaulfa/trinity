package jp.co.blueship.tri.fw.um.uix.wiki;

import javax.servlet.http.HttpServletResponse;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.fw.cmn.io.StreamUtils;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.um.domainx.wiki.dto.FlowWikiAttachmentDownloadServiceBean;

/**
 *
 * @version V4.00.00
 * @author Sharov.Maksym
 *
 */
@Controller
@RequestMapping("/lot/wiki/download")
public class WikiAttachmentDownloadController extends TriControllerSupport<FlowWikiAttachmentDownloadServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.UmWikiAttachmentDownloadService;
	}

	@Override
	protected FlowWikiAttachmentDownloadServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowWikiAttachmentDownloadServiceBean bean = new FlowWikiAttachmentDownloadServiceBean();
		return bean;
	}

	@RequestMapping("/validate")
	public String validate(FlowWikiAttachmentDownloadServiceBean bean, TriModel model) {
		String view = TriView.Messages.value() + "::message";
		try {
			bean.getParam().setRequestType(RequestType.validate);
			this.execute(getServiceId(), bean, model);
		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}
		model.getModel().addAttribute("result", bean);
		return view;
	}

	@RequestMapping
	public void download(HttpServletResponse response,
			FlowWikiAttachmentDownloadServiceBean bean,
						TriModel model) {
		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);
			if (bean.getResult().isCompleted()) {
				StreamUtils.download(response, bean.getFileResponse().getFile());
			}

		} catch (Exception e) {
			ExceptionUtils.printStackTrace(e);
		}

		model.getModel().addAttribute("result", bean);
	}

	private void mapping (FlowWikiAttachmentDownloadServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();
		bean.getParam().setSelectedWikiId	( requestInfo.getParameter("wikiId") );
		bean.getParam().setSeqNo			( requestInfo.getParameter("seqNo") );
		bean.getParam().setSelectedFileNm	( requestInfo.getParameter("fileName") );
	}
}
