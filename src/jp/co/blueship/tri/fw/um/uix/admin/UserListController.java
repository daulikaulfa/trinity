package jp.co.blueship.tri.fw.um.uix.admin;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowUserListServiceBean;

@Controller
@RequestMapping("/admin/user")
public class UserListController extends TriControllerSupport<FlowUserListServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.UmUserListService;
	}

	@Override
	protected FlowUserListServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowUserListServiceBean bean = new FlowUserListServiceBean();
		return bean;
	}

	@RequestMapping(value = "/redirect")
	public String redirect(FlowUserListServiceBean bean, TriModel model, HttpServletResponse response ){
		return listView( bean, model.setRedirect(true) );
	}

	@RequestMapping(value = {"", "/"})
	public String listView(FlowUserListServiceBean bean, TriModel model) {
		String view = TriTemplateView.AdminSettings.value();

		try {
			bean.getParam().setRequestType(RequestType.init);
			this.searchMapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.UserList.value())
			.addAttribute("selectedMenu", "userList")
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}

	@RequestMapping("/search")
	public String search(FlowUserListServiceBean bean, TriModel model) {
		String view = TriView.UserList.value() + "::userList";

		try {
			bean.getParam().setRequestType(RequestType.onChange);
			this.searchMapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel().addAttribute("result", bean);
		return view;
	}

	private void searchMapping( FlowUserListServiceBean bean, TriModel model) {

		IRequestInfo requestInfo = model.getRequestInfo();
		String keyword = requestInfo.getParameter("keyword");
		String roleId  = requestInfo.getParameter("roleId");
		String groupId = requestInfo.getParameter("groupId");
		String departmentId = requestInfo.getParameter("departmentId");

		bean.getParam().getSearchCondition().setKeyword(keyword)
											.setRoleId(roleId)
											.setGroupId(groupId)
											.setDeptId(departmentId);

	}
}
