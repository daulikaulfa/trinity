package jp.co.blueship.tri.fw.um.uix.lotsettings;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.domainx.lotsettings.beans.dto.MilestoneListServiceSortOrderEditBean;
import jp.co.blueship.tri.fw.um.domainx.lotsettings.dto.FlowMilestoneListServiceBean;

@Controller
@RequestMapping("/lot/settings/milestone")
public class MilestoneListController extends TriControllerSupport<FlowMilestoneListServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.UmMilestoneListService;
	}

	@Override
	protected FlowMilestoneListServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowMilestoneListServiceBean bean = new FlowMilestoneListServiceBean();
		return bean;
	}



	@RequestMapping(value = "/redirect")
	public String redirect( FlowMilestoneListServiceBean bean, TriModel model ){
		return listView( bean , model.setRedirect(true) );
	}

	@RequestMapping(value = "/refresh")
	public String refresh(FlowMilestoneListServiceBean bean, TriModel model){
		String view = TriTemplateView.LotSettings.value();

		try{
			bean.getParam().setRequestType(RequestType.submitChanges);
			this.execute(getServiceId(), bean , model.setRedirect(true) );

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.MilestoneList.value())
			.addAttribute("selectedSideMenu"  ,"milestone")
			.addAttribute("result"   , bean)
		;

		setPrev(model);
		return view;
	}

	@RequestMapping(value = {"","/"})
	public String listView(FlowMilestoneListServiceBean bean,TriModel model) {
		String view = TriTemplateView.LotSettings.value();

		try {
			ISessionInfo sesInfo = model.getSessionInfo();
			bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel().addAttribute("view", TriView.MilestoneList.value());
		model.getModel().addAttribute("selectedSideMenu"  , "milestone");
		model.getModel().addAttribute("result"   , bean);
		setPrev(model);
		return view;
	}

	@RequestMapping(value = "/changesort")
	public String changeSortOrder(FlowMilestoneListServiceBean bean,
			TriModel model) {
		String view = TriView.MilestoneList.value() + "::milestoneList";

		try {
			ISessionInfo sesInfo = model.getSessionInfo();
			bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
			this.changeSortOrderMapping(bean, model);
			this.execute(getServiceId(), bean , model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel().addAttribute("result", bean);
		return view;
	}

	private void changeSortOrderMapping(FlowMilestoneListServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();

		FlowMilestoneListServiceBean.RequestParam param = bean.getParam();
		param.setRequestType(RequestType.onChange);
		MilestoneListServiceSortOrderEditBean sortOrderBean = param.getSortOrderEdit();

		sortOrderBean.setMstoneId(requestInfo.getParameter("selectedMilestoneId"))
					.setNewSortOrder(Integer.parseInt(requestInfo.getParameter("newSortOrder")));
	}
}
