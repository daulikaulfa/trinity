package jp.co.blueship.tri.fw.um.uix.admin;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.domainx.admin.beans.dto.DepartmentListServiceSortOrderEditBean;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowDepartmentListServiceBean;

@Controller
@RequestMapping("/admin/department")
public class DepartmentListController extends TriControllerSupport<FlowDepartmentListServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.UmDepartmentListService;
	}

	@Override
	protected FlowDepartmentListServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowDepartmentListServiceBean bean = new FlowDepartmentListServiceBean();
		return bean;
	}

	@RequestMapping(value = "/redirect")
	public String redirect( FlowDepartmentListServiceBean bean, TriModel model, HttpServletResponse response ){
		return listView( bean , model.setRedirect(true) );
	}

	@RequestMapping(value = {"", "/"})
	public String listView(FlowDepartmentListServiceBean bean, TriModel model) {
		String view = TriTemplateView.AdminSettings.value();

		try {
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel().addAttribute("view", TriView.DepartmentList.value());
		model.getModel().addAttribute("selectedMenu", "departList");
		model.getModel().addAttribute("result", bean);
		setPrev(model);
		return view;
	}

	@RequestMapping(value="/changesort")
	public String changeSortOrder(FlowDepartmentListServiceBean bean, TriModel model) {
		String view = TriView.DepartmentList.value() + "::departmentList";

		try {
			this.changeSortOrderMapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel().addAttribute("result", bean);
		return view;
	}

	private void changeSortOrderMapping(FlowDepartmentListServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();

		FlowDepartmentListServiceBean.RequestParam param = bean.getParam();
		param.setRequestType(RequestType.submitChanges);
		DepartmentListServiceSortOrderEditBean sortOrderBean = param.getSortOrderEdit();

		sortOrderBean.setDeptId(requestInfo.getParameter("selectedDepartmentId"))
					 .setNewSortOrder(Integer.parseInt(requestInfo.getParameter("newSortOrder")));
	}
}