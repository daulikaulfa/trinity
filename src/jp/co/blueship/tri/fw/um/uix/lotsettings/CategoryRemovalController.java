package jp.co.blueship.tri.fw.um.uix.lotsettings;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.um.domainx.lotsettings.dto.FlowCategoryRemovalServiceBean;

@Controller
@RequestMapping("/lot/settings/category")
public class CategoryRemovalController extends TriControllerSupport<FlowCategoryRemovalServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.UmCategoryRemovalService;
	}

	@Override
	protected FlowCategoryRemovalServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowCategoryRemovalServiceBean bean = new FlowCategoryRemovalServiceBean();
		return bean;
	}


	@RequestMapping(value = "/remove")
	public String delete(FlowCategoryRemovalServiceBean bean,
			TriModel model) {

		String view = "redirect:/lot/settings/category/refresh";

		try {
			bean.getParam().setRequestType(RequestType.submitChanges);
			this.mapping(bean, model);
			this.execute(getServiceId(), bean , model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getRedirectAttributes().addFlashAttribute( "result", bean );

		return view;
	}


	private void mapping(FlowCategoryRemovalServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();
		FlowCategoryRemovalServiceBean.RequestParam param = bean.getParam();
		if ( requestInfo.getParameter("ctgId") != null )
		param.setSelectedCategoryId(requestInfo.getParameter("ctgId"));
	}
}
