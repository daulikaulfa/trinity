package jp.co.blueship.tri.fw.um.uix.dashboard;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckInOutRequestListServiceBean;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckInOutRequestListServiceBean.SearchCondition;
import jp.co.blueship.tri.am.domainx.lot.dto.FlowDashboardServiceLotListBean;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.domainx.dashboard.beans.dto.FlowDashboardServiceNotificationToUserBean;
import jp.co.blueship.tri.fw.um.domainx.dashboard.beans.dto.FlowDashboardServiceRecentUpdatesBean;
import jp.co.blueship.tri.fw.um.domainx.dashboard.beans.dto.FlowDashboardServiceRecentUpdatesBean.RequestOption;
import jp.co.blueship.tri.fw.um.domainx.dashboard.dto.FlowDashboardServiceBean;

/**
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
@Controller
@RequestMapping("/dashboard")
public class DashboardController extends TriControllerSupport<FlowDashboardServiceBean> {
	private static int LINES_PER_PAGE_FOR_REQUEST = 5;

	@Override
	public ServiceId getServiceId() {
		return ServiceId.UmDashBoardService;
	}

	@Override
	protected FlowDashboardServiceBean getServiceBean(ISessionInfo sesInfo) {
		return new FlowDashboardServiceBean();
	}

	@RequestMapping(value = "/error")
	public String redirect() {
		return TriTemplateView.ProjectTemplate.value();
	}

	@RequestMapping(value = "/redirect")
	public String redirect(FlowDashboardServiceBean bean, TriModel model) {
		model.setRedirect( true );

		String view = TriTemplateView.ProjectTemplate.value();

		try {
			bean.setLotListBean(new FlowDashboardServiceLotListBean());
			bean.setCheckInOutRequestListBean(new FlowCheckInOutRequestListServiceBean());
			bean.getCheckInOutRequestListBean().getParam().setUserId( this.getUserId() )
														  .setLinesPerPage(LINES_PER_PAGE_FOR_REQUEST)
														  ;

			bean.setNotificationToUser( new FlowDashboardServiceNotificationToUserBean() );
			bean.getNotificationToUser().getParam().setLinesPerPage(LINES_PER_PAGE_FOR_REQUEST);

			bean.setRecentUpdates(new FlowDashboardServiceRecentUpdatesBean());
			bean.getRecentUpdates().getParam().setLinesPerPage(LINES_PER_PAGE_FOR_REQUEST)
											  .setRequestOption(RequestOption.none)
											  ;
			bean.getParam().setRequestType(RequestType.init);

			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		} finally {
		}

		model.getModel()
			.addAttribute("view", TriView.DashBoard.value())
			.addAttribute("result", bean);
		setPrev(model);

		return TriTemplateView.ProjectTemplate.value();
	}

	@RequestMapping
	public String index(FlowDashboardServiceBean bean, TriModel model) {
		try {
			bean.setLotListBean(new FlowDashboardServiceLotListBean());
			bean.setCheckInOutRequestListBean(new FlowCheckInOutRequestListServiceBean());
			bean.getCheckInOutRequestListBean().getParam().setUserId( this.getUserId() )
														  .setLinesPerPage(LINES_PER_PAGE_FOR_REQUEST);

			bean.setNotificationToUser(new FlowDashboardServiceNotificationToUserBean());
			bean.getNotificationToUser().getParam().setLinesPerPage(LINES_PER_PAGE_FOR_REQUEST);

			bean.setRecentUpdates(new FlowDashboardServiceRecentUpdatesBean());
			bean.getRecentUpdates().getParam().setLinesPerPage(LINES_PER_PAGE_FOR_REQUEST);
			bean.getRecentUpdates().getParam().setRequestOption(RequestOption.none);

			this.execute(getServiceId(), bean, model);

		} catch (BaseBusinessException bbe) {
			if (bean.isAuthError()) {
				model.getRedirectAttributes().addFlashAttribute("result", bean);
				return "redirect:/login/error";
			}
		}

		model.getModel()
			.addAttribute("view", TriView.DashBoard.value())
			.addAttribute("result", bean);

		setPrev(model);

		return TriTemplateView.ProjectTemplate.value();
	}

	@RequestMapping("/lots")
	public String lotList(FlowDashboardServiceBean bean, TriModel model) {

		IRequestInfo requestInfo = model.getRequestInfo();

		if( bean.getLotListBean() == null ){
			bean.setLotListBean(new FlowDashboardServiceLotListBean());
		}

		bean.getLotListBean().getParam()
				.setIncrementalCondition(requestInfo.getParameter("lotSearch"))
				.setShowAll(Boolean.parseBoolean(requestInfo.getParameter("showAllByLot")));

		FlowDashboardServiceLotListBean.SearchCondition searchCond =
				bean.getLotListBean().getParam().getSearchCondition();

		int lotStatus = Integer.parseInt(requestInfo.getParameter("lotStatus"));

		switch (lotStatus) {
		case 0:
			searchCond.setSelectedLotInProgress(true);
			searchCond.setSelectedLotClosed(false);
			break;
		case 1:
			searchCond.setSelectedLotInProgress(false);
			searchCond.setSelectedLotClosed(true);
			break;
		default:
			searchCond.setSelectedLotInProgress(true);
			searchCond.setSelectedLotClosed(false);
		}

		bean.getLotListBean().getParam().setSearchCondition(searchCond);

		this.execute(getServiceId(), bean, model);

		model.getModel().addAttribute("result", bean);

		model.getModel().addAttribute("view", TriView.DashBoard.value());

		return TriTemplateView.ProjectTemplate.value();
	}

	@RequestMapping("/requests")
	public String requestList(FlowDashboardServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();

		if( bean.getCheckInOutRequestListBean() == null ){
			bean.setCheckInOutRequestListBean(new FlowCheckInOutRequestListServiceBean());
		}

		SearchCondition searchCondition = bean.getCheckInOutRequestListBean().getParam().getSearchCondition();

		bean.getCheckInOutRequestListBean().getParam()
				.setUserId		( this.getUserId() )
				.setShowAll		( Boolean.parseBoolean(requestInfo.getParameter("showAllByRequest")) )
				.setLinesPerPage( LINES_PER_PAGE_FOR_REQUEST )
				;

		searchCondition.setKeyword(requestInfo.getParameter("requestSearch"));

		FlowCheckInOutRequestListServiceBean.OrderBy orderBy = new FlowCheckInOutRequestListServiceBean().new OrderBy();
		orderBy.setAccsTimestamp(null);

		String sortKey = requestInfo.getParameter("sortKey");
		Boolean isAsc = Boolean.parseBoolean(requestInfo.getParameter("asc"));
		if (TriStringUtils.equals(sortKey, "prj")) {
			if (isAsc) {
				orderBy.setPjtId(TriSortOrder.Asc);
			} else {
				orderBy.setPjtId(TriSortOrder.Desc);
			}
		} else if (TriStringUtils.equals(sortKey, "areq")) {
			if (isAsc) {
				orderBy.setAreqId(TriSortOrder.Asc);
			} else {
				orderBy.setAreqId(TriSortOrder.Desc);
			}
		} else if (TriStringUtils.equals(sortKey, "status")) {
			if (isAsc) {
				orderBy.setStatus(TriSortOrder.Asc);
			} else {
				orderBy.setStatus(TriSortOrder.Desc);
			}
		}
		bean.getCheckInOutRequestListBean().getParam().setOrderBy(orderBy);

		this.execute(getServiceId(), bean, model);

		model.getModel().addAttribute("result", bean);

		model.getModel().addAttribute("view", TriView.DashBoard.value());

		return TriTemplateView.ProjectTemplate.value();
	}

	@RequestMapping("/notifications")
	public String notificationToUser(FlowDashboardServiceBean bean, TriModel model) {

		if(bean.getNotificationToUser() == null){
			bean.setNotificationToUser(new FlowDashboardServiceNotificationToUserBean());
			bean.getNotificationToUser().getParam().setLinesPerPage(LINES_PER_PAGE_FOR_REQUEST);

		}else {
			bean.getNotificationToUser().getParam().setRequestOption( FlowDashboardServiceNotificationToUserBean.RequestOption.value(model.getRequestInfo().getParameter("requestOption")));
		}

		this.execute(getServiceId(), bean, model);

		model.getModel()
			.addAttribute("view", TriView.DashBoard.value())
			.addAttribute("result", bean);

		return TriTemplateView.ProjectTemplate.value();
	}

	@RequestMapping("/recentupdates")
	public String recentUpdates(FlowDashboardServiceBean bean, TriModel model) {

		if(bean.getRecentUpdates() == null ){
			bean.setRecentUpdates(new FlowDashboardServiceRecentUpdatesBean());
			bean.getRecentUpdates().getParam().setLinesPerPage(LINES_PER_PAGE_FOR_REQUEST);
			bean.getRecentUpdates().getParam().setRequestOption(RequestOption.none);

		}else{
			bean.getRecentUpdates().getParam().setRequestOption(RequestOption.value(model.getRequestInfo().getParameter("requestOption")));
		}

		this.execute(getServiceId(), bean, model);

		model.getModel()
			.addAttribute("view", TriView.DashBoard.value())
			.addAttribute("result", bean);

		return TriTemplateView.ProjectTemplate.value();
	}
}
