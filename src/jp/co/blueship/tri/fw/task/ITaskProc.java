package jp.co.blueship.tri.fw.task;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskPropertyEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskResultTypeEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTargetEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTaskTypeEntity;
import jp.co.blueship.tri.fw.dao.orm.IEntity;

/**
 * 業務シーケンスのタスク部品の処理内容を記述するためのインターフェイス<br>
 *
 */
public interface ITaskProc extends IEntity {

	/**
	 * プロパティを設定します
	 *
	 * @param value プロパティ
	 */
	public void setProperty( ITaskPropertyEntity[] object ) ;
	/**
	 * 業務シーケンスのタスク部品の処理を実行します。
	 *
	 *
	 * @param prmTarget 業務シーケンスのターゲットパラメタ
	 * @param prmTask 業務シーケンスのタスクパラメタ
	 * @return 業務シーケンスの処理結果エンティティ
	 */
	public ITaskResultTypeEntity execute(
					ITaskTargetEntity prmTarget,
					ITaskTaskTypeEntity prmTask ) throws Exception;

	/**
	 * パス文字列中の代用キー{@hoge}を置換する
	 * @param task タスク処理内容を保持したエンティティ
	 * @throws Exception
	 */
	public void substitutePath( ITaskTaskTypeEntity task ) throws Exception ;
}
