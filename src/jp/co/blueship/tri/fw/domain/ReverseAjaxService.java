package jp.co.blueship.tri.fw.domain;

import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.security.InvalidActionException;
import jp.co.blueship.tri.fw.security.InvalidUserException;
import jp.co.blueship.tri.fw.security.SecurityService;


/**
 *
 * @author kanda
 *
 */
public class ReverseAjaxService extends GenericServiceAbstract implements IReverseAjaxService<IGeneralServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private SecurityService securityService = null;

	/**
	 * Springからセキュリティサービスのインスタンスを設定します。
	 * @param securityService SecurityServiceのインスタンス
	 */
	public void setSecurityService(SecurityService securityService) {
		this.securityService = securityService;
	}

	public GenericService getApplicationService(
			String targetService,
			IServiceDto<IGeneralServiceBean> serviceDto) throws Exception {

		IGeneralServiceBean gsBean = serviceDto.getServiceBean();
		gsBean.setFlowAction( targetService );
		gsBean.setSystemDate( TriDateUtils.getSystemTimestamp() );

		try{
			this.beforeAdvices( serviceDto );

			this.validAccess(gsBean, targetService);

			GenericService service = (GenericService)this.getContext().getBean( targetService );
			((IService)service).init();

			return service;

		}catch (BaseBusinessException e) {
			//同期処理では、業務メールを送付しない
			//gsBean.setBusinessThrowable( e );

			LogHandler.error( log , e ) ;
			throw e;
		}catch (Exception e) {
			gsBean.setSystemThrowable( e );

			LogHandler.fatal( log , e ) ;
			throw e;
		}finally{
			try {
				this.afterAdvices( serviceDto );
			}  catch ( Exception e ) {
				throw e;
			}
		}
	}

	@Deprecated public IServiceDto<IGeneralServiceBean> execute(
			String targetService,
			IServiceDto<IGeneralServiceBean> serviceDto ) throws Exception {

		IGeneralServiceBean gsBean = serviceDto.getServiceBean();
	gsBean.setFlowAction( targetService );
	gsBean.setSystemDate( TriDateUtils.getSystemTimestamp() );

	try{
		this.beforeAdvices( serviceDto );

		IGenericTransactionService service = (IGenericTransactionService)this.getContext().getBean( targetService );
		((IService)service).init();

		serviceDto = service.execute(targetService, serviceDto);

		if ( null != this.getPojoList() ) {
			for ( IDomain<IGeneralServiceBean> pojo: this.getPojoList() ) {
				serviceDto = pojo.execute(serviceDto);
			}
		}

		return serviceDto;

		}catch (BaseBusinessException e) {
			//同期処理では、業務メールを送付しない
			//gsBean.setBusinessThrowable( e );

			LogHandler.error( log , e ) ;
			throw e;
		}catch (Exception e) {
			gsBean.setSystemThrowable( e );

			LogHandler.fatal( log , e ) ;
			throw e;
		}finally{
			try {
				this.afterAdvices( serviceDto );
			}  catch ( Exception e ) {
				throw e;
			}
		}
	}

	/**
	 * ターゲットサービスの認可チェックを行います。
	 *
	 * @param gsBean サービスパラメタ情報
	 * @param targetService ターゲットサービスを指定します。
	 *
	 * @see GenericTransactionService#validAccess(GenericServiceBean, String)
	 */
	private void validAccess( IGeneralServiceBean gsBean, String targetService ) {

		if( null != this.securityService ) {

			try {

				this.securityService.validAccess(targetService);

			} catch (InvalidActionException e) {
				throw new BusinessException( SmMessageId.SM004031F , e );
			}

			try {

				this.securityService.validAccess(gsBean.getUserId(), targetService);

			} catch (InvalidActionException ae) {
				throw new BusinessException( SmMessageId.SM004031F , ae );
			} catch (InvalidUserException ue) {
				throw new BusinessException( SmMessageId.SM001000E, ue , gsBean.getUserId(), ue.getActionName() );
			}
		}
	}

}
