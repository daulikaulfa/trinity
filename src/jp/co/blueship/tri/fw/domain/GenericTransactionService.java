package jp.co.blueship.tri.fw.domain;

import jp.co.blueship.tri.fw.cmn.utils.SystemProps;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.log.TriLogMessage;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.security.InvalidActionException;
import jp.co.blueship.tri.fw.security.InvalidUserException;
import jp.co.blueship.tri.fw.security.SecurityService;
import jp.co.blueship.tri.fw.sm.domain.product.ProductActivate;

/**
 * トランザクション制御を行うビジネス処理を一括して処理する汎用クラスです。
 * <br>この実装クラスは、必ずsingleton = falseで実行します。
 *
 * @version V3L10R02
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 */
public class GenericTransactionService extends GenericServiceAbstract implements IGenericTransactionService {

	private static final ILog log = TriLogFactory.getInstance();
	private static final String SEP = SystemProps.LineSeparator.getProperty();

	private String nextTransactionService = null;
	private SecurityService securityService = null;

	/**
	 * 一度のリクエスト処理で、トランザクション制御を行う場合、指定されます。
	 *
	 * @param transactionService 次のトランザクション制御処理があれば、指定する
	 */
	public void setNextTargetService( String transactionService ) {
		this.nextTransactionService = transactionService;
	}

	/**
	 * Springからセキュリティサービスのインスタンスを設定します。
	 * @param securityService SecurityServiceのインスタンス
	 */
	public void setSecurityService(SecurityService securityService) {
		this.securityService = securityService;
	}

	/**
	 * このメソッド内は、SpringAOPにより暗黙的なトランザクション制御が行われます。
	 *
	 * @param targetService ターゲットサービスを指定します。
	 * @param prmBLBean サービスパラメタ情報
	 * @return 処理結果を戻します。
	 * @throws Exception
	 */
	public IServiceDto<IGeneralServiceBean> execute(
				String targetService,
				IServiceDto<IGeneralServiceBean> prmBLBean ) throws Exception {

		IGeneralServiceBean gsBean = prmBLBean.getServiceBean();
		gsBean.setFlowAction( targetService );

		checkActivate(targetService);

		if( null != this.securityService ) {
			if ( this.securityService.isExpired(gsBean.getUserId(), targetService) ) {
				throw new TriSystemException( SmMessageId.SM004215F );
			}
		}

		try{
			this.beforeAdvices( prmBLBean );

			this.validAccess( gsBean, targetService );

			if ( null == this.nextTransactionService ) {
				GenericService service = (GenericService)this.getContext().getBean( targetService );
				((IService)service).init();
				IServiceDto<IGeneralServiceBean> retBean = service.execute(prmBLBean);
				return retBean ;

			} else {
				IGenericTransactionService service = (IGenericTransactionService)this.getContext().getBean( this.nextTransactionService );
				((IService)service).init();

				IServiceDto<IGeneralServiceBean> retBean = service.execute(targetService, prmBLBean);
				return retBean ;
			}

		} catch (BaseBusinessException e) {
			LogHandler.error( log , e ) ;
			throw e;
		} catch (Exception e) {
			LogHandler.fatal( log , e ) ;
			throw e;
		} finally {
			try {
				this.afterAdvices( prmBLBean );
			}  catch ( Exception e ) {
				throw e;
			}
		}
	}

	/**
	 * ターゲットサービスの認可チェックを行います。
	 *
	 * @param gsBean サービスパラメタ情報
	 * @param targetService ターゲットサービスを指定します。
	 */
	private void validAccess( IGeneralServiceBean gsBean, String targetService ) {

		if( null != this.securityService ) {

			//業務エラーが発生した際の呼び返しの場合、認可チェックを行わない
			if ( ScreenType.bussinessException.equals( gsBean.getScreenType() ) ) {
				return;
			}

			try {

				this.securityService.validAccess(targetService);
			} catch (InvalidActionException e) {
				gsBean.setAuthError(true);
				throw new BusinessException( SmMessageId.SM004031F , e );
			}

			try {
				this.securityService.validAccess(gsBean.getUserId(), targetService);
			} catch (InvalidActionException ae) {
				gsBean.setAuthError(true);
				throw new BusinessException( SmMessageId.SM004031F , ae );
			} catch (InvalidUserException ue) {
				gsBean.setAuthError(true);
				throw new BusinessException( SmMessageId.SM001000E , ue , gsBean.getUserId(), ue.getActionName() );
			}
		}
	}

	private void checkActivate(String targetService) {
		if (targetService.startsWith("FlowUcf") || targetService.startsWith("FlowLogin") || targetService.startsWith("FlowLogIn")) {
			return;
		}

		if (!ProductActivate.isRmActive()) {
			LogHandler.info(log, TriLogMessage.LSM0032, SEP + ProductActivate.getMACAddressString());
			throw new BusinessException(SmMessageId.SM001011E);
		}

		if (!(targetService.startsWith("FlowRelDistribute") || targetService.startsWith("FlowDeployment") || targetService.startsWith("FlowPrintDeployment"))) {
			return;
		}

		if (!ProductActivate.isDmActive()) {
			LogHandler.info(log, TriLogMessage.LSM0032, SEP + ProductActivate.getMACAddressString());
			throw new BusinessException(SmMessageId.SM001011E);
		}
	}
}
