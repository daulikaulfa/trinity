/**
 * ビジネス処理を一括して汎用抽象クラスです。
 * <br>この実装クラスは、必ずsingleton = falseで実行します。
 *
 * @author Yukihiro Eguchi
 *
 */
package jp.co.blueship.tri.fw.domain;

import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.SmMessageId;

/**
 * ビジネス処理を一括して実行するクラスです。
 * <br>このクラスは非同期で実行されます。
 * <br>この実装クラスは、必ずsingleton = falseで実行します。
 *
 * @author Yukihiro Eguchi
 *
 */
public class GenericThreadService extends GenericService implements IService, Runnable {

	private static final ILog log = TriLogFactory.getInstance();

	private IServiceDto<IGeneralServiceBean> prmBLBean = null;

	public IServiceDto<IGeneralServiceBean> execute(IServiceDto<IGeneralServiceBean> prmBLBean) throws Exception{

		this.prmBLBean = prmBLBean;

		new Thread( this ).start();

		return prmBLBean;
	}

	public void run() {
		try {
			this.executeThread( this.prmBLBean );

		} catch (Throwable e) {
			e.printStackTrace();

			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( SmMessageId.SM005054S , e );
		}
	}

	private final  IServiceDto<IGeneralServiceBean> executeThread( IServiceDto<IGeneralServiceBean> prmBLBean ) throws Exception{

		try{
			this.beforeAdvices( prmBLBean );

			for ( IDomain<IGeneralServiceBean> pojo: this.getPojoList() ) {
				this.prmBLBean = pojo.execute(prmBLBean);
			}

			return this.prmBLBean;

		}catch (BaseBusinessException e) {
			LogHandler.error( log , e ) ;
			throw e;
		}catch (Exception e) {
			LogHandler.fatal( log , e ) ;
			throw e;
		}finally{
			try {
				this.afterAdvices( prmBLBean );
			}  catch ( Exception e ) {
				throw e;
			}
		}
	}

}
