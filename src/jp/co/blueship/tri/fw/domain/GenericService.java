package jp.co.blueship.tri.fw.domain;

import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;

/**
 * ビジネス処理を一括して汎用抽象クラスです。
 * <br>この実装クラスは、必ずsingleton = falseで実行します。
 *
 * @author Yukihiro Eguchi
 *
 */
public class GenericService extends GenericServiceAbstract {

	private static final ILog log = TriLogFactory.getInstance();

	public IServiceDto<IGeneralServiceBean> execute(
			IServiceDto<IGeneralServiceBean> prmBLBean ) throws Exception{

		try{
			this.beforeAdvices( prmBLBean );

			if ( null != this.getPojoList() ) {

				for ( IDomain<IGeneralServiceBean> pojo: this.getPojoList() ) {
					prmBLBean = pojo.execute( prmBLBean );
				}
			}

			return prmBLBean;

		} catch ( BaseBusinessException e ) {
			LogHandler.error( log , e ) ;
			throw e;
		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			throw e;
		} finally {
			try {
				this.afterAdvices( prmBLBean );
			}  catch ( Exception e ) {
				throw e;
			}

		}
	}

}