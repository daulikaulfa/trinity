package jp.co.blueship.tri.fw.domain;

import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;

public interface IGenericTransactionService {
	/**
	 * このメソッド内は、SpringAOPにより暗黙的なトランザクション制御が行われます。
	 *
	 * @param targetService ターゲットサービスを指定します。
	 * @param serviceDto サービスパラメタ情報
	 * @return 処理結果を戻します。
	 * @throws Exception
	 */
	public IServiceDto<IGeneralServiceBean> execute(
			String targetService,
			IServiceDto<IGeneralServiceBean> serviceDto ) throws Exception;
}
