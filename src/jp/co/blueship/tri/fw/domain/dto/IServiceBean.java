package jp.co.blueship.tri.fw.domain.dto;

/**
 * ドメイン層で利用するBeanの最上位 Interfaceです。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public interface IServiceBean extends java.io.Serializable {

}
