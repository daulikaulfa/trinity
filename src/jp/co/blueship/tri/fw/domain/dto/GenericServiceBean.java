package jp.co.blueship.tri.fw.domain.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

import jp.co.blueship.tri.fw.cmn.utils.SystemProps;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.ServerType;
import jp.co.blueship.tri.fw.dao.orm.constants.IStatusId;
import jp.co.blueship.tri.fw.ex.ITranslatable;
import jp.co.blueship.tri.fw.msg.IMessageId;

/**
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
abstract public class GenericServiceBean implements IGeneralServiceBean {

	/**
	 * デフォルトシリアル
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * It is used for compatibility with V3.
	 */
	private boolean statusMatrixV3 = true;

	private TimeZone timeZone = TriDateUtils.getDefaultTimeZone();
	private String language = SystemProps.TriSystemLanguage.getProperty();
	/** ログインユーザ */
	private String userId = null;
	/** ログインユーザ名称 */
	private String userName = null;
	
	/** プロジェクト名 */
	private String projectName = null;

	/** バージョン */
	private String version = null;
	/** プロセスID */
	private String procId = null;
	/** 遷移フローアクション */
	private String flowAction;
	/** プロセス管理のステータスID */
	private IStatusId procMgtStatusId;
	/** 登録／更新日時 */
	/** 登録／更新日時 */
	private Timestamp systemDate;
	/** 遷移元画面ID */
	private String referer = null;
	/** 遷移先画面ID */
	private String forward = null;
	/** 遷移タイプ */
	private String screenType = null;
	/** Threadによる業務排他を行うかどうか */
	private boolean isLockByThread = false;
	/** 業務排他中かどうか */
	private boolean isLock = false;
	/** 採番 */
	private String numberingVal;

	private IMessageId infoMessageId;
	private String[] infoMessageArgs = new String[]{"", ""};

	private List<IMessageId> infoMessageIdList;
	private List<String[]> infoMessageArgsList = null;

	private List<IMessageId> infoCommentIdList;
	private List<String[]> infoCommentArgsList = null;

	private ITranslatable businessThrowable = null;
	private Throwable systemThrowable = null;

	private List<String> messages = new ArrayList<String>();

	private List<Object> paramList = new ArrayList<Object>();

	/** 排他用ロット番号 */
	private String lockLotNo = null;
	private String[] lockLotNoArray = null;
	/** 排他用サーバ番号 */
	private String lockServerId = null;

	private boolean authError = false;

	public boolean isStatusMatrixV3() {
		return statusMatrixV3;
	}
	public void setStatusMatrixV3(boolean statusMatrixV3) {
		this.statusMatrixV3 = statusMatrixV3;
	}
	
	@Override
	public TimeZone getTimeZone() {
		return timeZone;
	}
	@Override
	public void setTimeZone(TimeZone zone) {
		this.timeZone = zone;
	}
	@Override
	public String getLanguage() {
		return language;
	}
	@Override
	public void setLanguage(String language) {
		this.language = language;
	}
	public final String getUserId() {
		return userId;
	}
	@Override
	public final void setUserId(String userId) {
		this.userId = userId;
	}

	@Override
	public final String getUserName() {
		return userName;
	}
	@Override
	public final void setUserName(String userName) {
		this.userName = userName;
	}

	@Override
	public boolean isEmptyMessages() {
		if ( null == messages || 0 == messages.size() ) {
			return true;
		}

		return false;
	}

	@Override
	public void setMessages( List<String> messages ) {
		this.messages = messages;
	}

	@Override
	public List<String> getMessages() {
		return this.messages;
	}

	/**
	 * プロジェクト名を取得します。
	 * @return プロジェクト名
	 */
	public String getProjectName() {
	    return projectName;
	}
	/**
	 * プロジェクト名を設定します。
	 * @param projectName プロジェクト名
	 */
	public void setProjectName(String projectName) {
	    this.projectName = projectName;
	}

	/**
	 * バージョンを取得します。
	 * @return バージョン
	 */
	public String getVersion() {
	    return version;
	}
	/**
	 * バージョンを設定します。
	 * @param version バージョン
	 */
	public void setVersion(String version) {
	    this.version = version;
	}

	@Override
	public String getFlowAction() {
		return flowAction;
	}
	@Override
	public void setFlowAction(String action) {
		this.flowAction = action;
	}

	@Override
	public String getProcId() {
		return procId;
	}

	@Override
	public IGeneralServiceBean setProcId( String procId ) {
		this.procId = procId;
	    return this;
	}

	@Override
	public IStatusId getProcMgtStatusId() {
	    return procMgtStatusId;
	}

	@Override
	public IGeneralServiceBean setProcMgtStatusId(IStatusId procMgtStatusId) {
	    this.procMgtStatusId = procMgtStatusId;
	    return this;
	}

	@Override
	public Timestamp getSystemDate() {
		return systemDate;
	}
	@Override
	public void setSystemDate(Timestamp systemDate) {
		if ( null == this.systemDate )
			this.systemDate = systemDate;
	}

	@Override
	public IMessageId getInfoMessageId() {
		return infoMessageId;
	}
	@Override
	public void setInfoMessage( IMessageId messageInfo ) {
		this.infoMessageId = messageInfo;
	}

	@Override
	public String[] getInfoMessageArgs() {
		return infoMessageArgs;
	}
	@Override
	public void setInfoMessageArgs(String[] messageInfoArgs) {
		this.infoMessageArgs = messageInfoArgs;
	}

	@Override
	public List<IMessageId> getInfoMessageIdList() {
		return infoMessageIdList;
	}
	@Override
	public void setInfoMessageIdList( List<IMessageId> messageInfoIdList ) {
		this.infoMessageIdList = messageInfoIdList;
	}

	@Override
	public List<String[]> getInfoMessageArgsList() {
		return infoMessageArgsList;
	}
	@Override
	public void setInfoMessageArgsList( List<String[]> messageInfoArgsList ) {
		this.infoMessageArgsList = messageInfoArgsList;
	}

	@Override
	public List<IMessageId> getInfoCommentIdList() {
		return infoCommentIdList;
	}
	@Override
	public void setInfoCommentIdList( List<IMessageId> commentInfoList ) {
		this.infoCommentIdList = commentInfoList;
	}

	@Override
	public List<String[]> getInfoCommentArgsList() {
		return infoCommentArgsList;
	}
	@Override
	public void setInfoCommentArgsList( List<String[]> commentInfoArgsList ) {
		this.infoCommentArgsList = commentInfoArgsList;
	}

	@Override
	public Throwable getSystemThrowable() {
		return systemThrowable;
	}
	@Override
	public void setSystemThrowable(Throwable systemThrowable) {
		this.systemThrowable = systemThrowable;
	}
	@Override
	public ITranslatable getBusinessThrowable() {
		return businessThrowable;
	}
	@Override
	public void setBusinessThrowable(ITranslatable businessThrowable) {
		this.businessThrowable = businessThrowable;
	}
	public String getForward() {
		return forward;
	}
	public void setForward(String forward) {
		this.forward = forward;
	}
	public String getReferer() {
		return referer;
	}
	public void setReferer(String referer) {
		this.referer = referer;
	}
	@Override
	public String getScreenType() {
		return screenType;
	}
	@Override
	public void setScreenType(String screenType) {
		this.screenType = screenType;
	}
	@Override
	public boolean isLockByThread() {
		return isLockByThread;
	}
	@Override
	public void setLockByThread(boolean isLockByThread) {
		this.isLockByThread = isLockByThread;
	}
	@Override
	public boolean isLock() {
		return isLock;
	}
	@Override
	public void setLock(boolean isLock) {
		this.isLock = isLock;
	}
	@Override
	public String getNumberingVal() {
		return numberingVal;
	}
	@Override
	public void setNumberingVal( String numberingVal ) {
		this.numberingVal = numberingVal;
	}
	public List<Object> getParamList() {
		return paramList;
	}
	public void setParamList(List<Object> paramList) {
		this.paramList = paramList;
	}

	@Override
	public final String getLockLotNo() {
		return lockLotNo;
	}
	@Override
	public final void setLockLotNo( String lockLotNo ) {
		this.lockLotNo = lockLotNo;
	}

	@Override
	public final String[] getLockLotNoArray() {
		return lockLotNoArray;
	}
	@Override
	public final void setLockLotNoArray( String[] lockLotNoArray ) {
		this.lockLotNoArray = lockLotNoArray;
	}

	@Override
	public String getLockServerId() {
		if ( TriStringUtils.isEmpty( this.lockServerId ) || "undefined".equals( this.lockServerId ) ) {
			this.lockServerId = ServerType.CORE.getValue();
		}
		return lockServerId;
	}
	@Override
	public void setLockServerId( String lockServerId ) {
		this.lockServerId = lockServerId;
	}

	@Override
	public boolean isAuthError() {
		return authError;
	}
	@Override
	public void setAuthError(boolean authError) {
		this.authError = authError;
	}

}
