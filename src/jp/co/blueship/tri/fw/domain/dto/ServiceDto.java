package jp.co.blueship.tri.fw.domain.dto;

import static jp.co.blueship.tri.fw.cmn.utils.predicate.TriPredicates.*;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;

/**
 * ドメイン層で利用するサービスDTOです。
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class ServiceDto<E> implements IServiceDto<E> {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	private E serviceBean = null;
	private List<Object> paramList = new ArrayList<Object>();

	@SuppressWarnings("unchecked")
	@Override
	public E getServiceBean() {
		if ( null != this.serviceBean )
			return this.serviceBean;

		IGeneralServiceBean bean = (IGeneralServiceBean) FluentList.//
				from(paramList).//
				atFirstOf(isInstanceOf(IGeneralServiceBean.class));

		if ( null != bean ) {
			return (E)bean;
		}

		return null;
	}

	@Override
	public IServiceDto<E> setServiceBean(E bean) {
		this.serviceBean = bean;
		return this;
	}

	@Override
	public IServiceDto<E> add(Object param) {
		paramList.add( param );
		return this;
	}

	@Override
	public IServiceDto<E> addAll(List<Object> paramList) {
		this.paramList.addAll( paramList );
		return this;
	}

	@Override
	public List<Object> getParamList() {
		return paramList;
	}

	@Override
	public IServiceDto<E> setParamList( List<Object> paramList) {
		this.paramList = paramList;
		return this;
	}

}
