package jp.co.blueship.tri.fw.domain.dto;

import java.io.Serializable;


/**
 * アプリケーション層で共通に使用するパラメタインタフェースです。
 *
 */
public interface ICommonParamInfo extends Serializable {

	

	/**
	 * ユーザＩＤを取得します。
	 * @return 取得した値を戻します。
	 */
	public String getUserId();
	/**
	 * ユーザＩＤを設定します。
	 * @param id ユーザＩＤ
	 */
	public void setUserId( String id );

	/**
	 * パスワード取得メソッド
	 * @return パスワード文字列
	 */
	public String getPassword();

	/**
	 * パスワード設定メソッド
	 * @param password パスワード文字列
	 */
	public void setPassword(String password);
}
