package jp.co.blueship.tri.fw.domain.dto;

import java.io.Serializable;
import java.util.ArrayList;


/**
 * アプリケーション層で共通に使用するパラメタリストクラスです。
 *
 * @author Yukihiro Eguchi
 * @param <E>
 *
 */
public class CommonParamList<E> extends ArrayList<E> implements Serializable {

	private static final long serialVersionUID = 1L;

	private String flowaction;
	public CommonParamList() {
		super();
	}

	public String getFlowAction() {
		return flowaction;
	}

	public void setFlowAction(String action) {
		this.flowaction = action;
	}
}
