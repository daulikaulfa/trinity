package jp.co.blueship.tri.fw.domain.dto;

import java.io.Serializable;



/**
 * アプリケーション層で共通に使用するパラメタクラスです。
 *
 * @author Yukihiro Eguchi
 *
 */
public class CommonParamInfo implements ICommonParamInfo , Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	private String userId = null;
	private String password = null;

	public final String getUserId() {
		return userId;
	}
	public final void setUserId(String id) {
		this.userId = id;
	}
	public final String getPassword() {
		return password;
	}
	public final void setPassword(String password) {
		this.password = password;
	}

}
