package jp.co.blueship.tri.fw.domain.dto;

import java.io.Serializable;
import java.util.List;


/**
 * ドメイン層で利用するサービスDTO Interfaceです。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public interface IServiceDto<E> extends Serializable {
	/**
	 * ドメイン層で共通に利用するService Bean Interfaceを取得します
	 *
	 * @return Service Bean Interface
	 */
	public E getServiceBean();

	/**
	 * ドメイン層で共通に利用するService Bean Interfaceを設定します
	 *
	 * @param bean Service Bean Interface
	 * @return a reference to this instance, so it's possible to chain several calls together
	 */
	public IServiceDto<E> setServiceBean( E bean );

	/**
	 * ドメイン層で共通に利用するService Objectを追加します
	 *
	 * @param paramList Service Beanのリスト
	 * @return a reference to this instance, so it's possible to chain several calls together
	 */
	public IServiceDto<E> add( Object param );

	/**
	 * ドメイン層で共通に利用するService Objectのリストを追加します
	 *
	 * @param paramList Service Beanのリスト
	 * @return a reference to this instance, so it's possible to chain several calls together
	 */
	public IServiceDto<E> addAll( List<Object> paramList );

	/**
	 * ドメイン層で共通に利用するService Objectのリストを取得します
	 *
	 * @return Service Objectのリスト
	 */
	public List<Object> getParamList();

	/**
	 * ドメイン層で共通に利用するService Objectのリストを設定します
	 *
	 * @param paramList Service Beanのリスト
	 * @return a reference to this instance, so it's possible to chain several calls together
	 */
	public IServiceDto<E> setParamList( List<Object> paramList );

}
