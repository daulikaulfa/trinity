package jp.co.blueship.tri.fw.domain.dto;

import java.sql.Timestamp;
import java.util.List;
import java.util.TimeZone;

import jp.co.blueship.tri.fw.dao.orm.constants.IStatusId;
import jp.co.blueship.tri.fw.ex.ITranslatable;
import jp.co.blueship.tri.fw.msg.IMessageId;



/**
 * ドメイン層で共通に利用するBean Interfaceです。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public interface IGeneralServiceBean extends IServiceBean {
	/**
	 * ユーザIDを取得します。
	 * @return 取得した値を戻します。
	 */
	public String getUserId();
	/**
	 * ユーザIDを設定します。
	 * @param id ユーザID
	 */
	public void setUserId(String userId);

	/**
	 * ユーザ名を取得します。
	 * @return 取得した値を戻します。
	 */
	public String getUserName();
	/**
	 * ユーザ名を設定します。
	 * @param userName ユーザ名
	 */
	public void setUserName(String userName);

	/**
	 * Threadで業務排他を行うかどうかを取得します。
	 * @return 取得した値を戻します。
	 */
	public boolean isLockByThread();
	/**
	 * Threadで業務排他を行うかどうかを設定します。
	 *
	 * @param isLockByThread Threadで業務排他を行うかどうか
	 */
	public void setLockByThread( boolean isLockByThread );

	/**
	 * 業務排他中かどうかを取得します。
	 * @return 取得した値を戻します。
	 */
	public boolean isLock();
	/**
	 * 業務排他中かどうかを設定します。
	 *
	 * @param isLock 業務排他中かどうか
	 */
	public void setLock( boolean isLock );

	/**
	 * フロー制御アクションを取得します。
	 * @return 取得した値を戻します。
	 */
	public String getFlowAction();
	/**
	 * フロー制御アクションを設定します。
	 * @param action アクション
	 */
	public void setFlowAction( String action );

	/**
	 * 遷移タイプを取得します。
	 * @return 取得した値を戻します。
	 */
	public String getScreenType();
	/**
	 * 遷移タイプを設定します。
	 * @param screenType 遷移タイプ
	 */
	public void setScreenType( String screenType );

	/**
	 * 採番を取得します。
	 * @return 取得した値を戻します。
	 */
	public String getNumberingVal();
	/**
	 * 採番を設定します。
	 * @param value 採番
	 */
	public void setNumberingVal( String value );

	/**
	 * プロセス管理のプロセスIDを取得します。
	 *
	 * @return 取得した値を戻します。
	 */
	public String getProcId();

	/**
	 * プロセス管理のプロセスIDを設定します。
	 *
	 * @param procId プロセス管理のプロセスID
	 * @return このオブジェクトへの参照
	 */
	public IGeneralServiceBean setProcId( String procId );

	/**
	 * プロセス管理のステータスIDを取得します。
	 * @return 取得した値を戻します。
	 */
	public IStatusId getProcMgtStatusId();
	/**
	 * プロセス管理のステータスIDを設定します。
	 * @param statusId ステータスID
	 * @return このオブジェクトへの参照
	 */
	public IGeneralServiceBean setProcMgtStatusId( IStatusId statusId );

	/**
	 * システムエラー発生時のThrowableを取得します。
	 *
	 * @return 取得したThrowableを戻します。
	 */
	public Throwable getSystemThrowable();
	/**
	 * システムエラー発生時のThrowableを設定します。
	 *
	 * @param systemThrowable
	 */
	public void setSystemThrowable(Throwable systemThrowable);
	/**
	 * 業務エラー発生時のThrowableを取得します。
	 *
	 * @return 取得したThrowableを戻します。
	 */
	public ITranslatable getBusinessThrowable();
	/**
	 * 業務エラー発生時のThrowableを設定します。
	 *
	 * @param businessThrowable
	 */
	public void setBusinessThrowable(ITranslatable businessThrowable);

	public boolean isEmptyMessages();

	public void setMessages( List<String> messages );

	public List<String> getMessages();

	/**
	 * 処理日時を取得します。
	 * <br>業務シーケンス内で統一した処理日時を設定する場合に使用します。
	 * @return 取得した値を戻します。
	 */
	public Timestamp getSystemDate();
	/**
	 * 処理日時を設定します。
	 * <br>業務シーケンス内で統一した処理日時を設定する場合に使用します。
	 * @param date 処理日時
	 */
	public void setSystemDate( Timestamp date );

	public IMessageId getInfoMessageId();
	public void setInfoMessage(IMessageId messageInfo);

	public String[] getInfoMessageArgs();
	public void setInfoMessageArgs(String[] messageInfoArgs);

	public List<IMessageId> getInfoMessageIdList();
	public void setInfoMessageIdList( List<IMessageId> messageInfoList );

	public List<String[]> getInfoMessageArgsList();
	public void setInfoMessageArgsList( List<String[]> messageInfoArgsList );

	public List<IMessageId> getInfoCommentIdList();
	public void setInfoCommentIdList( List<IMessageId> commentInfoList );

	public List<String[]> getInfoCommentArgsList();
	public void setInfoCommentArgsList( List<String[]> commentInfoArgsList );

	/** 排他用ロット番号 */
	public String getLockLotNo();
	public void setLockLotNo( String lockLotNo );
	public String[] getLockLotNoArray();
	public void setLockLotNoArray( String[] lockLotNoArray );

	/** 排他用サーバ番号 */
	public String getLockServerId();
	public void setLockServerId( String lockServerId );

	public boolean isAuthError();
	public void setAuthError( boolean authError );

	public TimeZone getTimeZone();
	public void setTimeZone(TimeZone zone);

	public String getLanguage();
	public void setLanguage(String lang);
	
}
