package jp.co.blueship.tri.fw.domain;

import jp.co.blueship.tri.fw.domain.dto.IServiceDto;

public interface IInnerTransactionService<E> {
	/**
	 * このメソッド内は、SpringAOPにより暗黙的なトランザクション制御が行われます。
	 *
	 * @param serviceDto Service呼び出し時のパラメータ
	 * @return 戻り値を返します。
	 */
	public IServiceDto<E> execute(IServiceDto<E> serviceDto );
}
