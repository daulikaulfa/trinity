package jp.co.blueship.tri.fw.domain;

import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.SmMessageId;



/**
 * 全体のトランザクション中の一部の処理について、トランザクション制御を行うビジネス処理を一括して処理する汎用クラスです。
 * <br>この実装クラスは、必ずsingleton = falseで実行します。
 *
 * @author Yukihiro Eguchi
 *
 */
public class InnerTransactionService extends Service implements IInnerTransactionService<IGeneralServiceBean>, IDomain<IGeneralServiceBean> {
	private String nextTransactionService = null;

	/**
	 * 一度のリクエスト処理で、トランザクション制御を行う場合、指定されます。
	 *
	 * @param transactionService 次のトランザクション制御処理があれば、指定する
	 */
	public void setNextTargetService( String transactionService ) {
		this.nextTransactionService = transactionService;
	}

	/**
	 * このメソッド内は、SpringAOPにより暗黙的なトランザクション制御が行われます。
	 *
	 * @param prmBLBean サービスパラメタ情報
	 * @param rtnBLBean 業務処理結果情報
	 * @return 処理結果を戻します。
	 * @throws Exception
	 */

	@SuppressWarnings("unchecked")
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		if( null == this.getContext() )
			this.init();

		if ( null == this.nextTransactionService ) {
			throw new TriSystemException(SmMessageId.SM005050S);

		} else {
			IDomain<IGeneralServiceBean> service = (IDomain<IGeneralServiceBean>)this.getContext().getBean( this.nextTransactionService );

			return service.execute(serviceDto);
		}
	}

}
