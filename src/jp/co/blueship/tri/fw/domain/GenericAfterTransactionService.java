package jp.co.blueship.tri.fw.domain;

import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;


/**
 * 読み取り一貫性の概念が通常のＤＢとは異なるDB(Shunsaku等)を利用する場合、
 * （普通、RDBでは同一トランザクションであれば、level設定次第で読み取れる）
 * トランザクションの上位のクラスでcommit後の処理を記述する必要があります。
 * このクラスは、commmit後の処理を記述するためのクラスです。
 * <br>この実装クラスは、必ずsingleton = falseで実行します。
 *
 * @author Yukihiro Eguchi
 *
 */
public class GenericAfterTransactionService extends GenericServiceAbstract implements IGenericTransactionService {

	private static final ILog log = TriLogFactory.getInstance();

	private String nextTransactionService = null;

	/**
	 * 一度のリクエスト処理で、トランザクション制御を行う場合、指定されます。
	 *
	 * @param transactionService 次のトランザクション制御処理があれば、指定する
	 */
	public void setNextTargetService( String transactionService ) {
		this.nextTransactionService = transactionService;
	}

	/**
	 * このメソッド内は、SpringAOPにより暗黙的なトランザクション制御が行われます。
	 *
	 * @param targetService ターゲットサービスを指定します。
	 * @param serviceDto サービスパラメタ情報
	 * @return 処理結果を戻します。
	 * @throws Exception
	 */

	public IServiceDto<IGeneralServiceBean> execute(
				String targetService,
				IServiceDto<IGeneralServiceBean> serviceDto ) throws Exception {

		IGeneralServiceBean gsBean = serviceDto.getServiceBean();
		gsBean.setFlowAction( targetService );
		gsBean.setSystemDate( TriDateUtils.getSystemTimestamp() );

		try{
			this.beforeAdvices( serviceDto );

			if ( null == this.nextTransactionService ) {
				GenericService service = (GenericService)this.getContext().getBean( targetService );
				((IService)service).init();
				serviceDto = service.execute(serviceDto);

			} else {
				IGenericTransactionService service = (IGenericTransactionService)this.getContext().getBean( this.nextTransactionService );
				((IService)service).init();

				serviceDto = service.execute(targetService, serviceDto);
			}

			if ( null != this.getPojoList() ) {
				for ( IDomain<IGeneralServiceBean> pojo: this.getPojoList() ) {
					serviceDto = pojo.execute(serviceDto);
				}
			}

			return serviceDto;

		}catch (BaseBusinessException e) {
			//同期処理では、業務メールを送付しない
			//gsBean.setBusinessThrowable( e );

			LogHandler.error( log , e ) ;
			throw e;
		}catch (Exception e) {
			gsBean.setSystemThrowable( e );

			LogHandler.fatal( log , e ) ;
			throw e;
		}finally{
			try {
				this.afterAdvices( serviceDto );
			}  catch ( Exception e ) {
				throw e;
			}
		}
	}

}
