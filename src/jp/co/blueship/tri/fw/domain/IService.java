package jp.co.blueship.tri.fw.domain;

import jp.co.blueship.tri.fw.di.IContextAdapter;

/**
 * サービスクラスInterfaceです。
 * 
 */
public interface IService {
	public void init() throws Exception;

	public IContextAdapter getContext() throws Exception;
	
}
