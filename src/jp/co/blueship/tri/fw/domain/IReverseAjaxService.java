package jp.co.blueship.tri.fw.domain;

import jp.co.blueship.tri.fw.domain.dto.IServiceDto;


public interface IReverseAjaxService<E> {
	/**
	 * @return Pojoのリストを返却します。
	 * @throws Exception
	 */
	public GenericService getApplicationService(
			String targetService,
			IServiceDto<E> prmBLBean ) throws Exception;
}
