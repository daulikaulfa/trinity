package jp.co.blueship.tri.fw.domain;

import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;

/**
 * ビジネス処理を一括して実行する抽象クラスです。
 * <br>このクラスは、必ずsingleton = falseで実行します。
 *
 * @author Yukihiro Eguchi
 *
 */
public class GenericServiceAbstract extends Service implements IService {

	private static final ILog log = TriLogFactory.getInstance();

	private List<IDomain<IGeneralServiceBean>> beforeAdvices = null;
	private List<IDomain<IGeneralServiceBean>> pojoList = null;
	private List<IDomain<IGeneralServiceBean>> afterAdvices = null;

	/**
	 * 処理前に必ず実行されるビジネス処理のリストを設定します。
	 * @param pojoList 一括して実行するリスト
	 */
	public final void setBeforeAdvices( List<IDomain<IGeneralServiceBean>> pojoList ) {
		beforeAdvices = pojoList;
	}

	/**
	 * 実行されるビジネス処理のリストを設定します。
	 * @param pojoList 一括して実行するリスト
	 */
	public final void setActions( List<IDomain<IGeneralServiceBean>> pojoList ) {
		this.pojoList = pojoList;
	}

	/**
	 * 処理終了後に必ず実行されるビジネス処理のリストを設定します。
	 * @param pojoList 一括して実行するリスト
	 */
	public final void setAfterAdvices( List<IDomain<IGeneralServiceBean>> pojoList ) {
		afterAdvices = pojoList;
	}

	public List<IDomain<IGeneralServiceBean>> getAfterAdvices() {
		return afterAdvices;
	}

	public List<IDomain<IGeneralServiceBean>> getBeforeAdvices() {
		return beforeAdvices;
	}

	public List<IDomain<IGeneralServiceBean>> getPojoList() {
		return pojoList;
	}

	/**
	 *処理前に必ず実行されるビジネス処理です。
	 * @param info アプリケーション層に使用する共通インタフェース
	 * @param list コンバート間に流通させるビーン
	 */
	protected final IServiceDto<IGeneralServiceBean> beforeAdvices(IServiceDto<IGeneralServiceBean> prmBLBean) throws Exception{

		try{
			if ( null == this.getBeforeAdvices() )
				return prmBLBean;

			for ( IDomain<IGeneralServiceBean> pojo: this.getBeforeAdvices() ) {
				prmBLBean = pojo.execute(prmBLBean);
			}

			return prmBLBean;

		}catch (BaseBusinessException e) {
			LogHandler.error( log , e ) ;
			throw e;
		}catch (Exception e) {
			LogHandler.fatal( log , e ) ;
			throw e;
		}
	}

	/**
	 * 処理終了後に必ず実行されるビジネス処理です。
	 * @param info アプリケーション層に使用する共通インタフェース
	 * @param list コンバート間に流通させるビーン
	 */
	protected final IServiceDto<IGeneralServiceBean> afterAdvices(IServiceDto<IGeneralServiceBean> prmBLBean) throws Exception{

		try{
			if ( null == this.getAfterAdvices() )
				return prmBLBean;

			for ( IDomain<IGeneralServiceBean> pojo: this.getAfterAdvices() ) {
				prmBLBean = pojo.execute(prmBLBean);
			}

			return prmBLBean;

		}catch (BaseBusinessException e) {
			LogHandler.error( log , e ) ;
			throw e;
		}catch (Exception e) {
			LogHandler.fatal( log , e ) ;
			throw e;
		}
	}

}
