package jp.co.blueship.tri.fw.domain;

import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;

/**
 *
 * @version V3L10R02
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 */
abstract public class Service implements IService {


	private IContextAdapter context = null;

	public void init(){
		IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
		this.context = ca;
	}

	public IContextAdapter getContext() {
		if ( null == this.context ) {
			this.init();
		}

		return context;
	}

}