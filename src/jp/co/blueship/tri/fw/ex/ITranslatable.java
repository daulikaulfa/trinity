package jp.co.blueship.tri.fw.ex;

import jp.co.blueship.tri.fw.msg.IMessageId;

/**
 * メッセージリソースによって翻訳可能となるよう、メッセージID、メッセージパラメタを保持する例外の共通インタフェースです。
 *
 * @author Takayuki Kubo
 *
 */
public interface ITranslatable {

	/**
	 * メッセージIDを返します。
	 *
	 * @return メッセージID
	 */
	public IMessageId getMessageID();

	/**
	 * メッセージIDに該当するメッセージ文言が、メッセージパラメタのプレースホルダを持つ場合、<br/>
	 * 補完するメッセージのパラメタを配列形式で返します。
	 *
	 * @return メッセージパラメタ
	 */
	public String[] getMessageArgs();

}
