package jp.co.blueship.tri.fw.ex;

import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriView;

/**
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 * @version V4.03.00
 * @author Cuong Nguyen
 *
 */
public class ControllerExceptionUtils {

	public static <T extends DomainServiceBean> boolean isBusinessException( Throwable ex ) {
		PreConditions.assertOf( ex != null, "ex is not specified" );

		if ( BaseBusinessException.class.isInstance(ex) ) {
			return true;
		}

		return false;
	}

	public static <T extends DomainServiceBean> boolean isRedirectException( Throwable ex, TriControllerSupport<T> controller, T domainBean, TriModel model, String view ) {
		PreConditions.assertOf( ex != null, "ex is not specified" );
		PreConditions.assertOf( controller != null, "controller is not specified" );

		if ( isBusinessException(ex) ) {

			DomainServiceBean prevBean =  controller.getPrevBean(model, domainBean.getHeader().getWindowsId());

			if ( RequestType.init.equals(domainBean.getParam().getRequestType())
				&& domainBean.getClass() != prevBean.getClass() ) {
				return true;
			}
			
		} else {
			return true;
		}

		return false;
	}

	public static <T extends DomainServiceBean> String redirectException( Throwable ex, TriControllerSupport<T> controller, T domainBean, TriModel model, String view ) {
		PreConditions.assertOf( ex != null, "ex is not specified" );
		PreConditions.assertOf( controller != null, "controller is not specified" );

		if ( isBusinessException(ex) ) {
			return redirectBusinessException( ex, controller, domainBean, model, view );
		} else {
			return redirectSystemException( ex, controller, domainBean, model );
		}
	}

	private static <T extends DomainServiceBean> String redirectBusinessException( Throwable ex, TriControllerSupport<T> controller, T domainBean, TriModel model, String view ) {

		if ( isBusinessException(ex) ) {

			DomainServiceBean prevBean =  controller.getPrevBean(model, domainBean.getHeader().getWindowsId());

			if ( RequestType.init.equals(domainBean.getParam().getRequestType())&& domainBean.getClass() != prevBean.getClass() ) {
				prevBean.setMessages(domainBean.getMessages());
//				prevBean.getParam().setRequestType(RequestType.init);
				model.getRedirectAttributes().addFlashAttribute("prevResult", prevBean);
				return "redirect:" + controller.getPrevPath(model, domainBean.getHeader().getWindowsId());
			}
		}

		return view;
	}

	private static <T extends DomainServiceBean> String redirectSystemException( Throwable ex, TriControllerSupport<T> controller, T domainBean, TriModel model ) {

		ExceptionUtils.printStackTrace(ex);
		String messageId = "";
		String errorMessage = ex.getMessage();
		if(TriRuntimeException.class.isInstance(ex)) {
			TriRuntimeException triRuntimeException = (TriRuntimeException) ex;
			IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
			messageId = triRuntimeException.getMessageID() + "";
			errorMessage = ca.getMessageManager().getMessage(domainBean.getLanguage(), triRuntimeException.getMessageID(), triRuntimeException.getMessageArgs());
		}

		model.getModel()
			.addAttribute( "errorCode", "SYSTEM ERROR" )
			.addAttribute( "errorMessage", errorMessage)
			.addAttribute("errorFullStackTrace", ex.getStackTrace())
			.addAttribute("result", domainBean)
			.addAttribute("messageId", messageId)
		;
		return TriView.ErrorPage.value();
	}

}
