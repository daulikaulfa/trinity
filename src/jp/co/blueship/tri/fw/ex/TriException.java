package jp.co.blueship.tri.fw.ex;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.Serializable;

import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.msg.IMessageId;

/**
 * trinityで扱うチェック例外の基底クラスです。
 *
 */
public class TriException extends Exception implements ITranslatable, Serializable {

	private static final long serialVersionUID = 3215930764442103067L;

	private IMessageId messageId;
	private String[] messageArgs = new String[] { "", "" };

	/**
	 * Constructor for Exception <br>
	 *
	 * @param messageId ID of Message
	 */
	public TriException(IMessageId messageId) {
		this.messageId = messageId;
	}

	/**
	 * Constructor for Exception <br>
	 *
	 * @param messageId ID of Message
	 * @param messageArgs message parameter
	 */
	public TriException(IMessageId messageId, String[] messageArgs) {
		this.messageId = messageId;
		this.messageArgs = messageArgs;
	}

	/**
	 * Constructor for Exception <br>
	 *
	 * @param messageId ID of Message
	 * @param cause the root cause
	 */
	public TriException(IMessageId messageId, Throwable cause) {
		super(cause);
		this.messageId = messageId;
	}

	/**
	 * Constructor for Exception <br>
	 *
	 * @param messageId ID of Message
	 * @param messageArgs message parameter
	 * @param cause the root cause
	 */
	public TriException(IMessageId messageId, String[] messageArgs, Throwable cause) {
		super(cause);
		this.messageId = messageId;
		this.messageArgs = messageArgs;
	}

	/**
	 * メッセージ付きコンストラクタ。<br>
	 *
	 * @param message メッセージ
	 */
	@Deprecated
	// メッセージIDを使用してください。
	public TriException(String message) {
		super(message);
	}

	/**
	 * メッセージ、例外チェーンコンストラクタ。<br>
	 *
	 * @param message メッセージ
	 * @param cause 起因例外
	 */
	@Deprecated
	// メッセージIDを使用してください。
	public TriException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * メッセージ、例外チェーンコンストラクタ。<br>
	 *
	 * @param cause 起因例外
	 */
	@Deprecated
	// メッセージIDを使用してください。
	public TriException(Throwable cause) {
		super(cause);
	}

	/**
	 * メッセージIDを返します。
	 *
	 * @return メッセージID
	 */
	@Override
	public IMessageId getMessageID() {
		return messageId;
	}

	/**
	 * メッセージIDに該当するメッセージ文言が、メッセージパラメタのプレースホルダを持つ場合、<br/>
	 * 補完するメッセージのパラメタを配列形式で返します。
	 *
	 * @return メッセージパラメタ
	 */
	@Override
	public String[] getMessageArgs() {
		return messageArgs;
	}

	public String getStackTraceString() {

		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(stream);

		try {
			this.printStackTrace(ps);
			return stream.toString();
		} finally {
			TriFileUtils.closeOfOutputStream(ps);
			TriFileUtils.close(stream);
		}
	}

}
