package jp.co.blueship.tri.fw.ex;

import java.util.List;

import jp.co.blueship.tri.fw.cmn.io.constants.LinefeedCode;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.IMessageId;

/**
 * Exception that gets thrown when an system failed because of unexpected runtime issues.
 *
 * @version V3L02
 * @author unknown
 *
 */
public class TriSystemException extends TriRuntimeException{

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for Exception
	 * <br>
	 * @param message messageID
	 */
	public TriSystemException(IMessageId message){
		super(message);
		TriLogFactory.getInstance().fatal( getMessageBuilder().toString() );
	}

	/**
	 * Constructor for Exception
	 * <br>
	 * @param message messageID
	 * @param messageArgs message parameter
	 */
	public TriSystemException(IMessageId message, String... messageArgs){
		super(message, messageArgs);
		TriLogFactory.getInstance().fatal( getMessageBuilder().toString() );
	}

	/**
	 * Constructor for Exception
	 * <br>
	 * @param message messageID
	 * @param cause the root cause
	 */
	public TriSystemException(IMessageId message, Throwable cause){
		super(message, cause);
		TriLogFactory.getInstance().fatal( getMessageBuilder().toString() );
	}

	/**
	 * Constructor for Exception
	 * <br>
	 * @param message messageID
	 * @param messageArgs message parameter
	 * @param cause the root cause
	 */
	public TriSystemException(IMessageId message, Throwable cause, String... messageArgs){
		super(message, cause, messageArgs);
		TriLogFactory.getInstance().fatal( getMessageBuilder().toString() );
	}

	private StringBuilder getMessageBuilder() {
		StringBuilder stb = new StringBuilder();

		List<String> messages = ContextAdapterFactory.getContextAdapter().getMessage((ITranslatable)this);
		for ( String mes: messages ) {
			stb.append(mes + LinefeedCode.CRLF.getValue());
		}

		return stb;
	}

}
