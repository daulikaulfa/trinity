package jp.co.blueship.tri.fw.ex;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import jp.co.blueship.tri.fw.cmn.utils.SystemProps;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;

public class ExceptionUtils {

	/**
	 * 指定された例外eがBusinessExceptionである場合、例外eを再スローします。
	 *
	 * @param e 再スローする例外
	 */
	public static void reThrowIfBusinessException(Throwable e) {

		reThrowIf(BusinessException.class, e);
	}

	/**
	 * 指定された例外eがContinuableBusinessExceptionである場合、例外eを再スローします。
	 *
	 * @param e 再スローする例外
	 */
	public static void reThrowIfContinuableBusinessException(Throwable e) {

		reThrowIf(ContinuableBusinessException.class, e);
	}

	/**
	 * ポップアップ画面での2画面操作防止用のメソッドです。
	 * 例外eがTrinityException(TriExceptionとTriRuntimeException,両者はITraslatableを実装している)でない場合、何も処理をせず通過します。
	 * TrinityExceptionの場合、メッセージIDとメッセージパラメータをBaseBusinessExceptionに乗せ変えて例外をスローします。
	 */
	public static <T> void throwBaseBusinessException(Throwable e){
		if ( ITranslatable.class.isInstance(e) ) {
			if (TriRuntimeException.class.isInstance(e)) {
				TriRuntimeException re = (TriRuntimeException) e;
				throwBaseBusinessException(re);
			} else if (TriException.class.isInstance(e)) {
				TriException be = (TriException) e;
				throwBaseBusinessException(be);
			}
		}
	}
	private static void throwBaseBusinessException(TriRuntimeException e) {
		throw new BaseBusinessException(e.getMessageID(), e, e.getMessageArgs() );
	}
	private static void throwBaseBusinessException(TriException e) {
		throw new BaseBusinessException(e.getMessageID(), e, e.getMessageArgs() );
	}
	/**
	 * 例外eがTrinityException(TriExceptionとTriRuntimeException,両者はITraslatableを実装している)でない場合、何も処理をせず通過します。
	 * TriRuntimeExceptionの場合、例外eを再スローします。
	 * TriExceptionの場合、メッセージIDとメッセージパラメータをTriSystemExceptionに乗せ変えて例外をスローします。
	 *
	 * @param e 再スローする例外
	 */
	public static <T> void reThrowIfTrinityException(Throwable e) {

		if ( ITranslatable.class.isInstance(e) ) {
			if (TriRuntimeException.class.isInstance(e)) {
				throw (TriRuntimeException) e;
			} else if (TriException.class.isInstance(e)) {
				TriException be = (TriException) e;
				throwTriSystemException(be);
			}
		}

	}
	private static void throwTriSystemException(TriException be) {
		throw new TriSystemException(be.getMessageID(), be, be.getMessageArgs() );
	}

	/**
	 * 指定された例外のクラス型がtである場合、例外eをスローします。
	 *
	 * @param t 再スローする条件となる例外のクラス型
	 * @param e 再スローする例外
	 */
	public static <T extends TriRuntimeException> void reThrowIf(Class<T> t, Throwable e) {

		if (t.isInstance(e)) {
			throw (TriRuntimeException) e;
		}

	}

	/**
	 * 指定された例外のクラス型がtである場合、例外eをスローします。
	 *
	 * @param t 再スローする条件となる例外のクラス型
	 * @param e 再スローする例外
	 */
	public static <T extends Exception> void reThrowExceptionIf(Class<T> t, Exception e) throws Exception {

		if (t.isInstance(e)) {
			throw e;
		}

	}

	/**
	 * 指定された条件がtrueの場合、例外をスローします。
	 * @param <T> スローする例外の型
	 * @param condition 条件式
	 * @param e 条件がtrueの場合にスローする例外
	 * @throws T 指定された条件式がfalseである
	 */
	public static <T extends Throwable> void throwIf(boolean condition, T e) throws T {

		if (condition) {
			throw e;
		}

		return;
	}

	public static String getStackTrace(Throwable th) {
		return getStackTrace(th, "");
	}

	public static String getStackTrace(Throwable th, String indent) {
		StringBuilder sb = new StringBuilder();
		sb.append(indent + th.getClass() + ":" + th.getMessage() + SystemProps.LineSeparator.getProperty());
		for (int i = 0; i < th.getStackTrace().length; i++) {
			sb.append(indent + " at " + th.getStackTrace()[i].toString() + SystemProps.LineSeparator.getProperty());
		}
		if (null != th.getCause()) {
			Throwable tc = th.getCause();
			sb.append(indent + "root cause" + SystemProps.LineSeparator.getProperty());
			sb.append(getStackTrace(tc, indent));
		}
		return sb.toString();
	}

	/**
	 *
	 * @param e
	 * @return
	 */
	public static String getStackTraceString(Throwable e) {

		ByteArrayOutputStream stream = null;
		PrintStream ps = null;
		String ret = null;

		try {
			stream = new ByteArrayOutputStream();
			ps = new PrintStream(stream);

			e.printStackTrace(ps);
			ret = stream.toString();

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				if (null != stream) {
					stream.flush();
					stream.close();
				}
				if (null != ps) {
					// ps.flush() ;
					ps.close();
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return ret;
	}

	/**
	 * 標準出力を製品ログ上に出力します。
	 * <br>e.printStackTraceは、実行環境によって出力先が変わるため
	 * 本体 / Agentで動作の統一をとるため、当メソッドを利用します。
	 *
	 * @param ex 発生した例外
	 */
	public static void printStackTrace( Throwable ex ) {
		ILog log = TriLogFactory.getInstance();

		if ( null != ex ) {
			if ( BaseBusinessException.class.isInstance(ex) ) {
				LogHandler.error(log, (BaseBusinessException)ex);
			} else {
				LogHandler.fatal(log, ex);
			}
		}
	}

	/**
	 *
	 * @param desc IGeneralServiceBean
	 * @param src IGeneralServiceBean
	 */
	public static void copyToParent( IGeneralServiceBean desc, IGeneralServiceBean src ) {
		desc.setInfoCommentArgsList( src.getInfoCommentArgsList() );
		desc.setInfoCommentIdList( src.getInfoCommentIdList() );
		desc.setInfoMessage( src.getInfoMessageId() );
		desc.setInfoMessageArgs( src.getInfoMessageArgs() );
		desc.setInfoMessageArgsList( src.getInfoMessageArgsList() );
		desc.setInfoMessageIdList( src.getInfoMessageIdList() );
	}
}
