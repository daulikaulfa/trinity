package jp.co.blueship.tri.fw.ex;

import static jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils.*;

import java.util.LinkedList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.function.MultiInputFunction;
import jp.co.blueship.tri.fw.cmn.utils.function.TriFunction;
import jp.co.blueship.tri.fw.msg.IMessageId;

/**
 * 継続可能な業務ロジック例外。<br>
 * <p>
 * 入力エラーやデータ不整合等の業務ロジカル的な例外を表現する。 この例外を発行しても、処理は継続される。
 */
public class ContinuableBusinessException extends BaseBusinessException {

	private static final long serialVersionUID = 2L;

	private static final TriFunction<IMessageId, Message> TO_MESSAGE = //
	new TriFunction<IMessageId, Message>() {

		@Override
		public Message apply(IMessageId id) {
			return new ContinuableBusinessException.Message(id, new String[] {});
		}
	};

	private static final TriFunction<Message, IMessageId> TO_MESSAGE_ID = //
	new TriFunction<Message, IMessageId>() {

		@Override
		public IMessageId apply(Message message) {
			return message.getMessageId();
		}
	};

	private static final TriFunction<Message, String[]> TO_ARGS = //
	new TriFunction<Message, String[]>() {

		@Override
		public String[] apply(Message message) {
			return message.getArgs();
		}
	};

	private static final MultiInputFunction<IMessageId, String[], Message> TO_MESSAGE_WITH_PARAM = //
	new MultiInputFunction<IMessageId, String[], Message>() {

		@Override
		public Message apply(IMessageId messageId, String[] args) {
			return new Message(messageId, args);
		}
	};

	private static final TriFunction<Message, String> TO_V2_MESSAGE = //
	new TriFunction<Message, String>() {

		@Override
		public String apply(Message message) {
			return message.getMessage();
		}
	};

	private List<Message> messageList = new LinkedList<Message>();

	/**
	 * メッセージIDを指定して、ContinuableBusinessExceptionを構築します。
	 *
	 * @param messageId メッセージID
	 */
	public ContinuableBusinessException(IMessageId messageId) {
		messageList.add(new Message(messageId, new String[] {}));
	}

	/**
	 * メッセージID、メッセージパラメタを指定して、ContinuableBusinessExceptionを構築します。
	 *
	 * @param messageId メッセージID
	 * @param args メッセージパラメタ
	 */
	public ContinuableBusinessException(IMessageId messageId, String... args) {
		messageList.add(new Message(messageId, args));
	}

	/**
	 * メッセージIDリストを指定して、ContinuableBusinessExceptionを構築します。
	 *
	 * @param messageIdList メッセージIDリスト
	 */
	public ContinuableBusinessException(List<? extends IMessageId> messageIdList) {

		collect(messageIdList, TO_MESSAGE, messageList);
	}

	/**
	 * メッセージIDリスト、メッセージパラメタリストを指定して、ContinuableBusinessExceptionを構築します。
	 *
	 * @param messageIdList メッセージIDリスト
	 * @param messageArgsList メッセージパラメタリスト
	 */
	public ContinuableBusinessException(List<IMessageId> messageIdList, List<String[]> messageArgsList) {

		composite(messageIdList, messageArgsList, TO_MESSAGE_WITH_PARAM, messageList);
	}

	@Deprecated
	// V2互換用。メッセージIDを使用してください。
	public List<String> getMessageList() {

		PreConditions.assertOf(messageList != null, "Has no message.");

		return collect(messageList, TO_V2_MESSAGE);
	}

	/**
	 * メッセージIDのリストを返します。
	 *
	 * @return メッセージIDのリスト
	 */
	public List<IMessageId> getMessageIdList() {

		PreConditions.assertOf(messageList != null, "Has no message.");

		return collect(messageList, TO_MESSAGE_ID);
	}

	/**
	 * メッセージパラメタ配列のリスト返します。
	 *
	 * @return メッセージパラメタ配列のリスト
	 */
	public List<String[]> getMessageArgsList() {

		PreConditions.assertOf(messageList != null, "Has no message.");

		return collect(messageList, TO_ARGS);
	}

	/**
	 * メッセージIDを返します。メッセージIDを複数保持している場合は先頭の1件を返します。
	 *
	 * @return メッセージID
	 * @see jp.co.blueship.tri.fw.ex.TriRuntimeException#getMessageID()
	 */
	@Override
	public IMessageId getMessageID() {
		return getMessageIdList().get(0);
	}

	/**
	 * メッセージパラメタ配列を返します。メッセージパラメタ配列Dを複数保持している場合は先頭の1件を返します。
	 *
	 * @return メッセージパラメタ配列
	 *
	 * @see jp.co.blueship.tri.fw.ex.TriRuntimeException#getMessageArgs()
	 */
	@Override
	public String[] getMessageArgs() {
		return getMessageArgsList().get(0);
	}

	private static class Message {

		private IMessageId messageId;
		@Deprecated
		// V2互換用
		private String message;
		private String[] args;

		private Message(IMessageId messageId, String[] args) {
			this.messageId = messageId;
			this.args = args;
		}

		@Deprecated
		// V2互換用
		private Message(String message, String[] args) {
			this.message = message;
			this.args = args;
		}

		IMessageId getMessageId() {
			return messageId;
		}

		@Deprecated
		// V2互換用
		String getMessage() {
			return message;
		}

		String[] getArgs() {
			return args;
		}
	}

}
