package jp.co.blueship.tri.fw.ex;

import jp.co.blueship.tri.fw.msg.IMessageId;

/**
 * 業務ロジック例外の基底class。<br>
 */
public class BaseBusinessException extends TriRuntimeException {

	private static final long serialVersionUID = 1L;

	

	/**
	 * デフォルトコンストラクタです。
	 */
	protected BaseBusinessException() {
	}

	/**
	 * Constructor for Exception
	 * <br>
	 * @param message messageID
	 */
	public BaseBusinessException(IMessageId message){
		super(message);
	}

	/**
	 * Constructor for Exception
	 * <br>
	 * @param message messageID
	 * @param messageArgs message parameter
	 */
	public BaseBusinessException(IMessageId message, String... messageArgs){
		super(message, messageArgs);
	}

	/**
	 * Constructor for Exception
	 * <br>
	 * @param message messageID
	 * @param cause the root cause
	 */
	public BaseBusinessException(IMessageId message, Throwable cause){
		super(message, cause);
	}

	/**
	 * Constructor for Exception
	 * <br>
	 * @param message messageID
	 * @param messageArgs message parameter
	 * @param cause the root cause
	 */
	public BaseBusinessException(IMessageId message, Throwable cause, String... messageArgs){
		super(message, cause, messageArgs);
	}

	@Deprecated //"メッセージID"を使用してください。
	BaseBusinessException(String errMsg, String[] messageArgs) {
		super(errMsg);
		this.setMessageArgs( messageArgs );
	}

}
