package jp.co.blueship.tri.fw.ex;

import jp.co.blueship.tri.fw.msg.IMessageId;

/**
 * 業務ロジック例外。<br>
 * <p>
 * 入力エラーやデータ不整合等の業務ロジカル的な例外を表現する。
 * </p>
 */
public class BusinessException extends BaseBusinessException {

	private static final long serialVersionUID = 1L;

	public BusinessException(IMessageId message){
		super(message);
	}

	/**
	 * Constructor for Exception
	 * <br>
	 * @param message messageID
	 * @param messageArgs message parameter
	 */
	public BusinessException(IMessageId message, String... messageArgs){
		super(message, messageArgs);
	}

	/**
	 * Constructor for Exception
	 * <br>
	 * @param message messageID
	 * @param cause the root cause
	 */
	public BusinessException(IMessageId message, Throwable cause){
		super(message, cause);
	}

	/**
	 * Constructor for Exception
	 * <br>
	 * @param message messageID
	 * @param messageArgs message parameter
	 * @param cause the root cause
	 */
	public BusinessException(IMessageId message, Throwable cause, String... messageArgs){
		super(message, cause, messageArgs);
	}

	@Deprecated //"メッセージID"を使用してください。
	public BusinessException(String errMsg, String[] messageArgs) {
		super(errMsg, messageArgs);
	}

}


