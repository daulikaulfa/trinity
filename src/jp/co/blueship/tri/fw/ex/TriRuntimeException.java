package jp.co.blueship.tri.fw.ex;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.Serializable;

import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.msg.IMessageId;

/**
 * trinityで扱うRuntimeExceptionの基底class。
 *
 */
public abstract class TriRuntimeException extends RuntimeException implements ITranslatable, Serializable {

	private static final long serialVersionUID = -7597552113407485952L;

	private IMessageId messageID;
	private String[] messageArgs = new String[] { "", "" };

	/**
	 * デフォルトコンストラクタです。
	 */
	protected TriRuntimeException() {
	}

	/**
	 * Constructor for Exception <br>
	 *
	 * @param message messageID
	 */
	public TriRuntimeException(IMessageId messageID) {
		this(messageID, null, (String[]) null);
	}

	/**
	 * Constructor for Exception <br>
	 *
	 * @param message messageID
	 * @param messageArgs message parameter
	 */
	public TriRuntimeException(IMessageId messageID, String... messageArgs) {
		this(messageID, null, messageArgs);
	}

	/**
	 * Constructor for Exception <br>
	 *
	 * @param message messageID
	 * @param cause the root cause
	 */
	public TriRuntimeException(IMessageId messageID, Throwable cause) {
		this(messageID, cause, (String[]) null);
	}

	/**
	 * Constructor for Exception <br>
	 *
	 * @param message messageID
	 * @param cause the root cause
	 * @param messageArgs message parameter
	 */
	public TriRuntimeException(IMessageId messageID, Throwable cause, String... messageArgs) {
		super(cause);
		this.messageID = messageID;
		this.messageArgs = messageArgs;
	}

	/**
	 * メッセージ付きコンストラクタ。<br>
	 *
	 * @param message メッセージ
	 */
	@Deprecated
	public TriRuntimeException(String message) {
		super(message);
	}

	/**
	 * メッセージ、例外チェーンコンストラクタ。<br>
	 *
	 * @param message メッセージ
	 * @param cause 起因例外
	 */
	@Deprecated
	public TriRuntimeException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * メッセージ、例外チェーンコンストラクタ。<br>
	 *
	 * @param cause 起因例外
	 */
	@Deprecated
	public TriRuntimeException(Throwable cause) {
		super(cause);
	}

	/**
	 * メッセージIDを返します。
	 *
	 * @return メッセージID
	 */
	@Override
	public IMessageId getMessageID() {
		return this.messageID;
	}

	/**
	 * メッセージIDに該当するメッセージ文言が、メッセージパラメタのプレースホルダを持つ場合、<br/>
	 * 補完するメッセージのパラメタを配列形式で返します。
	 *
	 * @return メッセージパラメタ
	 */
	@Override
	public String[] getMessageArgs() {
		return messageArgs;
	}

	protected void setMessageArgs(String[] messageArgs) {
		this.messageArgs = messageArgs;
	}

	public String getStackTraceString() {

		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(stream);

		try {
			this.printStackTrace(ps);
			return stream.toString();
		} finally {
			TriFileUtils.closeOfOutputStream(ps);
			TriFileUtils.close(stream);
		}
	}

}
