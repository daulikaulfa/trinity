package jp.co.blueship.tri.fw.ws;

/**
 * 検索条件として主キーを指定する際に使用するパラメタクラスのマーカインタフェースです。
 *
 *
 * @author Takayuki Kubo
 *
 */
public interface IPrimaryKey {
}
