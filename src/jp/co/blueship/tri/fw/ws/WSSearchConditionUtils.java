package jp.co.blueship.tri.fw.ws;

import java.sql.Timestamp;

import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;

public final class WSSearchConditionUtils {

	private WSSearchConditionUtils() {
		// NOP
	}

	/**
	 * 指定された文字列形式の日付(時刻)をタイムスタンプへ変換します。<br/>
	 * 日付形式はデザインシートに設定したものと一致している必要があります。
	 *
	 * @param dateTime 文字列形式の日付(時刻)
	 * @return タイムスタンプ
	 */
	public static Timestamp toTimeStamp(String dateTime) {

		PreConditions.assertOf(!TriStringUtils.isEmpty(dateTime), "Date time string is null or empty.");

		Timestamp result = null;
		TriDateUtils.convertStringToTimestampWithDateTime(dateTime);

		return result;
	}

	/**
	 * 整数値で与えられたステータスフラグを列挙型のそれに変換します。
	 *
	 * @param flag 整数値表現されたステータスフラグ(1:ON 0:Off)
	 * @return 1の場合はON、0の場合はOFFを返す。
	 */
	public static StatusFlg toStatusFlg(int flag) {

		PreConditions.assertOf(flag == 0 || flag == 1 , "Status flag shoud be specified 1 or 0.");
		return StatusFlg.value(flag);
	}

}
