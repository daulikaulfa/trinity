package jp.co.blueship.tri.fw.ws;

/**
 * リソースの追加・更新・参照で使用するValueObjectのインタフェースです。
 * <br>
 * @version V3L10.01
 * @author Takayuki Kubo
 *
 * @version V3L11.01
 * @author Yukihiro Eguchi
 *
 */
public interface IValueObject {

}
