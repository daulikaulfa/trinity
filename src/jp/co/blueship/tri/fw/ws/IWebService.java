package jp.co.blueship.tri.fw.ws;

import javax.ws.rs.core.Response;

/**
 * REST Webサービスを介してtrinityのDBデータ(リソース)へアクセスするためのFacade実装を規定したインタフェースです。
 *
 * @author Takayuki Kubo
 *
 * @param <V> リソースの追加・更新・参照で使用するValueObjectクラスの型
 * @param <S> リソースの検索クエリパラメタを格納するクラスの型
 * @param <P> リソースの主キーを表現するクラスの型
 */
public interface IWebService<V extends IValueObject, S extends ISearchConditions, P extends IPrimaryKey> {

	/**
	 * 主キーを指定してリソースを1件取得します。
	 *
	 * @param primaryKey 主キー
	 * @return リソースオブジェクトをラップしたレスポンス
	 */
	public Response get(P primaryKey);

	/**
	 * リソースをすべて返します。
	 *
	 * @return リソース一覧をラップしたレスポンス。リソースが0件の場合空のリスト。
	 */
	public Response list();

	/**
	 * 検索条件を指定してリソースを検索します。
	 *
	 * @param 検索クエリパラメタを格納したオブジェクト
	 * @return 検索結果をラップしたレスポンス。検索結果が0件の場合空のリスト。
	 */
	public Response find(S conditions);

	/**
	 * リソースを1件追加します。
	 *
	 * @param vo 追加するリソースの属性を格納したValueObject
	 * @return 追加されたリソースの件数をラップしたレスポンス
	 */
	public Response insert(V vo);

	/**
	 * リソースを1件更新します。<br/>
	 * 指定されたValueObjectにおいて、nullが設定されているプロパティは更新の対象とはならずに<br/>
	 * 無視されます。
	 *
	 * @param vo 更新するリソースの属性を格納したValueObject
	 * @return 更新されたリソースの件数をラップしたレスポンス
	 */
	public Response update(V vo);

	/**
	 * 指定された主キーを持つリソースを1件削除します。
	 *
	 * @param primaryKey 主キー
	 * @return 削除されたリソースの件数をラップしたレスポンス
	 */
	public Response delete(P primaryKey);
}
