package jp.co.blueship.tri.fw.ws;

import java.util.List;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;

/**
 * trinityのWebサービスで例外が発生した場合にクライアントへ返却するエラー情報を生成するためのクラスです。
 *
 * @author Takayuki Kubo
 *
 */
@Provider
public class BaseSysExceptionMapper implements ExceptionMapper<TriRuntimeException> {

	/**
	 * trinityで発生したBaseSysExceptionの派生例外の内容をHTTPレスポンスへ変換します。
	 *
	 *
	 * @see javax.ws.rs.ext.ExceptionMapper#toResponse(E)
	 */
	@Override
	public Response toResponse(TriRuntimeException e) {

		return Response.//
				status(deriveStatus(e)).//
				entity(interpolatorMessageFrom(e)).//
				build();
	}

	/**
	 * 発生した例外をもとに、クライアントへ通知するHTTPステータスコードを導出します。
	 *
	 * @param e 例外
	 * @return HTTPステータスコード。<br/>
	 *         ContinuableBusinessExceptionまたはBusinessExceptionの場合は400、
	 *         その他の場合は500。
	 */
	private static Response.Status deriveStatus(TriRuntimeException e) {

		if (ContinuableBusinessException.class.isInstance(e) //
				|| BusinessException.class.isInstance(e)) {
			return Response.Status.BAD_REQUEST;
		}

		return Response.Status.INTERNAL_SERVER_ERROR;
	}

	private List<String> interpolatorMessageFrom(ITranslatable e) {

		return context().getMessage(e);
	}

	private IContextAdapter context() {
		return ContextAdapterFactory.getContextAdapter();
	}
}
