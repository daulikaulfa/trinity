package jp.co.blueship.tri.fw.ws;

import java.util.List;

import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;
import jp.co.blueship.tri.fw.msg.IMessageId;

/**
 * REST Webサービスを介してDomain ServiceへアクセスするためのFacade実装の抽象クラスです。
 *
 * @param <V> リソースの追加・更新・参照で使用するValueObjectクラスの型
 * @param <S> リソースの検索クエリパラメタを格納するクラスの型
 * @param <P> リソースの主キーを表現するクラスの型
 *
 * @version V3L10.01
 * @author Takayuki Kubo
 *
 * @version V3L11.01
 * @author Yukihiro Eguchi
 */
public abstract class TriWebService<V extends IValueObject, S extends ISearchConditions, P extends IPrimaryKey> implements IWebService<V, S, P> {

	private IContextAdapter contextAdapter;

	/**
	 * ContextAdapterをインジェクションします。
	 *
	 * @param contextAdapter コンテキストアダプタ
	 */
	@Autowired
	@Qualifier("context")
	public void setContextAdapter(IContextAdapter contextAdapter) {
		this.contextAdapter = contextAdapter;
	}

	/**
	 * コンテキストアダプタを返します。
	 *
	 * @return コンテキストアダプタ
	 */
	protected IContextAdapter context() {
		return contextAdapter;
	}

	protected String message(IMessageId messageId, String... messageParams) {

		try {
			//return ContextAdapterFactory.getContextAdapter().getMessage(messageId, messageParams);
			return this.context().getMessage(messageId, messageParams);
		} catch (Exception e) {
			return e.getMessage();
		}
	}

	/**
	 * 1件の検索結果をラップしたレスポンスオブジェクトを構築して返します。
	 *
	 * @param resultVo 検索結果
	 * @return レスポンスオブジェクト
	 */
	protected Response responseAsSingleVo(V resultVo) {
		return Response.status(Response.Status.OK).//
				entity(resultVo).//
				build();
	}

	/**
	 * 検索結果リストをラップしたレスポンスオブジェクトを構築して返します。
	 *
	 * @param resultList 検索結果リスト
	 * @return レスポンスオブジェクト
	 */
	protected Response responseAsList(List<V> resultList) {
		return Response.status(Response.Status.OK).//
				entity(resultList).//
				build();
	}

	/**
	 * 検索結果リストをラップしたレスポンスオブジェクトを構築して返します。
	 *
	 * @param value 結果
	 * @return レスポンスオブジェクト
	 */
	protected Response responseAsString(String value) {
		return Response.status(Response.Status.OK).//
				entity(value).//
				build();
	}

	/**
	 * Integer型の結果をラップしたレスポンスオブジェクトを構築して返します。
	 *
	 * @param value Integer型の結果
	 * @return レスポンスオブジェクト
	 */
	protected Response responseAsInteger(int value) {

		return Response.status(Response.Status.OK).//
				entity(value).//
				build();
	}

	/**
	 * 例外に応じてレスポンス情報を生成して返します。
	 *
	 * @param e 例外
	 * @return レスポンス情報
	 */
	protected Response handleException(Exception e) {

		if (BaseBusinessException.class.isInstance(e)) {
			return responseAsBusinessErrorWithHTTPStatus400((ITranslatable)e);
		}

		if ( ITranslatable.class.isInstance(e) ) {
			return responseAsErrorWithHTTPStatus500((ITranslatable)e);
		}

		return Response.status(Response.Status.INTERNAL_SERVER_ERROR).//
				entity(e.getMessage()).//
				build();
	}

	/**
	 * エラー情報レスポンスを構築して返します。
	 *
	 * @param e 例外オブジェクト
	 * @return エラー情報レスポンスオブジェクト(HTTP ステータスは400)
	 */
	protected Response responseAsBusinessErrorWithHTTPStatus400(ITranslatable e) {
		return Response.status(Response.Status.BAD_REQUEST).//
				entity(this.message( e.getMessageID(), e.getMessageArgs() )).//
				build();
	}

	/**
	 * エラー情報レスポンスを構築して返します。
	 *
	 * @param e 例外オブジェクト
	 * @return エラー情報レスポンスオブジェクト(HTTP ステータスは500)
	 */
	protected Response responseAsErrorWithHTTPStatus500(ITranslatable e) {
		return Response.status(Response.Status.INTERNAL_SERVER_ERROR).//
				entity(this.message( e.getMessageID(), e.getMessageArgs() )).//
				build();
	}

}
