package jp.co.blueship.tri.fw.ws;

/**
 * trinity Webサービスにおけるエラーレスポンスクラスです。
 *
 * @author Takayuki Kubo
 *
 */
public class TriWSError {

	private String errorCode;
	private String errorMessage;

	public TriWSError(String errorCode, String errorMessage) {
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

}
