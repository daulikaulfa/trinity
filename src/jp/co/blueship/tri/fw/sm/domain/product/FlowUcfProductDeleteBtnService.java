package jp.co.blueship.tri.fw.sm.domain.product;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.sm.dao.plmgt.IPlMgtDao;
import jp.co.blueship.tri.fw.sm.dao.plmgt.eb.IPlMgtEntity;
import jp.co.blueship.tri.fw.sm.dao.plmgt.eb.PlMgtCondition;
import jp.co.blueship.tri.fw.sm.domain.product.dto.FlowUcfProductDeleteBtnServiceBean;
import jp.co.blueship.tri.fw.sm.domain.product.dto.FlowUcfProductListServiceBean.ProductListViewBean;

public class FlowUcfProductDeleteBtnService implements IDomain<FlowUcfProductDeleteBtnServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IPlMgtDao plMgtDao;
	public void setPlMgtDao(IPlMgtDao plMgtDao) {
		this.plMgtDao = plMgtDao;
	}

	public FlowUcfProductDeleteBtnService() {
	}

	@Override
	public IServiceDto<FlowUcfProductDeleteBtnServiceBean> execute( IServiceDto<FlowUcfProductDeleteBtnServiceBean> serviceDto ) {

		FlowUcfProductDeleteBtnServiceBean paramBean = null;

		try {
			paramBean	= serviceDto.getServiceBean();

			if ( null == paramBean ){
				throw new TriSystemException( SmMessageId.SM005111S );
			}
			PlMgtCondition condition = new PlMgtCondition();
			condition.setProductId(paramBean.getProductId());
			condition.setCpNm(paramBean.getComputerName());
			plMgtDao.delete(condition.getCondition()) ;

			PlMgtCondition allCondition = new PlMgtCondition();
			List<IPlMgtEntity> entitys = plMgtDao.find(allCondition.getCondition());

			List<ProductListViewBean> list = new ArrayList<ProductListViewBean>();

			for ( IPlMgtEntity entity : entitys ) {
				ProductListViewBean viewBean = paramBean.newListViewBean();
				viewBean.setProductId( entity.getProductId() ) ;
				viewBean.setComputerName(entity.getCpNm());

				viewBean.setSerialNumber(entity.getSerialNo());
				viewBean.setComment(entity.getRemarks());
				viewBean.setAgentLicense( entity.getAgentPlKey());
				list.add( viewBean );
			}

			paramBean.setViewBeanList(list);

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( SmMessageId.SM005114S, e );
		}
	}

}
