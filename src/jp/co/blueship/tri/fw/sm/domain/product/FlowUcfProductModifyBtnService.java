package jp.co.blueship.tri.fw.sm.domain.product;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.sm.dao.plmgt.constants.PlMgtItems;
import jp.co.blueship.tri.fw.sm.SmLicenseValidationUtils;
import jp.co.blueship.tri.fw.sm.dao.plmgt.IPlMgtDao;
import jp.co.blueship.tri.fw.sm.dao.plmgt.eb.IPlMgtEntity;
import jp.co.blueship.tri.fw.sm.dao.plmgt.eb.PlMgtCondition;
import jp.co.blueship.tri.fw.sm.domain.product.dto.FlowUcfProductListServiceBean.ProductListViewBean;
import jp.co.blueship.tri.fw.sm.domain.product.dto.FlowUcfProductModifyBtnServiceBean;

public class FlowUcfProductModifyBtnService implements IDomain<FlowUcfProductModifyBtnServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IPlMgtDao plMgtDao = null;

	public FlowUcfProductModifyBtnService() {
	}

	public void setPlMgtDao(IPlMgtDao plMgtDao) {
		this.plMgtDao = plMgtDao;
	}

	@Override
	public IServiceDto<FlowUcfProductModifyBtnServiceBean> execute(IServiceDto<FlowUcfProductModifyBtnServiceBean> serviceDto) {

		// 画面の入力値を取得
		FlowUcfProductModifyBtnServiceBean paramBean = null;

		try {
			// 型を変更
			paramBean = serviceDto.getServiceBean();

			if (null == paramBean) {
				throw new TriSystemException(SmMessageId.SM005111S);
			}
			paramBean.setKey1(paramBean.getKey1());
			paramBean.setKey2(paramBean.getKey2());
			paramBean.setKey3(paramBean.getKey3());
			paramBean.setKey4(paramBean.getKey4());
			String inKey = combineKey(paramBean.getKey1(), paramBean.getKey2(), paramBean.getKey3(), paramBean.getKey4());

			String agentLicense = null;
			if (paramBean.isRm()) {
				paramBean.setKey5(paramBean.getKey5());
				paramBean.setKey6(paramBean.getKey6());
				paramBean.setKey7(paramBean.getKey7());
				paramBean.setKey8(paramBean.getKey8());
				agentLicense = combineKey(paramBean.getKey5(), paramBean.getKey6(), paramBean.getKey7(), paramBean.getKey8());
			}

			// 入力チェック
			SmLicenseValidationUtils.checkLicenseInput(paramBean.getProductId(), inKey, agentLicense, paramBean.getComment());

			// 既存レコードを取得
			PlMgtCondition condition = new PlMgtCondition();
			condition.setProductId(paramBean.getProductId());
			condition.setCpNm(paramBean.getComputerName());
			IPlMgtEntity productEntity = plMgtDao.findByPrimaryKey(condition.getCondition());

			//  画面の値をproductEntityセットする
			productEntity.setPlKey(inKey);
			productEntity.setAgentPlKey(agentLicense);
			productEntity.setRemarks(paramBean.getComment());
			int ret = plMgtDao.update(productEntity);
			if (ret != 1) {
				throw new ContinuableBusinessException(SmMessageId.SM001008E);
			}
			paramBean.setProductId(paramBean.getProductId());
			paramBean.setComputerName(paramBean.getComputerName());
			paramBean.setSerialNumber(productEntity.getSerialNo());
			paramBean.setComment(productEntity.getRemarks());
			paramBean.setProductKey(inKey);
			paramBean.setAgentLicense(agentLicense);

			// 一覧を取得
			condition = new PlMgtCondition();
			ISqlSort sort = new SortBuilder().setElement(PlMgtItems.regTimestamp, TriSortOrder.Asc, 1);
			List<IPlMgtEntity> entities = plMgtDao.find(condition.getCondition(), sort);
			List<ProductListViewBean> list = new ArrayList<ProductListViewBean>();
			for (IPlMgtEntity entity : entities) {
				ProductListViewBean viewBean = paramBean.newListViewBean();
				viewBean.setProductId(entity.getProductId());
				viewBean.setComputerName(entity.getCpNm());
				viewBean.setSerialNumber(entity.getSerialNo());
				viewBean.setAgentLicense(entity.getAgentPlKey());
				viewBean.setComment(entity.getRemarks());
				list.add(viewBean);
			}

			paramBean.setViewBeanList(list);
			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(SmMessageId.SM005118S, e);
		}
	}

	private String combineKey(String key1, String key2, String key3, String key4) {
		StringBuffer buf = new StringBuffer();
		buf.append(key1);
		buf.append(key2);
		buf.append(key3);
		buf.append(key4);
		return buf.toString();
	}
}
