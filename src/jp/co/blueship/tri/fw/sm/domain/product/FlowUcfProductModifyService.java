package jp.co.blueship.tri.fw.sm.domain.product;

import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.sm.dao.plmgt.IPlMgtDao;
import jp.co.blueship.tri.fw.sm.dao.plmgt.eb.IPlMgtEntity;
import jp.co.blueship.tri.fw.sm.dao.plmgt.eb.PlMgtCondition;
import jp.co.blueship.tri.fw.sm.domain.product.dto.FlowUcfProductModifyServiceBean;

public class FlowUcfProductModifyService implements IDomain<FlowUcfProductModifyServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IPlMgtDao plMgtDao = null;

	public FlowUcfProductModifyService() {
		IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
		plMgtDao = (IPlMgtDao)ca.getBean( "smPlMgtDao" );
	}

	@Override
	public IServiceDto<FlowUcfProductModifyServiceBean> execute( IServiceDto<FlowUcfProductModifyServiceBean> serviceDto ) {

		FlowUcfProductModifyServiceBean paramBean = null;

		try {
			paramBean	= serviceDto.getServiceBean();

			if ( null == paramBean ){
				throw new TriSystemException( SmMessageId.SM005111S );
			}
			PlMgtCondition condition = new PlMgtCondition();
			condition.setProductId(paramBean.getProductId());
			condition.setCpNm(paramBean.getComputerName());
			IPlMgtEntity entity = plMgtDao.findByPrimaryKey( condition.getCondition() );

			paramBean.setProductId(paramBean.getProductId());
			paramBean.setComputerName(paramBean.getComputerName());
			if (entity != null) {
				paramBean.setSerialNumber(entity.getSerialNo());
				paramBean.setComment(entity.getRemarks());
				String key = entity.getPlKey();
				if (key != null) {
					if ( key.length() == 32) {
						paramBean.setProductKey(entity.getPlKey());
						paramBean.setKey1(key.substring(0, 8));
						paramBean.setKey2(key.substring(8, 16));
						paramBean.setKey3(key.substring(16, 24));
						paramBean.setKey4(key.substring(24, 32));
					}
				}
				String agentLicense = entity.getAgentPlKey();
				if (agentLicense != null ) {
					if ( agentLicense.length() == 32) {
						paramBean.setAgentLicense(entity.getAgentPlKey());
						paramBean.setKey5(agentLicense.substring(0, 8));
						paramBean.setKey6(agentLicense.substring(8, 16));
						paramBean.setKey7(agentLicense.substring(16, 24));
						paramBean.setKey8(agentLicense.substring(24, 32));
					}
				}
			} else {
				paramBean.setKey1("");
				paramBean.setKey2("");
				paramBean.setKey3("");
				paramBean.setKey4("");
				paramBean.setKey5("");
				paramBean.setKey6("");
				paramBean.setKey7("");
				paramBean.setKey8("");
			}

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( SmMessageId.SM005119S, e );
		}
	}


}
