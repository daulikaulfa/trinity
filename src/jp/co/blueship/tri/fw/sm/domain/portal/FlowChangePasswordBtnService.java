package jp.co.blueship.tri.fw.sm.domain.portal;

import static jp.co.blueship.tri.fw.cmn.utils.EncryptionUtils.*;
import static jp.co.blueship.tri.fw.security.spec.SecFunctions.*;
import static jp.co.blueship.tri.fw.security.spec.SecPredicates.*;
import static jp.co.blueship.tri.fw.security.spec.SecSpecifications.*;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang.math.NumberUtils;

import jp.co.blueship.tri.fw.cmn.utils.DesignSheetUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.domain.Service;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.sm.domain.portal.dto.FlowChangePasswordBtnServiceBean;
import jp.co.blueship.tri.fw.um.dao.user.IUserDao;
import jp.co.blueship.tri.fw.um.dao.user.IUserPassHistDao;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserPassHistEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserPassHistCondition;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserPassHistEntity;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 * パスワード変更を行うサービスクラスです。
 *
 */
public class FlowChangePasswordBtnService extends Service implements IDomain<FlowChangePasswordBtnServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private IUserDao userDao;

	public void setUserDao(IUserDao userDao) {
		this.userDao = userDao;
	}
	private IUserPassHistDao userPasswordHistoryDao;

	public void setUserPassHistDao(IUserPassHistDao userPasswordHistoryDao) {
		this.userPasswordHistoryDao = userPasswordHistoryDao;
	}

	private IUmFinderSupport umFinderSupport;
	public void setUmFinderSupport(IUmFinderSupport umFinderSupport) {
		this.umFinderSupport = umFinderSupport;
	}

	@Override
	public IServiceDto<FlowChangePasswordBtnServiceBean> execute(IServiceDto<FlowChangePasswordBtnServiceBean> serviceDto) {

		FlowChangePasswordBtnServiceBean paramBean = null;

		try {
			paramBean = serviceDto.getServiceBean();

			{
				// 注意書き
				List<IMessageId> commentList = new ArrayList<IMessageId>();
				List<String[]> commentArgsList = new ArrayList<String[]>();

				commentList.add(SmMessageId.SM003010I);
				commentArgsList.add(new String[] {});

				paramBean.setInfoCommentIdList(commentList);
				paramBean.setInfoCommentArgsList(commentArgsList);
			}

			if (ScreenType.bussinessException.equals(paramBean.getScreenType())) {
				return serviceDto;
			}

			IUserEntity entity = umFinderSupport.findUserByUserId(paramBean.getUserId() );

			List<IUserPassHistEntity> passwordEntities = umFinderSupport.findUserPassHistByUserId( paramBean.getUserId() );
			// パスワード世代数
			int historyNumber = getHistoryNumber();

			// 入力チェック
			String encryptedNewPassword = checkPassword(paramBean, entity, passwordEntities, historyNumber);

			// データの作成
			passwordEntities = initUserPasswordHistory(entity, passwordEntities, historyNumber);

			// ユーザ情報更新
			String passwordLimit = updateUserBasic(paramBean, entity, encryptedNewPassword);
			paramBean.setPasswordLimit(passwordLimit);

			// パスワード履歴更新
			updateUserPasswordHistory(entity, passwordEntities, encryptedNewPassword, historyNumber);

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(SmMessageId.SM005125S, e);
		}

		return serviceDto;
	}

	/**
	 * 初めてのパスワード変更の場合に現在のパスワードでパスワード履歴テーブルのデータを作成する。
	 *
	 * @param entity
	 * @param passwordEntities
	 * @param historyNumber
	 */
	private List<IUserPassHistEntity> initUserPasswordHistory(IUserEntity entity, List<IUserPassHistEntity> passwordEntities, int historyNumber) {

		// すでにデータがある場合はパス
		if (0 < passwordEntities.size())
			return passwordEntities;
		List<IUserPassHistEntity> userPassHistEntityList = new ArrayList<IUserPassHistEntity>();
		// 履歴をとらない場合
		if (0 == historyNumber)
			return userPassHistEntityList;

		IUserPassHistEntity passwordEntity = new UserPassHistEntity();
		passwordEntity.setUserId(entity.getUserId());
		passwordEntity.setPassHistSeqNo("0");
		passwordEntity.setUserPass(entity.getUserPass());
		userPassHistEntityList.add(passwordEntity);

		return userPassHistEntityList;
	}

	/**
	 * 入力されたパスワードのチェックを行う
	 *
	 * @param paramBean
	 * @param entity
	 * @param passwordEntities
	 * @param historyNumber
	 */
	private String checkPassword(FlowChangePasswordBtnServiceBean paramBean, IUserEntity entity, List<IUserPassHistEntity> passwordEntities,
			int historyNumber) {

		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]> messageArgsList = new ArrayList<String[]>();

		// 旧パスワードでの認証
		authenticateByOldPassword(entity, paramBean.getOldPassword(), messageList, messageArgsList);

		// 新パスワードの入力チェック
		String password = paramBean.getPassword();
		if (TriStringUtils.isEmpty(password)) {
			messageList.add(SmMessageId.SM003003I);
			messageArgsList.add(new String[] {});
		} else {
			// 新パスワードの入力許可文字のチェック
			if (!TriStringUtils.matches(password, DesignSheetUtils.getPasswordAllowPattern())) {
				messageList.add(SmMessageId.SM003010I);
				messageArgsList.add(new String[] {});
			}

			// 新パスワードの文字列長チェック
			String minPasswordLength = DesignSheetUtils.getPasswordMinimumLength();
			String maxPasswordLength = DesignSheetUtils.getPasswordMaxLength();
			int passMinLen = 0;
			int passMaxLen = 0;
			if (NumberUtils.isNumber(minPasswordLength)) {
				passMinLen = Integer.parseInt(minPasswordLength);
			}
			if (NumberUtils.isNumber(maxPasswordLength)) {
				passMaxLen = Integer.parseInt(maxPasswordLength);
			}
			if (0 < passMinLen && password.length() < passMinLen) {
				messageList.add(SmMessageId.SM003004I);
				messageArgsList.add(new String[] { String.valueOf(passMinLen) });
			}
			if (password.length() > passMaxLen) {
				messageList.add(SmMessageId.SM003001I);
				messageArgsList.add(new String[] { String.valueOf(passMaxLen) });
			}

			// 新パスワードと確認用新パスワードのチェック
			String rePassword = paramBean.getRePassword();
			if (!password.equals(rePassword)) {
				messageList.add(SmMessageId.SM003005I);
				messageArgsList.add(new String[] {});
			}

			// 新パスワードと旧パスワードのチェック
			if (password.equals(paramBean.getOldPassword())) {
				messageList.add(SmMessageId.SM003006I);
				messageArgsList.add(new String[] {});
			}

			// 履歴との比較
			if(isExistentInHistory(paramBean.getUserId(), paramBean.getPassword(), passwordEntities, historyNumber)) {
				messageList.add(SmMessageId.SM003009I);
				messageArgsList.add(new String[] { String.valueOf(historyNumber) });
			}
		}

		if (0 != messageList.size())
			throw new ContinuableBusinessException(messageList, messageArgsList);

		return encodePasswordInA1Format(paramBean.getUserId(), password);

	}

	private static void authenticateByOldPassword(IUserEntity entity, String oldPassword, List<IMessageId> messageList, List<String[]> messageArgsList) {

		if (specOfMatchingPasswordInSHAWith(entity.getUserPass()).isSpecified(oldPassword)
				|| specOfMatchingPasswordInMD5A1FormatWith(entity.getUserPass(), entity.getUserId()).isSpecified(oldPassword)) {
			return;
		}

		messageList.add(SmMessageId.SM003002I);
		messageArgsList.add(new String[] {});
	}

	private static boolean isExistentInHistory(String userID, String newPassword, List<IUserPassHistEntity> passwordHistories, int historyLimit) {

		if(historyLimit <= 0) {
			return false;
		}

		if (TriStringUtils.isEmpty(passwordHistories)) {
			return false;
		}

		return FluentList.from(passwordHistories).//
				limit(historyLimit).//
				rejectFirst().//
				map(PASS_HIST_TO_PASSWORD).//
				contains(isMatchedInSHAWith(newPassword).or(isMatchedInMD5A1FormatWith(newPassword, userID)));
	}

	/**
	 * ユーザ情報を更新する
	 *
	 * @param paramBean
	 */
	private String updateUserBasic(FlowChangePasswordBtnServiceBean paramBean, IUserEntity entity, String encryptedNewPassword) {

		IUserEntity userEntity = new UserEntity();

		userEntity.setUserId(entity.getUserId());

		// パスワード
		userEntity.setUserPass(encryptedNewPassword);

		// パスワード有効期間
		String defaultTerm = DesignSheetUtils.getPasswordDefaultEffectiveTerm();
		Timestamp passwordLimit = getPasswordEffectiveDate(defaultTerm);

		userEntity.setPassTimeLimit( passwordLimit );

		this.userDao.update(userEntity);

		return defaultTerm;
	}

	/**
	 * パスワード有効日時を取得する
	 *
	 * @param paramBean
	 * @return
	 */
	private Timestamp getPasswordEffectiveDate(String termId) {

		// 期間未指定、またはマイナス値の場合は有効期限の制限なし
		if (null != termId) {

			int termInt = Integer.parseInt(termId);

			if (0 <= termInt) {

				Calendar now = Calendar.getInstance();
				now.add(Calendar.DATE, termInt);

				return  new Timestamp(now.getTimeInMillis());
			}
		}

		return null;

	}

	/**
	 * ユーザパスワード履歴情報を更新する
	 *
	 * @param paramBean
	 * @param passwordEntities
	 * @param historyNumber
	 */
	private void updateUserPasswordHistory(IUserEntity entity, List<IUserPassHistEntity> passwordEntities, String encryptedNewPassword,
			int historyNumber) {

		// とりあえず全消し
		UserPassHistCondition condition = new UserPassHistCondition();
		condition.setUserId(entity.getUserId());
		this.userPasswordHistoryDao.delete(condition.getCondition());

		// 履歴をとらない場合
		if (0 == historyNumber)
			return;

		List<IUserPassHistEntity> insertEntityList = new ArrayList<IUserPassHistEntity>();

		IUserPassHistEntity passwordEntity = new UserPassHistEntity();
		passwordEntity.setUserId(entity.getUserId());
		passwordEntity.setPassHistSeqNo("0");
		passwordEntity.setUserPass(encryptedNewPassword);

		insertEntityList.add(passwordEntity);

		if (1 < historyNumber) {

			for (int i = 1; i < historyNumber; i++) {

				if (passwordEntities.size() < i)
					break;

				passwordEntity = new UserPassHistEntity();
				passwordEntity.setUserId(entity.getUserId());
				passwordEntity.setPassHistSeqNo( Integer.toString(i) );
				passwordEntity.setUserPass(passwordEntities.get(i-1).getUserPass());

				insertEntityList.add(passwordEntity);
			}
		}

		for (IUserPassHistEntity insertEntity : insertEntityList) {
			this.userPasswordHistoryDao.insert(insertEntity);
		}
	}

	private int getHistoryNumber() {

		String historyNum = DesignSheetUtils.getPasswordHistoryNumber();

		int historyNumber = 0;
		if (NumberUtils.isNumber(historyNum)) {
			historyNumber = Integer.parseInt(historyNum);
		}

		return historyNumber;
	}

}
