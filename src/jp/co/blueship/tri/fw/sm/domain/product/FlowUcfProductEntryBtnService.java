package jp.co.blueship.tri.fw.sm.domain.product;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.sm.SmLicenseValidationUtils;
import jp.co.blueship.tri.fw.sm.dao.plmgt.IPlMgtDao;
import jp.co.blueship.tri.fw.sm.dao.plmgt.eb.IPlMgtEntity;
import jp.co.blueship.tri.fw.sm.dao.plmgt.eb.PlMgtCondition;
import jp.co.blueship.tri.fw.sm.dao.plmgt.eb.PlMgtEntity;
import jp.co.blueship.tri.fw.sm.domain.product.dto.FlowUcfProductEntryBtnServiceBean;
import jp.co.blueship.tri.fw.sm.domain.product.dto.FlowUcfProductListServiceBean.ProductListViewBean;

public class FlowUcfProductEntryBtnService implements IDomain<FlowUcfProductEntryBtnServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IPlMgtDao plMgtDao;

	public void setPlMgtDao(IPlMgtDao plMgtDao) {
		this.plMgtDao = plMgtDao;
	}

	public FlowUcfProductEntryBtnService() {
	}

	@Override
	public IServiceDto<FlowUcfProductEntryBtnServiceBean> execute( IServiceDto<FlowUcfProductEntryBtnServiceBean> serviceDto ) {

		//画面の入力値を取得
		FlowUcfProductEntryBtnServiceBean paramBean = null;

		try {
			//型を変更
			paramBean	= serviceDto.getServiceBean();

			if ( null == paramBean ){
				throw new TriSystemException( SmMessageId.SM005111S );
			}
			// 入力チェック
			SmLicenseValidationUtils.checkLicenseInput(paramBean.getProductId(), paramBean.getProductKey(), paramBean.getAgentLicense(), paramBean.getComment());

			// 重複チェック
			PlMgtCondition checkCondition = new PlMgtCondition();
			checkCondition.setProductId(paramBean.getProductId());
			checkCondition.setCpNm(paramBean.getComputerName());
			IPlMgtEntity checkEntity = plMgtDao.findByPrimaryKey(checkCondition.getCondition());
			if (checkEntity != null) {
				throw new ContinuableBusinessException( SmMessageId.SM001007E );
			}

			// 画面の値をproductEntityセットする
			IPlMgtEntity productEntity = new PlMgtEntity();
			productEntity.setProductId( paramBean.getProductId()) ;
			productEntity.setCpNm(paramBean.getComputerName());
			productEntity.setSerialNo(paramBean.getSerialNumber());
			productEntity.setRemarks(paramBean.getComment());
			productEntity.setPlKey(paramBean.getProductKey());
			productEntity.setAgentPlKey( paramBean.getAgentLicense());
			int ret = 0;
			ret = plMgtDao.insert(productEntity);
			if (ret != 1) {
				throw new ContinuableBusinessException( SmMessageId.SM001008E );
			}
			paramBean.setProductId( paramBean.getProductId() );
			paramBean.setComputerName(paramBean.getComputerName());
			paramBean.setSerialNumber(paramBean.getSerialNumber());
			paramBean.setComment(paramBean.getComment());
			paramBean.setKey1(paramBean.getKey1());
			paramBean.setKey2(paramBean.getKey2());
			paramBean.setKey3(paramBean.getKey3());
			paramBean.setKey4(paramBean.getKey4());
			paramBean.setProductKey(paramBean.getProductKey());
			paramBean.setKey5(paramBean.getKey5());
			paramBean.setKey6(paramBean.getKey6());
			paramBean.setKey7(paramBean.getKey7());
			paramBean.setKey8(paramBean.getKey8());
			paramBean.setAgentLicense( paramBean.getAgentLicense());
			// 一覧を取得
			PlMgtCondition condition = new PlMgtCondition();
			List<IPlMgtEntity> entitys = plMgtDao.find(condition.getCondition());
			List<ProductListViewBean> list = new ArrayList<ProductListViewBean>();
			for ( IPlMgtEntity entity : entitys ) {
				ProductListViewBean viewBean = paramBean.newListViewBean();
				viewBean.setProductId( entity.getProductId());
				viewBean.setComputerName(entity.getCpNm());
				viewBean.setSerialNumber(entity.getSerialNo());
				viewBean.setComment(entity.getRemarks());
				viewBean.setAgentLicense( entity.getAgentPlKey());
				list.add( viewBean );
			}

			paramBean.setViewBeanList(list);
			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( SmMessageId.SM005116S, e );
		}
	}

}

