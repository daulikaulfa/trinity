package jp.co.blueship.tri.fw.sm.domain.product;

import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.sm.domain.product.dto.FlowUcfProductEntryServiceBean;

public class FlowUcfProductEntryService implements IDomain<FlowUcfProductEntryServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	public FlowUcfProductEntryService() {
	}

	@Override
	public IServiceDto<FlowUcfProductEntryServiceBean> execute( IServiceDto<FlowUcfProductEntryServiceBean> serviceDto ) {

		//画面の入力値を取得
		FlowUcfProductEntryServiceBean paramBean = null;

		try {
			//型を変更
			paramBean	= serviceDto.getServiceBean();
			if ( null == paramBean ){
				throw new TriSystemException( SmMessageId.SM005111S );
			}
			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( SmMessageId.SM005116S, e );
		}
	}

}

