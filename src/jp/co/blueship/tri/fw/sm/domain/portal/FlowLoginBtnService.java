package jp.co.blueship.tri.fw.sm.domain.portal;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.constants.UmDesignEntryKeyByCommon;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.Service;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.sm.domain.portal.dto.FlowLoginBtnServiceBean;
import jp.co.blueship.tri.fw.sm.domain.product.ProductActivate;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 * ログインを行うサービスクラスです。
 *
 */
public class FlowLoginBtnService extends Service implements IDomain<FlowLoginBtnServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IUmFinderSupport umFinderSupport;
	public void setUmFinderSupport(IUmFinderSupport umFinderSupport) {
		this.umFinderSupport = umFinderSupport;
	}

	@Override
	public IServiceDto<FlowLoginBtnServiceBean> execute(IServiceDto<FlowLoginBtnServiceBean> serviceDto) {

		FlowLoginBtnServiceBean paramBean = null;

		try {
			paramBean = serviceDto.getServiceBean();
			IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

			{
				// 注意書き
				List<IMessageId> commentList = new ArrayList<IMessageId>();
				List<String[]> commentArgsList = new ArrayList<String[]>();

				commentList.add(SmMessageId.SM003010I);
				commentArgsList.add(new String[] {});

				paramBean.setInfoCommentIdList(commentList);
				paramBean.setInfoCommentArgsList(commentArgsList);
			}

			IUserEntity entity = umFinderSupport.findUserByUserId( paramBean.getUserId() );
			throwIfInvalidUser(entity);

			paramBean.setUserName(entity.getUserNm());
			paramBean.setProjectName(sheet.getValue(UmDesignEntryKeyByCommon.projectName));
			paramBean.setVersion(sheet.getValue(UmDesignEntryKeyByCommon.version));

			// パスワード有効期限
			Timestamp now = new Timestamp(System.currentTimeMillis());
			Timestamp passwordLimit = entity.getPassTimeLimit();
			Boolean isPasswordEffect = true;

			if (null != passwordLimit && passwordLimit.getTime() <= now.getTime() && ProductActivate.isRmActive()) {

				isPasswordEffect = false;

				paramBean.setInfoMessage(SmMessageId.SM003007I);
			}

			paramBean.setPasswordEffect(isPasswordEffect);

			// パスワード変更画面からの遷移
			if ("ChangePasswordBtn".equals(paramBean.getReferer())) {

				String passswordLimitStr = getPasswordEffectiveLimit(passwordLimit);

				paramBean.setInfoMessage(SmMessageId.SM003008I);
				paramBean.setInfoMessageArgs(new String[] { passswordLimitStr });
			}

		} catch (Exception e) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(SmMessageId.SM005113S, e);
		}

		return serviceDto;
	}

	/**
	 * パスワード有効期限を取得する
	 *
	 * @param termLimit
	 * @return
	 */
	private String getPasswordEffectiveLimit(Timestamp termLimit) {

		if (null == termLimit)
			return null;

		return TriDateUtils.getViewDateFormat().format(new Date(termLimit.getTime()));

	}

	private void throwIfInvalidUser(IUserEntity entity) {

		if (entity != null) {
			return;
		}

		throw new TriSystemException(SmMessageId.SM005004S, UmTables.UM_USER.name());
	}

}
