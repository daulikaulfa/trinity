package jp.co.blueship.tri.fw.sm.domain.product.dto;

import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;

public class FlowUcfProductDeleteServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;

	private String productId;
	private String computerName;
	private String inSerialNumber;
	private String serialNumber;
	private String comment;
	private String productKey;
	private String agentLicense;

	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String number) {
		serialNumber = number;
	}
	public String getComputerName() {
		return computerName;
	}
	public void setComputerName(String name) {
		computerName = name;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String temp) {
		comment = temp;
	}
	public String getInSerialNumber() {
		return inSerialNumber;
	}
	public void setInSerialNumber(String inSerialNumber) {
		this.inSerialNumber = inSerialNumber;
	}
	public String getProductKey() {
		return productKey;
	}
	public void setProductKey(String key) {
		productKey = key;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getAgentLicense() {
		return agentLicense;
	}
	public void setAgentLicense(String agentLicense) {
		this.agentLicense = agentLicense;
	}

}
