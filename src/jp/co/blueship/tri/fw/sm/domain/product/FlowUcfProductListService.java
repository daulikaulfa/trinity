package jp.co.blueship.tri.fw.sm.domain.product;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.sm.dao.plmgt.IPlMgtDao;
import jp.co.blueship.tri.fw.sm.dao.plmgt.constants.PlMgtItems;
import jp.co.blueship.tri.fw.sm.dao.plmgt.eb.IPlMgtEntity;
import jp.co.blueship.tri.fw.sm.dao.plmgt.eb.PlMgtCondition;
import jp.co.blueship.tri.fw.sm.domain.product.dto.FlowUcfProductListServiceBean;
import jp.co.blueship.tri.fw.sm.domain.product.dto.FlowUcfProductListServiceBean.ProductListViewBean;

/**
 * FlowUcfProductListServiceイベントのサービスClass
 * <br>
 * <p>
 * コンピュータ一覧画面の表示情報を設定する。
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2008
 */
public class FlowUcfProductListService implements IDomain<FlowUcfProductListServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IPlMgtDao plMgtDao = null;

	public FlowUcfProductListService() {
		IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
		plMgtDao = (IPlMgtDao)ca.getBean( "smPlMgtDao" );
	}

	@Override
	public IServiceDto<FlowUcfProductListServiceBean> execute( IServiceDto<FlowUcfProductListServiceBean> serviceDto ) {

		FlowUcfProductListServiceBean paramBean = null;

		try {
			paramBean	= serviceDto.getServiceBean();

			if ( null == paramBean ){
				throw new TriSystemException( SmMessageId.SM005111S );
			}
			PlMgtCondition condition = new PlMgtCondition();
			ISqlSort sort = new SortBuilder().setElement(PlMgtItems.regTimestamp, TriSortOrder.Asc, 1);
			List<IPlMgtEntity> entities = plMgtDao.find( condition.getCondition(), sort );

			List<ProductListViewBean> list = new ArrayList<ProductListViewBean>();

			for ( IPlMgtEntity entity : entities ) {
				ProductListViewBean viewBean = paramBean.newListViewBean();
				viewBean.setProductId( entity.getProductId() ) ;
				viewBean.setComputerName(entity.getCpNm());
				viewBean.setSerialNumber(entity.getSerialNo());
				viewBean.setComment(entity.getRemarks());
				viewBean.setAgentLicense( entity.getAgentPlKey());
				list.add( viewBean );
			}

			paramBean.setViewBeanList(list);

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( SmMessageId.SM005117S, e );
		}
	}

}
