package jp.co.blueship.tri.fw.sm.domain.portal;

import jp.co.blueship.tri.bm.dao.bldsrv.IBldSrvDao;
import jp.co.blueship.tri.bm.dao.bldsrv.eb.BldSrvCondition;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.sm.domain.portal.dto.FlowLoginBtnServiceBean;

/**
 * ログインを行うサービスクラスです。 <br>
 * Agentのライセンスチェックを行うためのDAO使用により、特別にFlowLoginBtnServiceを継承している。 <br>
 * このクラスを画面作成時の雛形としない事。
 *
 */
public class FlowLoginBtnWithAgentCheckService extends FlowLoginBtnService implements IDomain<FlowLoginBtnServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IBldSrvDao bldSrvDao;

	public void setBldSrvDao(IBldSrvDao bldSrvDao) {
		this.bldSrvDao = bldSrvDao;
	}

	@Override
	public IServiceDto<FlowLoginBtnServiceBean> execute(IServiceDto<FlowLoginBtnServiceBean> serviceDto) {

		FlowLoginBtnServiceBean paramBean = null;

		try {
			paramBean = serviceDto.getServiceBean();

			// ログイン処理
			super.execute(serviceDto);

			// 必要なagentのライセンス数を取得（通常のログイン時には行われない）
			int needLicenseKeyCount = this.countNeedAgentLicense();
			paramBean.setNeedLicenseKeyCount(needLicenseKeyCount);

		} catch (Exception e) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(SmMessageId.SM005113S, e);
		}

		return serviceDto;
	}

	/**
	 * agentの登録件数を取得して返す。
	 *
	 * @return agentの登録件数
	 */
	private int countNeedAgentLicense() {

		BldSrvCondition condition = new BldSrvCondition();
		condition.setIsAgent(StatusFlg.on);

		return bldSrvDao.count(condition.getCondition());
	}

}
