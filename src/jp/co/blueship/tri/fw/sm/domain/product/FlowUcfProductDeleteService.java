package jp.co.blueship.tri.fw.sm.domain.product;

import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.sm.dao.plmgt.IPlMgtDao;
import jp.co.blueship.tri.fw.sm.dao.plmgt.eb.IPlMgtEntity;
import jp.co.blueship.tri.fw.sm.dao.plmgt.eb.PlMgtCondition;
import jp.co.blueship.tri.fw.sm.domain.product.dto.FlowUcfProductDeleteServiceBean;

public class FlowUcfProductDeleteService implements IDomain<FlowUcfProductDeleteServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IPlMgtDao plMgtDao = null;
	public void setPlMgtDao(IPlMgtDao plMgtDao) {
		this.plMgtDao = plMgtDao;
	}

	public FlowUcfProductDeleteService() {
	}

	@Override
	public IServiceDto<FlowUcfProductDeleteServiceBean> execute( IServiceDto<FlowUcfProductDeleteServiceBean> serviceDto ) {

		FlowUcfProductDeleteServiceBean paramBean = null;

		try {
			paramBean	= serviceDto.getServiceBean();

			if ( null == paramBean ){
				throw new TriSystemException( SmMessageId.SM005111S );
			}
			PlMgtCondition condition = new PlMgtCondition();
			condition.setProductId(paramBean.getProductId());
			condition.setCpNm(paramBean.getComputerName());
			IPlMgtEntity entity = plMgtDao.findByPrimaryKey( condition.getCondition() );

			if (entity != null) {
				paramBean.setProductId(entity.getProductId());
				paramBean.setComputerName(entity.getCpNm());
				paramBean.setSerialNumber(entity.getSerialNo());
				paramBean.setComment(entity.getRemarks());

				paramBean.setAgentLicense( this.splitKey( entity.getAgentPlKey() ) ) ;
				paramBean.setProductKey( this.splitKey( entity.getPlKey() ) ) ;
			}

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( SmMessageId.SM005115S , e );
		}
	}

	private String splitKey( String key ) {
		StringBuffer stb = new StringBuffer();

		if (key.length() == 32) {
			stb.append(key.substring(0, 8));
			stb.append("-");
			stb.append(key.substring(8, 16));
			stb.append("-");
			stb.append(key.substring(16, 24));
			stb.append("-");
			stb.append(key.substring(24, 32));
			return stb.toString() ;
		} else {
			return key ;
		}
	}
}
