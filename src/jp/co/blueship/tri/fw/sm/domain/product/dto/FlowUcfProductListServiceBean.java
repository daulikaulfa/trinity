package jp.co.blueship.tri.fw.sm.domain.product.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;

public class FlowUcfProductListServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;
	
	
	
	/**
	 * 一覧情報
	 */
	private List<ProductListViewBean> productViewBeanList = new ArrayList<ProductListViewBean>();

	/**
	 * 新しいインスタンスを取得します。
	 * 
	 * @return 取得した情報を戻します。
	 */
	public ProductListViewBean newListViewBean() {
		ProductListViewBean bean = new ProductListViewBean();
		
		return bean;
	}

	/**
	 * 画面のリスト情報を取得します。
	 * 
	 * @return 取得したリストを戻します。
	 */
	public List<ProductListViewBean> getViewBeanList() {
		return productViewBeanList;
	}

	/**
	 * 画面のリスト情報を設定します。
	 * 
	 * @param list 設定するリスト情報で内部を上書きします。
	 */
	public void setViewBeanList(List<ProductListViewBean> list) {
		this.productViewBeanList = list;
	}
	
	public class ProductListViewBean {
		
		/** プロダクトＩＤ **/
		private String productId	  = null;
		/** コンピュータ名 **/
		private String computerName	  = null;
		/** コメント **/		
		private String comment		  = null;
		/** シリアルナンバー **/
		private String serialNumber	  = null;
		/** agentライセンス **/
		private String agentLicense	= null ;
		
		public String getComputerName() {
			return computerName;
		}
		public void setComputerName(String computerName) {
			this.computerName = computerName;
		}
		public String getComment() {
			return comment;
		}
		public void setComment(String contents) {
			this.comment = contents;
		}
		public String getSerialNumber() {
			return serialNumber;
		}
		public void setSerialNumber(String serialNumber) {
			this.serialNumber = serialNumber;
		}
		public String getProductId() {
			return productId;
		}
		public void setProductId(String productId) {
			this.productId = productId;
		}
		public String getAgentLicense() {
			return agentLicense;
		}
		public void setAgentLicense(String agentLicense) {
			this.agentLicense = agentLicense;
		}
		
	}
}
