package jp.co.blueship.tri.fw.sm.domain.portal.dto;

import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;

public class FlowChangePasswordBtnServiceBean extends GenericServiceBean {
	
	private static final long serialVersionUID = 1L;
	
	private String oldPassword	= null;
	private String password	= null;
	private String rePassword	= null;
	private String passwordLimit = null;
	
	public String getOldPassword() {
		return oldPassword;
	}
	public void setOldPassword( String oldPassword ) {
		this.oldPassword = oldPassword;
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword( String password ) {
		this.password = password;
	}
	
	public String getRePassword() {
		return rePassword;
	}
	public void setRePassword( String rePassword ) {
		this.rePassword = rePassword;
	}
	
	public String getPasswordLimit() {
		return passwordLimit;
	}
	public void setPasswordLimit( String passwordLimit ) {
		this.passwordLimit = passwordLimit;
	}
}
