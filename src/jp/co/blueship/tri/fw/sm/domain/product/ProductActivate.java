package jp.co.blueship.tri.fw.sm.domain.product;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.xml.bind.DatatypeConverter;

import jp.co.blueship.tri.fw.cmn.utils.DesignUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.EditionType;
import jp.co.blueship.tri.fw.constants.UmDesignEntryKeyByProductId;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.sm.contants.ProductLicenseExpiration;
import jp.co.blueship.tri.fw.sm.dao.plmgt.IPlMgtDao;
import jp.co.blueship.tri.fw.sm.dao.plmgt.eb.IPlMgtEntity;
import jp.co.blueship.tri.fw.sm.dao.plmgt.eb.PlMgtCondition;
import jp.co.blueship.tri.fw.sm.dao.plmgt.eb.PlMgtEntity;

/**
 * 製品ライセンス認証
 *
 * @version V3L10.02
 * @author Satoshi Sasaki
 *
 */
public class ProductActivate {
	private static IContextAdapter ac;

	public static String RM_ID = null;
	public static String DM_ID = null;

	private final static String TEMPLATE = "23456789ABCDEFGHJKLMNPQRSTUVWXYZ";
	private final static String SALT = "0A1B2C3D4E5F";

	private final static String KEYSUFFIX_PRODUCT = "P";// 正式版
	private final static String KEYSUFFIX_TERM = "T";// 期限あり版
	private final static String KEYSUFFIX_AGENT = "A";// agentライセンス

	private final static int LICENSE_MAX = 9999;
	private final static int YEAR_BEGIN = 2011;
	private final static String YEAR_END = "210001";

	private enum DaoName {
		PRODUCT_DAO("smPlMgtDao");
		private String value = null;

		DaoName(String value) {
			this.value = value;
		}

		public String toString() {
			return this.value;
		}
	}

	public static boolean init() {
		RM_ID = DesignSheetFactory.getDesignSheet().getValue(UmDesignEntryKeyByProductId.rm);
		DM_ID = DesignSheetFactory.getDesignSheet().getValue(UmDesignEntryKeyByProductId.dm);
		boolean initRm = init(RM_ID);
		boolean initDm = init(DM_ID);
		return initRm && initDm;
	}

	private static boolean init(String productId) {
		// =============================================================
		// 開発者バージョンのみ、チェック不要。
		if (EditionType.Developer.equals(DesignUtils.getEdition())) {
			return true;
		}
		// =============================================================

		boolean flag = false;
		try {
			Set<String> macSet = getMACAddress();

			InetAddress localhost = InetAddress.getLocalHost();
			String hostName = localhost.getHostName();

			IPlMgtDao dao = getDao(DaoName.PRODUCT_DAO.toString());
			PlMgtCondition condition = new PlMgtCondition();
			condition.setProductId( productId );
			condition.setCpNm( hostName );
			IPlMgtEntity entity = dao.findByPrimaryKey( condition.getCondition() );
			if (entity == null) {// シリアルナンバーをDBに記録し、登録を促す
				IPlMgtEntity data = new PlMgtEntity();
				data.setProductId( productId );
				data.setCpNm( hostName );

				Iterator<String> iter = macSet.iterator();
				String serial = (iter.hasNext()) ? makeSerialNo(iter.next(),
						productId) : "";// MACアドレスは最初の１件のみ
				data.setSerialNo( serial );
				dao.insert( data );
			} else {// 認証
				String key = entity.getPlKey();
				flag = checkInit( productId, macSet, key );
			}

		} catch (TriSystemException e) {
			throw e;
		} catch (Exception e) {
			e.printStackTrace();// ログだけは残す
		}
		return flag;
	}

	public static boolean isRmActive() {
		return isActive(RM_ID);
	}

	public static boolean isDmActive() {
		return isActive(DM_ID);
	}

	private static boolean isActive(String productId) {
		// =============================================================
		// 開発者バージョンのみ、チェック不要。
		if (EditionType.Developer.equals(DesignUtils.getEdition())) {
			return true;
		}
		// =============================================================

		if (null == productId) {
			return false;
		}

		boolean active = false;

		try {
			InetAddress localhost = InetAddress.getLocalHost();
			String hostName = localhost.getHostName();

			IPlMgtDao dao = getDao(DaoName.PRODUCT_DAO.toString());
			PlMgtCondition condition = new PlMgtCondition();
			condition.setProductId( productId );
			condition.setCpNm( hostName );
			IPlMgtEntity entity = dao.findByPrimaryKey( condition.getCondition() );
			if (entity != null) {
				String key = entity.getPlKey();
				if (TriStringUtils.isEmpty(key)) {
					return false;
				}
				Set<String> macSet = getMACAddress();
				if (KEYSUFFIX_PRODUCT.equals(key.substring(0, 1))) {
					active = checkInit(productId, macSet, key);
				}
				if (!active) {
					if (KEYSUFFIX_TERM.equals(key.substring(0, 1))) {
						active = isTerm(productId, key);
					}
				}
			}

		} catch (TriSystemException e) {
			throw e;
		} catch (Exception e) {
			e.printStackTrace();// ログだけは残す
		}
		return active;
	}

	/**
	 * Check product license key is active or no.
	 * @return
	 */
	public static boolean isActive(String productId, String productLicenseKey) {
		// =============================================================
		// 開発者バージョンのみ、チェック不要。
		if (EditionType.Developer.equals(DesignUtils.getEdition())) {
			return true;
		}
		// =============================================================

		if (null == productId) {
			return false;
		}

		boolean active = false;

		try {
			if (TriStringUtils.isEmpty(productLicenseKey)) {
				return false;
			}
			Set<String> macSet = getMACAddress();
			if (KEYSUFFIX_PRODUCT.equals(productLicenseKey.substring(0, 1))) {
				active = checkInit(productId, macSet, productLicenseKey);
			}
			if (!active) {
				if (KEYSUFFIX_TERM.equals(productLicenseKey.substring(0, 1))) {
					active = isTerm(productId, productLicenseKey);
				}
			}
		} catch (TriSystemException e) {
			e.printStackTrace();// ログだけは残す
		} catch (Exception e) {
			e.printStackTrace();// ログだけは残す
		}
		return active;
	}

	public static void isRmActiveAgent(int needLicenseKeyCount) {
		isActiveAgent(RM_ID, needLicenseKeyCount);
	}

	/**
	 * agentのライセンスキー数が足りているかチェックを行う。
	 *
	 * @param needLicenseKeyCount
	 *             必要なライセンス数
	 */
	private static void isActiveAgent(String productId, int needLicenseKeyCount) {
		// =============================================================
		// 「開発者バージョン」は、チェック不要。
		EditionType editionType = DesignUtils.getEdition();
		if (EditionType.Developer.equals(editionType)) {
			return;
		}
		// =============================================================

		if (null == productId) {
			return;
		}

		try {
			InetAddress localhost = InetAddress.getLocalHost();
			String hostName = localhost.getHostName();

			IPlMgtDao dao = getDao(DaoName.PRODUCT_DAO.toString());
			PlMgtCondition condition = new PlMgtCondition();
			condition.setProductId( productId );
			condition.setCpNm( hostName );
			IPlMgtEntity entity = dao.findByPrimaryKey( condition.getCondition() );
			if (entity == null) {// レコードなし：通常ありえないはず
				throw new TriSystemException( SmMessageId.SM001011E );
			}

			String productKey = entity.getPlKey();
			String agentLicense = entity.getAgentPlKey();
			if (TriStringUtils.isEmpty(productKey)) {// プロダクトキーが設定されていない
				throw new TriSystemException( SmMessageId.SM001011E );
			} else if (TriStringUtils.isEmpty(agentLicense)) {// agentライセンスが設定されていない
				// agentが0件のライセンスを生成して対応
				agentLicense = makeAgentLicense(productKey, 0);
			}
			// 保有ライセンス数のチェック
			int holdLicenseKeyCount = 0;
			for (int count = 0; count <= LICENSE_MAX; count++) {
				String testAgentLicense = makeAgentLicense(productKey, count);
				if (agentLicense.equals(testAgentLicense)) {
					holdLicenseKeyCount = count;
					break;
				}
			}
			// 保持するagentライセンス数と必要なライセンス数とを比較
			if (needLicenseKeyCount > holdLicenseKeyCount) {
				throw new TriSystemException(SmMessageId.SM001012E , String.valueOf(holdLicenseKeyCount) , String.valueOf(needLicenseKeyCount) );
			}

		} catch (TriSystemException e) {
			throw e;
		} catch (Exception e) {
			e.printStackTrace();// ログだけは残す
		}
	}

	/**
	 * Return date of license expiration.
	 * @param inKey
	 * @return
	 */
	public static ProductLicenseExpiration getExpiration(String productId, String inKey) {
		// =============================================================
		// 「開発者バージョン」は、チェック不要。
			EditionType editionType = DesignUtils.getEdition();
			if (EditionType.Developer.equals(editionType)) {
				return ProductLicenseExpiration.UNLIMITED;
			}
		// =============================================================
		
		if (KEYSUFFIX_PRODUCT.equals(inKey.substring(0, 1))) {
			return ProductLicenseExpiration.UNLIMITED;
		}
		if (KEYSUFFIX_TERM.equals(inKey.substring(0, 1))) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");
			Calendar cal = Calendar.getInstance();
			cal.set(YEAR_BEGIN, Calendar.JANUARY, 1);
			boolean term;
			String dateStr;
			while (true) {
				Date date = cal.getTime();
				dateStr = sdf.format(date);
				if (YEAR_END.equals(dateStr)) {
					break;
				}
				term = checkTerm(productId, dateStr, inKey);
				if (term) {
					return ProductLicenseExpiration.createFromDate(dateStr);
				}
				cal.add(Calendar.MONTH, 1);
			}
		}
		return ProductLicenseExpiration.NONE;
	}

	/**
	 * Return number of agents for current product and agent license.
	 */
	public static int getNumberOfAgents(String productLicenseKey, String agentLicenseKey) {
		// =============================================================
			// 「開発者バージョン」は、チェック不要。
			EditionType editionType = DesignUtils.getEdition();
			if (EditionType.Developer.equals(editionType)) {
				return LICENSE_MAX;
			}
		// =============================================================

		int holdLicenseKeyCount = 0;
		for (int count = 0; count <= LICENSE_MAX; count++) {
			String testAgentLicense = makeAgentLicense(productLicenseKey, count);
			if (agentLicenseKey.equals(testAgentLicense)) {
				holdLicenseKeyCount = count;
				break;
			}
		}
		return holdLicenseKeyCount;
	}

	/**
	 *
	 * @param id
	 * @param addrSet
	 * @param inKey
	 * @return
	 */
	private static boolean checkInit(String id, Set<String> addrSet,
			String inKey) {
		boolean ret = false;
		Iterator<String> iter = addrSet.iterator();
		while (iter.hasNext()) {
			String addr = iter.next();

			String serial = makeSerialNo(addr, id);
			String outKey = makeProductKey(serial);
			if (inKey == null || outKey == null)
				return ret;
			if (inKey.equals(outKey)) {
				ret = true;
			}
		}
		return ret;
	}

	/**
	 *
	 *
	 * @param productId
	 * @param inKey
	 * @return
	 */
	private static boolean isTerm(String productId, String inKey) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");
		Calendar cal = Calendar.getInstance();
		Date nowDate = cal.getTime();
		String nowDateStr = sdf.format(nowDate);

		cal.set(YEAR_BEGIN, Calendar.JANUARY, 1);

		boolean term = false;
		try {
			// カレンダーを遡って期限切れの期日を取得
			String dateStr;
			while (true) {
				Date date = cal.getTime();
				dateStr = sdf.format(date);
				if (YEAR_END.equals(dateStr)) {
					IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
					throw new TriSystemException( SmMessageId.SM001013E, ca.getMessage(SmMessageId.SM004104F) );
				}
				term = checkTerm(productId, dateStr, inKey);
				if (term) {
					break;
				}
				cal.add(Calendar.MONTH, 1);
			}

			if (Integer.parseInt(nowDateStr) <= Integer.parseInt(dateStr)) {
				return true;
			} else {
				IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
				throw new TriSystemException(SmMessageId.SM001013E , ca.getMessage(SmMessageId.SM004105F , dateStr.substring(0, 4) , dateStr.substring(4, 6) ) );
			}

		} catch (TriSystemException e) {
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return term;
	}

	/**
	 *
	 * @param productId
	 * @param date
	 * @param inKey
	 * @return
	 */
	private static boolean checkTerm(String productId, String date, String inKey) {
		boolean ret = false;

		String outKey = makeTermProductKey(productId, date);
		if (inKey == null || outKey == null)
			return ret;
		if (inKey.equals(outKey)) {
			ret = true;
		}
		return ret;
	}

	/**
	 * パスワード暗号化処理 <br>
	 * 入力された文字列を暗号化する
	 *
	 * @param passwd
	 *            暗号化対象文字列
	 * @return 暗号化文字列
	 */
	private static String createEncryptionPasswd(String passwd) {

		StringBuilder buf = new StringBuilder();

		try {
			MessageDigest md = MessageDigest.getInstance("SHA-256");

			// ハッシュ値を計算
			md.update(passwd.getBytes());
			byte[] digest = md.digest();

			// 16進数文字列に変換、1桁の場合0を追加
			for (int i = 0; i < digest.length; i++) {
				buf.append(convert(Integer.valueOf(digest[i] & 0xff)));
			}

		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		return buf.toString().toUpperCase();
	}

	/**
	 *
	 * @param value
	 * @return
	 */
	private static String convert(int value) {
		String ret = null;
		int x = value % 32;
		ret = TEMPLATE.substring(x, x + 1);
		return ret;
	}

	/**
	 * MACアドレスを取得する。
	 *
	 * @return 取得したMACアドレスのString
	 */
	public static String getMACAddressString() {
		try {
			InetAddress localHost = InetAddress.getLocalHost();
			NetworkInterface networkInterface = NetworkInterface.getByInetAddress(localHost);
			byte[] hardwareAddress = networkInterface.getHardwareAddress();
			String macAddress = DatatypeConverter
					.printHexBinary(hardwareAddress);
			return macAddress;
		} catch (Exception e) {
			throw new TriSystemException( SmMessageId.SM005214S , e );
		}
	}

	/**
	 * MACアドレスを取得する。
	 *
	 * @return 取得したMACアドレスのSet<String>
	 */
	public static Set<String> getMACAddress() {
		try {
			InetAddress localHost = InetAddress.getLocalHost();
			NetworkInterface networkInterface = NetworkInterface.getByInetAddress(localHost);
//			if (!networkInterface.hasMoreElements()) {
//				return new HashSet<String>();
//			}
			Set<String> hako = new HashSet<String>();
			byte[] hardwareAddress = networkInterface.getHardwareAddress();
			String macAddress = DatatypeConverter
					.printHexBinary(hardwareAddress);
			hako.add(macAddress);
			return hako;
		} catch (Exception e) {
			throw new TriSystemException( SmMessageId.SM005214S , e );
		}
	}

	/**
	 * MACアドレス・プロダクトＩＤをもとに、シリアルNoを生成する。
	 *
	 * @param macAddr
	 *             MACアドレス
	 * @param productId
	 *             プロダクトＩＤ
	 * @return 生成したシリアルNo
	 */
	private static String makeSerialNo(String macAddr, String productId) {

		StringBuilder stb = new StringBuilder();
		stb.append(macAddr);
		stb.append(productId);
		return createEncryptionPasswd(stb.toString());// MAC-ADDRESS +
														// PRODUCT_ID
	}

	/**
	 * シリアルNoをもとに、プロダクトキーを生成する。
	 *
	 * @param serialNo
	 *             シリアルNo
	 * @return 生成したプロダクトキー
	 */
	private static String makeProductKey(String serialNo) {
		String code = createEncryptionPasswd(serialNo + SALT);
		return KEYSUFFIX_PRODUCT + code.substring(1);
	}

	/**
	 * プロダクトID・ライセンス期限年月(YYYYMM)をもとに、「期限ライセンス」プロダクトキーを生成する。
	 *
	 * @param productId
	 *             プロダクトＩＤ
	 * @param date
	 *            ライセンス期限年月(YYYYMM)
	 * @return 生成した「期限ライセンス」プロダクトキー
	 */
	private static String makeTermProductKey(String productId, String date) {
		StringBuilder stb = new StringBuilder();
		stb.append(productId);
		stb.append(date);
		stb.append(SALT);
		String code = createEncryptionPasswd(stb.toString());
		return KEYSUFFIX_TERM + code.substring(1);
	}

	/**
	 * プロダクトキー・agentライセンス数をもとに、agentライセンスを生成する。
	 *
	 * @param productKey
	 *             プロダクトキー
	 * @param count
	 *             agentライセンス数
	 * @return 生成したagentライセンス
	 */
	private static String makeAgentLicense(String productKey, int count) {
		StringBuilder stb = new StringBuilder();
		stb.append(productKey);
		stb.append(String.format("%04d", count));
		stb.append(SALT);
		String code = createEncryptionPasswd(stb.toString());
		return KEYSUFFIX_AGENT + code.substring(1);
	}

	/**
	 * Daoを取得する。
	 *
	 * @param name
	 *            bean名
	 * @return Dao
	 */
	private static IPlMgtDao getDao(String name) {

		if (ac == null) {
			ac = ContextAdapterFactory.getContextAdapter();
		}

		return (IPlMgtDao) ac.getBean(name);
	}

}
