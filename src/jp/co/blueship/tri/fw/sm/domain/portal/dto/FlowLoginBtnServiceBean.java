package jp.co.blueship.tri.fw.sm.domain.portal.dto;

import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;

public class FlowLoginBtnServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;

	//private String projectName = null;
	//private String version = null;
	private Boolean passwordEffect = null;
	private Integer needLicenseKeyCount = 0;

//	public String getProjectName() {
//		return projectName;
//	}
//	public void setProjectName(String projectName) {
//		this.projectName = projectName;
//	}
//	public String getVersion() {
//		return version;
//	}
//	public void setVersion(String version) {
//		this.version = version;
//	}
	public Boolean isPasswordEffect() {
		return passwordEffect;
	}
	public void setPasswordEffect( Boolean passwordEffect ) {
		this.passwordEffect = passwordEffect;
	}
	public Integer getNeedLicenseKeyCount() {
		return needLicenseKeyCount;
	}
	public void setNeedLicenseKeyCount(Integer needLicenseKeyCount) {
		this.needLicenseKeyCount = needLicenseKeyCount;
	}



}
