package jp.co.blueship.tri.fw.sm.contants;

import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

/**
 * This is the class for expiration of productLicense.
 *
 * @author Yukihiro Eguchi
 * @version V4.00.00
 */
public class ProductLicenseExpiration {
	public final static ProductLicenseExpiration NONE;
	public final static ProductLicenseExpiration UNLIMITED;

	private String value;
	private boolean none;
	private boolean unlimited;
	private boolean date;

	static {
		NONE = new ProductLicenseExpiration();
		NONE.value = "";
		NONE.none = true;

		UNLIMITED = new ProductLicenseExpiration();
		UNLIMITED.value = "unlimited";
		UNLIMITED.unlimited = true;
	}

	public static ProductLicenseExpiration createFromDate(String value) {
		ProductLicenseExpiration expiration = new ProductLicenseExpiration();
		expiration.value = TriDateUtils.convertExpirationViewDateFormat(value, new SimpleDateFormat("yyyyMMdd"), TriDateUtils.getYMDDateFormat());
		expiration.date = true;
		return expiration;
	}

	public Timestamp formatDbValue() {
		return date ? TriDateUtils.convertStringToTimestampWithDateTime(TriDateUtils.convertDateFormat(value, "")) : null;
	}

	public String getValue() {
		return value;
	}

	public boolean isNone() {
		return none;
	}

	public boolean isUnlimited() {
		return unlimited;
	}

	public boolean isDate() {
		return date;
	}
}
