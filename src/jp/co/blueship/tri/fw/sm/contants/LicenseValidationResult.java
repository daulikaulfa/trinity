package jp.co.blueship.tri.fw.sm.contants;

import jp.co.blueship.tri.fw.msg.IMessageId;

import java.util.ArrayList;
import java.util.List;

/**
 * @author GiAnG
 * @version V4.03.00
 */
public class LicenseValidationResult {
	private List<IMessageId> errorList = new ArrayList<>();
	private List<String[]> errorArgs = new ArrayList<>();

	private List<IMessageId> infoList = new ArrayList<>();
	private List<String[]> infoArgs = new ArrayList<>();

	private ProductLicenseExpiration expiration = ProductLicenseExpiration.NONE;

	public void addError(IMessageId iMessageId, String... args) {
		errorList.add(iMessageId);
		errorArgs.add(args);
	}

	public void addInfo(IMessageId iMessageId, String... args) {
		infoList.add(iMessageId);
		infoArgs.add(args);
	}

	public List<IMessageId> getErrorList() {
		return errorList;
	}

	public List<String[]> getErrorArgs() {
		return errorArgs;
	}

	public List<IMessageId> getInfoList() {
		return infoList;
	}

	public List<String[]> getInfoArgs() {
		return infoArgs;
	}

	public ProductLicenseExpiration getExpiration() {
		return expiration;
	}

	public LicenseValidationResult setExpiration(ProductLicenseExpiration expiration) {
		this.expiration = expiration;
		return this;
	}
}
