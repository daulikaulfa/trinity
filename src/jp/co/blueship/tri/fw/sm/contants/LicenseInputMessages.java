package jp.co.blueship.tri.fw.sm.contants;

import jp.co.blueship.tri.fw.msg.IMessageId;

/**
 * @author GiAnG
 * @version V4.03.00
 */
public class 	LicenseInputMessages {
	private IMessageId noInputProductKey;
	private IMessageId invalidLengthProductKey;
	private IMessageId invalidProductKey;
	private IMessageId expiredProductKey;
	private IMessageId successProductKey;
	private IMessageId successLimitedProductKey;

	private IMessageId invalidLengthComment;

	private IMessageId invalidLengthAgentKey;
	private IMessageId invalidAgentKey;
	private IMessageId successAgentKey;
	private IMessageId infoNoAgentKey;

	public IMessageId getNoInputProductKey() {
		return noInputProductKey;
	}

	public LicenseInputMessages setNoInputProductKey(IMessageId noInputProductKey) {
		this.noInputProductKey = noInputProductKey;
		return this;
	}

	public IMessageId getInvalidLengthProductKey() {
		return invalidLengthProductKey;
	}

	public LicenseInputMessages setInvalidLengthProductKey(IMessageId invalidLengthProductKey) {
		this.invalidLengthProductKey = invalidLengthProductKey;
		return this;
	}

	public IMessageId getInvalidProductKey() {
		return invalidProductKey;
	}

	public LicenseInputMessages setInvalidProductKey(IMessageId invalidProductKey) {
		this.invalidProductKey = invalidProductKey;
		return this;
	}

	public IMessageId getExpiredProductKey() {
		return expiredProductKey;
	}

	public LicenseInputMessages setExpiredProductKey(IMessageId expiredProductKey) {
		this.expiredProductKey = expiredProductKey;
		return this;
	}

	public IMessageId getSuccessProductKey() {
		return successProductKey;
	}

	public LicenseInputMessages setSuccessProductKey(IMessageId successProductKey) {
		this.successProductKey = successProductKey;
		return this;
	}

	public IMessageId getSuccessLimitedProductKey() {
		return successLimitedProductKey;
	}

	public LicenseInputMessages setSuccessLimitedProductKey(IMessageId successLimitedProductKey) {
		this.successLimitedProductKey = successLimitedProductKey;
		return this;
	}

	public IMessageId getInvalidLengthComment() {
		return invalidLengthComment;
	}

	public LicenseInputMessages setInvalidLengthComment(IMessageId invalidLengthComment) {
		this.invalidLengthComment = invalidLengthComment;
		return this;
	}

	public IMessageId getInvalidLengthAgentKey() {
		return invalidLengthAgentKey;
	}

	public LicenseInputMessages setInvalidLengthAgentKey(IMessageId invalidLengthAgentKey) {
		this.invalidLengthAgentKey = invalidLengthAgentKey;
		return this;
	}

	public IMessageId getInvalidAgentKey() {
		return invalidAgentKey;
	}

	public LicenseInputMessages setInvalidAgentKey(IMessageId invalidAgentKey) {
		this.invalidAgentKey = invalidAgentKey;
		return this;
	}

	public IMessageId getSuccessAgentKey() {
		return successAgentKey;
	}

	public LicenseInputMessages setSuccessAgentKey(IMessageId successAgentKey) {
		this.successAgentKey = successAgentKey;
		return this;
	}

	public IMessageId getInfoNoAgentKey() {
		return infoNoAgentKey;
	}

	public LicenseInputMessages setInfoNoAgentKey(IMessageId infoNoAgentKey) {
		this.infoNoAgentKey = infoNoAgentKey;
		return this;
	}
}