package jp.co.blueship.tri.fw.sm.contants;

/**
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public enum PasswordComplexityLevel {
	none( 0 ),
	Low( 1 ),
	Middle( 2 ),
	High( 3 ),
	;

	private int value = 1;

	private PasswordComplexityLevel( int value) {
		this.value = value;
	}

	public boolean equals( int value ) {
		PasswordComplexityLevel level = value( value );

		if ( null == level ) return false;
		if ( ! this.equals(level) ) return false;

		return true;
	}

	public int value() {
		return this.value;
	}

	public static PasswordComplexityLevel value( int value ) {
		for ( PasswordComplexityLevel level : values() ) {
			if ( level.value() == value ) {
				return level;
			}
		}
		return none;
	}

}
