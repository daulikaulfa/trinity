package jp.co.blueship.tri.fw.sm.contants;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;

/**
 * プレコンパイルの処理状況を表すアクションIDごとにユニークな識別IDの列挙型です。
 * <br>
 *
 * @version V4.00.00
 * @author Satoshi Sasaki
 */
public enum SmProcCtgCd {

	/**
	 * 二重返却資産チェック処理中
	 */
	DUPLICATION_FILE_CHECK("0010","ChkInReqStatus.checkDuplicate.text"),
	/**
	 * 返却フォルダ内部コピー処理中
	 */
	RETURN_FILE_COPY( "0020", "ChkInReqStatus.copyInternal.text" ),
	/**
	 * 拡張子チェック処理中
	 */
	EXTENSION_CHECK( "0030", "ChkInReqStatus.checkExtension.text" ),
	/**
	 * コンパイル構文チェック
	 */
	COMPILE_SYNTAX_CHECK( "0040", "" ),
	/**
	 * 文字コードチェック
	 */
	CHARSET_CHECK( "0050", "" ),
	/**
	 * 改行コードチェック
	 */
	LINEFEED_CHECK( "0060", "" ),
	/**
	 * 外部スクリプト呼び出し
	 */
	CALL_SCRIPT( "0070", "ChkInReqStatus.callExternalTool.text" ),
	;

	private String code = null;
	private String key = null;

	private SmProcCtgCd( String codeId, String key) {
		this.code = codeId;
		this.key = key;
	}

	public boolean equals( String codeId ) {
		SmProcCtgCd code = value( codeId );

		if ( null == code ) return false;
		if ( ! this.equals(code) ) return false;

		return true;
	}

	public String getCode() {
		return this.code;
	}

	public String getMessageKey() {
		return this.key;
	}

	/**
	 * 指定されたコードIDに対応する列挙型を取得します。
	 *
	 * @param codeId コードID
	 * @return 対応する列挙型
	 */
	public static SmProcCtgCd value( String codeId ) {
		if ( TriStringUtils.isEmpty( codeId ) ) {
			return null;
		}

		for ( SmProcCtgCd value : values() ) {
			if ( value.getCode().equals( codeId ) ) {
				return value;
			}
		}

		return null;
	}

}
