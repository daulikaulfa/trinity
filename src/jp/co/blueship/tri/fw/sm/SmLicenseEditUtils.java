package jp.co.blueship.tri.fw.sm;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.sm.domainx.license.bean.dto.ProductLicenseEditInputBean;
import jp.co.blueship.tri.fw.sm.domainx.license.dto.FlowProductLicenseEditServiceBean;
import jp.co.blueship.tri.fw.uix.TriModel;
import org.apache.commons.lang.StringUtils;

/**
 * @author GiAnG
 * @version V4.03.00
 */
public class SmLicenseEditUtils {
	public static void insertMapping(FlowProductLicenseEditServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();
		if (RequestType.submitChanges.equals(bean.getParam().getRequestType())) {
			for (ProductLicenseEditInputBean inputBean : bean.getParam().getInputBeans().values()) {
				String productId = inputBean.getProductId();
				if (!"true".equals(requestInfo.getParameter(productId + "Submit"))) {
					inputBean.setSubmit(false);
					continue;
				}
				inputBean.setSubmit(true)
						 .setComment(requestInfo.getParameter(productId + "Comment"))
						 .getProductKey()
						 .setKey1(StringUtils.upperCase(requestInfo.getParameter(productId + "ProductKey1")))
						 .setKey2(StringUtils.upperCase(requestInfo.getParameter(productId + "ProductKey2")))
						 .setKey3(StringUtils.upperCase(requestInfo.getParameter(productId + "ProductKey3")))
						 .setKey4(StringUtils.upperCase(requestInfo.getParameter(productId + "ProductKey4")));

				if (inputBean.isRm()) {
					inputBean.getAgentKey()
							 .setKey1(StringUtils.upperCase(requestInfo.getParameter(productId + "AgentKey1")))
							 .setKey2(StringUtils.upperCase(requestInfo.getParameter(productId + "AgentKey2")))
							 .setKey3(StringUtils.upperCase(requestInfo.getParameter(productId + "AgentKey3")))
							 .setKey4(StringUtils.upperCase(requestInfo.getParameter(productId + "AgentKey4")));
				}
			}
		}
	}
}
