package jp.co.blueship.tri.fw.sm;

import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.msg.MessageParameter;

public class SmProgressNotificationUtils {

	/**
	 * 残り時間の文字列変換を行います。<br>
	 * 残り時間が1日以上の場合、日のみの表示になります。<br>
	 * 残り時間が1日未満1時間以上の場合、時分表示になります。<br>
	 * 残り時間が1時間未満の場合、分表示のみになります。<br>
	 * 残り時間が1分未満1秒以上の場合は1分に丸めて表示されます。<br>
	 * <br>
	 * 秒数は30秒以上を切り上げ、30秒未満を切り捨てにします。<br>
	 * <br>
	 *
	 * @param language
	 * @param remainingTime  only miliSecond
	 * @return remainingTimeStr
	 */
	public static String stringOf(String language , long remainingTime){
		IContextAdapter contextAdapter = ContextAdapterFactory.getContextAdapter();

		String remainingTimeStr = "";
		if(remainingTime < 0) remainingTime = 0;

		long remainingData = remainingTime / 86400000;
		long remainingHour = (remainingTime % 86400000) / 3600000;
		long remainingMin  = (remainingTime % 3600000) / 60000;
		long remainingSec  = remainingTime % 60000;

		if(remainingSec > 30000) remainingMin++;

		if(remainingMin <= 0 && remainingSec > 1000) remainingMin = 1;

		if(remainingData > 0){
			remainingTimeStr = contextAdapter.getValue(MessageParameter.ProcessiongStatusRemainingTime1.getKey(), new String[]{remainingData+""}, language );

		}else{
			if(remainingHour > 0)
				remainingTimeStr = contextAdapter.getValue(MessageParameter.ProcessiongStatusRemainingTime2.getKey(), new String[]{ remainingHour+"" , remainingMin+"" }, language);
			else
				remainingTimeStr = contextAdapter.getValue(MessageParameter.ProcessiongStatusRemainingTime3.getKey(), new String[]{ remainingMin+"" }, language);
		}

		return remainingTimeStr;
	}
}
