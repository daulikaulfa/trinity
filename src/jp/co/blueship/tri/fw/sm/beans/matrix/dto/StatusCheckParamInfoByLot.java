package jp.co.blueship.tri.fw.sm.beans.matrix.dto;



/**
 * （関連するロット）アクション間のステータスマトリクスメッセージ
 * <br>
 *
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class StatusCheckParamInfoByLot extends StatusCheckParamInfo {

	private static final long serialVersionUID = -5840922292356090208L;
	private String[] lotIds = null;

	public String[] getLotIds() {
		return lotIds;
	}

	public void setLotIds(String[] lotIds) {
		this.lotIds = lotIds;
	}

}
