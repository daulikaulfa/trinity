package jp.co.blueship.tri.fw.sm.beans.matrix.dto;

/**
 * (関連するレポート)アクション間のステータスマトリクスメッセージ
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 */
public class StatusCheckParamInfoByReport extends StatusCheckParamInfo {

	private static final long serialVersionUID = 6684529412973322595L;
	private String[] repIds = null;

	public String[] getRepIds() {
		return repIds;
	}

	public void setRepIds(String[] repIds) {
		this.repIds = repIds;
	}

}
