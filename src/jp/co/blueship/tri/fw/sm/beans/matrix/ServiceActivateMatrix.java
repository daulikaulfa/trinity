package jp.co.blueship.tri.fw.sm.beans.matrix;

import java.util.Set;

import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.msg.SmMessageId;

/**
 * 機能サービスが有効かどうかをチェックします。
 * 縮退(AMのみ、RMのみ)の場合は、利用できる機能に制限を加えます。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public class ServiceActivateMatrix extends ServiceMatrixSupport<Set<String>> {

	@Override
	protected void actions(IServiceDto<IGeneralServiceBean> serviceDto, String key) {
		IGeneralServiceBean paramBean = serviceDto.getServiceBean();

		String serviceId = paramBean.getFlowAction();

		Set<String> serviceSet = this.getActions().get( key );

		if ( 0 == serviceSet.size() ) {
			//何も指定されていない場合は、全機能利用可能
			return;
		}

		if ( ! serviceSet.contains( serviceId ) ) {
			throw new BusinessException( SmMessageId.SM001001E );
		}
	}

}
