package jp.co.blueship.tri.fw.sm.beans.matrix.dto;

import jp.co.blueship.tri.fw.dao.orm.FinderSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;

/**
 * The interface of the status matrix check DTO.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public interface IStatusCheckDto {
	/**
	 * ドメイン層のBean Interfaceを取得します。
	 * @return ドメイン層のBean Interface
	 */
	public IGeneralServiceBean getServiceBean();
	/**
	 * データアクセスを支援するファインダーを取得します。
	 * @return データアクセスを支援するファインダー
	 */
	public FinderSupport getFinder();
	/**
	 * ビジネス処理を一括して実行するクラスを取得します。
	 * @return ビジネス処理を一括して実行するクラス
	 */
	public ActionStatusMatrixList getActionList();
	/**
	 * ロット・ベースラインID'sを取得します。
	 * @return ロット・ベースラインID's
	 */
	public String[] getLotBls();
	/**
	 * HEAD・ベースラインID'sを取得します。
	 * @return HEAD・ベースラインID's
	 */
	public String[] getHeadBls();
	/**
	 * ロットID'sを取得します。
	 * @return ロットID's
	 */
	public String[] getLotIds();
	/**
	 * 変更情報ID'sを取得します。
	 * @return 変更情報ID's
	 */
	public String[] getPjtIds();
	/**
	 * 資産申請ID'sを取得します。
	 * @return 資産申請ID's
	 */
	public String[] getAreqIds();
	/**
	 * ビルドパッケージID'sを取得します。
	 * @return ビルドパッケージID's
	 */
	public String[] getBpIds();
	/**
	 * リリース申請ID'sを取得します。
	 * @return リリース申請ID's
	 */
	public String[] getRaIds();
	/**
	 * リリースパッケージID'sを取得します。
	 * @return リリースパッケージID's
	 */
	public String[] getRpIds();
	/**
	 * レポートID'sを取得します。
	 * @return レポートID's
	 */
	public String[] getRepIds();

}
