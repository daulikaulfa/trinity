package jp.co.blueship.tri.fw.sm.beans.matrix.dto;



/**
 * （関連するリリース）アクション間のステータスマトリクスメッセージ
 * <br>
 *
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class StatusCheckParamInfoByRelCancel extends StatusCheckParamInfo {

	private static final long serialVersionUID = 6725681379474485836L;
	private String[] relNo = null;

	public String[] getRelNo() {
		return relNo;
	}

	public void setRelNo(String[] relNo) {
		this.relNo = relNo;
	}


}
