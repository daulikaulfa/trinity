package jp.co.blueship.tri.fw.sm.beans.lock;

import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.svc.beans.dto.ILockParamInfo;
import jp.co.blueship.tri.fw.svc.beans.dto.LockParamInfo;

/**
 * 同時実行制御の排他（スレッド専用）を行う
 *
 */
public class ActionThreadLockService extends ActionLockService  implements IDomain<IGeneralServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private static final IContextAdapter ac = ContextAdapterFactory.getContextAdapter();

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {
		try{
			IGeneralServiceBean paramBean = serviceDto.getServiceBean();

			paramBean.setLockByThread( true );

			if ( ! paramBean.isLock() ) {

				IContextAdapter ca = ContextAdapterFactory.getContextAdapter();

				IActionLock lock		= (IActionLock)ca.getBean( IActionLock.class.getSimpleName() );
				ILockParamInfo lockInfo	= new LockParamInfo();

				lockInfo.setActionId	( paramBean.getFlowAction() );
				lockInfo.setUserId		( paramBean.getUserId() );
				lockInfo.setLotNo		( paramBean.getLockLotNo() );
				lockInfo.setProcId		( paramBean.getProcId() );

				lock.lock( lockInfo );

				paramBean.setLock( true );
			}

			return serviceDto;

		} catch ( BaseBusinessException e ) {
			LogHandler.error( log , e ) ;
			throw e;
		} catch (Exception e) {
			LogHandler.fatal( log , ac.getMessage( SmMessageId.SM005123S ) , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( SmMessageId.SM005123S , e );
		}
	}


}
