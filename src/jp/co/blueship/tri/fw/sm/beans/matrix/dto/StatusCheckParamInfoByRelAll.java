package jp.co.blueship.tri.fw.sm.beans.matrix.dto;



/**
 * （関連する全リリース）アクション間のステータスマトリクスメッセージ
 * <br>
 *
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
/**
 * @author 江口
 *
 */
public class StatusCheckParamInfoByRelAll extends StatusCheckParamInfo {

	private static final long serialVersionUID = -7744847842353781237L;

}
