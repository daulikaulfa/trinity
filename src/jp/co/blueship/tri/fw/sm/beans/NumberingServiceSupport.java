package jp.co.blueship.tri.fw.sm.beans;

import jp.co.blueship.tri.fw.dao.orm.INumberingDao;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.svc.beans.dto.INumberingCallback;
import jp.co.blueship.tri.fw.svc.beans.dto.NumberingServiceBean;

/**
 * 採番をするためのPojoをサポートするクラスです。
 *
 */
public abstract class NumberingServiceSupport<E> implements IDomain<E> {

	private static final ILog log = TriLogFactory.getInstance();

	private NumberingServiceTemplate template = new NumberingServiceTemplate();

	private String dateFormat = null;
	private String timeFormat = null;
	private INumberingDao numberingDao = null;

	/**
	 * システム日付のフォーマット書式を取得します。
	 *
	 * @return 取得した値を戻します。
	 */
	public String getDateFormat() {
		return dateFormat;
	}
	/**
	 * システム日付のフォーマット書式を設定します。
	 *
	 * @param dateFormat システム日付のフォーマット書式
	 */
	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}
	/**
	 * システム時間のフォーマット書式を取得します。
	 *
	 * @return 取得した値を戻します。
	 */
	public String getTimeFormat() {
		return timeFormat;
	}
	/**
	 * システム時間のフォーマット書式を設定します。
	 *
	 * @param timeFormat システム時間のフォーマット書式
	 */
	public void setTimeFormat(String timeFormat) {
		this.timeFormat = timeFormat;
	}

	/**
	 * 採番を利用するためのサービステンプレートのインスタンスを取得します。
	 * @return 取得したインスタンスを戻します。
	 */
	public final NumberingServiceTemplate getNumberingServiceTemplate() {
		return template;
	}
	/**
	 * ナンバリングＤＡＯを取得します。
	 *
	 * @return 取得した値を戻します。
	 */
	public INumberingDao getNumberingDao() {
		return numberingDao;
	}
	/**
	 * ナンバリングＤＡＯを設定します。
	 *
	 * @param numberingDao ナンバリングＤＡＯ
	 */
	public void setNumberingDao(INumberingDao numberingDao) {
		this.numberingDao = numberingDao;
	}

	/**
     * 指定された各種情報に基づいて、採番を行います。<BR>
     *
	 * @param bean サービスビーン
	 * @param callback コールバック
	 */
	public void numbering( NumberingServiceBean bean, INumberingCallback callback ) {

		try {
			this.getNumberingServiceTemplate().numbering(
													bean,
													this.getDateFormat(),
													this.getTimeFormat(),
													this.getNumberingDao(),
													callback );

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( SmMessageId.SM005124S, e );
		}
	}

}
