package jp.co.blueship.tri.fw.sm.beans.matrix.dto;

import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;


/**
 * アクション間のステータスマトリクスメッセージ
 *
 * @version V3L10R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public class StatusCheckParamInfo extends GenericServiceBean implements IStatusCheckParamInfo {

	private static final long serialVersionUID = 1L;

	private boolean isValidationMode = false;
	private boolean isValid = false;

	@Override
	public boolean isValidationMode() {
		return isValidationMode;
	}
	/**
	 * if you want to check the Status in isValid, Set true. The default is false.
	 *
	 * @param isValidationMode
	 */
	public void setValidationMode(boolean isValidationMode) {
		this.isValidationMode = isValidationMode;
	}

	@Override
	public boolean isValid() {
		return isValid;
	}
	/**
	 * In the case of validationMode = true, it returns the verification result.
	 *
	 * @param isValid
	 */
	public void setValid(boolean isValid) {
		this.isValid = isValid;
	}


}
