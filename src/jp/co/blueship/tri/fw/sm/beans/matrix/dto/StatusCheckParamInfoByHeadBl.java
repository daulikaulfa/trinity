package jp.co.blueship.tri.fw.sm.beans.matrix.dto;

/**
 * (関連するHEADベースライン)アクション間のステータスマトリクスメッセージ
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 */
public class StatusCheckParamInfoByHeadBl extends StatusCheckParamInfo {

	private static final long serialVersionUID = 6684529412973322595L;
	private String[] headBlIds = null;

	public String[] getHeadBlIds() {
		return headBlIds;
	}

	public void setHeadBlIds(String[] headBlIds) {
		this.headBlIds = headBlIds;
	}

}
