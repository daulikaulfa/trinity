package jp.co.blueship.tri.fw.sm.beans.matrix;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.bm.dao.bp.eb.BpCondition;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.fw.cmn.utils.ExtractFinderAddonUtils;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.constants.RmDesignBeanId;
import jp.co.blueship.tri.fw.constants.UmDesignBeanId;
import jp.co.blueship.tri.fw.constants.status.BmBpStatusId;
import jp.co.blueship.tri.fw.constants.status.BmBpStatusIdForExecData;
import jp.co.blueship.tri.fw.dao.orm.FinderSupport;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.msg.MessageParameter;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckParamInfoByRelUnitAll;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.ExtractStatusCheckUtils;

/**
 * （関連する全ビルドパッケージ）アクション間のステータスマトリクスを検査します。
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author yukihiro eguchi
 */
public class StatusCheckByRelUnitAll extends GenericStatusCheck {
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	/**
	 * 対象ベースステータス
	 */
	List<String> targetsByBaseStatus = new ArrayList<String>();
	/**
	 * 対象全体ステータス
	 */
	List<String> targetsByStatus = new ArrayList<String>();

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {
		this.outputBLStartLog( this.getClass().getName() );

		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]>	messageArgsList	= new ArrayList<String[]>();

		List<Object> paramList = serviceDto.getParamList();

		try {
			FinderSupport finder = ExtractFinderAddonUtils.extractFinderSupport( paramList );
			StatusCheckParamInfoByRelUnitAll param = ExtractStatusCheckUtils.extractStatusCheckParamInfoByRelUnitAll( paramList );
			param.setValid(false);

			BpCondition condition = new BpCondition();
			condition.setStsIds( this.getTargetsByBaseStatus().toArray( new String[0] ) );
			condition.setProcStsIds( this.getTargetsByStatus().toArray( new String[0] ) );

			IEntityLimit<IBpEntity> limit = finder.getBmFinderSupport().getBpDao().find(condition.getCondition(), new SortBuilder(), 1, 0);

			IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
			GenericServiceBean serviceBean = (GenericServiceBean) serviceDto.getServiceBean();
			for ( IBpEntity entity : limit.getEntities() ) {
				if ( StatusFlg.on.value().equals( entity.getDelStsId().value() )) {
					messageList.add		( SmMessageId.SM001014E );
					messageArgsList.add	( new String[] { ca.getValue( MessageParameter.TARGET_BP.getKey(), serviceBean.getLanguage() ), entity.getBpId() } );
					continue;
				}

				for ( String status : this.getWithoutPermissionsByBaseStatus() ) {
					if ( status.equals( entity.getStsId() ) ) {
						String curStatusLabel = "";
						String flowAction = sheet.getValue( UmDesignBeanId.actionId, param.getFlowAction());;
						if( serviceBean.isStatusMatrixV3() ) {
							curStatusLabel = sheet.getValue( RmDesignBeanId.statusId, entity.getStsId() );
						} else {
							curStatusLabel = ca.getValue(BmBpStatusId.value(entity.getStsId()).getMessageKey(), serviceBean.getLanguage());
						}
						messageList.add		( SmMessageId.SM001015E );
						messageArgsList.add	( new String[] { ca.getValue( MessageParameter.TARGET_BP.getKey(), serviceBean.getLanguage() ),
																entity.getBpId(),
																ca.getValue( MessageParameter.BASE.getKey(), serviceBean.getLanguage() ),
																flowAction ,
																curStatusLabel } );
					}
				}

				for ( String status : this.getWithoutPermissionsByStatus() ) {
					if ( status.equals( entity.getProcStsId() ) ) {
						String curStatusLabel = "";
						String flowAction = sheet.getValue( UmDesignBeanId.actionId, param.getFlowAction());;
						if( serviceBean.isStatusMatrixV3() ) {
							curStatusLabel = sheet.getValue( RmDesignBeanId.statusId, entity.getProcStsId() );
						} else {
							curStatusLabel = ca.getValue(BmBpStatusIdForExecData.value(entity.getProcStsId()).getMessageKey(), serviceBean.getLanguage());
						}
						messageList.add		( SmMessageId.SM001015E );
						messageArgsList.add	( new String[] { ca.getValue( MessageParameter.TARGET_BP.getKey(), serviceBean.getLanguage() ),
																entity.getBpId(),
																"" ,
																flowAction ,
																curStatusLabel } );
					}
				}

			}

			if ( 0 != messageList.size() ) {
				if ( ! param.isValidationMode() )
					throw new ContinuableBusinessException( messageList, messageArgsList );
			} else {
				param.setValid(true);
			}

		} finally {
			this.outputBLEndLog( this.getClass().getName() );
		}

		return serviceDto;
	}

	public List<String> getTargetsByBaseStatus() {
		return targetsByBaseStatus;
	}

	public void setTargetsByBaseStatus(List<String> targetsByBaseStatus) {
		this.targetsByBaseStatus = targetsByBaseStatus;
	}

	public List<String> getTargetsByStatus() {
		return targetsByStatus;
	}

	public void setTargetsByStatus(List<String> targetsByStatus) {
		this.targetsByStatus = targetsByStatus;
	}

}
