package jp.co.blueship.tri.fw.sm.beans.lock;

import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.dao.orm.INumberingDao;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.svc.beans.dto.ILockParamInfo;
import jp.co.blueship.tri.fw.svc.beans.dto.LockParamInfo;

/**
 * 同時実行制御の排他を行う
 *
 */
public class ActionLockService implements IDomain<IGeneralServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private IContextAdapter ac;
	private INumberingDao numberingDao;

	/**
	 * 採番Daoがインスタンス生成時に自動的に設定されます。
	 *
	 * @param numberingDao 採番Dao
	 */
	public void setNumberingDao(INumberingDao numberingDao) {
		this.numberingDao = numberingDao;
	}

	@Override
	public IServiceDto<IGeneralServiceBean> execute(IServiceDto<IGeneralServiceBean> serviceDto) {
		try {

			IGeneralServiceBean paramBean = serviceDto.getServiceBean();

			if (!paramBean.isLock() && !paramBean.isLockByThread() && !ScreenType.bussinessException.equals(paramBean.getScreenType())) {

				IActionLock lock = (IActionLock) contextAdapter().getBean(IActionLock.class.getSimpleName());
				ILockParamInfo lockInfo = new LockParamInfo();

				paramBean.setProcId(numberingDao.nextval());

				lockInfo.setActionId(paramBean.getFlowAction());
				lockInfo.setUserId(paramBean.getUserId());
				lockInfo.setLotNo(paramBean.getLockLotNo());
				lockInfo.setProcId(paramBean.getProcId());

				lock.lock(lockInfo);

				paramBean.setLock(true);
			}

			return serviceDto;

		} catch (BaseBusinessException e) {
			LogHandler.error(log, e);
			throw e;
		} catch (Exception e) {
			LogHandler.fatal(log, contextAdapter().getMessage(SmMessageId.SM005123S), e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(SmMessageId.SM005123S, e);
		}
	}

	private IContextAdapter contextAdapter() {
		if (ac == null) {
			ac = ContextAdapterFactory.getContextAdapter();
		}

		return ac;
	}

}
