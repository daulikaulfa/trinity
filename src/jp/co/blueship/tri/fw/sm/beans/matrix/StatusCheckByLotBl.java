package jp.co.blueship.tri.fw.sm.beans.matrix;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.LotBlCondition;
import jp.co.blueship.tri.fw.cmn.utils.ExtractFinderAddonUtils;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RmDesignBeanId;
import jp.co.blueship.tri.fw.constants.UmDesignBeanId;
import jp.co.blueship.tri.fw.constants.status.AmLotBlStatusId;
import jp.co.blueship.tri.fw.constants.status.AmLotBlStatusIdForExecData;
import jp.co.blueship.tri.fw.dao.orm.FinderSupport;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckParamInfoByLotBl;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.ExtractStatusCheckUtils;

/**
 * （関連するロットベースライン）アクション間のステータスマトリクスを検査します。
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author yukihiro eguchi
 */
public class StatusCheckByLotBl extends GenericStatusCheck {
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {
		this.outputBLStartLog( this.getClass().getName() );

		List<IMessageId> messageList	= new ArrayList<IMessageId>();
		List<String[]> messageArgsList	= new ArrayList<String[]>();

		List<Object> paramList = serviceDto.getParamList();

		try {
			FinderSupport finder = ExtractFinderAddonUtils.extractFinderSupport( paramList );
			StatusCheckParamInfoByLotBl param = ExtractStatusCheckUtils.extractStatusCheckParamInfoByLotBl( paramList );
			param.setValid(false);

			if ( TriStringUtils.isEmpty( param.getLotBlIds() ) )
				return serviceDto;

			LotBlCondition condition = new LotBlCondition();
			condition.setDelStsId( null );
			condition.setLotBlIds( param.getLotBlIds() );

			List<ILotBlEntity> lotBlEntities = finder.getAmFinderSupport().getLotBlDao().find(condition.getCondition(), new SortBuilder());

			IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
			GenericServiceBean serviceBean = (GenericServiceBean) serviceDto.getServiceBean();
			for ( ILotBlEntity entity : lotBlEntities ) {
				if ( StatusFlg.on.value().equals( entity.getDelStsId().value() )) {
					messageList.add		( SmMessageId.SM005197S );
					messageArgsList.add	( new String[] { entity.getLotBlId() } );
					continue;
				}

				for ( String status : this.getWithoutPermissionsByBaseStatus() ) {
					if ( status.equals( entity.getStsId() ) ) {
						String curStatusLabel = "";
						String flowAction = sheet.getValue( UmDesignBeanId.actionId, param.getFlowAction());;
						if( serviceBean.isStatusMatrixV3() ) {
							curStatusLabel = sheet.getValue( RmDesignBeanId.statusId, entity.getStsId() );
						} else {
							curStatusLabel = ca.getValue(AmLotBlStatusId.value(entity.getStsId()).getMessageKey(), serviceBean.getLanguage());
						}
						messageList.add		( SmMessageId.SM004213F );
						messageArgsList.add	( new String[] { entity.getLotBlId(),
																flowAction ,
																curStatusLabel } );
					}
				}

				for ( String status : this.getWithoutPermissionsByStatus() ) {
					if ( status.equals( entity.getProcStsId() ) ) {
						String curStatusLabel = "";
						String flowAction = sheet.getValue( UmDesignBeanId.actionId, param.getFlowAction());;
						if( serviceBean.isStatusMatrixV3() ) {
							curStatusLabel = sheet.getValue( RmDesignBeanId.statusId, entity.getStsId() );
						} else {
							curStatusLabel = ca.getValue(AmLotBlStatusIdForExecData.value(entity.getStsId()).getMessageKey(), serviceBean.getLanguage());
						}
						messageList.add		( SmMessageId.SM004213F );
						messageArgsList.add	( new String[] { entity.getLotBlId(),
																flowAction ,
																curStatusLabel } );
					}
				}

			}

			if ( 0 != messageList.size() ) {
				if ( ! param.isValidationMode() )
					throw new ContinuableBusinessException( messageList, messageArgsList );
			} else {
				param.setValid(true);
			}

		} finally {
			this.outputBLEndLog( this.getClass().getName() );
		}

		return serviceDto;
	}

}
