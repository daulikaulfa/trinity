package jp.co.blueship.tri.fw.sm.beans.matrix;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.dao.baseline.eb.HeadBlCondition;
import jp.co.blueship.tri.am.dao.baseline.eb.IHeadBlEntity;
import jp.co.blueship.tri.fw.cmn.utils.ExtractFinderAddonUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.UmDesignBeanId;
import jp.co.blueship.tri.fw.constants.status.AmHeadBlStatusId;
import jp.co.blueship.tri.fw.constants.status.AmHeadBlStatusIdForExecData;
import jp.co.blueship.tri.fw.dao.orm.FinderSupport;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckParamInfoByHeadBl;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.ExtractStatusCheckUtils;

/**
 * (関連するHEADベースライン)アクション間のステータスマトリクスを検査します。
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 */
public class StatusCheckByHeadBl extends GenericStatusCheck {
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	@Override
	public IServiceDto<IGeneralServiceBean> execute(IServiceDto<IGeneralServiceBean> serviceDto) {

		this.outputBLStartLog( this.getClass().getName() );

		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]>	messageArgsList	= new ArrayList<String[]>();

		List<Object> paramList = serviceDto.getParamList();

		try{

			FinderSupport finder = ExtractFinderAddonUtils.extractFinderSupport( paramList );
			StatusCheckParamInfoByHeadBl param = ExtractStatusCheckUtils.extractStatusCheckParamInfoByHeadBl( paramList );
			param.setValid(false);

			if ( TriStringUtils.isEmpty( param.getHeadBlIds() ) ){
				return serviceDto;
			}

			HeadBlCondition condition = new HeadBlCondition();
			condition.setHeadBlIds( param.getHeadBlIds() );

			IEntityLimit<IHeadBlEntity> limit = finder.getAmFinderSupport().getHeadBlDao().find(condition.getCondition(), new SortBuilder(), 1, 0);

			for( IHeadBlEntity entity : limit.getEntities() ){

				for( String status : this.getWithoutPermissionsByBaseStatus() ){
					String curStatusId = entity.getStsId();

					if( status.equals(curStatusId) ){
						if( AmHeadBlStatusId.Merged.equals( status ) ){
							messageList.add		( AmMessageId.AM001138E );
							messageArgsList.add	( new String[] { entity.getLotId() } );
						}
					}
				}

				for( String status : this.getWithoutPermissionsByStatus() ){
					String curStatusId = entity.getProcStsId();

					if( status.equals( curStatusId ) ){
						String flowAction = sheet.getValue( UmDesignBeanId.actionId, param.getFlowAction() );

						if( AmHeadBlStatusIdForExecData.Merging.equals(status) ){
							messageList.add		( AmMessageId.AM001137E );
							messageArgsList.add	( new String[] { flowAction } );

						}else if( AmHeadBlStatusIdForExecData.ConflictChecking.equals(status) ){
							messageList.add		( AmMessageId.AM001136E );
							messageArgsList.add	( new String[] { flowAction } );
						}
					}
				}
			}

			if ( 0 != messageList.size() ) {
				if ( ! param.isValidationMode() )
					throw new ContinuableBusinessException( messageList, messageArgsList );
			} else {
				param.setValid(true);
			}

		} finally {
			this.outputBLEndLog( this.getClass().getName() );
		}

		return serviceDto;
	}
}
