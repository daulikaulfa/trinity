package jp.co.blueship.tri.fw.sm.beans.lock;

import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.sm.dao.lockmgt.eb.ILockMgtEntity;
import jp.co.blueship.tri.fw.sm.dao.lockmgt.eb.LockMgtCondition;
import jp.co.blueship.tri.fw.sm.dao.lockmgt.eb.LockMgtEntity;
import jp.co.blueship.tri.fw.sm.support.ISmFinderSupport;
import jp.co.blueship.tri.fw.svc.beans.dto.ILockParamInfo;
import jp.co.blueship.tri.fw.um.dao.locksvc.eb.ILockSvcEntity;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 * アクションに関連する業務排他を行います。
 *
 * @author Yukihiro Eguchi
 *
 */
public class ActionLock implements IActionLock {

	protected ISmFinderSupport smSupport;
	protected IUmFinderSupport umSupport;

	/**
	 * アクセスインタフェースがインスタンス生成時に自動的に設定されます。
	 * @param support アクセスインタフェース
	 */
	public void setSmSupport(ISmFinderSupport smSupport) {
		this.smSupport = smSupport;
	}

	/**
	 * アクセスインタフェースがインスタンス生成時に自動的に設定されます。
	 * @param umSupport アクセスインタフェース
	 */
	public void setUmSupport(IUmFinderSupport umSupport) {
		this.umSupport = umSupport;
	}

	@Override
	public final void lock( ILockParamInfo info ) throws BaseBusinessException {
		PreConditions.assertOf(info != null, "Lock parameter is null.");

		List<ILockSvcEntity> lockEntities = umSupport.getLockSvcDao().lock( info.getActionId() );

		LockMgtCondition lockCondition = new LockMgtCondition();
		lockCondition.setLockSvcId(info.getActionId());
		List<ILockMgtEntity> checkEntitys = smSupport.getLockMgtDao().find( lockCondition.getCondition() );

		boolean error = false;
		for ( ILockMgtEntity checkEntity : checkEntitys ) {

			// システム全体排他
			if ( !checkEntity.getLockForLot().equals(StatusFlg.on) ) {
				error = true;
				break;
			}
			// ロット単位排他
			else {

				if ( checkEntity.getLotId().equals( info.getLotNo() )) {
					error = true;
					break;
				}
			}
		}

		if ( error ) {
			throw new BusinessException(
					SmMessageId.SM001018E ,
					smSupport.findFuncSvcEntityByPrimaryKey(checkEntitys.get(0).getLockSvcId()).getSvcNm() );
		}


		// SM_LOCK_MGTにデータ登録
		for ( ILockSvcEntity lockEntity : lockEntities ) {
			ILockMgtEntity entity = new LockMgtEntity();

			entity.setProcId		( info.getProcId() );
			entity.setSvcId			( info.getActionId() );
			entity.setLockSvcId		( lockEntity.getLockSvcId() );
			entity.setUserId		( info.getUserId() );
			entity.setLotId			( info.getLotNo() );
			entity.setLockForLot	( lockEntity.getLockForLot() );

			smSupport.getLockMgtDao().insert( entity );
		}
	}

}
