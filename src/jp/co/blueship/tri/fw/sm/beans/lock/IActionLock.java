package jp.co.blueship.tri.fw.sm.beans.lock;

import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.svc.beans.dto.ILockParamInfo;

/**
 * アクションに関連する業務排他を行います。
 * <br>この処理は、業務排他を行うために、一時的にテーブルにＤＢ排他を行います。
 * <br>そのため、このメソッドのlock内は、DIコンテナで独立した
 * トランザクションとして管理される必要があります。
 *
 * @author Yukihiro Eguchi
 *
 */
public interface IActionLock {

	

	/**
    * 排他処理を行います。
     * <br>指定した実行アクションＩＤに紐づく、制限されるアクションＩＤをすべてロックします。
	 * @param info 排他共通情報
	 * @throws BaseBusinessException
	 */
	public void lock( ILockParamInfo info )  throws BaseBusinessException;

}
