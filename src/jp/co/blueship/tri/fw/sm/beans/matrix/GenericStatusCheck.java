package jp.co.blueship.tri.fw.sm.beans.matrix;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;

/**
 * アクション間のステータスマトリクスを検査します。
 *
 * @author Yukihiro Eguchi
 *
 */
public abstract class GenericStatusCheck extends ActionPojoAbstract<IGeneralServiceBean> {

	

	/**
	 * 許可されないベースステータス
	 */
	List<String> withoutPermissionsByBaseStatus = new ArrayList<String>();
	/**
	 * 許可されない全体ステータス
	 */
	List<String> withoutPermissionsByStatus = new ArrayList<String>();

	public final List<String> getWithoutPermissionsByBaseStatus() {
		return withoutPermissionsByBaseStatus;
	}
	public final void setWithoutPermissionsByBaseStatus(
			List<String> withoutPermissionsByBaseStatus) {
		this.withoutPermissionsByBaseStatus = withoutPermissionsByBaseStatus;
	}
	public final List<String> getWithoutPermissionsByStatus() {
		return withoutPermissionsByStatus;
	}
	public final void setWithoutPermissionsByStatus(
			List<String> withoutPermissionsByStatus) {
		this.withoutPermissionsByStatus = withoutPermissionsByStatus;
	}

}
