package jp.co.blueship.tri.fw.sm.beans.matrix;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.dm.dao.dmdo.eb.IDmDoEntity;
import jp.co.blueship.tri.fw.cmn.utils.ExtractFinderAddonUtils;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RmDesignBeanId;
import jp.co.blueship.tri.fw.constants.UmDesignBeanId;
import jp.co.blueship.tri.fw.constants.status.DmDoStatusId;
import jp.co.blueship.tri.fw.constants.status.RmRpStatusId;
import jp.co.blueship.tri.fw.constants.status.RmRpStatusIdForExecData;
import jp.co.blueship.tri.fw.dao.orm.FinderSupport;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.msg.MessageParameter;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckParamInfoByRel;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.ExtractStatusCheckUtils;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.RpCondition;

/**
 * （関連するリリース）アクション間のステータスマトリクスを検査します。
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author yukihiro eguchi
 *
 */
public class StatusCheckByRel extends GenericStatusCheck {
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	/**
	 * 許可されない配布ベースステータス
	 */
	List<String> withoutPermissionsByDistributeBaseStatus = new ArrayList<String>();

	public final List<String> getWithoutPermissionsByDistributeBaseStatus() {
		return withoutPermissionsByDistributeBaseStatus;
	}
	public final void setWithoutPermissionsByDistributeBaseStatus(
			List<String> withoutPermissionsByDistributeBaseStatus) {
		this.withoutPermissionsByDistributeBaseStatus = withoutPermissionsByDistributeBaseStatus;
	}

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {
		this.outputBLStartLog( this.getClass().getName() );

		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]>	messageArgsList	= new ArrayList<String[]>();

		List<Object> paramList = serviceDto.getParamList();

		try {
			FinderSupport finder = ExtractFinderAddonUtils.extractFinderSupport( paramList );
			StatusCheckParamInfoByRel param = ExtractStatusCheckUtils.extractStatusCheckParamInfoByRel( paramList );
			param.setValid(false);

			if ( TriStringUtils.isEmpty( param.getRpIds() ) )
				return serviceDto;

			RpCondition condition = new RpCondition();
			condition.setDelStsId( null );
			condition.setRpIds( param.getRpIds() );

			IEntityLimit<IRpEntity> limit = finder.getRmFinderSupport().getRpDao().find(condition.getCondition(), new SortBuilder(), 1, 0);

			IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
			GenericServiceBean serviceBean = (GenericServiceBean) serviceDto.getServiceBean();
			for ( IRpEntity entity : limit.getEntities() ) {
				if ( StatusFlg.on.value().equals(entity.getDelStsId().value() ) ) {
					messageList.add		( SmMessageId.SM001014E );
					messageArgsList.add	( new String[] { ca.getValue( MessageParameter.TARGET_REL.getKey(), serviceBean.getLanguage() ), entity.getRpId() } );
					continue;
				}

				for ( String status : this.getWithoutPermissionsByBaseStatus() ) {
					String curStatusId = entity.getStsId() ;
					if ( status.equals( curStatusId ) ) {
						String curStatusLabel = "";
						String flowAction = sheet.getValue( UmDesignBeanId.actionId, param.getFlowAction());;
						if( serviceBean.isStatusMatrixV3() ) {
							curStatusLabel = sheet.getValue( RmDesignBeanId.statusId, curStatusId );
						} else {
							curStatusLabel = ca.getValue(RmRpStatusId.value(curStatusId).getMessageKey(), serviceBean.getLanguage());
						}
						messageList.add		( SmMessageId.SM001015E );
						messageArgsList.add	( new String[] { ca.getValue( MessageParameter.TARGET_REL.getKey(), serviceBean.getLanguage() ),
																entity.getRpId(),
																ca.getValue( MessageParameter.BASE.getKey(), serviceBean.getLanguage() ) ,
																flowAction ,
																curStatusLabel } );
					}
				}

				for ( String status : this.getWithoutPermissionsByStatus() ) {
					String curStatusId = entity.getProcStsId() ;
					if ( status.equals( curStatusId ) ) {
						String curStatusLabel = "";
						String flowAction = sheet.getValue( UmDesignBeanId.actionId, param.getFlowAction());;
						if( serviceBean.isStatusMatrixV3() ) {
							curStatusLabel = sheet.getValue( RmDesignBeanId.statusId, curStatusId );
						} else {
							curStatusLabel = ca.getValue(RmRpStatusIdForExecData.value(curStatusId).getMessageKey(), serviceBean.getLanguage());
						}
						messageList.add		( SmMessageId.SM001015E );
						messageArgsList.add	( new String[] { ca.getValue( MessageParameter.TARGET_REL.getKey(), serviceBean.getLanguage() ),
																entity.getRpId(),
																"" ,
																flowAction ,
																curStatusLabel } );
					}
				}
				IDmDoEntity dmDoEntity = finder.getRmFinderSupport().getDmDoEntity(entity);
				if ( null != dmDoEntity ) {
					for ( String status : this.getWithoutPermissionsByDistributeBaseStatus() ) {
						String curStatusId = dmDoEntity.getStsId() ;
						if ( status.equals( curStatusId ) ) {
							String curStatusLabel = "";
							String flowAction = sheet.getValue( UmDesignBeanId.actionId, param.getFlowAction());;
							if( serviceBean.isStatusMatrixV3() ) {
								curStatusLabel = sheet.getValue( RmDesignBeanId.statusId, curStatusId );
							} else {
								curStatusLabel = ca.getValue(DmDoStatusId.value(curStatusId).getMessageKey(), serviceBean.getLanguage());
							}
							messageList.add		( SmMessageId.SM001015E );
							messageArgsList.add	( new String[] { ca.getValue( MessageParameter.TARGET_REL.getKey(), serviceBean.getLanguage() ),
																	entity.getRpId(),
																	ca.getValue( MessageParameter.ASSET_DISTRIBUTION_BASE.getKey(), serviceBean.getLanguage() ),
																	flowAction ,
																	curStatusLabel } );
						}
					}
				}

			}

			if ( 0 != messageList.size() ) {
				if ( ! param.isValidationMode() )
					throw new ContinuableBusinessException( messageList, messageArgsList );
			} else {
				param.setValid(true);
			}

		} finally {
			this.outputBLEndLog( this.getClass().getName() );
		}

		return serviceDto;
	}

}
