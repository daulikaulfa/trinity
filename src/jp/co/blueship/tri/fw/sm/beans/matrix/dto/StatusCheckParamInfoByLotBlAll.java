package jp.co.blueship.tri.fw.sm.beans.matrix.dto;



/**
 * （関連する全ロットベースライン）アクション間のステータスマトリクスメッセージ
 * <br>
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */

public class StatusCheckParamInfoByLotBlAll extends StatusCheckParamInfo {

	private static final long serialVersionUID = 6369924923111085068L;

}
