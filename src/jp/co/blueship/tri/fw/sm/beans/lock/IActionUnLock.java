package jp.co.blueship.tri.fw.sm.beans.lock;

import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.svc.beans.dto.ILockParamInfo;

/**
 * アクションに関連する業務排他を行います。
 * <br>この処理は、業務排他を行うために、一時的にテーブルにＤＢ排他を行います。
 * <br>そのため、このメソッドのlock内は、DIコンテナで独立した
 * トランザクションとして管理される必要があります。
 *
 * @author Yukihiro Eguchi
 *
 */
public interface IActionUnLock {

	

	/**
    * 排他処理を解除します。
     * <br>指定した実行アクションＩＤに紐づく、制限されるアクションＩＤをすべて解除します。
	 * @param info 排他共通情報
	 * @throws BaseBusinessException
	 */
	public void unLock( ILockParamInfo info )  throws BaseBusinessException;
}
