package jp.co.blueship.tri.fw.sm.beans.matrix.utils;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.TriObjectUtils;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.IStatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.IStatusCheckParamInfo;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckParamInfoByAssetApply;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckParamInfoByAssetApplyAll;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckParamInfoByHeadBl;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckParamInfoByLot;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckParamInfoByLotAll;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckParamInfoByLotBl;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckParamInfoByLotBlAll;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckParamInfoByPjt;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckParamInfoByPjtAll;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckParamInfoByRel;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckParamInfoByRelAll;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckParamInfoByRelApply;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckParamInfoByRelUnit;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckParamInfoByRelUnitAll;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckParamInfoByReport;

/**
 * Common processing of status check
 *
 * @version V3L10R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class StatusMatrixCheckUtils {

	/**
	 * Do the status-matrix check of action.
	 *
	 * @param statusCheckDto DTO for status matrix
	 * @return
	 */
	public static boolean checkStatusMatrix( IStatusCheckDto statusCheckDto ) {
		return checkStatusMatrix( statusCheckDto, false );

	}

	/**
	 * Do the status-matrix check of action.
	 *
	 * @param statusCheckDto DTO for status matrix
	 * @param isValidationMode if you want to check the Status in isValid, Set true.
	 * @return
	 */
	public static boolean checkStatusMatrix( IStatusCheckDto statusCheckDto, boolean isValidationMode ) {

		ActionStatusMatrixList actionList = statusCheckDto.getActionList();
		IGeneralServiceBean serviceBean = statusCheckDto.getServiceBean();

		List<Object> paramList = new ArrayList<Object>();
		paramList.add( statusCheckDto.getFinder() );
		for ( List<IDomain<IGeneralServiceBean>> list: actionList.getActions().values() ) {
			paramList.addAll( list );
		}

		//ロット・ベースラインのチェック
		{
			String[] lotBlIds = statusCheckDto.getLotBls();

			if ( ExtractStatusCheckUtils.isStatusCheckByLotBl( paramList ) ) {
				StatusCheckParamInfoByLotBl lotBlParam = new StatusCheckParamInfoByLotBl();
				lotBlParam.setValidationMode(isValidationMode);
				lotBlParam.setFlowAction( serviceBean.getFlowAction() );
				lotBlParam.setLotBlIds( TriObjectUtils.defaultIfNull(lotBlIds, new String[0]) );
				paramList.add( lotBlParam );
			}

			if ( ExtractStatusCheckUtils.isStatusCheckByLotAll( paramList ) ) {
				StatusCheckParamInfoByLotBlAll lotBlAllParam = new StatusCheckParamInfoByLotBlAll();
				lotBlAllParam.setValidationMode(isValidationMode);
				lotBlAllParam.setFlowAction( serviceBean.getFlowAction() );
				paramList.add( lotBlAllParam );
			}
		}

		//HEAD・ベースラインのチェック
		{
			String[] headBlIds = statusCheckDto.getHeadBls();

			if ( ExtractStatusCheckUtils.isStatusCheckByHeadBl( paramList ) ) {
				StatusCheckParamInfoByHeadBl headBlParam = new StatusCheckParamInfoByHeadBl();
				headBlParam.setValidationMode(isValidationMode);
				headBlParam.setFlowAction( serviceBean.getFlowAction() );
				headBlParam.setHeadBlIds( TriObjectUtils.defaultIfNull(headBlIds, new String[0]) );
				paramList.add( headBlParam );
			}
		}

		//ロットのチェック
		{
			String[] lotIds = statusCheckDto.getLotIds();

			if ( ExtractStatusCheckUtils.isStatusCheckByLot( paramList ) ) {
				StatusCheckParamInfoByLot lotParam = new StatusCheckParamInfoByLot();
				lotParam.setValidationMode(isValidationMode);
				lotParam.setFlowAction( serviceBean.getFlowAction() );
				lotParam.setLotIds( TriObjectUtils.defaultIfNull(lotIds, new String[0]) );
				paramList.add( lotParam );
			}

			if ( ExtractStatusCheckUtils.isStatusCheckByLotAll( paramList ) ) {
				StatusCheckParamInfoByLotAll lotAllParam = new StatusCheckParamInfoByLotAll();
				lotAllParam.setValidationMode(isValidationMode);
				lotAllParam.setFlowAction( serviceBean.getFlowAction() );
				paramList.add( lotAllParam );
			}
		}

		//変更管理のチェック
		{
			String[] pjtIds = statusCheckDto.getPjtIds();

			if ( ExtractStatusCheckUtils.isStatusCheckByPjt( paramList ) ) {
				StatusCheckParamInfoByPjt pjtParam = new StatusCheckParamInfoByPjt();
				pjtParam.setValidationMode(isValidationMode);
				pjtParam.setFlowAction( serviceBean.getFlowAction() );
				pjtParam.setPjtIds( TriObjectUtils.defaultIfNull(pjtIds, new String[0]) );
				paramList.add( pjtParam );
			}

			if ( ExtractStatusCheckUtils.isStatusCheckByPjtAll( paramList ) ) {
				StatusCheckParamInfoByPjtAll pjtAllParam = new StatusCheckParamInfoByPjtAll();
				pjtAllParam.setValidationMode(isValidationMode);
				pjtAllParam.setFlowAction( serviceBean.getFlowAction() );
				paramList.add( pjtAllParam );
			}
		}

		//資産申請のチェック
		{
			String[] areqIds = statusCheckDto.getAreqIds();

			if ( ExtractStatusCheckUtils.isStatusCheckByAssetApply( paramList ) ) {
				StatusCheckParamInfoByAssetApply applyParam = new StatusCheckParamInfoByAssetApply();
				applyParam.setValidationMode(isValidationMode);
				applyParam.setFlowAction( serviceBean.getFlowAction() );
				applyParam.setAreqIds( TriObjectUtils.defaultIfNull(areqIds, new String[0]) );
				paramList.add( applyParam );
			}

			if ( ExtractStatusCheckUtils.isStatusCheckByAssetApplyAll( paramList ) ) {
				StatusCheckParamInfoByAssetApplyAll assetApplyAllParam = new StatusCheckParamInfoByAssetApplyAll();
				assetApplyAllParam.setValidationMode(isValidationMode);
				assetApplyAllParam.setFlowAction( serviceBean.getFlowAction() );
				paramList.add( assetApplyAllParam );
			}
		}

		//ビルドパッケージのチェック
		{
			String[] bpIds = statusCheckDto.getBpIds();

			if ( ExtractStatusCheckUtils.isStatusCheckByRelUnit( paramList ) ) {
				StatusCheckParamInfoByRelUnit relUnitParam = new StatusCheckParamInfoByRelUnit();
				relUnitParam.setValidationMode(isValidationMode);
				relUnitParam.setFlowAction( serviceBean.getFlowAction() );
				relUnitParam.setBpIds( TriObjectUtils.defaultIfNull(bpIds, new String[0]) );
				paramList.add( relUnitParam );
			}

			if ( ExtractStatusCheckUtils.isStatusCheckByRelUnitAll( paramList ) ) {
				StatusCheckParamInfoByRelUnitAll relUnitAllParam = new StatusCheckParamInfoByRelUnitAll();
				relUnitAllParam.setValidationMode(isValidationMode);
				relUnitAllParam.setFlowAction( serviceBean.getFlowAction() );
				paramList.add( relUnitAllParam );
			}
		}

		//リリースのチェック
		{
			String[] rpIds = statusCheckDto.getRpIds();

			if ( ExtractStatusCheckUtils.isStatusCheckByRel( paramList ) ) {
				StatusCheckParamInfoByRel relParam = new StatusCheckParamInfoByRel();
				relParam.setValidationMode(isValidationMode);
				relParam.setFlowAction( serviceBean.getFlowAction() );
				relParam.setRpIds( TriObjectUtils.defaultIfNull(rpIds, new String[0]) );
				paramList.add( relParam );
			}

			if ( ExtractStatusCheckUtils.isStatusCheckByRelAll( paramList ) ) {
				StatusCheckParamInfoByRelAll relAllParam = new StatusCheckParamInfoByRelAll();
				relAllParam.setValidationMode(isValidationMode);
				relAllParam.setFlowAction( serviceBean.getFlowAction() );
				paramList.add( relAllParam );
			}
		}

		//リリース申請のチェック
		{
			String[] raIds = statusCheckDto.getRaIds();

			if ( ExtractStatusCheckUtils.isStatusCheckByRelApply( paramList ) ) {
				StatusCheckParamInfoByRelApply relParam = new StatusCheckParamInfoByRelApply();
				relParam.setValidationMode(isValidationMode);
				relParam.setFlowAction( serviceBean.getFlowAction() );
				relParam.setRaIds( TriObjectUtils.defaultIfNull(raIds, new String[0]) );
				paramList.add( relParam );
			}

			if ( ExtractStatusCheckUtils.isStatusCheckByRelApplyAll( paramList ) ) {
				StatusCheckParamInfoByRelAll relAllParam = new StatusCheckParamInfoByRelAll();
				relAllParam.setValidationMode(isValidationMode);
				relAllParam.setFlowAction( serviceBean.getFlowAction() );
				paramList.add( relAllParam );
			}
		}

		//レポートのチェック
		{
			String[] repIds = statusCheckDto.getRepIds();

			if ( ExtractStatusCheckUtils.isStatusCheckByReport( paramList ) ) {
				StatusCheckParamInfoByReport reportParam = new StatusCheckParamInfoByReport();
				reportParam.setValidationMode(isValidationMode);
				reportParam.setFlowAction( serviceBean.getFlowAction() );
				reportParam.setRepIds( TriObjectUtils.defaultIfNull(repIds, new String[0]) );
				paramList.add( reportParam );
			}
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>()
			.setServiceBean( serviceBean )
			.setParamList( paramList );

		actionList.execute( serviceDto );

		boolean isValid = true;
		if ( isValidationMode ) {
			for ( IStatusCheckParamInfo param: ExtractStatusCheckUtils.extractStatusCheckParamInfo(paramList) ) {
				if ( ! param.isValid() ) {
					isValid = false;
					break;
				}
			}

			return isValid;
		} else {
			return isValid;
		}

	}

}
