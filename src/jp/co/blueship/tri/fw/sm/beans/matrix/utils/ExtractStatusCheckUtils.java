package jp.co.blueship.tri.fw.sm.beans.matrix.utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import jp.co.blueship.tri.fw.sm.beans.matrix.StatusCheckByAssetApply;
import jp.co.blueship.tri.fw.sm.beans.matrix.StatusCheckByAssetApplyAll;
import jp.co.blueship.tri.fw.sm.beans.matrix.StatusCheckByHeadBl;
import jp.co.blueship.tri.fw.sm.beans.matrix.StatusCheckByLot;
import jp.co.blueship.tri.fw.sm.beans.matrix.StatusCheckByLotAll;
import jp.co.blueship.tri.fw.sm.beans.matrix.StatusCheckByLotBl;
import jp.co.blueship.tri.fw.sm.beans.matrix.StatusCheckByLotBlAll;
import jp.co.blueship.tri.fw.sm.beans.matrix.StatusCheckByPjt;
import jp.co.blueship.tri.fw.sm.beans.matrix.StatusCheckByPjtAll;
import jp.co.blueship.tri.fw.sm.beans.matrix.StatusCheckByRel;
import jp.co.blueship.tri.fw.sm.beans.matrix.StatusCheckByRelAll;
import jp.co.blueship.tri.fw.sm.beans.matrix.StatusCheckByRelApply;
import jp.co.blueship.tri.fw.sm.beans.matrix.StatusCheckByRelApplyAll;
import jp.co.blueship.tri.fw.sm.beans.matrix.StatusCheckByRelUnit;
import jp.co.blueship.tri.fw.sm.beans.matrix.StatusCheckByRelUnitAll;
import jp.co.blueship.tri.fw.sm.beans.matrix.StatusCheckByReport;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.IStatusCheckParamInfo;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckParamInfoByAssetApply;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckParamInfoByAssetApplyAll;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckParamInfoByHeadBl;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckParamInfoByLot;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckParamInfoByLotAll;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckParamInfoByLotBl;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckParamInfoByLotBlAll;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckParamInfoByPjt;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckParamInfoByPjtAll;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckParamInfoByRel;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckParamInfoByRelAll;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckParamInfoByRelApply;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckParamInfoByRelApplyAll;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckParamInfoByRelCancel;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckParamInfoByRelUnit;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckParamInfoByRelUnitAll;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckParamInfoByReport;

/**
 * アクション間のステータスマトリクスを扱うユーティリティークラスです。
 * <br>キャッシュエンティティを抽出します。
 *
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public class ExtractStatusCheckUtils {

	/**
	 * アクション間のステータスマトリクスメッセージを取得します。
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static List<IStatusCheckParamInfo> extractStatusCheckParamInfo( List<Object> list ) {
		List<IStatusCheckParamInfo> statusCheckList = new ArrayList<IStatusCheckParamInfo>();

		for ( Object obj: list ) {
			if ( obj instanceof IStatusCheckParamInfo ) {
				statusCheckList.add( (IStatusCheckParamInfo)obj );
			}
		}

		return statusCheckList;
	}

	/**
	 * （関連するロットベースライン）アクション間のステータスマトリクスメッセージを取得します。
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static StatusCheckParamInfoByLotBl extractStatusCheckParamInfoByLotBl( List<Object> list ) {

		for ( Iterator<Object> it = list.iterator(); it.hasNext(); ) {
			Object obj = (Object)it.next();

			if ( obj instanceof StatusCheckParamInfoByLotBl ) {
				StatusCheckParamInfoByLotBl message = (StatusCheckParamInfoByLotBl)obj;

				return message;
			}
		}

		return null;
	}

	/**
	 * （関連する全ロットベースライン）アクション間のステータスマトリクスメッセージを取得します。
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static StatusCheckParamInfoByLotBlAll extractStatusCheckParamInfoByLotBlAll( List<Object> list ) {

		for ( Iterator<Object> it = list.iterator(); it.hasNext(); ) {
			Object obj = (Object)it.next();

			if ( obj instanceof StatusCheckParamInfoByLotBlAll ) {
				StatusCheckParamInfoByLotBlAll message = (StatusCheckParamInfoByLotBlAll)obj;

				return message;
			}
		}

		return null;
	}

	/**
	 * （関連するHEADベースライン）アクション間のステータスマトリクスメッセージを取得します。
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static StatusCheckParamInfoByHeadBl extractStatusCheckParamInfoByHeadBl( List<Object> list ) {

		for ( Iterator<Object> it = list.iterator(); it.hasNext(); ) {
			Object obj = (Object)it.next();

			if ( obj instanceof StatusCheckParamInfoByHeadBl ) {
				StatusCheckParamInfoByHeadBl message = (StatusCheckParamInfoByHeadBl)obj;

				return message;
			}
		}

		return null;
	}

	/**
	 * （関連するロット）アクション間のステータスマトリクスメッセージを取得します。
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static StatusCheckParamInfoByLot extractStatusCheckParamInfoByLot( List<Object> list ) {

		for ( Iterator<Object> it = list.iterator(); it.hasNext(); ) {
			Object obj = (Object)it.next();

			if ( obj instanceof StatusCheckParamInfoByLot ) {
				StatusCheckParamInfoByLot message = (StatusCheckParamInfoByLot)obj;

				return message;
			}
		}

		return null;
	}

	/**
	 * （関連する全ロット）アクション間のステータスマトリクスメッセージを取得します。
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static StatusCheckParamInfoByLotAll extractStatusCheckParamInfoByLotAll( List<Object> list ) {

		for ( Iterator<Object> it = list.iterator(); it.hasNext(); ) {
			Object obj = (Object)it.next();

			if ( obj instanceof StatusCheckParamInfoByLotAll ) {
				StatusCheckParamInfoByLotAll message = (StatusCheckParamInfoByLotAll)obj;

				return message;
			}
		}

		return null;
	}

	/**
	 * （関連する変更管理）アクション間のステータスマトリクスメッセージを取得します。
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static StatusCheckParamInfoByPjt extractStatusCheckParamInfoByPjt( List<Object> list ) {

		for ( Iterator<Object> it = list.iterator(); it.hasNext(); ) {
			Object obj = (Object)it.next();

			if ( obj instanceof StatusCheckParamInfoByPjt ) {
				StatusCheckParamInfoByPjt message = (StatusCheckParamInfoByPjt)obj;

				return message;
			}
		}

		return null;
	}

	/**
	 * （関連する全変更管理）アクション間のステータスマトリクスメッセージを取得します。
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static StatusCheckParamInfoByPjtAll extractStatusCheckParamInfoByPjtAll( List<Object> list ) {

		for ( Iterator<Object> it = list.iterator(); it.hasNext(); ) {
			Object obj = (Object)it.next();

			if ( obj instanceof StatusCheckParamInfoByPjtAll ) {
				StatusCheckParamInfoByPjtAll message = (StatusCheckParamInfoByPjtAll)obj;

				return message;
			}
		}

		return null;
	}

	/**
	 * （関連する申請情報）アクション間のステータスマトリクスメッセージを取得します。
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static StatusCheckParamInfoByAssetApply extractStatusCheckParamInfoByAssetApply( List<Object> list ) {

		for ( Iterator<Object> it = list.iterator(); it.hasNext(); ) {
			Object obj = (Object)it.next();

			if ( obj instanceof StatusCheckParamInfoByAssetApply ) {
				StatusCheckParamInfoByAssetApply message = (StatusCheckParamInfoByAssetApply)obj;

				return message;
			}
		}

		return null;
	}

	/**
	 * （関連する全申請情報）アクション間のステータスマトリクスメッセージを取得します。
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static StatusCheckParamInfoByAssetApplyAll extractStatusCheckParamInfoByAssetApplyAll( List<Object> list ) {

		for ( Iterator<Object> it = list.iterator(); it.hasNext(); ) {
			Object obj = (Object)it.next();

			if ( obj instanceof StatusCheckParamInfoByAssetApplyAll ) {
				StatusCheckParamInfoByAssetApplyAll message = (StatusCheckParamInfoByAssetApplyAll)obj;

				return message;
			}
		}

		return null;
	}

	/**
	 * （関連するビルドパッケージ）アクション間のステータスマトリクスメッセージを取得します。
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static StatusCheckParamInfoByRelUnit extractStatusCheckParamInfoByRelUnit( List<Object> list ) {

		for ( Iterator<Object> it = list.iterator(); it.hasNext(); ) {
			Object obj = (Object)it.next();

			if ( obj instanceof StatusCheckParamInfoByRelUnit ) {
				StatusCheckParamInfoByRelUnit message = (StatusCheckParamInfoByRelUnit)obj;

				return message;
			}
		}

		return null;
	}

	/**
	 * （関連する全ビルドパッケージ）アクション間のステータスマトリクスメッセージを取得します。
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static StatusCheckParamInfoByRelUnitAll extractStatusCheckParamInfoByRelUnitAll( List<Object> list ) {

		for ( Iterator<Object> it = list.iterator(); it.hasNext(); ) {
			Object obj = (Object)it.next();

			if ( obj instanceof StatusCheckParamInfoByRelUnitAll ) {
				StatusCheckParamInfoByRelUnitAll message = (StatusCheckParamInfoByRelUnitAll)obj;

				return message;
			}
		}

		return null;
	}

	/**
	 * （関連するビルドパッケージ）アクション間のステータスマトリクスメッセージを取得します。
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static StatusCheckParamInfoByRelApply extractStatusCheckParamInfoByRelApply( List<Object> list ) {

		for ( Iterator<Object> it = list.iterator(); it.hasNext(); ) {
			Object obj = (Object)it.next();

			if ( obj instanceof StatusCheckParamInfoByRelApply ) {
				StatusCheckParamInfoByRelApply message = (StatusCheckParamInfoByRelApply)obj;

				return message;
			}
		}

		return null;
	}

	/**
	 * （関連する全ビルドパッケージ）アクション間のステータスマトリクスメッセージを取得します。
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static StatusCheckParamInfoByRelApplyAll extractStatusCheckParamInfoByRelApplyAll( List<Object> list ) {

		for ( Iterator<Object> it = list.iterator(); it.hasNext(); ) {
			Object obj = (Object)it.next();

			if ( obj instanceof StatusCheckParamInfoByRelApplyAll ) {
				StatusCheckParamInfoByRelApplyAll message = (StatusCheckParamInfoByRelApplyAll)obj;

				return message;
			}
		}

		return null;
	}

	/**
	 * （関連するリリース）アクション間のステータスマトリクスメッセージを取得します。
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static StatusCheckParamInfoByRel extractStatusCheckParamInfoByRel( List<Object> list ) {

		for ( Iterator<Object> it = list.iterator(); it.hasNext(); ) {
			Object obj = (Object)it.next();

			if ( obj instanceof StatusCheckParamInfoByRel ) {
				StatusCheckParamInfoByRel message = (StatusCheckParamInfoByRel)obj;

				return message;
			}
		}

		return null;
	}

	/**
	 * （関連する全リリース）アクション間のステータスマトリクスメッセージを取得します。
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static StatusCheckParamInfoByRelAll extractStatusCheckParamInfoByRelAll( List<Object> list ) {

		for ( Iterator<Object> it = list.iterator(); it.hasNext(); ) {
			Object obj = (Object)it.next();

			if ( obj instanceof StatusCheckParamInfoByRelAll ) {
				StatusCheckParamInfoByRelAll message = (StatusCheckParamInfoByRelAll)obj;

				return message;
			}
		}

		return null;
	}

	/**
	 * （関連するリリース）アクション間のステータスマトリクスメッセージを取得します。
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static StatusCheckParamInfoByRelCancel extractStatusCheckParamInfoByRelCancel( List<Object> list ) {

		for ( Iterator<Object> it = list.iterator(); it.hasNext(); ) {
			Object obj = (Object)it.next();

			if ( obj instanceof StatusCheckParamInfoByRelCancel ) {
				StatusCheckParamInfoByRelCancel message = (StatusCheckParamInfoByRelCancel)obj;

				return message;
			}
		}

		return null;
	}

	/**
	 * （関連するレポート）アクション間のステータスマトリクスメッセージを取得します。
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static StatusCheckParamInfoByReport extractStatusCheckParamInfoByReport( List<Object> list ) {

		for ( Iterator<Object> it = list.iterator(); it.hasNext(); ) {
			Object obj = (Object)it.next();

			if ( obj instanceof StatusCheckParamInfoByReport ) {
				StatusCheckParamInfoByReport message = (StatusCheckParamInfoByReport)obj;

				return message;
			}
		}

		return null;
	}

	/**
	 * ロットベースラインのステータスをチェックするかどうかを判定します。
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static boolean isStatusCheckByLotBl( List<Object> list ) {
		return ( null == extractStatusCheckByLotBl(list) )? false: true;

	}

	/**
	 * 全ロットベースラインのステータスをチェックするかどうかを判定します。
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static boolean isStatusCheckByLotBlAll( List<Object> list ) {
		return ( null == extractStatusCheckByLotBlAll(list) )? false: true;

	}

	/**
	 * HEADベースラインのステータスをチェックするかどうかを判定します。
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static boolean isStatusCheckByHeadBl( List<Object> list ) {
		return ( null == extractStatusCheckByHeadBl(list) )? false: true;

	}

	/**
	 * ロットのステータスをチェックするかどうかを判定します。
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static boolean isStatusCheckByLot( List<Object> list ) {
		return ( null == extractStatusCheckByLot(list) )? false: true;

	}

	/**
	 * 全ロットのステータスをチェックするかどうかを判定します。
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static boolean isStatusCheckByLotAll( List<Object> list ) {
		return ( null == extractStatusCheckByLotAll(list) )? false: true;

	}

	/**
	 * 変更管理のステータスをチェックするかどうかを判定します。
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static boolean isStatusCheckByPjt( List<Object> list ) {
		return ( null == extractStatusCheckByPjt(list) )? false: true;

	}

	/**
	 * 全変更管理のステータスをチェックするかどうかを判定します。
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static boolean isStatusCheckByPjtAll( List<Object> list ) {
		return ( null == extractStatusCheckByPjtAll(list) )? false: true;

	}

	/**
	 * 申請情報のステータスをチェックするかどうかを判定します。
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static boolean isStatusCheckByAssetApply( List<Object> list ) {
		return ( null == extractStatusCheckByAssetApply(list) )? false: true;

	}

	/**
	 * 全申請情報のステータスをチェックするかどうかを判定します。
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static boolean isStatusCheckByAssetApplyAll( List<Object> list ) {
		return ( null == extractStatusCheckByAssetApplyAll(list) )? false: true;

	}

	/**
	 * ビルドパッケージのステータスをチェックするかどうかを判定します。
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static boolean isStatusCheckByRelUnit( List<Object> list ) {
		return ( null == extractStatusCheckByRelUnit(list) )? false: true;

	}

	/**
	 * 全ビルドパッケージのステータスをチェックするかどうかを判定します。
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static boolean isStatusCheckByRelUnitAll( List<Object> list ) {
		return ( null == extractStatusCheckByRelUnitAll(list) )? false: true;

	}

	/**
	 * リリース申請のステータスをチェックするかどうかを判定します。
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static boolean isStatusCheckByRelApply( List<Object> list ) {
		return ( null == extractStatusCheckByRelApply(list) )? false: true;

	}

	/**
	 * 全リリース申請のステータスをチェックするかどうかを判定します。
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static boolean isStatusCheckByRelApplyAll( List<Object> list ) {
		return ( null == extractStatusCheckByRelApplyAll(list) )? false: true;

	}

	/**
	 * リリースのステータスをチェックするかどうかを判定します。
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static boolean isStatusCheckByRel( List<Object> list ) {
		return ( null == extractStatusCheckByRel(list) )? false: true;

	}

	/**
	 * 全リリースのステータスをチェックするかどうかを判定します。
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static boolean isStatusCheckByRelAll( List<Object> list ) {
		return ( null == extractStatusCheckByRelAll(list) )? false: true;

	}

	/**
	 * レポートのステータスをチェックするかどうかを判定します。
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static boolean isStatusCheckByReport( List<Object> list ) {
		return ( null == extractStatusCheckByReport(list) )? false: true;

	}

	/**
	 * （関連するロットベースライン）アクション間のステータスマトリクスを取得します。
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static StatusCheckByLotBl extractStatusCheckByLotBl( List<Object> list ) {

		for ( Iterator<Object> it = list.iterator(); it.hasNext(); ) {
			Object obj = (Object)it.next();

			if ( obj instanceof StatusCheckByLotBl ) {
				StatusCheckByLotBl pojo = (StatusCheckByLotBl)obj;

				return pojo;
			}
		}

		return null;
	}

	/**
	 * （関連する全ロットベースライン）アクション間のステータスマトリクスを取得します。
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static StatusCheckByLotBlAll extractStatusCheckByLotBlAll( List<Object> list ) {

		for ( Iterator<Object> it = list.iterator(); it.hasNext(); ) {
			Object obj = (Object)it.next();

			if ( obj instanceof StatusCheckByLotBlAll ) {
				StatusCheckByLotBlAll pojo = (StatusCheckByLotBlAll)obj;

				return pojo;
			}
		}

		return null;
	}

	/**
	 * （関連するHEADベースライン）アクション間のステータスマトリクスを取得します。
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static StatusCheckByHeadBl extractStatusCheckByHeadBl( List<Object> list ) {

		for ( Iterator<Object> it = list.iterator(); it.hasNext(); ) {
			Object obj = (Object)it.next();

			if ( obj instanceof StatusCheckByHeadBl ) {
				StatusCheckByHeadBl pojo = (StatusCheckByHeadBl)obj;

				return pojo;
			}
		}

		return null;
	}

	/**
	 * （関連するロット）アクション間のステータスマトリクスを取得します。
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static StatusCheckByLot extractStatusCheckByLot( List<Object> list ) {

		for ( Iterator<Object> it = list.iterator(); it.hasNext(); ) {
			Object obj = (Object)it.next();

			if ( obj instanceof StatusCheckByLot ) {
				StatusCheckByLot pojo = (StatusCheckByLot)obj;

				return pojo;
			}
		}

		return null;
	}

	/**
	 * （関連する全ロット）アクション間のステータスマトリクスを取得します。
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static StatusCheckByLotAll extractStatusCheckByLotAll( List<Object> list ) {

		for ( Iterator<Object> it = list.iterator(); it.hasNext(); ) {
			Object obj = (Object)it.next();

			if ( obj instanceof StatusCheckByLotAll ) {
				StatusCheckByLotAll pojo = (StatusCheckByLotAll)obj;

				return pojo;
			}
		}

		return null;
	}

	/**
	 * （関連する変更管理）アクション間のステータスマトリクスを取得します。
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static StatusCheckByPjt extractStatusCheckByPjt( List<Object> list ) {

		for ( Iterator<Object> it = list.iterator(); it.hasNext(); ) {
			Object obj = (Object)it.next();

			if ( obj instanceof StatusCheckByPjt ) {
				StatusCheckByPjt pojo = (StatusCheckByPjt)obj;

				return pojo;
			}
		}

		return null;
	}

	/**
	 * （関連する全変更管理）アクション間のステータスマトリクスを取得します。
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static StatusCheckByPjtAll extractStatusCheckByPjtAll( List<Object> list ) {

		for ( Iterator<Object> it = list.iterator(); it.hasNext(); ) {
			Object obj = (Object)it.next();

			if ( obj instanceof StatusCheckByPjtAll ) {
				StatusCheckByPjtAll pojo = (StatusCheckByPjtAll)obj;

				return pojo;
			}
		}

		return null;
	}

	/**
	 * （関連する申請情報）アクション間のステータスマトリクスを取得します。
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static StatusCheckByAssetApply extractStatusCheckByAssetApply( List<Object> list ) {

		for ( Iterator<Object> it = list.iterator(); it.hasNext(); ) {
			Object obj = (Object)it.next();

			if ( obj instanceof StatusCheckByAssetApply ) {
				StatusCheckByAssetApply pojo = (StatusCheckByAssetApply)obj;

				return pojo;
			}
		}

		return null;
	}

	/**
	 * （関連する全申請情報）アクション間のステータスマトリクスを取得します。
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static StatusCheckByAssetApplyAll extractStatusCheckByAssetApplyAll( List<Object> list ) {

		for ( Iterator<Object> it = list.iterator(); it.hasNext(); ) {
			Object obj = (Object)it.next();

			if ( obj instanceof StatusCheckByAssetApplyAll ) {
				StatusCheckByAssetApplyAll pojo = (StatusCheckByAssetApplyAll)obj;

				return pojo;
			}
		}

		return null;
	}

	/**
	 * （関連するビルドパッケージ）アクション間のステータスマトリクスを取得します。
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static StatusCheckByRelUnit extractStatusCheckByRelUnit( List<Object> list ) {

		for ( Iterator<Object> it = list.iterator(); it.hasNext(); ) {
			Object obj = (Object)it.next();

			if ( obj instanceof StatusCheckByRelUnit ) {
				StatusCheckByRelUnit pojo = (StatusCheckByRelUnit)obj;

				return pojo;
			}
		}

		return null;
	}

	/**
	 * （関連する全ビルドパッケージ）アクション間のステータスマトリクスを取得します。
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static StatusCheckByRelUnitAll extractStatusCheckByRelUnitAll( List<Object> list ) {

		for ( Iterator<Object> it = list.iterator(); it.hasNext(); ) {
			Object obj = (Object)it.next();

			if ( obj instanceof StatusCheckByRelUnitAll ) {
				StatusCheckByRelUnitAll pojo = (StatusCheckByRelUnitAll)obj;

				return pojo;
			}
		}

		return null;
	}

	/**
	 * （関連するリリース申請）アクション間のステータスマトリクスを取得します。
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static StatusCheckByRelApply extractStatusCheckByRelApply( List<Object> list ) {

		for ( Iterator<Object> it = list.iterator(); it.hasNext(); ) {
			Object obj = (Object)it.next();

			if ( obj instanceof StatusCheckByRelApply ) {
				StatusCheckByRelApply pojo = (StatusCheckByRelApply)obj;

				return pojo;
			}
		}

		return null;
	}

	/**
	 * （関連する全リリース申請）アクション間のステータスマトリクスを取得します。
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static StatusCheckByRelApplyAll extractStatusCheckByRelApplyAll( List<Object> list ) {

		for ( Iterator<Object> it = list.iterator(); it.hasNext(); ) {
			Object obj = (Object)it.next();

			if ( obj instanceof StatusCheckByRelApplyAll ) {
				StatusCheckByRelApplyAll pojo = (StatusCheckByRelApplyAll)obj;

				return pojo;
			}
		}

		return null;
	}

	/**
	 * （関連するリリース）アクション間のステータスマトリクスを取得します。
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static StatusCheckByRel extractStatusCheckByRel( List<Object> list ) {

		for ( Iterator<Object> it = list.iterator(); it.hasNext(); ) {
			Object obj = (Object)it.next();

			if ( obj instanceof StatusCheckByRel ) {
				StatusCheckByRel pojo = (StatusCheckByRel)obj;

				return pojo;
			}
		}

		return null;
	}

	/**
	 * （関連する全リリース）アクション間のステータスマトリクスを取得します。
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static StatusCheckByRelAll extractStatusCheckByRelAll( List<Object> list ) {

		for ( Iterator<Object> it = list.iterator(); it.hasNext(); ) {
			Object obj = (Object)it.next();

			if ( obj instanceof StatusCheckByRelAll ) {
				StatusCheckByRelAll pojo = (StatusCheckByRelAll)obj;

				return pojo;
			}
		}

		return null;
	}

	/**
	 * （関連するレポート）アクション間のステータスマトリクスを取得します。
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static StatusCheckByReport extractStatusCheckByReport( List<Object> list ) {

		for ( Iterator<Object> it = list.iterator(); it.hasNext(); ) {
			Object obj = (Object)it.next();

			if ( obj instanceof StatusCheckByReport ) {
				StatusCheckByReport pojo = (StatusCheckByReport)obj;

				return pojo;
			}
		}

		return null;
	}
}
