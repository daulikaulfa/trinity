package jp.co.blueship.tri.fw.sm.beans.matrix.dto;



/**
 * （関連する変更管理）アクション間のステータスマトリクスメッセージ
 * <br>
 *
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
/**
 * @author 江口
 *
 */

public class StatusCheckParamInfoByPjtAll extends StatusCheckParamInfo {

	/**
	 *serialVersionUIDを自動生成
	 */
	private static final long serialVersionUID = 8480696541765429587L;

}
