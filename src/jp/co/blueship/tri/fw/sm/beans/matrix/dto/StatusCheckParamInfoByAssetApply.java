package jp.co.blueship.tri.fw.sm.beans.matrix.dto;



/**
 * （関連する申請情報）アクション間のステータスマトリクスメッセージ
 * <br>
 *
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class StatusCheckParamInfoByAssetApply extends StatusCheckParamInfo {

	private static final long serialVersionUID = -846456547349724288L;
	private String[] areqIds = null;

	public String[] getAreqIds() {
		return areqIds;
	}

	public void setAreqIds(String[] areqIds) {
		this.areqIds = areqIds;
	}


}
