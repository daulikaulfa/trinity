package jp.co.blueship.tri.fw.sm.beans.matrix.dto;



/**
 * （関連する全ビルドパッケージ）アクション間のステータスマトリクスメッセージ
 * <br>
 *
 * All Rights Reserved, Copyright(c) Blueship  2009-2012
 */
public class StatusCheckParamInfoByRelApplyAll extends StatusCheckParamInfo {

	/**
	 * 修正したらインクリメントすること
	 */
	private static final long serialVersionUID = 1L;

}
