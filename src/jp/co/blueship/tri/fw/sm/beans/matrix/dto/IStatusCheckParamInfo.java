package jp.co.blueship.tri.fw.sm.beans.matrix.dto;

/**
 * アクション間のステータスマトリクスメッセージ
 *
 * @version V3L10R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public interface IStatusCheckParamInfo {

	/**
	 * if you want to check the Status in isValid, Set true. The default is false.
	 *
	 * @return
	 */
	public boolean isValidationMode();

	/**
	 * In the case of validationMode = true, it returns the verification result.
	 *
	 * @return
	 */
	public boolean isValid();

}
