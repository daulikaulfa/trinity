package jp.co.blueship.tri.fw.sm.beans.matrix.dto;



/**
 * （関連するロットベースライン）アクション間のステータスマトリクスメッセージ
 * <br>
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 */

public class StatusCheckParamInfoByLotBl extends StatusCheckParamInfo {

	private static final long serialVersionUID = -8450526774763223834L;

	private String[] lotBlIds = null;

	public String[] getLotBlIds() {
		return lotBlIds;
	}

	public void setLotBlIds(String... relUnitNo) {
		this.lotBlIds = relUnitNo;
	}


}
