package jp.co.blueship.tri.fw.sm.beans.matrix;

import java.util.HashMap;
import java.util.Map;

import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.SmMessageId;

/**
 * ビジネス処理を一括して実行する抽象クラスです。 <br>
 * <br>このクラスをsingleton = falseで実行するかどうかを継承先で決定して下さい。
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public abstract class ServiceMatrixSupport<E> implements IDomain<IGeneralServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private Map<String, E> actions = null;

	/**
	 * 一括してビジネス処理を実行するマップを設定します。
	 *
	 * @param map 一括して実行するマップ
	 */
	public final void setActions(Map<String, E> map) {
		actions = map;
	}

	/**
	 * 一括してビジネス処理を実行するマップを取得します。
	 *
	 * @return 一括して実行するマップ
	 */
	public final Map<String, E> getActions() {
		if (null == actions)
			actions = new HashMap<String, E>();

		return actions;
	}

	@Override
	public final IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {
		try {
			//IGeneralServiceBean paramBean = serviceDto.getServiceBean();

			for ( String actionKey : this.getActions().keySet() ) {
				this.actions( serviceDto, actionKey );
			}

		} catch ( BaseBusinessException e ) {
			LogHandler.error( log , e ) ;

			throw e;
		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( SmMessageId.SM005051S , e );

		}

		return serviceDto;

	}

	/**
	 * 一括してビジネス処理を実行するビジネス処理です。
	 *
	 * @param serviceDto コンバート間に流通させるビーン
	 * @param key 対象キー
	 */
	protected abstract void actions(IServiceDto<IGeneralServiceBean> serviceDto, String key);

}
