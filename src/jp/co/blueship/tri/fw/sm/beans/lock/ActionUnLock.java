package jp.co.blueship.tri.fw.sm.beans.lock;

import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.sm.dao.lockmgt.eb.LockMgtCondition;
import jp.co.blueship.tri.fw.sm.support.ISmFinderSupport;
import jp.co.blueship.tri.fw.svc.beans.dto.ILockParamInfo;

/**
 * アクションに関連する業務排他を行います。
 *
 * @author Yukihiro Eguchi
 *
 */
public class ActionUnLock implements IActionUnLock {

	protected ISmFinderSupport support;

	/**
	 * アクセスインタフェースがインスタンス生成時に自動的に設定されます。
	 * @param support アクセスインタフェース
	 */
	public void setSupport(ISmFinderSupport support) {
		this.support = support;
	}

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.business.integration.addon.lock.IActionLock#unLock(jp.co.blueship.trinity.business.integration.common.message.ILockParamInfo)
	 */
	public final void unLock( ILockParamInfo info ) throws BaseBusinessException {
		PreConditions.assertOf(info != null, "Lock parameter is null.");

		LockMgtCondition condition = new LockMgtCondition();
		condition.setProcId( info.getProcId() );
		support.getLockMgtDao().delete( condition.getCondition() );

	}

}
