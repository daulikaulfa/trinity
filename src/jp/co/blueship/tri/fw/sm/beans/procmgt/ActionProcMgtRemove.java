package jp.co.blueship.tri.fw.sm.beans.procmgt;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.sm.dao.procmgt.IProcMgtDao;
import jp.co.blueship.tri.fw.sm.dao.procmgt.eb.IProcMgtEntity;
import jp.co.blueship.tri.fw.sm.dao.procmgt.eb.ProcMgtEntity;
import jp.co.blueship.tri.fw.sm.support.ISmFinderSupport;
import jp.co.blueship.tri.fw.support.IFwFinderSupport;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserProcNoticeEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserProcNoticeEntity;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 * プロセス管理制御のjyyを行います
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public class ActionProcMgtRemove implements IDomain<IGeneralServiceBean> {

	private ISmFinderSupport smSupport = null;
	private IUmFinderSupport umSupport = null;

	/**
	 * アクセスインタフェースがインスタンス生成時に自動的に設定されます。
	 * @param support アクセスインタフェース
	 */
	public void setSupport( IFwFinderSupport support ) {
		smSupport = support.getSmFinderSupport();
		umSupport = support.getUmFinderSupport();
	}

	@Override
	public IServiceDto<IGeneralServiceBean> execute(IServiceDto<IGeneralServiceBean> serviceDto) {
		IGeneralServiceBean paramBean = serviceDto.getServiceBean();

		this.execute( paramBean );

		return serviceDto;
	}

	/**
	 * プロセス管理の除去を行います。
	 *
	 * @param paramBean ドメイン層で共通に利用するBean Interface
	 */
	private void execute( IGeneralServiceBean paramBean ) {
		IProcMgtDao procMgtDao = smSupport.getProcMgtDao();

		String procId = paramBean.getProcId();
		String stsId = paramBean.getProcMgtStatusId().getStatusId();

		IProcMgtEntity entity = new ProcMgtEntity();
		entity.setProcId( procId );
		entity.setProcEndTimestamp( TriDateUtils.getSystemTimestamp() );
		entity.setStsId( stsId );
		entity.setCompStsId( StatusFlg.on );
		entity.setDelStsId( StatusFlg.on );

		procMgtDao.update( entity );

		IUserProcNoticeEntity userProcNoticeEntity = new UserProcNoticeEntity();
		userProcNoticeEntity.setProcId( paramBean.getProcId() );
		userProcNoticeEntity.setIsRead( StatusFlg.off );

		this.umSupport.getUserProcNoticeDao().update( userProcNoticeEntity );
	}

}
