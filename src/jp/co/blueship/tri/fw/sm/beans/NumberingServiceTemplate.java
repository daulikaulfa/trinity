package jp.co.blueship.tri.fw.sm.beans;

import java.io.File;
import java.io.StringWriter;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

import jp.co.blueship.tri.fw.cmn.io.constants.Charset;
import jp.co.blueship.tri.fw.cmn.utils.DesignBusinessRuleUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.VelocityUtils;
import jp.co.blueship.tri.fw.constants.UmDesignEntryKeyByCommon;
import jp.co.blueship.tri.fw.dao.orm.IEntity;
import jp.co.blueship.tri.fw.dao.orm.INumberingDao;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.svc.beans.dto.INumberingCallback;
import jp.co.blueship.tri.fw.svc.beans.dto.NumberingServiceBean;

/**
 * 採番を利用するためのサービステンプレートクラスです。
 *
 */
public class NumberingServiceTemplate {

	private static final String NUMBERING_TEMPLATE = "numbering.vm";
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	/**
	 * システム予約変数
	 */
	public static enum NumberingPropertyName {
		date( "_date" ),
		time( "_time" ),
		nextval( "_nextval" ),
		entity( "_entity" );

		private String value = null;

		private NumberingPropertyName( String value ) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}
	}

	/**
	 * 指定された各種情報に基づいて、採番を行います。<BR>
	 *
	 * @param bean 採番サービスビーン
	 * @param dateFormat システム日付のフォーマット書式
	 * @param timeFormat システム時間のフォーマット書式
	 * @param numberingDao ナンバリングＤＡＯ
	 * @param callback コールバック
	 */
	public void numbering( 	NumberingServiceBean bean,
			String dateFormat,
			String timeFormat,
			INumberingDao numberingDao,
			INumberingCallback callback ) {

		Map<String, Object> data = new HashMap<String, Object>();

		data = this.formatDate( bean.getSystemDate(), dateFormat, data );
		data = this.formatTime( bean.getSystemDate(), timeFormat, data );
		data = this.formatNextval( numberingDao, data );
		data = this.formatEntity( bean.getEntities(), data );

		this.getNumbering( bean, data, callback );

	}

	/**
	 * システム日付を指定された書式に変換します。
	 *
	 * @param date システム日付
	 * @param dateFormat システム日付のフォーマット書式
	 * @param data テンプレートへマージするデータ
	 * @return テンプレートへマージするデータ
	 */
	private final Map<String, Object> formatDate( Timestamp date , String dateFormat, Map<String, Object> data ) {
		if ( TriStringUtils.isEmpty( date ) || TriStringUtils.isEmpty( dateFormat ) ) {
			data.put( 	NumberingServiceTemplate.NumberingPropertyName.date.getValue(),  "" );

			return data;
		}

		SimpleDateFormat format = new SimpleDateFormat( dateFormat );

		data.put( 	NumberingServiceTemplate.NumberingPropertyName.date.getValue(),
				TriDateUtils.convertViewDateFormat( date, format ) );

		return data;
	}

	/**
	 * システム時間を指定された書式に変換します。
	 *
	 * @param date システム日付
	 * @param timeFormat システム時間のフォーマット書式
	 * @param data テンプレートへマージするデータ
	 * @return テンプレートへマージするデータ
	 */
	private final Map<String, Object> formatTime( Timestamp date , String timeFormat, Map<String, Object> data ) {
		if ( TriStringUtils.isEmpty( date ) || TriStringUtils.isEmpty( timeFormat ) ) {
			data.put( 	NumberingServiceTemplate.NumberingPropertyName.time.getValue(), "" );

			return data;
		}

		SimpleDateFormat format = new SimpleDateFormat( timeFormat );

		data.put( 	NumberingServiceTemplate.NumberingPropertyName.time.getValue(),
				TriDateUtils.convertViewDateFormat( date, format ) );

		return data;
	}

	/**
	 * システム連番を指定された書式に変換します。
	 *
	 * @param numberingDao ナンバリングＤＡＯ
	 * @param data テンプレートへマージするデータ
	 * @return テンプレートへマージするデータ
	 */
	private final Map<String, Object> formatNextval( INumberingDao numberingDao, Map<String, Object> data ) {
		if ( TriStringUtils.isEmpty( numberingDao ) ) {
			data.put( 	NumberingServiceTemplate.NumberingPropertyName.nextval.getValue(), "" );

			return data;
		}

		data.put( 	NumberingServiceTemplate.NumberingPropertyName.nextval.getValue(),
				numberingDao.nextval() );

		return data;
	}

	/**
	 * エンティティを指定された書式に変換します。
	 *
	 * @param entityList エンティティのリスト
	 * @param data テンプレートへマージするデータ
	 * @return テンプレートへマージするデータ
	 */
	private final Map<String, Object> formatEntity( List<IEntity> entityList, Map<String, Object> data ) {
		data.put( 	NumberingServiceTemplate.NumberingPropertyName.entity.getValue(),
				entityList );

		return data;
	}

	/**
	 * 採番を取得します。
	 *
	 * @param bean 採番サービス情報
	 * @param data テンプレートへマージするデータ
	 * @param callback コールバック
	 * @return 採番を戻します。
	 */
	private final void getNumbering( NumberingServiceBean bean,
			Map<String, Object> data,
			INumberingCallback callback ) {

		if ( null != callback )
			data = callback.getNumbering(bean, data);

		File resource = DesignBusinessRuleUtils.getTemplatePath();
		//String relativePath = bean.getFlowAction() + "." + NUMBERING_TEMPLATE;

		String templatePath = sheet.getValue( UmDesignEntryKeyByCommon.numberingTemplatesRelativePath );
		if ( !StringUtils.endsWith( templatePath, "/" ) ) {
			templatePath = templatePath + "/";
		}

		String relativePath = templatePath + bean.getFlowAction() + "." + NUMBERING_TEMPLATE;
		String path = new File( resource, relativePath ).getPath();

		Properties p = new Properties();

		VelocityUtils.addProperties(Velocity.RESOURCE_LOADER, "numbering", p);
		p.put("numbering.resource.loader.class", "org.apache.velocity.runtime.resource.loader.FileResourceLoader");
		VelocityUtils.addProperties("numbering.resource.loader.path", resource.getPath(), p);
		p.put("numbering.resource.loader.cache", "true");
		p.put("numbering.resource.loader.modificationCheckInterval", "2");
		p.put(Velocity.INPUT_ENCODING, Charset.UTF_8.value());
		try {
			Velocity.init(p);
		} catch (Exception e) {
			throw new TriSystemException(SmMessageId.SM005112S, e);
		}

		VelocityContext context = new VelocityContext();

		for ( String key : data.keySet() ) {
			context.put( key, data.get( key ) );
		}

		Template template;
		try {
			template = Velocity.getTemplate( relativePath );
		} catch (Exception e) {
			throw new TriSystemException(SmMessageId.SM004102F , e , path );
		}

		StringWriter w = new StringWriter();
		try {
			template.merge( context, w );
		} catch (Exception e) {
			throw new TriSystemException(SmMessageId.SM004103F, e);
		}

		bean.setNumberingVal( w.toString() );
	}

}
