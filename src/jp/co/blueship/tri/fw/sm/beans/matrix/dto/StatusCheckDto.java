package jp.co.blueship.tri.fw.sm.beans.matrix.dto;

import jp.co.blueship.tri.fw.dao.orm.FinderSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;

/**
 * Status matrix check DTO.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public class StatusCheckDto implements IStatusCheckDto {
	/**
	 * ドメイン層のBean Interface
	 */
	private IGeneralServiceBean serviceBean;
	/**
	 * データアクセスを支援するファインダー
	 */
	private FinderSupport finder;
	/**
	 * ビジネス処理を一括して実行するクラス
	 */
	private ActionStatusMatrixList actionList;

	/**
	 * ロット・ベースラインID's
	 */
	private String[] lotBls = null;
	/**
	 * HEAD・ベースラインID's
	 */
	private String[] headBls = null;
	/**
	 * ロットID's
	 */
	private String[] lotIds = null;
	/**
	 * 変更情報ID's
	 */
	private String[] pjtIds = null;
	/**
	 * 資産申請ID's
	 */
	private String[] areqIds = null;
	/**
	 * ビルドパッケージID's
	 */
	private String[] bpIds = null;
	/**
	 * リリース申請ID's
	 */
	private String[] raIds = null;
	/**
	 * リリースパッケージID's
	 */
	private String[] rpIds = null;
	/**
	 * レポートID's
	 */
	private String[] repIds = null;


	@Override
	public IGeneralServiceBean getServiceBean() {
	    return serviceBean;
	}
	/**
	 * ドメイン層のBean Interfaceを設定します。
	 * @param serviceBean ドメイン層のBean Interface
	 */
	public StatusCheckDto setServiceBean(IGeneralServiceBean serviceBean) {
	    this.serviceBean = serviceBean;
	    return this;
	}
	@Override
	public FinderSupport getFinder() {
	    return finder;
	}
	/**
	 * データアクセスを支援するファインダーを設定します。
	 * @param finder データアクセスを支援するファインダー
	 */
	public StatusCheckDto setFinder(FinderSupport finder) {
	    this.finder = finder;
	    return this;
	}
	@Override
	public ActionStatusMatrixList getActionList() {
	    return actionList;
	}
	/**
	 * ビジネス処理を一括して実行するクラスを設定します。
	 * @param actionList ビジネス処理を一括して実行するクラス
	 */
	public StatusCheckDto setActionList(ActionStatusMatrixList actionList) {
	    this.actionList = actionList;
	    return this;
	}
	@Override
	public String[] getLotBls() {
	    return lotBls;
	}
	/**
	 * ロット・ベースラインID'sを設定します。
	 * @param lotBls ロット・ベースラインID's
	 */
	public StatusCheckDto setLotBls(String... lotBls) {
	    this.lotBls = lotBls;
	    return this;
	}
	@Override
	public String[] getHeadBls() {
	    return headBls;
	}
	/**
	 * HEAD・ベースラインID'sを設定します。
	 * @param lotBls HEAD・ベースラインID's
	 */
	public StatusCheckDto setHeadBls(String... headBls) {
	    this.headBls = headBls;
	    return this;
	}
	@Override
	public String[] getLotIds() {
	    return lotIds;
	}
	/**
	 * ロットID'sを設定します。
	 * @param lotIds ロットID's
	 */
	public StatusCheckDto setLotIds(String... lotIds) {
	    this.lotIds = lotIds;
	    return this;
	}
	@Override
	public String[] getPjtIds() {
	    return pjtIds;
	}
	/**
	 * 変更情報ID'sを設定します。
	 * @param pjtIds 変更情報ID's
	 */
	public StatusCheckDto setPjtIds(String... pjtIds) {
	    this.pjtIds = pjtIds;
	    return this;
	}
	@Override
	public String[] getAreqIds() {
	    return areqIds;
	}
	/**
	 * 資産申請ID'sを設定します。
	 * @param areqIds 資産申請ID's
	 */
	public StatusCheckDto setAreqIds(String... areqIds) {
	    this.areqIds = areqIds;
	    return this;
	}
	@Override
	public String[] getBpIds() {
	    return bpIds;
	}
	/**
	 * ビルドパッケージID'sを設定します。
	 * @param bpIds ビルドパッケージID's
	 */
	public StatusCheckDto setBpIds(String... bpIds) {
	    this.bpIds = bpIds;
	    return this;
	}
	@Override
	public String[] getRaIds() {
	    return raIds;
	}
	/**
	 * リリース申請ID'sを設定します。
	 * @param raIds リリース申請ID's
	 */
	public StatusCheckDto setRaIds(String... raIds) {
	    this.raIds = raIds;
	    return this;
	}
	@Override
	public String[] getRpIds() {
	    return rpIds;
	}
	/**
	 * リリースパッケージID'sを設定します。
	 * @param rpIds リリースパッケージID's
	 */
	public StatusCheckDto setRpIds(String... rpIds) {
	    this.rpIds = rpIds;
	    return this;
	}
	@Override
	public String[] getRepIds() {
	    return repIds;
	}
	/**
	 * レポートID'sを設定します。
	 * @param repIds レポートID's
	 */
	public StatusCheckDto setRepIds(String... repIds) {
	    this.repIds = repIds;
	    return this;
	}

}
