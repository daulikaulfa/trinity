package jp.co.blueship.tri.fw.sm.beans.matrix.dto;



/**
 * （関連する全ビルドパッケージ）アクション間のステータスマトリクスメッセージ
 * <br>
 *
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
/**
 * @author 江口
 *
 */

public class StatusCheckParamInfoByRelUnitAll extends StatusCheckParamInfo {

	private static final long serialVersionUID = 6369924923111085068L;

}
