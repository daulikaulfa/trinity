package jp.co.blueship.tri.fw.sm.beans.matrix.dto;



/**
 * （関連するリリース申請）アクション間のステータスマトリクスメッセージ
 * <br>
 *
 * All Rights Reserved, Copyright(c) Blueship  2009-2012
 */

public class StatusCheckParamInfoByRelApply extends StatusCheckParamInfo {

	/**
	 * 修正したらインクリメントすること
	 */
	private static final long serialVersionUID = 1L;

	private String[] raIds = null;

	public String[] getRaIds() {
		return raIds;
	}

	public void setRaIds(String[] raIds) {
		this.raIds = raIds;
	}

}
