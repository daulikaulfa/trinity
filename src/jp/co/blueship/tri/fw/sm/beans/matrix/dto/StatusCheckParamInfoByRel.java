package jp.co.blueship.tri.fw.sm.beans.matrix.dto;



/**
 * （関連するリリース）アクション間のステータスマトリクスメッセージ
 * <br>
 *
 * All Rights Reserved, Copyright(c) Blueship  2009
 */

public class StatusCheckParamInfoByRel extends StatusCheckParamInfo {

	private static final long serialVersionUID = 1972057766435562316L;
	private String[] rpIds = null;

	public String[] getRpIds() {
		return rpIds;
	}

	public void setRpIds(String[] rpIds) {
		this.rpIds = rpIds;
	}


}
