package jp.co.blueship.tri.fw.sm.beans.matrix;

import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;

/**
 * アクション間のステータスマトリクスチェックを実行するクラスです。
 * <br>このクラスは、必ずsingleton = falseで実行します。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public class ActionStatusMatrixList extends ServiceMatrixSupport<List<IDomain<IGeneralServiceBean>>> {

	@Override
	protected void actions(IServiceDto<IGeneralServiceBean> serviceDto, String key) {
		for (IDomain<IGeneralServiceBean> action : this.getActions().get( key )) {
			action.execute(serviceDto);
		}
	}

}
