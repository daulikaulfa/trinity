package jp.co.blueship.tri.fw.sm.beans.matrix;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotCondition;
import jp.co.blueship.tri.fw.cmn.utils.ExtractFinderAddonUtils;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.AmDesignBeanId;
import jp.co.blueship.tri.fw.constants.UmDesignBeanId;
import jp.co.blueship.tri.fw.constants.status.AmLotStatusIdForExecData;
import jp.co.blueship.tri.fw.dao.orm.FinderSupport;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.msg.MessageParameter;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckParamInfoByLot;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.ExtractStatusCheckUtils;

/**
 * （関連するロット）アクション間のステータスマトリクスを検査します。
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author yukihiro eguchi
 *
 */
public class StatusCheckByLot extends GenericStatusCheck {
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {
		this.outputBLStartLog( this.getClass().getName() );

		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]>	messageArgsList	= new ArrayList<String[]>();

		List<Object> paramList = serviceDto.getParamList();

		try {
			FinderSupport finder = ExtractFinderAddonUtils.extractFinderSupport( paramList );
			StatusCheckParamInfoByLot param = ExtractStatusCheckUtils.extractStatusCheckParamInfoByLot( paramList );
			param.setValid(false);

			if ( TriStringUtils.isEmpty( param.getLotIds() ) )
				return serviceDto;

			LotCondition condition = new LotCondition();
			condition.setDelStsId( null );
			condition.setLotIds( param.getLotIds() );

			IEntityLimit<ILotEntity> limit = finder.getLotDao().find(condition.getCondition(), new SortBuilder(), 1, 0);

			IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
			GenericServiceBean serviceBean = (GenericServiceBean) serviceDto.getServiceBean();
			for ( ILotEntity entity : limit.getEntities() ) {
				if ( StatusFlg.on ==  entity.getDelStsId() ) {
					messageList.add		( SmMessageId.SM001014E );
					messageArgsList.add	( new String[] { ca.getValue( MessageParameter.TARGET_LOT.getKey(), serviceBean.getLanguage() ), entity.getLotId() } );
					continue;
				}
				
			/*	for ( String status : this.getWithoutPermissionsByBaseStatus() ) {
					String curStatusId = entity.getStsId() ;
					if ( status.equals( curStatusId ) ) {
						String curStatusLabel = sheet.getValue( AmDesignBeanId.statusId, curStatusId ) ;
						messageList.add		( SmMessageId.SM001015E );
						messageArgsList.add	( new String[] { DesignUtils.getMessageParameter( MessageParameter.TARGET_LOT.getKey() ),
																entity.getLotId(),
																DesignUtils.getMessageParameter( MessageParameter.BASE.getKey() ) ,
																sheet.getValue( UmDesignBeanId.actionId, param.getFlowAction()) ,
																curStatusLabel });
					}
				}*/
				
				for ( String status : this.getWithoutPermissionsByStatus() ) {
					String curStatusId = entity.getProcStsId() ;
					if ( status.equals( curStatusId ) ) {
						String curStatusLabel = "";
						String flowAction = sheet.getValue( UmDesignBeanId.actionId, param.getFlowAction());;
						if( serviceBean.isStatusMatrixV3() ) {
							curStatusLabel = sheet.getValue( AmDesignBeanId.statusId, curStatusId );
						} else {
							curStatusLabel = ca.getValue(AmLotStatusIdForExecData.value(curStatusId).getMessageKey(), serviceBean.getLanguage());
						}
						messageList.add		( SmMessageId.SM001015E );
						messageArgsList.add	( new String[] { ca.getValue( MessageParameter.TARGET_LOT.getKey(), serviceBean.getLanguage() ),
																entity.getLotId(),
																"" ,
																flowAction ,
																curStatusLabel } );
					}
				}

			}

			if ( 0 != messageList.size() ) {
				if ( ! param.isValidationMode() )
					throw new ContinuableBusinessException( messageList, messageArgsList );
			} else {
				param.setValid(true);
			}

		} finally {
			this.outputBLEndLog( this.getClass().getName() );
		}

		return serviceDto;
	}

}
