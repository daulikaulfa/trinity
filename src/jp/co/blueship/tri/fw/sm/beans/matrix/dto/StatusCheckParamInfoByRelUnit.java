package jp.co.blueship.tri.fw.sm.beans.matrix.dto;



/**
 * （関連するビルドパッケージ）アクション間のステータスマトリクスメッセージ
 * <br>
 *
 * All Rights Reserved, Copyright(c) Blueship  2009
 */

public class StatusCheckParamInfoByRelUnit extends StatusCheckParamInfo {

	private static final long serialVersionUID = -8450526774763223834L;
	private String[] bpIds = null;

	public String[] getBpIds() {
		return bpIds;
	}

	public void setBpIds(String[] bpIds) {
		this.bpIds = bpIds;
	}


}
