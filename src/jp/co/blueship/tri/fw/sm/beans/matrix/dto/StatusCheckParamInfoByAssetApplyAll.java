package jp.co.blueship.tri.fw.sm.beans.matrix.dto;



/**
 * （関連する申請情報）アクション間のステータスマトリクスメッセージ
 * <br>
 *
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
/**
 * @author 江口
 *
 */
public class StatusCheckParamInfoByAssetApplyAll extends StatusCheckParamInfo {

	private static final long serialVersionUID = 8774098471301605455L;

}
