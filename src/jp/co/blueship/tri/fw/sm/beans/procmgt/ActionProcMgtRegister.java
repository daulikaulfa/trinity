package jp.co.blueship.tri.fw.sm.beans.procmgt;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.dao.lot.eb.ILotGrpLnkEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotGrpLnkCondition;
import jp.co.blueship.tri.am.support.IAmFinderSupport;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.status.SmProcMgtStatusId;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.sm.dao.procmgt.IProcMgtDao;
import jp.co.blueship.tri.fw.sm.dao.procmgt.eb.IProcMgtEntity;
import jp.co.blueship.tri.fw.sm.dao.procmgt.eb.ProcMgtEntity;
import jp.co.blueship.tri.fw.sm.support.ISmFinderSupport;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserProcNoticeEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserProcNoticeEntity;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 * プロセス管理制御の登録を行います。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public class ActionProcMgtRegister implements IDomain<IGeneralServiceBean> {

	private ISmFinderSupport smSupport = null;
	private IUmFinderSupport umSupport = null;
	private IAmFinderSupport amSupport = null;

	/**
	 * アクセスインタフェースがインスタンス生成時に自動的に設定されます。
	 * @param support アクセスインタフェース
	 */
	public void setAmSupport( IAmFinderSupport amSupport ) {
		this.amSupport = amSupport;
		smSupport = amSupport.getSmFinderSupport();
		umSupport = amSupport.getUmFinderSupport();
	}


	@Override
	public IServiceDto<IGeneralServiceBean> execute(IServiceDto<IGeneralServiceBean> serviceDto) {
		IGeneralServiceBean paramBean = serviceDto.getServiceBean();

		this.execute( paramBean );

		return serviceDto;
	}

	/**
	 * プロセス管理の登録を行います。
	 *
	 * @param paramBean ドメイン層で共通に利用するBean Interface
	 */
	private void execute( IGeneralServiceBean paramBean ) {
//		INumberingDao numberingDao = support.getProcMgtNumberingDao();
		IProcMgtDao procMgtDao = smSupport.getProcMgtDao();

		paramBean.setProcMgtStatusId( SmProcMgtStatusId.Error );

		IProcMgtEntity entity = new ProcMgtEntity();
		entity.setProcId( paramBean.getProcId() );
		entity.setSvcId( paramBean.getFlowAction() );
		entity.setLotId( TriStringUtils.defaultIfEmpty( paramBean.getLockLotNo(), "", paramBean.getLockLotNo() ) );
		entity.setUserId( paramBean.getUserId() );
		entity.setUserNm( paramBean.getUserName() );
		entity.setProcStTimestamp( TriDateUtils.getSystemTimestamp() );
		entity.setProcEndTimestamp( null );
		entity.setStsId( SmProcMgtStatusId.Processing.getStatusId() );
		entity.setCompStsId( StatusFlg.off );
		entity.setDelStsId( StatusFlg.off );

		procMgtDao.insert( entity );

		if(NoticeService.value(paramBean.getFlowAction()).equals(NoticeService.none))
			return;

		//insert data into um_user_proc_notice
		LotGrpLnkCondition lotGrpLnkCondition = new LotGrpLnkCondition();
		lotGrpLnkCondition.setLotId( TriStringUtils.defaultIfEmpty( paramBean.getLockLotNo(), "", paramBean.getLockLotNo() ));

		List<ILotGrpLnkEntity> lotGrpLnkEntityList =  this.amSupport.getLotGrpLnkDao().find(lotGrpLnkCondition.getCondition());

		List<String> userIdList = new ArrayList<String>();

		for(ILotGrpLnkEntity lotGrpLnkEntity : lotGrpLnkEntityList){
			String grpId = lotGrpLnkEntity.getGrpId();

			List<IGrpUserLnkEntity> grpUserLnkEntityList = this.umSupport.findGrpUserLnkByGrpId(grpId);

			for(IGrpUserLnkEntity grpUserLnkEntity : grpUserLnkEntityList){

				if( userIdList.contains( grpUserLnkEntity.getUserId() ) )
					continue;
				else
					userIdList.add( grpUserLnkEntity.getUserId() );

				IUserProcNoticeEntity userProcNoticeEntity = new UserProcNoticeEntity();
				userProcNoticeEntity.setProcId( paramBean.getProcId() );
				userProcNoticeEntity.setUserId( grpUserLnkEntity.getUserId() );

				this.umSupport.getUserProcNoticeDao().insert( userProcNoticeEntity );
			}
		}
	}

	/**
	 * ユーザーに通知するサービスの列挙型です<br>
	 * この列挙型に含まれないサービスはum_user_proc_noticeにデータが登録されません<br>
	 * <br>
	 * Don't insert data into um_user_proc_notice if service is not included this enum.<br>
	 */
	private enum NoticeService{
		none ("",""),
		lotCreate			( ServiceId.AmLotCreationService.value(),
							  ServiceId.AmLotCreationService.valueOfV3()),

		createBuildPackage	( ServiceId.BmBuildPackageCreationService.value() ,
							  ServiceId.BmBuildPackageCreationService.valueOfV3()),

		closeBuildPackage	( ServiceId.BmBuildPackageCloseService.value() ,
							  ServiceId.BmBuildPackageCloseService.valueOfV3()),

		createReleasePackage( ServiceId.RmReleasePackageCreationService.value() ,
							  ServiceId.RmReleasePackageCreationService.valueOfV3()),

		conflictCheck		( ServiceId.AmConflictCheckService.value(),
							  ServiceId.AmConflictCheckService.valueOfV3()),

		merge				( ServiceId.AmMergeService.value(),
							  ServiceId.AmMergeService.valueOfV3()),
		;

		private String value = null;
		private String valueOfV3 = null;

		private NoticeService(String serviceId ,  String v3ServiceId){
			this.value = serviceId;
			this.valueOfV3 = v3ServiceId;
		}

		private String value(){
			return this.value;
		}
		private String valueOfV3(){
			return this.valueOfV3;
		}

		private static NoticeService value( String serviceId){

			if( TriStringUtils.isEmpty( serviceId ) ) return null;

			for ( NoticeService value : values() ) {
				if ( value.value().equals( serviceId ) ||
					 ((value.valueOfV3() == null)? false : value.valueOfV3().equals( serviceId )))
					return value;
			}

			return none;
		}
	}

}
