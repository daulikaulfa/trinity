package jp.co.blueship.tri.fw.sm.beans.matrix.dto;



/**
 * （関連する変更管理）アクション間のステータスマトリクスメッセージ
 * <br>
 *
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class StatusCheckParamInfoByPjt extends StatusCheckParamInfo {

	private static final long serialVersionUID = 6684529412973322595L;
	private String[] pjtIds = null;

	public String[] getPjtIds() {
		return pjtIds;
	}

	public void setPjtIds(String[] pjtIds) {
		this.pjtIds = pjtIds;
	}


}
