package jp.co.blueship.tri.fw.sm.support;

import jp.co.blueship.tri.fw.dao.orm.constants.IStatusId;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.security.constants.PasswordCategory;
import jp.co.blueship.tri.fw.sm.dao.funcsvc.eb.IFuncSvcEntity;
import jp.co.blueship.tri.fw.sm.dao.passmgt.eb.IPassMgtEntity;
import jp.co.blueship.tri.fw.sm.dao.plmgt.eb.IPlMgtEntity;

import java.util.List;

/**
 * SM関連情報を検索するためのサービス機能を提供するインタフェースです。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public interface ISmFinderSupport extends ISmDaoFinder {

	/**
	 * プロセスIDを採番します。
	 *
	 * @return 採番したユニークID
	 */
	public String nextvalByProcId();

	/**
	 * パスワード管理テーブルから、対応する業務のパスワードを取得する
	 * <br>
	 *
	 * @param passCategory パスワード分類コード
	 * @param hostNm ホスト名
	 * @param userId ユーザ名
	 * @return 取得したパスワード管理テーブルを戻します。１件も取得できない場合、システム例外がthrowされます。
	 */
	public IPassMgtEntity findPasswordEntity( PasswordCategory passCategory , String hostNm , String userId ) throws TriSystemException;

	/**
	 * 実行中データのステータスをクリーニングします。
	 * <br>実行中にエラーになった場合、その後に再実行した処理の該当レコードを物理削除に
	 * することで、処理中ステータスに矛盾が発生しないようにします。
	 *
	 * @param attr table attribute
	 * @param dataIds データID
	 */
	public void cleaningExecDataSts( ITableAttribute attr, String... dataIds );

	/**
	 * 実行中データのステータスをクリーニングします。
	 * <br>実行中にエラーになった場合、その後に再実行した処理の該当レコードを削除済みに
	 * することで、処理中ステータスに矛盾が発生しないようにします。
	 *
	 * @param attr table attribute
	 * @param isPhysicalDelete 物理的にデータを削除する場合true。論理削除の場合はfalse
	 * @param dataIds データID
	 */
	public void cleaningExecDataSts( ITableAttribute attr, boolean isPhysicalDelete, String... dataIds );

	/**
	 * 実行中データのステータスをクリーニングします。
	 * <br>処理が正常に終了した場合に、このメソッドを利用してプロセスIDに紐づくレコードを物理削除します。
	 *
	 * @param procId プロセスID
	 */
	public void cleaningExecDataSts( String procId );

	/**
	 * 実行中データのステータスをクリーニングします。
	 * <br>処理が正常に終了した場合に、このメソッドを利用してプロセスIDに紐づくレコードを物理削除します。
	 *
	 * @param procId プロセスID
	 * @param attr table attribute
	 * @param dataIds データID
	 */
	public void cleaningExecDataSts( String procId, ITableAttribute attr, String... dataIds );

	/**
	 * 実行中データのステータス登録します。
	 *
	 * @param procId プロセスID
	 * @param attr table attribute
	 * @param statusId ステータスID
	 * @param dataIds データID
	 */
	public void registerExecDataSts(
			String procId, ITableAttribute attr, IStatusId statusId, String... dataIds );

	/**
	 * 実行中データのステータス登録します。
	 *
	 * @param procId プロセスID
	 * @param attr table attribute
	 * @param statusId ステータスID
	 * @param dataIds データID
	 */
	public void registerExecDataSts(
			String procId, ITableAttribute attr, String statusId, String... dataIds );

	/**
	 * 実行中データのステータス更新します。
	 *
	 * @param procId プロセスID
	 * @param attr table attribute
	 * @param statusId ステータスID
	 * @param dataIds データID
	 */
	public void updateExecDataSts(
			String procId, ITableAttribute attr, IStatusId statusId, String... dataIds );

	/**
	 * 実行中データのステータス更新します。
	 *
	 * @param procId プロセスID
	 * @param attr table attribute
	 * @param statusId ステータスID
	 * @param dataIds データID
	 */
	public void updateExecDataSts(
			String procId, ITableAttribute attr, String statusId, String... dataIds );
	/**
	 * サービスIDから機能サービスエンティティを取得します。
	 * @param svcId サービスID
	 * @return 機能サービスエンティティ
	 */
	public IFuncSvcEntity findFuncSvcEntityByPrimaryKey( String svcId );

	/**
	 * 製品ライセンス管理エンティティを取得します。
	 *
	 * @return 取得した製品ライセンス管理エンティティ
	 * @param productId
	 */
	public IPlMgtEntity findPlMgtEntity(String productId);

	public List<IPlMgtEntity> findPlMgtEntities();

	/**
	 * 製品ライセンス管理エンティティを取得します。
	 *
	 * @param productId 製品ID
	 * @param computerNm コンピュータ名
	 * @return 取得した製品ライセンス管理エンティティ
	 */
	public IPlMgtEntity findPlMgtEntityByPrimaryKey(String productId, String computerNm );

}
