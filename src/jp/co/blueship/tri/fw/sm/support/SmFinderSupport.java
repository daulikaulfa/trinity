package jp.co.blueship.tri.fw.sm.support;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.IStatusId;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.security.constants.PasswordCategory;
import jp.co.blueship.tri.fw.sm.dao.constants.SmTables;
import jp.co.blueship.tri.fw.sm.dao.execdatasts.eb.ExecDataStsCondition;
import jp.co.blueship.tri.fw.sm.dao.execdatasts.eb.ExecDataStsEntity;
import jp.co.blueship.tri.fw.sm.dao.execdatasts.eb.IExecDataStsEntity;
import jp.co.blueship.tri.fw.sm.dao.funcsvc.eb.FuncSvcCondition;
import jp.co.blueship.tri.fw.sm.dao.funcsvc.eb.IFuncSvcEntity;
import jp.co.blueship.tri.fw.sm.dao.passmgt.eb.IPassMgtEntity;
import jp.co.blueship.tri.fw.sm.dao.passmgt.eb.PassMgtCondition;
import jp.co.blueship.tri.fw.sm.dao.plmgt.constants.PlMgtItems;
import jp.co.blueship.tri.fw.sm.dao.plmgt.eb.IPlMgtEntity;
import jp.co.blueship.tri.fw.sm.dao.plmgt.eb.PlMgtCondition;


/**
 * SM関連情報を検索するためのサービス機能を提供するクラスです。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Sam
 */
public class SmFinderSupport extends SmDaoFinder implements ISmFinderSupport {

	@Override
	public final String nextvalByProcId() {
		return this.getProcMgtNumberingDao().nextval();
	}

	@Override
	public final IPassMgtEntity findPasswordEntity( PasswordCategory passCategory , String hostNm , String userId ) throws TriSystemException {

		PassMgtCondition condition = new PassMgtCondition();
		condition.setPassCtgCd( passCategory.getValue() );
		condition.setHostNm( hostNm );
		condition.setUserId(userId);

		IPassMgtEntity entity = this.getPassMgtDao().findByPrimaryKey( condition.getCondition() );

		if ( null == entity ) {
			throw new TriSystemException(SmMessageId.SM004214F, SmTables.SM_PASS_MGT.name(), passCategory.getValue(), hostNm , userId );
		}

		return entity;
	}

	@Override
	public void cleaningExecDataSts( ITableAttribute attr, String... dataIds ) {
		this.cleaningExecDataSts(attr, true, dataIds);
	}

	@Override
	public void cleaningExecDataSts( ITableAttribute attr, boolean isPhysicalDelete, String... dataIds ) {
		if ( TriStringUtils.isEmpty(dataIds) )
			return;

		ExecDataStsCondition condition = new ExecDataStsCondition();
		condition.setDataCtgCd( attr.name() );

		if ( 1 == dataIds.length ) {
			condition.setDataId( dataIds[0] );
		} else {
			condition.setDataIds( dataIds );
		}

		if ( isPhysicalDelete ) {
			this.getExecDataStsDao().delete(condition.getCondition());
		} else {
			IExecDataStsEntity entity = new ExecDataStsEntity();
			entity.setDelStsId( StatusFlg.on );

			this.getExecDataStsDao().update(condition.getCondition(), entity);
		}
	}

	@Override
	public void cleaningExecDataSts( String procId ) {
		ExecDataStsCondition condition = new ExecDataStsCondition();
		condition.setProcId( procId );

		this.getExecDataStsDao().delete( condition.getCondition() );
	}

	@Override
	public void cleaningExecDataSts( String procId, ITableAttribute attr, String... dataIds ) {

		ExecDataStsCondition condition = new ExecDataStsCondition();
		condition.setProcId( procId );
		condition.setDataCtgCd( attr.name() );

		if ( TriStringUtils.isNotEmpty(dataIds) ) {
			if ( 1 == dataIds.length ) {
				condition.setDataId( dataIds[0] );
			} else {
				condition.setDataIds( dataIds );
			}
		}

		this.getExecDataStsDao().delete( condition.getCondition() );
	}

	@Override
	public void registerExecDataSts( String procId, ITableAttribute attr, IStatusId statusId, String... dataIds ) {
		this.registerExecDataSts(procId, attr, statusId.getStatusId(), dataIds);
	}

	@Override
	public void registerExecDataSts( String procId, ITableAttribute attr, String statusId, String... dataIds ) {
		if ( TriStringUtils.isEmpty(dataIds) )
			return;

		List<IExecDataStsEntity> entities = new ArrayList<IExecDataStsEntity>();

		for ( String dataId: dataIds ) {
			IExecDataStsEntity entity = new ExecDataStsEntity();
			entity.setProcId( procId );
			entity.setDataCtgCd( attr.name() );
			entity.setDataId( dataId );
			entity.setStsId( statusId );

			entities.add( entity );
		}

		this.getExecDataStsDao().insert( entities );
	}

	@Override
	public void updateExecDataSts( String procId, ITableAttribute attr, IStatusId statusId, String... dataIds ) {

		this.updateExecDataSts( procId, attr, statusId.getStatusId(), dataIds );
	}

	@Override
	public void updateExecDataSts( String procId, ITableAttribute attr, String statusId, String... dataIds ) {

		ExecDataStsCondition condition = new ExecDataStsCondition();
		condition.setProcId( procId );
		condition.setDataCtgCd( attr.name() );

		if ( TriStringUtils.isNotEmpty(dataIds) ) {
			if ( 1 == dataIds.length ) {
				condition.setDataId( dataIds[0] );
			} else {
				condition.setDataIds( dataIds );
			}
		}

		IExecDataStsEntity entity = new ExecDataStsEntity();
		entity.setProcId( procId );
		entity.setStsId( statusId );

		this.getExecDataStsDao().update( condition.getCondition(), entity );
	}

	@Override
	public IFuncSvcEntity findFuncSvcEntityByPrimaryKey( String svcId ) {

		PreConditions.assertOf(svcId != null, "servceId is not specified.");

		FuncSvcCondition condition = new FuncSvcCondition();
		if ( TriStringUtils.isNotEmpty( svcId ) ) {
			condition.setSvcId( svcId );
		}
		return this.getFuncSvcDao().findByPrimaryKey( condition.getCondition() );
	}

	@Override
	public IPlMgtEntity findPlMgtEntity(String productId) {
		IPlMgtEntity entity = null;
		try {
			String computerNm = InetAddress.getLocalHost().getHostName();

			PlMgtCondition condition = new PlMgtCondition();
			condition.setProductId(productId);
			condition.setCpNm(computerNm);

			entity = getPlMgtDao().findByPrimaryKey(condition.getCondition());
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		return entity;
	}

	@Override
	public List<IPlMgtEntity> findPlMgtEntities() {
		List<IPlMgtEntity> entities = Collections.emptyList();
		try {
			String computerNm = InetAddress.getLocalHost().getHostName();
			PlMgtCondition condition = new PlMgtCondition();
			condition.setCpNm(computerNm);
			ISqlSort sort = new SortBuilder().setElement(PlMgtItems.regTimestamp, TriSortOrder.Asc, 1);
			entities = getPlMgtDao().find(condition.getCondition(), sort);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		return entities;
	}

	@Override
	public IPlMgtEntity findPlMgtEntityByPrimaryKey(String productId, String computerNm ) {

		PreConditions.assertOf(productId != null, "productId is not specified.");
		PreConditions.assertOf(computerNm != null, "ComputerName is not specified.");

		PlMgtCondition condition = new PlMgtCondition();
		condition.setProductId(productId);
		condition.setCpNm(computerNm);

		IPlMgtEntity entity = getPlMgtDao().findByPrimaryKey(condition.getCondition());

		return entity;

	}

}
