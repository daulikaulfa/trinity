package jp.co.blueship.tri.fw.sm.support;

import jp.co.blueship.tri.fw.dao.orm.FinderSupport;
import jp.co.blueship.tri.fw.sm.dao.execdatasts.IExecDataStsDao;
import jp.co.blueship.tri.fw.sm.dao.funcsvc.IFuncSvcDao;
import jp.co.blueship.tri.fw.sm.dao.lockmgt.ILockMgtDao;
import jp.co.blueship.tri.fw.sm.dao.login.IUserLoginDao;
import jp.co.blueship.tri.fw.sm.dao.passmgt.IPassMgtDao;
import jp.co.blueship.tri.fw.sm.dao.plmgt.IPlMgtDao;
import jp.co.blueship.tri.fw.sm.dao.procmgt.IProcDetailsDao;
import jp.co.blueship.tri.fw.sm.dao.procmgt.IProcMgtDao;
import jp.co.blueship.tri.fw.sm.dao.procmgt.ProcMgtNumberingDao;
import jp.co.blueship.tri.fw.sm.dao.sysdef.ISysDefDao;

/**
 * SM管理のDAOのGetterを提供するクラスです。
 * <br>ここでは、DAOのGetter Method以外の機能は提供されません。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 * 
 * @version V4.00.00
 * @author le.thixuan
 */
public abstract class SmDaoFinder extends FinderSupport implements ISmDaoFinder {
	private ProcMgtNumberingDao procMgtNumberingDao = null;

	private IPassMgtDao passMgtDao = null;
	private IPlMgtDao plMgtDao;
	private ILockMgtDao lockMgtDao = null;
	private IProcMgtDao procMgtDao = null;
	private IProcDetailsDao procDetailsDao = null;
	private IExecDataStsDao execDataStsDao = null;
	private IFuncSvcDao funcSvcDao;
	private ISysDefDao sysDefDao = null;
	private IUserLoginDao userLoginDao = null;

	public ProcMgtNumberingDao getProcMgtNumberingDao() {
	    return procMgtNumberingDao;
	}
	public void setProcMgtNumberingDao(ProcMgtNumberingDao procMgtNumberingDao) {
	    this.procMgtNumberingDao = procMgtNumberingDao;
	}

	public IPassMgtDao getPassMgtDao() {
	    return passMgtDao;
	}

	public void setPassMgtDao(IPassMgtDao passMgtDao) {
	    this.passMgtDao = passMgtDao;
	}

	public IPlMgtDao getPlMgtDao() {
		return this.plMgtDao;
	}

	public void setPlMgtDao(IPlMgtDao plMgtDao) {
		this.plMgtDao = plMgtDao;
	}

	@Override
	public ILockMgtDao getLockMgtDao() {
	    return lockMgtDao;
	}
	public void setLockMgtDao(ILockMgtDao lockMgtDao) {
	    this.lockMgtDao = lockMgtDao;
	}
	@Override
	public IProcMgtDao getProcMgtDao() {
	    return procMgtDao;
	}
	public void setProcMgtDao(IProcMgtDao procMgtDao) {
	    this.procMgtDao = procMgtDao;
	}
	@Override
	public IProcDetailsDao getProcDetailsDao() {
	    return procDetailsDao;
	}
	public void setProcDetailsDao(IProcDetailsDao procDetailsDao) {
	    this.procDetailsDao = procDetailsDao;
	}
	@Override
	public IExecDataStsDao getExecDataStsDao() {
	    return execDataStsDao;
	}

	public void setExecDataStsDao(IExecDataStsDao execDataStsDao) {
	    this.execDataStsDao = execDataStsDao;
	}

	@Override
	public IFuncSvcDao getFuncSvcDao() {
		return this.funcSvcDao;
	}

	public void setFuncSvcDao(IFuncSvcDao funcSvcDao) {
		this.funcSvcDao = funcSvcDao;
	}

	public final void setSysDefDao( ISysDefDao sysDefDao ) {
		this.sysDefDao = sysDefDao;
	}

	@Override
	public final ISysDefDao getSysDefDao() {
		return this.sysDefDao;
	}
	
	@Override
	public IUserLoginDao getUserLoginDao() {
		return userLoginDao;
	}
	
	public void setUserLoginDao(IUserLoginDao userLoginDao) {
		this.userLoginDao = userLoginDao;
	}
}
