package jp.co.blueship.tri.fw.sm.support;

import jp.co.blueship.tri.fw.sm.dao.execdatasts.IExecDataStsDao;
import jp.co.blueship.tri.fw.sm.dao.funcsvc.IFuncSvcDao;
import jp.co.blueship.tri.fw.sm.dao.lockmgt.ILockMgtDao;
import jp.co.blueship.tri.fw.sm.dao.login.IUserLoginDao;
import jp.co.blueship.tri.fw.sm.dao.procmgt.IProcDetailsDao;
import jp.co.blueship.tri.fw.sm.dao.procmgt.IProcMgtDao;
import jp.co.blueship.tri.fw.sm.dao.sysdef.ISysDefDao;



/**
 * SM管理のDAOのGetterを提供するインタフェースです。
 * <br>ここでは、DAOのGetter Method以外の機能は提供されません。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 * 
 * @version V4.00.00
 * @author le.thixuan
 */
public interface ISmDaoFinder {
	/**
	 * 禁止中サービス管理DAOを取得します。
	 * @return 禁止中サービス管理DAO
	 */
	public ILockMgtDao getLockMgtDao();

	/**
	 * プロセス管理DAOを取得します。
	 * @return プロセス管理DAO
	 */
	public IProcMgtDao getProcMgtDao();

	/**
	 * プロセス詳細DAOを取得します。
	 * @return プロセス詳細DAO
	 */
	public IProcDetailsDao getProcDetailsDao();

	/**
	 * 実行中データのステータスDAOを取得します。
	 * @return 実行中データのステータスDAO
	 */
	public IExecDataStsDao getExecDataStsDao();

	/**
	 * 機能サービスDAOを取得します。
	 * @return 機能サービスDAO
	 */
	public IFuncSvcDao getFuncSvcDao();

	/**
	 * system define DAO を取得します。
	 * @return system define DAO
	 */
	public ISysDefDao getSysDefDao();
	
	public IUserLoginDao getUserLoginDao();

}

