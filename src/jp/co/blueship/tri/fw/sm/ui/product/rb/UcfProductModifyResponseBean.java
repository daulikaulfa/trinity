package jp.co.blueship.tri.fw.sm.ui.product.rb;

import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;
import jp.co.blueship.tri.fw.sm.domain.product.ProductActivate;

public class UcfProductModifyResponseBean extends BaseResponseBean{
	private boolean rm;
	private String productId;
	private String serialNumber;
	
	private String computerName;
	
	private String comment;

	private String productKey;
	private String key1 = null;
	private String key2 = null;
	private String key3 = null;
	private String key4 = null;
	
	private String agentLicense;
	private String key5 = null;
	private String key6 = null;
	private String key7 = null;
	private String key8 = null;

	public boolean isRm() {
		return rm;
	}

	public void setRm(boolean rm) {
		this.rm = rm;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String number) {
		this.serialNumber = number;
	}

	public String getComputerName() {
		return computerName;
	}

	public void setComputerName(String name) {
		this.computerName = name;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String val) {
		this.comment = val;
	}

	public String getProductKey() {
		return productKey;
	}

	public void setProductKey(String key) {
		this.productKey = key;
	}

	public String getKey1() {
		return key1;
	}

	public void setKey1(String key) {
		this.key1 = key;
	}
	
	public String getKey2() {
		return key2;
	}

	public void setKey2(String key) {
		this.key2 = key;
	}
	
	public String getKey3() {
		return key3;
	}

	public void setKey3(String key) {
		this.key3 = key;
	}
	
	public String getKey4() {
		return key4;
	}

	public void setKey4(String key) {
		this.key4 = key;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getAgentLicense() {
		return agentLicense;
	}

	public void setAgentLicense(String agentLicense) {
		this.agentLicense = agentLicense;
	}

	public String getKey5() {
		return key5;
	}

	public void setKey5(String key5) {
		this.key5 = key5;
	}

	public String getKey6() {
		return key6;
	}

	public void setKey6(String key6) {
		this.key6 = key6;
	}

	public String getKey7() {
		return key7;
	}

	public void setKey7(String key7) {
		this.key7 = key7;
	}

	public String getKey8() {
		return key8;
	}

	public void setKey8(String key8) {
		this.key8 = key8;
	}
	
}
