package jp.co.blueship.tri.fw.sm.ui.product.beans;

import jp.co.blueship.tri.fw.act.rb.IBaseResponseBean;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.sm.domain.product.dto.FlowUcfProductEntryServiceBean;
import jp.co.blueship.tri.fw.sm.ui.product.rb.UcfProductEntryResponseBean;
import jp.co.blueship.tri.fw.ui.PresentationProsecutor;

public class FlowUcfProductEntryProsecutor extends PresentationProsecutor {

	public FlowUcfProductEntryProsecutor() {
		super(null);
	}

	public FlowUcfProductEntryProsecutor(BaseBusinessException bbe) {
		super(bbe);
	}

	@Override
	protected IGeneralServiceBean callApplicationService(IService service, IGeneralServiceBean info) throws Exception {
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( info );
    	return ((IGenericTransactionService)service).execute("FlowUcfProductEntryService", serviceDto).getServiceBean();
	}

	@Override
	protected void getApplicationInfo(IApplicationInfo arg0, ISessionInfo arg1, IRequestInfo arg2) throws Exception {

	}

	@Override
	protected IGeneralServiceBean getBussinessInfo(IApplicationInfo appInfo,
			ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {

		if ( this.getLog().isDebugEnabled() ) {
			this.getLog().debug("★★★userId:" + reqInfo.getParameter("userId"));
		}

		FlowUcfProductEntryServiceBean bean = new FlowUcfProductEntryServiceBean();
		String userId = (String)sesInfo.getAttribute(SessionScopeKeyConsts.FLWC00_USER_ID);
		bean.setUserId( userId );
		bean.setUserName(userName());

		return bean;
	}

	@Override
	protected IBaseResponseBean getRequestInfo(IApplicationInfo appInfo,
			ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {

		UcfProductEntryResponseBean resBean = new UcfProductEntryResponseBean();

		resBean.setProductId(reqInfo.getParameter("productId"));
		resBean.setComputerName(reqInfo.getParameter("computerName"));
		resBean.setSerialNumber(reqInfo.getParameter("serialNumber"));
		resBean.setComment(reqInfo.getParameter("comment"));
		resBean.setProductKey(reqInfo.getParameter("productKey"));
		resBean.setKey1(reqInfo.getParameter("key1"));
		resBean.setKey2(reqInfo.getParameter("key2"));
		resBean.setKey3(reqInfo.getParameter("key3"));
		resBean.setKey4(reqInfo.getParameter("key4"));
		resBean.setAgentLicense(reqInfo.getParameter("agentLicense"));
		resBean.setKey5(reqInfo.getParameter("key5"));
		resBean.setKey6(reqInfo.getParameter("key6"));
		resBean.setKey7(reqInfo.getParameter("key7"));
		resBean.setKey8(reqInfo.getParameter("key8"));
		resBean.new MessageUtility().reflectMessage(getBussinessException());
		return resBean;
	}

	@Override
	protected String getResponseName() {
		return "responseBean";
	}

	@Override
	protected IService getService(
			IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo ) {

		IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
		return (IService)ca.getBean("generalService");
	}

	@Override
	protected void getSessionInfo(IApplicationInfo arg0, ISessionInfo arg1, IRequestInfo arg2) throws Exception {

	}

	@Override
	protected void postProcessor(IApplicationInfo arg0, ISessionInfo arg1, IRequestInfo arg2) {

	}

	@Override
	protected void preProcessor(IApplicationInfo arg0, ISessionInfo arg1, IRequestInfo arg2) {

	}

}
