package jp.co.blueship.tri.fw.sm.ui.product;

import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.sm.ui.product.beans.FlowUcfProductDeleteBtnProsecutor;
import jp.co.blueship.tri.fw.ui.PresentationController;

public class FlowUcfProductDeleteBtn extends PresentationController {

	protected void addPresentationProsecutores(PresentationProsecutorManager ppm) {
		ppm.addPresentationProsecutor(new FlowUcfProductDeleteBtnProsecutor());
	}	
	
	protected void addBusinessErrorPresentationProsecutores(PresentationProsecutorManager ppm, BaseBusinessException bbe) {
		ppm.addBusinessErrorPresentationProsecutor(new FlowUcfProductDeleteBtnProsecutor(bbe));
	}

	@Override
	protected String getForward(PresentationProsecutorManager arg0, 
			IRequestInfo arg1, ISessionInfo arg2, IApplicationInfo arg3) {
		return "UcfProductList";
	}

	@Override
	protected String getForwardForBusinessException(PresentationProsecutorManager arg0, 
			IRequestInfo arg1, ISessionInfo arg2, IApplicationInfo arg3, TriRuntimeException arg4) {
		return "UcfProductDelete";
	}

}
