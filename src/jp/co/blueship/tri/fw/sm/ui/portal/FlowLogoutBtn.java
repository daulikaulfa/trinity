package jp.co.blueship.tri.fw.sm.ui.portal;

import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.sm.ui.portal.beans.FlowLogoutBtnProsecutor;
import jp.co.blueship.tri.fw.ui.PresentationController;

public class FlowLogoutBtn extends PresentationController {

	protected void addPresentationProsecutores(PresentationProsecutorManager ppm) {
		ppm.addPresentationProsecutor(new FlowLogoutBtnProsecutor());
	}

	protected String getForward(PresentationProsecutorManager ppm, IRequestInfo reqInfo, ISessionInfo sesInfo, IApplicationInfo appInfo) {
		return "LOGOUT";
	}

	protected String getForwardForBusinessException(PresentationProsecutorManager ppm, IRequestInfo reqInfo, ISessionInfo sesInfo, IApplicationInfo appInfo, TriRuntimeException bbe) {
		return "SystemError";
	}
}