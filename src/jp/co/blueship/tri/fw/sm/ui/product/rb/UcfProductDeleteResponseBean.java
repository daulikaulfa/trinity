package jp.co.blueship.tri.fw.sm.ui.product.rb;

import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;

public class UcfProductDeleteResponseBean extends BaseResponseBean{

	private String productId ;

	private String serialNumber;

	private String computerName;

	private String comment;

	private String productKey;

	private String agentLicense;

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String number) {
		this.serialNumber = number;
	}

	public String getComputerName() {
		return computerName;
	}

	public void setComputerName(String name) {
		this.computerName = name;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String val) {
		this.comment = val;
	}

	public String getProductKey() {
		return productKey;
	}

	public void setProductKey(String key) {
		this.productKey = key;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getAgentLicense() {
		return agentLicense;
	}

	public void setAgentLicense(String agentLicense) {
		this.agentLicense = agentLicense;
	}

}
