package jp.co.blueship.tri.fw.sm.ui.product.rb;

import java.util.List;

import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;
import jp.co.blueship.tri.fw.sm.domain.product.dto.FlowUcfProductListServiceBean.ProductListViewBean;

public class UcfProductListResponseBean extends BaseResponseBean{

	private List<ProductListViewBean> productViewBeanList;

	public List<ProductListViewBean> getProductViewBeanList() {
		return productViewBeanList;
	}

	public void setProductViewBeanList(List<ProductListViewBean> productViewBeanList) {
		this.productViewBeanList = productViewBeanList;
	}

}
