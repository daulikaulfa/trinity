package jp.co.blueship.tri.fw.sm.ui.portal.rb;

import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;


public class TopResponseBean extends BaseResponseBean {
	
	private String projectName	= null;
	private String version		= null;
	private String userId		= null;
	
	
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	
	public String getUserId() {
		return userId;
	}
	public void setUserId( String userId ) {
		this.userId = userId;
	}
}