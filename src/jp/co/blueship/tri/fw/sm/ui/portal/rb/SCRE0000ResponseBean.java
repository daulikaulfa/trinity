package jp.co.blueship.tri.fw.sm.ui.portal.rb;

import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;
import jp.co.blueship.tri.fw.ex.ITranslatable;


/**
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public class SCRE0000ResponseBean extends BaseResponseBean {

	public class Utility {

		public void reflectResponseBean(ITranslatable e){
			// メッセージの取得
			new MessageUtility().reflectMessage(e);
		}
	}
}
