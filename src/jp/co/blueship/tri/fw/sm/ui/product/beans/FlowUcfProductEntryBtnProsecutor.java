package jp.co.blueship.tri.fw.sm.ui.product.beans;

import jp.co.blueship.tri.fw.act.rb.IBaseResponseBean;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.sm.domain.product.dto.FlowUcfProductEntryBtnServiceBean;
import jp.co.blueship.tri.fw.sm.ui.product.rb.UcfProductListResponseBean;
import jp.co.blueship.tri.fw.ui.PresentationProsecutor;

public class FlowUcfProductEntryBtnProsecutor extends PresentationProsecutor {

	public FlowUcfProductEntryBtnProsecutor() {
		super(null);
	}

	public FlowUcfProductEntryBtnProsecutor(BaseBusinessException bbe) {
		super(bbe);
	}

	@Override
	protected IGeneralServiceBean callApplicationService(IService service,
			IGeneralServiceBean info) throws Exception {
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( info );
		return ((IGenericTransactionService)service).execute("FlowUcfProductEntryBtnService", serviceDto).getServiceBean();
	}

	@Override
	protected void getApplicationInfo(IApplicationInfo arg0, ISessionInfo arg1, IRequestInfo arg2) throws Exception {

	}

	@Override
	protected IGeneralServiceBean getBussinessInfo(IApplicationInfo appInfo,
			ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {
		if ( this.getLog().isDebugEnabled() ) {
			this.getLog().debug("★★★serialNumber:" + reqInfo.getParameter("serialNumber"));
			this.getLog().debug("★★★productKey:" + reqInfo.getParameter("productKey"));
			this.getLog().debug("★★★computerName:" + reqInfo.getParameter("computerName"));
		}

		FlowUcfProductEntryBtnServiceBean bean = new FlowUcfProductEntryBtnServiceBean();
		String userId = (String)sesInfo.getAttribute(SessionScopeKeyConsts.FLWC00_USER_ID);
		bean.setUserId( userId );
		bean.setUserName(userName());

		bean.setProductId(reqInfo.getParameter( "productId"));
		bean.setComputerName(reqInfo.getParameter("computerName"));

		bean.setSerialNumber(reqInfo.getParameter("serialNumber"));
		bean.setKey1(reqInfo.getParameter("key1"));
		bean.setKey2(reqInfo.getParameter("key2"));
		bean.setKey3(reqInfo.getParameter("key3"));
		bean.setKey4(reqInfo.getParameter("key4"));
		StringBuilder buf = new StringBuilder();
		buf.append(reqInfo.getParameter("key1"));
		buf.append(reqInfo.getParameter("key2"));
		buf.append(reqInfo.getParameter("key3"));
		buf.append(reqInfo.getParameter("key4"));
		bean.setProductKey(buf.toString());
		buf = new StringBuilder() ;
		buf.append(reqInfo.getParameter("key5"));
		buf.append(reqInfo.getParameter("key6"));
		buf.append(reqInfo.getParameter("key7"));
		buf.append(reqInfo.getParameter("key8"));
		bean.setAgentLicense(buf.toString());
		bean.setComment(reqInfo.getParameter("comment"));

		return bean;
	}

	@Override
	protected IBaseResponseBean getRequestInfo(IApplicationInfo arg0, ISessionInfo arg1, IRequestInfo arg2) throws Exception {

		FlowUcfProductEntryBtnServiceBean bean = (FlowUcfProductEntryBtnServiceBean)getServiceReturnInformation();
		UcfProductListResponseBean resBean = new UcfProductListResponseBean();

		resBean.setProductViewBeanList(bean.getViewBeanList());
		resBean.new MessageUtility().reflectMessage(getBussinessException());
		return resBean;
	}

	@Override
	protected String getResponseName() {
		return "responseBean";
	}

	@Override
	protected IService getService(
			IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo ) {

		IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
		return (IService)ca.getBean("generalService");
	}

	@Override
	protected void getSessionInfo(IApplicationInfo arg0, ISessionInfo arg1, IRequestInfo arg2) throws Exception {

	}

	@Override
	protected void postProcessor(IApplicationInfo arg0, ISessionInfo arg1, IRequestInfo arg2) {

	}

	@Override
	protected void preProcessor(IApplicationInfo arg0, ISessionInfo arg1, IRequestInfo arg2) {

	}

}
