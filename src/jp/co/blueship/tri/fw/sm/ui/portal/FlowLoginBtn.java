package jp.co.blueship.tri.fw.sm.ui.portal;

import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.sm.domain.product.ProductActivate;
import jp.co.blueship.tri.fw.sm.ui.portal.beans.FlowLoginBtnProsecutor;
import jp.co.blueship.tri.fw.ui.PresentationController;

public class FlowLoginBtn extends PresentationController {

	protected void addPresentationProsecutores(PresentationProsecutorManager ppm) {
		ppm.addPresentationProsecutor( new FlowLoginBtnProsecutor() );
	}

	protected void addBusinessErrorPresentationProsecutores(PresentationProsecutorManager ppm, BaseBusinessException bbe) {
		ppm.addBusinessErrorPresentationProsecutor(new FlowLoginBtnProsecutor(bbe));
	}

	protected String getForward(PresentationProsecutorManager ppm, IRequestInfo reqInfo, ISessionInfo sesInfo, IApplicationInfo appInfo) {

		boolean active = ProductActivate.isRmActive();

		if ( active ) {
			//必要なagentのライセンス数を取得
			Integer needLicenseKeyCount =
				(Integer)sesInfo.getAttribute( SessionScopeKeyConsts.FLWC00_AGENT_COUNT );
			if(null != needLicenseKeyCount) {
				//Agentライセンス数チェック
				ProductActivate.isRmActiveAgent(needLicenseKeyCount) ;
			}

			// パスワード有効期限チェック
			Boolean isPasswordEffect =
				(Boolean)sesInfo.getAttribute( SessionScopeKeyConsts.FLWC00_PASSWORD_LIMIT );

			if ( !isPasswordEffect ) {
				return "ChangePassword";
			}

			return "TOP";
		} else {
			return "TOP2";
		}
	}

	protected String getForwardForBusinessException(PresentationProsecutorManager ppm, IRequestInfo reqInfo, ISessionInfo sesInfo, IApplicationInfo appInfo, TriRuntimeException bbe) {
		return "SystemError";
	}
}