package jp.co.blueship.tri.fw.sm.ui.product.beans;

import jp.co.blueship.tri.fw.act.rb.IBaseResponseBean;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.sm.domain.product.ProductActivate;
import jp.co.blueship.tri.fw.sm.domain.product.dto.FlowUcfProductModifyServiceBean;
import jp.co.blueship.tri.fw.sm.ui.product.rb.UcfProductModifyResponseBean;
import jp.co.blueship.tri.fw.ui.PresentationProsecutor;

public class FlowUcfProductModifyProsecutor extends PresentationProsecutor {

	public static final String FLOW_ACTION_ID = "FlowUcfProductModifyService";

	public FlowUcfProductModifyProsecutor() {
		super(null);
	}

	public FlowUcfProductModifyProsecutor(BaseBusinessException bbe) {
		super(bbe);
	}

	@Override
	protected IGeneralServiceBean callApplicationService(IService service,
			IGeneralServiceBean info) throws Exception {
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( info );

		return ((IGenericTransactionService)service).execute(FLOW_ACTION_ID, serviceDto).getServiceBean();
	}

	@Override
	protected void getApplicationInfo(IApplicationInfo arg0, ISessionInfo arg1, IRequestInfo arg2) throws Exception {

	}

	@Override
	protected IGeneralServiceBean getBussinessInfo(IApplicationInfo appInfo,
			ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {

		if ( this.getLog().isDebugEnabled() ) {
			this.getLog().debug("★★★serialNumber:" + reqInfo.getParameter("serialNumber"));
		}

		FlowUcfProductModifyServiceBean bean = new FlowUcfProductModifyServiceBean();
		String userId = (String)sesInfo.getAttribute(SessionScopeKeyConsts.FLWC00_USER_ID);
		bean.setUserId( userId );
		bean.setUserName(userName());

		bean.setProductId(reqInfo.getParameter("productId"));
		bean.setComputerName(reqInfo.getParameter("computerName"));
		bean.setSerialNumber(reqInfo.getParameter("serialNumber"));
		bean.setComment(reqInfo.getParameter("comment"));
		bean.setProductKey(reqInfo.getParameter("productKey"));
		bean.setKey1(reqInfo.getParameter("key1"));
		bean.setKey2(reqInfo.getParameter("key2"));
		bean.setKey3(reqInfo.getParameter("key3"));
		bean.setKey4(reqInfo.getParameter("key4"));
		bean.setAgentLicense(reqInfo.getParameter("agentLicense"));
		bean.setKey5(reqInfo.getParameter("key5"));
		bean.setKey6(reqInfo.getParameter("key6"));
		bean.setKey7(reqInfo.getParameter("key7"));
		bean.setKey8(reqInfo.getParameter("key8"));
		return bean;
	}

	@Override
	protected IBaseResponseBean getRequestInfo(IApplicationInfo arg0, ISessionInfo arg1, IRequestInfo arg2) throws Exception {

		FlowUcfProductModifyServiceBean bean = (FlowUcfProductModifyServiceBean)getServiceReturnInformation();
		UcfProductModifyResponseBean resBean = new UcfProductModifyResponseBean();

		resBean.setRm(ProductActivate.RM_ID.equals(bean.getProductId()));
		resBean.setProductId( bean.getProductId());
		resBean.setComputerName(bean.getComputerName());

		resBean.setSerialNumber(bean.getSerialNumber());
		resBean.setComment(bean.getComment());
		resBean.setProductKey(bean.getProductKey());
		resBean.setKey1(bean.getKey1());
		resBean.setKey2(bean.getKey2());
		resBean.setKey3(bean.getKey3());
		resBean.setKey4(bean.getKey4());
		resBean.setAgentLicense(bean.getAgentLicense());
		resBean.setKey5(bean.getKey5());
		resBean.setKey6(bean.getKey6());
		resBean.setKey7(bean.getKey7());
		resBean.setKey8(bean.getKey8());

		resBean.new MessageUtility().reflectMessage(getBussinessException());

		return resBean;
	}


	@Override
	protected String getResponseName() {
		return "responseBean";
	}

	@Override
	protected IService getService(
			IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo ) {

		IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
		return (IService)ca.getBean("generalService");
	}

	@Override
	protected void getSessionInfo(IApplicationInfo arg0, ISessionInfo arg1, IRequestInfo arg2) throws Exception {

	}

	@Override
	protected void postProcessor(IApplicationInfo arg0, ISessionInfo arg1, IRequestInfo arg2) {

	}

	@Override
	protected void preProcessor(IApplicationInfo arg0, ISessionInfo arg1, IRequestInfo arg2) {

	}

}
