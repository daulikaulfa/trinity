package jp.co.blueship.tri.fw.sm.ui.portal.beans;

import jp.co.blueship.tri.fw.act.rb.IBaseResponseBean;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionMultiWindows;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.sm.domain.portal.dto.FlowLoginBtnServiceBean;
import jp.co.blueship.tri.fw.sm.ui.portal.rb.TopResponseBean;
import jp.co.blueship.tri.fw.ui.PresentationProsecutor;

public class FlowLoginBtnProsecutor extends PresentationProsecutor {

	public static final String FLOW_ACTION_ID = "FlowLoginBtnService";

	public FlowLoginBtnProsecutor() {
		super(null);
	}

	public FlowLoginBtnProsecutor(BaseBusinessException bbe) {
		super(bbe);
	}

	@Override
	protected IGeneralServiceBean callApplicationService(IService service,
			IGeneralServiceBean info) throws Exception {
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( info );

		return ((IGenericTransactionService)service).execute(FLOW_ACTION_ID, serviceDto).getServiceBean();
	}

	protected void getApplicationInfo(
			IApplicationInfo appInfo,
			ISessionInfo sesInfo,
			IRequestInfo reqInfo ) throws Exception {

	}

	protected IGeneralServiceBean getBussinessInfo(
			IApplicationInfo appInfo,
			ISessionInfo sesInfo,
			IRequestInfo reqInfo ) throws Exception {

		SessionMultiWindows session = new SessionMultiWindows( sesInfo,reqInfo );
		session.setAttribute( SessionScopeKeyConsts.FLOW_ACTION_ID, FLOW_ACTION_ID );

		FlowLoginBtnServiceBean bean = new FlowLoginBtnServiceBean();

		String referer		= session.getReferer( this );
		String forward		= session.getForward( this );
		String screenType	= session.getScreenType( this );

		bean.setReferer			( referer );
		bean.setForward			( forward );
		bean.setScreenType		( screenType );

		String userId = (String)sesInfo.getAttribute( SessionScopeKeyConsts.FLWC00_USER_ID );
		if ( null == referer ) {
			// ログイン用処理
			bean.setUserId( reqInfo.getUserId() );
			bean.setUserName(userName());

			// FlowChangePasswordBtnProsecutorからのキラーパス
			String tempRef = (String)sesInfo.getAttribute( SessionScopeKeyConsts.FLWC00_REFERER );
			bean.setReferer( tempRef );

		} else {
			bean.setUserId( userId );
			bean.setUserName(userName());
		}

		return bean;
	}

	protected IBaseResponseBean getRequestInfo(
			IApplicationInfo appInfo,
			ISessionInfo sesInfo,
			IRequestInfo reqInfo ) throws Exception {

		TopResponseBean resBean = new TopResponseBean();

		FlowLoginBtnServiceBean bean =
			(FlowLoginBtnServiceBean)getServiceReturnInformation();

		//ログイン用処理
		{
			resBean.setProjectName	( bean.getProjectName() );
			resBean.setVersion		( bean.getVersion() );
			resBean.setUserId		( bean.getUserId() );
		}

		resBean.new MessageUtility().reflectComment( bean, false );
		resBean.new MessageUtility().reflectMessage( bean );
		resBean.new MessageUtility().reflectMessage( getBussinessException() );

		return resBean;
	}

	protected String getResponseName() {
		return "responseBean";
	}

	protected IService getService(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) {
		IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
		return (IService)ca.getBean("loginService");
	}

	protected void getSessionInfo(IApplicationInfo appInfo,
			ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {
		String userID = reqInfo.getUserId();
		sesInfo.setAttribute(SessionScopeKeyConsts.FLWC00_USER_ID, userID);

		FlowLoginBtnServiceBean bean = (FlowLoginBtnServiceBean)getServiceReturnInformation();
		sesInfo.setAttribute(SessionScopeKeyConsts.FLWC00_USER_NAME, bean.getUserName());

		String projectName = bean.getProjectName();
		sesInfo.setAttribute(SessionScopeKeyConsts.FLWC00_PROJECT_NAME, projectName);

		String version = bean.getVersion();
		sesInfo.setAttribute(SessionScopeKeyConsts.FLWC00_VERSION, version);

		Boolean isPasswordEffect = bean.isPasswordEffect();
		sesInfo.setAttribute( SessionScopeKeyConsts.FLWC00_PASSWORD_LIMIT, isPasswordEffect );

		Integer needLicenseKeyCount = bean.getNeedLicenseKeyCount();
		sesInfo.setAttribute( SessionScopeKeyConsts.FLWC00_AGENT_COUNT, needLicenseKeyCount );

	}

	protected void postProcessor(
			IApplicationInfo appInfo,
			ISessionInfo sesInfo,
			IRequestInfo reqInfo ) {

		sesInfo.removeAttribute( FLOW_ACTION_ID );
		sesInfo.removeAttribute( SessionScopeKeyConsts.FLWC00_REFERER );
	}

	protected void preProcessor(
			IApplicationInfo appInfo,
			ISessionInfo sesInfo,
			IRequestInfo reqInfo ) {

	}

}
