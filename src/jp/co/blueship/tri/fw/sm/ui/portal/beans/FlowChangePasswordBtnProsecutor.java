package jp.co.blueship.tri.fw.sm.ui.portal.beans;

import jp.co.blueship.tri.fw.act.rb.IBaseResponseBean;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionMultiWindows;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.sm.domain.portal.dto.FlowChangePasswordBtnServiceBean;
import jp.co.blueship.tri.fw.sm.ui.portal.rb.TopResponseBean;
import jp.co.blueship.tri.fw.ui.PresentationProsecutor;

public class FlowChangePasswordBtnProsecutor extends PresentationProsecutor {

	public static final String FLOW_ACTION_ID = "FlowChangePasswordBtnService";

	public FlowChangePasswordBtnProsecutor() {
		super( null );
	}

	public FlowChangePasswordBtnProsecutor( BaseBusinessException bbe ) {
		super( bbe );
	}

	@Override
	protected IGeneralServiceBean callApplicationService(IService service,
			IGeneralServiceBean info) throws Exception {
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( info );

		return ((IGenericTransactionService)service).execute(FLOW_ACTION_ID, serviceDto).getServiceBean();
	}

	protected void getApplicationInfo(
			IApplicationInfo appInfo,
			ISessionInfo sesInfo,
			IRequestInfo reqInfo ) throws Exception {

	}

	protected IGeneralServiceBean getBussinessInfo(
			IApplicationInfo appInfo,
			ISessionInfo sesInfo,
			IRequestInfo reqInfo ) throws Exception {

		SessionMultiWindows session = new SessionMultiWindows( sesInfo,reqInfo );
		session.setAttribute( SessionScopeKeyConsts.FLOW_ACTION_ID, FLOW_ACTION_ID );

		FlowChangePasswordBtnServiceBean bean = new FlowChangePasswordBtnServiceBean();

		String screenType = session.getScreenType( this );
		bean.setScreenType	( screenType );

		bean.setUserId( reqInfo.getUserId() );
		bean.setUserName(userName());

		bean.setOldPassword	( reqInfo.getParameter("oldPassword") );
		bean.setPassword	( reqInfo.getParameter("password") );
		bean.setRePassword	( reqInfo.getParameter("rePassword") );

		String referer = session.getReferer( this );
		bean.setReferer( referer );

		return bean;
	}

	protected IBaseResponseBean getRequestInfo(
			IApplicationInfo appInfo,
			ISessionInfo sesInfo,
			IRequestInfo reqInfo ) throws Exception {

		TopResponseBean resBean = new TopResponseBean();
		FlowChangePasswordBtnServiceBean bean =
			(FlowChangePasswordBtnServiceBean)getServiceReturnInformation();

		String projectName =
			(String)sesInfo.getAttribute( SessionScopeKeyConsts.FLWC00_PROJECT_NAME );
		resBean.setProjectName	( projectName );

		String version =
			(String)sesInfo.getAttribute( SessionScopeKeyConsts.FLWC00_VERSION );
		resBean.setVersion		( version );

		resBean.setUserId		( bean.getUserId() );

		resBean.new MessageUtility().reflectComment( bean, false );
		resBean.new MessageUtility().reflectMessage( bean );
		resBean.new MessageUtility().reflectMessage( getBussinessException() );

		return resBean;
	}

	protected String getResponseName() {
		return "responseBean";
	}

	protected IService getService(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) {
		IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
		return (IService)ca.getBean("changePasswordService");
	}

	protected void getSessionInfo(
			IApplicationInfo appInfo,
			ISessionInfo sesInfo,
			IRequestInfo reqInfo ) throws Exception {

		FlowChangePasswordBtnServiceBean bean =
			(FlowChangePasswordBtnServiceBean)getServiceReturnInformation();

		String passwordLimit = bean.getPasswordLimit();
		sesInfo.setAttribute( SessionScopeKeyConsts.FLWC00_PASSWORD_LIMIT, passwordLimit );

		// FlowLoginBtnProsecutorへのキラーパス
		String referer = bean.getReferer();
		sesInfo.setAttribute( SessionScopeKeyConsts.FLWC00_REFERER, referer );
	}

	protected void postProcessor(
			IApplicationInfo appInfo,
			ISessionInfo sesInfo,
			IRequestInfo reqInfo ) {

		ISessionInfo session = new SessionMultiWindows( sesInfo,reqInfo );
		session.removeAttribute( FLOW_ACTION_ID );

	}

	protected void preProcessor(
			IApplicationInfo appInfo,
			ISessionInfo sesInfo,
			IRequestInfo reqInfo ) {

	}

}
