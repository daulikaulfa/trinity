package jp.co.blueship.tri.fw.sm.ui.portal;

import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.sm.ui.portal.beans.FlowChangePasswordBtnProsecutor;
import jp.co.blueship.tri.fw.ui.PresentationController;

public class FlowChangePasswordBtn extends PresentationController {

	protected void addPresentationProsecutores( PresentationProsecutorManager ppm ) {
		ppm.addPresentationProsecutor( new FlowChangePasswordBtnProsecutor() );
	}

	protected void addBusinessErrorPresentationProsecutores(
			PresentationProsecutorManager ppm, BaseBusinessException bbe ) {
		ppm.addBusinessErrorPresentationProsecutor( new FlowChangePasswordBtnProsecutor( bbe ) );
	}
	
	protected String getForward(
			PresentationProsecutorManager ppm,
			IRequestInfo reqInfo,
			ISessionInfo sesInfo,
			IApplicationInfo appInfo ) {
		
			return "LOGIN";
	}

	protected String getForwardForBusinessException(
			PresentationProsecutorManager ppm,
			IRequestInfo reqInfo,
			ISessionInfo sesInfo,
			IApplicationInfo appInfo,
			TriRuntimeException bbe ) {
		
		return "ChangePassword";
	}
}