package jp.co.blueship.tri.fw.sm;

import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.sm.contants.LicenseInputMessages;
import jp.co.blueship.tri.fw.sm.contants.LicenseValidationResult;
import jp.co.blueship.tri.fw.sm.contants.ProductLicenseExpiration;
import jp.co.blueship.tri.fw.sm.domain.product.ProductActivate;

import java.util.HashMap;
import java.util.Map;

/**
 * @author GiAnG
 * @version V4.03.00
 */
public class SmLicenseValidationUtils {
	private static final int KEY_LENGTH = 32;
	private static final int MAX_COMMENT_LENGTH = 40;
	private static Map<String, LicenseInputMessages> messagesMap = new HashMap<>();

	static {
		messagesMap.put(ProductActivate.RM_ID, new LicenseInputMessages()
				.setNoInputProductKey(SmMessageId.SM001020E)
				.setInvalidLengthProductKey(SmMessageId.SM001021E)
				.setInvalidProductKey(SmMessageId.SM001022E)
				.setExpiredProductKey(SmMessageId.SM001023E)
				.setInvalidLengthAgentKey(SmMessageId.SM001024E)
				.setInvalidAgentKey(SmMessageId.SM001025E)
				.setInvalidLengthComment(SmMessageId.SM001005E)

				.setSuccessProductKey(SmMessageId.SM003020I)
				.setSuccessLimitedProductKey(SmMessageId.SM003021I)
				.setSuccessAgentKey(SmMessageId.SM003022I)
				.setInfoNoAgentKey(SmMessageId.SM003023I));

		messagesMap.put(ProductActivate.DM_ID, new LicenseInputMessages()
				.setNoInputProductKey(SmMessageId.SM001026E)
				.setInvalidLengthProductKey(SmMessageId.SM001027E)
				.setInvalidProductKey(SmMessageId.SM001028E)
				.setExpiredProductKey(SmMessageId.SM001028E)
				.setInvalidLengthComment(SmMessageId.SM001005E)

				.setSuccessProductKey(SmMessageId.SM003024I)
				.setSuccessLimitedProductKey(SmMessageId.SM003024I));
	}

	public static void checkLicenseInput(String productId, String productKey, String agentKey, String comment) {
		LicenseValidationResult result = validateLicenseInput(productId, productKey, agentKey, comment);
		if (!result.getErrorList().isEmpty()) {
			throw new ContinuableBusinessException(result.getErrorList(), result.getErrorArgs());
		}
	}

	public static LicenseValidationResult validateLicenseInput(String productId, String productKey, String agentKey, String comment) {
		LicenseValidationResult result = new LicenseValidationResult();

		if (productKey.isEmpty()) {
			result.addError(messagesMap.get(productId).getNoInputProductKey());
		} else if (productKey.length() != KEY_LENGTH) {
			result.addError(messagesMap.get(productId).getInvalidLengthProductKey());
		} else {
			ProductLicenseExpiration expiration = ProductActivate.getExpiration(productId, productKey);
			result.setExpiration(expiration);
			if (ProductActivate.isActive(productId, productKey)) {
				if (expiration.isDate()) {
					result.addInfo(messagesMap.get(productId).getSuccessLimitedProductKey(), expiration.getValue());
				} else {
					result.addInfo(messagesMap.get(productId).getSuccessProductKey());
				}

				if (agentKey != null) {
					if (agentKey.isEmpty()) {
						result.addInfo(messagesMap.get(productId).getInfoNoAgentKey());
					} else if (agentKey.length() == KEY_LENGTH) {
						int numberOfAgents = ProductActivate.getNumberOfAgents(productKey, agentKey);
						if (numberOfAgents == 0) {
							result.addError(messagesMap.get(productId).getInvalidAgentKey());
						} else {
							result.addInfo(messagesMap.get(productId).getSuccessAgentKey(), String.valueOf(numberOfAgents));
						}
					}
				}

			} else {
				if (expiration.isDate()) {
					result.addError(messagesMap.get(productId).getExpiredProductKey(), expiration.getValue());
				} else {
					result.addError(messagesMap.get(productId).getInvalidProductKey());
				}
			}
		}

		if (agentKey != null && !agentKey.isEmpty() && agentKey.length() != KEY_LENGTH) {
			result.addError(messagesMap.get(productId).getInvalidLengthAgentKey());
		}

		if (comment != null && comment.length() > MAX_COMMENT_LENGTH) {
			result.addError(messagesMap.get(productId).getInvalidLengthComment());
		}

		return result;
	}
}
