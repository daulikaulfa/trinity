package jp.co.blueship.tri.fw.sm.domainx.account;

import java.sql.Timestamp;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.UmDesignEntryKeyByProductId;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.session.LoginSession;
import jp.co.blueship.tri.fw.sm.dao.login.eb.IUserLoginEntity;
import jp.co.blueship.tri.fw.sm.dao.login.eb.UserLoginCondition;
import jp.co.blueship.tri.fw.sm.dao.login.eb.UserLoginEntity;
import jp.co.blueship.tri.fw.sm.domain.product.ProductActivate;
import jp.co.blueship.tri.fw.sm.domainx.account.dto.FlowLogInServiceBean;
import jp.co.blueship.tri.fw.support.IFwFinderSupport;
import jp.co.blueship.tri.fw.um.UmDesignBusinessRuleUtils;
import jp.co.blueship.tri.fw.um.constants.IconSelectionOption;
import jp.co.blueship.tri.fw.um.constants.LookAndFeel;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.fw.um.dao.project.eb.IProjectEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowLogInService implements IDomain<FlowLogInServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();


	private LoginSession loginSession;
	public void setLoginSession(LoginSession loginSession) {
		this.loginSession = loginSession;
	}

	private IUmFinderSupport support;
	private IFwFinderSupport fwSupport;

	public void setSupport(IFwFinderSupport fwSupport) {
		this.fwSupport = fwSupport;
		this.support = this.fwSupport.getUmFinderSupport();
	}

	@Override
	public IServiceDto<FlowLogInServiceBean> execute( IServiceDto<FlowLogInServiceBean> serviceDto ) {

		FlowLogInServiceBean paramBean = serviceDto.getServiceBean();

		try {
			if ( RequestType.init.equals(paramBean.getParam().getRequestType()) ) {
				this.init(paramBean);
			}
			if ( RequestType.submitChanges.equals(paramBean.getParam().getRequestType()) ) {
				this.submitChanges(paramBean);
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(SmMessageId.SM005215S, e, paramBean.getFlowAction());
		} finally {
		}
	}

	/**
	 * @param paramBean Service Bean
	 */
	private void init( FlowLogInServiceBean paramBean ) {
		this.getProjectInfo(paramBean);
	}

	/**
	 * @param paramBean Service Bean
	 */
	private void submitChanges( FlowLogInServiceBean paramBean ) {
		IUserEntity entity = support.findUserByUserId( paramBean.getParam().getUserId() );
		throwIfInvalidUser(entity);
		// clear old user session
		loginSession.clearOldUserSession( paramBean.getParam().getUserId() );
		// make login with successfull history
		makeLogInHistory(entity);

		Timestamp now = new Timestamp(System.currentTimeMillis());
		Timestamp passwordLimit = entity.getPassTimeLimit();
		Boolean expired = false;

		String productId = DesignSheetFactory.getDesignSheet().getValue(UmDesignEntryKeyByProductId.rm);
		IProjectEntity projectEntity = this.support.findProjectByPrimaryKey(productId);
		String language = projectEntity.getLanguage();
		String timezone = projectEntity.getTimezone();

		if( TriStringUtils.isNotEmpty(entity.getLang())){
			language = entity.getLang();
		}
		if( TriStringUtils.isNotEmpty(entity.getTimeZone())){
			timezone = entity.getTimeZone();
		}

		LookAndFeel lookAndFeel = this.support.getLookAndFeel( paramBean.getParam().getUserId() );

		if (null != passwordLimit && passwordLimit.getTime() <= now.getTime() && ProductActivate.isRmActive()) {
			expired = true;
		}

		paramBean.getHeader().setUserIconPath(UmDesignBusinessRuleUtils.getUserIconSharePath(entity));

		paramBean.getDetailsView()
			.setExpired(expired)
			.setLang(language)
			.setTimeZone(timezone)
			.setLookAndFeel(lookAndFeel)
		;
	}

	private void getProjectInfo(FlowLogInServiceBean serviceBean) {
		String productId = DesignSheetFactory.getDesignSheet().getValue(UmDesignEntryKeyByProductId.rm);
		IProjectEntity project = this.support.findProjectByPrimaryKey(productId);

		serviceBean.setProjectName(project.getProjectNm());
		if (IconSelectionOption.CustomImage.equals(project.getIconTyp())) {
			serviceBean.getHeader().setProjectIconPath(project.getCustomIconPath());

		} else {
			serviceBean.getHeader().setProjectIconPath(project.getDefaultIconPath());
		}
	}

	private void makeLogInHistory(IUserEntity entity) {
		// remove last log in record
		UserLoginCondition condition = new UserLoginCondition();
		condition.setUserId(entity.getUserId());
		fwSupport.getSmFinderSupport().getUserLoginDao().delete(condition.getCondition());

		// insert last login record
		IUserLoginEntity userLogInEntity = new UserLoginEntity();
		userLogInEntity.setUserId(entity.getUserId());
		userLogInEntity.setDelStsId(entity.getDelStsId());
		userLogInEntity.setLastLoginTimestamp(new Timestamp(System.currentTimeMillis()));
		userLogInEntity.setRegTimestamp(entity.getRegTimestamp());
		userLogInEntity.setRegUserNm(entity.getRegUserNm());
		userLogInEntity.setRegUserId(entity.getRegUserId());
		userLogInEntity.setUpdTimestamp(entity.getUpdTimestamp());
		userLogInEntity.setUpdUserNm(entity.getUpdUserNm());
		userLogInEntity.setUpdUserId(entity.getUpdUserId());
		fwSupport.getSmFinderSupport().getUserLoginDao().insert(userLogInEntity);
	}

	/**
	 * @param entity
	 */
	private void throwIfInvalidUser(IUserEntity entity) {
		if (entity != null) {
			return;
		}

		throw new TriSystemException(SmMessageId.SM005004S, UmTables.UM_USER.name());
	}

}
