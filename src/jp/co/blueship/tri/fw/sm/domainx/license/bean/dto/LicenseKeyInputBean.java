package jp.co.blueship.tri.fw.sm.domainx.license.bean.dto;

/**
 *
 * @version V4.02.00
 * @author Akahoshi
 *
 */
public class LicenseKeyInputBean {
	private String key1;
	private String key2;
	private String key3;
	private String key4;

	public String getKey1() {
		return key1;
	}
	public LicenseKeyInputBean setKey1(String key1) {
		this.key1 = key1;
		return this;
	}
	public String getKey2() {
		return key2;
	}
	public LicenseKeyInputBean setKey2(String key2) {
		this.key2 = key2;
		return this;
	}
	public String getKey3() {
		return key3;
	}
	public LicenseKeyInputBean setKey3(String key3) {
		this.key3 = key3;
		return this;
	}
	public String getKey4() {
		return key4;
	}
	public LicenseKeyInputBean setKey4(String key4) {
		this.key4 = key4;
		return this;
	}

	public String combineKey() {
		return key1 + key2 + key3 + key4;
	}
}
