package jp.co.blueship.tri.fw.sm.domainx.account;

import java.util.HashMap;
import java.util.Map;

import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.sm.contants.PasswordComplexityLevel;
import jp.co.blueship.tri.fw.sm.domain.portal.dto.FlowChangePasswordBtnServiceBean;
import jp.co.blueship.tri.fw.sm.domainx.account.beans.dto.PasswordDetailsView;
import jp.co.blueship.tri.fw.sm.domainx.account.dto.FlowLogInServiceChangePasswordBean;
import jp.co.blueship.tri.fw.um.constants.LookAndFeel;
import jp.co.blueship.tri.fw.um.support.UmFinderSupport;

/**
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Sam
 */
public class FlowLogInServiceChangePassword implements IDomain<FlowLogInServiceChangePasswordBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IDomain<IGeneralServiceBean> completeService = null;

	private UmFinderSupport umFinderSupport = null;

	public void setUmFinderSupport(UmFinderSupport umFinderSupport) {
		this.umFinderSupport = umFinderSupport;
	}

	public void setCompleteService(IDomain<IGeneralServiceBean> completeService) {
		this.completeService = completeService;
	}

	@Override
	public IServiceDto<FlowLogInServiceChangePasswordBean> execute( IServiceDto<FlowLogInServiceChangePasswordBean> serviceDto ) {

		FlowLogInServiceChangePasswordBean paramBean = serviceDto.getServiceBean();

		try {
			if ( RequestType.init.equals(paramBean.getParam().getRequestType()) ) {
				paramBean.getLoginDetailsView().setUserId(paramBean.getUserId());
			}
			if (RequestType.onChange.equals(paramBean.getParam().getRequestType())) {
				checkComplexity(paramBean);
			}
			if ( RequestType.submitChanges.equals(paramBean.getParam().getRequestType()) ) {
				this.submitChanges(paramBean);
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(SmMessageId.SM005215S, e, paramBean.getFlowAction());
		} finally {
		}
	}

	/**
	 * @param paramBean Service Bean
	 */
	private void submitChanges( FlowLogInServiceChangePasswordBean paramBean ) {

		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowChangePasswordBtnServiceBean innerServiceBean = new FlowChangePasswordBtnServiceBean();
		TriPropertyUtils.copyProperties(innerServiceBean, paramBean);
		dto.setServiceBean(innerServiceBean);

		this.beforeExecution(paramBean, innerServiceBean);
		LookAndFeel lookAndFeel = this.umFinderSupport.getLookAndFeel( paramBean.getUserId() );
		paramBean.getLoginDetailsView().setLookAndFeel(lookAndFeel);
		completeService.execute(dto);
	}

	private void beforeExecution( FlowLogInServiceChangePasswordBean src, FlowChangePasswordBtnServiceBean dest) {
		if ( RequestType.submitChanges.equals(src.getParam().getRequestType()) ) {
			dest.setUserId(src.getUserId());
			dest.setOldPassword(src.getParam().getCurrentPassword());
			dest.setPassword(src.getParam().getNewPassword());
			dest.setRePassword(src.getParam().getConfirmPassword());
		}
	}

	private void checkComplexity( FlowLogInServiceChangePasswordBean paramBean) {

		String newPassword = paramBean.getParam().getNewPassword();
		PasswordDetailsView passwordView = paramBean.getLoginDetailsView().getPassword();

		if(!TriStringUtils.isEmpty(newPassword)){
			int complexity = 0;

			if(newPassword.length()>=8){
				passwordView.setMoreThan8characters(true);
				complexity++;
			}

			boolean hasUpperCase = !newPassword.equals(newPassword.toLowerCase());
			boolean hasLowerCase = !newPassword.equals(newPassword.toUpperCase());

			if(hasUpperCase && hasLowerCase){
				passwordView.setContainsUppercaseAndLowercase(true);
				complexity++;
			}

			if (TriStringUtils.containsNumerals(newPassword) ) {
				passwordView.setContainsNumerals(true);
				complexity++;
			}

			Map<Character, Integer> charCountMap = new HashMap<Character, Integer>();

			char[] strArray = newPassword.toCharArray();
			for (char c : strArray){
				if(charCountMap.containsKey(c)){
					charCountMap.put(c, charCountMap.get(c)+1);
				}
				else {
					charCountMap.put(c, 1);
				}
			}

			for(Map.Entry<Character , Integer> entry : charCountMap.entrySet() ){
				int val = (int)entry.getValue();
				if(val >= 3 ){
					passwordView.setSameCharacterLessThan3Times(true);
					complexity = 1;
					break;
				}
			}

			if(complexity == 0){
				passwordView.setPasswordComplexityLevel(PasswordComplexityLevel.none);
			}

			if(complexity == 1){
				passwordView.setPasswordComplexityLevel(PasswordComplexityLevel.Low);
			}

			if(complexity == 2){
				passwordView.setPasswordComplexityLevel(PasswordComplexityLevel.Middle);
			}

			if(complexity >= 3){
				passwordView.setPasswordComplexityLevel(PasswordComplexityLevel.High);
			}
		}else{
			passwordView.setPasswordComplexityLevel(PasswordComplexityLevel.none);
			passwordView.setSameCharacterLessThan3Times(false);
			passwordView.setContainsNumerals(false);
			passwordView.setMoreThan8characters(false);
			passwordView.setContainsUppercaseAndLowercase(false);
		}
	}
}


