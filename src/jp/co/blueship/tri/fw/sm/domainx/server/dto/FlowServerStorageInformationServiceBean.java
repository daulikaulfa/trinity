package jp.co.blueship.tri.fw.sm.domainx.server.dto;

import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;

import java.util.Collection;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author GiAnG
 * @version 4.03.00
 */
public class FlowServerStorageInformationServiceBean extends DomainServiceBean {
	private static final long serialVersionUID = 1L;
	private RequestParam param = new RequestParam();
	private boolean calc;
	private boolean done;
	private Collection<SpaceInfo> spaceInfo;

	@Override
	public RequestParam getParam() {
		return param;
	}

	public boolean isCalc() {
		return calc;
	}

	public FlowServerStorageInformationServiceBean setCalc(boolean calc) {
		this.calc = calc;
		return this;
	}

	public boolean isDone() {
		return done;
	}

	public FlowServerStorageInformationServiceBean setDone(boolean done) {
		this.done = done;
		return this;
	}

	public Collection<SpaceInfo> getSpaceInfo() {
		return spaceInfo;
	}

	public FlowServerStorageInformationServiceBean setSpaceInfo(Collection<SpaceInfo> spaceInfo) {
		this.spaceInfo = spaceInfo;
		return this;
	}

	public static class SpaceInfo {
		private String root;
		private long total;
		private long free;
		private long other;

		private AtomicLong publicSize = new AtomicLong();
		private AtomicLong privateSize = new AtomicLong();

		private String totalText;
		private String freeText;
		private String publicText;
		private String privateText;
		private String otherText;

		private float publicPercent;
		private float privatePercent;
		private float otherPercent;

		public String getRoot() {
			return root;
		}

		public SpaceInfo setRoot(String root) {
			this.root = root;
			return this;
		}

		public long getTotal() {
			return total;
		}

		public SpaceInfo setTotal(long total) {
			this.total = total;
			return this;
		}

		public long getFree() {
			return free;
		}

		public SpaceInfo setFree(long free) {
			this.free = free;
			return this;
		}

		public long getOther() {
			return other;
		}

		public SpaceInfo setOther(long other) {
			this.other = other;
			return this;
		}

		public AtomicLong getPublicSize() {
			return publicSize;
		}

		public SpaceInfo setPublicSize(AtomicLong publicSize) {
			this.publicSize = publicSize;
			return this;
		}

		public AtomicLong getPrivateSize() {
			return privateSize;
		}

		public SpaceInfo setPrivateSize(AtomicLong privateSize) {
			this.privateSize = privateSize;
			return this;
		}

		public String getTotalText() {
			return totalText;
		}

		public SpaceInfo setTotalText(String totalText) {
			this.totalText = totalText;
			return this;
		}

		public String getFreeText() {
			return freeText;
		}

		public SpaceInfo setFreeText(String freeText) {
			this.freeText = freeText;
			return this;
		}

		public String getPublicText() {
			return publicText;
		}

		public SpaceInfo setPublicText(String publicText) {
			this.publicText = publicText;
			return this;
		}

		public String getPrivateText() {
			return privateText;
		}

		public SpaceInfo setPrivateText(String privateText) {
			this.privateText = privateText;
			return this;
		}

		public String getOtherText() {
			return otherText;
		}

		public SpaceInfo setOtherText(String otherText) {
			this.otherText = otherText;
			return this;
		}

		public float getPublicPercent() {
			return publicPercent;
		}

		public SpaceInfo setPublicPercent(float publicPercent) {
			this.publicPercent = publicPercent;
			return this;
		}

		public float getPrivatePercent() {
			return privatePercent;
		}

		public SpaceInfo setPrivatePercent(float privatePercent) {
			this.privatePercent = privatePercent;
			return this;
		}

		public float getOtherPercent() {
			return otherPercent;
		}

		public SpaceInfo setOtherPercent(float otherPercent) {
			this.otherPercent = otherPercent;
			return this;
		}
	}
}
