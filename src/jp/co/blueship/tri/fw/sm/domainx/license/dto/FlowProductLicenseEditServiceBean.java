package jp.co.blueship.tri.fw.sm.domainx.license.dto;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.sm.domainx.license.bean.dto.ProductLicenseDetailsViewBean;
import jp.co.blueship.tri.fw.sm.domainx.license.bean.dto.ProductLicenseEditInputBean;
import jp.co.blueship.tri.fw.um.constants.LookAndFeel;

import java.util.*;


/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 * @version V4.02.00
 * @author Akahoshi
 */
public class FlowProductLicenseEditServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private RequestsCompletion result = new RequestsCompletion();
	private Map<String, ProductLicenseDetailsViewBean> detailsViewBeans = new LinkedHashMap<>();
	private LookAndFeel lookAndFeel;

	public RequestParam getParam() {
		return param;
	}

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowProductLicenseEditServiceBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	public Map<String, ProductLicenseDetailsViewBean> getDetailsViewBeans() {
		return detailsViewBeans;
	}

	public LookAndFeel getLookAndFeel() {
		return lookAndFeel;
	}

	public FlowProductLicenseEditServiceBean setLookAndFeel(LookAndFeel lookAndFeel) {
		this.lookAndFeel = lookAndFeel;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private Map<String, ProductLicenseEditInputBean> inputBeans = new LinkedHashMap<>();

		public Map<String, ProductLicenseEditInputBean> getInputBeans() {
			return inputBeans;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {

		private boolean completed = false;

		public boolean isCompleted() {
			return completed;
		}
		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}

}
