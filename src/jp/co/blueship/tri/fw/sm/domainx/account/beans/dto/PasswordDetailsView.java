package jp.co.blueship.tri.fw.sm.domainx.account.beans.dto;

import jp.co.blueship.tri.fw.sm.contants.PasswordComplexityLevel;

/**
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class PasswordDetailsView {
	private PasswordComplexityLevel passwordComplexityLevel = PasswordComplexityLevel.none;
	private boolean moreThan8characters = false;
	private boolean containsUppercaseAndLowercase  = false;
	private boolean containsNumerals  = false;
	private boolean sameCharacterLessThan3Times  = false;

	public PasswordComplexityLevel getPasswordComplexityLevel() {
		return passwordComplexityLevel;
	}
	public PasswordDetailsView setPasswordComplexityLevel(PasswordComplexityLevel passwordComplexityLevel) {
		this.passwordComplexityLevel = passwordComplexityLevel;
		return this;
	}
	public boolean isMoreThan8characters() {
		return moreThan8characters;
	}
	public PasswordDetailsView setMoreThan8characters(boolean moreThan8characters) {
		this.moreThan8characters = moreThan8characters;
		return this;
	}
	public boolean containsUppercaseAndLowercase() {
		return containsUppercaseAndLowercase;
	}
	public PasswordDetailsView setContainsUppercaseAndLowercase(boolean containsUppercaseAndLowercase) {
		this.containsUppercaseAndLowercase = containsUppercaseAndLowercase;
		return this;
	}
	public boolean containsNumerals() {
		return containsNumerals;
	}
	public PasswordDetailsView setContainsNumerals(boolean containsNumerals) {
		this.containsNumerals = containsNumerals;
		return this;
	}
	public boolean isSameCharacterLessThan3Times() {
		return sameCharacterLessThan3Times;
	}
	public PasswordDetailsView setSameCharacterLessThan3Times(boolean sameCharacterLessThan3Times) {
		this.sameCharacterLessThan3Times = sameCharacterLessThan3Times;
		return this;
	}

}
