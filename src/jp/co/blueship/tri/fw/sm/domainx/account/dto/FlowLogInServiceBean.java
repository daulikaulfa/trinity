package jp.co.blueship.tri.fw.sm.domainx.account.dto;

import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.um.constants.LookAndFeel;

/**
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowLogInServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private LogInDetailsView detailsView = new LogInDetailsView();

	@Override
	public RequestParam getParam() {
		return param;
	}

	public LogInDetailsView getDetailsView() {
		return detailsView;
	}

	public FlowLogInServiceBean setDetailsView(LogInDetailsView detailsView) {
		this.detailsView = detailsView;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String userId = null;
		private String password = null;

		public String getUserId() {
			return userId;
		}
		public RequestParam setUserId(String userId) {
			this.userId = userId;
			return this;
		}
		public String getPassword() {
			return password;
		}
		public RequestParam setPassword(String password) {
			this.password = password;
			return this;
		}
	}

	/**
	 * LogIn View Bean
	 */
	public class LogInDetailsView {
		private boolean expired = false;
		private String lang;
		private String timezone;
		private LookAndFeel lookAndFeel;

		public boolean isExpired() {
			return expired;
		}
		public LogInDetailsView setExpired(boolean expired) {
			this.expired = expired;
			return this;
		}

		public String getLang() {
			return lang;
		}
		public LogInDetailsView setLang(String lang) {
			this.lang = lang;
			return this;
		}

		public String getTimeZone() {
			return timezone;
		}
		public LogInDetailsView setTimeZone(String id) {
			this.timezone = id;
			return this;
		}
		
		public LookAndFeel getLookAndFeel() {
			return lookAndFeel;
		}
		public LogInDetailsView setLookAndFeel(LookAndFeel lookAndFeel) {
			this.lookAndFeel = lookAndFeel;
			return this;
		}

	}

}
