package jp.co.blueship.tri.fw.sm.domainx.license.bean.dto;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class ProductLicenseEditInputBean {
	private boolean submit;
	private boolean rm;
	private String productId;
	private String comment;
	private LicenseKeyInputBean productKey;
	private LicenseKeyInputBean agentKey;

	public boolean isSubmit() {
		return submit;
	}
	public ProductLicenseEditInputBean setSubmit(boolean submit) {
		this.submit = submit;
		return this;
	}

	public boolean isRm() {
		return rm;
	}
	public ProductLicenseEditInputBean setRm(boolean rm) {
		this.rm = rm;
		return this;
	}

	public String getProductId() {
		return productId;
	}
	public ProductLicenseEditInputBean setProductId(String productId) {
		this.productId = productId;
		return this;
	}

	public String getComment() {
		return comment;
	}
	public ProductLicenseEditInputBean setComment(String comment) {
		this.comment = comment;
		return this;
	}

	public LicenseKeyInputBean getProductKey() {
		return productKey;
	}
	public ProductLicenseEditInputBean setProductKey(LicenseKeyInputBean productKey) {
		this.productKey = productKey;
		return this;
	}

	public LicenseKeyInputBean getAgentKey() {
		return agentKey;
	}
	public ProductLicenseEditInputBean setAgentKey(LicenseKeyInputBean agentKey) {
		this.agentKey = agentKey;
		return this;
	}
}
