package jp.co.blueship.tri.fw.sm.domainx.cmn;

import jp.co.blueship.tri.am.dao.areq.eb.AreqCondition;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.HeadBlCondition;
import jp.co.blueship.tri.am.dao.baseline.eb.IHeadBlEntity;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotCondition;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.PjtCondition;
import jp.co.blueship.tri.am.support.IAmFinderSupport;
import jp.co.blueship.tri.bm.dao.bp.eb.BpCondition;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.bm.dao.constants.BmTables;
import jp.co.blueship.tri.bm.support.IBmFinderSupport;
import jp.co.blueship.tri.dcm.dao.constants.DcmTables;
import jp.co.blueship.tri.dcm.dao.rep.eb.IRepEntity;
import jp.co.blueship.tri.dcm.dao.rep.eb.RepCondition;
import jp.co.blueship.tri.dcm.support.IDcmFinderSupport;
import jp.co.blueship.tri.dm.dao.constants.DmTables;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusId;
import jp.co.blueship.tri.fw.constants.status.AmHeadBlStatusId;
import jp.co.blueship.tri.fw.constants.status.AmLotStatusId;
import jp.co.blueship.tri.fw.constants.status.RmRaStatusId;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.sm.domainx.cmn.dto.FlowDispatcherServiceBean;
import jp.co.blueship.tri.fw.sm.domainx.cmn.dto.FlowDispatcherServiceBean.SubmitMode;
import jp.co.blueship.tri.fw.um.constants.DataAttribute;
import jp.co.blueship.tri.fw.um.constants.IconSelectionOption;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.CtgCondition;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.ICtgEntity;
import jp.co.blueship.tri.fw.um.dao.filter.eb.IFilterEntity;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.IMstoneEntity;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.MstoneCondition;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.IWikiEntity;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.WikiCondition;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;
import jp.co.blueship.tri.rm.dao.constants.RmTables;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.RaCondition;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.RpCondition;
import jp.co.blueship.tri.rm.support.IRmFinderSupport;

/**
 * @version V4.00.00
 * @author Yohei Tanaka
 *
 * @version V4.03.00
 * @author Cuong Nguyen
 */
public class FlowDispatcherService implements IDomain<FlowDispatcherServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IUmFinderSupport umSupport;
	private IAmFinderSupport amSupport;
	private IBmFinderSupport bmSupport;
	private IRmFinderSupport rmSupport;
	private IDcmFinderSupport dcmSupport;

	public void setAmSupport(IAmFinderSupport amSupport) {
		this.amSupport = amSupport;
		this.umSupport = amSupport.getUmFinderSupport();
	}
	public void setBmSupport(IBmFinderSupport bmSupport) {
		this.bmSupport = bmSupport;
		this.dcmSupport = bmSupport.getDcmFinderSupport();
	}
	public void setRmSupport(IRmFinderSupport rmSupport) {
		this.rmSupport = rmSupport;
	}

	@Override
	public IServiceDto<FlowDispatcherServiceBean> execute( IServiceDto<FlowDispatcherServiceBean> serviceDto ) {

		FlowDispatcherServiceBean serviceBean = serviceDto.getServiceBean();

		try {
			String lotId = "";
			String dataId = "";
			ServiceId redirectServiceId = ServiceId.none;

			if (serviceBean.getParam().getSubmitMode() == SubmitMode.list) {
				lotId = serviceBean.getParam().getToListInputInfo().getLotId();
				dataId = lotId;
				redirectServiceId = serviceBean.getParam().getToListInputInfo().getServiceId();
			}

			if (serviceBean.getParam().getSubmitMode() == SubmitMode.details || serviceBean.getParam().getSubmitMode() == SubmitMode.pendingDetails) {
				DataAttribute dataAttribute = serviceBean.getParam().getToDetailsInputInfo().getDataAttribute();
				String tableName = dataAttribute.getTableAttribute().name();
				dataId = serviceBean.getParam().getToDetailsInputInfo().getDataId();

				if (isDeleted(tableName, dataId)) {
					throw new ContinuableBusinessException(SmMessageId.SM003015I);
				}

				lotId = getLotId(tableName, dataId);
				String status = getStatusId(tableName, dataId);
				String areqCtgCd = "";
				if ( AmTables.AM_AREQ.name().equals(tableName) ) areqCtgCd = getAreqCtgCd(dataId);

				redirectServiceId = getServiceId(dataAttribute, status, areqCtgCd);

				if( serviceBean.getParam().getSubmitMode() == SubmitMode.pendingDetails ){

					redirectServiceId = this.getServiceIdOfPendingDetails(dataAttribute, lotId, status, areqCtgCd);

					if( ServiceId.AmChangeApprovedDetailsService.equals( redirectServiceId ) ||
							ServiceId.AmChangeApprovalPendingDetailsService.equals( redirectServiceId ) ){

						dataId = this.getPjtId(dataId);
					}
			}
			}

			if( serviceBean.getParam().getSubmitMode() == SubmitMode.searchFilter ) {
				IFilterEntity filterEntity = this.umSupport.findFilterByPrimaryKey( serviceBean.getParam().getToSearchFilterInputInfo().getDataId() );

				lotId = filterEntity.getLotId();
				dataId = lotId;
				redirectServiceId = ServiceId.value( filterEntity.getSvcId() );

				serviceBean.getDetailsView().setFilterData( filterEntity.getFilterData() );
			}

			if (TriStringUtils.isNotEmpty(lotId)) {
				ILotEntity lot = amSupport.findLotEntity(lotId);

				String lotIconPath = lot.getCustomIconPath();
				if(IconSelectionOption.DefaultImage.value().equals(lot.getIconTyp())) {
					lotIconPath = lot.getDefaultIconPath();
				}

				Boolean isLotEnabled = AmLotStatusId.LotInProgress.getStatusId().equals(lot.getStsId());

				serviceBean.getDetailsView()
						.setLotId(lotId)
						.setDataId(dataId)
						.setLotSubject(lot.getLotNm())
						.setThemeColor(lot.getThemeColor())
						.setLotIconPath(lotIconPath)
						.setIsLotEnabled(isLotEnabled)
				;
			}

			serviceBean.getDetailsView().setServiceId(redirectServiceId);

			return serviceDto;
		} catch (Exception e) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(SmMessageId.SM005215S, e, serviceBean.getFlowAction());
		}
	}

	private ServiceId getServiceId(DataAttribute dataAttribute, String status, String areqCtgCd){

		if (dataAttribute == null) return ServiceId.none;

		if ( dataAttribute.equals(DataAttribute.Lot) ) {
			return ServiceId.AmLotDetailsService;
		}

		if ( dataAttribute.equals(DataAttribute.ChangeProperty) ) {
			return ServiceId.AmChangePropertyDetailsService;
		}

		if ( dataAttribute.equals(DataAttribute.CheckOutRequest)
				|| dataAttribute.equals(DataAttribute.CheckInRequest)
				|| dataAttribute.equals(DataAttribute.RemovalRequest) ) {
			if( AreqCtgCd.LendingRequest.equals( areqCtgCd ) ) {
				return ServiceId.AmCheckoutRequestDetailsService;
			}
			if( AreqCtgCd.ReturningRequest.equals( areqCtgCd ) ) {
				return ServiceId.AmCheckinRequestDetailsService;
			}
			if( AreqCtgCd.RemovalRequest.equals( areqCtgCd ) ) {

				if( AmAreqStatusId.DraftRemovalRequest.equals( status )
						|| AmAreqStatusId.PendingRemovalRequest.equals( status )){
					return ServiceId.AmRemovalRequestEditService;
				}

				return ServiceId.AmRemovalRequestDetailsService;
			}
		}

		if ( dataAttribute.equals(DataAttribute.BuildPackage) ) {
			return ServiceId.BmBuildPackageDetailsService;
		}

		if ( dataAttribute.equals(DataAttribute.ReleaseRequest) ) {

			if(RmRaStatusId.ReleaseRequestApproved.equals(status)){
				return ServiceId.RmReleaseRequestApprovedDetailsService;
			}

			if( RmRaStatusId.DraftReleaseRequest.equals(status)
					|| RmRaStatusId.PendingReleaseRequest.equals( status ) ){

				return ServiceId.RmReleaseRequestEditService;
			}

			return ServiceId.RmReleaseRequestApprovalPendingDetailsService;
		}

		if ( dataAttribute.equals(DataAttribute.ReleasePackage) ) {
			return ServiceId.RmFlowReleasePackageDetailsService;
		}

		if ( dataAttribute.equals(DataAttribute.DeploymentJob )) {
			return ServiceId.DmFlowDeploymentJobDetailsService;
		}

		if ( dataAttribute.equals(DataAttribute.Report) ) {
			return ServiceId.DcmReportListService;
		}

		if ( dataAttribute.equals(DataAttribute.Category) ) {
			return ServiceId.UmCategoryEditService;
		}

		if ( dataAttribute.equals(DataAttribute.Milestone) ) {
			return ServiceId.UmMilestoneEditService;
		}

		if ( dataAttribute.equals(DataAttribute.Wiki) ) {
			return ServiceId.UmWikiDetailsService;
		}
		

		return ServiceId.none;
	}

	private ServiceId getServiceIdOfPendingDetails( DataAttribute dataAttribute, String lotId, String status, String areqCtgCd){

		if( DataAttribute.Lot.equals(dataAttribute) ){

			HeadBlCondition headBlCondition = new HeadBlCondition();
			headBlCondition.setLotId(lotId);
			headBlCondition.setStsIds( new String[]{ AmHeadBlStatusId.ConflictChecked.getStatusId(),
													 AmHeadBlStatusId.Merged.getStatusId()});

			if(this.amSupport.getHeadBlDao().count( headBlCondition.getCondition()) > 0) {
				return ServiceId.AmConflictCheckResourceFileResultsService;
			}
		}

		if ( DataAttribute.CheckOutRequest.equals(dataAttribute)
				|| DataAttribute.CheckInRequest.equals(dataAttribute)
				|| DataAttribute.RemovalRequest.equals(dataAttribute) ) {

			if( AmAreqStatusId.CheckoutRequested.equals(status)) {
				return ServiceId.AmCheckinRequestService;
			}

			if(AmAreqStatusId.CheckinRequested.equals(status) || AmAreqStatusId.RemovalRequested.equals(status)){
				return ServiceId.AmChangeApprovalPendingDetailsService;
			}

			if(AmAreqStatusId.CheckinRequestApproved.equals(status) || AmAreqStatusId.RemovalRequestApproved.equals(status)){
				return ServiceId.AmChangeApprovedDetailsService;
			}
		}

		return this.getServiceId(dataAttribute, status, areqCtgCd);
	}

	private String getAreqCtgCd(String recordId){
		return amSupport.findAreqEntity(recordId).getAreqCtgCd();
	}

	private String getPjtId(String recordId){
		return amSupport.findAreqEntity(recordId).getPjtId();
	}

	private String getStatusId(String tableName, String recordId){
		if ( AmTables.AM_LOT.name().equals(tableName) ) {
			return amSupport.findLotEntity(recordId).getStsId();
		}
		if ( AmTables.AM_PJT.name().equals(tableName) ) {
			return amSupport.findPjtEntity(recordId).getStsId();
		}
		if ( AmTables.AM_AREQ.name().equals(tableName) ) {
			return amSupport.findAreqEntity(recordId).getStsId();
		}
		if ( BmTables.BM_BP.name().equals(tableName) ) {
			return bmSupport.findBpEntity(recordId).getStsId();
		}
		if ( RmTables.RM_RA.name().equals(tableName) ) {
			return rmSupport.findRaEntity(recordId).getStsId();
		}
		if ( RmTables.RM_RP.name().equals(tableName) ) {
			return rmSupport.findRpEntity(recordId).getStsId();
		}
		if ( AmTables.AM_HEAD_BL.name().equals(tableName) ) {
			return amSupport.findHeadBlEntity(recordId).getStsId();
		}
		if ( DcmTables.DCM_REP.name().equals(tableName) ) {
			return dcmSupport.findRepEntity(recordId).getStsId();
		}

		return "";
	}

	private String getLotId(String tableName, String recordId){
		if ( AmTables.AM_LOT.name().equals(tableName) ) {
			return recordId;
		}
		if ( AmTables.AM_PJT.name().equals(tableName) ) {
			return amSupport.findPjtEntity(recordId).getLotId();
		}
		if ( AmTables.AM_AREQ.name().equals(tableName) ) {
			return amSupport.findAreqEntity(recordId).getLotId();
		}
		if ( BmTables.BM_BP.name().equals(tableName) ) {
			return bmSupport.findBpEntity(recordId).getLotId();
		}
		if ( RmTables.RM_RA.name().equals(tableName) ) {
			return rmSupport.findRaEntity(recordId).getLotId();
		}
		if ( RmTables.RM_RP.name().equals(tableName) ) {
			return rmSupport.findRpEntity(recordId).getLotId();
		}
		if ( DmTables.DM_DO.name().equals(tableName) ) {
			return rmSupport.findDmDoEntity(recordId).getLotId();
		}
		if ( AmTables.AM_HEAD_BL.name().equals(tableName) ) {
			return amSupport.findHeadBlEntity(recordId).getLotId();
		}
		if ( DcmTables.DCM_REP.name().equals(tableName) ) {
			return dcmSupport.findRepEntity(recordId).getLotId();
		}
		if ( UmTables.UM_CTG.name().equals(tableName) ) {
			return umSupport.findCtgByPrimaryKey(recordId).getLotId();
		}
		if ( UmTables.UM_MSTONE.name().equals(tableName) ) {
			return umSupport.findMstoneByPrimaryKey(recordId).getLotId();
		}
		if ( UmTables.UM_WIKI.name().equals(tableName) ) {
			return umSupport.findWikiByPrimaryKey(recordId).getLotId();
		}

		return "";
	}

	private boolean isDeleted(String tableName, String recordId){
		if ( AmTables.AM_LOT.name().equals(tableName) ) {
			LotCondition condition = new LotCondition();
			condition.setLotId(recordId);
			ILotEntity entity = amSupport.getLotDao().findByPrimaryKey(condition.getCondition()) ;
			return entity == null || entity.getDelStsId() == StatusFlg.on;
		}

		if ( AmTables.AM_PJT.name().equals(tableName) ) {
			PjtCondition condition = new PjtCondition();
			condition.setPjtId(recordId);
			IPjtEntity entity = amSupport.getPjtDao().findByPrimaryKey(condition.getCondition()) ;
			return entity == null || entity.getDelStsId() == StatusFlg.on;
		}

		if ( AmTables.AM_AREQ.name().equals(tableName) ) {
			AreqCondition condition = new AreqCondition();
			condition.setAreqId(recordId);
			IAreqEntity entity = amSupport.getAreqDao().findByPrimaryKey( condition.getCondition() );
			return entity == null || entity.getDelStsId() == StatusFlg.on;
		}

		if ( BmTables.BM_BP.name().equals(tableName) ) {
			BpCondition condition = new BpCondition();
			condition.setBpId( recordId );
			IBpEntity entity = bmSupport.getBpDao().findByPrimaryKey( condition.getCondition() );
			return entity == null || entity.getDelStsId() == StatusFlg.on;
		}

		if ( RmTables.RM_RA.name().equals(tableName) ) {
			RaCondition condition = new RaCondition();
			condition.setRaId( recordId );
			IRaEntity entity = rmSupport.getRaDao().findByPrimaryKey( condition.getCondition() );
			return entity == null || entity.getDelStsId() == StatusFlg.on;
		}

		if ( RmTables.RM_RP.name().equals(tableName) ) {
			RpCondition condition = new RpCondition();
			condition.setRpId( recordId );
			IRpEntity entity = rmSupport.getRpDao().findByPrimaryKey( condition.getCondition() );
			return entity == null || entity.getDelStsId() == StatusFlg.on;
		}

		if ( AmTables.AM_HEAD_BL.name().equals(tableName) ) {
			HeadBlCondition condition = new HeadBlCondition();
			condition.setHeadBlId(recordId);
			IHeadBlEntity entity = amSupport.getHeadBlDao().findByPrimaryKey(condition.getCondition()) ;
			return entity == null || entity.getDelStsId() == StatusFlg.on;
		}

		if ( DcmTables.DCM_REP.name().equals(tableName) ) {
			RepCondition condition = new RepCondition();
			condition.setRepId(recordId);
			IRepEntity entity = dcmSupport.getRepDao().findByPrimaryKey( condition.getCondition() );
			return entity == null || entity.getDelStsId() == StatusFlg.on;
		}

		if ( UmTables.UM_CTG.name().equals(tableName) ) {
			CtgCondition condition = new CtgCondition();
			condition.setCtgId(recordId);
			ICtgEntity entity = umSupport.getCtgDao().findByPrimaryKey(condition.getCondition());
			return entity == null || entity.getDelStsId() == StatusFlg.on;
		}

		if ( UmTables.UM_MSTONE.name().equals(tableName) ) {
			MstoneCondition condition = new MstoneCondition();
			condition.setMstoneId(recordId);
			IMstoneEntity entity = umSupport.getMstoneDao().findByPrimaryKey(condition.getCondition());
			return entity == null || entity.getDelStsId() == StatusFlg.on;
		}

		if ( UmTables.UM_WIKI.name().equals(tableName) ) {
			WikiCondition condition = new WikiCondition();
			condition.setWikiId(recordId);
			IWikiEntity entity = umSupport.getWikiDao().findByPrimaryKey(condition.getCondition());
			return entity == null || entity.getDelStsId() == StatusFlg.on;
		}

		return false;
	}
}



