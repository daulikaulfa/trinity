package jp.co.blueship.tri.fw.sm.domainx.cmn.dto;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.um.constants.DataAttribute;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author Yohei Tanaka
 */
public class FlowDispatcherServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private DispatcherDetailsView detailsView = new DispatcherDetailsView();

	@Override
	public RequestParam getParam() {
		return param;
	}

	public DispatcherDetailsView getDetailsView() {
		return detailsView;
	}
	public FlowDispatcherServiceBean setDetailsView(DispatcherDetailsView detailsView) {
		this.detailsView = detailsView;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {

		private SubmitMode submitMode = SubmitMode.details;
		private DispatcherToDetailsInput toDetailsInputInfo = new DispatcherToDetailsInput();
		private DispatcherToListInput toListInputInfo = new DispatcherToListInput();
		private DispatcherToSearchFilterInput toSearchFilterInputInfo = new DispatcherToSearchFilterInput();

		public SubmitMode getSubmitMode() {
			return submitMode;
		}
		public RequestParam setSubmitMode(SubmitMode submitMode) {
			this.submitMode = submitMode;
			return this;
		}

		public DispatcherToDetailsInput getToDetailsInputInfo() {
			return toDetailsInputInfo;
		}
		public RequestParam setToDetailsInputInfo(DispatcherToDetailsInput toDetailsInputInfo) {
			this.toDetailsInputInfo = toDetailsInputInfo;
			return this;
		}

		public DispatcherToListInput getToListInputInfo() {
			return toListInputInfo;
		}
		public RequestParam setToListInputInfo(DispatcherToListInput toListInputInfo) {
			this.toListInputInfo = toListInputInfo;
			return this;
		}

		public DispatcherToSearchFilterInput getToSearchFilterInputInfo() {
			return toSearchFilterInputInfo;
		}
		public RequestParam setToSearchFilterInputInfo(DispatcherToSearchFilterInput toSearchFilterInputInfo) {
			this.toSearchFilterInputInfo = toSearchFilterInputInfo;
			return this;
		}
	}

	public class DispatcherToDetailsInput {
		private DataAttribute dataAttribute = DataAttribute.none;
		private String dataId = null;

		public DataAttribute getDataAttribute() {
			return dataAttribute;
		}
		public DispatcherToDetailsInput setDataAttribute(DataAttribute dataAttribute) {
			this.dataAttribute = dataAttribute;
			return this;
		}

		public String getDataId() {
			return dataId;
		}
		public DispatcherToDetailsInput setDataId(String dataId) {
			this.dataId = dataId;
			return this;
		}
	}

	public class DispatcherToListInput {
		private String lotId = null;
		private ServiceId serviceId = ServiceId.none;

		public String getLotId() {
			return lotId;
		}
		public DispatcherToListInput setLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}

		public ServiceId getServiceId() {
			return serviceId;
		}
		public DispatcherToListInput setServiceId(ServiceId serviceId) {
			this.serviceId = serviceId;
			return this;
		}
	}

	public class DispatcherToSearchFilterInput {
		private String dataId = null;

		public String getDataId() {
			return dataId;
		}
		public DispatcherToSearchFilterInput setDataId(String dataId) {
			this.dataId = dataId;
			return this;
		}
	}

	/**
	 * Dispatcher Details
	 */
	public class DispatcherDetailsView {

		private ServiceId serviceId = ServiceId.none;
		private String dataId = null;
		private String filterData = null;
		private String lotId = null;
		private String lotSubject = null;
		private String themeColor = null;
		private String lotIconPath = null;
		private Boolean isLotEnabled = true;

		public Boolean getIsLotEnabled() {
			return isLotEnabled;
		}
		public void setIsLotEnabled(Boolean isLotEnabled) {
			this.isLotEnabled = isLotEnabled;
		}

		public ServiceId getServiceId() {
			return serviceId;
		}
		public DispatcherDetailsView setServiceId(ServiceId serviceId) {
			this.serviceId = serviceId;
			return this;
		}

		public String getDataId() {
			return dataId;
		}
		public DispatcherDetailsView setDataId(String dataId) {
			this.dataId = dataId;
			return this;
		}

		public String getFilterData() {
			return filterData;
		}
		public DispatcherDetailsView setFilterData(String filterData) {
			this.filterData = filterData;
			return this;
		}

		public String getLotId() {
			return lotId;
		}
		public DispatcherDetailsView setLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}

		public String getLotSubject() {
			return lotSubject;
		}
		public DispatcherDetailsView setLotSubject(String lotSubject) {
			this.lotSubject = lotSubject;
			return this;
		}

		public String getThemeColor() {
			return themeColor;
		}
		public DispatcherDetailsView setThemeColor(String themeColor) {
			this.themeColor = themeColor;
			return this;
		}

		public String getLotIconPath() {
			return lotIconPath;
		}
		public DispatcherDetailsView setLotIconPath(String lotIconPath) {
			this.lotIconPath = lotIconPath;
			return this;
		}

	}

	/**
	 * This is the class of enumeration types for 'submit request'.
	 */
	public enum SubmitMode {
		/**
		 * Transition to the details
		 */
		details( "details" ),
		/**
		 * Transition to the next step details
		 */
		pendingDetails("pendingDetails"),
		/**
		 * Transition to the list
		 */
		list( "list" ),
		/**
		 *
		 */
		searchFilter( "searchFilter" ),
		;

		private String value = null;

		private SubmitMode( String value) {
			this.value = value;
		}

		public boolean equals( String value ) {
			SubmitMode mode = value( value );

			if ( null == mode ) return false;
			if ( ! this.equals(mode) ) return false;

			return true;
		}

		public String value() {
			return this.value;
		}

		public static SubmitMode value( String value ) {
			if ( TriStringUtils.isEmpty( value ) ) {
				return null;
			}

			for ( SubmitMode mode : values() ) {
				if ( mode.value().equals( value ) ) {
					return mode;
				}
			}

			return details;
		}
	}

}
