package jp.co.blueship.tri.fw.sm.domainx.license.bean.dto;


/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class ProductLicenseDetailsViewBean {
	private String productId;
	private String cpNm;
	private String serialNumber;
	private Integer numberOfAgent = 0;
	private String expiration;

	public String getProductId() {
		return productId;
	}
	public ProductLicenseDetailsViewBean setProductId(String productId) {
		this.productId = productId;
		return this;
	}
	public String getCpNm() {
		return cpNm;
	}
	public ProductLicenseDetailsViewBean setCpNm(String cpNm) {
		this.cpNm = cpNm;
		return this;
	}
	public String getSerialNumber() {
		return serialNumber;
	}
	public ProductLicenseDetailsViewBean setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
		return this;
	}
	public Integer getNumberOfAgent() {
		return numberOfAgent;
	}
	public ProductLicenseDetailsViewBean setNumberOfAgent(Integer numberOfAgent) {
		this.numberOfAgent = numberOfAgent;
		return this;
	}
	public String getExpiration() {
		return expiration;
	}
	public ProductLicenseDetailsViewBean setExpiration(String expiration) {
		this.expiration = expiration;
		return this;
	}

}
