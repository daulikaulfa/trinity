package jp.co.blueship.tri.fw.sm.domainx.account.dto;

import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.sm.domainx.account.beans.dto.PasswordDetailsView;
import jp.co.blueship.tri.fw.um.constants.LookAndFeel;

/**
 * This is a service for the back-end(domain).
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Sam
 */
public class FlowLogInServiceChangePasswordBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private LogInDetailsView loginDetailsView = new LogInDetailsView();


	@Override
	public RequestParam getParam() {
		return param;
	}

	public LogInDetailsView getLoginDetailsView() {
		return loginDetailsView;
	}
	public void setLoginDetailsView(LogInDetailsView loginDetailsView) {
		this.loginDetailsView = loginDetailsView;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String currentPassword = null;
		private String newPassword = null;
		private String confirmPassword = null;

		public String getCurrentPassword() {
			return currentPassword;
		}
		public RequestParam setCurrentPassword(String currentPassword) {
			this.currentPassword = currentPassword;
			return this;
		}

		public String getNewPassword() {
			return newPassword;
		}
		public RequestParam setNewPassword(String newPassword) {
			this.newPassword = newPassword;
			return this;
		}

		public String getConfirmPassword() {
			return confirmPassword;
		}
		public RequestParam setConfirmPassword(String confirmPassword) {
			this.confirmPassword = confirmPassword;
			return this;
		}
	}

	/**
	 * Password View
	 */
	public class LogInDetailsView {
		private String userId = null;
		private PasswordDetailsView password = new PasswordDetailsView();
		private LookAndFeel lookAndFeel = LookAndFeel.none;

		public LookAndFeel getLookAndFeel() {
			return lookAndFeel;
		}

		public LogInDetailsView setLookAndFeel(LookAndFeel lookAndFeel) {
			this.lookAndFeel = lookAndFeel;
			return this;
		}

		public String getUserId() {
			return userId;
		}
		public LogInDetailsView setUserId(String userId) {
			this.userId = userId;
			return this;
		}

		public PasswordDetailsView getPassword() {
			return password;
		}
		public LogInDetailsView setPassword(PasswordDetailsView password) {
			this.password = password;
			return this;
		}
	}
}
