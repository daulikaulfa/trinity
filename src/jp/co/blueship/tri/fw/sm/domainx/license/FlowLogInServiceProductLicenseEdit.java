package jp.co.blueship.tri.fw.sm.domainx.license;

import jp.co.blueship.tri.fw.sm.domainx.license.dto.FlowProductLicenseEditServiceBean;
import jp.co.blueship.tri.fw.um.constants.LookAndFeel;
import jp.co.blueship.tri.fw.um.support.UmFinderSupport;

public class FlowLogInServiceProductLicenseEdit extends FlowProductLicenseEditService {
	private UmFinderSupport umFinderSupport = null;

	public void setUmFinderSupport(UmFinderSupport umFinderSupport) {
		this.umFinderSupport = umFinderSupport;
	}

	@Override
	protected void init(FlowProductLicenseEditServiceBean paramBean) {
		super.init(paramBean);
		LookAndFeel lookAndFeel = this.umFinderSupport.getLookAndFeel(paramBean.getUserId());
		paramBean.setLookAndFeel(lookAndFeel);
	}
}
