package jp.co.blueship.tri.fw.sm.domainx.license;

import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.sm.contants.LicenseValidationResult;
import jp.co.blueship.tri.fw.sm.SmLicenseValidationUtils;
import jp.co.blueship.tri.fw.sm.dao.plmgt.eb.IPlMgtEntity;
import jp.co.blueship.tri.fw.sm.domain.product.ProductActivate;
import jp.co.blueship.tri.fw.sm.domainx.license.bean.dto.LicenseKeyInputBean;
import jp.co.blueship.tri.fw.sm.domainx.license.bean.dto.ProductLicenseDetailsViewBean;
import jp.co.blueship.tri.fw.sm.domainx.license.bean.dto.ProductLicenseEditInputBean;
import jp.co.blueship.tri.fw.sm.domainx.license.dto.FlowProductLicenseEditServiceBean;
import jp.co.blueship.tri.fw.sm.support.SmFinderSupport;

import java.util.ArrayList;
import java.util.List;

public class FlowProductLicenseEditService implements IDomain<FlowProductLicenseEditServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	protected SmFinderSupport support = null;

	public void setSupport(SmFinderSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowProductLicenseEditServiceBean> execute(IServiceDto<FlowProductLicenseEditServiceBean> serviceDto) {
		FlowProductLicenseEditServiceBean paramBean = serviceDto.getServiceBean();
		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");
			if (RequestType.init.equals(paramBean.getParam().getRequestType())) {
				this.init(paramBean);
			}

			if (RequestType.submitChanges.equals(paramBean.getParam().getRequestType())) {
				this.submitChanges(paramBean);
			}

			return serviceDto;
		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(SmMessageId.SM005215S, e, paramBean.getFlowAction());
		}
	}

	protected void init(FlowProductLicenseEditServiceBean paramBean) {
		List<IPlMgtEntity> entities = support.findPlMgtEntities();
		for (IPlMgtEntity entity : entities) {
			String productId = entity.getProductId();

			ProductLicenseDetailsViewBean viewBean = new ProductLicenseDetailsViewBean()
					.setProductId(productId)
					.setCpNm(entity.getCpNm())
					.setSerialNumber(entity.getSerialNo());

			ProductLicenseEditInputBean inputBean = new ProductLicenseEditInputBean()
					.setRm(ProductActivate.RM_ID.equals(productId))
					.setProductId(productId)
					.setComment(entity.getRemarks())
					.setProductKey(parseLicenseKeyInputBean(entity.getPlKey()));

			if (inputBean.isRm()) {
				inputBean.setSubmit(true)
						 .setAgentKey(parseLicenseKeyInputBean(entity.getAgentPlKey()));

				if (entity.getPlKey() != null) {
					viewBean.setExpiration(ProductActivate.getExpiration(productId, entity.getPlKey()).getValue());
					if (entity.getAgentPlKey() != null) {
						viewBean.setNumberOfAgent(ProductActivate.getNumberOfAgents(entity.getPlKey(), entity.getAgentPlKey()));
					}
				}
			}

			paramBean.getDetailsViewBeans().put(productId, viewBean);
			paramBean.getParam().getInputBeans().put(productId, inputBean);
		}
	}

	private LicenseKeyInputBean parseLicenseKeyInputBean(String key) {
		return key == null || key.length() != 32 ? new LicenseKeyInputBean()
				: new LicenseKeyInputBean()
				.setKey1(key.substring(0, 8))
				.setKey2(key.substring(8, 16))
				.setKey3(key.substring(16, 24))
				.setKey4(key.substring(24, 32));
	}

	private void submitChanges(FlowProductLicenseEditServiceBean paramBean) {
		List<IPlMgtEntity> entities = new ArrayList<>();

		List<IMessageId> errorMsgIds = new ArrayList<>();
		List<String[]> errorArgs = new ArrayList<>();

		List<IMessageId> infoMsgIds = new ArrayList<>();
		List<String[]> infoArgs = new ArrayList<>();

		for (ProductLicenseEditInputBean inputBean : paramBean.getParam().getInputBeans().values()) {
			if (!inputBean.isSubmit()) {
				continue;
			}
			String productId = inputBean.getProductId();
			String productKeyStr = inputBean.getProductKey().combineKey();
			String agentKeyStr = inputBean.isRm() ? inputBean.getAgentKey().combineKey() : null;
			String comment = inputBean.getComment();

			LicenseValidationResult result = SmLicenseValidationUtils.validateLicenseInput(productId, productKeyStr, agentKeyStr, comment);
			errorMsgIds.addAll(result.getErrorList());
			errorArgs.addAll(result.getErrorArgs());
			infoMsgIds.addAll(result.getInfoList());
			infoArgs.addAll(result.getInfoArgs());

			IPlMgtEntity entity = support.findPlMgtEntity(productId);
			entity.setAgentPlKey(agentKeyStr);
			entity.setPlKey(productKeyStr);
			entity.setPlTimeLimit(result.getExpiration().formatDbValue());
			entity.setRemarks(comment);
			entities.add(entity);
		}

		if (!errorMsgIds.isEmpty()) {
			throw new ContinuableBusinessException(errorMsgIds, errorArgs);
		}
		for (IPlMgtEntity entity : entities) {
			int ret = support.getPlMgtDao().update(entity);
			if (ret != 1) {
				throw new ContinuableBusinessException(SmMessageId.SM001008E);
			}
		}
		for (int i = 0; i < infoMsgIds.size(); i++) {
			paramBean.getMessageInfo().addFlashTranslatable(infoMsgIds.get(i), infoArgs.get(i));
		}
		paramBean.getResult().setCompleted(true);
	}

}
