package jp.co.blueship.tri.fw.sm.domainx.server;

import jp.co.blueship.tri.am.dao.head.eb.HeadCondition;
import jp.co.blueship.tri.am.dao.head.eb.IHeadEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotCondition;
import jp.co.blueship.tri.am.support.AmFinderSupport;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.sm.domainx.server.dto.FlowServerStorageInformationServiceBean;
import jp.co.blueship.tri.fw.sm.domainx.server.dto.FlowServerStorageInformationServiceBean.SpaceInfo;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author GiAnG
 * @version 4.03.00
 */
public class FlowServerStorageInformationService implements IDomain<FlowServerStorageInformationServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();
	private static final int BUFFER_TIME = 15000;

	private static final ExecutorService executor = Executors.newSingleThreadExecutor();
	private static volatile Future<Collection<SpaceInfo>> future;
	private static volatile long lastDone;

	private AmFinderSupport support;

	public void setSupport(AmFinderSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowServerStorageInformationServiceBean> execute(IServiceDto<FlowServerStorageInformationServiceBean> serviceDto) {
		FlowServerStorageInformationServiceBean paramBean = serviceDto.getServiceBean();
		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");
			asyncCalc(paramBean);
			return serviceDto;
		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(SmMessageId.SM005215S, e, paramBean.getFlowAction());
		}
	}

	private void asyncCalc(final FlowServerStorageInformationServiceBean paramBean) {
		if (future == null || (future.isDone() && paramBean.isCalc() && System.currentTimeMillis() - lastDone > BUFFER_TIME)) {
			future = executor.submit(new Callable<Collection<SpaceInfo>>() {
				@Override
				public Collection<SpaceInfo> call() throws Exception {
					Map<File, SpaceInfo> result = new LinkedHashMap<>();

					for (File root : File.listRoots()) {
						result.put(root, new SpaceInfo()
								.setRoot(root.getPath())
								.setTotal(root.getTotalSpace())
								.setFree(root.getFreeSpace()));
					}

					List<ILotEntity> lots = support.getLotDao().find(new LotCondition().getCondition());
					for (ILotEntity lot : lots) {
						addPublicSize(result, lot.getInOutBox());
						addPublicSize(result, lot.getLotMwPath());
						addPublicSize(result, lot.getHistPath());

						addPrivateSize(result, lot.getSysInBox());
						addPrivateSize(result, lot.getWsPath());
					}

					List<IHeadEntity> heads = support.getHeadDao().find(new HeadCondition().getCondition());
					for (IHeadEntity head : heads) {
						addPrivateSize(result, head.getMwPath());
					}

					for (File root : File.listRoots()) {
						SpaceInfo info = result.get(root);
						if (info.getPrivateSize().get() + info.getPublicSize().get() == 0) {
							result.remove(root);
						} else {
							info.setOther(info.getTotal() - info.getFree() - info.getPublicSize().get() - info.getPrivateSize().get());

							info.setPublicPercent(info.getPublicSize().get() * 100f / info.getTotal());
							info.setPrivatePercent(info.getPrivateSize().get() * 100f / info.getTotal());
							info.setOtherPercent(info.getOther() * 100f / info.getTotal());

							info.setTotalText(TriFileUtils.fileSizeOf(info.getTotal()));
							info.setFreeText(TriFileUtils.fileSizeOf(info.getFree()));

							info.setPublicText(TriFileUtils.fileSizeOf(info.getPublicSize().get()));
							info.setPrivateText(TriFileUtils.fileSizeOf(info.getPrivateSize().get()));
							info.setOtherText(TriFileUtils.fileSizeOf(info.getOther()));
						}
					}

					lastDone = System.currentTimeMillis();
					return result.values();
				}
			});
		}
		paramBean.setDone(future.isDone());
		if (future.isDone()) {
			try {
				paramBean.setSpaceInfo(future.get());
			} catch (InterruptedException | ExecutionException e) {
				LogHandler.fatal(log, e);
			}
		}
	}

	private void addPrivateSize(Map<File, SpaceInfo> result, String path) {
		try {
			File root = new File(path).getCanonicalFile().toPath().getRoot().toFile();
			addSize(path, result.get(root).getPrivateSize());
		} catch (Exception e) {
			LogHandler.fatal(log, e);
		}
	}

	private void addPublicSize(Map<File, SpaceInfo> result, String path) {
		try {
			File root = new File(path).getCanonicalFile().toPath().getRoot().toFile();
			addSize(path, result.get(root).getPublicSize());
		} catch (Exception e) {
			LogHandler.fatal(log, e);
		}
	}

	private void addSize(String path, final AtomicLong publicSize) throws IOException {
		Files.walkFileTree(Paths.get(path), new FileVisitor<Path>() {
			@Override
			public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
				publicSize.addAndGet(attrs.size());
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
				return FileVisitResult.CONTINUE;
			}
		});
	}
}
