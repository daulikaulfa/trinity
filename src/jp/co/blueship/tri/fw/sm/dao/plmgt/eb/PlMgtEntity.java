package jp.co.blueship.tri.fw.sm.dao.plmgt.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.fw.dao.orm.EntityFooter;


/**
 * product license management entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class PlMgtEntity extends EntityFooter implements IPlMgtEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * product-ID
	 */
	public String productId = null;
	/**
	 * computer name
	 */
	public String cpNm = null;
	/**
	 * serial number
	 */
	public String serialNo = null;
	/**
	 *  product license key
	 */
	public String plKey = null;
	/**
	 *  agent product license key
	 */
	public String agentPlKey = null;
	/**
	 *  remarks
	 */
	public String remarks = null;

	/**
	 *  Product license Time Limit
	 */
	public Timestamp plTimeLimit = null;


	/**
	 * product-IDを取得します。
	 * @return product-ID
	 */
	public String getProductId() {
	    return productId;
	}
	/**
	 * product-IDを設定します。
	 * @param productId product-ID
	 */
	public void setProductId(String productId) {
	    this.productId = productId;
	}
	/**
	 * computer nameを取得します。
	 * @return computer name
	 */
	public String getCpNm() {
	    return cpNm;
	}
	/**
	 * computer nameを設定します。
	 * @param cpName computer name
	 */
	public void setCpNm(String cpNm) {
	    this.cpNm = cpNm;
	}
	/**
	 * serial numberを取得します。
	 * @return serial number
	 */
	public String getSerialNo() {
	    return serialNo;
	}
	/**
	 * serial numberを設定します。
	 * @param serialNo serial number
	 */
	public void setSerialNo(String serialNo) {
	    this.serialNo = serialNo;
	}
	/**
	 * product license keyを取得します。
	 * @return product license key
	 */
	public String getPlKey() {
	    return plKey;
	}
	/**
	 * product license keyを設定します。
	 * @param plKey product license key
	 */
	public void setPlKey(String plKey) {
	    this.plKey = plKey;
	}
	/**
	 * agent product license keyを取得しますkey。
	 * @return agent product license
	 */
	public String getAgentPlKey() {
	    return agentPlKey;
	}
	/**
	 * agent product license keyを設定します。
	 * @param agentPlKey agent product license key
	 */
	public void setAgentPlKey(String agentPlKey) {
	    this.agentPlKey = agentPlKey;
	}
	/**
	 * remarksを取得します。
	 * @return remarks
	 */
	public String getRemarks() {
	    return remarks;
	}
	/**
	 * remarksを設定します。
	 * @param remarks remarks
	 */
	public void setRemarks(String remarks) {
	    this.remarks = remarks;
	}

	/**
	 * get Product license Time Limit
	 */
	public Timestamp getPlTimeLimit(){
		return this.plTimeLimit;
	}
	/**
	 * setting Product license Time Limit
	 * @param plTimeLimit Product license Time Limit
	 */
	public void setPlTimeLimit(Timestamp plTimeLimit){
		this.plTimeLimit = plTimeLimit;
	}

}