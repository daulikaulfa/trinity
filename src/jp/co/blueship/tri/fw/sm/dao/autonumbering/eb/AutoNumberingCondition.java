package jp.co.blueship.tri.fw.sm.dao.autonumbering.eb;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.sm.dao.autonumbering.constants.AutoNumberingItems;
import jp.co.blueship.tri.fw.sm.dao.constants.SmTables;

/**
 * The SQL condition of the user entity.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public class AutoNumberingCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute attr = SmTables.SM_AUTO_NUMBERING;

	public AutoNumberingCondition() {
		super(attr);
	}

	/**
	 * auto numbering key
	 */
	public String numberingKey = null;


	/**
	 * auto numbering keyを取得します。
	 * @return auto numbering key
	 */
	public String getNumberingKey() {
	    return numberingKey;
	}
	/**
	 * auto numbering keyを設定します。
	 * @param numberingKey auto numbering key
	 */
	public void setNumberingKey(String numberingKey) {
	    this.numberingKey = numberingKey;
	    super.append( AutoNumberingItems.numberingKey, numberingKey );
	}

}
