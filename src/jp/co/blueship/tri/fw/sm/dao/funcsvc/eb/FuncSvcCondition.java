package jp.co.blueship.tri.fw.sm.dao.funcsvc.eb;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.dao.orm.psql.SqlFormatUtils;
import jp.co.blueship.tri.fw.sm.dao.constants.SmTables;
import jp.co.blueship.tri.fw.sm.dao.funcsvc.constants.FuncSvcItems;

/**
 * The SQL condition of the function service entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class FuncSvcCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute attr = SmTables.SM_FUNC_SVC;

	/**
	 * svc-ID
	 */
	public String svcId = null;
	/**
	 * svc-ID's
	 */
	public String[] svcIds = null;
	/**
	 * service name
	 */
	public String svcNm = null;
	/**
	 * service content
	 */
	public String svcContent = null;
	/**
	 * is concurrent
	 */
	public StatusFlg isConcurrent = StatusFlg.off;
	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		   super.append(FuncSvcItems.delStsId, StatusFlg.off.parseBoolean() );
	}

	public FuncSvcCondition(){
		super(attr);
	}

	/**
	 * svc-IDを取得します。
	 * @return svc-ID
	 */
	public String getSvcId() {
	    return svcId;
	}

	/**
	 * svc-IDを設定します。
	 * @param svcId svc-ID
	 */
	public void setSvcId(String svcId) {
	    this.svcId = svcId;
	    super.append(FuncSvcItems.svcId, svcId );
	}

	/**
	 * svc-ID'sを取得します。
	 * @return svc-ID's
	 */
	public String[] getSvcIds() {
	    return svcIds;
	}

	/**
	 * svc-ID'sを設定します。
	 * @param svcIds svc-ID's
	 */
	public void setSvcIds(String[] svcIds) {
	    this.svcIds = svcIds;
	    super.append( FuncSvcItems.svcId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(svcIds, FuncSvcItems.svcId.getItemName(), false, true, false) );
	}

	/**
	 * service nameを取得します。
	 * @return service name
	 */
	public String getSvcNm() {
	    return svcNm;
	}

	/**
	 * service nameを設定します。
	 * @param svcNm service-name
	 */
	public void setUserNm(String svcNm) {
	    this.svcNm = svcNm;
	    super.append(FuncSvcItems.svcNm, svcNm );
	}

	/**
	 * is concurrentを取得します。
	 * @return is-concurrent
	 */
	public StatusFlg getIsConcurrent() {
	    return isConcurrent;
	}

	/**
	 * is concurrentを設定します。
	 * @param isConcurrent is-concurrent
	 */
	public void setIsConcurrent(StatusFlg isConcurrent) {
	    this.isConcurrent = isConcurrent;
	    super.append( FuncSvcItems.isConcurrent, (null == isConcurrent)? StatusFlg.off.parseBoolean(): isConcurrent.parseBoolean() );
	}

	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	    super.append( FuncSvcItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}
}