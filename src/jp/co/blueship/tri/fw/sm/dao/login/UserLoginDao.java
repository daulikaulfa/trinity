package jp.co.blueship.tri.fw.sm.dao.login;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;
import jp.co.blueship.tri.fw.sm.dao.constants.SmTables;
import jp.co.blueship.tri.fw.sm.dao.login.constants.UserLoginItems;
import jp.co.blueship.tri.fw.sm.dao.login.eb.IUserLoginEntity;
import jp.co.blueship.tri.fw.sm.dao.login.eb.UserLoginEntity;
import jp.co.blueship.tri.fw.um.dao.user.constants.UserItems;

/**
 * 
 * @version V4.00.00
 * @author le.thixuan
 */
public class UserLoginDao extends JdbcBaseDao<IUserLoginEntity> implements IUserLoginDao {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected ITableAttribute getTableAttribute() {
		return SmTables.SM_USER_LOGIN;
	}

	@Override
	protected ISqlBuilder append(ISqlBuilder builder, IUserLoginEntity entity) {
		builder
			.append(UserLoginItems.userId, entity.getUserId(), true)
			.append(UserLoginItems.lastLoginTimestamp, entity.getLastLoginTimestamp())
			.append(UserLoginItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(UserLoginItems.regTimestamp, entity.getRegTimestamp())
			.append(UserLoginItems.regUserId, entity.getRegUserId())
			.append(UserLoginItems.regUserNm, entity.getRegUserNm())
			.append(UserLoginItems.updTimestamp, entity.getUpdTimestamp())
			.append(UserLoginItems.updUserId, entity.getUpdUserId())
			.append(UserLoginItems.updUserNm, entity.getUpdUserNm());
		return builder;
	}

	@Override
	protected IUserLoginEntity entityMapping(ResultSet rs, int row)
			throws SQLException {
		UserLoginEntity entity = new UserLoginEntity();
		
		entity.setUserId( rs.getString(UserLoginItems.userId.getItemName()) );
		entity.setLastLoginTimestamp( rs.getTimestamp(UserLoginItems.lastLoginTimestamp.getItemName()) );
		entity.setDelStsId( StatusFlg.value(rs.getBoolean(UserItems.delStsId.getItemName())) );
		entity.setRegTimestamp( rs.getTimestamp(UserItems.regTimestamp.getItemName()) );
	  	entity.setRegUserId( rs.getString(UserItems.regUserId.getItemName()) );
	  	entity.setRegUserNm( rs.getString(UserItems.regUserNm.getItemName()) );
	  	entity.setUpdTimestamp( rs.getTimestamp(UserItems.updTimestamp.getItemName()) );
	  	entity.setUpdUserId( rs.getString(UserItems.updUserId.getItemName()) );
	  	entity.setUpdUserNm( rs.getString(UserItems.updUserNm.getItemName()) );
	  	
		return entity;
	}

}
