package jp.co.blueship.tri.fw.sm.dao.plmgt.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of theproduct license management entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IPlMgtEntity extends IEntityFooter {

	public String getProductId();
	public void setProductId(String productId);

	public String getCpNm();
	public void setCpNm(String cpNm);

	public String getSerialNo();
	public void setSerialNo(String serialNo);

	public String getPlKey();
	public void setPlKey(String plKey);

	public String getAgentPlKey();
	public void setAgentPlKey(String agentPlKey);

	public String getRemarks();
	public void setRemarks(String remarks);

	public Timestamp getPlTimeLimit();
	public void setPlTimeLimit(Timestamp plTimeLimit);
}
