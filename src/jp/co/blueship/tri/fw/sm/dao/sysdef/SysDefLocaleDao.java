package jp.co.blueship.tri.fw.sm.dao.sysdef;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;
import jp.co.blueship.tri.fw.sm.dao.constants.SmTables;
import jp.co.blueship.tri.fw.sm.dao.sysdef.constants.SysDefLocaleItems;
import jp.co.blueship.tri.fw.sm.dao.sysdef.eb.ISysDefLocaleEntity;
import jp.co.blueship.tri.fw.sm.dao.sysdef.eb.SysDefLocaleEntity;

/**
 * The implements of the system define locale DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class SysDefLocaleDao extends JdbcBaseDao<ISysDefLocaleEntity> implements ISysDefLocaleDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected final ITableAttribute getTableAttribute() {
		return SmTables.SM_SYS_DEF_LOCALE;
	}

	@Override
	protected final ISqlBuilder append(ISqlBuilder builder, ISysDefLocaleEntity entity) {
		builder.append(SysDefLocaleItems.sysDefId, entity.getSysDefId(), true)
				.append(SysDefLocaleItems.setId, entity.getSetId()).append(SysDefLocaleItems.setKey, entity.getSetKey())
				.append(SysDefLocaleItems.lang, entity.getLang()).append(SysDefLocaleItems.country, entity.getCountry())
				.append(SysDefLocaleItems.setValue, entity.getSetValue())
				.append(SysDefLocaleItems.sortOdr, (null == entity.getSortOdr()) ? 0 : entity.getSortOdr())
				.append(SysDefLocaleItems.delStsId,
						(null == entity.getDelStsId()) ? null : entity.getDelStsId().parseBoolean())
				.append(SysDefLocaleItems.regTimestamp, entity.getRegTimestamp())
				.append(SysDefLocaleItems.regUserId, entity.getRegUserId())
				.append(SysDefLocaleItems.regUserNm, entity.getRegUserNm())
				.append(SysDefLocaleItems.updTimestamp, entity.getUpdTimestamp())
				.append(SysDefLocaleItems.updUserId, entity.getUpdUserId())
				.append(SysDefLocaleItems.updUserNm, entity.getUpdUserNm());

		return builder;
	}

	@Override
	protected final ISysDefLocaleEntity entityMapping(ResultSet rs, int row) throws SQLException {
		ISysDefLocaleEntity entity = new SysDefLocaleEntity();

		entity.setSysDefId(rs.getString(SysDefLocaleItems.sysDefId.getItemName()));
		entity.setSetId(rs.getString(SysDefLocaleItems.setId.getItemName()));
		entity.setSetKey(rs.getString(SysDefLocaleItems.setKey.getItemName()));
		entity.setLang(rs.getString(SysDefLocaleItems.lang.getItemName()));
		entity.setCountry(rs.getString(SysDefLocaleItems.country.getItemName()));
		entity.setSetValue(rs.getString(SysDefLocaleItems.setValue.getItemName()));
		entity.setSortOdr(rs.getInt(SysDefLocaleItems.sortOdr.getItemName()));
		entity.setDelStsId(StatusFlg.value(rs.getBoolean(SysDefLocaleItems.delStsId.getItemName())));
		entity.setRegTimestamp(rs.getTimestamp(SysDefLocaleItems.regTimestamp.getItemName()));
		entity.setRegUserId(rs.getString(SysDefLocaleItems.regUserId.getItemName()));
		entity.setRegUserNm(rs.getString(SysDefLocaleItems.regUserNm.getItemName()));
		entity.setUpdTimestamp(rs.getTimestamp(SysDefLocaleItems.updTimestamp.getItemName()));
		entity.setUpdUserId(rs.getString(SysDefLocaleItems.updUserId.getItemName()));
		entity.setUpdUserNm(rs.getString(SysDefLocaleItems.updUserNm.getItemName()));

		return entity;
	}

}
