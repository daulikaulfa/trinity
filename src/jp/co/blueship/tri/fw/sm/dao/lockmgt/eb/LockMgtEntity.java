package jp.co.blueship.tri.fw.sm.dao.lockmgt.eb;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.EntityFooter;

/**
 * lock management entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class LockMgtEntity extends EntityFooter implements ILockMgtEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * proc-ID
	 */
	public String procId = null;
	/**
	 * lock svc-ID
	 */
	public String lockSvcId = null;
	/**
	 * svc-ID
	 */
	public String svcId = null;
	/**
	 * user-ID
	 */
	public String userId = null;
	/**
	 * lot-ID
	 */
	public String lotId = null;
	/**
	 * lock for lot
	 */
	public StatusFlg lockForLot = StatusFlg.off;

	/**
	 * proc-IDを取得します。
	 * @return proc-ID
	 */
	public String getProcId() {
	    return procId;
	}
	/**
	 * proc-IDを設定します。
	 * @param procId proc-ID
	 */
	public void setProcId(String procId) {
	    this.procId = procId;
	}
	/**
	 * lock svc-IDを取得します。
	 * @return lock svc-ID
	 */
	public String getLockSvcId() {
	    return lockSvcId;
	}
	/**
	 * lock svc-IDを設定します。
	 * @param lockSvcId lock svc-ID
	 */
	public void setLockSvcId(String lockSvcId) {
	    this.lockSvcId = lockSvcId;
	}
	/**
	 * svc-IDを取得します。
	 * @return svc-ID
	 */
	public String getSvcId() {
	    return svcId;
	}
	/**
	 * svc-IDを設定します。
	 * @param svcId svc-ID
	 */
	public void setSvcId(String svcId) {
	    this.svcId = svcId;
	}
	/**
	 * user-IDを取得します。
	 * @return user-ID
	 */
	public String getUserId() {
	    return userId;
	}
	/**
	 * user-IDを設定します。
	 * @param userId user-ID
	 */
	public void setUserId(String userId) {
	    this.userId = userId;
	}
	/**
	 * lot-IDを取得します。
	 * @return lot-ID
	 */
	public String getLotId() {
	    return lotId;
	}
	/**
	 * lot-IDを設定します。
	 * @param lotId lot-ID
	 */
	public void setLotId(String lotId) {
	    this.lotId = lotId;
	}
	/**
	 * lock for lotを取得します。
	 * @return lock for lot
	 */
	public StatusFlg getLockForLot() {
	    return lockForLot;
	}
	/**
	 * lock for lotを設定します。
	 * @param lockForLot lock for lot
	 */
	public void setLockForLot(StatusFlg lockForLot) {
	    this.lockForLot = lockForLot;
	}

}