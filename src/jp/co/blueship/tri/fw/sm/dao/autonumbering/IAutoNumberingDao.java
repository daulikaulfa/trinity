package jp.co.blueship.tri.fw.sm.dao.autonumbering;

import jp.co.blueship.tri.fw.dao.orm.IDao;


/**
 * 自動採番を行うためのアクセスインタフェースです。
 *
 */
public interface IAutoNumberingDao extends IDao {



	/**
	 * 自動採番します。
	 * <br>指定されたパターン文字列でフォーマットした採番IDを取得します。
	 * <br>ex.) 4桁でゼロサプレスする場合、"0000"を指定
	 *
	 * @param numberingKey 採番キー
	 * @param pattern パターン文字列
	 * @return 採番された値を戻します
	 */

	public String nextval( String numberingKey, String pattern );

	/**
	 * 自動採番します。指定された採番年月日で採番します。
	 * <br>指定されたパターン文字列でフォーマットした採番IDを取得します。
	 * <br>ex.) 4桁でゼロサプレスする場合、"0000"を指定
	 *
	 * @param numberingKey 採番キー
	 * @param numberingDate 採番年月日
	 * @param pattern パターン文字列
	 * @return 採番された値を戻します
	 */
	public String nextval( String numberingKey, String numberingDate, String pattern );

}
