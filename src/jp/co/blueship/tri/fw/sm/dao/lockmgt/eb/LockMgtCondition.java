package jp.co.blueship.tri.fw.sm.dao.lockmgt.eb;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.dao.orm.psql.SqlFormatUtils;
import jp.co.blueship.tri.fw.sm.dao.constants.SmTables;
import jp.co.blueship.tri.fw.sm.dao.lockmgt.constants.LockMgtItems;

/**
 * The SQL condition of the lock management entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class LockMgtCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute attr = SmTables.SM_LOCK_MGT;

	/**
	 * proc-ID
	 */
	public String procId = null;
	/**
	 * proc-ID's
	 */
	public String[] procIds = null;
	/**
	 * lock-svc-ID
	 */
	public String lockSvcId = null;
	/**
	 * lock-svc-ID's
	 */
	public String[] lockSvcIds = null;
	/**
	 * svc-ID
	 */
	public String svcId = null;
	/**
	 * user-ID
	 */
	public String userId = null;
	/**
	 * lot-ID
	 */
	public String lotId = null;
	/**
	 * lock for lot
	 */
	public StatusFlg lockForLot = StatusFlg.off;
	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		   super.append(LockMgtItems.delStsId, StatusFlg.off.parseBoolean() );
	}

	public LockMgtCondition(){
		super(attr);
	}

	/**
	 * proc-IDを取得します。
	 * @return proc-ID
	 */
	public String getProcId() {
	    return procId;
	}

	/**
	 * proc-IDを設定します。
	 * @param procId proc-ID
	 */
	public void setProcId(String procId) {
	    this.procId = procId;
	    super.append(LockMgtItems.procId, procId );
	}

	/**
	 * proc-ID'sを取得します。
	 * @return proc-ID's
	 */
	public String[] getProcIds() {
	    return procIds;
	}

	/**
	 * proc-ID'sを設定します。
	 * @param procIds proc-ID's
	 */
	public void setProcIds(String[] procIds) {
	    this.procIds = procIds;
	    super.append( LockMgtItems.procId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(procIds, LockMgtItems.procId.getItemName(), false, true, false) );
	}

	/**
	 * lock-svc-IDを取得します。
	 * @return lock-svc-ID
	 */
	public String getLockSvcId() {
	    return lockSvcId;
	}

	/**
	 * lock-svc-IDを設定します。
	 * @param lockSvcId lock-svc-ID
	 */
	public void setLockSvcId(String lockSvcId) {
	    this.lockSvcId = lockSvcId;
	    super.append(LockMgtItems.lockSvcId, lockSvcId );
	}

	/**
	 * lock-svc-ID'sを取得します。
	 * @return lock-svc-ID's
	 */
	public String[] getLockSvcIds() {
	    return lockSvcIds;
	}

	/**
	 * lock-svc-ID'sを設定します。
	 * @param lockSvcIds lock-svc-ID's
	 */
	public void setLockSvcIds(String[] lockSvcIds) {
	    this.lockSvcIds = lockSvcIds;
	    super.append( LockMgtItems.lockSvcId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(lockSvcIds, LockMgtItems.lockSvcId.getItemName(), false, true, false) );
	}

	/**
	 * svc-IDを取得します。
	 * @return svc-ID
	 */
	public String getSvcId() {
	    return svcId;
	}

	/**
	 * svc-IDを設定します。
	 * @param svcId svc-ID
	 */
	public void setSvcId(String svcId) {
	    this.svcId = svcId;
	    super.append(LockMgtItems.svcId, svcId );
	}

	/**
	 * user-IDを取得します。
	 * @return user-ID
	 */
	public String getUserId() {
	    return userId;
	}

	/**
	 * user-IDを設定します。
	 * @param userId user-ID
	 */
	public void setUserId(String userId) {
	    this.userId = userId;
	    super.append(LockMgtItems.userId, userId );
	}

	/**
	 * lot-IDを取得します。
	 * @return lot-ID
	 */
	public String getLotId() {
	    return lotId;
	}

	/**
	 * lot-IDを設定します。
	 * @param lotId lot-ID
	 */
	public void setLotId(String lotId) {
	    this.lotId = lotId;
	    super.append(LockMgtItems.lotId, lotId );
	}

	/**
	 * lock for lotを取得します。
	 * @return lock for lot
	 */
	public StatusFlg getLockForLot() {
	    return lockForLot;
	}

	/**
	 * lock for lotを設定します。
	 * @param lockForLot lock for lot
	 */
	public void setLockForLot(StatusFlg lockForLot) {
	    this.lockForLot = lockForLot;
	    super.append( LockMgtItems.lockForLot, (null == lockForLot)? StatusFlg.off.parseBoolean(): lockForLot.parseBoolean() );
	}

	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	    super.append( LockMgtItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}
}