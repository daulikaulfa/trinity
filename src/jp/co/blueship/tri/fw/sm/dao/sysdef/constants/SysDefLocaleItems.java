package jp.co.blueship.tri.fw.sm.dao.sysdef.constants;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;


/**
 * The items of the system define locale entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public enum SysDefLocaleItems implements ITableItem {
	sysDefId("sys_def_id"),
	setId("set_id"),
	setKey("set_key"),
	lang("lang"),
	country("country"),
	setValue("set_value"),
	sortOdr("sort_odr"),
	delStsId("del_sts_id"),
	regUserId("reg_user_id"),
	regUserNm("reg_user_nm"),
	regTimestamp("reg_timestamp"),
	updUserId("upd_user_id"),
	updUserNm("upd_user_nm"),
	updTimestamp("upd_timestamp");

	private String element = null;

	private SysDefLocaleItems( String element) {
		this.element = element;
	}

	public String getItemName() {
		return this.element;
	}

}
