package jp.co.blueship.tri.fw.sm.dao.procmgt.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.dao.orm.psql.SqlFormatUtils;
import jp.co.blueship.tri.fw.sm.dao.constants.SmTables;
import jp.co.blueship.tri.fw.sm.dao.procmgt.constants.ProcMgtItems;

/**
 * The SQL condition of the process management entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class ProcMgtCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute attr = SmTables.SM_PROC_MGT;

	/**
	 * proc-ID
	 */
	public String procId = null;
	/**
	 * proc-ID's
	 */
	public String[] procIds = null;
	/**
	 * svc-ID
	 */
	public String svcId = null;
	/**
	 * lot-ID
	 */
	public String lotId = null;
	/**
	 * user-ID
	 */
	public String userId = null;
	/**
	 * user name
	 */
	public String userNm = null;
	/**
	 * process start time stamp
	 */
	public Timestamp procStTimestamp = null;
	/**
	 * process end time stamp
	 */
	public Timestamp procEndTimestamp = null;
	/**
	 * service-ID
	 */
	public String serviceId = null;
	/**
	 * service-IDs
	 */
	public String[] serviceIds = null;
	/**
	 * sts-ID
	 */
	public String stsId = null;
	/**
	 * complete status ID
	 */
	public String[] stsIds = null;
	/**
	 * complete status ID
	 */
	public StatusFlg compStsId = StatusFlg.off;
	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		   super.append(ProcMgtItems.delStsId, StatusFlg.off.parseBoolean() );
	}

	public ProcMgtCondition(){
		super(attr);
	}

	/**
	 * proc-IDを取得します。
	 * @return proc-ID
	 */
	public String getProcId() {
	    return procId;
	}

	/**
	 * proc-IDを設定します。
	 * @param procId proc-ID
	 */
	public void setProcId(String procId) {
	    this.procId = procId;
	    super.append(ProcMgtItems.procId, procId );
	}

	/**
	 * proc-ID'sを取得します。
	 * @return proc-ID's
	 */
	public String[] getProcIds() {
	    return procIds;
	}

	/**
	 * proc-ID'sを設定します。
	 * @param procIds proc-ID's
	 */
	public void setProcIds(String[] procIds) {
	    this.procIds = procIds;
	    super.append( ProcMgtItems.procId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(procIds, ProcMgtItems.procId.getItemName(), false, true, false) );
	}

	/**
	 * svc-IDを取得します。
	 * @return svc-ID
	 */
	public String getSvcId() {
	    return svcId;
	}

	/**
	 * svc-IDを設定します。
	 * @param svcId svc-ID
	 */
	public void setSvcId(String svcId) {
	    this.svcId = svcId;
	    super.append(ProcMgtItems.svcId, svcId );
	}

	/**
	 * lot-IDを取得します。
	 * @return lot-ID
	 */
	public String getLotId() {
	    return lotId;
	}

	/**
	 * lot-IDを設定します。
	 * @param lotId lot-ID
	 */
	public void setLotId(String lotId) {
	    this.lotId = lotId;
	    super.append(ProcMgtItems.lotId, lotId );
	}

	/**
	 * user-IDを取得します。
	 * @return user-ID
	 */
	public String getUserId() {
	    return userId;
	}

	/**
	 * user-IDを設定します。
	 * @param userId user-ID
	 */
	public void setUserId(String userId) {
	    this.userId = userId;
	    super.append(ProcMgtItems.userId, userId );
	}

	/**
	 * user nameを取得します。
	 * @return user name
	 */
	public String getUserNm() {
	    return userNm;
	}

	/**
	 * user nameを設定します。
	 * @param userNm user name
	 */
	public void setUserNm(String userNm) {
	    this.userNm = userNm;
	    super.append(ProcMgtItems.userNm, userNm );
	}

	/**
	 * process start time stampを取得します。
	 * @return process start time stamp
	 */
	public Timestamp getProcStTimestamp() {
	    return procStTimestamp;
	}

	/**
	 * process start time stampを設定します。
	 * @param procStTimestamp process start time stamp
	 */
	public void setProcStTimestamp(Timestamp procStTimestamp) {
	    this.procStTimestamp = procStTimestamp;
	    super.append(ProcMgtItems.procStTimestamp, procStTimestamp );
	}

	/**
	 * process end time stampを取得します。
	 * @return process end time stamp
	 */
	public Timestamp getProcEndTimestamp() {
	    return procEndTimestamp;
	}

	/**
	 * process end time stampを設定します。
	 * @param procEndTimestamp process end time stamp
	 */
	public void setProcEndTimestamp(Timestamp procEndTimestamp) {
	    this.procEndTimestamp = procEndTimestamp;
	    super.append(ProcMgtItems.procEndTimestamp, procEndTimestamp );
	}

	public String getServiceId(){
		return this.serviceId;
	}

	public void setServiceId(String serviceId){
		this.serviceId = serviceId;
		super.append(ProcMgtItems.svcId, serviceId );
	}

	public String[] getServiceIds(){
		return this.serviceIds;
	}

	public void setServiceIds(String[] serviceIds){
		this.serviceIds = serviceIds;
	    super.append( ProcMgtItems.svcId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(serviceIds, ProcMgtItems.svcId.getItemName(), false, true, false) );
	}

	/**
	 * sts-IDを取得します。
	 * @return sts-ID
	 */
	public String getStsId() {
	    return stsId;
	}

	/**
	 * sts-IDを設定します。
	 * @param stsId sts-ID
	 */
	public void setStsId(String stsId) {
	    this.stsId = stsId;
	    super.append(ProcMgtItems.stsId, stsId );
	}

	/**
	 * sts-ID'sを取得します。
	 * @return sts-ID's
	 */
	public String[] getStsIds() {
	    return stsIds;
	}

	/**
	 * sts-ID'sを設定します。
	 * @param stsId sts-ID's
	 */
	public void setStsIds(String[] stsIds) {
	    this.stsIds = stsIds;
	    super.append( ProcMgtItems.stsId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(stsIds, ProcMgtItems.stsId.getItemName(), false, true, false) );
	}


	/**
	 * complete status IDを取得します。
	 * @return complete status ID
	 */
	public StatusFlg getCompStsId() {
	    return compStsId;
	}

	/**
	 * complete status IDを設定します。
	 * @param compStsId complete status ID
	 */
	public void setCompStsId(StatusFlg compStsId) {
	    this.compStsId = compStsId;
	    super.append( ProcMgtItems.compStsId, (null == compStsId)? null: compStsId.parseBoolean() );
	}

	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	    super.append( ProcMgtItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}

	/**
	 * FromToで検索範囲を設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setFromToUpdTimestamp(Timestamp from, Timestamp to ){

		super.append(ProcMgtItems.updTimestamp.getItemName() ,
				SqlFormatUtils.getFromTo(from, to, ProcMgtItems.updTimestamp.getItemName()));
	}
}