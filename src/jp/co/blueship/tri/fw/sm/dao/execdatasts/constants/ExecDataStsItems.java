package jp.co.blueship.tri.fw.sm.dao.execdatasts.constants;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;


/**
 * The items of the execute data status entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public enum ExecDataStsItems implements ITableItem {
	procId("proc_id"),
	dataCtgCd("data_ctg_cd"),
	dataId("data_id"),
	stsId("sts_id"),
	/**
	 * process status-id(read only)
	 */
	procStsId("proc_sts_id"),
	delStsId("del_sts_id"),
	regUserId("reg_user_id"),
	regUserNm("reg_user_nm"),
	regTimestamp("reg_timestamp"),
	updUserId("upd_user_id"),
	updUserNm("upd_user_nm"),
	updTimestamp("upd_timestamp");

	private String element = null;

	private ExecDataStsItems( String element) {
		this.element = element;
	}

	public String getItemName() {
		return this.element;
	}

}
