package jp.co.blueship.tri.fw.sm.dao.procmgt;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;
import jp.co.blueship.tri.fw.sm.dao.constants.SmTables;
import jp.co.blueship.tri.fw.sm.dao.procmgt.constants.ProcDetailsItems;
import jp.co.blueship.tri.fw.sm.dao.procmgt.eb.IProcDetailsEntity;
import jp.co.blueship.tri.fw.sm.dao.procmgt.eb.ProcDetailsEntity;

/**
 * The implements of the process details DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 */
public class ProcDetailsDao extends JdbcBaseDao<IProcDetailsEntity> implements IProcDetailsDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 2L;

	@Override
	protected final ITableAttribute getTableAttribute() {
		return SmTables.SM_PROC_DETAILS;
	}

	@Override
	protected final ISqlBuilder append( ISqlBuilder builder, IProcDetailsEntity entity ) {
		builder
			.append(ProcDetailsItems.procId, entity.getProcId(), true)
			.append(ProcDetailsItems.procCtgCd, entity.getProcCtgCd(), true)
			.append(ProcDetailsItems.procStTimestamp, entity.getProcStTimestamp())
			.append(ProcDetailsItems.procEndTimestamp, entity.getProcEndTimestamp())
			.append(ProcDetailsItems.stsId, entity.getStsId())
			.append(ProcDetailsItems.msgId, entity.getMsgId())
			.append(ProcDetailsItems.msg, entity.getMsg())
			.append(ProcDetailsItems.compStsId, (null == entity.getCompStsId())? null: entity.getCompStsId().parseBoolean())
			.append(ProcDetailsItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(ProcDetailsItems.regTimestamp, entity.getRegTimestamp())
			.append(ProcDetailsItems.regUserId, entity.getRegUserId())
			.append(ProcDetailsItems.regUserNm, entity.getRegUserNm())
			.append(ProcDetailsItems.updTimestamp, entity.getUpdTimestamp())
			.append(ProcDetailsItems.updUserId, entity.getUpdUserId())
			.append(ProcDetailsItems.updUserNm, entity.getUpdUserNm())
			;

		return builder;
	}

	@Override
	protected final IProcDetailsEntity entityMapping( ResultSet rs, int row ) throws SQLException {
  	  IProcDetailsEntity entity = new ProcDetailsEntity();

  	  entity.setProcId( rs.getString(ProcDetailsItems.procId.getItemName()) );
  	  entity.setProcCtgCd( rs.getString(ProcDetailsItems.procCtgCd.getItemName()) );
  	  entity.setProcStTimestamp( rs.getTimestamp(ProcDetailsItems.procStTimestamp.getItemName()) );
  	  entity.setProcEndTimestamp( rs.getTimestamp(ProcDetailsItems.procEndTimestamp.getItemName()) );
  	  entity.setStsId( rs.getString(ProcDetailsItems.stsId.getItemName()) );
  	  entity.setMsgId( rs.getString(ProcDetailsItems.msgId.getItemName()) );
  	  entity.setMsg( rs.getString(ProcDetailsItems.msg.getItemName()) );
  	  entity.setCompStsId( StatusFlg.value(rs.getBoolean(ProcDetailsItems.compStsId.getItemName())) );
  	  entity.setDelStsId( StatusFlg.value(rs.getBoolean(ProcDetailsItems.delStsId.getItemName())) );
  	  entity.setRegTimestamp( rs.getTimestamp(ProcDetailsItems.regTimestamp.getItemName()) );
  	  entity.setRegUserId( rs.getString(ProcDetailsItems.regUserId.getItemName()) );
  	  entity.setRegUserNm( rs.getString(ProcDetailsItems.regUserNm.getItemName()) );
  	  entity.setUpdTimestamp( rs.getTimestamp(ProcDetailsItems.updTimestamp.getItemName()) );
  	  entity.setUpdUserId( rs.getString(ProcDetailsItems.updUserId.getItemName()) );
  	  entity.setUpdUserNm( rs.getString(ProcDetailsItems.updUserNm.getItemName()) );

  	  return entity;
	}

}
