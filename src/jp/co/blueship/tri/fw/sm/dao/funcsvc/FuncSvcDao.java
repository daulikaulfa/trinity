package jp.co.blueship.tri.fw.sm.dao.funcsvc;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;
import jp.co.blueship.tri.fw.sm.dao.constants.SmTables;
import jp.co.blueship.tri.fw.sm.dao.funcsvc.constants.FuncSvcItems;
import jp.co.blueship.tri.fw.sm.dao.funcsvc.eb.FuncSvcEntity;
import jp.co.blueship.tri.fw.sm.dao.funcsvc.eb.IFuncSvcEntity;

/**
 * The implements of the function service DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 */
public class FuncSvcDao extends JdbcBaseDao<IFuncSvcEntity> implements IFuncSvcDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected final ITableAttribute getTableAttribute() {
		return SmTables.SM_FUNC_SVC;
	}

	@Override
	protected final ISqlBuilder append( ISqlBuilder builder, IFuncSvcEntity entity ) {
		builder
			.append(FuncSvcItems.svcId, entity.getSvcId(), true)
			.append(FuncSvcItems.svcNm, entity.getSvcNm())
			.append(FuncSvcItems.svcContent, entity.getSvcContent())
			.append(FuncSvcItems.isConcurrent, (null == entity.getIsConcurrent())? StatusFlg.off.parseBoolean(): entity.getIsConcurrent().parseBoolean())
			.append(FuncSvcItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(FuncSvcItems.regTimestamp, entity.getRegTimestamp())
			.append(FuncSvcItems.regUserId, entity.getRegUserId())
			.append(FuncSvcItems.regUserNm, entity.getRegUserNm())
			.append(FuncSvcItems.updTimestamp, entity.getUpdTimestamp())
			.append(FuncSvcItems.updUserId, entity.getUpdUserId())
			.append(FuncSvcItems.updUserNm, entity.getUpdUserNm())
			;

		return builder;
	}

	@Override
	protected final IFuncSvcEntity entityMapping( ResultSet rs, int row ) throws SQLException {
  	  IFuncSvcEntity entity = new FuncSvcEntity();

  	  entity.setSvcId( rs.getString(FuncSvcItems.svcId.getItemName()) );
  	  entity.setSvcNm( rs.getString(FuncSvcItems.svcNm.getItemName()) );
  	  entity.setSvcContent( rs.getString(FuncSvcItems.svcContent.getItemName()) );
  	  entity.setIsConcurrent( StatusFlg.value(rs.getBoolean(FuncSvcItems.isConcurrent.getItemName())) );
  	  entity.setDelStsId( StatusFlg.value(rs.getBoolean(FuncSvcItems.delStsId.getItemName())) );
  	  entity.setRegTimestamp( rs.getTimestamp(FuncSvcItems.regTimestamp.getItemName()) );
  	  entity.setRegUserId( rs.getString(FuncSvcItems.regUserId.getItemName()) );
  	  entity.setRegUserNm( rs.getString(FuncSvcItems.regUserNm.getItemName()) );
  	  entity.setUpdTimestamp( rs.getTimestamp(FuncSvcItems.updTimestamp.getItemName()) );
  	  entity.setUpdUserId( rs.getString(FuncSvcItems.updUserId.getItemName()) );
  	  entity.setUpdUserNm( rs.getString(FuncSvcItems.updUserNm.getItemName()) );

  	  return entity;
	}

}
