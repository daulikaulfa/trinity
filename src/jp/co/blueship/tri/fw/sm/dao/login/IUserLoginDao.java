package jp.co.blueship.tri.fw.sm.dao.login;

import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;
import jp.co.blueship.tri.fw.sm.dao.login.eb.IUserLoginEntity;

/**
 * 
 * @version V4.00.00
 * @author le.thixuan
 */
public interface IUserLoginDao extends IJdbcDao<IUserLoginEntity> {

}
