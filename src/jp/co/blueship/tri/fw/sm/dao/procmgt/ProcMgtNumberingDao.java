package jp.co.blueship.tri.fw.sm.dao.procmgt;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

import jp.co.blueship.tri.fw.dao.orm.INumberingDao;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.ex.TriJdbcDaoException;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.sm.dao.constants.SmTables;

/**
 * The implements of the Process Management DAO.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 */
public class ProcMgtNumberingDao extends JdbcBaseDao<String> implements INumberingDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	private final String SEQ_NAME = "SM_PROC_ID_SEQ";
	private String SQL_SELECT_QUERY = "select (to_char(now(), 'YYMMDDHH24MISS') || ltrim(to_char(nextval('" + SEQ_NAME + "'), '00' ))) as nextval";

	public String nextval() {
		try {
			List<?> rows = super.query(
					SQL_SELECT_QUERY,
					new ParameterizedRowMapper<String>() {
						@Override
						public String mapRow(ResultSet resultset, int i) throws SQLException {
							return entityMapping( resultset, i );
						}
					});

			return (String)rows.get(0);
		} catch ( Exception e ) {
			throw new TriJdbcDaoException( SmMessageId.SM005008S, e, SEQ_NAME );
		}
	}

	@Override
	protected final String entityMapping( ResultSet rs, int row ) throws SQLException {
		return rs.getString("nextval");
	}

	@Override
	protected final ITableAttribute getTableAttribute() {
		return SmTables.SM_PROC_MGT;
	}

	@Override
	protected ISqlBuilder append(ISqlBuilder builder, String entity) {
		return builder;
	}

}
