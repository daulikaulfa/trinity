package jp.co.blueship.tri.fw.sm.dao.passmgt.eb;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.sm.dao.constants.SmTables;
import jp.co.blueship.tri.fw.sm.dao.passmgt.constants.PassMgtItems;

/**
 * The SQL condition of the password management entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class PassMgtCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute attr = SmTables.SM_PASS_MGT;

	/**
	 * password catalog code
	 */
	public String passCtgCd = null;
	/**
	 * host name
	 */
	public String hostNm = null;
	/**
	 * user-ID
	 */
	public String userId = null;
	/**
	 * encode password
	 */
	public String epass = null;
	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		   super.append(PassMgtItems.delStsId, StatusFlg.off.parseBoolean() );
	}

	public PassMgtCondition(){
		super(attr);
	}

	/**
	 * password-catalog-codeを取得します。
	 * @return password-catalog-code
	 */
	public String getPassCtgCd() {
	    return passCtgCd;
	}

	/**
	 * password-catalog-codeを設定します。
	 * @param passCtgCd password-catalog-code
	 */
	public void setPassCtgCd(String passCtgCd) {
	    this.passCtgCd = passCtgCd;
	    super.append(PassMgtItems.passCtgCd, passCtgCd );
	}

	/**
	 * host-nameを取得します。
	 * @return host-name
	 */
	public String getHostNm() {
	    return hostNm;
	}

	/**
	 * host-nameを設定します。
	 * @param hostNm host-name
	 */
	public void setHostNm(String hostNm) {
	    this.hostNm = hostNm;
	    super.append(PassMgtItems.hostNm, hostNm );
	}

	/**
	 * user-IDを取得します。
	 * @return user-ID
	 */
	public String getUserId() {
	    return userId;
	}

	/**
	 * user-IDを設定します。
	 * @param userId user-ID
	 */
	public void setUserId(String userId) {
	    this.userId = userId;
	    super.append(PassMgtItems.userId, userId );
	}

	/**
	 * encode-passwordを取得します。
	 * @return encode-password
	 */
	public String getEpass() {
	    return epass;
	}

	/**
	 * encode-passwordを設定します。
	 * @param epass encode-password
	 */
	public void setLotId(String epass) {
	    this.epass = epass;
	    super.append(PassMgtItems.epass, epass );
	}

	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	    super.append( PassMgtItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}
}