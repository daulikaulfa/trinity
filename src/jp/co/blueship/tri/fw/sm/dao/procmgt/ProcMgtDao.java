package jp.co.blueship.tri.fw.sm.dao.procmgt;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;
import jp.co.blueship.tri.fw.sm.dao.constants.SmTables;
import jp.co.blueship.tri.fw.sm.dao.procmgt.constants.ProcMgtItems;
import jp.co.blueship.tri.fw.sm.dao.procmgt.eb.IProcMgtEntity;
import jp.co.blueship.tri.fw.sm.dao.procmgt.eb.ProcMgtEntity;

/**
 * The implements of the process management DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 */
public class ProcMgtDao extends JdbcBaseDao<IProcMgtEntity> implements IProcMgtDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected final ITableAttribute getTableAttribute() {
		return SmTables.SM_PROC_MGT;
	}

	@Override
	protected final ISqlBuilder append( ISqlBuilder builder, IProcMgtEntity entity ) {
		builder
			.append(ProcMgtItems.procId, entity.getProcId(), true)
			.append(ProcMgtItems.svcId, entity.getSvcId())
			.append(ProcMgtItems.lotId, entity.getLotId())
			.append(ProcMgtItems.userId, entity.getUserId())
			.append(ProcMgtItems.userNm, entity.getUserNm())
			.append(ProcMgtItems.procStTimestamp, entity.getProcStTimestamp() )
			.append(ProcMgtItems.procEndTimestamp, entity.getProcEndTimestamp())
			.append(ProcMgtItems.stsId, entity.getStsId())
			.append(ProcMgtItems.compStsId, (null == entity.getCompStsId())? null: entity.getCompStsId().parseBoolean())
			.append(ProcMgtItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(ProcMgtItems.regTimestamp, entity.getRegTimestamp())
			.append(ProcMgtItems.regUserId, entity.getRegUserId())
			.append(ProcMgtItems.regUserNm, entity.getRegUserNm())
			.append(ProcMgtItems.updTimestamp, entity.getUpdTimestamp())
			.append(ProcMgtItems.updUserId, entity.getUpdUserId())
			.append(ProcMgtItems.updUserNm, entity.getUpdUserNm())
			;

		return builder;
	}

	@Override
	protected final IProcMgtEntity entityMapping( ResultSet rs, int row ) throws SQLException {
  	  IProcMgtEntity entity = new ProcMgtEntity();

  	  entity.setProcId( rs.getString(ProcMgtItems.procId.getItemName()) );
  	  entity.setSvcId( rs.getString(ProcMgtItems.svcId.getItemName()) );
  	  entity.setLotId( rs.getString(ProcMgtItems.lotId.getItemName()) );
  	  entity.setUserId( rs.getString(ProcMgtItems.userId.getItemName()) );
  	  entity.setUserNm( rs.getString(ProcMgtItems.userNm.getItemName()) );
  	  entity.setProcStTimestamp( rs.getTimestamp(ProcMgtItems.procStTimestamp.getItemName()) );
  	  entity.setProcEndTimestamp( rs.getTimestamp(ProcMgtItems.procEndTimestamp.getItemName()) );
  	  entity.setStsId( rs.getString(ProcMgtItems.stsId.getItemName()) );
  	  entity.setCompStsId( StatusFlg.value(rs.getBoolean(ProcMgtItems.compStsId.getItemName())) );
  	  entity.setDelStsId( StatusFlg.value(rs.getBoolean(ProcMgtItems.delStsId.getItemName())) );
  	  entity.setRegTimestamp( rs.getTimestamp(ProcMgtItems.regTimestamp.getItemName()) );
  	  entity.setRegUserId( rs.getString(ProcMgtItems.regUserId.getItemName()) );
  	  entity.setRegUserNm( rs.getString(ProcMgtItems.regUserNm.getItemName()) );
  	  entity.setUpdTimestamp( rs.getTimestamp(ProcMgtItems.updTimestamp.getItemName()) );
  	  entity.setUpdUserId( rs.getString(ProcMgtItems.updUserId.getItemName()) );
  	  entity.setUpdUserNm( rs.getString(ProcMgtItems.updUserNm.getItemName()) );

  	  return entity;
	}

}
