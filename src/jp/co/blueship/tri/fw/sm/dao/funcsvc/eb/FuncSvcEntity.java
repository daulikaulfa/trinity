package jp.co.blueship.tri.fw.sm.dao.funcsvc.eb;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.EntityFooter;

/**
 * function service entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class FuncSvcEntity extends EntityFooter implements IFuncSvcEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * svc-ID
	 */
	public String svcId = null;
	/**
	 * service name
	 */
	public String svcNm = null;
	/**
	 * service content
	 */
	public String svcContent = null;
	/**
	 * is concurrent
	 */
	public StatusFlg isConcurrent = StatusFlg.off;


	/**
	 * svc-IDを取得します。
	 * @return svc-ID
	 */
	public String getSvcId() {
	    return svcId;
	}
	/**
	 * svc-IDを設定します。
	 * @param svcId svc-ID
	 */
	public void setSvcId(String svcId) {
	    this.svcId = svcId;
	}
	/**
	 * service nameを取得します。
	 * @return service name
	 */
	public String getSvcNm() {
	    return svcNm;
	}
	/**
	 * service nameを設定します。
	 * @param svcNm service name
	 */
	public void setSvcNm(String svcNm) {
	    this.svcNm = svcNm;
	}
	/**
	 * service contentを取得します。
	 * @return service content
	 */
	public String getSvcContent() {
	    return svcContent;
	}
	/**
	 * service contentを設定します。
	 * @param svcContent service content
	 */
	public void setSvcContent(String svcContent) {
	    this.svcContent = svcContent;
	}
	/**
	 * is concurrentを取得します。
	 * @return is concurrent
	 */
	public StatusFlg getIsConcurrent() {
	    return isConcurrent;
	}
	/**
	 * is concurrentを設定します。
	 * @param isConcurrent is concurrent
	 */
	public void setIsConcurrent(StatusFlg isConcurrent) {
	    this.isConcurrent = isConcurrent;
	}

}