package jp.co.blueship.tri.fw.sm.dao.login.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.fw.dao.orm.EntityFooter;

/**
 * 
 * @version V4.00.00
 * @author le.thixuan
 */
public class UserLoginEntity extends EntityFooter implements IUserLoginEntity {
	
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;
	
	private String userId = null;
	private Timestamp lastLoginTimestamp = null;
	
	public String getUserId() {
		return userId;
	}
	
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public Timestamp getLastLoginTimestamp() {
		return lastLoginTimestamp;
	}
	
	public void setLastLoginTimestamp(Timestamp lastLoginTimestamp) {
		this.lastLoginTimestamp = lastLoginTimestamp;
	}
}
