package jp.co.blueship.tri.fw.sm.dao.procmgt.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.EntityFooter;

/**
 * process details entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class ProcDetailsEntity extends EntityFooter implements IProcDetailsEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * proc-ID
	 */
	public String procId = null;
	/**
	 * process catalog code
	 */
	public String procCtgCd = null;
	/**
	 * process start time stamp
	 */
	public Timestamp procStTimestamp = null;
	/**
	 * process end time stamp
	 */
	public Timestamp procEndTimestamp = null;
	/**
	 * sts-ID
	 */
	public String stsId = null;
	/**
	 *  msg-ID
	 */
	public String msgId = null;
	/**
	 *  message
	 */
	public String msg = null;
	/**
	 * complete status ID
	 */
	public StatusFlg compStsId = StatusFlg.off;

	/**
	 * proc-IDを取得します。
	 * @return proc-ID
	 */
	public String getProcId() {
	    return procId;
	}
	/**
	 * proc-IDを設定します。
	 * @param procId proc-ID
	 */
	public void setProcId(String procId) {
	    this.procId = procId;
	}
	/**
	 * process catalog codeを取得します。
	 * @return process catalog code
	 */
	public String getProcCtgCd() {
	    return procCtgCd;
	}
	/**
	 * process catalog codeを設定します。
	 * @param procCtgCd process catalog code
	 */
	public void setProcCtgCd(String procCtgCd) {
	    this.procCtgCd = procCtgCd;
	}
	/**
	 * process start time stampを取得します。
	 * @return process start time stamp
	 */
	public Timestamp getProcStTimestamp() {
	    return procStTimestamp;
	}
	/**
	 * process start time stampを設定します。
	 * @param procStTimestamp process start time stamp
	 */
	public void setProcStTimestamp(Timestamp procStTimestamp) {
	    this.procStTimestamp = procStTimestamp;
	}
	/**
	 * process end time stampを取得します。
	 * @return process end time stamp
	 */
	public Timestamp getProcEndTimestamp() {
	    return procEndTimestamp;
	}
	/**
	 * process end time stampを設定します。
	 * @param procEndTimestamp process end time stamp
	 */
	public void setProcEndTimestamp(Timestamp procEndTimestamp) {
	    this.procEndTimestamp = procEndTimestamp;
	}
	/**
	 * sts-IDを取得します。
	 * @return sts-ID
	 */
	public String getStsId() {
	    return stsId;
	}
	/**
	 * sts-IDを設定します。
	 * @param stsId sts-ID
	 */
	public void setStsId(String stsId) {
	    this.stsId = stsId;
	}
	/**
	 * msg-IDを取得します。
	 * @return msg-ID
	 */
	public String getMsgId() {
	    return msgId;
	}
	/**
	 * msg-IDを設定します。
	 * @param msgId msg-ID
	 */
	public void setMsgId(String msgId) {
	    this.msgId = msgId;
	}
	/**
	 * messageを取得します。
	 * @return message
	 */
	public String getMsg() {
	    return msg;
	}
	/**
	 * messageを設定します。
	 * @param msg message
	 */
	public void setMsg(String msg) {
	    this.msg = msg;
	}

	/**
	 * complete status IDを取得します。
	 * @return complete status ID
	 */
	public StatusFlg getCompStsId() {
	    return compStsId;
	}
	/**
	 * complete status IDを設定します。
	 * @param cmpStsId complete status ID
	 */
	public void setCompStsId(StatusFlg compStsId) {
	    this.compStsId = compStsId;
	}

}