package jp.co.blueship.tri.fw.sm.dao.funcsvc.constants;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;


/**
 * The items of the function service entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public enum FuncSvcItems implements ITableItem {
	svcId("svc_id"),
	svcNm("svc_nm"),
	svcContent("svc_content"),
	isConcurrent("is_concurrent"),
	delStsId("del_sts_id"),
	regUserId("reg_user_id"),
	regUserNm("reg_user_nm"),
	regTimestamp("reg_timestamp"),
	updUserId("upd_user_id"),
	updUserNm("upd_user_nm"),
	updTimestamp("upd_timestamp");

	private String element = null;

	private FuncSvcItems( String element) {
		this.element = element;
	}

	public String getItemName() {
		return this.element;
	}

}
