package jp.co.blueship.tri.fw.sm.dao.procmgt.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the process details entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IProcDetailsEntity extends IEntityFooter {

	public String getProcId();
	public void setProcId(String procId);

	public String getProcCtgCd();
	public void setProcCtgCd(String procCtgCd);

	public Timestamp getProcStTimestamp();
	public void setProcStTimestamp(Timestamp procStTimestamp);

	public Timestamp getProcEndTimestamp();
	public void setProcEndTimestamp(Timestamp procEndTimestamp);

	public String getStsId();
	public void setStsId(String stsId);

	public 	String getMsgId();
	public void setMsgId(String msgId);

	public String getMsg();
	public void setMsg(String msg);

	public StatusFlg getCompStsId();
	public void setCompStsId(StatusFlg stsId);

}
