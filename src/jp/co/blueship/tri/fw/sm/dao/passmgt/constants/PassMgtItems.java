package jp.co.blueship.tri.fw.sm.dao.passmgt.constants;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;


/**
 * The items of the password management entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public enum PassMgtItems implements ITableItem {
	passCtgCd("pass_ctg_cd"),
	hostNm("host_nm"),
	userId("user_id"),
	epass("epass"),
	password("password"),
	delStsId("del_sts_id"),
	regUserId("reg_user_id"),
	regUserNm("reg_user_nm"),
	regTimestamp("reg_timestamp"),
	updUserId("upd_user_id"),
	updUserNm("upd_user_nm"),
	updTimestamp("upd_timestamp");

	private String element = null;

	private PassMgtItems( String element) {
		this.element = element;
	}

	public String getItemName() {
		return this.element;
	}

}
