package jp.co.blueship.tri.fw.sm.dao.passmgt;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.IDaoTemplate;
import jp.co.blueship.tri.fw.dao.orm.IDaoTemplateCallback;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.ISqlCondition;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.DaoTemplate;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;
import jp.co.blueship.tri.fw.sm.dao.constants.SmTables;
import jp.co.blueship.tri.fw.sm.dao.passmgt.constants.PassMgtItems;
import jp.co.blueship.tri.fw.sm.dao.passmgt.eb.IPassMgtEntity;
import jp.co.blueship.tri.fw.sm.dao.passmgt.eb.PassMgtEntity;

/**
 * The implements of the password management DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class PassMgtDao extends JdbcBaseDao<IPassMgtEntity> implements IPassMgtDao {
	public final String PGPKEY = "trinity";

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected final ITableAttribute getTableAttribute() {
		return SmTables.SM_PASS_MGT;
	}

	@Override
	public void setDaoTemplate( IDaoTemplate daoTemplate ) {
		super.setDaoTemplate( new CustomTemplate( new CustomCallback() ) );
	}

	@Override
	protected final ISqlBuilder append( ISqlBuilder builder, IPassMgtEntity entity ) {
		builder
			.append(PassMgtItems.passCtgCd, entity.getPassCtgCd(), true)
			.append(PassMgtItems.hostNm, entity.getHostNm(), true)
			.append(PassMgtItems.userId, entity.getUserId(), true)
			.append(PassMgtItems.epass, entity.getEpass())
			.append(PassMgtItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(PassMgtItems.regTimestamp, entity.getRegTimestamp())
			.append(PassMgtItems.regUserId, entity.getRegUserId())
			.append(PassMgtItems.regUserNm, entity.getRegUserNm())
			.append(PassMgtItems.updTimestamp, entity.getUpdTimestamp())
			.append(PassMgtItems.updUserId, entity.getUpdUserId())
			.append(PassMgtItems.updUserNm, entity.getUpdUserNm())
			;

		return builder;
	}

	@Override
	protected final IPassMgtEntity entityMapping( ResultSet rs, int row ) throws SQLException {
  	  PassMgtEntity entity = new PassMgtEntity();

  	  entity.setPassCtgCd( rs.getString(PassMgtItems.passCtgCd.getItemName()) );
  	  entity.setHostNm( rs.getString(PassMgtItems.hostNm.getItemName()) );
  	  entity.setUserId( rs.getString(PassMgtItems.userId.getItemName()) );
  	  entity.setEpass( rs.getString(PassMgtItems.epass.getItemName()) );
  	  entity.setPassword( rs.getString(PassMgtItems.password.getItemName()) );
  	  entity.setDelStsId( StatusFlg.value(rs.getBoolean(PassMgtItems.delStsId.getItemName())) );
  	  entity.setRegTimestamp( rs.getTimestamp(PassMgtItems.regTimestamp.getItemName()) );
  	  entity.setRegUserId( rs.getString(PassMgtItems.regUserId.getItemName()) );
  	  entity.setRegUserNm( rs.getString(PassMgtItems.regUserNm.getItemName()) );
  	  entity.setUpdTimestamp( rs.getTimestamp(PassMgtItems.updTimestamp.getItemName()) );
  	  entity.setUpdUserId( rs.getString(PassMgtItems.updUserId.getItemName()) );
  	  entity.setUpdUserNm( rs.getString(PassMgtItems.updUserNm.getItemName()) );

  	  return entity;
	}

	private class CustomTemplate extends DaoTemplate {
		protected CustomTemplate( IDaoTemplateCallback callback ) {
			super.setDaoTemplateCallback(callback);
		}
	}

	private class CustomCallback implements IDaoTemplateCallback {
		@Override
		public String appendOfSelectClause(ISqlCondition condition, boolean isCountMode) {
			StringBuffer buf = new StringBuffer()
					.append("  ,pgp_sym_decrypt( epass::bytea , '" + PGPKEY + "'::text ) as password")
				;

				return buf.toString();
		}

		@Override
		public String appendOfFromClause(ISqlCondition condition, boolean isCountMode) {
			StringBuffer buf = new StringBuffer()
				;

			return buf.toString();
		}
	}

}
