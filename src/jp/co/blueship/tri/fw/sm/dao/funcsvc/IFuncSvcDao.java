package jp.co.blueship.tri.fw.sm.dao.funcsvc;

import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;
import jp.co.blueship.tri.fw.sm.dao.funcsvc.eb.IFuncSvcEntity;


/**
 * The interface of the function service DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IFuncSvcDao extends IJdbcDao<IFuncSvcEntity> {

}
