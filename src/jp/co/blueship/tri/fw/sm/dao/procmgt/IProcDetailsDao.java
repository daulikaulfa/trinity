package jp.co.blueship.tri.fw.sm.dao.procmgt;

import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;
import jp.co.blueship.tri.fw.sm.dao.procmgt.eb.IProcDetailsEntity;


/**
 * The interface of the process details DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IProcDetailsDao extends IJdbcDao<IProcDetailsEntity> {

}
