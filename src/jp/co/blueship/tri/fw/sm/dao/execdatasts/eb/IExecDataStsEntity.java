package jp.co.blueship.tri.fw.sm.dao.execdatasts.eb;

import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the execute data status entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IExecDataStsEntity extends IEntityFooter {

	public String getProcId();
	public void setProcId(String procId);

	public String getDataCtgCd();
	public void setDataCtgCd(String dataCtgCd);

	public String getDataId();
	public void setDataId(String dataId);

	public String getStsId();
	public void setStsId(String stsId);

}
