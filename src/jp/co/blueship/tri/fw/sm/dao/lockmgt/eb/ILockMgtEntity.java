package jp.co.blueship.tri.fw.sm.dao.lockmgt.eb;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the lock management entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface ILockMgtEntity extends IEntityFooter {

	public String getProcId();
	public void setProcId(String procId);

	public String getLockSvcId();
	public void setLockSvcId(String lockSvcId);

	public String getSvcId();
	public void setSvcId(String svcId);

	public String getUserId();
	public void setUserId(String userId);

	public String getLotId();
	public void setLotId(String lotId);

	public StatusFlg getLockForLot();
	public void setLockForLot(StatusFlg lockForLot);

}
