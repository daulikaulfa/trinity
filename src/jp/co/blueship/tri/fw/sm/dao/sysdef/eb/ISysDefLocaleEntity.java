package jp.co.blueship.tri.fw.sm.dao.sysdef.eb;

import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the system define locale entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public interface ISysDefLocaleEntity extends IEntityFooter {

	public String getSysDefId();
	public void setSysDefId(String sysDefId);

	public String getSetId();
	public void setSetId(String setId);

	public String getSetKey();
	public void setSetKey(String setKey);

	public String getLang();
	public void setLang(String lang);

	public String getCountry();
	public void setCountry(String country);

	public String getSetValue();
	public void setSetValue(String setValue);

	public Integer getSortOdr();
	public void setSortOdr(Integer sortOdr);

}
