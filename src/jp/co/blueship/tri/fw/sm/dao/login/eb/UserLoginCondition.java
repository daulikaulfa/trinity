package jp.co.blueship.tri.fw.sm.dao.login.eb;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.dao.orm.psql.SqlFormatUtils;
import jp.co.blueship.tri.fw.sm.dao.constants.SmTables;
import jp.co.blueship.tri.fw.um.dao.user.constants.UserItems;

/**
 * 
 * @version V4.00.00
 * @author le.thixuan
 */
public class UserLoginCondition extends ConditionSupport {
	
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute attr = SmTables.SM_USER_LOGIN;
	
	public UserLoginCondition() {
		super(attr);
	}
	
	public String userId = null;
	
	public String[] userIds = null;
	
	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		   super.append(UserItems.delStsId, StatusFlg.off.parseBoolean() );
	}
	
	/**
	 * user-IDを取得します。
	 * @return user-ID
	 */
	public String getUserId() {
	    return userId;
	}
	/**
	 * user-IDを設定します。
	 * @param userId user-ID
	 */
	public void setUserId(String userId) {
	    this.userId = userId;
	    super.append( UserItems.userId, userId );
	}
	
	/**
	 * user-ID'sを取得します。
	 * @return user-ID's
	 */
	public String[] getUserIds() {
	    return userIds;
	}
	
	/**
	 * user-ID'sを設定します。
	 * @param userId user-ID's
	 */
	public void setUserIds(String[] userIds) {
	    this.userIds = userIds;
	    super.append( UserItems.userId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(userIds, UserItems.userId.getItemName(), false, true, false) );
	}
	
	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	    super.append( UserItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}
}
