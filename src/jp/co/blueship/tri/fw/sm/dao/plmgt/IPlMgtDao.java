package jp.co.blueship.tri.fw.sm.dao.plmgt;

import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;
import jp.co.blueship.tri.fw.sm.dao.plmgt.eb.IPlMgtEntity;


/**
 * The interface of the product license management DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IPlMgtDao extends IJdbcDao<IPlMgtEntity> {

}
