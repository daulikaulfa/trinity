package jp.co.blueship.tri.fw.sm.dao.execdatasts;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;
import jp.co.blueship.tri.fw.sm.dao.constants.SmTables;
import jp.co.blueship.tri.fw.sm.dao.execdatasts.constants.ExecDataStsItems;
import jp.co.blueship.tri.fw.sm.dao.execdatasts.eb.ExecDataStsEntity;
import jp.co.blueship.tri.fw.sm.dao.execdatasts.eb.IExecDataStsEntity;

/**
 * The implements of the execute data status DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 */
public class ExecDataStsDao extends JdbcBaseDao<IExecDataStsEntity> implements IExecDataStsDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected final ITableAttribute getTableAttribute() {
		return SmTables.SM_EXEC_DATA_STS;
	}

	@Override
	protected final ISqlBuilder append( ISqlBuilder builder, IExecDataStsEntity entity ) {
		builder
			.append(ExecDataStsItems.procId, entity.getProcId(), true)
			.append(ExecDataStsItems.dataCtgCd, entity.getDataCtgCd())
			.append(ExecDataStsItems.dataId, entity.getDataId())
			.append(ExecDataStsItems.stsId, entity.getStsId())
			.append(ExecDataStsItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(ExecDataStsItems.regTimestamp, entity.getRegTimestamp())
			.append(ExecDataStsItems.regUserId, entity.getRegUserId())
			.append(ExecDataStsItems.regUserNm, entity.getRegUserNm())
			.append(ExecDataStsItems.updTimestamp, entity.getUpdTimestamp())
			.append(ExecDataStsItems.updUserId, entity.getUpdUserId())
			.append(ExecDataStsItems.updUserNm, entity.getUpdUserNm())
			;

		return builder;
	}

	@Override
	protected final IExecDataStsEntity entityMapping( ResultSet rs, int row ) throws SQLException {
  	  IExecDataStsEntity entity = new ExecDataStsEntity();

  	  entity.setProcId( rs.getString(ExecDataStsItems.procId.getItemName()) );
  	  entity.setDataCtgCd( rs.getString(ExecDataStsItems.dataCtgCd.getItemName()) );
  	  entity.setDataId( rs.getString(ExecDataStsItems.dataId.getItemName()) );
  	  entity.setStsId( rs.getString(ExecDataStsItems.stsId.getItemName()) );
  	  entity.setDelStsId( StatusFlg.value(rs.getBoolean(ExecDataStsItems.delStsId.getItemName())) );
  	  entity.setRegTimestamp( rs.getTimestamp(ExecDataStsItems.regTimestamp.getItemName()) );
  	  entity.setRegUserId( rs.getString(ExecDataStsItems.regUserId.getItemName()) );
  	  entity.setRegUserNm( rs.getString(ExecDataStsItems.regUserNm.getItemName()) );
  	  entity.setUpdTimestamp( rs.getTimestamp(ExecDataStsItems.updTimestamp.getItemName()) );
  	  entity.setUpdUserId( rs.getString(ExecDataStsItems.updUserId.getItemName()) );
  	  entity.setUpdUserNm( rs.getString(ExecDataStsItems.updUserNm.getItemName()) );

  	  return entity;
	}

}
