package jp.co.blueship.tri.fw.sm.dao.execdatasts;

import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;
import jp.co.blueship.tri.fw.sm.dao.execdatasts.eb.IExecDataStsEntity;


/**
 * The interface of the execute data status DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IExecDataStsDao extends IJdbcDao<IExecDataStsEntity> {

}
