package jp.co.blueship.tri.fw.sm.dao.login.constants;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;

/**
 * 
 * @version V4.00.00
 * @author le.thixuan
 */
public enum UserLoginItems implements ITableItem {
	userId("user_id"),
	lastLoginTimestamp("last_login_timestamp"),
	delStsId("del_sts_id"),
	regUserId("reg_user_id"),
	regUserNm("reg_user_nm"),
	regTimestamp("reg_timestamp"),
	updUserId("upd_user_id"),
	updUserNm("upd_user_nm"),
	updTimestamp("upd_timestamp");
	
	private String element = null;

	private UserLoginItems( String element) {
		this.element = element;
	}

	public String getItemName() {
		return this.element;
	}
}
