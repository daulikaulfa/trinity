package jp.co.blueship.tri.fw.sm.dao.sysdef.eb;

import jp.co.blueship.tri.fw.dao.orm.EntityFooter;

/**
 * system define locale entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public class SysDefLocaleEntity extends EntityFooter implements ISysDefLocaleEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * system def-ID
	 */
	public String sysDefId = null;
	/**
	 * set-ID
	 */
	public String setId = null;
	/**
	 * set key
	 */
	public String setKey = null;
	/**
	 * language
	 */
	public String lang = null;
	/**
	 * country
	 */
	public String country = null;
	/**
	 * set value
	 */
	public String setValue = null;
	/**
	 * sort order
	 */
	public Integer sortOdr = null;

	/**
	 * system def-IDを取得します。
	 * @return system def-ID
	 */
	public String getSysDefId() {
	    return sysDefId;
	}
	/**
	 * system def-IDを設定します。
	 * @param sysDefId system def-ID
	 */
	public void setSysDefId(String sysDefId) {
	    this.sysDefId = sysDefId;
	}
	/**
	 * set-IDを取得します。
	 * @return set-ID
	 */
	public String getSetId() {
	    return setId;
	}
	/**
	 * set-IDを設定します。
	 * @param setId set-ID
	 */
	public void setSetId(String setId) {
	    this.setId = setId;
	}
	/**
	 * set keyを取得します。
	 * @return set key
	 */
	public String getSetKey() {
	    return setKey;
	}
	/**
	 * set keyを設定します。
	 * @param setKey set key
	 */
	public void setSetKey(String setKey) {
	    this.setKey = setKey;
	}
	/**
	 * languageを取得します。
	 * @return language
	 */
	public String getLang() {
	    return lang;
	}
	/**
	 * languageを設定します。
	 * @param lang language
	 */
	public void setLang(String lang) {
	    this.lang = lang;
	}
	/**
	 * countryを取得します。
	 * @return country
	 */
	public String getCountry() {
	    return country;
	}
	/**
	 * countryを設定します。
	 * @param country country
	 */
	public void setCountry(String country) {
	    this.country = country;
	}
	/**
	 * set valueを取得します。
	 * @return set value
	 */
	public String getSetValue() {
	    return setValue;
	}
	/**
	 * set valueを設定します。
	 * @param setValue set value
	 */
	public void setSetValue(String setValue) {
	    this.setValue = setValue;
	}
	/**
	 * sort orderを取得します。
	 * @return sort order
	 */
	public Integer getSortOdr() {
	    return sortOdr;
	}
	/**
	 * sort orderを設定します。
	 * @param sortOdr sort order
	 */
	public void setSortOdr(Integer sortOdr) {
	    this.sortOdr = sortOdr;
	}

}