package jp.co.blueship.tri.fw.sm.dao.sysdef;

import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;
import jp.co.blueship.tri.fw.sm.dao.sysdef.eb.ISysDefLocaleEntity;


/**
 * The interface of the system define locale DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface ISysDefLocaleDao extends IJdbcDao<ISysDefLocaleEntity> {

}
