package jp.co.blueship.tri.fw.sm.dao.lockmgt;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;
import jp.co.blueship.tri.fw.sm.dao.constants.SmTables;
import jp.co.blueship.tri.fw.sm.dao.lockmgt.constants.LockMgtItems;
import jp.co.blueship.tri.fw.sm.dao.lockmgt.eb.ILockMgtEntity;
import jp.co.blueship.tri.fw.sm.dao.lockmgt.eb.LockMgtEntity;

/**
 * The implements of the lock management DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 */
public class LockMgtDao extends JdbcBaseDao<ILockMgtEntity> implements ILockMgtDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected final ITableAttribute getTableAttribute() {
		return SmTables.SM_LOCK_MGT;
	}

	@Override
	protected final ISqlBuilder append( ISqlBuilder builder, ILockMgtEntity entity ) {
		builder
			.append(LockMgtItems.procId, entity.getProcId(), true)
			.append(LockMgtItems.lockSvcId, entity.getLockSvcId(),true)
			.append(LockMgtItems.svcId, entity.getSvcId())
			.append(LockMgtItems.userId, entity.getUserId())
			.append(LockMgtItems.lotId, entity.getLotId())
			.append(LockMgtItems.lockForLot, (null == entity.getLockForLot())? StatusFlg.off.parseBoolean(): entity.getLockForLot().parseBoolean())
			.append(LockMgtItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(LockMgtItems.regTimestamp, entity.getRegTimestamp())
			.append(LockMgtItems.regUserId, entity.getRegUserId())
			.append(LockMgtItems.regUserNm, entity.getRegUserNm())
			.append(LockMgtItems.updTimestamp, entity.getUpdTimestamp())
			.append(LockMgtItems.updUserId, entity.getUpdUserId())
			.append(LockMgtItems.updUserNm, entity.getUpdUserNm())
			;

		return builder;
	}

	@Override
	protected final ILockMgtEntity entityMapping( ResultSet rs, int row ) throws SQLException {
  	  ILockMgtEntity entity = new LockMgtEntity();

  	  entity.setProcId( rs.getString(LockMgtItems.procId.getItemName()) );
  	  entity.setLockSvcId( rs.getString(LockMgtItems.lockSvcId.getItemName()) );
  	  entity.setSvcId( rs.getString(LockMgtItems.svcId.getItemName()) );
  	  entity.setUserId( rs.getString(LockMgtItems.userId.getItemName()) );
  	  entity.setLotId( rs.getString(LockMgtItems.lotId.getItemName()) );
  	  entity.setLockForLot( StatusFlg.value(rs.getBoolean(LockMgtItems.lockForLot.getItemName())) );
  	  entity.setDelStsId( StatusFlg.value(rs.getBoolean(LockMgtItems.delStsId.getItemName())) );
  	  entity.setRegTimestamp( rs.getTimestamp(LockMgtItems.regTimestamp.getItemName()) );
  	  entity.setRegUserId( rs.getString(LockMgtItems.regUserId.getItemName()) );
  	  entity.setRegUserNm( rs.getString(LockMgtItems.regUserNm.getItemName()) );
  	  entity.setUpdTimestamp( rs.getTimestamp(LockMgtItems.updTimestamp.getItemName()) );
  	  entity.setUpdUserId( rs.getString(LockMgtItems.updUserId.getItemName()) );
  	  entity.setUpdUserNm( rs.getString(LockMgtItems.updUserNm.getItemName()) );

  	  return entity;
	}

}
