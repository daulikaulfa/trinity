package jp.co.blueship.tri.fw.sm.dao.sysdef.eb;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.sm.dao.constants.SmTables;
import jp.co.blueship.tri.fw.sm.dao.sysdef.constants.SysDefItems;

/**
 * The SQL condition of the system define  entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class SysDefCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute attr = SmTables.SM_SYS_DEF;

	/**
	 * system def-ID
	 */
	public String sysDefId = null;
	/**
	 * set-ID
	 */
	public String setId = null;
	/**
	 * set key
	 */
	public String setKey = null;
	/**
	 * set value
	 */
	public String setValue = null;
	/**
	 * is cache
	 */
	public StatusFlg isCache = StatusFlg.on;
	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		   super.append(SysDefItems.delStsId, StatusFlg.off.parseBoolean() );
	}

	public SysDefCondition(){
		super(attr);
	}

	/**
	 * system def-IDを取得します。
	 * @return system def-ID
	 */
	public String getSysDefId() {
	    return sysDefId;
	}

	/**
	 * system def-IDを設定します。
	 * @param sysDefId system def-ID
	 */
	public void setSysDefId(String sysDefId) {
	    this.sysDefId = sysDefId;
	    super.append(SysDefItems.sysDefId, sysDefId );
	}

	/**
	 * set-IDを取得します。
	 * @return set-ID
	 */
	public String getSetId() {
	    return setId;
	}

	/**
	 * set-IDを設定します。
	 * @param setId set-ID
	 */
	public void setSetId(String setId) {
	    this.setId = setId;
	    super.append(SysDefItems.setId, setId );
	}

	/**
	 * set keyを取得します。
	 * @return set key
	 */
	public String getSetKey() {
	    return setKey;
	}

	/**
	 * set keyを設定します。
	 * @param setKey set key
	 */
	public void setSetKey(String setKey) {
	    this.setKey = setKey;
	    super.append(SysDefItems.setKey, setKey );
	}

	/**
	 * set valueを取得します。
	 * @return set value
	 */
	public String getSetValue() {
	    return setValue;
	}

	/**
	 * set valueを設定します。
	 * @param setValue set value
	 */
	public void setSetValue(String setValue) {
	    this.setValue = setValue;
	    super.append(SysDefItems.setValue, setValue );
	}

	/**
	 * is cacheを取得します。
	 * @return is cache
	 */
	public StatusFlg getIsCache() {
	    return isCache;
	}

	/**
	 * is cacheを設定します。
	 * @param isCache is cache
	 */
	public void setIsCache(StatusFlg isCache) {
	    this.isCache = isCache;
	    super.append( SysDefItems.delStsId, (null == isCache)? StatusFlg.on.parseBoolean(): isCache.parseBoolean() );
	}

	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	    super.append( SysDefItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}
}