package jp.co.blueship.tri.fw.sm.dao.autonumbering.eb;


/**
 * auto numbering entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class AutoNumberingEntity implements IAutoNumberingEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * auto numbering key
	 */
	public String numberingKey = null;
	/**
	 * numbering date
	 */
	public String numberingDate = null;
	/**
	 * sequential number
	 */
	public Integer seqNo = null;

	/**
	 * auto numbering keyを取得します。
	 * @return auto numbering key
	 */
	public String getNumberingKey() {
	    return numberingKey;
	}
	/**
	 * auto numbering keyを設定します。
	 * @param numberingKey auto numbering key
	 */
	public void setNumberingKey(String numberingKey) {
	    this.numberingKey = numberingKey;
	}
	/**
	 * numbering dateを取得します。
	 * @return numbering date
	 */
	public String getNumberingDate() {
	    return numberingDate;
	}
	/**
	 * numbering dateを設定します。
	 * @param numberingDate numbering date
	 */
	public void setNumberingDate(String numberingDate) {
	    this.numberingDate = numberingDate;
	}
	/**
	 * sequential numberを取得します。
	 * @return sequential number
	 */
	public Integer getSeqNo() {
	    return seqNo;
	}
	/**
	 * sequential numberを設定します。
	 * @param seqNo sequential number
	 */
	public void setSeqNo(Integer seqNo) {
	    this.seqNo = seqNo;
	}

}