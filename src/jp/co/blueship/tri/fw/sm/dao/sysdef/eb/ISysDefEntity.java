package jp.co.blueship.tri.fw.sm.dao.sysdef.eb;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the system define entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public interface ISysDefEntity extends IEntityFooter {

	public String getSysDefId();
	public void setSysDefId(String sysDefId);

	public String getSetId();
	public void setSetId(String setId);

	public String getSetKey();
	public void setSetKey(String setKey);

	public String getSetValue();
	public void setSetValue(String setValue);

	public StatusFlg getIsCache();
	public void setIsCache(StatusFlg isCache);

	public Integer getSortOdr();
	public void setSortOdr(Integer sortOdr);


}
