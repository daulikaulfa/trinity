package jp.co.blueship.tri.fw.sm.dao.sysdef;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;
import jp.co.blueship.tri.fw.sm.dao.constants.SmTables;
import jp.co.blueship.tri.fw.sm.dao.sysdef.constants.SysDefItems;
import jp.co.blueship.tri.fw.sm.dao.sysdef.eb.ISysDefEntity;
import jp.co.blueship.tri.fw.sm.dao.sysdef.eb.SysDefEntity;

/**
 * The implements of the system define DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class SysDefDao extends JdbcBaseDao<ISysDefEntity> implements ISysDefDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected final ITableAttribute getTableAttribute() {
		return SmTables.SM_SYS_DEF;
	}

	@Override
	protected final ISqlBuilder append(ISqlBuilder builder, ISysDefEntity entity) {
		builder.append(SysDefItems.sysDefId, entity.getSysDefId(), true)
				.append(SysDefItems.setId, entity.getSetId(), true).append(SysDefItems.setKey, entity.getSetKey(), true)
				.append(SysDefItems.setValue, entity.getSetValue())
				.append(SysDefItems.isCache,
						(null == entity.getIsCache()) ? StatusFlg.on.parseBoolean()
								: entity.getIsCache().parseBoolean())
				.append(SysDefItems.sortOdr, (null == entity.getSortOdr()) ? 0 : entity.getSortOdr())
				.append(SysDefItems.delStsId,
						(null == entity.getDelStsId()) ? null : entity.getDelStsId().parseBoolean())
				.append(SysDefItems.regTimestamp, entity.getRegTimestamp())
				.append(SysDefItems.regUserId, entity.getRegUserId())
				.append(SysDefItems.regUserNm, entity.getRegUserNm())
				.append(SysDefItems.updTimestamp, entity.getUpdTimestamp())
				.append(SysDefItems.updUserId, entity.getUpdUserId())
				.append(SysDefItems.updUserNm, entity.getUpdUserNm());

		return builder;
	}

	@Override
	protected final ISysDefEntity entityMapping(ResultSet rs, int row) throws SQLException {
		ISysDefEntity entity = new SysDefEntity();

		entity.setSysDefId(rs.getString(SysDefItems.sysDefId.getItemName()));
		entity.setSetId(rs.getString(SysDefItems.setId.getItemName()));
		entity.setSetKey(rs.getString(SysDefItems.setKey.getItemName()));
		entity.setSetValue(rs.getString(SysDefItems.setValue.getItemName()));
		entity.setIsCache(StatusFlg.value(rs.getBoolean(SysDefItems.isCache.getItemName())));
		entity.setSortOdr(rs.getInt(SysDefItems.sortOdr.getItemName()));
		entity.setDelStsId(StatusFlg.value(rs.getBoolean(SysDefItems.delStsId.getItemName())));
		entity.setRegTimestamp(rs.getTimestamp(SysDefItems.regTimestamp.getItemName()));
		entity.setRegUserId(rs.getString(SysDefItems.regUserId.getItemName()));
		entity.setRegUserNm(rs.getString(SysDefItems.regUserNm.getItemName()));
		entity.setUpdTimestamp(rs.getTimestamp(SysDefItems.updTimestamp.getItemName()));
		entity.setUpdUserId(rs.getString(SysDefItems.updUserId.getItemName()));
		entity.setUpdUserNm(rs.getString(SysDefItems.updUserNm.getItemName()));

		return entity;
	}

	@Override
	protected String authUserID() {
		return null;
	}

	@Override
	protected String authUserName() {
		return null;
	}

}
