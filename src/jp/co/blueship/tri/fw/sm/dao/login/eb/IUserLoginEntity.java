package jp.co.blueship.tri.fw.sm.dao.login.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * 
 * @version V4.00.00
 * @author le.thixuan
 */
public interface IUserLoginEntity extends IEntityFooter {
	
	public String getUserId();
	public void setUserId(String userId);
	
	public Timestamp getLastLoginTimestamp();
	public void setLastLoginTimestamp(Timestamp lastLoginTimestamp);
}
