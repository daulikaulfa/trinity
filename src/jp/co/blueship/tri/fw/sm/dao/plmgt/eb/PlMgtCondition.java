package jp.co.blueship.tri.fw.sm.dao.plmgt.eb;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.sm.dao.constants.SmTables;
import jp.co.blueship.tri.fw.sm.dao.plmgt.constants.PlMgtItems;

/**
 * The SQL condition of the product license management entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class PlMgtCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute attr = SmTables.SM_PL_MGT;

	/**
	 * product-ID
	 */
	public String productId = null;
	/**
	 * computer name
	 */
	public String cpNm = null;
	/**
	 * serial number
	 */
	public String serialNo = null;
	/**
	 *  product license key
	 */
	public String plKey = null;
	/**
	 *  agent product license key
	 */
	public String agentPlKey = null;
	/**
	 *  remarks
	 */
	public String remarks = null;
	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		   super.append(PlMgtItems.delStsId, StatusFlg.off.parseBoolean() );
	}

	public PlMgtCondition(){
		super(attr);
	}

	/**
	 * product-IDを取得します。
	 * @return product-ID
	 */
	public String getProductId() {
	    return productId;
	}

	/**
	 * product-IDを設定します。
	 * @param productId product-ID
	 */
	public void setProductId(String productId) {
	    this.productId = productId;
	    super.append(PlMgtItems.productId, productId );
	}

	/**
	 * computer nameを取得します。
	 * @return computer name
	 */
	public String getCpNm() {
	    return cpNm;
	}

	/**
	 * computer nameを設定します。
	 * @param cpNm computer name
	 */
	public void setCpNm(String cpNm) {
	    this.cpNm = cpNm;
	    super.append(PlMgtItems.cpNm, cpNm );
	}

	/**
	 * serial numberを取得します。
	 * @return serial number
	 */
	public String getSerialNo() {
	    return serialNo;
	}

	/**
	 * serial numberを設定します。
	 * @param serialNo serial number
	 */
	public void setSerialNo(String serialNo) {
	    this.serialNo = serialNo;
	    super.append(PlMgtItems.serialNo, serialNo );
	}

	/**
	 * product license keyを取得します。
	 * @return product license key
	 */
	public String getPlKey() {
	    return plKey;
	}

	/**
	 * product license keyを設定します。
	 * @param plKey product license key
	 */
	public void setPlKey(String plKey) {
	    this.plKey = plKey;
	    super.append(PlMgtItems.plKey, (null == plKey)? "0": plKey );
	}

	/**
	 * agent product license keyを取得します。
	 * @return agent product license key
	 */
	public String getAgentPlKey() {
	    return agentPlKey;
	}

	/**
	 * agent product license keyを設定します。
	 * @param agentPlKey agent product license key
	 */
	public void setAgentPlKey(String agentPlKey) {
	    this.agentPlKey = agentPlKey;
	    super.append(PlMgtItems.agentPlKey, agentPlKey );
	}

	/**
	 * remarksを取得します。
	 * @return remarks
	 */
	public String getRemarks() {
	    return remarks;
	}

	/**
	 * remarksを設定します。
	 * @param remarks remarks
	 */
	public void setRemarks(String remarks) {
	    this.remarks = remarks;
	    super.append(PlMgtItems.remarks, remarks );
	}

	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	    super.append( PlMgtItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}
}