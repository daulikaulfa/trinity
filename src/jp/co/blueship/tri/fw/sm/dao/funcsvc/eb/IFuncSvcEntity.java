package jp.co.blueship.tri.fw.sm.dao.funcsvc.eb;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the function service entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IFuncSvcEntity extends IEntityFooter {

	public String getSvcId();
	public void setSvcId(String svcId);

	public String getSvcNm();
	public void setSvcNm(String svcNm);

	public String getSvcContent();
	public void setSvcContent(String svcContent);

	public StatusFlg getIsConcurrent();
	public void setIsConcurrent(StatusFlg isConcurrent);

}
