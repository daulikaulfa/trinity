package jp.co.blueship.tri.fw.sm.dao.lockmgt;

import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;
import jp.co.blueship.tri.fw.sm.dao.lockmgt.eb.ILockMgtEntity;


/**
 * The interface of the lock management DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface ILockMgtDao extends IJdbcDao<ILockMgtEntity> {

}
