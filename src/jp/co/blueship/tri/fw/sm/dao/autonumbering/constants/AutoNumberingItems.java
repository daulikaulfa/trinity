package jp.co.blueship.tri.fw.sm.dao.autonumbering.constants;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;


/**
 * The items of the auto numbering entity.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public enum AutoNumberingItems implements ITableItem {
	numberingKey("numbering_key"),
	numberingDate("numbering_date"),
	seqNo("seq_no");

	private String element = null;

	private AutoNumberingItems( String element) {
		this.element = element;
	}

	public String getItemName() {
		return this.element;
	}

}
