package jp.co.blueship.tri.fw.sm.dao.passmgt.eb;

import jp.co.blueship.tri.fw.dao.orm.EntityFooter;

/**
 * password management entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class PassMgtEntity extends EntityFooter implements IPassMgtEntity {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * password catalog code
	 */
	public String passCtgCd = null;
	/**
	 * host name
	 */
	public String hostNm = null;
	/**
	 * data-ID
	 */
	public String dataId = null;
	/**
	 * user-ID
	 */
	public String userId = null;
	/**
	 * encode password
	 */
	public String epass = null;
	/**
	 * decode password
	 */
	public String password = null;

	/**
	 * password catalog codeを取得します。
	 * @return password catalog code
	 */
	public String getPassCtgCd() {
	    return passCtgCd;
	}
	/**
	 * password catalog codeを設定します。
	 * @param passCtgCd password catalog code
	 */
	public void setPassCtgCd(String passCtgCd) {
	    this.passCtgCd = passCtgCd;
	}
	/**
	 * host nameを取得します。
	 * @return host name
	 */
	public String getHostNm() {
	    return hostNm;
	}
	/**
	 * host nameを設定します。
	 * @param hostNm host name
	 */
	public void setHostNm(String hostNm) {
	    this.hostNm = hostNm;
	}
	/**
	 * data-IDを取得します。
	 * @return data-ID
	 */
	public String getDataId() {
	    return dataId;
	}
	/**
	 * data-IDを設定します。
	 * @param dataId data-ID
	 */
	public void setDataId(String dataId) {
	    this.dataId = dataId;
	}
	/**
	 * user-IDを取得します。
	 * @return user-ID
	 */
	public String getUserId() {
	    return userId;
	}
	/**
	 * user-IDを設定します。
	 * @param userId user-ID
	 */
	public void setUserId(String userId) {
	    this.userId = userId;
	}
	/**
	 * encode passwordを取得します。
	 * @return encode password
	 */
	public String getEpass() {
	    return epass;
	}
	/**
	 * encode passwordを設定します。
	 * @param epass encode password
	 */
	public void setEpass(String epass) {
	    this.epass = epass;
	}
	/**
	 * decode passwordを取得します。
	 * @return decode password
	 */
	public String getPassword() {
	    return password;
	}
	/**
	 * decode passwordを設定します。
	 * @param password decode password
	 */
	public void setPassword(String password) {
	    this.password = password;
	}

}