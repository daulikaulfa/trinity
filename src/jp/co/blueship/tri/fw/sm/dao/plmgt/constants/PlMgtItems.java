package jp.co.blueship.tri.fw.sm.dao.plmgt.constants;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;


/**
 * The items of the product license management entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public enum PlMgtItems implements ITableItem {
	productId("product_id"),
	cpNm("cp_nm"),
	serialNo("serial_no"),
	plKey("pl_key"),
	agentPlKey("agent_pl_key"),
	remarks("remarks"),
	delStsId("del_sts_id"),
	regUserId("reg_user_id"),
	regUserNm("reg_user_nm"),
	regTimestamp("reg_timestamp"),
	updUserId("upd_user_id"),
	updUserNm("upd_user_nm"),
	updTimestamp("upd_timestamp"),
	plTimeLimit("pl_time_limit"),
	;

	private String element = null;

	private PlMgtItems( String element) {
		this.element = element;
	}

	public String getItemName() {
		return this.element;
	}

}
