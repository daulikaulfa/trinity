package jp.co.blueship.tri.fw.sm.dao.passmgt.eb;

import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the password management entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IPassMgtEntity extends IEntityFooter {

	public String getPassCtgCd();
	public void setPassCtgCd(String passCtgCd);

	public String getHostNm();
	public void setHostNm(String hostNm);

	public String getDataId();
	public void setDataId(String dataId);

	public String getUserId();
	public void setUserId(String userId);

	public String getEpass();
	public void setEpass(String epass);

	public String getPassword();


}
