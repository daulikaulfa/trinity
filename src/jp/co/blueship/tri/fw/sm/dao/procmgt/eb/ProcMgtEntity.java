package jp.co.blueship.tri.fw.sm.dao.procmgt.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.EntityFooter;

/**
 * process management entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class ProcMgtEntity extends EntityFooter implements IProcMgtEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * proc-ID
	 */
	public String procId = null;
	/**
	 * svc-ID
	 */
	public String svcId = null;
	/**
	 * lot-ID
	 */
	public String lotId = null;
	/**
	 * user-ID
	 */
	public String userId = null;
	/**
	 * user name
	 */
	public String userNm = null;
	/**
	 * process start time stamp
	 */
	public Timestamp procStTimestamp = null;
	/**
	 * process end time stamp
	 */
	public Timestamp procEndTimestamp = null;
	/**
	 * sts-ID
	 */
	public String stsId = null;
	/**
	 * complete status ID
	 */
	public StatusFlg compStsId = StatusFlg.off;

	/**
	 * proc-IDを取得します。
	 * @return proc-ID
	 */
	public String getProcId() {
	    return procId;
	}
	/**
	 * proc-IDを設定します。
	 * @param procId proc-ID
	 */
	public void setProcId(String procId) {
	    this.procId = procId;
	}
	/**
	 * svc-IDを取得します。
	 * @return svc-ID
	 */
	public String getSvcId() {
	    return svcId;
	}
	/**
	 * svc-IDを設定します。
	 * @param svcId svc-ID
	 */
	public void setSvcId(String svcId) {
	    this.svcId = svcId;
	}
	/**
	 * lot-IDを取得します。
	 * @return lot-ID
	 */
	public String getLotId() {
	    return lotId;
	}
	/**
	 * lot-IDを設定します。
	 * @param lotId lot-ID
	 */
	public void setLotId(String lotId) {
	    this.lotId = lotId;
	}
	/**
	 * user-IDを取得します。
	 * @return user-ID
	 */
	public String getUserId() {
	    return userId;
	}
	/**
	 * user-IDを設定します。
	 * @param userId user-ID
	 */
	public void setUserId(String userId) {
	    this.userId = userId;
	}
	/**
	 * user nameを取得します。
	 * @return user name
	 */
	public String getUserNm() {
	    return userNm;
	}
	/**
	 * user nameを設定します。
	 * @param userNm user name
	 */
	public void setUserNm(String userNm) {
	    this.userNm = userNm;
	}
	/**
	 * process start time stampを取得します。
	 * @return process start time stamp
	 */
	public Timestamp getProcStTimestamp() {
	    return procStTimestamp;
	}
	/**
	 * process start time stampを設定します。
	 * @param procStTimestamp process start time stamp
	 */
	public void setProcStTimestamp(Timestamp procStTimestamp) {
	    this.procStTimestamp = procStTimestamp;
	}
	/**
	 * process end time stampを取得します。
	 * @return process end time stamp
	 */
	public Timestamp getProcEndTimestamp() {
	    return procEndTimestamp;
	}
	/**
	 * process end time stampを設定します。
	 * @param procEndTimestamp process end time stamp
	 */
	public void setProcEndTimestamp(Timestamp procEndTimestamp) {
	    this.procEndTimestamp = procEndTimestamp;
	}
	/**
	 * sts-IDを取得します。
	 * @return sts-ID
	 */
	public String getStsId() {
	    return stsId;
	}
	/**
	 * sts-IDを設定します。
	 * @param stsId sts-ID
	 */
	public void setStsId(String stsId) {
	    this.stsId = stsId;
	}
	/**
	 * complete status IDを取得します。
	 * @return complete status ID
	 */
	public StatusFlg getCompStsId() {
	    return compStsId;
	}
	/**
	 * complete status IDを設定します。
	 * @param cmpStsId complete status ID
	 */
	public void setCompStsId(StatusFlg compStsId) {
	    this.compStsId = compStsId;
	}

}