package jp.co.blueship.tri.fw.sm.dao.procmgt.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the process management entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IProcMgtEntity extends IEntityFooter {

	public String getProcId();
	public void setProcId(String procId);

	public String getSvcId();
	public void setSvcId(String svcId);

	public String getLotId();
	public void setLotId(String lotId);

	public String getUserId();
	public void setUserId(String userId);

	public String getUserNm();
	public void setUserNm(String userNm);

	public Timestamp getProcStTimestamp();
	public void setProcStTimestamp(Timestamp procStTimestamp);

	public Timestamp getProcEndTimestamp();
	public void setProcEndTimestamp(Timestamp procEndTimestamp);

	public String getStsId();
	public void setStsId(String stsId);

	public StatusFlg getCompStsId();
	public void setCompStsId(StatusFlg compStsId);

}
