package jp.co.blueship.tri.fw.sm.dao.procmgt.constants;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;


/**
 * The items of the process management entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public enum ProcMgtItems implements ITableItem {
	procId("proc_id"),
	svcId("svc_id"),
	lotId("lot_id"),
	userId("user_id"),
	userNm("user_nm"),
	procStTimestamp("proc_st_timestamp"),
	procEndTimestamp("proc_end_timestamp"),
	stsId("sts_id"),
	compStsId("comp_sts_id"),
	delStsId("del_sts_id"),
	regUserId("reg_user_id"),
	regUserNm("reg_user_nm"),
	regTimestamp("reg_timestamp"),
	updUserId("upd_user_id"),
	updUserNm("upd_user_nm"),
	updTimestamp("upd_timestamp");

	private String element = null;

	private ProcMgtItems( String element) {
		this.element = element;
	}

	public String getItemName() {
		return this.element;
	}

}
