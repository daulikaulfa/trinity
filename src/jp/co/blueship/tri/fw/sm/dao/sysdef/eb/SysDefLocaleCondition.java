package jp.co.blueship.tri.fw.sm.dao.sysdef.eb;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.sm.dao.constants.SmTables;
import jp.co.blueship.tri.fw.sm.dao.sysdef.constants.SysDefLocaleItems;

/**
 * The SQL condition of the system define locale entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class SysDefLocaleCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute attr = SmTables.SM_SYS_DEF_LOCALE;

	/**
	 * system def-ID
	 */
	public String sysDefId = null;
	/**
	 * set-ID
	 */
	public String setId = null;
	/**
	 * set key
	 */
	public String setKey = null;
	/**
	 * language
	 */
	public String lang = null;
	/**
	 * country
	 */
	public String country = null;
	/**
	 * set value
	 */
	public String setValue = null;
	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		   super.append(SysDefLocaleItems.delStsId, StatusFlg.off.parseBoolean() );
	}

	public SysDefLocaleCondition(){
		super(attr);
	}

	/**
	 * system def-IDを取得します。
	 * @return system def-ID
	 */
	public String getSysDefId() {
	    return sysDefId;
	}

	/**
	 * system def-IDを設定します。
	 * @param sysDefId system def-ID
	 */
	public void setSysDefId(String sysDefId) {
	    this.sysDefId = sysDefId;
	    super.append(SysDefLocaleItems.sysDefId, sysDefId );
	}

	/**
	 * set-IDを取得します。
	 * @return set-ID
	 */
	public String getSetId() {
	    return setId;
	}

	/**
	 * set-IDを設定します。
	 * @param setId set-ID
	 */
	public void setSetId(String setId) {
	    this.setId = setId;
	    super.append(SysDefLocaleItems.setId, setId );
	}

	/**
	 * set keyを取得します。
	 * @return set key
	 */
	public String getSetKey() {
	    return setKey;
	}

	/**
	 * set keyを設定します。
	 * @param setKey set key
	 */
	public void setSetKey(String setKey) {
	    this.setKey = setKey;
	    super.append(SysDefLocaleItems.setKey, setKey );
	}

	/**
	 * languageを取得します。
	 * @return language
	 */
	public String getLang() {
	    return lang;
	}

	/**
	 * languageを設定します。
	 * @param lang language
	 */
	public void setLang(String lang) {
	    this.lang = lang;
	    super.append(SysDefLocaleItems.lang, lang );
	}

	/**
	 * countryを取得します。
	 * @return country
	 */
	public String getCountry() {
	    return country;
	}

	/**
	 * countryを設定します。
	 * @param setKey country
	 */
	public void setCountry(String country) {
	    this.country = country;
	    super.append(SysDefLocaleItems.country, country );
	}

	/**
	 * set valueを取得します。
	 * @return set value
	 */
	public String getSetValue() {
	    return setValue;
	}

	/**
	 * set valueを設定します。
	 * @param setValue set value
	 */
	public void setSetValue(String setValue) {
	    this.setValue = setValue;
	    super.append(SysDefLocaleItems.setValue, setValue );
	}

	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	    super.append( SysDefLocaleItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}
}