package jp.co.blueship.tri.fw.sm.dao.execdatasts.eb;

import jp.co.blueship.tri.fw.dao.orm.EntityFooter;

/**
 * execute data status entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class ExecDataStsEntity extends EntityFooter implements IExecDataStsEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * proc-ID
	 */
	public String procId = null;
	/**
	 * data catalog code
	 */
	public String dataCtgCd = null;
	/**
	 * data-ID
	 */
	public String dataId = null;
	/**
	 * sts-ID
	 */
	public String stsId = null;

	/**
	 * proc-IDを取得します。
	 * @return proc-ID
	 */
	public String getProcId() {
	    return procId;
	}
	/**
	 * proc-IDを設定します。
	 * @param procId proc-ID
	 */
	public void setProcId(String procId) {
	    this.procId = procId;
	}
	/**
	 * data catalog codeを取得します。
	 * @return data catalog code
	 */
	public String getDataCtgCd() {
	    return dataCtgCd;
	}
	/**
	 * data catalog codeを設定します。
	 * @param dataCtgCd data catalog code
	 */
	public void setDataCtgCd(String dataCtgCd) {
	    this.dataCtgCd = dataCtgCd;
	}
	/**
	 * data-IDを取得します。
	 * @return data-ID
	 */
	public String getDataId() {
	    return dataId;
	}
	/**
	 * data-IDを設定します。
	 * @param dataId data-ID
	 */
	public void setDataId(String dataId) {
	    this.dataId = dataId;
	}
	/**
	 * sts-IDを取得します。
	 * @return sts-ID
	 */
	public String getStsId() {
	    return stsId;
	}
	/**
	 * sts-IDを設定します。
	 * @param stsId sts-ID
	 */
	public void setStsId(String stsId) {
	    this.stsId = stsId;
	}

}
