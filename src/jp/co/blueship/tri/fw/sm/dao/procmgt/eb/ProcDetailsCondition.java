package jp.co.blueship.tri.fw.sm.dao.procmgt.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.sm.dao.constants.SmTables;
import jp.co.blueship.tri.fw.sm.dao.procmgt.constants.ProcDetailsItems;

/**
 * The SQL condition of the process details entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class ProcDetailsCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute attr = SmTables.SM_PROC_DETAILS;

	/**
	 * proc-ID
	 */
	public String procId = null;
	/**
	 * process-catalog-code
	 */
	public String procCtgCd = null;
	/**
	 * process start time stamp
	 */
	public Timestamp procStTimestamp = null;
	/**
	 * process end time stamp
	 */
	public Timestamp procEndTimestamp = null;
	/**
	 * sts-ID
	 */
	public String stsId = null;
	/**
	 * msg-ID
	 */
	public String msgId = null;
	/**
	 * msg
	 */
	public String msg = null;
	/**
	 * complete status ID
	 */
	public StatusFlg compStsId = StatusFlg.off;
	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		   super.append(ProcDetailsItems.delStsId, StatusFlg.off.parseBoolean() );
	}
	/**
	 * registration user-ID
	 */
	public String regUserId = null;
	/**
	 * update user-ID
	 */
	public String updUserId = null;

	public ProcDetailsCondition(){
		super(attr);
	}

	/**
	 * proc-IDを取得します。
	 * @return proc-ID
	 */
	public String getProcId() {
	    return procId;
	}

	/**
	 * proc-IDを設定します。
	 * @param procId proc-ID
	 */
	public void setProcId(String procId) {
	    this.procId = procId;
	    super.append(ProcDetailsItems.procId, procId );
	}

	/**
	 * process-catalog-codeを取得します。
	 * @return process-catalog-code
	 */
	public String getProcCtgCd() {
	    return procCtgCd;
	}

	/**
	 * process-catalog-codeを設定します。
	 * @param procCtgCd process-catalog-code
	 */
	public void setProcCtgCd(String procCtgCd) {
	    this.procCtgCd = procCtgCd;
	    super.append(ProcDetailsItems.procCtgCd, procCtgCd );
	}

	/**
	 * process start time stampを取得します。
	 * @return process start time stamp
	 */
	public Timestamp getProcStTimestamp() {
	    return procStTimestamp;
	}

	/**
	 * process start time stampを設定します。
	 * @param procStTimestamp process start time stamp
	 */
	public void setProcStTimestamp(Timestamp procStTimestamp) {
	    this.procStTimestamp = procStTimestamp;
	    super.append(ProcDetailsItems.procStTimestamp, procStTimestamp );
	}

	/**
	 * process end time stampを取得します。
	 * @return process end time stamp
	 */
	public Timestamp getProcEndTimestamp() {
	    return procEndTimestamp;
	}

	/**
	 * process end time stampを設定します。
	 * @param procEndTimestamp process end time stamp
	 */
	public void setProcEndTimestamp(Timestamp procEndTimestamp) {
	    this.procEndTimestamp = procEndTimestamp;
	    super.append(ProcDetailsItems.procEndTimestamp, procEndTimestamp );
	}

	/**
	 * sts-IDを取得します。
	 * @return sts-ID
	 */
	public String getStsId() {
	    return stsId;
	}

	/**
	 * sts-IDを設定します。
	 * @param stsId sts-ID
	 */
	public void setStsId(String stsId) {
	    this.stsId = stsId;
	    super.append(ProcDetailsItems.stsId, stsId );
	}

	/**
	 * msg-IDを取得します。
	 * @return msg-ID
	 */
	public String getMsgId() {
	    return msgId;
	}

	/**
	 * msg-IDを設定します。
	 * @param msgId msg-ID
	 */
	public void setMsgId(String msgId) {
	    this.msgId = msgId;
	    super.append(ProcDetailsItems.msgId, msgId );
	}

	/**
	 * msgを取得します。
	 * @return msg
	 */
	public String getMsg() {
	    return msg;
	}

	/**
	 * msgを設定します。
	 * @param msg msg
	 */
	public void setMsg(String msg) {
	    this.msg = msg;
	    super.append(ProcDetailsItems.msg, msg );
	}

	/**
	 * complete status IDを取得します。
	 * @return complete status ID
	 */
	public StatusFlg getCompStsId() {
	    return compStsId;
	}

	/**
	 * complete status IDを設定します。
	 * @param compStsId complete status ID
	 */
	public void setCompStsId(StatusFlg compStsId) {
	    this.compStsId = compStsId;
	    super.append( ProcDetailsItems.compStsId, (null == compStsId)? StatusFlg.off.parseBoolean(): compStsId.parseBoolean() );
	}

	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	    super.append( ProcDetailsItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}

	/**
	 * registration user-IDを取得します。
	 * @return registration user-ID
	 */
	public String getRegUserId() {
	    return regUserId;
	}

	/**
	 * registration user-IDを設定します。
	 * @param regUserId registration user-ID
	 */
	public void setRegUserId(String regUserId) {
	    this.regUserId = regUserId;
	    super.append(ProcDetailsItems.regUserId, regUserId );
	}

	/**
	 * update user-IDを取得します。
	 * @return update user-ID
	 */
	public String getUpdUserId() {
	    return updUserId;
	}

	/**
	 * update user-IDを設定します。
	 * @param updUserId update user-ID
	 */
	public void setUpdUserId(String updUserId) {
	    this.updUserId = updUserId;
	    super.append(ProcDetailsItems.updUserId, updUserId );
	}
}