package jp.co.blueship.tri.fw.sm.dao.execdatasts.eb;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.dao.orm.psql.SqlFormatUtils;
import jp.co.blueship.tri.fw.sm.dao.constants.SmTables;
import jp.co.blueship.tri.fw.sm.dao.execdatasts.constants.ExecDataStsItems;

/**
 * The SQL condition of the execute data status entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class ExecDataStsCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute attr = SmTables.SM_EXEC_DATA_STS;

	/**
	 * proc-ID
	 */
	public String procId = null;
	/**
	 * proc-ID's
	 */
	public String[] procIds = null;
	/**
	 * data catalog code
	 */
	public String dataCtgCd = null;
	/**
	 * data-ID
	 */
	public String dataId = null;
	/**
	 * data-ID's
	 */
	public String[] dataIds = null;
	/**
	 * sts-ID
	 */
	public String stsId = null;
	/**
	 * sts-ID's
	 */
	public String[] stsIds = null;
	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		   super.append(ExecDataStsItems.delStsId, StatusFlg.off.parseBoolean() );
	}

	public ExecDataStsCondition() {
		super(attr);
	}

	/**
	 * proc-IDを取得します。
	 * @return proc-ID
	 */
	public String getProcId() {
	    return procId;
	}

	/**
	 * proc-IDを設定します。
	 * @param procId proc-ID
	 */
	public void setProcId(String procId) {
	    this.procId = procId;
	    super.append(ExecDataStsItems.procId, procId );
	}

	/**
	 * proc-ID'sを取得します。
	 * @return proc-ID's
	 */
	public String[] getProcIds() {
	    return procIds;
	}

	/**
	 * proc-ID'sを設定します。
	 * @param procIds proc-ID's
	 */
	public void setProcIds(String... procIds) {
	    this.procIds = procIds;
	    super.append( ExecDataStsItems.procId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(procIds, ExecDataStsItems.procId.getItemName(), false, true, false) );
	}

	/**
	 * data catalog codeを取得します。
	 * @return data catalog cod
	 */
	public String getDataCtgCd() {
	    return dataCtgCd;
	}

	/**
	 * data catalog codeを設定します。
	 * @param dataCtgCd data catalog code
	 */
	public void setDataCtgCd(String dataCtgCd) {
	    this.dataCtgCd = dataCtgCd;
	    super.append(ExecDataStsItems.dataCtgCd, dataCtgCd );
	}

	/**
	 * data-IDを取得します。
	 * @return data-ID
	 */
	public String getDataId() {
	    return dataId;
	}

	/**
	 * data-IDを設定します。
	 * @param dataId data-ID
	 */
	public void setDataId(String dataId) {
	    this.dataId = dataId;
	    super.append(ExecDataStsItems.dataId, dataId );
	}

	/**
	 * data-ID'sを取得します。
	 * @return data-ID's
	 */
	public String[] getDataIds() {
	    return dataIds;
	}

	/**
	 * data-ID'sを設定します。
	 * @param dataId data-ID's
	 */
	public void setDataIds(String... dataIds) {
	    this.dataIds = dataIds;
	    super.append( ExecDataStsItems.dataId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(dataIds, ExecDataStsItems.dataId.getItemName(), false, true, false) );
	}

	/**
	 * sts-IDを取得します。
	 * @return sts-ID
	 */
	public String getStsId() {
	    return stsId;
	}

	/**
	 * sts-IDを設定します。
	 * @param procId sts-ID
	 */
	public void setStsId(String stsId) {
	    this.stsId = stsId;
	    super.append(ExecDataStsItems.stsId, stsId );
	}

	/**
	 * sts-ID'sを取得します。
	 * @return sts-ID's
	 */
	public String[] getStsIds() {
	    return stsIds;
	}

	/**
	 * sts-ID'sを設定します。
	 * @param stsIds sts-ID's
	 */
	public void setStsIds(String...stsIds) {
	    this.stsIds = stsIds;
	    super.append( ExecDataStsItems.stsId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(stsIds, ExecDataStsItems.stsId.getItemName(), false, true, false) );
	}

	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	    super.append( ExecDataStsItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}
}