package jp.co.blueship.tri.fw.sm.dao.passmgt;

import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;
import jp.co.blueship.tri.fw.sm.dao.passmgt.eb.IPassMgtEntity;


/**
 * The interface of the password management DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IPassMgtDao extends IJdbcDao<IPassMgtEntity> {

}
