package jp.co.blueship.tri.fw.sm.dao.constants;

import org.apache.commons.lang.StringUtils;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;

/**
 * The enum of the tables.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 * 
 * @version V4.00.00
 * @author le.thixuan
 */
public enum SmTables implements ITableAttribute {

	SM_AUTO_NUMBERING( "SA" ),
	SM_PROC_MGT( "SPM" ),
	SM_PROC_DETAILS( "SPD" ),
	SM_EXEC_DATA_STS( "SE" ),
	SM_FUNC_SVC( "SF" ),
	SM_LOCK_MGT( "SL" ),
	SM_PASS_MGT( "SP" ),
	SM_PL_MGT( "SPM" ),
	SM_SYS_DEF( "SSD" ),
	SM_SYS_DEF_LOCALE( "SSDL" ),
	SM_USER_LOGIN( "SUL" );

	private String alias = null;

	private SmTables( String alias) {
		this.alias = alias;
	}

	/**
	 * Alias付きのテーブル名を取得します。
	 *
	 * @return 文字列
	 */
	public String nameWithAlias() {
		return this.name() + " " + this.alias() ;
	}

	/**
	 * テーブルごとに一意となるエイリアスを取得します。
	 *
	 * @return 文字列
	 */
	public String alias() {
		return this.alias;
	}

	/**
	 * 指定された名前に対応する列挙型を取得します。
	 *
	 * @param name 名前
	 * @return 対応する列挙型
	 */
	public static SmTables name( String name ) {
		if ( StringUtils.isBlank( name ) ) {
			return null;
		}

		for ( SmTables table : values() ) {
			if ( table.name().equalsIgnoreCase( name ) ) {
				return table;
			}
		}

		return null;
	}

}
