package jp.co.blueship.tri.fw.sm.dao.autonumbering;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.List;

import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.ex.TriJdbcDaoException;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.sm.dao.autonumbering.constants.AutoNumberingItems;
import jp.co.blueship.tri.fw.sm.dao.autonumbering.eb.AutoNumberingCondition;
import jp.co.blueship.tri.fw.sm.dao.autonumbering.eb.AutoNumberingEntity;
import jp.co.blueship.tri.fw.sm.dao.autonumbering.eb.IAutoNumberingEntity;
import jp.co.blueship.tri.fw.sm.dao.constants.SmTables;

/**
 * The implements of the auto numbering DAO.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 */
public class AutoNumberingDao extends JdbcBaseDao<IAutoNumberingEntity> implements IAutoNumberingDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public String nextval( String numberingKey, String pattern ) {
		try {
			String numberingDate = TriDateUtils.getSystemDate( TriDateUtils.getYMDDateFormat() );

			return this.innerNextval(numberingKey, numberingDate, pattern);

		} catch ( Exception e ) {
			throw new TriJdbcDaoException( SmMessageId.SM005008S, e, numberingKey );
		}
	}

	@Override
	public String nextval( String numberingKey, String numberingDate, String pattern ) {
		try {
			return this.innerNextval(numberingKey, numberingDate, pattern);

		} catch ( Exception e ) {
			throw new TriJdbcDaoException( SmMessageId.SM005008S, e, numberingKey );
		}
	}

	/**
	 * 自動採番します。指定された採番年月日で採番します。
	 * <br>指定されたパターン文字列でフォーマットした採番IDを取得します。
	 * <br>ex.) 4桁でゼロサプレスする場合、"0000"を指定
	 *
	 * @param numberingKey 採番キー
	 * @param numberingDate 採番年月日
	 * @param pattern パターン文字列
	 * @return 採番された値を戻します
	 */
	private String innerNextval( String numberingKey, String numberingDate, String pattern ) {

		AutoNumberingCondition condition = new AutoNumberingCondition();
		condition.setNumberingKey( numberingKey );

		//排他検索
		String sql = this.getDaoTemplate().toSelectQuery(condition.getCondition())
				+ " FOR UPDATE";

		List<IAutoNumberingEntity> rows = super.query(
				sql,
				new ParameterizedRowMapper<IAutoNumberingEntity>() {
					@Override
					public IAutoNumberingEntity mapRow(ResultSet resultset, int i) throws SQLException {
						return entityMapping( resultset, i );
					}
				},
				condition.getCondition());

		IAutoNumberingEntity entity = ( 0 == rows.size() )? null: rows.get(0);

		//登録されてなければ初期登録する
		if ( null == entity ) {
			entity = new AutoNumberingEntity();
			entity.setNumberingKey( numberingKey );
			entity.setNumberingDate( numberingDate );
			entity.setSeqNo( 0 );
			this.insert(entity);
		} else {
			if ( ! numberingDate.equals(entity.getNumberingDate()) ) {
				entity.setNumberingDate( numberingDate );
				entity.setSeqNo( 0 );
			}
		}

		entity.setSeqNo( entity.getSeqNo() + 1 );

		//採番更新
		this.update(entity);

		DecimalFormat format = new DecimalFormat( pattern );
		return format.format( entity.getSeqNo() );

	}

	@Override
	protected final ITableAttribute getTableAttribute() {
		return SmTables.SM_AUTO_NUMBERING;
	}

	@Override
	protected ISqlBuilder append(ISqlBuilder builder, IAutoNumberingEntity entity) {
		builder
			.append(AutoNumberingItems.numberingKey, entity.getNumberingKey(), true)
			.append(AutoNumberingItems.numberingDate, entity.getNumberingDate())
			.append(AutoNumberingItems.seqNo, entity.getSeqNo())
			;

		return builder;
	}

	@Override
	protected final IAutoNumberingEntity entityMapping( ResultSet rs, int row ) throws SQLException {
		IAutoNumberingEntity entity = new AutoNumberingEntity();

	  	  entity.setNumberingKey( rs.getString(AutoNumberingItems.numberingKey.getItemName()) );
	  	  entity.setNumberingDate( rs.getString(AutoNumberingItems.numberingDate.getItemName()) );
	  	  entity.setSeqNo( rs.getInt(AutoNumberingItems.seqNo.getItemName()) );

	  	  return entity;
	}

}
