package jp.co.blueship.tri.fw.sm.dao.plmgt;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;
import jp.co.blueship.tri.fw.sm.dao.constants.SmTables;
import jp.co.blueship.tri.fw.sm.dao.plmgt.constants.PlMgtItems;
import jp.co.blueship.tri.fw.sm.dao.plmgt.eb.IPlMgtEntity;
import jp.co.blueship.tri.fw.sm.dao.plmgt.eb.PlMgtEntity;

/**
 * The implements of the product license management DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 */
public class PlMgtDao extends JdbcBaseDao<IPlMgtEntity> implements IPlMgtDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected final ITableAttribute getTableAttribute() {
		return SmTables.SM_PL_MGT;
	}

	@Override
	protected final ISqlBuilder append( ISqlBuilder builder, IPlMgtEntity entity ) {
		builder
			.append(PlMgtItems.productId, entity.getProductId(), true)
			.append(PlMgtItems.cpNm, entity.getCpNm(),true)
			.append(PlMgtItems.serialNo, entity.getSerialNo())
			.append(PlMgtItems.plKey, (null == entity.getPlKey())? "0": entity.getPlKey())
			.append(PlMgtItems.agentPlKey, entity.getAgentPlKey())
			.append(PlMgtItems.plTimeLimit, entity.getPlTimeLimit())
			.append(PlMgtItems.remarks, entity.getRemarks())
			.append(PlMgtItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(PlMgtItems.regTimestamp, entity.getRegTimestamp())
			.append(PlMgtItems.regUserId, entity.getRegUserId())
			.append(PlMgtItems.regUserNm, entity.getRegUserNm())
			.append(PlMgtItems.updTimestamp, entity.getUpdTimestamp())
			.append(PlMgtItems.updUserId, entity.getUpdUserId())
			.append(PlMgtItems.updUserNm, entity.getUpdUserNm())
			;

		return builder;
	}

	@Override
	protected final IPlMgtEntity entityMapping( ResultSet rs, int row ) throws SQLException {
  	  IPlMgtEntity entity = new PlMgtEntity();

  	  entity.setProductId( rs.getString(PlMgtItems.productId.getItemName()) );
  	  entity.setCpNm( rs.getString(PlMgtItems.cpNm.getItemName()) );
  	  entity.setSerialNo( rs.getString(PlMgtItems.serialNo.getItemName()) );
  	  entity.setPlKey( rs.getString(PlMgtItems.plKey.getItemName()) );
  	  entity.setAgentPlKey( rs.getString(PlMgtItems.agentPlKey.getItemName()) );
  	  entity.setPlTimeLimit( rs.getTimestamp(PlMgtItems.plTimeLimit.getItemName()) );
  	  entity.setRemarks( rs.getString(PlMgtItems.remarks.getItemName()) );
  	  entity.setDelStsId( StatusFlg.value(rs.getBoolean(PlMgtItems.delStsId.getItemName())) );
  	  entity.setRegTimestamp( rs.getTimestamp(PlMgtItems.regTimestamp.getItemName()) );
  	  entity.setRegUserId( rs.getString(PlMgtItems.regUserId.getItemName()) );
  	  entity.setRegUserNm( rs.getString(PlMgtItems.regUserNm.getItemName()) );
  	  entity.setUpdTimestamp( rs.getTimestamp(PlMgtItems.updTimestamp.getItemName()) );
  	  entity.setUpdUserId( rs.getString(PlMgtItems.updUserId.getItemName()) );
  	  entity.setUpdUserNm( rs.getString(PlMgtItems.updUserNm.getItemName()) );

  	  return entity;
	}

}
