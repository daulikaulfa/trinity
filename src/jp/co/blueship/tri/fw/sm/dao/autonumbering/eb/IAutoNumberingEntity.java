package jp.co.blueship.tri.fw.sm.dao.autonumbering.eb;

import jp.co.blueship.tri.fw.dao.orm.IEntity;

/**
 * The interface of the auto numbering entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IAutoNumberingEntity extends IEntity {

	public String getNumberingKey();
	public void setNumberingKey(String numberingKey);

	public String getNumberingDate();
	public void setNumberingDate(String numberingDate);

	public Integer getSeqNo();
	public void setSeqNo(Integer seqNo);

}
