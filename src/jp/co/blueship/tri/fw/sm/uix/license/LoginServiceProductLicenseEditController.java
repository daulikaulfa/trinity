package jp.co.blueship.tri.fw.sm.uix.license;

import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.sm.SmLicenseEditUtils;
import jp.co.blueship.tri.fw.sm.domainx.license.dto.FlowProductLicenseEditServiceBean;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/login")
public class LoginServiceProductLicenseEditController extends TriControllerSupport<FlowProductLicenseEditServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.SmLogInServiceProductLicenseEdit;
	}

	@Override
	protected FlowProductLicenseEditServiceBean getServiceBean(ISessionInfo sesInfo) {
		return new FlowProductLicenseEditServiceBean();
	}

	@RequestMapping("/license")
	public String license(FlowProductLicenseEditServiceBean bean, TriModel model) {
		String view = TriView.LicenseRegistration.value();

		try {
			SmLicenseEditUtils.insertMapping(bean, model);
			this.execute(getServiceId(), bean, model);
			if (bean.getResult().isCompleted()) {
				bean.getMessages().clear();
			}

		} catch (Exception e) {
			ExceptionUtils.printStackTrace(e);
		}

		model.getModel().addAttribute("result", bean);
		return view;
	}
}