package jp.co.blueship.tri.fw.sm.uix.license;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.sm.SmLicenseEditUtils;
import jp.co.blueship.tri.fw.sm.domainx.license.dto.FlowProductLicenseEditServiceBean;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @version V4.00.00
 * @author thang.vu
 */
@Controller
@RequestMapping("/admin")
public class ProductLicenseEditController extends TriControllerSupport<FlowProductLicenseEditServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.SmProductLicenseEditService;
	}

	@Override
	protected FlowProductLicenseEditServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowProductLicenseEditServiceBean bean = new FlowProductLicenseEditServiceBean();
		return bean;
	}

	@RequestMapping("/license/redirect")
	public String redirectLicense(FlowProductLicenseEditServiceBean bean, TriModel model) {
		if( !bean.getMessageInfo().isEmptyFlashMessages() ) {
			bean.getMessageInfo().clear();
		}
		return updateLicense( bean , model.setRedirect(true) );
	}

	@RequestMapping("/license")
	public String updateLicense(FlowProductLicenseEditServiceBean bean, TriModel model) {
		String view = TriTemplateView.AdminSettings.value();
		List<String> flashMessages = new ArrayList<String>();

		try {
			SmLicenseEditUtils.insertMapping(bean, model);
			this.execute(getServiceId(), bean, model);
			if (bean.getResult().isCompleted()) {
				bean.getParam().setRequestType(RequestType.init);
				model.getRedirectAttributes().addFlashAttribute("result", bean);
				view = "redirect:/admin/license/redirect";
				if( !bean.getMessageInfo().isEmptyFlashMessages() ) {
					flashMessages = bean.getMessageInfo().getFlashMessages();
				}
			} else if( model.isRedirect() && !bean.getMessageInfo().isEmptyFlashMessages() ) {
				flashMessages = bean.getMessageInfo().getFlashMessages();
			}
		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.ProductLicenseEdit.value())
			.addAttribute("selectedMenu", "product")
			.addAttribute("result", bean)
		;

		setPrev(model);
		if( flashMessages.size() > 0 && bean.getMessageInfo().isEmptyFlashMessages() ){
			bean.getMessageInfo().addFlashMessages( flashMessages );
		}
		return view;
	}
}
