package jp.co.blueship.tri.fw.sm.uix.server;

import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.sm.domainx.server.dto.FlowServerStorageInformationServiceBean;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author GiAnG
 * @version 4.03.00
 */
@Controller
@RequestMapping("/admin/storage")
public class StorageInformationController extends TriControllerSupport<FlowServerStorageInformationServiceBean> {
	@Override
	public ServiceId getServiceId() {
		return ServiceId.SmServerStorageInformationService;
	}

	@Override
	protected FlowServerStorageInformationServiceBean getServiceBean(ISessionInfo sesInfo) {
		return new FlowServerStorageInformationServiceBean();
	}

	@RequestMapping
	public String calc(FlowServerStorageInformationServiceBean bean, TriModel model) {
		String view = TriTemplateView.AdminSettings.value();
		IRequestInfo requestInfo = model.getRequestInfo();
		try {
			this.execute(getServiceId(), bean.setCalc(requestInfo.getParameter("polling") == null), model);
		} catch (Exception e) {
			if (ControllerExceptionUtils.isRedirectException(e, this, bean, model, view)) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}
		model.getModel()
			 .addAttribute("view", TriView.ServerStorageInformation.value())
			 .addAttribute("selectedMenu", "storageInfo")
			 .addAttribute("result", bean);

		setPrev(model);
		return view;
	}

}