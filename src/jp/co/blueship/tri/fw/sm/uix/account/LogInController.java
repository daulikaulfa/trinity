package jp.co.blueship.tri.fw.sm.uix.account;

import jp.co.blueship.tri.fw.msg.UmMessageId;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.ViewMessageHelper;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.constants.TriSessionAttributes;
import jp.co.blueship.tri.fw.sm.domain.product.ProductActivate;
import jp.co.blueship.tri.fw.sm.domainx.account.dto.FlowLogInServiceBean;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.constants.LookAndFeel;

/**
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
@Controller
@RequestMapping(value = "/login")
public class LogInController extends TriControllerSupport<FlowLogInServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.SmLogInService;
	}

	@Override
	protected FlowLogInServiceBean getServiceBean(ISessionInfo sesInfo) {
		return new FlowLogInServiceBean();
	}

	@RequestMapping
	public String login(FlowLogInServiceBean bean, TriModel model) {
		ISessionInfo sesInfo = model.getSessionInfo();
		this.execute(getServiceId(), bean, model);
		model.getModel().addAttribute("result", bean);
		sesInfo.setAttribute(TriSessionAttributes.ProjectName.value(), bean.getProjectName());
		sesInfo.setAttribute(TriSessionAttributes.ProjectIconPath.value(), bean.getHeader().getProjectIconPath());
		return TriView.LogIn.value();
	}

	@RequestMapping(value = "/error")
	public String redirect() {
		return TriView.LogIn.value();
	}

	@RequestMapping(value = "/top")
	public String top(FlowLogInServiceBean bean, TriModel model) {

		ISessionInfo sesInfo = model.getSessionInfo();

		try {
			initParam( bean, model );

			bean.getParam().setUserId(bean.getUserId()).setRequestType(RequestType.submitChanges);
			this.execute(getServiceId(), bean, model);

			sesInfo.setAttribute(TriSessionAttributes.UserIconPath.value(), bean.getHeader().getUserIconPath());
			sesInfo.setAttribute(TriSessionAttributes.UserName.value(), bean.getUserName());

			if (bean.getDetailsView().isExpired()) {
				return "redirect:/login/passwordexpiry";
			}

			String lang = bean.getDetailsView().getLang();
			String timezone = bean.getDetailsView().getTimeZone();
			if ( TriStringUtils.isNotEmpty(lang) ) {
				sesInfo.setAttribute(TriSessionAttributes.UserLanguage.value(), lang);
			}
			if ( TriStringUtils.isNotEmpty(timezone) ) {
				sesInfo.setAttribute(TriSessionAttributes.UserTimeZone.value(), timezone);
			}

			if(!ProductActivate.isRmActive()) {
				return "redirect:/login/license";
			}
			
			if (bean.getDetailsView().getLookAndFeel().isTrinityClassic()) {
				return "redirect:/dashboardV3";
			} else {
				return "redirect:/dashboard";
			}
		} catch (BaseBusinessException bbe) {
			model.getRedirectAttributes().addFlashAttribute("result", bean);
			return "redirect:/login/error";
		} catch (TriSystemException tte) {
			return "redirect:/login/license";
		}
		finally {
		}
	}

	@RequestMapping("/error/auth")
	public String loginError(
			FlowLogInServiceBean bean,
			TriModel model) {

		model.getModel().addAttribute("result", bean);
		model.getModel().addAttribute("authError", true);
		return TriView.LogIn.value();
	}
	

	@RequestMapping("/passwordrecovery")
	public String passwordRecovery(
			FlowLogInServiceBean bean,
			TriModel model) {
		this.execute(getServiceId(), bean, model);
		model.getModel().addAttribute("result", bean);
		return TriView.PasswordRecovery.value();
	}

	@RequestMapping("/error/loginexpired")
	public String loginSessionError(
			FlowLogInServiceBean bean,
			TriModel model) {
		this.execute(getServiceId(), bean, model);
		model.getModel().addAttribute("result", bean);
		model.getModel().addAttribute("sessionerror", true);
		model.getModel().addAttribute("sessionerrorCode", UmMessageId.UM001053E.name());
		model.getModel().addAttribute("sessionerrorMessage", ViewMessageHelper.msgOfInvalidConcurrencySession(bean.getLanguage()));
		return TriView.DoubleLoginError.value();
	}

}
