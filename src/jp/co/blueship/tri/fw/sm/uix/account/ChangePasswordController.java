package jp.co.blueship.tri.fw.sm.uix.account;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.sm.contants.PasswordComplexityLevel;
import jp.co.blueship.tri.fw.sm.domainx.account.dto.FlowLogInServiceChangePasswordBean;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriView;

/**
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 */
@Controller
@RequestMapping("/login/passwordexpiry")
public class ChangePasswordController extends TriControllerSupport<FlowLogInServiceChangePasswordBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.SmLogInServiceChangePassword;
	}

	@Override
	protected FlowLogInServiceChangePasswordBean getServiceBean( ISessionInfo sesInfo ) {
		return new FlowLogInServiceChangePasswordBean();
	}

	@RequestMapping
	public String changePassword(
			FlowLogInServiceChangePasswordBean bean,
			TriModel model) {
		String view = TriView.ChangePassword.value();

		try {
			this.mapping(bean, model);
			this.execute( getServiceId(), bean, model);

			if ( RequestType.submitChanges.equals(bean.getParam().getRequestType()) ) {
				model.getModel().addAttribute( "result", bean );
				if (bean.getLoginDetailsView().getLookAndFeel().isTrinityClassic()) {
					return "redirect:/dashboardV3";
				}
				return "redirect:/dashboard/redirect";
			}

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel().addAttribute("view", TriView.ChangePassword.value());
		model.getModel().addAttribute( "result", bean );
		setPrev(model);
		return view;
	}

	@RequestMapping("/validate")
	public String validate(
			FlowLogInServiceChangePasswordBean bean,
			TriModel model) {
		String view = TriView.ChangePassword.value();

		try {
			this.mapping(bean, model);
			this.execute( getServiceId(), bean, model );

			PasswordComplexityLevel level = bean.getLoginDetailsView().getPassword().getPasswordComplexityLevel();

			if( level == PasswordComplexityLevel.none ){
				model.getModel().addAttribute( "levelClass", "level00" );
			}else if( level == PasswordComplexityLevel.Low ){
				model.getModel().addAttribute( "levelClass", "level01" );
			}else if( level == PasswordComplexityLevel.Middle ){
				model.getModel().addAttribute( "levelClass", "level02" );
			}else if( level == PasswordComplexityLevel.High ){
				model.getModel().addAttribute( "levelClass", "level03" );
			}

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel().addAttribute( "result", bean );

		return view;
	}

	private void mapping(
			FlowLogInServiceChangePasswordBean bean,
			TriModel model ) {

		IRequestInfo requestInfo = model.getRequestInfo();

		if ( requestInfo.getParameter("currentPassword") != null){
			bean.getParam().setCurrentPassword( requestInfo.getParameter("currentPassword") );
		}

		if ( requestInfo.getParameter("newPassword") != null){
			bean.getParam().setNewPassword( requestInfo.getParameter("newPassword") );
		}

		if ( requestInfo.getParameter("confirmPassword") != null ){
			bean.getParam().setConfirmPassword( requestInfo.getParameter("confirmPassword") );
		}

	}
}
