package jp.co.blueship.tri.fw.sm.uix.cmn;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.sm.domainx.cmn.dto.FlowDispatcherServiceBean;
import jp.co.blueship.tri.fw.sm.domainx.cmn.dto.FlowDispatcherServiceBean.DispatcherDetailsView;
import jp.co.blueship.tri.fw.sm.domainx.cmn.dto.FlowDispatcherServiceBean.SubmitMode;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriModelAttributes;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.um.constants.DataAttribute;
import jp.co.blueship.tri.fw.um.constants.ThemeColor;

/**
*
* @version V4.00.00
* @author nguyen.vanchung
*/

@Controller
@RequestMapping("/dispatch")
public class DispatcherController extends TriControllerSupport<FlowDispatcherServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.SmFlowDispatcherService;
	}

	@Override
	protected FlowDispatcherServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowDispatcherServiceBean bean = new FlowDispatcherServiceBean();
		return bean;
	}

	@RequestMapping
	public String index(FlowDispatcherServiceBean bean, TriModel model) {

		String view = TriTemplateView.MainTemplate.value();

		try {
			ISessionInfo sesInfo = model.getSessionInfo();

			this.insertmapping(bean, model);
			this.execute(getServiceId(), bean, model);

			Integer windowId = bean.getHeader().getWindowsId();
			DispatcherDetailsView detailView = bean.getDetailsView();
			saveLotInfoToSession(sesInfo, windowId, detailView.getLotId(), detailView.getLotSubject(), detailView.getLotIconPath(), ThemeColor.id(detailView.getThemeColor()).value(), detailView.getIsLotEnabled());
			this.setHeader(bean, sesInfo, windowId);

			if (bean.getParam().getSubmitMode() == SubmitMode.details || bean.getParam().getSubmitMode() == SubmitMode.pendingDetails) {
				setRedirectAttributes(bean, model);
			}
			if(bean.getParam().getSubmitMode() == SubmitMode.searchFilter){
				this.setRedirectFilter(bean, model);
			}

			model.getRedirectAttributes().addAttribute("WindowsId", windowId);

			if( bean.getParam().getSubmitMode() == SubmitMode.searchFilter ){
				view = "redirect:" + getUrlOfModeSearchFilter(bean.getDetailsView().getServiceId());

			} else {
				view = "redirect:" + getUrl(bean.getDetailsView().getServiceId());
			}

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}
		return view;
	}

	private void insertmapping(FlowDispatcherServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();

		if (RequestType.init.equals(bean.getParam().getRequestType())) {
			FlowDispatcherServiceBean.RequestParam param = bean.getParam();

			String submitMode = requestInfo.getParameter("submitMode");
			param.setSubmitMode(TriStringUtils.isEmpty(submitMode) ? SubmitMode.details : SubmitMode.value(submitMode));

			if (param.getSubmitMode() == SubmitMode.details) {
				String dataAttribute = requestInfo.getParameter("dataAttribute");
				param.getToDetailsInputInfo().setDataAttribute(DataAttribute.value(dataAttribute))
											 .setDataId(requestInfo.getParameter("dataId"));
			}

			if (param.getSubmitMode() == SubmitMode.pendingDetails) {
				String dataAttribute = requestInfo.getParameter("dataAttribute");
				param.getToDetailsInputInfo().setDataAttribute(DataAttribute.value(dataAttribute))
											 .setDataId(requestInfo.getParameter("dataId"));
			}


			if (param.getSubmitMode() == SubmitMode.list) {
				param.getToListInputInfo().setLotId(requestInfo.getParameter("lotId"))
										  .setServiceId(ServiceId.value(requestInfo.getParameter("serviceId")));
			}

			if(param.getSubmitMode() == SubmitMode.searchFilter){
				param.getToSearchFilterInputInfo().setDataId(requestInfo.getParameter("dataId"));
			}
		}
	}

	private void setRedirectAttributes(FlowDispatcherServiceBean bean, TriModel model){

		ServiceId serviceId = bean.getDetailsView().getServiceId();
		String dataId = bean.getDetailsView().getDataId();

		if ( serviceId.equals(ServiceId.AmLotDetailsService) ) {
			model.getRedirectAttributes().addAttribute("lotId", dataId);
		}
		else if ( serviceId.equals(ServiceId.AmChangePropertyDetailsService) ) {
			model.getRedirectAttributes().addAttribute("pjtId", dataId);
		}
		else if ( serviceId.equals(ServiceId.AmChangeApprovalPendingDetailsService) ) {
			model.getRedirectAttributes().addAttribute("pjtId", dataId);
		}
		else if ( serviceId.equals(ServiceId.AmChangeApprovedDetailsService) ) {
			model.getRedirectAttributes().addAttribute("pjtId", dataId);
		}
		else if ( serviceId.equals(ServiceId.AmCheckoutRequestDetailsService) ) {
			model.getRedirectAttributes().addAttribute("areqId", dataId);
		}

		else if( serviceId.equals( ServiceId.AmCheckinRequestService) ){
			model.getRedirectAttributes().addAttribute("areqId", dataId);
		}
		else if ( serviceId.equals(ServiceId.AmCheckinRequestDetailsService) ) {
			model.getRedirectAttributes().addAttribute("areqId", dataId);
		}
		else if ( serviceId.equals(ServiceId.AmRemovalRequestDetailsService) ) {
			model.getRedirectAttributes().addAttribute("areqId", dataId);
		}
		else if ( serviceId.equals(ServiceId.AmRemovalRequestEditService) ) {
			model.getRedirectAttributes().addAttribute("areqId", dataId);
		}
		else if ( serviceId.equals(ServiceId.BmBuildPackageDetailsService) ) {
			model.getRedirectAttributes().addAttribute("bpId", dataId);
		}
		else if ( serviceId.equals(ServiceId.RmReleaseRequestApprovalPendingDetailsService) ) {
			model.getRedirectAttributes().addAttribute("raId", dataId);
		}
		else if ( serviceId.equals(ServiceId.RmReleaseRequestApprovedDetailsService) ) {
			model.getRedirectAttributes().addAttribute("raId", dataId);
		}
		else if ( serviceId.equals(ServiceId.RmReleaseRequestEditService) ) {
			model.getRedirectAttributes().addAttribute("raId", dataId);
		}
		else if ( serviceId.equals(ServiceId.RmFlowReleasePackageDetailsService) ) {
			model.getRedirectAttributes().addAttribute("RpId", dataId);
		}
		else if ( serviceId.equals(ServiceId.DmFlowDeploymentJobDetailsService) ) {
			model.getRedirectAttributes().addAttribute("mgtVer", dataId);
		}
		else if ( serviceId.equals(ServiceId.AmConflictCheckResourceFileResultsService) ) {

		}
		else if ( serviceId.equals(ServiceId.DcmReportListService) ) {
			model.getRedirectAttributes().addAttribute("repId", dataId);
		}
		else if ( serviceId.equals(ServiceId.UmMilestoneEditService) ) {
			model.getRedirectAttributes().addAttribute("milestoneId", dataId);
		}
		else if ( serviceId.equals(ServiceId.UmCategoryEditService) ) {
			model.getRedirectAttributes().addAttribute("categoryId", dataId);
		}
		else if ( serviceId.equals(ServiceId.UmWikiDetailsService) ) {
			model.getRedirectAttributes().addAttribute("wikiId", dataId);
		}
	}

	private void setRedirectFilter(FlowDispatcherServiceBean bean, TriModel model){
		String json = bean.getDetailsView().getFilterData();

		model.getRedirectAttributes().addFlashAttribute( TriModelAttributes.RedirectFilter.value() , json );
	}

	private String getUrl(ServiceId serviceId){
		if ( serviceId.equals(ServiceId.AmLotDetailsService) ) {
			return "/lot/details";
		}

		if ( serviceId.equals(ServiceId.AmChangePropertyDetailsService) ) {
			return "/chgproperty/details";
		}

		if ( serviceId.equals(ServiceId.AmChangeApprovalPendingDetailsService) ) {
			return "/chgproperty/pending/details";
		}

		if ( serviceId.equals(ServiceId.AmCheckoutRequestDetailsService) ) {
			return "/checkout/details";
		}

		if ( serviceId.equals( ServiceId.AmCheckinRequestService) ) {
			return "/checkin/create";
		}

		if ( serviceId.equals(ServiceId.AmCheckinRequestDetailsService) ) {
			return "/checkin/details";
		}

		if ( serviceId.equals(ServiceId.AmRemovalRequestDetailsService) ) {
			return "/removal/details";
		}

		if ( serviceId.equals(ServiceId.AmRemovalRequestEditService) ) {
			return "/removal/edit";
		}

		if ( serviceId.equals(ServiceId.AmChangeApprovedDetailsService) ) {
			return "/chgproperty/approved/details";
		}

		if ( serviceId.equals(ServiceId.BmBuildPackageDetailsService) ) {
			return "/build/details";
		}

		if ( serviceId.equals(ServiceId.RmReleaseRequestApprovalPendingDetailsService) ) {
			return "/release/request/pending/details";
		}

		if ( serviceId.equals(ServiceId.RmReleaseRequestApprovedDetailsService) ) {
			return "/release/request/approved/details";
		}

		if ( serviceId.equals(ServiceId.RmReleaseRequestEditService) ) {
			return "/release/request/edit";
		}

		if ( serviceId.equals(ServiceId.RmFlowReleasePackageDetailsService) ) {
			return "/release/details";
		}

		if ( serviceId.equals(ServiceId.DmFlowDeploymentJobDetailsService) ) {
			return "/job/deploy/details";
		}

		if ( serviceId.equals(ServiceId.AmConflictCheckResourceFileResultsService) ) {
			return "/merge/conflictcheck/result";
		}

		if ( serviceId.equals(ServiceId.DcmReportListService) ) {
			return "/reports";
		}

		if ( serviceId.equals(ServiceId.UmMilestoneEditService) ) {
			return "/lot/settings/milestone/edit";
		}

		if ( serviceId.equals(ServiceId.UmCategoryEditService) ) {
			return "/lot/settings/category/edit";
		}

		if ( serviceId.equals(ServiceId.UmWikiDetailsService) ) {
			return "/lot/wiki";
		}

		if ( serviceId.equals(ServiceId.AmChangePropertyListService) ) {
			return "/chgproperty";
		}

		if ( serviceId.equals(ServiceId.AmCheckinOutRequestListService) ) {
			return "/checkinout";
		}

		if ( serviceId.equals(ServiceId.BmBuildPackageListService) ) {
			return "/build";
		}

		if ( serviceId.equals(ServiceId.DcmReportListService) ) {
			return "/reports";
		}

		if ( serviceId.equals(ServiceId.UmWikiListService) ) {
			return "/lot/wiki";
		}

		if ( serviceId.equals(ServiceId.UmGanttChartService) ) {
			return "/lot/gantt";
		}

		if ( serviceId.equals(ServiceId.UmDashBoardService) ) {
			return "/dashboard";
		}

		if ( serviceId.equals(ServiceId.BmBuildPackageCreationProcessingStatusService) ) {
			return "/build/progress";
		}

		if ( serviceId.equals(ServiceId.RmReleasePackageCreationProcessingStatusService) ) {
			return "/release/progress";
		}

		if ( serviceId.equals(ServiceId.AmMergeService) ) {
			return "/merge";
		}

		return "/dashboard";
	}

	private String getUrlOfModeSearchFilter(ServiceId serviceId){
		if (serviceId.equals(ServiceId.AmChangePropertyListService)) {
			return "/chgproperty/filter/search";
		}
		if (serviceId.equals(ServiceId.AmCheckinOutRequestListService)) {
			return "/checkinout/filter/search";
		}
		if (serviceId.equals(ServiceId.AmRemovalRequestListService)) {
			return "/removal/filter/search";
		}
		if (serviceId.equals(ServiceId.AmChangeApprovalPendingListService)) {
			return "/chgproperty/pending/filter/search";
		}
		if (serviceId.equals(ServiceId.AmChangeApprovedListService)) {
			return "/chgproperty/approved/filter/search";
		}
		if (serviceId.equals(ServiceId.BmBuildPackageListService)) {
			return "/build/filter/search";
		}
		if (serviceId.equals(ServiceId.RmReleasePackageListService)) {
			return "/release/filter/search";
		}
		if (serviceId.equals(ServiceId.RmReleaseRequestApprovalPendingListService)) {
			return "/release/request/pending/filter/search";
		}
		if (serviceId.equals(ServiceId.RmReleaseRequestApprovedListService)) {
			return "/release/request/approved/filter/search";
		}
		if (serviceId.equals(ServiceId.DmFlowDeploymentJobListService)) {
			return "/job/deploy/filter/search";
		}
		if (serviceId.equals(ServiceId.DcmReportListService)) {
			return "/reports/filter/search";
		}
		if (serviceId.equals(ServiceId.RmReleaseRequestListService)) {
			return "/release/request/filter/search";
		}
		if (serviceId.equals(ServiceId.UmSearchThisLotService)) {
			return "/lot/search/filter/search";
		}
		if (serviceId.equals(ServiceId.UmSearchThisSiteService)) {
			return "/project/search/filter/search";
		}

		return "/dashboard";
	}
}