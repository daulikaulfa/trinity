package jp.co.blueship.tri.fw.sm.uix.cmn;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.constants.TriSessionAttributes;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;

/**
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
@Controller
@RequestMapping("/")
public class HomeController extends TriControllerSupport<DomainServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.none;
	}

	@Override
	protected DomainServiceBean getServiceBean(ISessionInfo sesInfo) {
		return null;
	}

	@RequestMapping
    public String index(TriModel model) {
        return "index";
    }

    @RequestMapping("/error")
    public String error(HttpServletRequest request, TriModel model) {
      model.getModel().addAttribute("errorCode", request.getAttribute("javax.servlet.error.status_code"));
      Throwable throwable = (Throwable) request.getAttribute("javax.servlet.error.exception");
      String errorMessage = null;
      if (throwable != null) {
        errorMessage = throwable.getMessage();
      }
      model.getModel().addAttribute("errorMessage", errorMessage);
      return "common/Error";
    }

    @RequestMapping("/error/404")
    public String error404(HttpServletRequest request, TriModel model) {
      model.getModel()
      	.addAttribute("errorCode", "404")
      	.addAttribute("errorMessage", "The content could not be found.");
      return "common/Error";
    }


	@RequestMapping(value = "/common/windows-id/save")
	public @ResponseBody String addWindowsIdRedirect(DomainServiceBean bean, TriModel model) {
		return "";
	}

	@RequestMapping(value = "/common/windows-id/remove")
	public @ResponseBody String removeWindowsIdRedirect(DomainServiceBean bean, TriModel model) {
		ISessionInfo sesInfo = model.getSessionInfo();
		Integer windowId = (Integer)sesInfo.getAttribute(TriSessionAttributes.RedirectWindowsId.value());
		if (windowId != null && Integer.valueOf(windowId) != 0) {
			sesInfo.removeAttribute(TriSessionAttributes.RedirectWindowsId.value());

			// retrieve lot information from current session-windowsid and set to session
			String selectedLotId = (String)sesInfo.getAttribute(this.getSessionKey(Integer.valueOf(windowId), TriSessionAttributes.SelectedLotId.value()));
			String selectedLotThemeColor = (String)sesInfo.getAttribute(this.getSessionKey(Integer.valueOf(windowId), TriSessionAttributes.SelectedLotThemeColor.value()));
			String selectedLotIconPath = (String)sesInfo.getAttribute(this.getSessionKey(Integer.valueOf(windowId), TriSessionAttributes.SelectedLotIconPath.value()));
			String selectedLotName = (String)sesInfo.getAttribute(this.getSessionKey(Integer.valueOf(windowId), TriSessionAttributes.SelectedLotName.value()));
			Boolean isLotEnabled = (Boolean)sesInfo.getAttribute(this.getSessionKey(Integer.valueOf(windowId), TriSessionAttributes.SelectedLotEnabled.value()));

			saveLotInfoToSession(sesInfo, 0, selectedLotId, selectedLotName, selectedLotIconPath, selectedLotThemeColor, isLotEnabled);

		}

		return "";
	}

    @SuppressWarnings({ "unchecked" })
	public DomainServiceBean getParam(
			TriModel model,
			String windowsId,
			String requestType,
			String forward,
			String referer) {

    	ISessionInfo sesInfo = model.getSessionInfo();
    	DomainServiceBean bean = new DomainServiceBean();

    	if (!(TriStringUtils.isEmpty(windowsId) || "undefined".equalsIgnoreCase(windowsId) || Integer.valueOf(windowsId) == 0)) {
    		Integer redirectWindowsId = Integer.valueOf(windowsId);
    		sesInfo.setAttribute(TriSessionAttributes.RedirectWindowsId.value(), redirectWindowsId);

    	}

		return bean;
	}
}
