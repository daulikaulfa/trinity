package jp.co.blueship.tri.fw.dao.oxm;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.SystemProps;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.UmDesignBeanId;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.sm.dao.funcsvc.IFuncSvcDao;
import jp.co.blueship.tri.fw.sm.dao.funcsvc.eb.FuncSvcCondition;
import jp.co.blueship.tri.fw.sm.dao.funcsvc.eb.IFuncSvcEntity;
import jp.co.blueship.tri.fw.sm.dao.sysdef.ISysDefDao;
import jp.co.blueship.tri.fw.sm.dao.sysdef.constants.SysDefItems;
import jp.co.blueship.tri.fw.sm.dao.sysdef.eb.ISysDefEntity;
import jp.co.blueship.tri.fw.sm.dao.sysdef.eb.SysDefCondition;
import jp.co.blueship.tri.fw.sm.dao.sysdef.eb.SysDefEntity;
import jp.co.blueship.tri.fw.support.FwFinderSupport;

/**
 * デザインシート（ユーザ設計項目）文書情報を管理するクラスです。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public class DesignSheetProduct implements IDesignSheet {
	private Map<String, DesignSheet> designMap = new ConcurrentHashMap<String, DesignSheet>();
	private FwFinderSupport finder = null;

	/**
	 * デザインXML文書のファイル名がDIコンテナ設定されます。 <br>
	 * このmethodはインスタンス生成時に一度だけDIコンテナから呼び出されます。
	 *
	 * @param fileName デザインXML文書のファイル名
	 */
	public final void setDesignSheet(List<String> fileNames) {

		for (String fileName : fileNames) {
			try {
				DesignSheet sheet = new DesignSheet();
				sheet.setFileName(withLocale(fileName));

				designMap.put(sheet.getDesignSheetId(), sheet);

			} catch (Exception e) {
				e.printStackTrace();
				throw new TriSystemException(SmMessageId.SM005010S, e, new String[] { fileName });
			}
		}
	}

	/**
	 * データアクセス支援がDIコンテナ設定されます。
	 *
	 * @param finder データアクセス支援
	 */
	public final void setFinder(FwFinderSupport finder) {
		this.finder = finder;
	}

	/**
	 * データアクセス支援を取得します。
	 *
	 * @return データアクセス支援を戻します。
	 */
	public final FwFinderSupport getFinder() {

		if (null == this.finder) {
			throw new TriSystemException(SmMessageId.SM005013S, new String[] { FwFinderSupport.class.getName() });
		}

		return this.finder;
	}

	@Override
	public Map<String, String> getKeyMap(IDesignBeanId beanId) {
		this.checkContains(beanId);
		Map<String, String> map = designMap.get(beanId.getSheetId()).getKeyMap(beanId.getBeanId());

		if (null == map) {
			throw new TriSystemException(SmMessageId.SM005012S, new String[] { beanId.getSheetId(), beanId.getBeanId() });
		}

		return map;
	}

	@Override
	public Map<String, String> getKeyMap(IDesignBeanId.Sheet sheet, String beanId) {
		this.checkContains(sheet);
		Map<String, String> map = designMap.get(sheet.value()).getKeyMap(beanId);

		if (null == map) {
			throw new TriSystemException(SmMessageId.SM005012S, new String[] { sheet.value(), beanId });
		}

		return map;
	}

	@Override
	public Map<String, String> getValueMap(IDesignBeanId beanId) {
		this.checkContains(beanId);
		Map<String, String> map = designMap.get(beanId.getSheetId()).getValueMap(beanId.getBeanId());

		if (null == map) {
			throw new TriSystemException(SmMessageId.SM005012S, new String[] { beanId.getSheetId(), beanId.getBeanId() });
		}

		return map;
	}

	@Override
	public List<String> getKeyList(IDesignBeanId beanId) {
		Map<String, String> map = this.getValueMap(beanId);

		return new ArrayList<String>(map.values());
	}

	@Override
	public List<String> getValueList(IDesignBeanId beanId) {
		Map<String, String> map = this.getKeyMap(beanId);

		return new ArrayList<String>(map.values());
	}

	@Override
	public String getKey(IDesignBeanId beanId, String value) {

		if (null == beanId || TriStringUtils.isEmpty(value)) {
			return "";
		}
		Map<String, String> prop = this.getValueMap(beanId);
		return prop.get(value);
	}

	@Override
	public String getValue(IDesignBeanId beanId, String key) {

		if (null == beanId || TriStringUtils.isEmpty(key)) {
			return "";
		}
		Map<String, String> prop = this.getKeyMap(beanId);
		return prop.get(key);
	}

	@Override
	public Integer intValue(IDesignBeanId beanId, String key) {

		String value = getValue(beanId, key);

		int intValue = 0;

		try {
			intValue = Integer.valueOf(value).intValue();
		} catch (NumberFormatException nfe) {
			throw new TriSystemException(SmMessageId.SM005015S, nfe, new String[] { beanId.getSheetId(), beanId.getBeanId(), key, value });
		}

		return intValue;

	}

	@Override
	public Integer intValue(IDesignEntryKey key) {
		if (null == key) {
			return 0;
		}

		return this.intValue(key.getDesignBeanId(), key.getKey());

	}

	@Override
	public String getValue(IDesignEntryKey key) {
		if (null == key) {
			return "";
		}

		String value = this.getValue(key.getDesignBeanId(), key.getKey());
//
//		if (TriStringUtils.isEmpty(value)) {
//			throw new TriSystemException(SmMessageId.SM005016S, key.toString());
//		}

		return value;
	}

	private void checkContains(IDesignBeanId beanId) {

		if (null == beanId) {
			throw new TriSystemException(SmMessageId.SM005011S, new String[] { "null" });
		}

		if (!designMap.containsKey(beanId.getSheetId())) {
			throw new TriSystemException(SmMessageId.SM005011S, new String[] { beanId.getSheetId() });
		}
	}

	private void checkContains(IDesignBeanId.Sheet sheet) {

		if (null == sheet) {
			throw new TriSystemException(SmMessageId.SM005011S, new String[] { "null" });
		}

		if (!designMap.containsKey(sheet.value())) {
			throw new TriSystemException(SmMessageId.SM005011S, new String[] { sheet.value() });
		}
	}

	@Override
	public void init() {

		this.syncSystemDefinition();
		this.refreshDesign();

		if (!designMap.containsKey(IDesignBeanId.Sheet.umDesignSheet.value())) {
			DesignSheet sheet = new DesignSheet();
			designMap.put(IDesignBeanId.Sheet.umDesignSheet.value(), sheet);
		}

		// アクションのキャッシング
		try {
			IFuncSvcDao dao = finder.getSmFinderSupport().getFuncSvcDao();
			FuncSvcCondition condition = new FuncSvcCondition();
			condition.setDelStsId(StatusFlg.off);
			List<IFuncSvcEntity> entities = dao.find(condition.getCondition());

			Map<String, String> key = this.getKeyMap(UmDesignBeanId.actionId);
			Map<String, String> value = this.getKeyMap(UmDesignBeanId.actionId);

			for (IFuncSvcEntity entity : entities) {
				key.put(entity.getSvcId(), entity.getSvcNm());
				value.put(entity.getSvcNm(), entity.getSvcId());
			}
		} catch (Exception e) {
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(SmMessageId.SM005014S, new String[] { IDesignBeanId.Sheet.umDesignSheet.value(),
					UmDesignBeanId.actionId.name() });
		}

	}

	/**
	 * システム定義テーブル情報と同期を行う
	 */
	private void syncSystemDefinition() {
		ISysDefDao dao = finder.getSmFinderSupport().getSysDefDao();

		List<ISysDefEntity> insertEntities = new ArrayList<ISysDefEntity>();
		//List<ISysDefEntity> updateEntities = new ArrayList<ISysDefEntity>();

		for (Entry<String, DesignSheet> sysEntry : designMap.entrySet()) {
			String sysDefId = sysEntry.getKey();
			DesignSheet sheet = sysEntry.getValue();

			for (Entry<String, Map<String, String>> beanEntry : sheet.entrySetKey()) {
				String beanId = beanEntry.getKey();
				Map<String, String> map = beanEntry.getValue();

				if (UmDesignBeanId.common.getSheetId().equals(sysDefId) && UmDesignBeanId.common.getBeanId().equals(beanId)) {
					// 共通項目は各Managerで個別にLoadingするためDBには記録しない
					continue;
				}

				{
					SysDefCondition condition = new SysDefCondition();
					condition.setSysDefId(sysDefId);
					condition.setSetId(beanId);

					dao.delete(condition.getCondition());
				}

				int seq = 0;
				for (Entry<String, String> keyEntry : map.entrySet()) {

					SysDefCondition condition = new SysDefCondition();
					condition.setSysDefId(sysDefId);
					condition.setSetId(beanId);
					condition.setSetKey(keyEntry.getKey());

					//ISysDefEntity entity = dao.findByPrimaryKey(condition.getCondition());
					//boolean isNew = (null == entity)? true: false;

					ISysDefEntity entity = new SysDefEntity();
					entity.setSysDefId(sysDefId);
					entity.setSetId(beanId);
					entity.setSetKey(keyEntry.getKey());
					entity.setSetValue(keyEntry.getValue());
					entity.setSortOdr(++seq);

					//if (isNew) {
						insertEntities.add(entity);
					//} else {
					//	updateEntities.add(entity);
					//}
				}
			}
		}

		dao.insert(insertEntities);
		//dao.update(updateEntities);
	}

	/**
	 * システム定義テーブルの最新情報をデザインシート（ユーザ設計項目）文書情報に反映します。
	 */
	private void refreshDesign() {
		// Map<String, DesignSheet> sysDefMap = new ConcurrentHashMap<String,
		// DesignSheet>();

		ISysDefDao dao = finder.getSmFinderSupport().getSysDefDao();
		SysDefCondition condition = new SysDefCondition();
		condition.setDelStsId(StatusFlg.off);
		ISqlSort sort = new SortBuilder()
			.setElement(SysDefItems.sysDefId, TriSortOrder.Asc, 1)
			.setElement(SysDefItems.setId, TriSortOrder.Asc, 2)
			.setElement(SysDefItems.sortOdr, TriSortOrder.Asc, 3);

		for (ISysDefEntity entity : dao.find(condition.getCondition(), sort)) {
			String sysDefId = entity.getSysDefId();
			String beanId = entity.getSetId();
			String key = entity.getSetKey();
			String value = entity.getSetValue();

			DesignSheet sheet = null;
			if (!designMap.containsKey(sysDefId)) {
				sheet = new DesignSheet();
				designMap.put(sysDefId, sheet);
			}

			sheet = designMap.get(sysDefId);
			sheet.getKeyMap(beanId).put(key, value);
			sheet.getValueMap(beanId).put(value, key);
		}

		// designMap = sysDefMap;
	}

	private String withLocale(String fileName) {

		return TriStringUtils.getBaseName(fileName) + //
				"_" + //
				SystemProps.TriSystemLanguage.getProperty() + //
				"." + //
				TriStringUtils.getExtension(fileName);
	}

}
