package jp.co.blueship.tri.fw.dao.oxm;

import java.util.List;
import java.util.Map;

/**
 * デザインシート（ユーザ設計項目）文書情報を管理するインタフェースです。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public interface IDesignSheet {
	/**
	 * 初期化を行います。
	 * <br>このmethodはインスタンス生成時に一度だけ呼び出されます。
	 */
	public void init();

	/**
	 * 指定されたＩＤに対応するセットを取得します。
	 * <br>Key情報をkeyとしてvalue情報を取得するセットを戻します。
	 *
	 * @param beanId リソースを特定するＩＤ
	 * @return セットを戻します。
	 */
	public Map<String, String> getKeyMap( IDesignBeanId beanId );

	/**
	 * 指定されたＩＤに対応するセットを取得します。
	 * <br>Key情報をkeyとしてvalue情報を取得するセットを戻します。
	 *
	 * @param sheet 定義リソースを識別するためのＩＤ
	 * @param beanId リソースを特定するＩＤ
	 * @return セットを戻します。
	 * @Deprecated V2互換性のための機能。原則として使用しないこと。
	 */
	@Deprecated
	public Map<String, String> getKeyMap( IDesignBeanId.Sheet sheet, String beanId );

	/**
	 * 指定されたＩＤに対応するセットを取得します。
	 * <br>value情報をkeyとしてkey情報を取得するセットを戻します。
	 *
	 * @param beanId リソースを特定するＩＤ
	 * @return セットを戻します。
	 */
	public Map<String, String> getValueMap( IDesignBeanId beanId );

	/**
	 * 定義名に該当するキーのリストを取得します。
	 *
	 * @param beanId リソースを特定するＩＤ
	 * @return セットを戻します。
	 */
	public List<String> getKeyList( IDesignBeanId beanId );

	/**
	 * 定義名に該当する値のリストを取得します。
	 *
	 * @param beanId リソースを特定するＩＤ
	 * @return セットを戻します。
	 */
	public List<String> getValueList( IDesignBeanId beanId );

	/**
	 * 定義名、値に該当するキーを取得します。
	 *
	 * @param beanId リソースを特定するＩＤ
	 * @param value 値
	 * @return キー
	 */
	public String getKey( IDesignBeanId beanId, String value );

	/**
	 * 定義名、キーに該当する値を取得します。
	 * <br>StatusID等、keyが可変の場合に使用します。
	 * <br>基本的に{@link IDesignSheet#getValue(IDesignBeanId)}を使用して下さい。
	 *
	 * @param beanId リソースを特定するＩＤ
	 * @param key キー
	 * @return 値
	 */
	public String getValue( IDesignBeanId beanId, String key );

	/**
	 * 定義名、値に該当するキーを取得します。
	 * <br>Returns the value of this Integer.
	 *
	 * @param beanId リソースを特定するＩＤ
	 * @param key キー
	 * @return 値
	 */
	public Integer intValue( IDesignBeanId beanId, String key );

	/**
	 * 定義名、キーに該当する値を取得します。
	 *
	 * @param key キー
	 * @return 値
	 */
	public String getValue( IDesignEntryKey key );

	/**
	 * 定義名、キーに該当する値を取得します。
	 *
	 * @param key キー
	 * @return 値
	 */
	public Integer intValue( IDesignEntryKey key );


}
