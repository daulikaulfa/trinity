package jp.co.blueship.tri.fw.dao.oxm;


/**
 * デザインシート（ユーザ設計項目）文書情報を識別するためのEntry Keyインタフェースです。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public interface IDesignEntryKey {

	/**
	 * デザインシート（ユーザ設計項目）文書情報を識別するインタフェースを取得します。
	 * @return 取得したIDを戻します。
	 */
	public IDesignBeanId getDesignBeanId();

	/**
	 * デザインシートのEntry-Keyを特定するKeyを取得します。
	 * @return 取得したIDを戻します。
	 */
	public String getKey();

}
