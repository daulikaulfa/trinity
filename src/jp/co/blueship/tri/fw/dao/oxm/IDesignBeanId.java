package jp.co.blueship.tri.fw.dao.oxm;


/**
 * デザインシート（ユーザ設計項目）文書情報を識別するためのIDインタフェースです。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public interface IDesignBeanId {
	enum Sheet {
		designInit( "Design-Init" ),
		umDesignSheet( "Um-Design-Sheet" ),
		amDesignSheet( "Am-Design-Sheet" ),
		rmDesignSheet( "Rm-Design-Sheet" ),
		dmDesignSheet( "Dm-Design-Sheet" ),
		dcmDesignSheet( "Dcm-Design-Sheet" ),
		smDesignSheet( "Sm-Design-Sheet" );

		private String value = null;

		private Sheet( String value) {
			this.value = value;
		}

		public String value() {
			return this.value;
		}

		public static Sheet enumValue( String value ) {
			for ( Sheet sheet : values() ) {
				if ( sheet.value().equals(value) )
					return sheet;
			}

			return null;
		}
	}

	/**
	 * デザインシートのリソースを特定するIDを取得します。
	 * @return 取得したIDを戻します。
	 */
	public String getSheetId();

	/**
	 * デザインシートのBean-IDを特定するIDを取得します。
	 * @return 取得したIDを戻します。
	 */
	public String getBeanId();

}
