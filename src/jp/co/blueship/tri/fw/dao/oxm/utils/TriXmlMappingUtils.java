package jp.co.blueship.tri.fw.dao.oxm.utils;

import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.ConvertUtilsBean;
import org.apache.commons.beanutils.Converter;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.beanutils.PropertyUtilsBean;
import org.apache.commons.lang.StringUtils;
import org.apache.xmlbeans.XmlCalendar;
import org.apache.xmlbeans.XmlObject;
import org.apache.xmlbeans.impl.values.XmlComplexContentImpl;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.dao.orm.IEntity;

/**
 * XMLBeansのEntityとDartaBaseのEntity、双方向のマッピング基底クラス。
 *
 * 「独自コンバーターの登録には、ConvertUtils#register()というメソッドを使います。
 * これを呼び出すと、デフォルトのConvertUtilsBeanインスタンスにコンバーターを登録することになり、
 * BeanUtilsを使っている他のライブラリー（Struts等）の変換にも影響を与えてしまって好ましくありません。
 *
 * そこで、独自変換を行いたい場合は、BeanUtilsBeanを自分で生成して使用し、
 * BeanUtilsは使わないようにします。」
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Eguchi Yukihiro
 *
 */
public class TriXmlMappingUtils {



	private static boolean isFullCheck = false;

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param src 複写元
	 * @param dst 複写先
	 */
	protected final void copyProperties (Object src, Object dst) {

		try {
			if ( isFullCheck ) {
				this.amendProperties( src );
			}

			bu.copyProperties(dst, src);

		} catch (IllegalAccessException e) {
			e.printStackTrace();
			throw new RuntimeException( e.getMessage() );
		} catch (InvocationTargetException e) {
			e.printStackTrace();
			throw new RuntimeException( e.getMessage() );
		}
	}

	/**
	 * Copy property values from the XmlObject to the destination bean for all cases where the property names are the same.
	 *
	 * @param src Origin bean whose properties are retrieved
	 * @param dst Destination bean whose properties are modified
	 */
	protected final void copyPropertiesOfHistData(XmlObject src, IEntity dst) {

		try {
			bux.copyProperties(dst, src);

		} catch (IllegalAccessException e) {
			e.printStackTrace();
			throw new RuntimeException( e.getMessage() );
		} catch (InvocationTargetException e) {
			e.printStackTrace();
			throw new RuntimeException( e.getMessage() );
		}
	}

	/**
	 * ピンポイントで指定された項目のみを複写元から複写先へコピーします。
	 *
	 * @param src 複写元
	 * @param dst 複写先
	 * @param pinpoint 抽出する項目をbooleanで定義されたsetter/getterクラス
	 * @param value 抽出対象の値
	 */
	protected final void copyProperties (Object src, Object dst, Object pinpoint, boolean value) {

		try {
			PropertyDescriptor[] propertys = PropertyUtils.getPropertyDescriptors( pinpoint );

			for ( PropertyDescriptor property :propertys ) {
				if ( ! property.getPropertyType().equals(boolean.class) ) {
					continue;
				}

				String propertyName = property.getName();

				boolean isUpdate = ((Boolean)PropertyUtils.getProperty(pinpoint, propertyName)).booleanValue();
				if ( value != isUpdate ) {
					continue;
				}

				this.amendProperty( src, property );
				bu.copyProperty( dst, propertyName, PropertyUtils.getProperty(src, propertyName) );
			}

		} catch (IllegalAccessException e) {
			e.printStackTrace();
			throw new RuntimeException( e.getMessage() );
		} catch (InvocationTargetException e) {
			e.printStackTrace();
			throw new RuntimeException( e.getMessage() );
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
			throw new RuntimeException( e.getMessage() );
		}

	}

	/**
	 * XML文書オブジェクトの値を修正します。
	 * <br>
	 * <br>指定されたオブジェクトにCalender型が含まれる場合、nullを設定して
	 * 文字列にtoStringすると、lendDate="" のように空値変換されてしまいます。
	 * <br>このような""（空文字）をバインドすると値の取得時にエラーとなってしまうため、
	 * あらかじめnullを再設定し直します。
	 *
	 * @param obj コピー元のオブジェクト
	 */
	private void amendProperties( Object obj ) {
		if ( ! (obj instanceof XmlComplexContentImpl) ) {
			return;
		}

		PropertyDescriptor[] propertys = PropertyUtils.getPropertyDescriptors( obj );

		for ( PropertyDescriptor property : propertys ) {
			this.amendProperty( obj, property );
		}
	}

	/**
	 * XML文書オブジェクトの値を修正します。
	 *
	 * @param obj 対象オブジェクト
	 * @param property 対象プロパティ定義
	 */
	private void amendProperty( Object obj, PropertyDescriptor property ) {
		if ( ! this.isCalender(obj, property) ) {
			return;
		}

		try {
			String propertyName = property.getName();

			String capitalPropertyName = StringUtils.capitalize( propertyName );

			@SuppressWarnings("rawtypes")
			Method method = obj.getClass().getMethod( "xget" + capitalPropertyName, (Class)null );
			Object value = method.invoke( obj, (Object)null );

			if ( null == value ) {
				return;
			}

			//空値判定
			if ( "<xml-fragment/>".equals(value.toString()) ) {
				this.setProperty( obj, propertyName, null );
			}

		} catch (IllegalAccessException e) {
			e.printStackTrace();
			throw new RuntimeException( e.getMessage() );
		} catch (InvocationTargetException e) {
			e.printStackTrace();
			throw new RuntimeException( e.getMessage() );
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
			throw new RuntimeException( e.getMessage() );
		} finally {

		}
	}

	/**
	 * XMLBeansの{@link java.util.Calendar}型かどうかを判定します。
	 *
	 * @param obj 対象オブジェクト
	 * @param property 対象プロパティ定義
	 * @return 真の場合、true。それ以外はfalseを戻します。
	 */
	private final boolean isCalender( Object obj, PropertyDescriptor property ) {
		if ( ! (obj instanceof XmlComplexContentImpl) ) {
			return false;
		}

		//Calendar型のみチェック
		if ( ! property.getPropertyType().equals(java.util.Calendar.class) ) {
			return false;
		}

		String propertyName = property.getName();

		if ( "calendarValue".equals( propertyName ) ) {
			return false;
		}

		return true;
	}

	/**
	 *
	 * @param obj 対象オブジェクト
	 * @param propertyName 対象プロパティ
	 * @param value 設定値
	 */
	private final void setProperty( Object obj, String propertyName, Object value ) {
		try {
			PropertyUtils.setProperty( obj, propertyName, value );

		} catch (IllegalAccessException e) {
			e.printStackTrace();
			throw new RuntimeException( e.getMessage() );
		} catch (InvocationTargetException e) {
			e.printStackTrace();
			throw new RuntimeException( e.getMessage() );
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
			throw new RuntimeException( e.getMessage() );
		}
	}

	/**
	 * XMLBeansのEntityからDataBaseのEntityにMappingする際のConverter
	 * @author kanda
	 *
	 */
	private class X2DConverter implements Converter {

		/* (non-Javadoc)
		 * @see org.apache.commons.beanutils.Converter#convert(java.lang.Class, java.lang.Object)
		 */
		public Object convert(@SuppressWarnings("rawtypes") Class type, Object obj) {

			if(String.class == type) {

				if(obj instanceof String) {
					return obj;
				}

				return null;

			}else if(Date.class == type) {

				if(obj instanceof XmlCalendar) {
					return (Date)((XmlCalendar)obj).getTime();
				}

				return null;
			}else if(Integer.class == type) {

				if(obj instanceof BigInteger) {
					return new Integer( ((BigInteger)obj).intValue() );
				}

				return new Integer(0);
			} else {
				return null;
			}
		}

	}

	/**
	 * DataBaseのEntityからXMLBeansのEntityにMappingする際のConverter
	 * @author kanda
	 *
	 */
	private class D2XConverter implements Converter {

		/* (non-Javadoc)
		 * @see org.apache.commons.beanutils.Converter#convert(java.lang.Class, java.lang.Object)
		 */
		public Object convert(@SuppressWarnings("rawtypes") Class type, Object obj) {

			if(String.class == type) {

				if(obj instanceof String) {
					return obj;
				}

				return null;

			}else if(Calendar.class == type) {

				if(obj instanceof Date) {
					return new XmlCalendar((Date)obj);
				}

				return null;
			}else if(BigInteger.class == type) {

				if(obj instanceof Integer) {
					return BigInteger.valueOf( ((Integer)obj).intValue() );
				}

				return BigInteger.valueOf(0);
			} else {
				return null;
			}
		}
	}

	public final class X2DHistDataConverter implements Converter {

		public Object convert(@SuppressWarnings("rawtypes") Class type, Object obj) {
			if (obj == null) {
				return null;
			} else {
				if (java.sql.Timestamp.class == type && String.class == obj.getClass()) {
					return TriDateUtils.convertStringToTimestamp( (String)obj, TriDateUtils.getTimestampFormat() );
				}

				if (StatusFlg.class == type && String.class == obj.getClass()) {
					return StatusFlg.value((String)obj);
				}

				return obj;
			}
		}
	}

	/**
	 * 独自コンバーターを登録する為に新しいインスタンスを生成
	 * デフォルトのPropertyUtilsBeanを共用
	 */
	private static final BeanUtilsBean bu =
								new BeanUtilsBeanEx(	new ConvertUtilsBean(),
													BeanUtilsBean.getInstance().getPropertyUtils() );

	{
		if ( isFullCheck ) {
			// 独自BeanUtilsBeanに独自コンバーターをセットしてdstへコピー
			bu.getConvertUtils().register(
					new D2XConverter(), 	//独自コンバーターを
					String.class			//String型への変換として登録
			);

			bu.getConvertUtils().register(
					new X2DConverter(), 	//独自コンバーターを
					String.class			//String型への変換として登録
			);

			bu.getConvertUtils().register(
					new D2XConverter(), 	//独自コンバーターを
					Calendar.class			//Calendar型への変換として登録
			);

			bu.getConvertUtils().register(
					new X2DConverter(), 	//独自コンバーターを
					Date.class				//Date型への変換として登録
			);

			bu.getConvertUtils().register(
					new D2XConverter(), 	//{@see org.apache.commons.beanutils.converters.BigIntegerConverter}の
					BigInteger.class		//nullの扱いを参照（nullのときerrorとする仕様に対応）
			);

			bu.getConvertUtils().register(
					new X2DConverter(), 	//{@see org.apache.commons.beanutils.converters.BigIntegerConverter}の
					Integer.class			//nullの扱いを参照（nullのときerrorとする仕様に対応）
			);
		}

	}

	/**
	 * 独自コンバーターを登録する為に新しいインスタンスを生成
	 * デフォルトのPropertyUtilsBeanを共用
	 */
	private static final BeanUtilsBean bux =
								new BeanUtilsBeanEx(	new ConvertUtilsBean(),
													BeanUtilsBean.getInstance().getPropertyUtils() );

	{
			bux.getConvertUtils().register(
					new X2DHistDataConverter(),
					java.sql.Timestamp.class
					);

			bux.getConvertUtils().register(
					new X2DHistDataConverter(),
					StatusFlg.class
					);
	}

	/**
	 * XMLでは「null指定」の扱いが通常のObjectとは異なります。
	 * XMLBeansへの、nullの指定を行わないように変更します。
	 * （複写先は、常に新規のインスタンスであり、nullの複写を行う必要がありません）
	 */
	public static class BeanUtilsBeanEx extends BeanUtilsBean {
	    public BeanUtilsBeanEx() {
	        super();
	    }

	    public BeanUtilsBeanEx(ConvertUtilsBean convertUtilsBean, PropertyUtilsBean propertyUtilsBean) {
	    	super( convertUtilsBean, propertyUtilsBean );
	    }

	    public void copyProperty(Object bean, String name, Object value)
        		throws IllegalAccessException, InvocationTargetException {
	    	//複写元が空文字""の場合、nullとして扱うよう変更
	    	if ( value instanceof String && "".equals(value) ) {
	    		value = null;
	    	}

	    	//複写元がnullの場合、複写を行わないよう変更
	    	if ( null == value && (bean instanceof XmlObject) ) {
	    		return;
	    	}

	    	super.copyProperty( bean, name, value );
	    }
	}

}
