package jp.co.blueship.tri.fw.dao.oxm.utils;

import org.apache.xmlbeans.XmlObject;

/**
 * XML文書を扱うためのユーティリティークラスです。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public class TriXmlUtils {

	/**
	 * XML文書の文字列から余分な文字を取り除きます。
	 * <BR>・要素の終端と要素の開始の間の空白（現在無効）
	 * <BR>・名前空間
	 *
	 * @param xml XMLBeansのDocumentオブジェクト
	 * @return 編集したXML文字列を戻します。
	 */
    public static String replaceExcessString( XmlObject xml ) {
    	String nameSpace = xml.getDomNode().getFirstChild().getNamespaceURI();
		String replaceString = xml.toString();

		replaceString = replaceString.replaceAll( " xmlns=\"" + nameSpace + "\"", "" );

		return replaceString;
	}

}
