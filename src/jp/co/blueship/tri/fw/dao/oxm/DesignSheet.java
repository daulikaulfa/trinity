package jp.co.blueship.tri.fw.dao.oxm;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;

import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.DesignUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.trinity.common.schema.beans.design.DesignDocument;
import jp.co.blueship.trinity.common.schema.beans.design.DesignDocument.Design;
import jp.co.blueship.trinity.common.schema.beans.design.DesignDocument.Design.Bean;
import jp.co.blueship.trinity.common.schema.beans.design.DesignDocument.Design.Bean.Entry;

/**
 * デザインシート（ユーザ設計項目）文書情報を管理するクラスです。
 *
 * @version V3L10.01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class DesignSheet {

	private static final String LS_NAMESPACES_KEY = "";
	private static final String LS_NAMESPACES_VAL = "http://www.blueship.co.jp/tri/fw/schema/beans/design";
	private Map<String, String> xmlNamespaceMap = new HashMap<String, String>();
	private XmlOptions opt = new XmlOptions(); /*
												 * http://xmlbeans.apache.org/docs
												 * /
												 * 2.0.0/reference/constant-values
												 * .html#org.apache.xmlbeans.
												 * XmlOptions
												 * .COMPILE_MDEF_NAMESPACES
												 */

	private Map<String, Map<String, String>> designKeyProp = new Hashtable<String, Map<String, String>>();
	private Map<String, Map<String, String>> designValueProp = new Hashtable<String, Map<String, String>>();

	private String designSheetId = null;

	/**
	 * デザインXML文書のファイル名が設定されます。
	 *
	 * @param fileName デザインXML文書のファイル名
	 */
	public final void setFileName(String fileName) {
		this.mapping(fileName, DesignUtils.getPriorityDefineDesignPath());
	}

	public DesignSheet() {
		this.addNameSpace(LS_NAMESPACES_KEY, LS_NAMESPACES_VAL);
	}

	public String getDesignSheetId() {
		return designSheetId;
	}

	/**
	 * Key情報の全セットを取得します。
	 *
	 * @return セットを戻します。
	 */
	public Set<Map.Entry<String, Map<String, String>>> entrySetKey() {
		return designKeyProp.entrySet();
	}

	/**
	 * Value情報の全セットを取得します。
	 *
	 * @return セットを戻します。
	 */
	public Set<Map.Entry<String, Map<String, String>>> entrySetValue() {
		return designValueProp.entrySet();
	}

	/**
	 * 指定されたＩＤに対応するセットを取得します。 <br>
	 * Key情報をkeyとしてvalue情報を取得するセットを戻します。
	 *
	 * @param beanId リソースを特定するＩＤ
	 * @return セットを戻します。
	 */
	public Map<String, String> getKeyMap(String beanId) {
		if (!designKeyProp.containsKey(beanId)) {
			designKeyProp.put(beanId, new LinkedHashMap<String, String>());
		}

		return designKeyProp.get(beanId);
	}

	/**
	 * 指定されたＩＤに対応するセットを取得します。 <br>
	 * value情報をkeyとしてkey情報を取得するセットを戻します。
	 *
	 * @param beanId リソースを特定するＩＤ
	 * @return セットを戻します。
	 */
	public Map<String, String> getValueMap(String beanId) {
		if (!designValueProp.containsKey(beanId)) {
			designValueProp.put(beanId, new LinkedHashMap<String, String>());
		}

		return designValueProp.get(beanId);
	}

	/**
	 * XML文書を任意のオブジェクトにマッピング（Unmarshall）します。
	 *
	 * @param fileName ファイル名
	 * @return マッピングしたオブジェクトを戻します
	 */
	private final Object mapping(String fileName, String priorityPath) {

		InputStream stream = null;
		try {

			DesignUtils du = new DesignUtils();
			stream = du.getDefineFileStream(this.getClass(), fileName, priorityPath);

			{
				File defFile = new File(priorityPath, fileName);

				if (defFile.isFile() && defFile.exists()) {
					designSheetId = trimLocale(TriStringUtils.getBaseName(defFile.getName()));
				} else {
					designSheetId = trimLocale(TriStringUtils.getBaseName(this.getClass().getResource("/"+fileName).getFile()));
				}
			}

			DesignDocument doc = DesignDocument.Factory.parse(stream, this.getXmlOptions());

			Design xml = doc.getDesign();
			for (Bean bean : xml.getBeanArray()) {
				Map<String, String> keyProp = new LinkedHashMap<String, String>();
				Map<String, String> valueProp = new LinkedHashMap<String, String>();

				for (Entry prop : bean.getEntryArray()) {
					keyProp.put(prop.getKey(), prop.getValue());
					valueProp.put(prop.getValue(), prop.getKey());
				}

				designKeyProp.put(bean.getId(), keyProp);
				designValueProp.put(bean.getId(), valueProp);
			}

			return designKeyProp;

		} catch (XmlException e) {
			e.printStackTrace();
			throw new TriSystemException(SmMessageId.SM005038S, e, "XmlError");
		} catch (IOException e) {
			e.printStackTrace();
			throw new TriSystemException(SmMessageId.SM005038S, e, "IOError");
		} finally {
			TriFileUtils.close(stream);
		}
	}

	/**
	 * XML名前空間を指定します。
	 *
	 * @param key キー値
	 * @param nameSpace 名前空間
	 */
	private void addNameSpace(String key, String nameSpace) {
		xmlNamespaceMap.put(key, nameSpace);
		opt.setLoadSubstituteNamespaces(xmlNamespaceMap);
	}

	/**
	 * 名前空間を示すパラメータを取得します。
	 *
	 * @return 名前空間を示すパラメータを戻します
	 */
	private final XmlOptions getXmlOptions() {
		return opt;
	}

	private String trimLocale(String fileName) {

		return fileName.substring(0, fileName.lastIndexOf("_"));
	}
}
