package jp.co.blueship.tri.fw.dao.oxm;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;

import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.DesignUtils;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.log.TriLogMessage;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.trinity.common.schema.beans.allowFilePath.AllowFilePathDocument;
import jp.co.blueship.trinity.common.schema.beans.allowFilePath.AllowFilePathDocument.AllowFilePath;
import jp.co.blueship.trinity.common.schema.beans.allowFilePath.AllowFilePathDocument.AllowFilePath.Allow;
import jp.co.blueship.trinity.common.schema.beans.allowFilePath.AllowFilePathDocument.AllowFilePath.Deny;
import jp.co.blueship.trinity.common.schema.beans.allowFilePath.RangeType;
import jp.co.blueship.trinity.common.schema.beans.allowFilePath.RangeType.Exclude;
import jp.co.blueship.trinity.common.schema.beans.allowFilePath.RegexpType;


/**
 * 許容ファイルパス設定シート（ユーザ設計項目）文書情報を管理するクラスです。
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public class AllowFilePathSheet {

	private static final ILog log = TriLogFactory.getInstance();

    private static final String LS_NAMESPACES_KEY = "";
    private static final String LS_NAMESPACES_VAL = "http://www.blueship.co.jp/tri/fw/schema/beans/allowFilePath";
	private Map<String, String> xmlNamespaceMap = new HashMap<String, String>();
	private XmlOptions opt = new XmlOptions(); /* http://xmlbeans.apache.org/docs/2.0.0/reference/constant-values.html#org.apache.xmlbeans.XmlOptions.COMPILE_MDEF_NAMESPACES */

	private List<Character> allowRangeList = new LinkedList<Character>();
	private List<String> allowRegexpList = new LinkedList<String>();
	private List<Character> denyRangeList = new LinkedList<Character>();
	private List<String> denyRegexpList = new LinkedList<String>();

	private static final IContextAdapter ac = ContextAdapterFactory.getContextAdapter();

	public static String[] CHARSET_NAMES = {
		"Shift_JIS"
	};
	public static final String HALFWIDTH_HEXADECIMAL = "[a-fA-F0-9]+";

	/**
	 * 許容ファイルパスXML文書のファイル名が設定されます。
	 *
	 * @param fileName 許容ファイルパスXML文書のファイル名
	 */
	public final void setFileName( String fileName ) {
		this.mapping( fileName , DesignUtils.getPriorityDefinePath() );
	}

	private AllowFilePathSheet() {
		this.addNameSpace(LS_NAMESPACES_KEY, LS_NAMESPACES_VAL);

	}

	/**
	 * 許可するファイルパス情報を取得します。
	 *
	 * @return セットを戻します。
	 */
	public List<Character> getAllowRangeList() {
		return this.allowRangeList;
	}
	/**
	 * 許可するファイルパス情報を取得します。
	 *
	 * @return セットを戻します。
	 */
	public List<String> getAllowRegexpList() {
		return this.allowRegexpList;
	}
	/**
	 * 許可しないファイルパス情報を取得します。
	 *
	 * @return セットを戻します。
	 */
	public List<Character> getDenyRangeList() {
		return this.denyRangeList;
	}
	/**
	 * 許可しないファイルパス情報を取得します。
	 *
	 * @return セットを戻します。
	 */
	public List<String> getDenyRegexpList() {
		return this.denyRegexpList;
	}

	/**
	 * XML文書を任意のオブジェクトにマッピング（Unmarshall）します。
	 *
	 * @param fileName ファイル名
	 * @return マッピングしたオブジェクトを戻します
	 */
	private final void mapping( String fileName , String priorityPath ) {

		InputStream stream = null;
		try {

			DesignUtils du	= new DesignUtils();
			stream			= du.getDefineFileStream( this.getClass(), fileName , priorityPath );

			AllowFilePathDocument doc = AllowFilePathDocument.Factory.parse( stream, this.getXmlOptions() );

			AllowFilePath xml = doc.getAllowFilePath();

			Allow allow = xml.getAllow();
			for ( RangeType range : allow.getRangeArray() ) {

				List<String> eList = new LinkedList<String>();
				for (Exclude exclude : range.getExcludeArray()) {
					String excludeChar = exclude.getChar();
					eList.add(excludeChar);
				}

				RangeObject rangeObj = new RangeObject(
						range.getCharset(),
						range.getFrom(),
						range.getTo(),
						eList
						);
				List<Character> list = null;

				try{
					list = rangeObj.validate();
				} catch (IllegalArgumentException e) {
					LogHandler.warn( log , e.getMessage() ) ;
				}
				LogHandler.info( log , rangeObj.toString() ) ;
				if(0 < rangeObj.getWarnings().size() ) {
					LogHandler.warn( log , rangeObj.getWarnings().toString() ) ;
				}

				if( null != list ) {
					this.allowRangeList.addAll(list);
					LogHandler.info( log , TriLogMessage.LSM0005 , String.valueOf(list) ) ;
				}
			}
			for ( RegexpType regexp : allow.getRegexpArray() ) {

				RegexpObject regexpObj = new RegexpObject(
						regexp.getPattern()
						);

				List<String> list = null;

				try{
					list = regexpObj.validate();
				} catch (PatternSyntaxException e) {
					LogHandler.warn( log , e.getMessage() ) ;
				} catch (IllegalArgumentException e) {
					LogHandler.warn( log , e.getMessage() ) ;
				}
				LogHandler.info( log , regexpObj.toString() ) ;
				if(0 < regexpObj.getWarnings().size() ) {
					LogHandler.warn( log , regexpObj.getWarnings().toString() ) ;
				}

				if( null != list ) {
					this.allowRegexpList.addAll(list);
					LogHandler.info( log , TriLogMessage.LSM0006 , list.toString() ) ;
				}
			}

			Deny deny = xml.getDeny();
			for ( RangeType range : deny.getRangeArray() ) {

				List<String> eList = new LinkedList<String>();
				for (Exclude exclude : range.getExcludeArray()) {
					String excludeChar = exclude.getChar();
					eList.add(excludeChar);
				}

				RangeObject rangeObj = new RangeObject(
						range.getCharset(),
						range.getFrom(),
						range.getTo(),
						eList
						);
				List<Character> list = null;

				try{
					list = rangeObj.validate();
				} catch (IllegalArgumentException e) {
					LogHandler.warn( log , e.getMessage() ) ;
				}
				LogHandler.info( log , rangeObj.toString() ) ;
				if(0 < rangeObj.getWarnings().size() ) {
					LogHandler.warn( log , rangeObj.getWarnings().toString() ) ;
				}

				if( null != list ) {
					this.denyRangeList.addAll(list);
					LogHandler.info( log , TriLogMessage.LSM0007 , String.valueOf(list) ) ;
				}
			}
			for ( RegexpType regexp : deny.getRegexpArray() ) {

				RegexpObject regexpObj = new RegexpObject(
						regexp.getPattern()
						);

				List<String> list = null;

				try{
					list = regexpObj.validate();
				} catch (PatternSyntaxException e) {
					LogHandler.warn( log , e.getMessage() ) ;
				} catch (IllegalArgumentException e) {
					LogHandler.warn( log , e.getMessage() ) ;
				}
				LogHandler.info( log , regexpObj.toString() ) ;
				if(0 < regexpObj.getWarnings().size() ) {
					LogHandler.warn( log , regexpObj.getWarnings().toString() ) ;
				}

				if( null != list ) {
					this.denyRegexpList.addAll(list);
					LogHandler.info( log , TriLogMessage.LSM0008 , list.toString() ) ;
				}
			}

		} catch (XmlException e) {
			e.printStackTrace();
			throw new TriSystemException( SmMessageId.SM005038S , e , "XmlError" );
		} catch (IOException e) {
			e.printStackTrace();
			throw new TriSystemException( SmMessageId.SM005038S , e , "IOError" );
		} finally {
			TriFileUtils.close(stream);
		}
	}

	/**
	 * XML名前空間を指定します。
	 * @param key キー値
	 * @param nameSpace 名前空間
	 */
	private void addNameSpace( String key, String nameSpace ) {
		xmlNamespaceMap.put(key, nameSpace);
		opt.setLoadSubstituteNamespaces( xmlNamespaceMap );
	}

    /**
     * 名前空間を示すパラメータを取得します。
     *
     * @return 名前空間を示すパラメータを戻します
     */
	private final XmlOptions getXmlOptions() {
    	return opt;
    }

	public class RangeObject {

		private String charset = "";
		private String from = "";
		private String to = "";
		private List<String> excludeList = new LinkedList<String>();
		private List<String> warnings = new ArrayList<String>(){
			private static final long serialVersionUID = 1L;

			public String toString() {
				StringBuilder buf = new StringBuilder();
				buf.append("[");

			        Iterator<String> i = iterator();
			        boolean hasNext = i.hasNext();
			        while (hasNext) {
			        	String o = i.next();
			            buf.append("[" + String.valueOf(o) + "]");
			            hasNext = i.hasNext();
			            if (hasNext)
			                buf.append("\n");
			        }

				buf.append("]");
				return buf.toString();
			}
		};

		public RangeObject(String charset, String from, String to, List<String> excludeList) {

			if(null != charset)this.charset = charset.trim();
			if(null != from)this.from = from.trim();
			if(null != to)this.to = to.trim();
			if(null != excludeList)this.excludeList = excludeList;
		}

		public List<Character> validate() throws UnsupportedEncodingException {

			List<Character> list = new LinkedList<Character>();
			IContextAdapter ac = ContextAdapterFactory.getContextAdapter();

			// required check
			if( "".equals(charset) || "".equals(from) || "".equals(to) ){
				throw new IllegalArgumentException( ac.getMessage( SmMessageId.SM004017F , charset, from , to ) );
			}

			// charset check
			if( 0 > Arrays.binarySearch(CHARSET_NAMES, charset) ) {
				throw new IllegalArgumentException( ac.getMessage( SmMessageId.SM004018F , Arrays.toString(CHARSET_NAMES) , charset , from , to ) );
			}

			// type check
			if( from.matches(HALFWIDTH_HEXADECIMAL) && to.matches(HALFWIDTH_HEXADECIMAL) ) {} else {
				throw new IllegalArgumentException( ac.getMessage( SmMessageId.SM004019F , HALFWIDTH_HEXADECIMAL , charset , from , to ) );
			}

			// length check
			if( 4 == from.length() && 4 == to.length()
					|| 2 == from.length() && 2 == to.length() ) {} else {
				throw new IllegalArgumentException( ac.getMessage( SmMessageId.SM004020F , charset , from , to ));
			}

			// compare check
			if( 0 < from.compareToIgnoreCase(to) ) {
				throw new IllegalArgumentException( ac.getMessage( SmMessageId.SM004021F , charset , from , to ) );
			}

			byte[] bFrom = getBytesByCode(from);
			byte[] bTo = getBytesByCode(to);

			int intCodeFrom = 0x20;
			if( 4 == from.length() ) {
				intCodeFrom = getCodeInt(bFrom, charset, true);
			}
			if( 2 == from.length() ) {
				intCodeFrom = getCodeInt(bTo, charset, false);
			}

			int intCodeTo = 0x20;
			if( 4 == from.length() ) {
				intCodeTo = getCodeInt(bTo, charset, true);
			}
			if( 2 == from.length() ) {
				intCodeTo = getCodeInt(bTo, charset, false);
			}

			for (int target = intCodeFrom; target < intCodeTo + 1; target++) {

				String str = "";
				if( 4 == from.length() ) {
					str = getString(target, charset, true);
				}
				if( 2 == from.length() ) {
					str = getString(target, charset, false);
				}

				if( str.length() == 1 ) {
					list.add(str.charAt(0));
				} else {
					String targetCode = "";
					if( 4 == from.length() ) {
						targetCode = getCodeString(target, charset, true);
					}
					if( 2 == from.length() ) {
						targetCode = getCodeString(target, charset, false);
					}
					warnings.add( ac.getMessage( TriLogMessage.LSM0017 , str , charset , from , to , targetCode ) );
				}
			}

			if(! excludeList.isEmpty()) {

				for (String excludeChar : excludeList) {
					// required check
					if( null == excludeChar ){
						warnings.add( ac.getMessage( TriLogMessage.LSM0022 ) );
					} else if( "".equals(excludeChar) ){
						warnings.add( ac.getMessage( TriLogMessage.LSM0026 ) );
					} else {
						// type check
						if( excludeChar.matches(HALFWIDTH_HEXADECIMAL) ) {} else {
							warnings.add( ac.getMessage( TriLogMessage.LSM0027 , HALFWIDTH_HEXADECIMAL , excludeChar ) );
						}

						// length check
						if( 4 == from.length() && 4 == to.length() ) {
							if( 4 == excludeChar.length() ) {} else {
								warnings.add( ac.getMessage( TriLogMessage.LSM0040 , excludeChar, from, to ) );
							}
						}
						if( 2 == from.length() && 2 == to.length() ) {
							if( 2 == excludeChar.length() ) {} else {
								warnings.add( ac.getMessage( TriLogMessage.LSM0040 , excludeChar, from, to ) );
							}
						}

						// compare check
						if( 0 < from.compareToIgnoreCase(excludeChar) ||
								0 < excludeChar.compareToIgnoreCase(to) ) {
							throw new IllegalArgumentException( ac.getMessage( SmMessageId.SM004022F , excludeChar , from , to ) );
						}
					}

					byte[] bExcludeChar = getBytesByCode(excludeChar);

					int intExcludeChar = 0x20;
					if( 4 == excludeChar.length() ) {
						intExcludeChar = getCodeInt(bExcludeChar, charset, true);
					}
					if( 2 == excludeChar.length() ) {
						intExcludeChar = getCodeInt(bExcludeChar, charset, false);
					}

					String str = "";
					if( 4 == from.length() ) {
						str = getString(intExcludeChar, charset, true);
					}
					if( 2 == from.length() ) {
						str = getString(intExcludeChar, charset, false);
					}
					if( str.length() == 1 ) {
						try {
							list.remove(str.charAt(0));
						} catch (IndexOutOfBoundsException e) {
							warnings.add(e.getMessage() +
									" char = " + excludeChar + ",from = " + from + ",to = " + to);
						}
					} else {
						String excludeCharCode = "";
						if( 4 == from.length() ) {
							excludeCharCode = getCodeString(intExcludeChar, charset, true);
						}
						if( 2 == from.length() ) {
							excludeCharCode = getCodeString(intExcludeChar, charset, false);
						}
						warnings.add( ac.getMessage( TriLogMessage.LSM0041 , str, excludeChar, from, to, excludeCharCode ) );
					}
				}
			}
			return list;
		}

		public List<String> getWarnings() {
			return this.warnings;
		}

		public String toString() {
			return "[charset = " + charset + "," +
					"from = " + from + "," +
					"to = " + to + "," +
					"excludeList = " + excludeList + "]";

		}
	}

	public class RegexpObject {

		private String pattern = "";
		private List<String> warnings = new ArrayList<String>(){
			private static final long serialVersionUID = 1L;

			public String toString() {
				StringBuilder buf = new StringBuilder();
				buf.append("[");

			        Iterator<String> i = iterator();
			        boolean hasNext = i.hasNext();
			        while (hasNext) {
			        	String o = i.next();
			            buf.append("[" + String.valueOf(o) + "]");
			            hasNext = i.hasNext();
			            if (hasNext)
			                buf.append("\n");
			        }

				buf.append("]");
				return buf.toString();
			}
		};

		public RegexpObject(String pattern) {

			if(null != pattern)this.pattern = pattern.trim();
		}

		public List<String>  validate() {

			List<String> list = new LinkedList<String>();

			// required check
			if( "".equals(pattern) ){
				throw new IllegalArgumentException( ac.getMessage( SmMessageId.SM004023F , pattern ) );
			}

			// pattern check
			try{
				Pattern.compile(pattern);
			} catch (PatternSyntaxException e){
				throw e;
			}

			list.add(pattern);

			return list;
		}

		public List<String> getWarnings() {
			return this.warnings;
		}

		public String toString() {
			return "[pattern = " + pattern + "]";

		}
	}

	/**
	 * JIS文字コードを文字列に変換します。
	 *
	 * @param hex 文字コード
	 * @param charset エンコーディング文字コード
	 * @param is2Byte true:2byte文字として処理、false:1byte文字として処理
	 * @return 変換したコードを戻します。
	 */
	static final String getString( Integer hex, String charset, boolean is2Byte ) {
		try {
			int lowMask = 0x000000FF;

			byte[] b = new byte[]{0x20};

			if( true == is2Byte) {

				b = new byte[2];

				b[0] = (byte)(hex>>8);
				b[1] = (byte)(hex & lowMask);
			}

			if( false == is2Byte) {

				b = new byte[1];

				b[0] = (byte)(hex & lowMask);
			}

			return new String(b, charset);
		} catch (UnsupportedEncodingException e) {
			throw new IllegalArgumentException( ac.getMessage( SmMessageId.SM005034S , e.getMessage() ) , e );
		}
	}

	/**
	 * JIS文字コードを文字列に変換します。
	 *
	 * @param hex 文字コード
	 * @param charset エンコーディング文字コード
	 * @param is2Byte true:2byte文字として処理、false:1byte文字として処理
	 * @return 変換したコードを戻します。
	 */
	static final String getCodeString( Integer hex, String charset, boolean is2Byte ) {

		int lowMask = 0x000000FF;

		String conv = "";

		if( true == is2Byte) {

			int hex1 = (byte)(hex>>8);
			int hex2 = (byte)(hex & lowMask);

			conv = Integer.toHexString(hex1) + Integer.toHexString(hex2);
			conv = conv.substring(conv.length()-4, conv.length());
		}

		if( false == is2Byte) {

			int hex1 = (byte)(hex & lowMask);

			conv = Integer.toHexString(hex1);
		}

		return conv;

	}

	/**
	 * JIS文字コードを文字列に変換します。
	 *
	 * @param hex 文字コード
	 * @param charset エンコーディング文字コード
	 * @param is2Byte true:2byte文字として処理、false:1byte文字として処理
	 * @return 変換したコードを戻します。
	 */
	static final int getCodeInt( String value, String charset, boolean is2Byte ) {
		try {

			byte[] b = value.getBytes(charset);

			int highMask = 0x0000FF00;
			int lowMask = 0x000000FF;

			int bCode = 0x00000032;

			if( true == is2Byte) {
				bCode = (b[0]<<8 & highMask) | (b[1] & lowMask);
			}
			if( false == is2Byte) {
				bCode = (b[0] & lowMask);
			}

			return bCode;

		} catch (UnsupportedEncodingException e) {
			throw new IllegalArgumentException( ac.getMessage( SmMessageId.SM005034S , e.getMessage() ) , e );
		}
	}

	/**
	 * JIS文字コードを文字列に変換します。
	 *
	 * @param hex 文字コード
	 * @param charset エンコーディング文字コード
	 * @param is2Byte true:2byte文字として処理、false:1byte文字として処理
	 * @return 変換したコードを戻します。
	 */
	static final int getCodeInt( byte[] b, String charset, boolean is2Byte ) {

		int highMask = 0x0000FF00;
		int lowMask = 0x000000FF;

		int bCode = 0x00000032;

		if( true == is2Byte) {
			bCode = (b[0]<<8 & highMask) | (b[1] & lowMask);
		}
		if( false == is2Byte) {
			bCode = (b[0] & lowMask);
		}

		return bCode;

	}

	static final byte[] getBytesByCode(String value) {

		byte b[] = new byte[]{0x00000020};

		if ( 4 == value.trim().length() ) {
			int hex1 = Integer.parseInt(value.substring(0, 2), 16);
			int hex2 = Integer.parseInt(value.substring(2, 4), 16);

			b = new byte[]{(byte)hex1, (byte)hex2};
		} else {
			int hex = Integer.parseInt(value.substring(0, 2), 16);
			b = new byte[]{(byte)hex};
		}

		return b;

	}

}
