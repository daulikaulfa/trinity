package jp.co.blueship.tri.fw.dao.oxm;

import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.SmMessageId;


/**
 * デザインシート（ユーザ設計項目）文書情報を生成するクラスです。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public class DesignSheetFactory {
	private static DesignSheetFactory factory = null;
	private IDesignSheet designSheet = null;

	private void init( IDesignSheet designSheet ) {
		this.designSheet = designSheet;
		this.designSheet.init();
		factory = this;
	}

	/**
	 * デザインXML文書のファイル名がDIコンテナ設定されます。
	 * <br>このmethodはインスタンス生成時に一度だけDIコンテナから呼び出されます。
	 *
	 * @param fileName デザインXML文書のファイル名
	 */
	public final void setDesignSheet( IDesignSheet designSheet ) {
		new DesignSheetFactory().init(designSheet);
	}

	/**
	 * デザインシート文書情報を取得します。
	 *
	 * @return デザインシート文書情報を戻します
	 */
	public static final IDesignSheet getDesignSheet() {
		if ( null == factory.designSheet ) {
			throw new TriSystemException( SmMessageId.SM005013S, new String[]{ IDesignSheet.class.getName() } );
		}

		return factory.designSheet;
	}


}
