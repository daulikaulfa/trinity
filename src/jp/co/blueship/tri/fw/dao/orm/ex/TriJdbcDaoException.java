package jp.co.blueship.tri.fw.dao.orm.ex;

import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.IMessageId;

/**
 * Exception that gets thrown when an JDBC invocation failed because of unexpected runtime issues.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public class TriJdbcDaoException extends TriSystemException {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 2L;

	/**
	 * Constructor for JdbcDaoException
	 * <br>
	 * @param message messageID
	 */
	public TriJdbcDaoException(IMessageId message){
		super(message);
	}

	/**
	 * Constructor for JdbcDaoException
	 * <br>
	 * @param message messageID
	 * @param messageArgs message parameter
	 */
	public TriJdbcDaoException(IMessageId message, String... messageArgs){
		super(message, messageArgs);
	}

	/**
	 * Constructor for JdbcDaoException
	 * <br>
	 * @param message messageID
	 * @param cause the root cause
	 */
	public TriJdbcDaoException(IMessageId message, Throwable cause){
		super(message, cause);
	}

	/**
	 * Constructor for JdbcDaoException
	 * <br>
	 * @param message messageID
	 * @param messageArgs message parameter
	 * @param cause the root cause
	 */
	public TriJdbcDaoException(IMessageId message, Throwable cause, String... messageArgs){
		super(message, cause, messageArgs);
	}

}
