package jp.co.blueship.tri.fw.dao.orm.psql;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.dao.orm.IEntityNullItem;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.ISqlCondition;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;

/**
 * Update queryを組みたてるためのStringBuilderクラスです
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public class UpdateBuilder implements ISqlBuilder {

	private ISqlCondition condition = null;
	private StringBuilder keyBuilder = new StringBuilder();
	private StringBuilder builder = new StringBuilder();
	private ITableAttribute tableAttr = null;
	private Set<String> keySet = new LinkedHashSet<String>();
	private Set<String> itemSet = new LinkedHashSet<String>();
	private MapSqlParameterSource paramSource = new MapSqlParameterSource();
	private List<SqlParameterSource> paramSourceList = new ArrayList<SqlParameterSource>();

	/**
	 * Constructor for UpdateBuilder
	 * <br>
	 */
	public UpdateBuilder() {
		this.setTableAttribute( null );
	}

	/**
	 * Constructor for UpdateBuilder
	 * <br>
	 * @param tableAttr table attribute
	 */
	public UpdateBuilder( ITableAttribute tableAttr ) {
		this.setTableAttribute(tableAttr);
	}

	/**
	 * tableNamesを途中で切り替える場合に設定します
	 *
	 * @param tableAttr table attribute
	 */
	public void setTableAttribute( ITableAttribute tableAttr ) {
		this.tableAttr = tableAttr;
	}

	@Override
	public ITableAttribute getTableAttribute() {
		return this.tableAttr;
	}

	@Override
	public ISqlBuilder append( ITableItem element, Object value ) {
		return this.append( element, value, false );
	}

	@Override
	public ISqlBuilder append( ITableItem item, Object value, boolean isPrimaryKey ) {
		if ( null == item )
			return this;

		if ( null == value )
			return this;

		if ( isPrimaryKey ) {
			if ( ! keySet.contains(item.getItemName()) ) {
				keySet.add( item.getItemName() );

				if ( 0 != keyBuilder.length() ) {
					keyBuilder.append(" and ");
				}
				keyBuilder.append( item.getItemName() ).append( "=:" ).append( item.getItemName() );
			}
		} else {
			if ( ! itemSet.contains(item.getItemName()) ) {
				itemSet.add( item.getItemName() );

				if ( 0 != builder.length() ) {
					builder.append(" , ");
				}
				builder.append( item.getItemName() ).append( "=:" ).append( item.getItemName() );
			}
		}

		paramSource.addValue( item.getItemName(), (IEntityNullItem.class.isInstance(value))? null: value );

		return this;
	}

	/**
	 * Sql ParameterをbatchUpdate向けにListに追加し、現在のSql Parameterを初期化します。
	 *
	 * @return a reference to this instance, so it's possible to chain several calls together
	 */
	public UpdateBuilder add() {
		paramSourceList.add( paramSource );
		paramSource = new MapSqlParameterSource();

		return this;
	}

	/**
	 * Sql ParameterにValuesを追加します。
	 *
	 * @paramSource SQL Parameter
	 * @return a reference to this instance, so it's possible to chain several calls together
	 */
	public UpdateBuilder add( MapSqlParameterSource paramSource ) {
		this.paramSource.addValues(paramSource.getValues());

		return this;
	}


	/**
	 * Query実行時の条件を指定する場合に設定します。
	 *
	 * @param condition The interface of the SQL conditon object
	 * @return Queryを組みたてるためのBuilderインタフェース
	 */
	public ISqlBuilder setCondition(ISqlCondition condition) {
		this.condition = condition;
		return this;
	}

	@Override
	public String toQueryString() {
		String update = builder.toString();
		String updateKey = keyBuilder.toString();

		StringBuffer buf = new StringBuffer();

		buf.append(" UPDATE " + tableAttr.name());
		buf.append(" SET " + update);

		if ( null == this.condition ) {
			if ( TriStringUtils.isNotEmpty(updateKey) ) {
				buf.append(" WHERE " + updateKey);
			}
		} else {
			buf.append( this.condition.toQueryString() );
		}

		return buf.toString();
	}

	/**
	 * SQL Parameterを取得します。
	 *
	 * @return SQL Parameterを戻します。
	 */
	public SqlParameterSource getSqlParameter() {
		return paramSource;
	}

	/**
	 * SQL ParameterのListを取得します。
	 *
	 * @return SQL ParameterのListを戻します。
	 */
	public List<SqlParameterSource> getSqlParameterList() {
		return paramSourceList;
	}

}
