package jp.co.blueship.tri.fw.dao.orm;

/**
 * ページ制御を行うバーの実装です。
 * <br>画面に表示する検索結果等の一覧をページ制御するために使用します。
 *
 */
public class PageBar implements java.io.Serializable {
	private static final long serialVersionUID = 1L;

	private int selectPage;
	private int maximum;

	/**
	 * ページバーの値を取得します。
	 * @return 取得した値を戻します。
	 */
	public final int getValue() {
		if ( 0 == selectPage && 0 != maximum )
			selectPage = 1;

		return selectPage;
	}

	/**
	 * ページバーの値を設定します。
	 * @param selectPage ページバーの値
	 */
	public final void setValue( int selectPage ) {
		if ( selectPage < 0 )
			selectPage = 0;

		if ( maximum < selectPage )
			selectPage = maximum;

		this.selectPage = selectPage;
	}

	/**
	 * ページバーの最大値を取得します。
	 * @return 取得した値を戻します。
	 */
	public final int getMaximum() {
		return maximum;
	}

	/**
	 * ページバーの最大値を設定します。
	 * @param maximum ページバーの値
	 */
	public final void setMaximum( int maximum ) {
		if ( maximum < 0 )
			maximum = 0;

		if ( maximum < selectPage )
			maximum = selectPage;

		this.maximum = maximum;
	}

}
