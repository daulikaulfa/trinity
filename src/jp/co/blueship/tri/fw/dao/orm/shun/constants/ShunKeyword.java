package jp.co.blueship.tri.fw.dao.orm.shun.constants;

/**
 * Shunsakuを利用するためのキーワードの指定形式です。
 *
 */
public class ShunKeyword {

	

	/**
	 * 前方一致指定検索
	 */
	public static final String prefixSearch = "^";
	/**
	 * 後方一致指定検索
	 */
	public static final String suffixSearch = "$";
	/**
	 * 空文字指定検索
	 */
	public static final String nothingSearch = prefixSearch + suffixSearch;

}
