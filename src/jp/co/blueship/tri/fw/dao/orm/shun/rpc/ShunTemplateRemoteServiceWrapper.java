package jp.co.blueship.tri.fw.dao.orm.shun.rpc;

import java.sql.SQLException;
import java.util.List;

import jp.co.blueship.tri.fw.dao.orm.ILimit;
import jp.co.blueship.tri.fw.dao.orm.shun.IShunTemplate;
import jp.co.blueship.tri.fw.dao.orm.shun.ShunMapper;
import jp.co.blueship.tri.fw.dao.orm.shun.rpc.dto.ShunRemoteList;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.SmMessageId;

/**
 * Shunsakuをリモートから利用するためのアクセステンプレートのラッパークラスです。
 */
public class ShunTemplateRemoteServiceWrapper implements IShunTemplate {

	

	private static final ILog log = TriLogFactory.getInstance();

	private String datasource = null;
	private IShunTemplateRemote shunTemplateRemote = null;

	/**
	 * アクセステンプレートを生成します。
	 * @param datasource Shunsakuデータソース
	 */
	public ShunTemplateRemoteServiceWrapper( String datasource ) {
		this.datasource = datasource;
	}

	/**
	 * Shunsakuリモートサービスを利用するためのアクセステンプレートが設定されます。
	 *
	 * @param shunTemplateRemote アクセステンプレート
	 */
	public void setShunTemplateRemote( IShunTemplateRemote shunTemplateRemote ) {
		this.shunTemplateRemote = shunTemplateRemote;
	}

    /**
     * Shunsakuリモートサービスを利用するためのアクセステンプレートを取得します。
     * @return アクセステンプレート
     */
    protected final IShunTemplateRemote getShunTemplateRemote() {
        return shunTemplateRemote;
    }

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.db.shunsaku.core.IShunTemplate#countQuery(java.lang.String)
	 */
	public int countQuery(String queryExp) throws SQLException {
		return this.countQuery(queryExp, 0, false);
	}

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.db.shunsaku.core.IShunTemplate#countQuery(java.lang.String, boolean)
	 */
	public int countQuery(String queryExp, boolean isSort) throws SQLException {
		return this.countQuery(queryExp, 0, isSort);
	}

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.db.shunsaku.core.IShunTemplate#countQuery(java.lang.String)
	 */
	public int countQuery(String queryExp, int maxHitLimit, boolean isSort ) throws SQLException {
		long time = 0;

		if ( log.isDebugEnabled() ) {
			time = System.currentTimeMillis();
		}

		try {
			return this.getShunTemplateRemote().countQuery( this.datasource, queryExp, maxHitLimit, isSort );
		} catch ( Exception e ) {
			if ( e instanceof SQLException )
				throw (SQLException)e;

			throw new TriSystemException( SmMessageId.SM005203S , e );
		} finally {
			if ( log.isDebugEnabled() ) {
				LogHandler.debug( log , "int:countQuery( String queryExp ):profileTime:=" + (System.currentTimeMillis() - time) );
			}
		}
	}

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.db.shunsaku.core.IShunTemplate#delete(java.lang.String)
	 */
	public int delete(String queryExp) throws SQLException {
		long time = 0;

		if ( log.isDebugEnabled() ) {
			time = System.currentTimeMillis();
		}

		try {
			return this.getShunTemplateRemote().delete( this.datasource, queryExp );
		} catch ( Exception e ) {
			if ( e instanceof SQLException )
				throw (SQLException)e;

			throw new TriSystemException( SmMessageId.SM005204S , e );
		} finally {
			if ( log.isDebugEnabled() ) {
				LogHandler.debug( log , "int:delete( String queryExp ):profileTime:=" + (System.currentTimeMillis() - time) );
			}
		}
	}

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.db.shunsaku.core.IShunTemplate#deleteByKey(java.lang.String, java.lang.String[])
	 */
	public int deleteByKey(String key, String[] keyValues) throws SQLException {
		long time = 0;

		if ( log.isDebugEnabled() ) {
			time = System.currentTimeMillis();
		}

		try {
			return this.getShunTemplateRemote().deleteByKey( this.datasource, key, keyValues );
		} catch ( Exception e ) {
			if ( e instanceof SQLException )
				throw (SQLException)e;

			throw new TriSystemException( SmMessageId.SM005205S , e );
		} finally {
			if ( log.isDebugEnabled() ) {
				LogHandler.debug( log , "int:deleteByKey( String key, String[] keyValues ):profileTime:=" + (System.currentTimeMillis() - time) );
			}
		}
	}

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.db.shunsaku.core.IShunTemplate#find(java.lang.String, java.lang.String, jp.co.blueship.trinity.common.db.shunsaku.core.ShunMapper)
	 */
	public List<?> find(String queryExp, String returnExp, ShunMapper map) throws SQLException {
		long time = 0;

		if ( log.isDebugEnabled() ) {
			time = System.currentTimeMillis();
		}

		try {
			return this.getShunTemplateRemote().find( this.datasource, queryExp, returnExp, map );
		} catch ( Exception e ) {
			if ( e instanceof SQLException )
				throw (SQLException)e;

			throw new TriSystemException( SmMessageId.SM005206S , e );
		} finally {
			if ( log.isDebugEnabled() ) {
				LogHandler.debug( log , "List<?>:find( String queryExp, String returnExp, ShunMapper map ):profileTime:=" + (System.currentTimeMillis() - time) );
			}
		}
	}

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.db.shunsaku.core.IShunTemplate#find(java.lang.String, java.lang.String, jp.co.blueship.trinity.common.db.ILimit, jp.co.blueship.trinity.common.db.shunsaku.core.ShunMapper)
	 */
	public List<?> find(String queryExp, String returnExp, ILimit limit, ShunMapper map) throws SQLException {
		long time = 0;

		if ( log.isDebugEnabled() ) {
			time = System.currentTimeMillis();
		}

		try {
			return this.getShunTemplateRemote().find( this.datasource, queryExp, returnExp, limit, map );
		} catch ( Exception e ) {
			if ( e instanceof SQLException )
				throw (SQLException)e;

			throw new TriSystemException( SmMessageId.SM005206S ,  e );
		} finally {
			if ( log.isDebugEnabled() ) {
				LogHandler.debug( log , "List<?> find(String queryExp, String returnExp, ILimit limit, ShunMapper map):profileTime:=" + (System.currentTimeMillis() - time) );
			}
		}
	}

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.db.shunsaku.core.IShunTemplate#find(java.lang.String, java.lang.String, java.lang.String, jp.co.blueship.trinity.common.db.shunsaku.core.ShunMapper)
	 */
	public List<?> find(String queryExp, String returnExp, String sortExp, ShunMapper map) throws SQLException {
		long time = 0;

		if ( log.isDebugEnabled() ) {
			time = System.currentTimeMillis();
		}

		try {
			return this.getShunTemplateRemote().find(this.datasource, queryExp, returnExp, sortExp, map);
		} catch ( Exception e ) {
			if ( e instanceof SQLException )
				throw (SQLException)e;

			throw new TriSystemException( SmMessageId.SM005206S ,  e );
		} finally {
			if ( log.isDebugEnabled() ) {
				LogHandler.debug( log , "List<?> find(String queryExp, String returnExp, String sortExp, ShunMapper map):profileTime:=" + (System.currentTimeMillis() - time) );
			}
		}
	}

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.db.shunsaku.core.IShunTemplate#find(java.lang.String, java.lang.String, java.lang.String, jp.co.blueship.trinity.common.db.ILimit, jp.co.blueship.trinity.common.db.shunsaku.core.ShunMapper)
	 */
	public List<?> find(String queryExp, String returnExp, String sortExp, ILimit limit, ShunMapper map) throws SQLException {
		long time = 0;

		if ( log.isDebugEnabled() ) {
			time = System.currentTimeMillis();
		}

		try {
			return this.getShunTemplateRemote().find( this.datasource, queryExp, returnExp, sortExp, limit, map );
		} catch ( Exception e ) {
			if ( e instanceof SQLException )
				throw (SQLException)e;

			throw new TriSystemException( SmMessageId.SM005206S ,  e );
		} finally {
			if ( log.isDebugEnabled() ) {
				LogHandler.debug( log , "List<?> find(String queryExp, String returnExp, String sortExp, ILimit limit, ShunMapper map):profileTime:=" + (System.currentTimeMillis() - time) );
			}
		}
	}

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.db.shunsaku.core.IShunTemplate#findByKey(java.lang.String, java.util.List, java.lang.String, jp.co.blueship.trinity.common.db.shunsaku.core.ShunMapper)
	 */
	public List<?> findByKey(String key, List<String[]> keyValues, String returnExp, ShunMapper map) throws SQLException {
		long time = 0;

		if ( log.isDebugEnabled() ) {
			time = System.currentTimeMillis();
		}

		try {
			ShunRemoteList<String[]> shunKeyValues = new ShunRemoteList<String[]>();
			shunKeyValues.addAll( keyValues );

			return this.getShunTemplateRemote().findByKey( this.datasource, key, shunKeyValues, returnExp, map );
		} catch ( Exception e ) {
			if ( e instanceof SQLException )
				throw (SQLException)e;

			throw new TriSystemException( SmMessageId.SM005207S , e );
		} finally {
			if ( log.isDebugEnabled() ) {
				LogHandler.debug( log , "List<?> findByKey(String key, List<String[]> keyValues, String returnExp, ShunMapper map):profileTime:=" + (System.currentTimeMillis() - time) );
			}
		}
	}

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.db.shunsaku.core.IShunTemplate#insert(java.lang.String)
	 */
	public int insert(String data) throws SQLException {
		long time = 0;

		if ( log.isDebugEnabled() ) {
			time = System.currentTimeMillis();
		}

		try {
			return this.getShunTemplateRemote().insert( this.datasource, data );
		} catch ( Exception e ) {
			if ( e instanceof SQLException )
				throw (SQLException)e;

			throw new TriSystemException( SmMessageId.SM005208S , e );
		} finally {
			if ( log.isDebugEnabled() ) {
				LogHandler.debug( log , "int insert(String data):profileTime:=" + (System.currentTimeMillis() - time) );
			}
		}
	}

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.db.shunsaku.core.IShunTemplate#update(java.lang.String, java.lang.String)
	 */
	public int update(String queryExp, String data) throws SQLException {
		long time = 0;

		if ( log.isDebugEnabled() ) {
			time = System.currentTimeMillis();
		}

		try {
			return this.getShunTemplateRemote().update( this.datasource, queryExp, data );
		} catch ( Exception e ) {
			if ( e instanceof SQLException )
				throw (SQLException)e;

			throw new TriSystemException( SmMessageId.SM005209S , e );
		} finally {
			if ( log.isDebugEnabled() ) {
				LogHandler.debug( log , "int update(String queryExp, String data):profileTime:=" + (System.currentTimeMillis() - time) );
			}
		}
	}

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.db.shunsaku.core.IShunTemplate#updateByKey(java.lang.String, java.lang.String[], java.lang.String)
	 */
	public int updateByKey(String key, String[] keyValues, String data) throws SQLException {
		long time = 0;

		if ( log.isDebugEnabled() ) {
			time = System.currentTimeMillis();
		}

		try {
			return this.getShunTemplateRemote().updateByKey( this.datasource, key, keyValues, data );
		} catch ( Exception e ) {
			if ( e instanceof SQLException )
				throw (SQLException)e;

			throw new TriSystemException( SmMessageId.SM005210S , e );
		} finally {
			if ( log.isDebugEnabled() ) {
				LogHandler.debug( log , "int updateByKey(String key, String[] keyValues, String data):profileTime:=" + (System.currentTimeMillis() - time) );
			}
		}
	}

}
