package jp.co.blueship.tri.fw.dao.orm;

import jp.co.blueship.tri.am.dao.areq.IAreqAttachedFileDao;
import jp.co.blueship.tri.am.dao.areq.IAreqBinaryFileDao;
import jp.co.blueship.tri.am.dao.areq.IAreqDao;
import jp.co.blueship.tri.am.dao.areq.IAreqFileDao;
import jp.co.blueship.tri.am.dao.lot.ILotDao;
import jp.co.blueship.tri.am.dao.pjt.IPjtDao;
import jp.co.blueship.tri.am.dao.pjtavl.IPjtAvlAreqLnkDao;
import jp.co.blueship.tri.am.dao.pjtavl.IPjtAvlDao;
import jp.co.blueship.tri.am.support.IAmFinderSupport;
import jp.co.blueship.tri.bm.dao.bp.BpNumberingDao;
import jp.co.blueship.tri.bm.support.IBmFinderSupport;
import jp.co.blueship.tri.dcm.dao.rep.IRepDao;
import jp.co.blueship.tri.dcm.support.IDcmFinderSupport;
import jp.co.blueship.tri.fw.sm.dao.procmgt.IProcDetailsDao;
import jp.co.blueship.tri.fw.um.dao.hist.IBpHistDao;
import jp.co.blueship.tri.fw.um.dao.hist.IHistDao;
import jp.co.blueship.tri.fw.um.dao.hist.ILotHistDao;
import jp.co.blueship.tri.fw.um.dao.hist.IRaHistDao;
import jp.co.blueship.tri.fw.um.dao.hist.IRepHistDao;
import jp.co.blueship.tri.fw.um.dao.hist.IRpHistDao;
import jp.co.blueship.tri.rm.dao.rp.RpNumberingDao;
import jp.co.blueship.tri.rm.support.IRmFinderSupport;

/**
 * データアクセスを支援するファインダー
 * <br>
 * <p>
 * データアクセスを支援するサポートクラスです。
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class FinderSupport {

	//ステータスマトリクスが超横断アクセスを行うための処置
	private IAmFinderSupport amFinderSupport = null;
	private IBmFinderSupport bmFinderSupport = null;
	private IRmFinderSupport rmFinderSupport = null;
	private IDcmFinderSupport dcmFinderSupport = null;

	private BpNumberingDao bpNumberingDao = null;
	private RpNumberingDao rpNumberingDao = null;

	private IPjtAvlDao pjtAvlDao = null;
	private IPjtAvlAreqLnkDao pjtAvlAreqLnkDao = null;
	private IAreqFileDao areqFileDao = null;
	private IAreqBinaryFileDao areqBinaryFileDao = null;
	private IAreqAttachedFileDao areqAttachedFileDao = null;
	private IProcDetailsDao procDetailsDao = null;

	private IRepDao repDao = null;
	private IHistDao histDao = null;
	private IBpHistDao bpHistDao = null;
	private ILotHistDao lotHistDao = null;
	private IRaHistDao raHistDao = null;
	private IRpHistDao rpHistDao = null;
	private IRepHistDao repHistDao = null;

	/**
	 * amFinderSupportを取得します。
	 * @return amFinderSupport
	 */
	public IAmFinderSupport getAmFinderSupport() {
	    return amFinderSupport;
	}
	/**
	 * amFinderSupportを設定します。
	 * @param amFinderSupport amFinderSupport
	 */
	public void setAmFinderSupport(IAmFinderSupport amFinderSupport) {
	    this.amFinderSupport = amFinderSupport;
	}
	/**
	 * bmFinderSupportを取得します。
	 * @return bmFinderSupport
	 */
	public IBmFinderSupport getBmFinderSupport() {
	    return bmFinderSupport;
	}
	/**
	 * bmFinderSupportを設定します。
	 * @param bmFinderSupport bmFinderSupport
	 */
	public void setBmFinderSupport(IBmFinderSupport bmFinderSupport) {
	    this.bmFinderSupport = bmFinderSupport;
	}
	/**
	 * rmFinderSupportを取得します。
	 * @return rmFinderSupport
	 */
	public IRmFinderSupport getRmFinderSupport() {
	    return rmFinderSupport;
	}
	/**
	 * rmFinderSupportを設定します。
	 * @param rmFinderSupport rmFinderSupport
	 */
	public void setRmFinderSupport(IRmFinderSupport rmFinderSupport) {
	    this.rmFinderSupport = rmFinderSupport;
	}
	/**
	 * dcmFinderSupportを取得します。
	 * @return dcmFinderSupport
	 */
	public IDcmFinderSupport getDcmFinderSupport() {
	    return dcmFinderSupport;
	}
	/**
	 * dcmFinderSupportを設定します。
	 * @param dcmFinderSupport dcmFinderSupport
	 */
	public void setDcmFinderSupport(IDcmFinderSupport dcmFinderSupport) {
	    this.dcmFinderSupport = dcmFinderSupport;
	}
	/**
	 * @deprecated
	 */
	public BpNumberingDao getBpNumberingDao() {
		return bpNumberingDao;
	}
	public void setBpNumberingDao(BpNumberingDao bpNumberingDao) {
		this.bpNumberingDao = bpNumberingDao;
	}
	/**
	 * @deprecated
	 */
	public RpNumberingDao getRpNumberingDao() {
		return rpNumberingDao;
	}
	public void setRpNumberingDao(RpNumberingDao rpNumberingDao) {
		this.rpNumberingDao = rpNumberingDao;
	}
	/**
	 * @deprecated
	 */
	public IPjtAvlAreqLnkDao getPjtAvlAreqLnkDao() {
	    return pjtAvlAreqLnkDao;
	}
	/**
	 * pjtAvlAreqLnkDaoを設定します。
	 * @param pjtAvlAreqLnkDao pjtAvlAreqLnkDao
	 */
	public void setPjtAvlAreqLnkDao(IPjtAvlAreqLnkDao pjtAvlAreqLnkDao) {
	    this.pjtAvlAreqLnkDao = pjtAvlAreqLnkDao;
	}
	/**
	 * @deprecated
	 */
	public IAreqDao getAreqDao() {
		return this.getAmFinderSupport().getAreqDao();
	}
	/**
	 * @deprecated
	 */
	public IAreqFileDao getAreqFileDao() {
		return areqFileDao;
	}
	public void setAreqFileDao(IAreqFileDao areqFileDao) {
		this.areqFileDao = areqFileDao;
	}
	/**
	 * @deprecated
	 */
	public IAreqBinaryFileDao getAreqBinaryFileDao() {
		return areqBinaryFileDao;
	}
	public void setAreqBinaryFileDao(IAreqBinaryFileDao areqBinaryFileDao) {
		this.areqBinaryFileDao = areqBinaryFileDao;
	}
	/**
	 * @deprecated
	 */
	public IAreqAttachedFileDao getAreqAttachedFileDao() {
		return areqAttachedFileDao;
	}
	public void setAreqAttachedFileDao(IAreqAttachedFileDao areqAttachedFileDao) {
		this.areqAttachedFileDao = areqAttachedFileDao;
	}
	/**
	 * @deprecated
	 */
	public IProcDetailsDao getProcDetailsDao() {
		return procDetailsDao;
	}
	public void setProcDetailsDao(IProcDetailsDao procDetailsDao) {
		this.procDetailsDao = procDetailsDao;
	}
	/**
	 * @deprecated
	 */
	public IPjtDao getPjtDao() {
		return this.getAmFinderSupport().getPjtDao();
	}
	/**
	 * @deprecated
	 */
	public IPjtAvlDao getPjtAvlDao() {
		return pjtAvlDao;
	}
	public void setPjtAvlDao(IPjtAvlDao pjtApproveDao) {
		this.pjtAvlDao = pjtApproveDao;
	}
	/**
	 * @deprecated
	 */
	public ILotDao getLotDao() {
		return this.getAmFinderSupport().getLotDao();
	}
	/**
	 * @deprecated
	 */
	public IRepDao getRepDao() {
		return repDao;
	}
	public void setRepDao(IRepDao reportDao) {
		this.repDao = reportDao;
	}
	/**
	 * @deprecated
	 */
	public IHistDao getHistDao() {
	    return histDao;
	}
	public void setHistDao(IHistDao histDao) {
	    this.histDao = histDao;
	}
	/**
	 * @deprecated
	 */
	public ILotHistDao getLotHistDao() {
	    return lotHistDao;
	}
	public void setLotHistDao(ILotHistDao lotHistDao) {
	    this.lotHistDao = lotHistDao;
	}
	/**
	 * @deprecated
	 */
	public IBpHistDao getBpHistDao() {
	    return bpHistDao;
	}
	public void setBpHistDao(IBpHistDao bpHistDao) {
	    this.bpHistDao = bpHistDao;
	}
	/**
	 * @deprecated
	 */
	public IRaHistDao getRaHistDao() {
	    return raHistDao;
	}
	public void setRaHistDao(IRaHistDao raHistDao) {
	    this.raHistDao = raHistDao;
	}
	/**
	 * @deprecated
	 */
	public IRpHistDao getRpHistDao() {
	    return rpHistDao;
	}
	public void setRpHistDao(IRpHistDao rpHistDao) {
	    this.rpHistDao = rpHistDao;
	}
	/**
	 * @deprecated
	 */
	public IRepHistDao getRepHistDao() {
	    return repHistDao;
	}
	public void setRepHistDao(IRepHistDao repHistDao) {
	    this.repHistDao = repHistDao;
	}
}
