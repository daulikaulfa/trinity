package jp.co.blueship.tri.fw.dao.orm.psql;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import jp.co.blueship.tri.fw.constants.JoinType;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.dao.orm.ISqlCondition;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;
import jp.co.blueship.tri.fw.sm.dao.constants.SmTables;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;

/**
 * The SQL condition of the entity.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 * @version V4.03.00
 * @author Cuong Nguyen
 */
public class ConditionTemplate implements ISqlCondition {

	/**
	 * The interface of the table properties.
	 */
	private ITableAttribute tableAttr = null;

	/**
	 * 単純検索に使用する
	 */
	private Map<String, String> entityNameMap = new LinkedHashMap<String, String>();
	/**
	 * 単純検索に使用する
	 */
	private Map<String, Object> itemMap = new LinkedHashMap<String, Object>();
	/**
	 * 複雑検索に使用する
	 */
	private Map<String, String> extendedItemMap = new LinkedHashMap<String, String>();

	/**
	 * 単純検索に使用する（“実行中データのステータス”と外部結合）
	 */
	private Map<String, Object> itemMapByJoinExecData = new LinkedHashMap<String, Object>();
	/**
	 * 複雑検索に使用する（“実行中データのステータス”と外部結合）
	 */
	private Map<String, String> extendedItemMapByJoinExecData = new LinkedHashMap<String, String>();

	/**
	 * @see SmTables#SM_EXEC_DATA_STS 実行中データのステータスと外部結合（LEFT JOIN）を行うかどうか
	 */
	private boolean isJoinExecData = false;

	/**
	 * @see ISqlCondition#isJoinExecData() isJoinExecDataの戻り値がtrueの場合、結合キーを取得するための項目名
	 */
	private ITableItem keyItem = null;

	/**
	 * @see UmTables#UM_ACCS_HIST アクセス履歴と外部結合（LEFT JOIN）を行うかどうか
	 */
	private boolean isJoinAccsHist = false;

	private JoinType join = JoinType.LEFT;

	/**
	 * @see ISqlCondition#isJoinAccsHist() isJoinAccsHistの戻り値がtrueの場合、結合キーを取得するための項目名
	 */
	private String keyItemByAccs = null;

	/**
	 * @see UmTables#UM_USER ユーザと外部結合（LEFT JOIN）を行うかどうか
	 */
	private boolean isJoinUser = false;

	/**
	 * @see ISqlCondition#isJoinUser() isJoinUserの戻り値がtrueの場合、結合キーを取得するための項目名
	 */
	private ITableItem[] keyItemByUsers = new ITableItem[0];

	/**
	 * @see UmTables#UM_CTG アクセス履歴と外部結合（LEFT JOIN）を行うかどうか
	 */
	private boolean isJoinCtg = false;

	/**
	 * @see UmTables#UM_MSTONE アクセス履歴と外部結合（LEFT JOIN）を行うかどうか
	 */
	private boolean isJoinMstone = false;

	/**
	 * Constructor for ConditionTemplate
	 * <br>
	 */
	public ConditionTemplate() {
		this.setTableAttribute( null );
	}

	/**
	 * Constructor for ConditionTemplate
	 * <br>
	 * @param tableAttr table attribute
	 */
	public ConditionTemplate( ITableAttribute tableAttr ) {
		this.tableAttr = tableAttr;
	}

	/**
	 * tableNamesを途中で切り替える場合に設定します
	 *
	 * @param tableAttr table attribute
	 */
	public ConditionTemplate setTableAttribute( ITableAttribute tableAttr ) {
		this.tableAttr = tableAttr;
		return this;
	}

	@Override
	public ITableAttribute getTableAttribute() {
		return this.tableAttr;
	}

	/**
	 * @see SmTables#SM_EXEC_DATA_STS 実行中データのステータスと外部結合（LEFT JOIN）を行うかどうかを設定します
	 *
	 * @param isJoinExecData 外部結合を行う場合true。それ以外はfalse。
	 */
	protected ConditionTemplate setJoinExecData( boolean isJoinExecData ) {
		this.isJoinExecData = isJoinExecData;
		return this;
	}

	@Override
	public boolean isJoinExecData() {
		if ( null == getKeyByJoinExecData() ) {
			return false;
		}

		return isJoinExecData;
	}

	/**
	 * @see ISqlCondition#isJoinExecData() isJoinExecDataの戻り値がtrueの場合、結合キーを取得するための項目名を設定します。
	 *
	 * @param keyItem 対象レコードのキー項目
	 */
	protected ConditionTemplate setKeyByJoinExecData( ITableItem keyItem ) {
		this.keyItem = keyItem;
		return this;
	}

	@Override
	public ITableItem getKeyByJoinExecData() {
		return keyItem;
	}

	/**
	 * アクセス履歴と外部結合（LEFT JOIN）を行うかどうかを設定します
	 * <br>isJoinExecDataの戻り値がtrueの場合のみ有効です。
	 * @see UmTables#UM_ACCS_HIST アクセス履歴
	 *
	 * @param isJoinAccsHist 外部結合を行う場合true。それ以外はfalse。
	 */
	protected ConditionTemplate setJoinAccsHist( boolean isJoinAccsHist ) {
		this.setJoinAccsHist(isJoinAccsHist, JoinType.LEFT);
		return this;
	}

	/**
	 * Set whether to perform access history and outer join (LEFT or RIGHT JOIN)
	 * This function is valid only when the return value of isJoinExecData is true.
	 * @see UmTables#UM_ACCS_HIST access history
	 *
	 * @param isJoinAccsHist True when performing outer join. Otherwise false.
	 * @param join Specific LEFT or RIGHT JOIN
	 */
	protected ConditionTemplate setJoinAccsHist( boolean isJoinAccsHist, JoinType join ) {
		this.isJoinAccsHist = isJoinAccsHist;
		this.join = join;
		return this;
	}

	@Override
	public boolean isJoinAccsHist() {
		return isJoinAccsHist;
	}

	@Override
	public String getKeyByJoinAccsHist() {
		return keyItemByAccs;
	}

	@Override
	public ConditionTemplate setKeyByJoinAccsHist(String userId) {
		this.keyItemByAccs = userId;
		return this;
	}

	@Override
	public boolean isRightJoinAccsHist() {
		return JoinType.RIGHT == this.join;
	}

	/**
	 * ユーザと外部結合（LEFT JOIN）を行うかどうかを設定します
	 * <br>isJoinExecDataの戻り値がtrueの場合のみ有効です。
	 * @see UmTables#UM_USER ユーザ
	 *
	 * @param isJoinUser 外部結合を行う場合true。それ以外はfalse。
	 */
	protected ConditionTemplate setJoinUser( boolean isJoinUser ) {
		this.isJoinUser = isJoinUser;
		return this;
	}

	@Override
	public boolean isJoinUser() {
		return isJoinUser;
	}

	@Override
	public ConditionTemplate setKeyByJoinUsers( ITableItem... items ) {
		this.keyItemByUsers = items;
		return this;
	}

	@Override
	public ITableItem[] getKeyByJoinUsers() {
		return keyItemByUsers;
	}

	/**
	 * カテゴリーと外部結合（LEFT JOIN）を行うかどうかを設定します
	 * <br>isJoinCtgの戻り値がtrueの場合のみ有効です。
	 * @see UmTables#UM_CTG カテゴリー
	 *
	 * @param isJoinCtg 外部結合を行う場合true。それ以外はfalse。
	 */
	protected ConditionTemplate setJoinCtg( boolean isJoinCtg ) {
		this.isJoinCtg = isJoinCtg;
		return this;
	}

	@Override
	public boolean isJoinCtg() {
		return isJoinCtg;
	}

	/**
	 * マイルストーンと外部結合（LEFT JOIN）を行うかどうかを設定します
	 * <br>isJoinMstoneの戻り値がtrueの場合のみ有効です。
	 * @see UmTables#UM_MSTONE カテゴリー
	 *
	 * @param isJoinMstone 外部結合を行う場合true。それ以外はfalse。
	 */
	protected ConditionTemplate setJoinMstone( boolean isJoinMstone ) {
		this.isJoinMstone = isJoinMstone;
		return this;
	}

	@Override
	public boolean isJoinMstone() {
		return isJoinMstone;
	}

	/**
	 * Queryを組みたてるための文字列にappendします
	 *
	 * @param item テーブル項目
	 * @param value 要素に設定する値
	 * @return a reference to this instance, so it's possible to chain several calls together
	 */
	protected ConditionTemplate append( ITableItem item, Object value ) {
		if ( null == item )
			return this;

		entityNameMap.put(item.getItemName(), item.name());

		if ( null == value ) {
			if ( itemMap.containsKey( item.getItemName() ) )
				itemMap.remove( item.getItemName() );

			return this;
		}

		itemMap.put( item.getItemName(), value );

		return this;
	}

	/**
	 * Queryを組みたてるための文字列にappendします
	 *
	 * @param item テーブル項目
	 * @param value 要素に設定する値
	 * @return a reference to this instance, so it's possible to chain several calls together
	 */
	protected ConditionTemplate append( String item, String value ) {
		if ( TriStringUtils.isEmpty(item) )
			return this;

		if ( null == value ) {
			if ( extendedItemMap.containsKey(item) )
				extendedItemMap.remove(item);
			return this;
		}

		extendedItemMap.put( item, value );

		return this;
	}

	/**
	 * Queryを組みたてるための文字列にappendします
	 * <br>“実行中データのステータス”と外部結合を行う場合のみ使用します
	 *
	 * @param item テーブル項目
	 * @param value 要素に設定する値
	 * @return a reference to this instance, so it's possible to chain several calls together
	 */
	protected ConditionTemplate appendByJoinExecData( String item, String value ) {
		if ( TriStringUtils.isEmpty(item) )
			return this;

		if ( null == value ) {
			if ( extendedItemMapByJoinExecData.containsKey(item) )
				extendedItemMapByJoinExecData.remove(item);
			return this;
		}

		extendedItemMapByJoinExecData.put( item, value );

		return this;
	}

	@Override
	public String toQueryString() {
		StringBuilder builder = new StringBuilder();

		builder = this.setSqlBuilder(builder, itemMap, extendedItemMap);

		String sql = builder.toString();

		if ( ! TriStringUtils.isEmpty(sql) ) {
			sql = " WHERE " + sql;
		}

		return sql;
	}

	@Override
	public String toQueryStringByJoinExecData() {
		StringBuilder builder = new StringBuilder();

		builder = this.setSqlBuilder(builder, itemMapByJoinExecData, extendedItemMapByJoinExecData);

		String sql = builder.toString();

		if ( ! TriStringUtils.isEmpty(sql) ) {
			sql = " WHERE " + sql;
		}

		return sql;
	}

	private StringBuilder setSqlBuilder( StringBuilder builder, Map<String, Object> itemMap, Map<String, String> extendedItemMap ) {

		for ( Entry<String, Object> entry: itemMap.entrySet() ) {
			if ( 0 != builder.length() ) {
				builder.append(" and ");
			}

			String key = entityNameMap.get( entry.getKey() );
			builder.append( entry.getKey() ).append( "=:" ).append( key );
		}

		for ( Entry<String, String> entry: extendedItemMap.entrySet() ) {
			if ( 0 != builder.length() ) {
				builder.append(" and ");
			}
			builder.append( entry.getValue() );
		}

		return builder;
	}

	@Override
	public SqlParameterSource getSqlParameter() {
		MapSqlParameterSource paramSource = new MapSqlParameterSource();

		for ( Entry<String, Object> entry: itemMap.entrySet() ) {
			paramSource.addValue(entityNameMap.get( entry.getKey() ), entry.getValue());
		}

		return paramSource;
	}

}
