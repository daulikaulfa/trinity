package jp.co.blueship.tri.fw.dao.orm.shun;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import org.springframework.jdbc.datasource.AbstractDataSource;

/**
 * Shunsakuコネクションを管理するデータソースです。
 */
public class ShunDataSource extends AbstractDataSource {

	

	private Properties props = null;

	/**
	 * Shunsakuコネクションに指定するプロパティ情報を設定します。
	 *
	 * <DD>以下のキー値を設定します。</DD>
	 * <DD>connection.hostには、接続先のホスト名またはIPアドレスを指定してください。</DD>
	 * <DD>connection.portには、接続先のポート番号を指定してください。</DD>
	 * <DD>connection.shunsakuFileには、接続先のShunsaku File名を指定してください。</DD>
	 *
	 * @param properties Shunsakuに接続するための情報を持ったプロパティリスト
	 */
	public void setDataSourceAttributes(Properties properties) {
		this.props = properties;
	}
	/**
	 * Shunsakuコネクションに指定するプロパティ情報を取得します。
	 * @return properties Shunsakuに接続するための情報を持ったプロパティリスト
	 */
	public Properties getDataSourceAttributes() {
		return this.props ;
	}
	/**
	 * Shunsakuコネクションを取得します。
	 */
	public Connection getConnection() throws SQLException {

		try {

			if ( null != props ) {
				return new ShunConnectionWrapper( props );
			}

			throw new SQLException("can not Injection method := setDataSourceAttribute ");

		} finally {
		}
	}

	/**
	 * <B>推奨されないメソッドです。</B>
	 * <BR>Shunsakuでは、別メソッド {@link #getConnection()}に置き換えられています。
	 * <BR>このメソッドを呼び出すと、パラメタの内容は無視され、{@link #getConnection()}を呼び出し
	 * た場合と同じになります。
	 * @deprecated
	 */
	public Connection getConnection(String username, String password)
			throws SQLException {
		return getConnection();
	}
	/**
	 * @deprecated JRE1.6未対応
	 */
	@Override
	public <T> T unwrap(Class<T> iface) throws SQLException {
		return null;
	}
	/**
	 * @deprecated JRE1.6未対応
	 */
	@Override
	public boolean isWrapperFor(Class<?> iface) throws SQLException {
		return false;
	}

}
