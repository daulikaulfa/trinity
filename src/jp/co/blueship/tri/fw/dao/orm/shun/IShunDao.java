package jp.co.blueship.tri.fw.dao.orm.shun;

import jp.co.blueship.tri.fw.dao.orm.IDao;

/**
 * Shunsakuを利用するDAO(Data Access Object)が継承する必要のある、インタフェースです。
 *
 */
public interface IShunDao extends IDao {
	
	/**
     * 検索件数を取得します。
     * @param condition 検索条件
     * @return 取得した検索結果を戻します。
     */
	public int getCount( ICondition condition );

}
