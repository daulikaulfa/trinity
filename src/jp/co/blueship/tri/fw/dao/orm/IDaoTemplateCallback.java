package jp.co.blueship.tri.fw.dao.orm;

/**
 * The interface of callback for the DAO template.
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public interface IDaoTemplateCallback {
	/**
	 * Appends the string representation of the SQL statement  for "SELECT clause" to the DAO.
	 * <br>
	 * <br>SELECT句に追加する文字列を取得します。
	 *
	 * @param condition
	 * @param isCountMode
	 * @return
	 */
	public String appendOfSelectClause( ISqlCondition condition, boolean isCountMode );

	/**
	 * Appends the string representation of the SQL statement  for "FROM clause" to the DAO.
	 * <br>
	 * <br>FROM句に追加する文字列を取得します。
	 *
	 * @param condition
	 * @param isCountMode
	 * @return
	 */
	public String appendOfFromClause( ISqlCondition condition, boolean isCountMode );

}
