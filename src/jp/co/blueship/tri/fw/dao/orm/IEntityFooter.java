package jp.co.blueship.tri.fw.dao.orm;

import java.sql.Timestamp;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;

/**
 * テーブルのエンティティが継承する必要のある、インタフェースです。
 * <br>エンティティ共通のfooterです。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public interface IEntityFooter extends IEntity {
	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId();
	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId);
	/**
	 * registration user-IDを取得します。
	 * @return registration user-ID
	 */
	public String getRegUserId();
	/**
	 * registration user-IDを設定します。
	 * @param regUserId registration user-ID
	 */
	public void setRegUserId(String regUserId);
	/**
	 * registration user nameを取得します。
	 * @return registration user name
	 */
	public String getRegUserNm();
	/**
	 * registration user nameを設定します。
	 * @param regUserNm registration user name
	 */
	public void setRegUserNm(String regUserNm);
	/**
	 * registration time stampを取得します。
	 * @return registration time stamp
	 */
	public Timestamp getRegTimestamp();
	/**
	 * registration time stampを設定します。
	 * @param regTimestamp registration time stamp
	 */
	public void setRegTimestamp(Timestamp regTimestamp);
	/**
	 * update user-IDを取得します。
	 * @return update user-ID
	 */
	public String getUpdUserId();
	/**
	 * update user-IDを設定します。
	 * @param updUserId update user-ID
	 */
	public void setUpdUserId(String updUserId);
	/**
	 * update user nameを取得します。
	 * @return update user name
	 */
	public String getUpdUserNm();
	/**
	 * update user nameを設定します。
	 * @param updUserNm update user name
	 */
	public void setUpdUserNm(String updUserNm);
	/**
	 * update time stampを取得します。
	 * @return update time stamp
	 */
	public Timestamp getUpdTimestamp();
	/**
	 * update time stampを設定します。
	 * @param updTimestamp update time stamp
	 */
	public void setUpdTimestamp(Timestamp updTimestamp);
}
