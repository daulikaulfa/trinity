package jp.co.blueship.tri.fw.dao.orm.constants;

/**
 * The interface of the table attribute.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 */
public interface ITableAttribute {
	/**
	 *
	 * @return table name
	 */
	public String name();
	/**
	 *
	 * @return table alias
	 */
	public String alias();
	/**
	 *
	 * @return table name with alias
	 */
	public String nameWithAlias();
}
