package jp.co.blueship.tri.fw.dao.orm.shun.rpc;

import javax.sql.DataSource;

import jp.co.blueship.tri.fw.dao.orm.ILimit;
import jp.co.blueship.tri.fw.dao.orm.shun.IShunTemplate;
import jp.co.blueship.tri.fw.dao.orm.shun.ShunMapper;
import jp.co.blueship.tri.fw.dao.orm.shun.ShunTemplate;
import jp.co.blueship.tri.fw.dao.orm.shun.rpc.dto.ShunRemoteList;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;


/**
 * Shunsakuをリモートから利用するためのアクセステンプレートクラスです。
 */
public class ShunTemplateRemoteService implements IShunTemplateRemote {

	/**
	 * アクセステンプレートを生成します。
	 */
	public ShunTemplateRemoteService() {
	}

    /**
     * Shunsakuを利用するためのアクセステンプレートを取得します。
	 * @param datasource Shunsakuデータソース名
     * @return アクセステンプレート
     */
    protected final IShunTemplate getShunTemplate( String datasource ) {
    	IContextAdapter context = ContextAdapterFactory.getContextAdapter();

        return new ShunTemplate( (DataSource)context.getBean( datasource ) );
    }

	/**
	 * ダイレクトアクセスキーでデータ検索を行います。
	 *
	 * @param datasource Shunsakuデータソース名
	 * @param key ダイレクトアクセスキー
	 * @param keyValues キー値
	 * @param returnExp リターン式
	 * @param map マッピングルール
	 * @return 検索結果をマッピングルールに変換して戻します。
	 * @throws Exception Shunsaku Java APIの例外
	 */
	public final ShunRemoteList<?> findByKey( String datasource, String key, ShunRemoteList<String[]> keyValues, String returnExp, ShunMapper map )
			throws Exception {
		ShunRemoteList<Object> outList = new ShunRemoteList<Object>();

		outList.addAll( this.getShunTemplate(datasource).findByKey(key, keyValues, returnExp, map) );

		return outList;
	}

	/**
	 * 指定された条件でデータ検索を行います。
	 *
	 * @param datasource Shunsakuデータソース名
	 * @param queryExp 検索式
	 * @param returnExp リターン式
	 * @param map マッピングルール
	 * @return 検索結果をマッピングルールに変換して戻します。
	 * @throws Exception Shunsaku Java APIの例外
	 */
	public final ShunRemoteList<?> find( String datasource, String queryExp, String returnExp, ShunMapper map ) throws Exception {
		ShunRemoteList<Object> outList = new ShunRemoteList<Object>();

		outList.addAll( this.getShunTemplate(datasource).find(queryExp, returnExp, map) );

		return outList;
	}

	/**
	 * 指定された条件でデータ検索を行います。
	 *
	 * @param datasource Shunsakuデータソース名
	 * @param queryExp 検索式
	 * @param returnExp リターン式
	 * @param limit ページ制御
	 * @param map マッピングルール
	 * @return 検索結果をマッピングルールに変換して戻します。
	 * @throws Exception Shunsaku Java APIの例外
	 */
	public final ShunRemoteList<?> find( String datasource, String queryExp, String returnExp, ILimit limit, ShunMapper map ) throws Exception {
		ShunRemoteList<Object> outList = new ShunRemoteList<Object>();

		outList.addAll( this.getShunTemplate(datasource).find(queryExp, returnExp, limit, map) );

		return outList;
	}

	/**
	 * 指定された条件でデータ検索を行います。
	 *
	 * @param datasource Shunsakuデータソース名
	 * @param queryExp 検索式
	 * @param returnExp リターン式
	 * @param sortExp ソート式
	 * @param map マッピングルール
	 * @return 検索結果をマッピングルールに変換して戻します。
	 * @throws Exception Shunsaku Java APIの例外
	 */
	public final ShunRemoteList<?> find( String datasource, String queryExp, String returnExp, String sortExp, ShunMapper map ) throws Exception {
		ShunRemoteList<Object> outList = new ShunRemoteList<Object>();

		outList.addAll( this.getShunTemplate(datasource).find(queryExp, returnExp, sortExp, map) );

		return outList;
	}

	/**
	 * 指定された条件でデータ検索を行います。
	 *
	 * @param datasource Shunsakuデータソース名
	 * @param queryExp 検索式
	 * @param returnExp リターン式
	 * @param sortExp ソート式
	 * @param limit ページ制御
	 * @param map マッピングルール
	 * @return 検索結果をマッピングルールに変換して戻します。
	 * @throws Exception Shunsaku Java APIの例外
	 */
	public final ShunRemoteList<?> find(
						String datasource,
						String queryExp,
						String returnExp,
						String sortExp,
						ILimit limit,
						ShunMapper map ) throws Exception {
		ShunRemoteList<Object> outList = new ShunRemoteList<Object>();

		outList.addAll( this.getShunTemplate(datasource).find(queryExp, returnExp, sortExp, limit, map) );

		return outList;
	}

	/**
	 * 指定された検索式の件数を取得します。
	 * <br>デフォルトで、ソート制約ありになるため、最大値はプロパティ指定値（HitCountLimit）となります
	 * @param datasource Shunsakuデータソース名
	 * @param queryExp 検索式
	 * @return 件数を取得します。
	 * @throws Exception Shunsaku Java APIの例外
	 */
	public final Integer countQuery( String datasource, String queryExp )
			throws Exception {
		Integer ret = 0;

		ret = new Integer( this.getShunTemplate(datasource).countQuery(queryExp) );

		return ret;
	}

	/**
	 * 指定された検索式の件数を取得します。
	 * @param datasource Shunsakuデータソース名
	 * @param queryExp 検索式
 	 * @param isLimit ページ制御有りの件数かどうか。trueの場合、最大値はプロパティ指定値（HitCountLimit）となります
	 * @return 件数を取得します。
	 * @throws Exception Shunsaku Java APIの例外
	 */
	public final Integer countQuery( String datasource, String queryExp, boolean isLimit )
			throws Exception {
		Integer ret = 0;

		ret = new Integer( this.getShunTemplate(datasource).countQuery(queryExp, isLimit) );

		return ret;
	}

	/**
	 * 指定された検索式の件数を取得します。
     * <br>全体の表示最大件数＜＝０の場合、デフォルト値が適用されます。
	 * @param datasource Shunsakuデータソース名
	 * @param queryExp 検索式
     * @param maxHitLimit 全体の最大表示件数
 	 * @param isLimit ページ制御有りの件数かどうか。trueの場合、最大値はプロパティ指定値（HitCountLimit）となります
	 * @return 件数を取得します。
	 * @throws Exception Shunsaku Java APIの例外
	 */
	public Integer countQuery(String datasource, String queryExp, int maxHitLimit, boolean isLimit) throws Exception {
		Integer ret = 0;

		ret = new Integer( this.getShunTemplate(datasource).countQuery(queryExp, maxHitLimit, isLimit) );

		return ret;
	}

	/**
	 * 設定したXML文書を、Shunsakuに追加します。
	 *
	 * @param datasource Shunsakuデータソース名
	 * @param data XML文書の文字列
	 * @return 0
	 * @throws Exception Shunsaku Java APIの例外
	 */
	public final Integer insert( String datasource, String data ) throws Exception {
		Integer ret = 0;

		ret = new Integer( this.getShunTemplate(datasource).insert(data) );

		return ret;
	}

	/**
	 * ダイレクトアクセスキーに該当するXML文書を、指定された文字列で上書きします。
	 *
	 * @param datasource Shunsakuデータソース名
	 * @param key ダイレクトアクセスキー
	 * @param keyValues キー値
	 * @param data XML文書の文字列
	 * @return 0
	 * @throws Exception Shunsaku Java APIの例外
	 */
	public final Integer updateByKey( String datasource, String key, String[] keyValues, String data ) throws Exception {
		Integer ret = 0;

		ret = new Integer( this.getShunTemplate(datasource).updateByKey(key, keyValues, data) );

		return ret;
	}

	/**
	 * 指定された検索式に該当するXML文書を、指定された文字列で上書きします。
	 *
	 * @param datasource Shunsakuデータソース名
	 * @param queryExp 検索式
	 * @param data XML文書の文字列
	 * @return 0
	 * @throws Exception Shunsaku Java APIの例外
	 */
	public final Integer update( String datasource, String queryExp, String data ) throws Exception {
		Integer ret = 0;

		ret = new Integer( this.getShunTemplate(datasource).update(queryExp, data) );

		return ret;
	}

	/**
	 * ダイレクトアクセスキーに該当するXML文書を、すべて削除します。
	 *
	 * @param datasource Shunsakuデータソース名
	 * @param key ダイレクトアクセスキー
	 * @param keyValues キー値
	 * @return 0
	 * @throws Exception Shunsaku Java APIの例外
	 */
	public final Integer deleteByKey( String datasource, String key, String[] keyValues ) throws Exception {
		Integer ret = 0;

		ret = new Integer( this.getShunTemplate(datasource).deleteByKey(key, keyValues) );

		return ret;
	}

	/**
	 * 指定された検索式に該当するＸＭＬ文書を、すべて削除します。
	 *
	 * @param datasource Shunsakuデータソース名
	 * @param queryExp 検索式
	 * @return 0
	 * @throws Exception Shunsaku Java APIの例外
	 */
	public final Integer delete( String datasource, String queryExp ) throws Exception {
		Integer ret = 0;

		ret = new Integer( this.getShunTemplate(datasource).delete(queryExp) );

		return ret;
	}

}
