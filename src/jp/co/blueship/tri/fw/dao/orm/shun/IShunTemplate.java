package jp.co.blueship.tri.fw.dao.orm.shun;

import java.sql.SQLException;
import java.util.List;

import jp.co.blueship.tri.fw.dao.orm.ILimit;


/**
 * Shunsakuを利用するためのアクセステンプレートインタフェースです。
 */
public interface IShunTemplate {

	

	/**
	 * ダイレクトアクセスキーでデータ検索を行います。
	 *
	 * @param key ダイレクトアクセスキー
	 * @param keyValues キー値
	 * @param returnExp リターン式
	 * @param map マッピングルール
	 * @return 検索結果をマッピングルールに変換して戻します。
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	public List<?> findByKey( String key, List<String[]> keyValues, String returnExp, ShunMapper map )
			throws SQLException;

	/**
	 * 指定された条件でデータ検索を行います。
	 *
	 * @param queryExp 検索式
	 * @param returnExp リターン式
	 * @param map マッピングルール
	 * @return 検索結果をマッピングルールに変換して戻します。
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	public List<?> find( String queryExp, String returnExp, ShunMapper map ) throws SQLException;

	/**
	 * 指定された条件でデータ検索を行います。
	 *
	 * @param queryExp 検索式
	 * @param returnExp リターン式
	 * @param limit ページ制御
	 * @param map マッピングルール
	 * @return 検索結果をマッピングルールに変換して戻します。
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	public List<?> find( String queryExp, String returnExp, ILimit limit, ShunMapper map ) throws SQLException;

	/**
	 * 指定された条件でデータ検索を行います。
	 *
	 * @param queryExp 検索式
	 * @param returnExp リターン式
	 * @param sortExp ソート式
	 * @param map マッピングルール
	 * @return 検索結果をマッピングルールに変換して戻します。
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	public List<?> find( String queryExp, String returnExp, String sortExp, ShunMapper map ) throws SQLException;

	/**
	 * 指定された条件でデータ検索を行います。
	 *
	 * @param queryExp 検索式
	 * @param returnExp リターン式
	 * @param sortExp ソート式
	 * @param limit ページ制御
	 * @param map マッピングルール
	 * @return 検索結果をマッピングルールに変換して戻します。
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	public List<?> find( 	String queryExp,
						String returnExp,
						String sortExp,
						ILimit limit,
						ShunMapper map ) throws SQLException;

	/**
	 * 指定された検索式の件数を取得します。
	 * @param queryExp 検索式
	 * @return 件数を取得します。
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	public int countQuery( String queryExp )  throws SQLException;

	/**
	 * 指定された検索式の件数を取得します。
	 * @param queryExp 検索式
	 * @param isLimit ページ制御有りの件数かどうか。trueの場合、最大値はプロパティ指定値（HitCountLimit）となります
	 * @return 件数を取得します。
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	public int countQuery( String queryExp, boolean isLimit )  throws SQLException;

	/**
	 * 指定された検索式の件数を取得します。
	 * @param queryExp 検索式
     * @param maxHitLimit 全体の最大表示件数
	 * @param isLimit ページ制御有りの件数かどうか。trueの場合、最大値はプロパティ指定値（HitCountLimit）となります
	 * @return 件数を取得します。
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	public int countQuery( String queryExp, int maxHitLimit, boolean isLimit )  throws SQLException;

	/**
	 * 設定したXML文書を、Shunsakuに追加します。
	 *
	 * @param data XML文書の文字列
	 * @return 0
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	public int insert( String data ) throws SQLException;

	/**
	 * ダイレクトアクセスキーに該当するXML文書を、指定された文字列で上書きします。
	 *
	 * @param key ダイレクトアクセスキー
	 * @param keyValues キー値
	 * @param data XML文書の文字列
	 * @return 0
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	public int updateByKey( String key, String[] keyValues, String data ) throws SQLException;

	/**
	 * 指定された検索式に該当するXML文書を、指定された文字列で上書きします。
	 *
	 * @param queryExp 検索式
	 * @param data XML文書の文字列
	 * @return 0
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	public int update( String queryExp, String data ) throws SQLException;

	/**
	 * ダイレクトアクセスキーに該当するXML文書を、すべて削除します。
	 *
	 * @param key ダイレクトアクセスキー
	 * @param keyValues キー値
	 * @return 0
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	public int deleteByKey( String key, String[] keyValues ) throws SQLException;

	/**
	 * 指定された検索式に該当するＸＭＬ文書を、すべて削除します。
	 *
	 * @param queryExp 検索式
	 * @return 0
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	public int delete( String queryExp ) throws SQLException;


}
