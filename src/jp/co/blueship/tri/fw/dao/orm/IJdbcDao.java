package jp.co.blueship.tri.fw.dao.orm;

import java.util.List;

import jp.co.blueship.tri.fw.dao.orm.ex.TriJdbcDaoException;
import jp.co.blueship.tri.fw.ex.TriSystemException;


/**
 * The interface of the JDBC data access object.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public interface IJdbcDao<E> extends IDao {

	/**
     * 検索件数を取得します。
     * @param condition 検索条件
     * @return 取得した検索結果を戻します。
     */
	public int count( ISqlCondition condition ) throws TriJdbcDaoException;

	/**
	 * 主キーで検索を行います。
	 *
     * @param condition 主キー
	 * @return 検索結果を戻します。条件に該当する情報が1件もない場合はNull。
	 * @throws TriJdbcDaoException
	 */
	public E findByPrimaryKey( ISqlCondition condition ) throws TriJdbcDaoException;

	/**
	 * 指定された検索条件で検索を行います。
	 *
     * @param condition 検索条件
     * @param sort ソート条件
	 * @return 検索結果を戻します。
	 * @throws TriJdbcDaoException
	 */
	public List<E> find( ISqlCondition condition, ISqlSort sort ) throws TriJdbcDaoException;

	/**
	 * 指定された検索条件で検索を行います。
	 *
     * @param condition 検索条件
     * @param sort ソート条件
     * @param pageNo 表示するページ番号
     * @param viewRows １画面の表示件数
     * @param maxHitLimit 全体の最大表示件数
	 * @return 検索結果を戻します。
	 * @throws TriJdbcDaoException
	 */
	public IEntityLimit<E> find( ISqlCondition condition, ISqlSort sort, int pageNo, int viewRows , int maxHitLimit ) throws TriJdbcDaoException;

	/**
	 * 指定された検索条件で検索を行います。
	 *
     * @param condition 検索条件
     * @param sort ソート条件
     * @param pageNo 表示するページ番号
     * @param viewRows １画面の表示件数
	 * @return 検索結果を戻します。
	 * @throws TriJdbcDaoException
	 */
	public IEntityLimit<E> find( ISqlCondition condition, ISqlSort sort, int pageNo, int viewRows ) throws TriJdbcDaoException;

	/**
	 * 指定された検索条件で検索を行います。
	 *
     * @param condition 検索条件
	 * @return 検索結果を戻します。
	 * @throws TriJdbcDaoException
	 */
	public List<E> find( ISqlCondition condition ) throws TriJdbcDaoException;

	/**
	 * 指定された検索条件で検索を行います。
	 *
     * @param condition 検索条件
     * @param pageNo 表示するページ番号
     * @param viewRows １画面の表示件数
	 * @return 検索結果を戻します。
	 * @throws TriJdbcDaoException
	 */
	public IEntityLimit<E> find( ISqlCondition condition, int pageNo, int viewRows ) throws TriJdbcDaoException;

    /**
	 * 登録処理を行います。
	 *
	 * @param entity 登録するエンティティ
	 * @return the number of rows affected
	 * @throws TriJdbcDaoException
	 */
	public int insert( E entity ) throws TriJdbcDaoException;

    /**
	 * 登録処理を行います。
	 * <br>大量のレコードを一度に登録する際に利用します。
	 *
	 * @param entity 登録するエンティティ
	 * @return the number of rows affected
	 * @throws TriJdbcDaoException
	 */
	public int[] insert( List<E> entities ) throws TriJdbcDaoException;

    /**
	 * 更新処理を行います。
	 * <br>Nullでない要素のみを更新対象とします。
	 *
	 * @param entity 更新するエンティティ
	 * @return the number of rows affected
	 * @throws TriJdbcDaoException
	 */
	public int update( E entity ) throws TriJdbcDaoException;

    /**
	 * 更新処理を行います。
	 * <br>Nullでない要素のみを更新対象とします。
	 * <br>大量のレコードを一度に更新する際に利用します。
	 *
	 * @param entities 更新するエンティティ
	 * @return the number of rows affected
	 * @throws TriJdbcDaoException
	 */
	public int[] update( List<E> entities ) throws TriJdbcDaoException;

    /**
	 * 条件に該当する対象レコードすべてに更新処理を行います。
	 * <br>Nullでない要素のみを更新対象とします。
	 *
	 * @param condition 検索条件
	 * @param entity 更新するエンティティ
	 * @return the number of rows affected
	 * @throws TriJdbcDaoException
	 */
	public int update( ISqlCondition condition, E entity ) throws TriJdbcDaoException;

    /**
	 * 削除処理を行います。
     * @param condition 検索条件
	 * @return the number of rows affected
	 * @throws TriSystemException
	 */
	public int delete( ISqlCondition condition ) throws TriJdbcDaoException;

}
