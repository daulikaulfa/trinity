package jp.co.blueship.tri.fw.dao.orm;

import java.sql.Timestamp;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;

/**
 * The implements of the IEntityFooter.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 */
public class EntityFooter implements IEntityFooter {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * DAO検索により取得したEntityかどうか?
	 */
	public boolean forReading = false;

	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	/**
	 * registration user-ID
	 */
	public String regUserId = null;
	/**
	 * registration user name
	 */
	public String regUserNm = null;
	/**
	 * registration time stamp
	 */
	public Timestamp regTimestamp = null;
	/**
	 * update user-ID
	 */
	public String updUserId = null;
	/**
	 * update user name
	 */
	public String updUserNm = null;
	/**
	 * update time stamp
	 */
	public Timestamp updTimestamp = null;

	/**
	 * DAO検索により取得したEntityかどうかを取得します。
	 * @return DAO検索されたEntityであればtrue。それ以外はfalse
	 */
	public boolean isForReading() {
	    return forReading;
	}
	/**
	 * DAO検索により取得したEntityかどうかを設定します。
	 * @param DAO検索されたEntityであればtrue。それ以外はfalse
	 */
	public void setForReading(boolean forReading) {
	    this.forReading = forReading;
	}

	@Override
	public StatusFlg getDelStsId() {
	    return delStsId;
	}
	@Override
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	}
	@Override
	public String getRegUserId() {
	    return regUserId;
	}
	@Override
	public void setRegUserId(String regUserId) {
	    this.regUserId = regUserId;
	}
	@Override
	public String getRegUserNm() {
	    return regUserNm;
	}
	@Override
	public void setRegUserNm(String regUserNm) {
	    this.regUserNm = regUserNm;
	}
	@Override
	public Timestamp getRegTimestamp() {
	    return regTimestamp;
	}
	@Override
	public void setRegTimestamp(Timestamp regTimestamp) {
	    this.regTimestamp = regTimestamp;
	}
	@Override
	public String getUpdUserId() {
	    return updUserId;
	}
	@Override
	public void setUpdUserId(String updUserId) {
	    this.updUserId = updUserId;
	}
	@Override
	public String getUpdUserNm() {
	    return updUserNm;
	}
	@Override
	public void setUpdUserNm(String updUserNm) {
	    this.updUserNm = updUserNm;
	}
	@Override
	public Timestamp getUpdTimestamp() {
	    return updTimestamp;
	}
	@Override
	public void setUpdTimestamp(Timestamp updTimestamp) {
	    this.updTimestamp = updTimestamp;
	}
}