package jp.co.blueship.tri.fw.dao.orm.constants;



/**
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class SortItem implements ITableItem {

	private String element = null;

	@Override
	public String getItemName() {
		return this.element;
	}

	public void setItemName( String element ) {
		this.element = element;
	}

	@Override
	@Deprecated
	public String name() {
		return this.element;
	}
}
