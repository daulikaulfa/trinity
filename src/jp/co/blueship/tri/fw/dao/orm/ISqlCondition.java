package jp.co.blueship.tri.fw.dao.orm;

import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionTemplate;

/**
 * The interface of the SQL conditon object.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 * @version V4.03.00
 * @author Cuong Nguyen
 */
public interface ISqlCondition {
	/**
	 * テーブル属性インタフェースを取得します。
	 * @return 取得したテーブル属性を戻します。
	 */
	public ITableAttribute getTableAttribute();

	/**
	 * 指定された検索条件から、DB用の検索条件を生成します。
	 *
	 * @return DBの検索条件を戻します。
	 */
	public String toQueryString();

	/**
	 * 指定された検索条件（“実行中データのステータス”と外部結合する条件）から、DB用の検索条件を生成します。
	 *
	 * @return DBの検索条件を戻します。
	 */
	public String toQueryStringByJoinExecData();

	/**
	 * SQL Parameterを取得します。
	 *
	 * @return SQL Parameterを戻します。
	 */
	public SqlParameterSource getSqlParameter();

	/**
	 * “実行中データのステータス”と外部結合（LEFT JOIN）を行うかどうかを判定します。
	 * @see jp.co.blueship.tri.fw.sm.dao.constants.SmTables#SM_EXEC_DATA_STS
	 *
	 * @return 外部結合を行う場合true。それ以外はfalseを戻します。
	 */
	public boolean isJoinExecData();

	/**
	 * isJoinExecDataの戻り値がtrueの場合、結合キーを取得するための項目名を取得します。
	 * @see ISqlCondition#isJoinExecData()
	 *
	 * @return 対象レコードのキー項目
	 */
	public ITableItem getKeyByJoinExecData();

	/**
	 * “アクセス履歴”と外部結合（LEFT JOIN）を行うかどうかを判定します。
	 * @see jp.co.blueship.tri.fw.um.dao.constants.UmTables#UM_ACCS_HIST
	 *
	 * @return 外部結合を行う場合true。それ以外はfalseを戻します。
	 */
	public boolean isJoinAccsHist();

	/**
	 * isJoinAccsHistの戻り値がtrueの場合、結合キーを取得するための値を取得します。
	 * @see ISqlCondition#isJoinAccsHist()
	 *
	 * @return 対象レコードのキー項目
	 */
	public String getKeyByJoinAccsHist();

	/**
	 * isJoinAccsHistの戻り値がtrueの場合、結合キーを取得するための値を設定します。
	 * @see ISqlCondition#isJoinAccsHist()
	 *
	 * @param userId 対象レコードのキー項目
	 */
	public ConditionTemplate setKeyByJoinAccsHist( String userId );

	/**
	 *
	 * Whether or not to perform "access history" and outer join (RIGHT JOIN).
	 * @see jp.co.blueship.tri.fw.um.dao.constants.UmTables # UM_ACCS_HIST
	 *
	 * @return true when performing outer join. Otherwise it returns false.
	 */
	public boolean isRightJoinAccsHist();

	/**
	 * “ユーザ”と外部結合（LEFT JOIN）を行うかどうかを判定します。
	 * @see jp.co.blueship.tri.fw.um.dao.constants.UmTables#UM_USER
	 *
	 * @return 外部結合を行う場合true。それ以外はfalseを戻します。
	 */
	public boolean isJoinUser();

	/**
	 * isJoinUserの戻り値がtrueの場合、結合キーを取得するための値を取得します。
	 * @see ISqlCondition#isJoinUser()
	 *
	 * @return 対象レコードのキー項目
	 */
	public ITableItem[] getKeyByJoinUsers();

	/**
	 * isJoinAccsHistの戻り値がtrueの場合、結合キーを取得するための値を設定します。
	 * @see ISqlCondition#isJoinUser()
	 *
	 * @param items 対象レコードのキー項目
	 */
	public ConditionTemplate setKeyByJoinUsers( ITableItem... items );

	/**
	 * “カテゴリー”と外部結合（LEFT JOIN）を行うかどうかを判定します。
	 * @see jp.co.blueship.tri.fw.um.dao.constants.UmTables#UM_CTG
	 *
	 * @return 外部結合を行う場合true。それ以外はfalseを戻します。
	 */
	public boolean isJoinCtg();

	/**
	 * “マイルストーン”と外部結合（LEFT JOIN）を行うかどうかを判定します。
	 * @see jp.co.blueship.tri.fw.um.dao.constants.UmTables#UM_MSTONE
	 *
	 * @return 外部結合を行う場合true。それ以外はfalseを戻します。
	 */
	public boolean isJoinMstone();


}
