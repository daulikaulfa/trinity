package jp.co.blueship.tri.fw.dao.orm;

import java.util.Map;

import jp.co.blueship.tri.bm.dao.bldtimeline.constants.BldTimelineItems;
import jp.co.blueship.tri.fw.constants.BmDesignBeanId;
import jp.co.blueship.tri.fw.constants.DcmDesignBeanId;
import jp.co.blueship.tri.fw.constants.DmDesignBeanId;
import jp.co.blueship.tri.fw.constants.RmDesignBeanId;
import jp.co.blueship.tri.fw.dao.orm.constants.SortItem;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;


/**
 * データアクセスソート条件の共通処理Class
 *
 * @version V3L10.01
 * @author Takashi Ono
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 */
public class DBSearchSortAddonUtil {

	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	/**
	 * リリース・タイムラインのソートオブジェクトを取得する。
	 * @return ソートオブジェクト
	 */
	public static final ISqlSort getRelTimeLineSort() {

		ISqlSort sort = new SortBuilder();
		sort.setElement(BldTimelineItems.bldLineNo, TriSortOrder.Asc, 1);

		return sort;
	}

	/**
	 * ビルドパッケージ作成履歴のソートオブジェクトを取得する。
	 * @return ビルドパッケージ作成履歴のソートオブジェクト
	 */
	public static final ISqlSort getRelUnitSortFromDesignDefineByRelUnitHistory() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( BmDesignBeanId.bpHistoryListOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( BmDesignBeanId.bpHistoryListOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine(sortItems, sortOrder, sort);
	}

	/**
	 * ビルドパッケージ・トップ画面のソート条件を取得する。
	 * @return ソート条件
	 */
	public static ISqlSort getPjtLotSortFromDesignDefineByRelUnitTop() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( BmDesignBeanId.bpTopLotRowOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( BmDesignBeanId.bpTopLotRowOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine( sortItems, sortOrder, sort );
	}

	/**
	 * ビルドパッケージ・トップ画面（パッケージ一覧）のソート情報を取得する。
	 * @return ソート情報
	 */
	public static ISqlSort getRelUnitSortFromDesignDefineByRelUnitTop() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems =
			sheet.getKeyMap( BmDesignBeanId.bpTopUnitLineOrder );
		Map<String, String> sortOrder =
			sheet.getKeyMap( BmDesignBeanId.bpTopUnitLineOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine( sortItems, sortOrder, sort );
	}

	/**
	 * ビルドパッケージ作成・ロット選択画面のソート条件を取得する。
	 * @return ソート条件
	 */
	public static ISqlSort getPjtLotSortFromDesignDefineByRelUnitEntityLotSelect() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( BmDesignBeanId.bpEntryLotSelectOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( BmDesignBeanId.bpEntryLotSelectOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine( sortItems, sortOrder, sort );
	}

	/**
	 * ビルドパッケージ・ロット履歴一覧画面のソート条件を取得する。
	 * @return ソート条件
	 */
	public static ISqlSort getPjtLotSortFromDesignDefineByRelUnitLotHistory() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( BmDesignBeanId.bpLotHistoryListOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( BmDesignBeanId.bpLotHistoryListOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine( sortItems, sortOrder, sort );
	}

	/**
	 * ビルドパッケージ作成・変更管理選択画面のソート条件を取得する。
	 * @return ビルドパッケージ作成・変更管理選択のソート条件
	 */
	public static ISqlSort getPjtSortFromDesignDefineByRelUnitEntryPjtSelect() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( BmDesignBeanId.bpEntryPjtSelectOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( BmDesignBeanId.bpEntryPjtSelectOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine( sortItems, sortOrder, sort );
	}

	/**
	 * ビルドパッケージ作成確認・変更管理一覧画面のソート条件を取得する。
	 * @return ビルドパッケージ作成確認・変更管理一覧のソート条件
	 */
	public static ISqlSort getPjtSortFromDesignDefineByRelUnitEntryConfirm() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( BmDesignBeanId.bpEntryConfirmPjtListOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( BmDesignBeanId.bpEntryConfirmPjtListOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine( sortItems, sortOrder, sort );
	}

	/**
	 * ビルドパッケージ作成確認・変更管理一覧画面のソート条件を取得する。
	 * @return ビルドパッケージ作成確認・変更管理一覧のソート条件
	 */
	public static ISqlSort getAssetApplySortFromDesignDefineByRelUnitPjtDetailView() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( BmDesignBeanId.bpPjtDetailViewAssetApplyListOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( BmDesignBeanId.bpPjtDetailViewAssetApplyListOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine( sortItems, sortOrder, sort );
	}

	/**
	 * リリース作成・ロット選択画面のソート条件を取得する。
	 * @return ロット一覧のソート条件
	 */
	public static ISqlSort getPjtLotSortFromDesignDefineByRelEntityLotSelect() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( RmDesignBeanId.rpEntryLotSelectOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( RmDesignBeanId.rpEntryLotSelectOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine( sortItems, sortOrder, sort );
	}

	/**
	 * ビルドパッケージクローズトップ画面(ロット一覧)のソート条件を取得する。
	 * @return ロット一覧のソート条件
	 */
	public static ISqlSort getPjtLotSortFromDesignDefineByRelUnitCloseTop() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems =
			sheet.getKeyMap( BmDesignBeanId.bpCloseTopOrder );
		Map<String, String> sortOrder =
			sheet.getKeyMap( BmDesignBeanId.bpCloseTopOrderBy );

		ISqlSort retSort = DBSearchSortAddonUtil.getSortFromDesignDefine( sortItems, sortOrder, sort );

		return retSort;
	}

	/**
	 * ビルドパッケージクローズのビルドパッケージ一覧のソート条件を取得する。
	 * @return ビルドパッケージのソート条件
	 */
	public static final ISqlSort getRelUnitSortFromDesignDefineByRelUnitCloseList() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems =
			sheet.getKeyMap( BmDesignBeanId.bpCloseListOrder );
		Map<String, String> sortOrder =
			sheet.getKeyMap( BmDesignBeanId.bpCloseListOrderBy );

		ISqlSort retSort = DBSearchSortAddonUtil.getSortFromDesignDefine( sortItems, sortOrder, sort );

		return retSort;
	}

	/**
	 * ビルドパッケージクローズ履歴のビルドパッケージ一覧のソート条件を取得する。
	 * @return ビルドパッケージのソート条件
	 */
	public static final ISqlSort getRelUnitSortFromDesignDefineByRelUnitCloseHistoryList() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems =
			sheet.getKeyMap( BmDesignBeanId.bpCloseHistoryListOrder );
		Map<String, String> sortOrder =
			sheet.getKeyMap( BmDesignBeanId.bpCloseHistoryListOrderBy );

		ISqlSort retSort = DBSearchSortAddonUtil.getSortFromDesignDefine( sortItems, sortOrder, sort );

		return retSort;
	}

	/**
	 * ビルドパッケージクローズ確認のソート条件を取得する。
	 * @return ビルドパッケージのソート条件
	 */
	public static final ISqlSort getRelUnitSortFromDesignDefineByRelUnitCloseRequestConfirm() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems =
			sheet.getKeyMap( BmDesignBeanId.bpCloseRequestConfirmOrder );
		Map<String, String> sortOrder =
			sheet.getKeyMap( BmDesignBeanId.bpCloseRequestConfirmOrderBy );

		ISqlSort retSort = DBSearchSortAddonUtil.getSortFromDesignDefine( sortItems, sortOrder, sort );

		return retSort;
	}

	/**
	 * ビルドパッケージ取消トップ画面(ロット一覧)のソート条件を取得する。
	 * @return ロット一覧のソート条件
	 */
	public static ISqlSort getPjtLotSelectSortFromDesignDefineByRelUnitCancel() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems =
			sheet.getKeyMap( BmDesignBeanId.bpCancelTopOrder );
		Map<String, String> sortOrder =
			sheet.getKeyMap( BmDesignBeanId.bpCancelTopOrderBy );

		ISqlSort retSort = DBSearchSortAddonUtil.getSortFromDesignDefine( sortItems, sortOrder, sort );

		return retSort;
	}

	/**
	 * ビルドパッケージ取消のビルドパッケージ一覧のソート条件を取得する。
	 * @return ビルドパッケージのソート条件
	 */
	public static final ISqlSort getUnitSortFromDesignDefineByRelUnitCancelList() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems =
			sheet.getKeyMap( BmDesignBeanId.bpCancelListOrder );
		Map<String, String> sortOrder =
			sheet.getKeyMap( BmDesignBeanId.bpCancelListOrderBy );

		ISqlSort retSort = DBSearchSortAddonUtil.getSortFromDesignDefine( sortItems, sortOrder, sort );

		return retSort;
	}

	/**
	 * ビルドパッケージ取消確認のソート条件を取得する。
	 * @return ビルドパッケージのソート条件
	 */
	public static final ISqlSort getUnitSortFromDesignDefineByRelUnitCancelConfirm() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems =
			sheet.getKeyMap( BmDesignBeanId.bpCancelConfirmOrder );
		Map<String, String> sortOrder =
			sheet.getKeyMap( BmDesignBeanId.bpCancelConfirmOrderBy );

		ISqlSort retSort = DBSearchSortAddonUtil.getSortFromDesignDefine( sortItems, sortOrder, sort );

		return retSort;
	}

	/**
	 * リリース履歴一覧のソートオブジェクトを取得する。
	 * @return ソートオブジェクト
	 */
	public static final ISqlSort getRelSortFromDesignDefineByRelHistoryList() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( RmDesignBeanId.rpHistoryListOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( RmDesignBeanId.rpHistoryListOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine(sortItems, sortOrder, sort);
	}

	/**
	 * 配付資源履歴一覧のソートオブジェクトを取得する。
	 * @return ソートオブジェクト
	 */
	public static final ISqlSort getRelSortFromDesignDefineByRelDistributeList() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( DmDesignBeanId.distributeListOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( DmDesignBeanId.distributeListOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine(sortItems, sortOrder, sort);
	}

	/**
	 * リリース閲覧のソートオブジェクトを取得する。
	 * @return ソートオブジェクト
	 */
	public static final ISqlSort getRelSortFromDesignDefineByRelUnitDetailView() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( RmDesignBeanId.rpRelUnitDetailViewRelListOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( RmDesignBeanId.rpRelUnitDetailViewRelListOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine(sortItems, sortOrder, sort);
	}

	/**
	 * リリース作成・リリース環境選択のソートオブジェクトを取得する。
	 * @return ソートオブジェクト
	 */
	public static final ISqlSort getRelEnvSortFromDesignDefineByRelEntryConfigurationSelect() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( RmDesignBeanId.rpEntryConfigurationSelectOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( RmDesignBeanId.rpEntryConfigurationSelectOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine(sortItems, sortOrder, sort);
	}

	/**
	 * リリース作成・リリース申請のソートオブジェクトを取得する。
	 * @return ソートオブジェクト
	 */
	public static final ISqlSort getRelApplySortFromDesignDefineByRelEntryBaseInfoInput() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( RmDesignBeanId.rpEntryListOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( RmDesignBeanId.rpEntryListOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine(sortItems, sortOrder, sort);
	}

	/**
	 * リリース作成・リリース希望日選択のソートオブジェクトを取得する。
	 * @return ソートオブジェクト
	 */
	public static final ISqlSort getRelCalendarSortFromDesignDefineByRelEntryBaseInfoInput() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( RmDesignBeanId.rpEntryRelWishDateSelectOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( RmDesignBeanId.rpEntryRelWishDateSelectOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine(sortItems, sortOrder, sort);
	}

	/**
	 * リリーストップのソートオブジェクトを取得する。
	 * @return ソートオブジェクト
	 */
	public static final ISqlSort getRelEnvSortFromDesignDefineByRelTop() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( RmDesignBeanId.rpTopOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( RmDesignBeanId.rpTopOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine(sortItems, sortOrder, sort);
	}

	/**
	 * リリース作成・確認画面のパッケージリストのソートオブジェクトを取得する。
	 * @return ビルドパッケージ選択のソートオブジェクト
	 */
	public static final ISqlSort getRelUnitSortFromDesignDefineByRelEntryConfirm() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( RmDesignBeanId.rpEntryConfirmRelUnitListOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( RmDesignBeanId.rpEntryConfirmRelUnitListOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine(sortItems, sortOrder, sort);
	}

	/**
	 * リリース作成・パッケージ選択のソートオブジェクトを取得する。
	 * @return ビルドパッケージ選択のソートオブジェクト
	 */
	public static final ISqlSort getRelUnitSortFromDesignDefineByRelEntryReleaseUnitSelect() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( RmDesignBeanId.rpEntryReleaseUnitSelectOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( RmDesignBeanId.rpEntryReleaseUnitSelectOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine(sortItems, sortOrder, sort);
	}

	/**
	 * リリース履歴詳細・ビルドパッケージのソートオブジェクトを取得する。
	 * @return リリース履歴詳細・ビルドパッケージのソートオブジェクト
	 */
	public static final ISqlSort getRelUnitSortFromDesignDefineByRelHistoryDetailView() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( RmDesignBeanId.rpHistoryDetailViewRelUnitListOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( RmDesignBeanId.rpHistoryDetailViewRelUnitListOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine(sortItems, sortOrder, sort);
	}

	/**
	 * レポート・レポート一覧画面のソート条件を取得する。
	 * @return ソート条件を戻します。
	 */
	public static ISqlSort getReportSortFromDesignDefineByReportAssetList() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( DcmDesignBeanId.reportAssetListOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( DcmDesignBeanId.reportAssetListOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine( sortItems, sortOrder, sort );
	}
	/**
	 * リリース履歴一覧・ロット選択のソートオブジェクトを取得する。
	 * @return ソートオブジェクト
	 */
	public static final ISqlSort getRelSortFromDesignDefineByRelCtlHistoryListLotSelect() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( RmDesignBeanId.rpHistoryListLotSelectOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( RmDesignBeanId.rpHistoryListLotSelectOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine(sortItems, sortOrder, sort);
	}
	/**
	 * リリース履歴一覧のソートオブジェクトを取得する。
	 * @return ソートオブジェクト
	 */
	public static final ISqlSort getRelSortFromDesignDefineByRelDistributeTimerList() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( DmDesignBeanId.distributeListOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( DmDesignBeanId.distributeListOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine(sortItems, sortOrder, sort);
	}

	/**
	 * リリース取消トップのソートオブジェクトを取得する。
	 * @return ソートオブジェクト
	 */
	public static final ISqlSort getRelSortFromDesignDefineByRelCtlCancelTop() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( RmDesignBeanId.rpCancelTopOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( RmDesignBeanId.rpCancelTopOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine(sortItems, sortOrder, sort);
	}
	/**
	 * リリース取消一覧・ロット選択のソートオブジェクトを取得する。
	 * @return ソートオブジェクト
	 */
	public static final ISqlSort getRelSortFromDesignDefineByRelCtlCancelListLotSelect() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( RmDesignBeanId.rpCancelListLotSelectOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( RmDesignBeanId.rpCancelListLotSelectOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine(sortItems, sortOrder, sort);
	}
	/**
	 * リリース取消一覧・リリースのソートオブジェクトを取得する。
	 * @return ソートオブジェクト
	 */
	public static final ISqlSort getRelSortFromDesignDefineByRelCtlCancelList() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( RmDesignBeanId.rpCancelListOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( RmDesignBeanId.rpCancelListOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine(sortItems, sortOrder, sort);
	}
	/**
	 * リリース取消確認のソート条件を取得する。
	 * @return ビルドパッケージのソート条件
	 */
	public static final ISqlSort getRelSortFromDesignDefineByRelCtlCancelConfirm() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( RmDesignBeanId.rpCancelConfirmOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( RmDesignBeanId.rpCancelConfirmOrderBy );

		ISqlSort retSort = DBSearchSortAddonUtil.getSortFromDesignDefine( sortItems, sortOrder, sort );

		return retSort;
	}

	/**
	 * ビルドパッケージ作成履歴のソートオブジェクトを取得する。
	 * @return ビルドパッケージ作成履歴のソートオブジェクト
	 */
	public static final ISqlSort getRelUnitSortFromDesignDefineByRelUnitGenerateDetailViewLotSelect() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( BmDesignBeanId.bpGenerateDetailViewLotSelectOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( BmDesignBeanId.bpGenerateDetailViewLotSelectOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine(sortItems, sortOrder, sort);
	}

	/**
	 * リリース作成状況状況・ロット選択のソートオブジェクトを取得する。
	 * @return ソートオブジェクト
	 */
	public static final ISqlSort getRelSortFromDesignDefineByRelCtlGenerateDetailViewLotSelect() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( RmDesignBeanId.rpGenerateDetailViewLotSelectOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( RmDesignBeanId.rpGenerateDetailViewLotSelectOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine(sortItems, sortOrder, sort);
	}

	/**
	 * リリース申請・トップのソートオブジェクトを取得する。
	 * @return ソートオブジェクト
	 */
	public static final ISqlSort getRelApplySortFromDesignDefineByRelApplyTop() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( RmDesignBeanId.raTopListOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( RmDesignBeanId.raTopListOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine(sortItems, sortOrder, sort);
	}

	/**
	 * リリース申請・ロット選択のソート条件を取得する。（クローズ待ち一覧、クローズ履歴一覧、パッケージ選択と併用）
	 * @return ロット一覧のソート条件
	 */
	public static ISqlSort getPjtLotSortFromDesignDefineByRelApplyTop() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( RmDesignBeanId.raTopLotSelectOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( RmDesignBeanId.raTopLotSelectOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine( sortItems, sortOrder, sort );
	}

	/**
	 * リリース申請・リリース環境選択のソートオブジェクトを取得する。（クローズ待ち一覧、クローズ履歴一覧、リリース申請登録、リリース申請編集と併用）
	 * @return ソートオブジェクト
	 */
	public static final ISqlSort getRelEnvSortFromDesignDefineByRelApplyTop() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( RmDesignBeanId.raTopConfigurationSelectOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( RmDesignBeanId.raTopConfigurationSelectOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine(sortItems, sortOrder, sort);
	}

	/**
	 * リリース申請・リリース希望日選択のソートオブジェクトを取得する。（クローズ待ち一覧、クローズ履歴一覧、リリース申請登録、リリース申請編集と併用）
	 * @return ソートオブジェクト
	 */
	public static final ISqlSort getRelCalendarSortFromDesignDefineByRelApplyTop() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( RmDesignBeanId.raTopRelWishDateSelectOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( RmDesignBeanId.raTopRelWishDateSelectOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine(sortItems, sortOrder, sort);
	}

	/**
	 * リリース申請・パッケージ選択のソートオブジェクトを取得する。
	 * @return ビルドパッケージ選択のソートオブジェクト
	 */
	public static final ISqlSort getRelUnitSortFromDesignDefineByRelApplyPackageListSelect() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( RmDesignBeanId.raPackageListSelectListOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( RmDesignBeanId.raPackageListSelectListOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine(sortItems, sortOrder, sort);
	}

	/**
	 * リリース申請・クローズ待ち一覧のソートオブジェクトを取得する。
	 * @return ソートオブジェクト
	 */
	public static final ISqlSort getRelApplySortFromDesignDefineByRelApplyCloseListView() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( RmDesignBeanId.raCloseListViewListOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( RmDesignBeanId.raCloseListViewListOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine(sortItems, sortOrder, sort);
	}

	/**
	 * リリース申請・クローズ履歴一覧のソートオブジェクトを取得する。
	 * @return ソートオブジェクト
	 */
	public static final ISqlSort getRelApplySortFromDesignDefineByRelApplyCloseHistoryListView() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( RmDesignBeanId.raCloseHistoryListViewListOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( RmDesignBeanId.raCloseHistoryListViewListOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine(sortItems, sortOrder, sort);
	}

	/**
	 * カスタマイズ設定値のソート定義を元に、Sort設定を行います。
	 *
	 * @param sortItems 要素名のマップ
	 * @param sortOrder ソート順のマップ
	 * @param sort ソーター
	 * @return 設定したソーターを戻します。
	 */
	public static ISqlSort getSortFromDesignDefine(
								Map<String, String> sortItems,
								Map<String, String> sortOrder,
								ISqlSort sort ) {
		for ( int i = sort.getLatestSeq(); i < sortItems.size(); i++ ) {

			String key;

			SortItem sItem = new SortItem();
			TriSortOrder order = TriSortOrder.Asc;

			if ( i < 9 ) {
				key = "0" + String.valueOf(i + 1);
			} else {
				key = String.valueOf(i + 1);
			}

			if ( sortItems.containsKey(key) ) {
				// ソート項目
				sItem.setItemName( sortItems.get(key) );
				// ソート順
				if ( sortOrder.containsKey(key) ) {
					if ( TriSortOrder.Desc.value().matches(sortOrder.get(key)) ) {
						order = TriSortOrder.Desc;
					} else {
						order = TriSortOrder.Asc;
					}
				}
				// ソートの設定
				sort.setElement(sItem, order, i + 1);
			}
		}

		return sort;
	}

}
