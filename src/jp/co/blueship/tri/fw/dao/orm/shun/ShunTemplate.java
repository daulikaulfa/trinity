package jp.co.blueship.tri.fw.dao.orm.shun;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.fujitsu.shun.ShunPreparedKey;
import com.fujitsu.shun.common.ShunException;

import jp.co.blueship.tri.fw.cmn.utils.DesignUtils;
import jp.co.blueship.tri.fw.dao.orm.ILimit;
import jp.co.blueship.tri.fw.dao.orm.psql.PageLimit;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.SmMessageId;

/**
 * Shunsakuを利用するためのアクセステンプレートクラスです。
 */
public class ShunTemplate implements IShunTemplate {

	

	private static final ILog log = TriLogFactory.getInstance();

	private DataSource dataSource = null;
	private static Properties prop = new Properties();
	private static int ansMax = 0;
	private static int hitCountLimit = 0;
	private static boolean isHitCountLimitException = true;
	IContextAdapter ac = ContextAdapterFactory.getContextAdapter();

	private static String PROPERTY_FILE_PATH = "shunsaku_ja_JP.properties";

	private static void load() throws TriSystemException {

		InputStream stream = null;
		try {
			if( prop.isEmpty() ) {

				String priorityPath = DesignUtils.getPriorityDefinePath() ;

				DesignUtils du	= new DesignUtils();
				stream			= du.getDefineFileStream( ShunTemplate.class, PROPERTY_FILE_PATH , priorityPath );

				prop.load( stream );
			}

			ansMax = Integer.valueOf(prop.getProperty("AnsMax"));
			hitCountLimit = Integer.valueOf(prop.getProperty("HitCountLimit"));

			String propValue = prop.getProperty("HitCountLimitException");
			isHitCountLimitException =  (null == propValue || ! "0".equals(propValue))? true: false;
		} catch (IOException e) {
			e.printStackTrace();
			throw new TriSystemException( SmMessageId.SM005211S , e );
		} finally {

			if ( null != stream ) {
				try {
					stream.close();
				} catch ( IOException ioe ) {
				}
			}
		}
	}

	/**
	 * アクセステンプレートを生成します。
	 * @param datasource Shunsakuデータソース
	 */
	public ShunTemplate( DataSource datasource ) {
		this.dataSource = datasource;
		load();
	}

	/**
	 * Shunsakuデータソースを取得します。
	 * @return Shunsakuデータソース
	 */
	private final DataSource getDataSource() {
		return dataSource;
	}

    /**
     * Shunsakuコネクションを取得します。
     * @return Shunsakuコネクション
     */
	public final ShunConnectionWrapper getShunConnection() {
		Connection con = DataSourceUtils.getConnection( this.getDataSource() );

		if ( con instanceof ShunConnectionWrapper )
			return (ShunConnectionWrapper)con;
		throw new TriSystemException( SmMessageId.SM005041S );
    }

	/**
	 * Shunsaku固有プロパティを取得します。
	 * @return 取得したプロパティを戻します。
	 */
	public final Properties getShunProperties() {
		return prop;
	}

	/**
	 * プロパティで定義された"AnsMax"の値を取得します。
	 *
	 * @return 取得したプロパティの値を戻します。
	 */
	public final static int getAnsMax() {
		return ansMax;
	}

	/**
	 * プロパティで定義された"HitCountLimit"の値を取得します。
	 *
	 * @return 取得したプロパティの値を戻します。
	 */
	public final static int getHitCountLimit() {
		return hitCountLimit;
	}

	/**
	 * プロパティで定義された"HitCountLimitException"の値を取得します。
	 *
	 * @return 取得したプロパティの値を戻します。
	 */
	public final static boolean isHitCountLimitException() {
		return isHitCountLimitException;
	}

	/**
	 * ダイレクトアクセスキーでデータ検索を行います。
	 *
	 * @param key ダイレクトアクセスキー
	 * @param keyValues キー値
	 * @param returnExp リターン式
	 * @param map マッピングルール
	 * @return 検索結果をマッピングルールに変換して戻します。
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	public List<?> findByKey( String key, List<String[]> keyValues, String returnExp, ShunMapper map )
			throws SQLException {

		if ( null == key ) {
			throw new RuntimeException( ac.getMessage(SmMessageId.SM005042S) );
		}

		if ( null == keyValues ) {
			throw new RuntimeException( ac.getMessage(SmMessageId.SM005043S) );
		}

		if ( null == returnExp ) {
			returnExp = "/";
		}

		List<Object> ret = new ArrayList<Object>();

		ShunPreparedKeyWrapper pstmt = null;
		ShunResultSetWrapper rs = null;

		try {
			ShunConnectionWrapper con = this.getShunConnection();

			pstmt = con.prepareSearchKey(key, returnExp);

			for ( String[] values : keyValues ) {
				pstmt.add( ShunFormatUtils.getKey(values) );
			}
			pstmt.setSearchType(ShunPreparedKey.SHUN_KEY_COMPLETE_MATCH);

			rs = pstmt.searchByKey();

			//検索結果を取得
			int count = 0;
			while(rs.next()) {
				ret.add( map.mapRow(rs, count) );

				count++;
			}

		} catch (SQLException e) {
			e.printStackTrace();
			throw e;
		} finally {
			if ( null != rs )
				rs.close();

			if ( null != pstmt )
				pstmt.close();
		}

		return ret;
	}

	/**
	 * 指定された条件でデータ検索を行います。
	 *
	 * @param queryExp 検索式
	 * @param returnExp リターン式
	 * @param map マッピングルール
	 * @return 検索結果をマッピングルールに変換して戻します。
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	public List<?> find( String queryExp, String returnExp, ShunMapper map ) throws SQLException {
		return this.find(queryExp, returnExp, null, null, map);
	}

	/**
	 * 指定された条件でデータ検索を行います。
	 *
	 * @param queryExp 検索式
	 * @param returnExp リターン式
	 * @param limit ページ制御
	 * @param map マッピングルール
	 * @return 検索結果をマッピングルールに変換して戻します。
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	public List<?> find( String queryExp, String returnExp, ILimit limit, ShunMapper map ) throws SQLException {
		return this.find(queryExp, returnExp, null, limit, map);
	}

	/**
	 * 指定された条件でデータ検索を行います。
	 *
	 * @param queryExp 検索式
	 * @param returnExp リターン式
	 * @param sortExp ソート式
	 * @param map マッピングルール
	 * @return 検索結果をマッピングルールに変換して戻します。
	 * @throws SQLException Shunsaku Java APIの例外
	 */

	public List<?> find( String queryExp, String returnExp, String sortExp, ShunMapper map ) throws SQLException {
		return this.find(queryExp, returnExp, sortExp, null, map);
	}

	/**
	 * 指定された条件でデータ検索を行います。
	 *
	 * @param queryExp 検索式
	 * @param returnExp リターン式
	 * @param sortExp ソート式
	 * @param limit ページ制御
	 * @param map マッピングルール
	 * @return 検索結果をマッピングルールに変換して戻します。
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	public List<?> find( 	String queryExp,
						String returnExp,
						String sortExp,
						ILimit limit,
						ShunMapper map ) throws SQLException {

		try {

			if ( null == queryExp ) {
				throw new RuntimeException( ac.getMessage(SmMessageId.SM005044S) );
			}

			if ( null == returnExp ) {
				returnExp = "/";
			}

			if ( null != sortExp ) {
				sortExp = ( 0 == sortExp.length() )? null: sortExp;
			}

			List<Object> ret = new ArrayList<Object>();

			ShunPreparedStatementWrapper pstmt = null;
			ShunResultSetWrapper rs = null;

			try {
				ShunConnectionWrapper con = this.getShunConnection();

				if ( this.getLog().isInfoEnabled() ) {
					this.getLog().info("find queryExp:=" + queryExp);
					//this.getLog().info("returnExp:=" + returnExp);
					if ( null != sortExp )
						this.getLog().info("sortExp:=" + sortExp);
				}

				pstmt = con.prepareSearch(queryExp, returnExp);
				if ( null != sortExp ) {
					pstmt.setSort(sortExp);

					if ( isHitCountLimitException ) {
						//hitCountLimitを設定すると、指定件数を超える検索結果の場合、
						//検索結果しか戻さないので、検索結果を取得したいケースでは設定してはいけない
						pstmt.setHitCountLimit( hitCountLimit );
					}
				}

				int max = ansMax;

				if ( null != limit ) {
					int position =
						( 1 >= limit.getPageBar().getValue() ) ? 1: ((limit.getPageBar().getValue() - 1) * limit.getViewRows()) + 1;

					max = limit.getViewRows();

					if ( max > limit.getMaxRows() ) {
						max = limit.getMaxRows();
					}

					pstmt.setRequest(position, max);
				} else {
					pstmt.setRequest(null, max, 0);
				}

				rs = pstmt.executeSearch();
				if ( rs.isHitCountLimitOver() && isHitCountLimitException ) {

					throw new BusinessException( SmMessageId.SM001010E , Integer.toString(rs.getHitCount()) , prop.getProperty("HitCountLimit")  );
				}

				//検索結果を取得
				if ( null == limit ) {
					ILimit ansMaxLimit = new PageLimit();

					ansMaxLimit.setMaxRows( rs.getHitCount() );
					ansMaxLimit.setViewRows( max );

					int count = 0;

//					if ( false ) {
//						//Shunsaku 1,000件問題への対処前
//						for ( int i = 1; i < ansMaxLimit.getPageBar().getMaximum() + 1; i++ ) {
//							ansMaxLimit.getPageBar().setValue( i );
//
//							if ( 1 < i ) {
//								int position = ((ansMaxLimit.getPageBar().getValue() - 1) * ansMaxLimit.getViewRows()) + 1;
//								pstmt.setRequest(position, max);
//								rs = pstmt.executeSearch();
//							}
//
//							while(rs.next()) {
//								ret.add( map.mapRow(rs, count) );
//
//								count++;
//							}
//						}
//					}

					//Shunsaku 1,000件問題への対処済
					for ( int i = 1; i < ansMaxLimit.getPageBar().getMaximum() + 1; i++ ) {
						ansMaxLimit.getPageBar().setValue( i );

						if ( 1 < i ) {
							pstmt.setRequest(rs.getLastPosition(), max, ShunPreparedStatementWrapper.SHUN_DIRECTION_FORWARD_EXCLUSIVE);
							rs = pstmt.executeSearch();
						}

						while(rs.next()) {
							ret.add( map.mapRow(rs, count) );

							count++;
						}
					}
				} else {
					int count = 0;
					while(rs.next()) {
						ret.add( map.mapRow(rs, count) );

						count++;
					}
				}

			} catch (SQLException e) {
				e.printStackTrace();
				throw e;
			} finally {
				if ( null != rs )
					rs.close();

				if ( null != pstmt )
					pstmt.close();
			}

			return ret;
		} finally {
		}

	}

	/**
	 * 指定された検索式の件数を取得します。
	 * @param queryExp 検索式
	 * @return 件数を取得します。
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	public final int countQuery( String queryExp )
			throws SQLException {

		return this.countQuery( queryExp, 0, false );
	}

	/**
	 * 指定された検索式の件数を取得します。
	 * @param queryExp 検索式
	 * @param isLimit ページ制御有りの件数かどうか。trueの場合、最大値はプロパティ指定値（HitCountLimit）となります
	 * @return 件数を取得します。
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	public final int countQuery( String queryExp, boolean isLimit )
			throws SQLException {

		return this.countQuery( queryExp, 0, isLimit );
	}

	/**
	 * 指定された検索式の件数を取得します。
     * <br>全体の表示最大件数＜＝０の場合、デフォルト値が適用されます。
	 * @param queryExp 検索式
	 * @param maxHitLimit 全体の表示最大件数
	 * @param isLimit ページ制御有りの件数かどうか。trueの場合、最大値はプロパティ指定値（HitCountLimit）となります
	 * @return 件数を取得します。
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	public final int countQuery( String queryExp, int maxHitLimit, boolean isLimit )
			throws SQLException {

		if ( null == queryExp ) {
			throw new RuntimeException("メソッドの使用方法に誤り :検索式が設定されていません。\n");
		}

		ShunPreparedStatementWrapper pstmt = null;
		ShunResultSetWrapper rs = null;

		try {
			ShunConnectionWrapper con = this.getShunConnection();

			if ( this.getLog().isInfoEnabled() ) {
				this.getLog().info("queryExp:=" + queryExp);
			}

			pstmt = con.prepareSearch(queryExp, "/");

			pstmt.setRequest(1, 0);

			rs = pstmt.executeSearch();

			int hitCount = rs.getHitCount();

			if ( this.getLog().isInfoEnabled() ) {
				this.getLog().info("hitCount:=" + hitCount);
			}

			if ( 0 < maxHitLimit && hitCount > maxHitLimit ) {
				hitCount = maxHitLimit;
			}

			if ( isLimit ) {
				if ( ! isHitCountLimitException && hitCount > hitCountLimit ) {
					//ヒット件数の上限で業務エラーとしない場合、ヒット件数を上限値に合わせる
					hitCount = hitCountLimit;
				}
			}

			return hitCount;

		} catch (SQLException e) {
			e.printStackTrace();
			throw e;
		} finally {
			if ( null != rs )
				rs.close();

			if ( null != pstmt )
				pstmt.close();
		}
	}

	/**
	 * 設定したXML文書を、Shunsakuに追加します。
	 *
	 * @param data XML文書の文字列
	 * @return 0
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	public int insert( String data ) throws SQLException {

		ShunPreparedStatementWrapper pstmt = null;

		try {
			ShunConnectionWrapper con = this.getShunConnection();

			pstmt = con.prepareInsert();
			pstmt.add( data );

			return pstmt.executeInsert();

		} catch (ShunException e) {
			e.printStackTrace();
			this.getLog().fatal("insert:=" + ((null == data)? "null": data));
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
			this.getLog().fatal("insert:=" + ((null == data)? "null": data));
			throw new RuntimeException( e );
		} finally {
			if ( null != pstmt )
				pstmt.close();
		}
	}

	/**
	 * ダイレクトアクセスキーに該当するXML文書を、指定された文字列で上書きします。
	 *
	 * @param key ダイレクトアクセスキー
	 * @param keyValues キー値
	 * @param data XML文書の文字列
	 * @return 0
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	public int updateByKey( String key, String[] keyValues, String data ) throws SQLException {

		if ( null == key ) {
			throw new RuntimeException("メソッドの使用方法に誤り :ダイレクトアクセスキーが設定されていません。\n");
		}

		if ( null == keyValues ) {
			throw new RuntimeException("メソッドの使用方法に誤り :アクセスキー値が設定されていません。\n");
		}

		ShunPreparedKeyWrapper pstmt = null;

		try {
			ShunConnectionWrapper con = this.getShunConnection();

			pstmt = con.prepareUpdateKey( key );
			pstmt.add( ShunFormatUtils.getKey(keyValues), data );
			pstmt.setSearchType(ShunPreparedKey.SHUN_KEY_COMPLETE_MATCH);

			return pstmt.updateByKey();

		} catch (ShunException e) {
			e.printStackTrace();
			throw e;
		} finally {
			if ( null != pstmt )
				pstmt.close();
		}
	}

	/**
	 * 指定された検索式に該当するXML文書を、指定された文字列で上書きします。
	 *
	 * @param queryExp 検索式
	 * @param data XML文書の文字列
	 * @return 0
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	public int update( String queryExp, String data ) throws SQLException {

		if ( null == queryExp ) {
			throw new RuntimeException("メソッドの使用方法に誤り :検索式が設定されていません。\n");
		}

		ShunPreparedRecordIDWrapper pstmt = null;

		try {
			ShunConnectionWrapper con = this.getShunConnection();

			if ( this.getLog().isInfoEnabled() ) {
				this.getLog().info("update queryExp:=" + queryExp);
			}

			List<?> recordIDs = this.getRecordID( queryExp );

			pstmt = con.prepareUpdateRecordID();

			for( Object id : recordIDs ) {
				pstmt.add((String)id, data);
			}

			if( 0 < recordIDs.size() ) {
				return pstmt.updateByRecordID();
			}

			return 0;

		} catch (ShunException e) {
			e.printStackTrace();
			this.getLog().fatal("update:=" + ((null == data)? "null": data));
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
			this.getLog().fatal("update:=" + ((null == data)? "null": data));
			throw new RuntimeException( e );
		} finally {
			if ( null != pstmt )
				pstmt.close();
		}
	}

	/**
	 * ダイレクトアクセスキーに該当するXML文書を、すべて削除します。
	 *
	 * @param key ダイレクトアクセスキー
	 * @param keyValues キー値
	 * @return 0
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	public int deleteByKey( String key, String[] keyValues ) throws SQLException {

		if ( null == key ) {
			throw new RuntimeException("メソッドの使用方法に誤り :ダイレクトアクセスキーが設定されていません。\n");
		}

		if ( null == keyValues ) {
			throw new RuntimeException("メソッドの使用方法に誤り :アクセスキー値が設定されていません。\n");
		}

		ShunPreparedKeyWrapper pstmt = null;

		try {
			ShunConnectionWrapper con = this.getShunConnection();

			pstmt = con.prepareDeleteKey( key );
			pstmt.add( ShunFormatUtils.getKey(keyValues) );
			pstmt.setSearchType(ShunPreparedKey.SHUN_KEY_COMPLETE_MATCH);

			return pstmt.deleteByKey();

		} catch (ShunException e) {
			e.printStackTrace();
			throw e;
		} finally {
			if ( null != pstmt )
				pstmt.close();
		}
	}

	/**
	 * 指定された検索式に該当するＸＭＬ文書を、すべて削除します。
	 *
	 * @param queryExp 検索式
	 * @return 0
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	public int delete( String queryExp ) throws SQLException {

		if ( null == queryExp ) {
			throw new RuntimeException("メソッドの使用方法に誤り :検索式が設定されていません。\n");
		}

		ShunPreparedRecordIDWrapper pstmt = null;

		try {
			ShunConnectionWrapper con = this.getShunConnection();

			if ( this.getLog().isInfoEnabled() ) {
				this.getLog().info("delete queryExp:=" + queryExp);
			}

			List<?> recordIDs = this.getRecordID( queryExp );

			pstmt = con.prepareDeleteRecordID();

			for( Object id : recordIDs ) {
				pstmt.add((String)id);
			}

			if( 0 < recordIDs.size() ) {
				return pstmt.deleteByRecordID();
			}

			return 0;

		} catch (ShunException e) {
			e.printStackTrace();
			throw e;
		} finally {
			if ( null != pstmt )
				pstmt.close();
		}
	}

	/**
	 * 指定された検索式で検索を行い、該当する文書のレコードＩＤを取得します。
	 * <br>取得したレコードＩＤは、文書の更新／削除を行う場合に使用します。
	 *
	 * @param queryExp 検索式
	 * @return 該当する文書のレコードＩＤのリスト
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	@SuppressWarnings("serial")
	private final List<?> getRecordID( String queryExp ) throws SQLException {

		String returnExp = "/";

		return 	this.find( queryExp, returnExp, new ShunMapper() {
								public Object mapRow( ShunResultSetWrapper resultSet, int row ) throws SQLException {
									return resultSet.getRecordID();
								}
							});

	}

	protected final ILog getLog() {
    	return log;
    }

}
