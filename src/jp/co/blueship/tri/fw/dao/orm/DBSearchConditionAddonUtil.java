package jp.co.blueship.tri.fw.dao.orm;


import jp.co.blueship.tri.am.dao.areq.eb.AreqCondition;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.am.dao.lot.eb.LotCondition;
import jp.co.blueship.tri.am.dao.pjt.eb.PjtCondition;
import jp.co.blueship.tri.bm.dao.bldenv.eb.BldEnvCondition;
import jp.co.blueship.tri.bm.dao.bp.eb.BpCondition;
import jp.co.blueship.tri.dcm.constants.DcmReportType;
import jp.co.blueship.tri.dcm.dao.rep.eb.RepCondition;
import jp.co.blueship.tri.fw.cmn.utils.ExtractStatusAddonUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusId;
import jp.co.blueship.tri.fw.constants.status.AmPjtStatusId;
import jp.co.blueship.tri.fw.constants.status.BmBldEnvStatusId;
import jp.co.blueship.tri.fw.constants.status.BmBpStatusId;
import jp.co.blueship.tri.fw.constants.status.BmBpStatusIdForExecData;
import jp.co.blueship.tri.fw.constants.status.RmRpStatusId;
import jp.co.blueship.tri.fw.constants.status.RmRpStatusIdForExecData;
import jp.co.blueship.tri.rm.dao.cal.eb.CalCondition;
import jp.co.blueship.tri.rm.dao.ra.eb.RaCondition;
import jp.co.blueship.tri.rm.dao.rp.eb.RpBpLnkCondition;
import jp.co.blueship.tri.rm.dao.rp.eb.RpCondition;
import jp.co.blueship.tri.rm.ui.rp.beans.FlowRelCtlEntryProsecutor;

/**
 * データアクセス検索条件の共通処理Class
 *
 * @varsion V3L10.02
 * @author Takashi Ono
 */
public class DBSearchConditionAddonUtil {

	/**
	 * リリースの検索条件を取得する。
	 * @param rpIds ビルドパッケージ番号
	 * @return リリースの検索条件
	 */
	public static final IJdbcCondition getRpConditionByRpIds( String[] rpIds ) {

		RpCondition condition = new RpCondition();
		if ( TriStringUtils.isEmpty( rpIds ) ) {
			condition.setRpIds( "" );
		} else {
			condition.setRpIds( rpIds );
		}

		return condition;
	}

	public static final IJdbcCondition getRpBpLnkConditionByBuildNo( String[] buildNo ) {

		RpBpLnkCondition condition = new RpBpLnkCondition();
		if ( TriStringUtils.isEmpty( buildNo ) ) {
			condition.setBpIds( "" );
		} else {
			condition.setBpIds( buildNo );
		}
  		condition.setRpStsIds(
				RmRpStatusId.ReleasePackageCreated.getStatusId(),
				RmRpStatusId.ReleasePackageClosed.getStatusId());

		return condition;
	}

	/**
	 * リリース申請の検索条件を取得する。
	 * @param raId ビルドパッケージ番号
	 * @return リリース申請の検索条件
	 */
	public static final IJdbcCondition getRelApplyConditionByRaId( String[] raId ) {

		RaCondition condition = new RaCondition();

		if ( TriStringUtils.isEmpty( raId ) ) {
			//ダミーを設定して、全件検索を防止する。
			condition.setRaIds( "" );
		} else {
			condition.setRaIds( raId );
		}

		return condition;
	}

	/**
	 * リリース申請の検索条件を取得する。
	 * @param buildNo リリース番号
	 * @return リリース申請の検索条件
	 */
	public static final IJdbcCondition getRelApplyConditionByRelNo( String[] relNo ) {

		RaCondition condition = new RaCondition();

		if ( TriStringUtils.isEmpty( relNo ) ) {
			//ダミーを設定して、全件検索を防止する。
			condition.setRelationIds( "" );
		} else {
			condition.setRelationIds( relNo );
		}

		return condition;
	}

	/**
	 * リリースの検索条件を取得する。
	 * @param buildNo ビルドパッケージ番号
	 * @return リリースの検索条件
	 */
	public static final IJdbcCondition getRpConditionByRelNo( String[] relNo ) {

		RpCondition condition = new RpCondition();

		if ( TriStringUtils.isEmpty( relNo ) ) {
			//ダミーを設定して、全件検索を防止する。
			condition.setRpIds( "" );
		} else {
			condition.setRpIds( relNo );
		}

		return condition;
	}

	/**
	 * リリースの検索条件を取得する。
	 * @param relNo リリース番号
	 * @return リリースの検索条件
	 */
	public static final IJdbcCondition getRpConditionByRelNo( String relNo ) {

		RpCondition condition = new RpCondition();
		condition.setRpId( relNo );

		return condition;
	}

	/**
	 * リリースの検索条件を取得する。
	 * @param envNo リリース環境番号
	 * @return リリースの検索条件
	 */
	public static final IJdbcCondition getRpConditionByEnvNo( String envNo ) {

		RpCondition condition = new RpCondition();
		condition.setBldEnvId( envNo );

		return condition;
	}

	/**
	 * リリースの検索条件を取得する。
	 * @param envNo リリース環境番号
	 * @param lotId ロット番号
	 * @return リリースの検索条件
	 */
	public static final IJdbcCondition getRpConditionByEnvNoLotNo( String envNo, String lotId ) {

		RpCondition condition = new RpCondition();
		condition.setBldEnvId( envNo );
		condition.setLotId( lotId );

		return condition;
	}
	/**
	 * リリースの検索条件を取得する。
	 * @param envNo リリース環境番号
	 * @param RpIds RpIds
	 * @return リリースの検索条件
	 */
	public static final IJdbcCondition getRpConditionByEnvNoRpIds( String envNo, String[] rpIds) {

		RpCondition condition = new RpCondition();
		condition.setBldEnvId( envNo );
		if ( TriStringUtils.isEmpty( rpIds ) ) {
			//ダミーを設定して、全件検索を防止する。
			condition.setRpIds( "" );
		} else {
			condition.setRpIds( rpIds );
		}

		return condition;
	}
	/**
	 * リリースの検索条件を取得する。
	 * @param envNo リリース環境番号
	 * @param lotId ロット番号
	 * @return リリースの検索条件
	 */
	public static final IJdbcCondition getRelDistributeConditionByEnvNoLotNo( String envNo, String lotId, String[] rpIdsFromDistributeStatus) {

		RpCondition condition = new RpCondition();
		condition.setBldEnvId( envNo );
		condition.setLotId( lotId );
		if (null != rpIdsFromDistributeStatus) {
			condition.setRpIds( rpIdsFromDistributeStatus ) ;
		}

		return condition;
	}
	/**
	 * 進行中のリリース環境の検索条件を取得する。
	 * @return 進行中のリリース環境の検索条件
	 */
	public static final IJdbcCondition getActivelRelEnvCondition() {

		BldEnvCondition condition = new BldEnvCondition();
		condition.setSvcId( FlowRelCtlEntryProsecutor.FLOW_ACTION_ID );
		condition.setStsIds( new String[]{ BmBldEnvStatusId.Running.getStatusId() } );

		return condition;
	}

	/**
	 * リリース環境の検索条件を取得する。
	 * @param envNo 環境番号
	 * @return リリース環境の検索条件
	 */
	public static final IJdbcCondition getRelEnvCondition( String[] envNo ) {

		BldEnvCondition condition = new BldEnvCondition();
		condition.setBldEnvIds( envNo );

		return condition;
	}

	/**
	 * ビルドパッケージ作成可能なステータスの返却申請情報の検索条件を取得する。
	 * @return ビルドパッケージ作成可能なステータスの返却申請情報の検索条件
	 */
	public static final IJdbcCondition
		getAreqConditionByGeneratableRelUnit() {

		return getAreqConditionByGeneratableRelUnitByPjtNo( new String[]{} );
	}

	/**
	 * ビルドパッケージ作成可能なステータスの返却申請情報の検索条件を取得する。
	 * @param pjtNo 変更管理番号
	 * @return ビルドパッケージ作成可能なステータスの返却申請情報の検索条件
	 */
	public static final IJdbcCondition
		getAreqConditionByGeneratableRelUnitByPjtNo( String pjtNo ) {

		return getAreqConditionByGeneratableRelUnitByPjtNo( new String[]{ pjtNo } );
	}

	/**
	 * ビルドパッケージ作成可能なステータスの返却申請情報の検索条件を取得する。
	 * @param pjtNo 変更管理番号
	 * @return ビルドパッケージ作成可能なステータスの返却申請情報の検索条件
	 */
	public static final IJdbcCondition
		getAreqConditionByGeneratableRelUnitByPjtNo( String[] pjtNo ) {

		AreqCondition condition = new AreqCondition();

		condition.setStsIds( getAssetApplyBaseStatusByGeneratableRelUnit() );

		if ( ! TriStringUtils.isEmpty( pjtNo )) {
			condition.setPjtIds( pjtNo );
		}


		return condition;
	}

	/**
	 * ビルドパッケージ作成可能なステータスの返却申請情報の検索条件を取得する。
	 * @param lotId ロット番号
	 * @return ビルドパッケージ作成可能なステータスの返却申請情報の検索条件
	 */
	public static final IJdbcCondition
		getAreqConditionByGeneratableRelUnitByLotNo( String lotId ) {

		AreqCondition condition = new AreqCondition();

		condition.setStsIds( getAssetApplyBaseStatusByGeneratableRelUnit() );
		condition.setLotId( lotId );

		return condition;
	}

	/**
	 * ビルドパッケージ作成可能な基本ステータスを取得します。
	 *
	 * @return ステータスの文字列配列を戻します。
	 */
	private static final String[] getAssetApplyBaseStatusByGeneratableRelUnit() {

		//RPクローズされた申請は、画面／レポートに表示しない
		String[] baseStatusId = {
				AmAreqStatusId.CheckinRequestApproved.getStatusId(),
				AmAreqStatusId.RemovalRequestApproved.getStatusId(),
				/* AssetApplyStatusId.BUILD_CLOSE_COMPLETE */ };

		return baseStatusId;
	}

	/**
	 * ビルドパッケージ作成可能な変更管理の検索条件を取得する。
	 * @return ビルドパッケージ作成可能な変更管理の検索条件
	 */
	public static final IJdbcCondition
		getPjtConditionByGenerateRelUnit( String[] pjtNo ) {

		PjtCondition pjtCondition = new PjtCondition();
		if ( TriStringUtils.isEmpty( pjtNo ) ) {
			//ダミーを設定して、全件検索を防止する。
			pjtCondition.setPjtIds( new String[]{ "" } );
		} else {
			pjtCondition.setPjtIds( pjtNo );
		}

		String[] statusId = { 	AmPjtStatusId.ChangePropertyInProgress.getStatusId(),
								AmPjtStatusId.ChangePropertyTestCompleted.getStatusId(),
								AmPjtStatusId.Merged.getStatusId(),
								AmPjtStatusId.ChangePropertyClosed.getStatusId() };
		pjtCondition.setStsIds( statusId );

		return pjtCondition;
	}

	/**
	 * 全ての変更管理の検索条件を取得する。
	 * @return 変更管理の検索条件
	 */
	public static final IJdbcCondition getPjtCondition( String[] pjtNo ) {

		PjtCondition pjtCondition = new PjtCondition();
		if ( TriStringUtils.isEmpty( pjtNo ) ) {
			//ダミーを設定して、全件検索を防止する。
			pjtCondition.setPjtIds( new String[]{ "" } );
		} else {
			pjtCondition.setPjtIds( pjtNo );
		}

		return pjtCondition;
	}

	/**
	 * ビルドパッケージの検索条件を取得する。
	 * @param lotId ロット番号
	 * @return ビルドパッケージの検索条件
	 */
	public static final IJdbcCondition getBuildConditionByLotNo( String lotId ) {

		BpCondition condition = new BpCondition();
		condition.setLotId( lotId );

		return condition;
	}

	/**
	 * ビルドパッケージの検索条件を取得する。
	 * @param buildNo ビルドパッケージ番号
	 * @return ビルドパッケージの検索条件
	 */
	public static final IJdbcCondition getBuildConditionByBuildNo( String[] buildNo ) {

		BpCondition condition = new BpCondition();
		condition.setBpIds( buildNo );

		return condition;
	}

	/**
	 * ビルドパッケージの検索条件を取得する。
	 * @param buildId ビルドパッケージ番号
	 * @return ビルドパッケージの検索条件
	 */
	public static final IJdbcCondition getBuildConditionByBuildNo( String buildId ) {

		BpCondition condition = new BpCondition();
		condition.setBpId( buildId );

		return condition;
	}

	/**
	 * リリース可能なステータスのビルドパッケージの検索条件を取得する。
	 * @param lotNo対象となる ロット番号の配列
	 * @return リリース可能なステータスのビルドパッケージの検索条件
	 */
	public static final IJdbcCondition
		getBuildConditionByReleasableRelUnit( String[] lotId ) {

		BpCondition condition = new BpCondition();

		String[] baseStatusId = {	BmBpStatusId.BuildPackageCreated.getStatusId(),
									BmBpStatusId.BuildPackageClosed.getStatusId() };
		condition.setStsIds( baseStatusId );

		if ( null != lotId ) {
			condition.setLotIds( (0 == lotId.length)? new String[]{""} : lotId );
		}

		return condition;
	}

	/**
	 * リリース可能なステータスのビルドパッケージの検索条件を取得する（ＲＰ複数選択時用）。
	 * @param lotId ロット番号
	 * @return リリース可能なステータスのビルドパッケージの検索条件
	 */
	public static final IJdbcCondition
		getBuildConditionByReleasableRelUnit4MultiSelectable( String lotId ) {

		BpCondition condition = new BpCondition();

		String[] baseStatusId = {	BmBpStatusId.BuildPackageCreated.getStatusId() };
		condition.setStsIds( baseStatusId );

		if ( null != lotId ) {
			condition.setLotId( lotId );
		}

		return condition;
	}

	/**
	 * ビルドパッケージ中のビルドパッケージの検索条件を取得する。
	 * @return ビルドパッケージ中のビルドパッケージの検索条件
	 */
	public static final IJdbcCondition getActiveBuildCondition() {

		BpCondition condition = new BpCondition();

		String[] statusId = { BmBpStatusIdForExecData.CreatingBuildPackage.getStatusId() };
		condition.setProcStsIds( statusId );

		return condition;
	}

	/**
	 * リリース可能なステータスのビルドパッケージの検索条件を取得する。
	 * @return リリース可能なステータスのビルドパッケージの検索条件
	 */
	public static final IJdbcCondition getBuildConditionByReleasableRelUnit() {

		return getBuildConditionByReleasableRelUnit( null );
	}

	/**
	 * 有効なロットの検索条件を取得する。
	 * @return 有効なロットの検索条件
	 */
	public static final IJdbcCondition getActiveLotCondition() {

		return getActiveLotCondition( null );
	}

	/**
	 * 有効なロットの検索条件を取得する。
	 * @param lotId ロット番号
	 * @return 有効なロットの検索条件
	 */
	public static final IJdbcCondition getActiveLotCondition( String[] lotId ) {

		LotCondition pjtLotCondition = new LotCondition();
		pjtLotCondition.setStsIds	( ExtractStatusAddonUtils.getLotBaseStatusByLotProgress() );

		if ( null != lotId ) {
			pjtLotCondition.setLotIds( (0 == lotId.length)? new String[]{""} : lotId );
		}

		return pjtLotCondition;
	}

	/**
	 * 申請情報の検索条件を取得する。
	 * @param applyNo 申請番号
	 * @return 申請情報の検索条件
	 */
	public static final IJdbcCondition getAreqCondition( String[] applyNo ) {

		AreqCondition condition = new AreqCondition();
		condition.setAreqIds( applyNo );

		return condition;
	}

	/**
	 * 削除申請情報の検索条件を取得する。
	 * @param applyNo 申請番号
	 * @param applyId 申請区分
	 * @return 申請情報の検索条件
	 */
	public static final IJdbcCondition getAreqCondition( String[] applyNo, String applyId ) {

		AreqCondition condition = new AreqCondition();

		condition.setAreqIds( applyNo );
		condition.setAreqCtgCds( new String[]{ applyId } );

		return condition;
	}

	/**
	 * ビルドパッケージのロット履歴の検索条件を取得する。
	 * @return ロットの検索条件
	 */
	public static final IJdbcCondition getLotConditionByRelUnitLotHistory() {

		LotCondition condition = new LotCondition();
		condition.setStsIds( ExtractStatusAddonUtils.getLotBaseStatusByAll() );

		return condition;
	}

	/**
	 * 指定されたロット番号の検索条件を取得する。
	 * @return ロットの検索条件
	 */
	public static final IJdbcCondition getLotConditionByLotNo( String[] lotId ) {

		LotCondition condition = new LotCondition();
		condition.setLotIds			( lotId );
		condition.setStsIds			( ExtractStatusAddonUtils.getLotBaseStatusByAll() );

		return condition;
	}

	/**
	 * ロットの検索条件を取得する。
	 * @return ロットの検索条件
	 */
	public static final IJdbcCondition getLotCondition() {

		LotCondition condition = new LotCondition();
		condition.setStsIds	( ExtractStatusAddonUtils.getLotBaseStatusByAll() );

		return condition;
	}

	/**
	 * 申請情報の検索条件を取得する。
	 * @param pjtNo 変更管理番号
	 * @return 申請情報の検索条件
	 */
	public static final IJdbcCondition getAreqCondition( String pjtNo ) {

		AreqCondition condition = new AreqCondition();

		condition.setPjtId	( pjtNo );

		return condition;
	}

	/**
	 * クローズ可能なステータスのビルドパッケージの検索条件を取得する。
	 * @param lotId ロット番号
	 * @return クローズ可能なステータスのビルドパッケージの検索条件
	 */
	public static final IJdbcCondition getBuildConditionByClosableRelUnit( String lotId ) {

		BpCondition condition = new BpCondition();

		String[] baseStatusId = { BmBpStatusId.BuildPackageCreated.getStatusId() };
		condition.setStsIds( baseStatusId );

		if ( null != lotId ) {
			condition.setLotId( lotId );
		}

		return condition;
	}

	/**
	 * クローズ可能なステータスのビルドパッケージの検索条件を取得する。
	 * <br>
	 * <br>パッケージクローズ可能な全ロットを取得する。
	 *
	 * @param lotId ロット番号
	 * @return クローズ可能なステータスのビルドパッケージの検索条件
	 */
	public static final IJdbcCondition getBuildConditionByClosableRelUnit( String[] lotId ) {

		BpCondition condition = new BpCondition();

		String[] baseStatusId = {	BmBpStatusId.BuildPackageCreated.getStatusId(),
									BmBpStatusId.BuildPackageClosed.getStatusId() };
		condition.setStsIds( baseStatusId );

		if ( null != lotId ) {
			condition.setLotIds( (0 == lotId.length)? new String[]{""} : lotId );
		}

		return condition;
	}

	/**
	 * パッケージクローズ可能な申請情報の検索条件を取得する。
	 * @param applyNo 申請番号
	 * @return クローズ可能な申請情報の検索条件
	 */
	public static final IJdbcCondition getAreqConditionByClosableRelUnit( String[] applyNo ) {

		AreqCondition condition = new AreqCondition();

		condition.setAreqIds		( applyNo );

		condition.setStsIds	( ExtractStatusAddonUtils.getAssetApplyBaseStatusByPjtApprove() );

		return condition;
	}

	/**
	 * クローズ中のビルドパッケージの検索条件を取得する。
	 * @return クローズ中のビルドパッケージの検索条件
	 */
	public static final IJdbcCondition getCloseActiveBuildCondition() {

		BpCondition condition = new BpCondition();

		String[] statusId = { BmBpStatusIdForExecData.BuildPackageClosing.getStatusId() };
		condition.setProcStsIds( statusId );

		return condition;
	}

	/**
	 * 取消可能なステータスのビルドパッケージの検索条件を取得する。
	 * @param lotId ロット番号
	 * @return 取消可能なステータスのビルドパッケージの検索条件
	 */
	public static final IJdbcCondition getBuildConditionByCancelableRelUnit( String[] lotId ) {

		BpCondition condition = new BpCondition();

		String[] procStsId = {	BmBpStatusId.BuildPackageCreated.getStatusId(),
								BmBpStatusIdForExecData.BuildPackageError.getStatusId(),
								BmBpStatusIdForExecData.BuildPackageCloseError.getStatusId() };
		condition.setProcStsIds( procStsId );

		if ( null != lotId ) {
			condition.setLotIds( (0 == lotId.length)? new String[]{""} : lotId );
		}

		return condition;
	}

	/**
	 * 取消可能なステータスのビルドパッケージの検索条件を取得する。
	 * @param envNo 環境番号
	 * @return 取消可能なステータスのビルドパッケージの検索条件
	 */
	public static final IJdbcCondition getRpConditionByCancelableRelease( String envNo ,String[] rpIds ) {

		RpCondition condition = new RpCondition();

		String[] statusId = {	RmRpStatusId.ReleasePackageCreated.getStatusId(),
								RmRpStatusIdForExecData.ReleasePackageError.getStatusId()	};
		condition.setProcStsIds( statusId );
		if (TriStringUtils.isNotEmpty(rpIds)) {
			condition.setRpIdsNotEquals( rpIds );
		}
		if ( null != envNo ) {
			condition.setBldEnvId( envNo );
		}

		return condition;
	}

	/**
	 * 取消可能なステータスのビルドパッケージの検索条件を取得する。
	 * @return 取消可能なステータスのビルドパッケージの検索条件
	 */
	public static final IJdbcCondition getBuildConditionByCancelableRelUnit() {

		return getBuildConditionByCancelableRelUnit( null );
	}

	/**
	 * 全ての申請台帳情報を取得するための検索条件を取得します。
	 * @return 全ての申請台帳情報の検索条件
	 */
	public static final RepCondition getRelReportCondition() {

		RepCondition condition = new RepCondition();

		String[] reportId = {	DcmReportType.BmBpReport.value(),
								DcmReportType.RmRpReport.value(),
								DcmReportType.RmRaReport.value(),
								DcmReportType.AmRsReport.value(),};
		condition.setRepCtgCds( reportId );

		return condition;

	}

	/**
	 * ビルドパッケージクローズ時の警告チェック対象の返却申請情報の検索条件を取得する。
	 * @param lotId ロット番号
	 * @return ビルドパッケージクローズ時の警告チェック対象の返却申請情報の検索条件
	 */
	public static final IJdbcCondition
		getReturnAreqConditionForWarningAtRelUnitClose( String lotId ) {

		AreqCondition condition = new AreqCondition();

		String[] baseStatusId = {	AmAreqStatusId.CheckoutRequested.getStatusId(),
									AmAreqStatusId.CheckinRequested.getStatusId(),
									AmAreqStatusId.CheckinRequestApproved.getStatusId(),
									AmAreqStatusId.BuildPackageClosed.getStatusId() };
		condition.setStsIds			( baseStatusId );
		condition.setLotId			( lotId );
		condition.setAreqCtgCds		( new String[]{ AreqCtgCd.ReturningRequest.value() } );

		return condition;
	}
	/**
	 * パッケージクローズ済みのビルドパッケージの検索条件を取得する。
	 * @param lotId ロット番号
	 * @return クローズ済みあるいはクローズ操作後ステータスのビルドパッケージの検索条件
	 */
	public static final IJdbcCondition getBuildConditionByCloseRelUnit( String lotId ) {

		BpCondition condition = new BpCondition();

		condition.setStsIds( ExtractStatusAddonUtils.getRelUnitBaseStatusByRelUnitClose() );

		if ( null != lotId ) {
			condition.setLotId( lotId );
		}

		return condition;
	}

	/**
	 * 進行中のリリース希望日の検索条件を取得する。
	 * @return 進行中のリリース環境の検索条件
	 */
	public static final CalCondition getActivelRelCalendarCondition() {

		CalCondition condition = new CalCondition();

		return condition;
	}

}
