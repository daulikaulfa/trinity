package jp.co.blueship.tri.fw.dao.orm.psql;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import jp.co.blueship.tri.fw.dao.orm.IEntityNullItem;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;

/**
 * Insert queryを組みたてるためのStringBuilderクラスです
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public class InsertBuilder implements ISqlBuilder {

	private StringBuilder intoBuilder = new StringBuilder();
	private StringBuilder valuesBuilder = new StringBuilder();
	private ITableAttribute tableAttr = null;
	private Set<String> itemSet = new LinkedHashSet<String>();
	private MapSqlParameterSource paramSource = new MapSqlParameterSource();
	private List<SqlParameterSource> paramSourceList = new ArrayList<SqlParameterSource>();

	/**
	 * Constructor for UpdateBuilder
	 * <br>
	 */
	public InsertBuilder() {
		this.setTableAttribute( null );
	}

	/**
	 * Constructor for UpdateBuilder
	 * <br>
	 * @param tableAttr table attribute
	 */
	public InsertBuilder( ITableAttribute tableAttr ) {
		this.setTableAttribute(tableAttr);
	}

	/**
	 * tableNamesを途中で切り替える場合に設定します
	 *
	 * @param tableAttr table attribute
	 */
	public void setTableAttribute( ITableAttribute tableAttr ) {
		this.tableAttr = tableAttr;
	}

	@Override
	public ITableAttribute getTableAttribute() {
		return this.tableAttr;
	}

	@Override
	public ISqlBuilder append( ITableItem item, Object value ) {
		return this.append( item, value, false );
	}

	@Override
	public ISqlBuilder append( ITableItem item, Object value, boolean isPrimaryKey ) {
		if ( null == item )
			return this;

//		if ( null == value )
//			return this;

		if ( ! itemSet.contains(item.getItemName()) ) {
			itemSet.add( item.getItemName() );

			if ( 0 != intoBuilder.length() ) {
				intoBuilder.append(" , ");
				valuesBuilder.append( " , " );
			}
			intoBuilder.append( item.getItemName() );
			valuesBuilder.append(":").append( item.getItemName() );
		}

		paramSource.addValue(item.getItemName(), (IEntityNullItem.class.isInstance(value))? null: value );

		return this;
	}

	/**
	 * Sql ParameterをbatchUpdate向けにListに追加し、現在のSql Parameterを初期化します。
	 *
	 * @return a reference to this instance, so it's possible to chain several calls together
	 */
	public InsertBuilder add() {
		paramSourceList.add( paramSource );
		paramSource = new MapSqlParameterSource();

		return this;
	}

	@Override
	public String toQueryString() {
		String into = intoBuilder.toString();
		String values = valuesBuilder.toString();

		String sql =
				" INSERT INTO " + tableAttr.name()
				+ " ( " + into + " ) "
				+ " VALUES ( " + values + " ) ";

		return sql;
	}

	/**
	 * SQL ParameterのListを取得します。
	 *
	 * @return SQL Parameterを戻します。
	 */
	public SqlParameterSource getSqlParameter() {
		return paramSource;
	}

	/**
	 * SQL ParameterのListを取得します。
	 *
	 * @return SQL ParameterのListを戻します。
	 */
	public List<SqlParameterSource> getSqlParameterList() {
		return paramSourceList;
	}
}
