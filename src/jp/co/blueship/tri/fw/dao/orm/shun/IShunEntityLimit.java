package jp.co.blueship.tri.fw.dao.orm.shun;

import jp.co.blueship.tri.fw.dao.orm.IEntity;
import jp.co.blueship.tri.fw.dao.orm.ILimit;



/**
 * ページ制御を行うテーブルのエンティティが継承する必要のある、インタフェースです。
 */
public interface IShunEntityLimit<E> extends IEntity {

	/**
	 * 検索結果のページ制御を取得します。
	 * @return 取得した値を戻します。
	 */
	public ILimit getLimit();
	/**
	 * 検索結果のページ制御を設定します。
	 * @param limit ページ制御
	 */
	public void setLimit( ILimit limit );
	/**
	 * 取得した検索結果を戻します。
     * @return 取得した検索結果を戻します。
	 */
	public E[] getEntity();
	/**
	 * 取得した検索結果を設定します。
	 * @param entities 検索結果
	 */
	public void setEntity( E[] entities  );
}
