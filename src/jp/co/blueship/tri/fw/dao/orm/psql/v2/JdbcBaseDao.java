package jp.co.blueship.tri.fw.dao.orm.psql.v2;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import jp.co.blueship.tri.fw.cmn.utils.TriObjectUtils;
import jp.co.blueship.tri.fw.dao.orm.ILimit;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.SmMessageId;

/**
 * Data Access Object(DAO)の基底抽象クラスです。
 *
 *
 * @deprecated
 */
public class JdbcBaseDao extends JdbcDaoSupport {

	private IContextAdapter context = null;

	/**
	 * ビーンを生成するコンテキストが設定されます。
	 *
	 * @param context コンテキスト
	 *
	 * @deprecated
	 */
	public final void setContext(IContextAdapter context) {
		this.context = context;
	}

	/**
	 * ビーンを生成するコンテキストを取得します。
	 *
	 * @return コンテキスト
	 *
	 * @deprecated
	 */
	protected final IContextAdapter getContext() {
		return this.context;
	}

	/**
	 * ビーン名に対応するオブジェクトのインスタンスを取得します。
	 *
	 * @param beanName 取得するビーン名
	 * @return 取得したインスタンスを戻します。
	 *
	 * @deprecated
	 */
	protected final Object getBean(String beanName) {
		if (null == this.context)
			throw new TriSystemException( SmMessageId.SM005039S );

		return context.getBean(beanName);
	}

	/**
	 * 指定されたテーブルの件数を取得します。 <br>
	 * ex.) countQuery( "return",
	 * "WHERE project_id = ?  and delete_status != '1'", new Object[] {
	 * projectId } )
	 *
	 * @param tableName テーブル名
	 * @param where WHERE句の文字列
	 * @param values ステートメント値
	 * @return 件数を取得します。
	 * @throws TriSystemException
	 *
	 * @deprecated
	 */
	public final int countQuery(String tableName, String where, Object[] values) throws TriSystemException {

		if (null == tableName) {
			throw new TriSystemException( SmMessageId.SM005040S );
		}

		Object[] queryValues = TriObjectUtils.defaultIfNull(values, new Object[] {});
		String queryWhere = TriObjectUtils.defaultIfNull(where, "");

		List<Integer> rows = //
		getJdbcTemplate().query(//
				"SELECT count(*) count FROM " + tableName + " " + queryWhere, //
				queryValues,//
				new RowMapper<Integer>() {
					public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
						return new Integer(rs.getInt("count"));
					}
				});

		return (0 == rows.size()) ? 0 : rows.get(0).intValue();
	}

	/**
	 * 指定されたテーブルの全件数を取得します。
	 *
	 * @param tableName テーブル名
	 * @param where WHERE句の文字列
	 * @param parameters ステートメントパラメタ
	 * @return 件数を取得します。
	 * @throws TriSystemException
	 *
	 * @deprecated
	 */
	public final int countQuery(String tableName) throws TriSystemException {

		if (null == tableName)
			throw new TriSystemException( SmMessageId.SM005040S );

		return countQuery(tableName, null, new Object[] {});
	}

	/**
	 * 指定されたページ位置、表示行数より、ページ制御クラスを取得します。
	 *
	 * @param pageNo ページ番号
	 * @param viewRows 表示行数
	 * @param count 対象件数
	 * @return ページ制御を戻します。取得できない場合、nullを戻します。
	 * @throws TriSystemException
	 *
	 * @deprecated
	 */
	public final ILimit getPageLimit(Integer pageNo, Integer viewRows, int count) throws TriSystemException {

		if (null == pageNo || null == viewRows)
			return null;

		ILimit limit = (ILimit) this.getBean("pageLimit");
		limit.setMaxRows(count);
		limit.setViewRows(viewRows.intValue());
		limit.getPageBar().setValue(pageNo.intValue());

		return limit;
	}

}
