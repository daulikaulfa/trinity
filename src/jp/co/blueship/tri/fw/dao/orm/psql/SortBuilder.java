package jp.co.blueship.tri.fw.dao.orm.psql;

import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;

/**
 * sort orderを組みたてるためのStringBuilderクラスです
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 */
public class SortBuilder implements ISqlSort {

	/**
	 * ソート式、ソート順を格納するマップ
	 */
	private SortedMap<Integer, String> sorts = new TreeMap<Integer, String>(
			new Comparator<Integer>() {
					public int compare(Integer param1, Integer param2) {
						return param1.compareTo(param2);
					}
			} );

	private Set<String> elementSet = new LinkedHashSet<String>();
	private int latestSeq = 0;

	/**
	 * Constructor for JdbcDaoException
	 * <br>
	 */
	public SortBuilder() {
	}

	@Override
	public ISqlSort setElement(ITableItem element, TriSortOrder sort, int seq) {
		if ( null == element )
			return this;

		if ( null == sort )
			sort = TriSortOrder.Asc;

		if ( ! elementSet.contains(element.getItemName()) ) {
			elementSet.add( element.getItemName() );

			sorts.put(seq, element.getItemName() + " " + sort.value() );
			if ( latestSeq < seq ) {
				latestSeq = seq;
			}
		}

		return this;
	}

	@Override
	public int getLatestSeq() {
		return latestSeq;
	}

	@Override
	public String toSortString() {
		if ( 0 != sorts.size() ) {
			StringBuilder buf = new StringBuilder();

			for ( String value : sorts.values() ) {
				if ( 0 < buf.length() ) {
					buf.append(" , ");
				}

				buf.append( value );
			}

			return " ORDER BY " + buf.toString();
		}

		return "";
	}
}
