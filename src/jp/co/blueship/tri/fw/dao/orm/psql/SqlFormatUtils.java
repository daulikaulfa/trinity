package jp.co.blueship.tri.fw.dao.orm.psql;

import java.lang.reflect.InvocationTargetException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;

import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.msg.UmMessageId;

/**
 * SQL Queryの文字列処理を行うユーティリティークラスです。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V3L11.01
 * @author Yukihiro Eguchi
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class SqlFormatUtils {

	private SqlFormatUtils() {};

	/**
	 * booleanで定義されたオブジェクトから指定された値に該当する文字列のリストを取得します。
	 * @param pinpoint 抽出する項目をbooleanで定義されたsetter/getterクラス
	 * @param columns 抽出項目の文字列配列
	 * @param value 抽出対象の値
	 * @return 取得した文字列リストを戻します。
	 * @throws TriSystemException
	 *
	 * @deprecated
	 */
	public static final List<String> getProperty( Object pinpoint, String[] columns, boolean value )
			throws TriSystemException {

		if ( null == pinpoint || null == columns ){
			throw new TriSystemException( SmMessageId.SM005035S );
		}
		List<String> lists = new ArrayList<String>();

		try {
			for ( int i = 0; i < columns.length; i++ ) {
				boolean isUpdate = ((Boolean)PropertyUtils.getProperty(pinpoint, columns[i])).booleanValue();
				if ( value != isUpdate )
					continue;

				lists.add( columns[i] );
			}

			return lists;

		} catch (IllegalAccessException e) {
			throw new TriSystemException( SmMessageId.SM005036S , e , "IllegalAccessError" );
		} catch (InvocationTargetException e) {
			throw new TriSystemException( SmMessageId.SM005036S , e , "InvocationTargetError" );
		} catch (NoSuchMethodException e) {
			throw new TriSystemException( SmMessageId.SM005036S , e , "NoSuchMethodError" );
		} finally {
		}
	}

	/**
	 * booleanで定義されたオブジェクトに指定した値を設定します。
	 * @param pinpoint 設定する項目をbooleanで定義されたsetter/getterクラス
	 * @param columns
	 * @param value 設定対象の値
	 * @return 設定したpinpointを戻します。
	 * @throws TriSystemException
	 *
	 * @deprecated
	 */
	public static final Object setProperty( Object pinpoint, String[] columns, boolean value )
			throws TriSystemException {

		if ( null == pinpoint || null == columns ){
			throw new TriSystemException( SmMessageId.SM005035S );
		}

		try {
			for ( int i = 0; i < columns.length; i++ ) {
				PropertyUtils.setProperty( pinpoint, columns[i], new Boolean( value ) );
			}

			return pinpoint;

		} catch (IllegalAccessException e) {
			throw new TriSystemException( SmMessageId.SM005037S , e , "IllegalAccessError" );
		} catch (InvocationTargetException e) {
			throw new TriSystemException( SmMessageId.SM005037S , e , "InvocationTargetError" );
		} catch (NoSuchMethodException e) {
			throw new TriSystemException( SmMessageId.SM005037S , e , "NoSuchMethodError" );
		} finally {
		}
	}

	/**
	 * 指定されたoffset位置（デフォルトは0）以降の行を抽出するSQLのLIMIT文を取得します。
	 * @param offsetRow offset位置
	 * @param findRows 検索件数
	 * @return SQLのLIMIT文を戻します。
	 */
	public static final String getSqlLimit( Integer offsetRow, Integer findRows ) {
		int offset = ( null == offsetRow )? 0: offsetRow.intValue();
		int rows = ( null == findRows )? 0: findRows.intValue();

		return getSqlLimit( offset, rows );
	}

	/**
	 * 指定されたoffset位置（デフォルトは0）以降の行を抽出するSQLのLIMIT文を取得します。
	 * @param offsetRow offset位置
	 * @param findRows 検索件数
	 * @return SQLのLIMIT文を戻します。
	 */
	public static final String getSqlLimit( int offsetRow, int findRows ) {

		return " OFFSET " + offsetRow + " LIMIT " + findRows;
	}

	/**
	 * from-toの値から、検索式を取得します。
	 *
	 * @param from 検索条件のFrom値
	 * @param to 検索条件のTo値
	 * @param element テーブルの要素名
	 * @return 検索式を戻します。
	 */
	public static final String getFromTo( Timestamp from, Timestamp to, String element ) {
		//XXX Timezoneおよび、時分秒への配慮が足りない
		String strFrom = null;
		String strTo = null;
		if( from!=null ) {
			strFrom = from.toString();
		}
		if( to!=null ) {
			strTo = to.toString();
		}
		return getFromTo( strFrom, strTo, element );
	}

	/**
	 * from-toの値から、検索式を取得します。
	 *
	 * @param from 検索条件のFrom値
	 * @param to 検索条件のTo値
	 * @param element テーブルの要素名
	 * @return 検索式を戻します。
	 */
	public static final String getFromTo( String from, String to, String element ) {
		return getFromTo( from, to, element, false );
	}

	/**
	 * from-toの値から、検索式を取得します。
	 *
	 * @param from 検索条件のFrom値
	 * @param to 検索条件のTo値
	 * @param element テーブルの要素名
	 * @param toCharFunction TO_CHAR関数に変換する場合true.
	 * @return 検索式を戻します。
	 */
	public static final String getFromTo( String from, String to, String element, boolean toCharFunction ) {
		StringBuilder buf = new StringBuilder();

		if ( TriStringUtils.isEmpty(element) )
			return null;

		String targetElement = (toCharFunction)? convertToCharFunction( element ) : element;

		from = TriStringUtils.defaultIfEmpty(from, null, from);
		to = TriStringUtils.defaultIfEmpty(to, null, to);

		if ( null == from && null == to ) {
			return null;
		}

		buf.append( " (" );

		if ( null == from && null != to ) {
			buf.append( targetElement + " <= '" + to + "'" );
		}
		if ( null != from && null == to ) {
			buf.append( targetElement + " >= '" + from + "'" );
		}
		if ( null != from && null != to ) {
			buf.append( targetElement + " >= '" + from + "' AND " );
			buf.append( targetElement + " <= '" + to + "'" );
		}

		buf.append( ")" );

		return buf.toString();
	}

	/**
	 * Converts the elements of the table to TO_CHAR function.
	 * <br>
	 * <br>テーブルの要素をTO_CHAR関数のついた要素に変換します。
	 *
	 * @param element table element
	 * @return
	 */
	private static final String convertToCharFunction( String element ) {
		PreConditions.assertOf(element != null, "element is not specified");

		return "TO_CHAR(" + element + ", 'YYYY/MM/DD')";
	}

	/**
	 * from-toの値から、検索式を取得します。
	 *
	 * @param from 検索条件のFrom値
	 * @param to 検索条件のTo値
	 * @param elementSt テーブルの要素名(Start)
	 * @param elementEnd テーブルの要素名(End)
	 * @return 検索式を戻します。
	 */
	public static final String getFromTo( String from, String to, ITableItem elementSt, ITableItem elementEnd ) {
		StringBuilder buf = new StringBuilder();

		from = TriStringUtils.defaultIfEmpty(from, null, from);
		to = TriStringUtils.defaultIfEmpty(to, null, to);

		if ( null == from && null == to ) {
			return null;
		}

		buf.append( " (" );

		if ( null == from && null != to ) {
			buf.append( elementSt.getItemName() + " <= '" + to + "'" );
		}
		if ( null != from && null == to ) {
			buf.append( "'" + from + "' <= " + elementEnd.getItemName() );
		}
		if ( null != from && null != to ) {
			if ( !TriDateUtils.before(from, to) ) {
				List<IMessageId> messageList = new ArrayList<IMessageId>();
				List<String[]>	messageArgsList	= new ArrayList<String[]>();
				messageList.add		( UmMessageId.UM001063E );
				messageArgsList.add	( new String[] {} );
				throw new ContinuableBusinessException( messageList, messageArgsList );
			}
			buf.append( elementSt.getItemName() + " <= '" + to + "' AND '" );
			buf.append( from + "' <= " + elementEnd.getItemName() );
		}

		buf.append( ")" );

		return buf.toString();
	}

	/**
	 * 検索式を取得します。（完全一致）
	 * <br>空白で区切られた条件の場合、それを複数の検索条件としてOR検索を行います。
	 * これにより、特定項目の絞込み検索を行います。
	 *
	 * @param conditionValue OR検索条件
	 * @param element テーブルの要素名
	 * @return 検索式を戻します。
	 */
	public static final String getCondition( String conditionValue, String element ) {
		return getCondition( conditionValue, element, false, false );
	}

	/**
	 * 検索式を取得します。（完全不一致を対象）
	 * <br>空白で区切られた条件の場合、それを複数の検索条件としてOR検索を行います。
	 * これにより、特定項目の絞込み検索を行います。
	 *
	 * @param conditionValue OR検索条件
	 * @param element テーブルの要素名
	 * @return 検索式を戻します。
	 */
	public static final String getConditionByNotEquals( String conditionValue, String element ) {
		return getCondition( conditionValue, element, false, true );
	}

	/**
	 * 検索式を取得します。
	 * <br>空白で区切られた条件の場合、それを複数の検索条件としてOR検索を行います。
	 * これにより、特定項目の絞込み検索を行います。
	 *
	 * @param conditionValue OR検索条件
	 * @param element テーブルの要素名
	 * @param isLike 部分検索の場合、true
	 * @return 検索式を戻します。
	 */
	public static final String getCondition( 	String conditionValue,
												String element,
												boolean isLike ) {
		return getCondition( conditionValue, element, isLike, false );
	}

	/**
	 * 検索式を取得します。
	 * <br>空白で区切られた条件の場合、それを複数の検索条件としてOR検索を行います。
	 * これにより、特定項目の絞込み検索を行います。
	 *
	 * @param conditionValue OR検索条件
	 * @param element テーブルの要素名
	 * @param isLike 部分検索の場合、true
	 * @param isNotEquals 一致しない条件を対象とする場合、true。それ以外はfalseを設定
	 * @return 検索式を戻します。
	 */
	public static final String getCondition( 	String conditionValue,
												String element,
												boolean isLike,
												boolean isNotEquals ) {

		return getCondition(conditionValue, element, isLike, isNotEquals, false, false);
	}

	/**
	 * 検索式を取得します。
	 * <br>空白で区切られた条件の場合、それを複数の検索条件としてOR検索を行います。
	 * これにより、特定項目の絞込み検索を行います。
	 *
	 * @param conditionValue OR検索条件
	 * @param element テーブルの要素名
	 * @param isLike 部分検索の場合、true
	 * @param isNotEquals 一致しない条件を対象とする場合、true。それ以外はfalseを設定
	 * @param isPrefix 部分検索の場合に前方一致させる場合、true。
	 * @param isSuffix 部分検索の場合に後方一致させる場合、true。
	 * @return 検索式を戻します。
	 */
	public static final String getCondition( 	String conditionValue,
												String element,
												boolean isLike,
												boolean isNotEquals,
												boolean isPrefix,
												boolean isSuffix) {

		conditionValue = TriStringUtils.defaultIfEmpty( conditionValue, null, conditionValue );

		if ( null == conditionValue ) {
			return null;
		}

		String newConditionValue = conditionValue;

		String[] splits = StringUtils.split(conditionValue, " ");
		if ( 1 < splits.length ) {
			return getCondition( splits, element, isLike, true, isNotEquals );
		}

		newConditionValue = ( 0 == splits.length )? "" : splits[0];

		String prefix = "'";
		String suffix = "' ";

		if ( isLike ) {
			prefix = (isPrefix)? "'": "'%";
			suffix = (isSuffix)? "' ": "%' ";
		}

		return " " + element + ((isLike)? (isNotEquals)? " NOT LIKE ":" LIKE ": (isNotEquals)? " != ":" = ") + prefix + newConditionValue + suffix;
	}

	/**
	 * 複数ORの値から、検索式を取得します。
	 *
	 * @param conditionValues AND検索条件
	 * @param element テーブルの要素名
	 * @return 検索式を戻します。
	 */
	public static final String getOr( String[] conditionValues, String element ) {
		return getCondition( conditionValues, element, false, true, false );
	}

	/**
	 * 複数ORの値から、検索式を取得します。
	 *
	 * @param conditionValues AND検索条件
	 * @param element テーブルの要素名
	 * @param isLike 部分検索の場合、true
	 * @return 検索式を戻します。
	 */
	public static final String getOr( String[] conditionValues, String element, boolean isLike ) {
		return getCondition( conditionValues, element, isLike, true, false );
	}

	/**
	 * 複数ANDの値から、検索式を取得します。
	 *
	 * @param conditionValues OR検索条件
	 * @param element テーブルの要素名
	 * @return 検索式を戻します。
	 */
	public static final String getAnd( String[] conditionValues, String element ) {
		return getCondition( conditionValues, element, false, false, false );
	}

	/**
	 * 複数ANDの値から、検索式を取得します。（不一致を対象）
	 *
	 * @param conditionValues OR検索条件
	 * @param element テーブルの要素名
	 * @return 検索式を戻します。
	 */
	public static final String getAndByNotEquals( String[] conditionValues, String element ) {
		return getCondition( conditionValues, element, false, false, true );
	}

	/**
	 * 条件の値から、検索式を取得します。
	 *
	 * @param conditionValue OR検索条件
	 * @param element テーブルの要素名
	 * @param isLike 部分検索の場合、true
	 * @param isOr 複数条件をOR連結する場合は、true。それ以外(and)はfalseを設定
	 * @param isNotEquals 一致しない条件を対象とする場合、true。それ以外はfalseを設定
	 * @return 検索式を戻します。
	 */
	public static final String getCondition( 	String conditionValue,
												String element,
												boolean isLike,
												boolean isOr,
												boolean isNotEquals) {

		String[] values = new String[0];

		if ( TriStringUtils.isNotEmpty(conditionValue) ) {
			values = StringUtils.split(conditionValue, " ");
		}

		return getCondition(values, element, isLike, isOr, isNotEquals, false, false);
	}

	/**
	 * 複数条件の値から、検索式を取得します。
	 *
	 * @param conditionValues OR検索条件
	 * @param element テーブルの要素名
	 * @param isLike 部分検索の場合、true
	 * @param isOr 複数条件をOR連結する場合は、true。それ以外(and)はfalseを設定
	 * @param isNotEquals 一致しない条件を対象とする場合、true。それ以外はfalseを設定
	 * @return 検索式を戻します。
	 */
	public static final String getCondition( 	String[] conditionValues,
												String element,
												boolean isLike,
												boolean isOr,
												boolean isNotEquals) {
		return getCondition(conditionValues, element, isLike, isOr, isNotEquals, false, false);
	}

	/**
	 * 複数条件の値から、検索式を取得します。
	 *
	 * @param conditionValues OR検索条件
	 * @param element テーブルの要素名
	 * @param isLike 部分検索の場合、true
	 * @param isOr 複数条件をOR連結する場合は、true。それ以外(and)はfalseを設定
	 * @param isNotEquals 一致しない条件を対象とする場合、true。それ以外はfalseを設定
	 * @param isPrefix 部分検索の場合に前方一致させる場合、true。
	 * @param isSuffix 部分検索の場合に後方一致させる場合、true。
	 * @return 検索式を戻します。
	 */
	public static final String getCondition( 	String[] conditionValues,
												String element,
												boolean isLike,
												boolean isOr,
												boolean isNotEquals,
												boolean isPrefix,
												boolean isSuffix) {
		StringBuilder buf = new StringBuilder();

		if ( null == conditionValues || conditionValues.length == 0 ) {
			return null;
		}

		String[] newConditionValues = new String[conditionValues.length];

		buf.append( " (" );

		for ( int i =0; i < conditionValues.length; i++ ) {
			if ( i != 0 ) {
				if ( isOr )
					buf.append(" OR ");
				else
					buf.append(" AND ");
			}

			String prefix = "'";
			String suffix = "' ";

			if ( isLike ) {
				prefix = (isPrefix)? "'": "'%";
				suffix = (isSuffix)? "' ": "%' ";
			}

			newConditionValues[i] = conditionValues[i].replaceAll("'", "''");
			buf.append( element + ((isLike)? ((isNotEquals)? " NOT LIKE ":" LIKE "): (isNotEquals)? " != ":" = ") + prefix + newConditionValues[i] + suffix );
		}

		buf.append( ") " );

		return buf.toString();
	}

	/**
	 * 複数条件の値から、検索式を取得します。
	 *
	 * @param conditionValues OR検索条件
	 * @param element テーブルの要素名
	 * @param isOr 複数条件をOR連結する場合は、true。それ以外(and)はfalseを設定
	 * @param isNotEquals 一致しない条件を対象とする場合、true。それ以外はfalseを設定
	 * @return 検索式を戻します。
	 */
	public static final String getCondition( 	StatusFlg[] conditionValues,
												String element,
												boolean isOr,
												boolean isNotEquals) {
		StringBuilder buf = new StringBuilder();

		if ( null == conditionValues || conditionValues.length == 0 ) {
			return null;
		}

		String[] newConditionValues = new String[conditionValues.length];

		buf.append( " (" );

		for ( int i =0; i < conditionValues.length; i++ ) {
			if ( i != 0 ) {
				if ( isOr )
					buf.append(" OR ");
				else
					buf.append(" AND ");
			}

			newConditionValues[i] = conditionValues[i].parseBoolean().toString();
			buf.append( element + ((isNotEquals)? " != ":" = ") + "'" + newConditionValues[i] + "'" );
		}

		buf.append( ") " );

		return buf.toString();
	}

	/**
	 * 条件式を指定された条件で結合します。
	 *
	 * @param isOr 複数条件をOR連結する場合は、true。それ以外(and)はfalseを設定
	 * @param conditions 結合する条件式
	 * @return 検索式を戻します。
	 */
	public static String joinCondition( boolean isOr, String... conditions ) {
		List<String> list = new ArrayList<String>();

		for ( String condition: conditions ) {
			if ( TriStringUtils.isEmpty(condition) ) {
				continue;
			}
			list.add( condition );
		}

		if ( 0 == list.size() ) {
			return null;
		}

		String separator;

		if ( isOr )
			separator = (" OR ");
		else
			separator = (" AND ");

		StringBuilder buf = new StringBuilder();
		buf.append( " (" );
		buf.append( StringUtils.join(list, separator) );
		buf.append( ") " );

		return buf.toString();
	}

	/**
	 * ソート条件Queryを取得します。
	 *
	 * @param sort ソート条件
	 * @return ソート条件のQueryを戻します。
	 */
	public static String toSortString(ISqlSort sort) {
		return (null == sort)? "": sort.toSortString();
	}

}
