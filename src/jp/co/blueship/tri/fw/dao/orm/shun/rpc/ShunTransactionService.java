package jp.co.blueship.tri.fw.dao.orm.shun.rpc;

import jp.co.blueship.tri.fw.dao.orm.ILimit;
import jp.co.blueship.tri.fw.dao.orm.shun.ShunMapper;
import jp.co.blueship.tri.fw.dao.orm.shun.rpc.dto.ShunRemoteList;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.SmMessageId;


/**
 * Shunsakuリモートアクセスでトランザクション制御を利用するための汎用クラスです。
 * <br>この実装クラスは、必ずsingleton = falseで実行します。
 *
 */
public class ShunTransactionService implements IShunTemplateRemote {

	private IContextAdapter context = null;
	private String nextTransactionService = null;

	/**
	 * ビーンを生成するコンテキストを取得します。
	 * @return 取得したインスタンスを戻します。
	 */
	private final IContextAdapter getContext() {
		if ( null == this.context )
			this.context = ContextAdapterFactory.getContextAdapter();

		return context;
	}

	/**
	 * 一度のリクエスト処理で、複数のShunsakuコネクション、またはRDBを同時に
	 * トランザクション制御を行う場合、指定されます。
	 *
	 * @param transactionService 次のトランザクション制御処理があれば、指定する
	 */
	public final void setNextTargetService( String transactionService ) {
		this.nextTransactionService = transactionService;
	}

	/**
	 * @return 取得したトランザクション制御処理を戻します。
	 */
	private final IShunTemplateRemote getShunTemplateRemote() {

		if ( null != this.nextTransactionService ) {
			Object obj = this.getContext().getBean( nextTransactionService );

			if ( obj instanceof IShunTemplateRemote ) {
				return (IShunTemplateRemote)obj;
			}
		}
		throw new TriSystemException( SmMessageId.SM005046S );
	}

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.db.shunsaku.rpc.IShunTemplateRemote#countQuery(java.lang.String, java.lang.String)
	 */
	public final Integer countQuery(String datasource, String queryExp) throws Exception {
		return this.getShunTemplateRemote().countQuery(datasource, queryExp);
 	}

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.db.shunsaku.rpc.IShunTemplateRemote#countQuery(java.lang.String, java.lang.String, boolean)
	 */
	public final Integer countQuery(String datasource, String queryExp, boolean isSort) throws Exception {
		return this.getShunTemplateRemote().countQuery(datasource, queryExp, isSort);
 	}

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.db.shunsaku.rpc.IShunTemplateRemote#countQuery(java.lang.String, java.lang.String, int, boolean)
	 */
	public Integer countQuery(String datasource, String queryExp, int maxHitLimit, boolean isSort) throws Exception {
		return this.getShunTemplateRemote().countQuery(datasource, queryExp, maxHitLimit, isSort);
	}

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.db.shunsaku.rpc.IShunTemplateRemote#delete(java.lang.String, java.lang.String)
	 */
	public final Integer delete(String datasource, String queryExp) throws Exception {
		return this.getShunTemplateRemote().delete(datasource, queryExp);

	}

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.db.shunsaku.rpc.IShunTemplateRemote#deleteByKey(java.lang.String, java.lang.String, java.lang.String[])
	 */
	public final Integer deleteByKey(String datasource, String key, String[] keyValues) throws Exception {
		return this.getShunTemplateRemote().deleteByKey(datasource, key, keyValues);

	}

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.db.shunsaku.rpc.IShunTemplateRemote#find(java.lang.String, java.lang.String, java.lang.String, jp.co.blueship.trinity.common.db.shunsaku.core.ShunMapper)
	 */
	public final ShunRemoteList<?> find(String datasource, String queryExp, String returnExp, ShunMapper map) throws Exception {
		return this.getShunTemplateRemote().find(datasource, queryExp, returnExp, map);

	}

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.db.shunsaku.rpc.IShunTemplateRemote#find(java.lang.String, java.lang.String, java.lang.String, jp.co.blueship.trinity.common.db.ILimit, jp.co.blueship.trinity.common.db.shunsaku.core.ShunMapper)
	 */
	public final ShunRemoteList<?> find(String datasource, String queryExp, String returnExp, ILimit limit, ShunMapper map) throws Exception {
		return this.getShunTemplateRemote().find(datasource, queryExp, returnExp, limit, map);

	}

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.db.shunsaku.rpc.IShunTemplateRemote#find(java.lang.String, java.lang.String, java.lang.String, java.lang.String, jp.co.blueship.trinity.common.db.shunsaku.core.ShunMapper)
	 */
	public final ShunRemoteList<?> find(String datasource, String queryExp, String returnExp, String sortExp, ShunMapper map) throws Exception {
		return this.getShunTemplateRemote().find(datasource, queryExp, returnExp, sortExp, map);

	}

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.db.shunsaku.rpc.IShunTemplateRemote#find(java.lang.String, java.lang.String, java.lang.String, java.lang.String, jp.co.blueship.trinity.common.db.ILimit, jp.co.blueship.trinity.common.db.shunsaku.core.ShunMapper)
	 */
	public final ShunRemoteList<?> find(String datasource, String queryExp, String returnExp, String sortExp, ILimit limit, ShunMapper map) throws Exception {
		return this.getShunTemplateRemote().find(datasource, queryExp, returnExp, sortExp, limit, map);

	}

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.db.shunsaku.rpc.IShunTemplateRemote#findByKey(java.lang.String, java.lang.String, jp.co.blueship.trinity.common.db.shunsaku.rpc.message.ShunRemoteList, java.lang.String, jp.co.blueship.trinity.common.db.shunsaku.core.ShunMapper)
	 */
	public final ShunRemoteList<?> findByKey(String datasource, String key, ShunRemoteList<String[]> keyValues, String returnExp, ShunMapper map) throws Exception {
		return this.getShunTemplateRemote().findByKey(datasource, key, keyValues, returnExp, map);

	}

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.db.shunsaku.rpc.IShunTemplateRemote#insert(java.lang.String, java.lang.String)
	 */
	public final Integer insert(String datasource, String data) throws Exception {
		return this.getShunTemplateRemote().insert(datasource, data);

	}

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.db.shunsaku.rpc.IShunTemplateRemote#update(java.lang.String, java.lang.String, java.lang.String)
	 */
	public final Integer update(String datasource, String queryExp, String data) throws Exception {
		return this.getShunTemplateRemote().update(datasource, queryExp, data);

	}

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.db.shunsaku.rpc.IShunTemplateRemote#updateByKey(java.lang.String, java.lang.String, java.lang.String[], java.lang.String)
	 */
	public final Integer updateByKey(String datasource, String key, String[] keyValues, String data) throws Exception {
		return this.getShunTemplateRemote().updateByKey(datasource, key, keyValues, data);

	}

}
