package jp.co.blueship.tri.fw.dao.orm;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotRelEnvLnkEntity;
import jp.co.blueship.tri.bm.beans.dto.RelUnitSearchBean;
import jp.co.blueship.tri.dcm.beans.dto.ReportSearchBean;
import jp.co.blueship.tri.fw.cmn.utils.DesignDefineSearchUtils;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.ViewInfoAddonUtil;
import jp.co.blueship.tri.fw.constants.BmDesignBeanId;
import jp.co.blueship.tri.fw.constants.DcmDesignBeanId;
import jp.co.blueship.tri.fw.constants.RmDesignBeanId;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.svc.beans.ItemLabelsUtils;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.rm.beans.dto.RelApplySearchBean;
import jp.co.blueship.tri.rm.beans.dto.RelCtlSearchBean;
import jp.co.blueship.tri.rm.domain.ra.beans.dto.RelApplyPackageListSearchBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.BaseInfoRelApplySearchBean;

public class DBSearchBeanAddonUtil {
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	/**
	 * ビルドパッケージレコードの詳細検索条件を生成する
	 * @param searchBean
	 * @param closeListCount
	 * @return
	 */
	public static final RelUnitSearchBean setRelUnitSearchBean(
													RelUnitSearchBean searchBean,
													BmDesignBeanId closeListCount ) {

		RelUnitSearchBean retRelUnitSearchBean = new RelUnitSearchBean() ;

		if ( null != searchBean ) {
			retRelUnitSearchBean.setSearchRelUnitCountList	( new ArrayList<ItemLabelsBean>() );
			retRelUnitSearchBean.setSelectedRelUnitCount	( searchBean.getSelectedRelUnitCount() );
			retRelUnitSearchBean.setSearchBuildNo			( searchBean.getSearchBuildNo() );
			retRelUnitSearchBean.setSearchBuildName			( searchBean.getSearchBuildName() );
			retRelUnitSearchBean.setSearchRelNo				( searchBean.getSearchRelNo() );
			retRelUnitSearchBean.setSearchPjtNo				( searchBean.getSearchPjtNo() );
			retRelUnitSearchBean.setSearchChangeCauseNo		( searchBean.getSearchChangeCauseNo() );
		}

		//検索件数のリストアップ
		List<ItemLabelsBean> countList = DesignDefineSearchUtils.mappingItemLabelsBeanByKeyParseInt( closeListCount );
		retRelUnitSearchBean.setSearchRelUnitCountList( countList ) ;

		//全体検索件数の選択
		if( null == searchBean || null == searchBean.getSelectedRelUnitCount() ) {
			//初期表示時のみ
			Map<String,String> srcIdMap = sheet.getKeyMap( closeListCount );

			List<String> selectedCount = DesignDefineSearchUtils.extractKey( closeListCount, StatusFlg.on.value() ) ;
			retRelUnitSearchBean.setSelectedRelUnitCount( ( 0 == selectedCount.size() )? srcIdMap.keySet().iterator().next(): selectedCount.get( 0 ) ) ;
		}

		return retRelUnitSearchBean ;
	}

	/**
	 * リリース申請レコードの詳細検索条件を生成する
	 * @param paramBean RelApplySearchBeanオブジェクト
	 * @return RelApplySearchBeanオブジェクト
	 */
	public static final RelApplySearchBean setRelApplySearchBean(
			RelApplySearchBean searchBean,
			RmDesignBeanId topListCount) {

		RelApplySearchBean retRelApplySearchBean = new RelApplySearchBean() ;

		if ( null != searchBean ) {
			retRelApplySearchBean.setSearchLotNoList	( new ArrayList<ItemLabelsBean>() );
			retRelApplySearchBean.setSearchLotNoValueList	( new ArrayList<String>() );
			retRelApplySearchBean.setSelectedLotNo	( searchBean.getSelectedLotNo() );
			retRelApplySearchBean.setSelectedServerNo	( searchBean.getSelectedServerNo() );
			retRelApplySearchBean.setSearchRelEnvNoList	( new ArrayList<ItemLabelsBean>() );
			retRelApplySearchBean.setSearchRelEnvNoValueList	( new ArrayList<String>() );
			retRelApplySearchBean.setSelectedRelEnvNo	( searchBean.getSelectedRelEnvNo() );
			retRelApplySearchBean.setSearchRelWishDateList	( new ArrayList<ItemLabelsBean>() );
			retRelApplySearchBean.setSearchRelWishDateValueList	( new ArrayList<String>() );
			retRelApplySearchBean.setSelectedRelWishDate	( searchBean.getSelectedRelWishDate() );
			retRelApplySearchBean.setSearchRelApplyNo	( searchBean.getSearchRelApplyNo() );
			retRelApplySearchBean.setSearchBuildNo	( searchBean.getSearchBuildNo() );
			retRelApplySearchBean.setSearchRelNo	( searchBean.getSearchRelNo() );
			retRelApplySearchBean.setSearchSummary	( searchBean.getSearchSummary() );
			retRelApplySearchBean.setSearchContent	( searchBean.getSearchContent() );
			retRelApplySearchBean.setSearchPjtNo	( searchBean.getSearchPjtNo() );
			retRelApplySearchBean.setSearchChangeCauseNo	( searchBean.getSearchChangeCauseNo() );
			retRelApplySearchBean.setSearchRelationNo	( searchBean.getSearchRelationNo() );
			retRelApplySearchBean.setSearchRelApplyCountList	( searchBean.getSearchRelApplyCountList() );
			retRelApplySearchBean.setSelectedRelApplyCount	( searchBean.getSelectedRelApplyCount() );
		}

		//検索件数のリストアップ
		List<ItemLabelsBean> countList = DesignDefineSearchUtils.mappingItemLabelsBeanByKeyParseInt( topListCount );
		retRelApplySearchBean.setSearchRelApplyCountList( countList ) ;

		//全体検索件数の選択
		if( null == searchBean || null == searchBean.getSelectedRelApplyCount() ) {
			//初期表示時のみ
			Map<String,String> srcIdMap = sheet.getKeyMap( topListCount );

			List<String> selectedCount = DesignDefineSearchUtils.extractKey( topListCount, StatusFlg.on.value() ) ;
			retRelApplySearchBean.setSelectedRelApplyCount( ( 0 == selectedCount.size() )? srcIdMap.keySet().iterator().next(): selectedCount.get( 0 ) ) ;
		}

		return retRelApplySearchBean ;
	}

	/**
	 * リリース申請レコードの詳細検索条件を生成する
	 * @param paramBean RelApplySearchBeanオブジェクト
	 * @param listCount 絞込み検索件数のカスタマイズ定義
	 * @return RelApplySearchBeanオブジェクト
	 */
	public static final BaseInfoRelApplySearchBean setRelApplySearchBean(
			BaseInfoRelApplySearchBean searchBean,
			RmDesignBeanId listCount) {

		BaseInfoRelApplySearchBean retRelApplySearchBean = new BaseInfoRelApplySearchBean() ;

		if ( null != searchBean ) {
			retRelApplySearchBean.setSearchLotNoList			( new ArrayList<ItemLabelsBean>() );
			retRelApplySearchBean.setSearchLotNoValueList		( new ArrayList<String>() );
			retRelApplySearchBean.setSelectedLotNo				( searchBean.getSelectedLotNo() );
			retRelApplySearchBean.setSelectedServerNo			( searchBean.getSelectedServerNo() );
			retRelApplySearchBean.setSearchRelEnvNoList			( new ArrayList<ItemLabelsBean>() );
			retRelApplySearchBean.setSearchRelEnvNoValueList	( new ArrayList<String>() );
			retRelApplySearchBean.setSelectedRelEnvNo			( searchBean.getSelectedRelEnvNo() );
			retRelApplySearchBean.setSearchRelWishDateList		( new ArrayList<ItemLabelsBean>() );
			retRelApplySearchBean.setSearchRelWishDateValueList	( new ArrayList<String>() );
			retRelApplySearchBean.setSelectedRelWishDate		( searchBean.getSelectedRelWishDate() );
			retRelApplySearchBean.setSearchRelApplyCountList	( searchBean.getSearchRelApplyCountList() );
			retRelApplySearchBean.setSelectedRelApplyCount		( searchBean.getSelectedRelApplyCount() );
		}

		//検索件数のリストアップ
		List<ItemLabelsBean> countList = DesignDefineSearchUtils.mappingItemLabelsBeanByKeyParseInt( listCount );
		retRelApplySearchBean.setSearchRelApplyCountList( countList ) ;

		//全体検索件数の選択
		if( null == searchBean || null == searchBean.getSelectedRelApplyCount() ) {
			//初期表示時のみ
			Map<String,String> srcIdMap = sheet.getKeyMap( listCount );

			List<String> selectedCount = DesignDefineSearchUtils.extractKey( listCount, StatusFlg.on.value() ) ;
			retRelApplySearchBean.setSelectedRelApplyCount( ( 0 == selectedCount.size() )? srcIdMap.keySet().iterator().next(): selectedCount.get( 0 ) ) ;
		}

		return retRelApplySearchBean ;
	}

	/**
	 * ビルドパッケージレコードの詳細検索条件を生成する
	 * @param paramBean RelApplySearchBeanオブジェクト
	 * @return RelApplySearchBeanオブジェクト
	 */
	public static final RelApplyPackageListSearchBean setRelUnitSearchBeanByRelApplyEntry(
			RelApplyPackageListSearchBean searchBean) {

		RelApplyPackageListSearchBean retRelApplySearchBean = new RelApplyPackageListSearchBean() ;

		if ( null != searchBean ) {
			retRelApplySearchBean.setSelectedLotNo	( searchBean.getSelectedLotNo() );
			retRelApplySearchBean.setSelectedServerNo	( searchBean.getSelectedServerNo() );
			retRelApplySearchBean.setSelectedRelEnvNo	( searchBean.getSelectedRelEnvNo() );
			retRelApplySearchBean.setSelectedRelWishDate	( searchBean.getSelectedRelWishDate() );
			retRelApplySearchBean.setSelectedRelUnitCount	( searchBean.getSelectedRelUnitCount() );
			retRelApplySearchBean.setSelectedBuildNo	( searchBean.getSelectedBuildNo() );
			retRelApplySearchBean.setSelectedGroupId	( searchBean.getSelectedGroupId() );
		}

		return retRelApplySearchBean ;
	}

	/**
	 * リリースレコードの詳細検索条件を生成する
	 * @param searchBean
	 * @param closeListCount
	 * @param pjtLotDtoArray
	 * @param groupUserEntityArray
	 * @param statusId
	 * @return
	 */
	public static final RelCtlSearchBean setRelCtlSearchBean(
												RelCtlSearchBean searchBean,
												RmDesignBeanId closeListCount ,
												List<ILotDto> pjtLotDtoArray ,
												List<IGrpUserLnkEntity> groupUserEntityArray ,
												RmDesignBeanId statusId,
												String srvId) {

		RelCtlSearchBean retRelCtlSearchBean = new RelCtlSearchBean() ;

		if ( null != searchBean ) {
			retRelCtlSearchBean.setSearchReleaseCountList	( new ArrayList<ItemLabelsBean>() );
			retRelCtlSearchBean.setSelectedReleaseCount		( searchBean.getSelectedReleaseCount() );

			retRelCtlSearchBean.setSelectedLotNo			( searchBean.getSelectedLotNo() );
			retRelCtlSearchBean.setSelectedServerNo			( searchBean.getSelectedServerNo() );
			retRelCtlSearchBean.setSearchRelNo				( searchBean.getSearchRelNo() );
			retRelCtlSearchBean.setSearchBuildNo			( searchBean.getSearchBuildNo() );

			retRelCtlSearchBean.setSearchSummary			( searchBean.getSearchSummary() );
			retRelCtlSearchBean.setSearchContent			( searchBean.getSearchContent() );

			retRelCtlSearchBean.setSearchPjtNo				( searchBean.getSearchPjtNo() );
			retRelCtlSearchBean.setSearchChangeCauseNo		( searchBean.getSearchChangeCauseNo() );

			//retReleaseSearchBean.setSelectedStatus			( releaseSearchBean.getSelectedStatus() );
			//retReleaseSearchBean.setSelectedDistributeStatus( releaseSearchBean.getSelectedDistributeStatus() );
		}

		//検索件数のリストアップ
		List<ItemLabelsBean> countList = DesignDefineSearchUtils.mappingItemLabelsBeanByKeyParseInt( closeListCount );
		retRelCtlSearchBean.setSearchReleaseCountList( countList ) ;

		//全体検索件数の選択
		if( null == searchBean || null == searchBean.getSelectedReleaseCount() ) {
			//初期表示時のみ
			Map<String,String> srcIdMap = sheet.getKeyMap( closeListCount );

			List<String> selectedCount = DesignDefineSearchUtils.extractKey( closeListCount, StatusFlg.on.value() ) ;
			retRelCtlSearchBean.setSelectedReleaseCount( ( 0 == selectedCount.size() )? srcIdMap.keySet().iterator().next(): selectedCount.get( 0 ) ) ;
		}


		//ロットのリストアップ
		List<ILotEntity> lotList = new ArrayList<ILotEntity>();

		ViewInfoAddonUtil.setAccessablePjtLotEntity( pjtLotDtoArray, groupUserEntityArray, lotList, null );

		List<ItemLabelsBean> labelList = ItemLabelsUtils.conv(lotList, srvId ) ;
		List<String> lotNoList = new ArrayList<String>() ;

		for( ILotEntity pjtLotEntity : lotList ) {
			lotNoList.add( pjtLotEntity.getLotId() );
		}
		retRelCtlSearchBean.setSearchLotNoList		( labelList ) ;
		retRelCtlSearchBean.setSearchLotNoValueList	( lotNoList ) ;


		return retRelCtlSearchBean ;
	}

	/**
	 * リリースレコードの詳細検索条件を生成する。
	 * <br>対象のリリース環境にアクセス可能なロット情報の絞り込みを行う。
	 *
	 * @param selectedEnvNo
	 * @param searchBean
	 * @param lotAllEntityArray
	 * @return
	 */
	public static final RelCtlSearchBean setRelCtlSearchBean(
												String selectedEnvNo,
												RelCtlSearchBean searchBean,
												List<ILotDto> lotAllEntityArray ) {

		if ( TriStringUtils.isEmpty(selectedEnvNo) ) {
			return searchBean;
		}

		List<ItemLabelsBean> innerLabelList = new ArrayList<ItemLabelsBean>();
		List<String> innerLotNoList = new ArrayList<String>() ;

		Map<String, ILotDto> lotMap = ViewInfoAddonUtil.convertToMap(lotAllEntityArray);

		for( ItemLabelsBean item : searchBean.getSearchLotNoList() ) {
			ILotDto lotDto = lotMap.get(item.getValue());

			if ( TriStringUtils.isEmpty( lotDto.getLotRelEnvLnkEntities() ) ) {
				//ロットにリリース環境が未指定ならば、旧データ互換として絞り込まない
				innerLabelList.add(item);
				innerLotNoList.add(item.getValue());
			} else {
				for ( ILotRelEnvLnkEntity env: lotDto.getLotRelEnvLnkEntities() ) {
					if ( selectedEnvNo.equals( env.getBldEnvId() ) ) {
						innerLabelList.add(item);
						innerLotNoList.add(item.getValue());
						break;
					}
				}
			}
		}

		searchBean.setSearchLotNoList		( innerLabelList ) ;
		searchBean.setSearchLotNoValueList	( innerLotNoList ) ;

		return searchBean;
	}

	/**
	 * レポートレコードの詳細検索条件を生成する
	 * @param businessCommonInfo
	 * @param searchBean
	 * @param reportListCount
	 * @param reportEntityArray
	 * @param statusId
	 * @return
	 */
	public static final ReportSearchBean setReportSearchBean(
												IGeneralServiceBean businessCommonInfo,
												ReportSearchBean searchBean,
												DcmDesignBeanId reportListCount,
												DcmDesignBeanId statusId ) {

		ReportSearchBean retSearchBean = new ReportSearchBean();

		//レポートステータスのリストアップ
		List<String> selectStatusViewList = DesignDefineSearchUtils.extractValue(
				DcmDesignBeanId.statusId,
				DcmDesignBeanId.reportListStatusIdSearch );

		retSearchBean.setSelectStatusViewList( selectStatusViewList );

		if ( null != searchBean ) {
			retSearchBean.setSelectedStatusString	( searchBean.getSelectedStatusStringArray() );
			retSearchBean.setSearchCountList		( new ArrayList<ItemLabelsBean>() );
			retSearchBean.setSelectedCount			( searchBean.getSelectedCount() );

			retSearchBean.setSelectedReportId		( searchBean.getSelectedReportId() );
			retSearchBean.setSearchReportNo			( searchBean.getSearchReportNo() );
			retSearchBean.setSearchExecuteStartDate	( searchBean.getSearchExecuteStartDate() );
			retSearchBean.setSearchExecuteEndDate	( searchBean.getSearchExecuteEndDate() );

			retSearchBean.setSearchExecuteUser		( searchBean.getSearchExecuteUser() );

		}

		//ステータスの選択状態
		if( null == searchBean ) {
			//初期表示時のみ
			List<String> selectedStatus = DesignDefineSearchUtils.extractValue(
					DcmDesignBeanId.statusId,
					DcmDesignBeanId.reportListStatusIdSearch,
					StatusFlg.on.value() );

			retSearchBean.setSelectedStatusString( selectedStatus.toArray( new String[ 0 ] ) ) ;

			retSearchBean.setSearchExecuteUser( businessCommonInfo.getUserName() );
		}

		//検索件数のリストアップ
		List<ItemLabelsBean> countList = DesignDefineSearchUtils.mappingItemLabelsBeanByKeyParseInt( reportListCount );
		retSearchBean.setSearchCountList( countList ) ;

		//全体検索件数の選択
		if( null == searchBean || null == searchBean.getSelectedCount() ) {
			//初期表示時のみ
			Map<String,String> srcIdMap = sheet.getKeyMap( reportListCount );

			List<String> selectedCount = DesignDefineSearchUtils.extractKey( reportListCount, StatusFlg.on.value() ) ;
			retSearchBean.setSelectedCount( ( 0 == selectedCount.size() )? srcIdMap.keySet().iterator().next(): selectedCount.get( 0 ) ) ;
		}

		//レポート区分のリストアップ
		List<ItemLabelsBean> reportIdList = new ArrayList<ItemLabelsBean>() ;
		Map<String,String> srcIdMap = sheet.getKeyMap( DcmDesignBeanId.reportId );

		for( String reportId : srcIdMap.keySet() ) {
			reportIdList.add( new ItemLabelsBean( srcIdMap.get(reportId), reportId ) ) ;
		}
		retSearchBean.setSearchReportIdList( reportIdList ) ;

		return retSearchBean ;
	}
}
