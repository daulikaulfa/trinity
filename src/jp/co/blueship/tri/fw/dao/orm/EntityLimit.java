package jp.co.blueship.tri.fw.dao.orm;

import java.util.List;

/**
 * The implements of the IEntityLimit.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 */
public class EntityLimit<E> implements IEntityLimit<E> {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	private ILimit limit = null;
	private List<E> entities = null;

	/**
	 * 検索結果のページ制御を取得します。
	 * @return 取得した値を戻します。
	 */
	public ILimit getLimit() {
		return limit;
	}
	/**
	 * 検索結果のページ制御を設定します。
	 * @param limit ページ制御
	 */
	public void setLimit( ILimit limit ) {
		this.limit = limit;
	}

	/**
	 * 取得した検索結果を戻します。
     * @return 取得した検索結果を戻します。
	 */
	public List<E> getEntities() {
		return this.entities;
	}
	/**
	 * 取得した検索結果を設定します。
	 * @param entities 検索結果
	 */
	public void setEntities( List<E> entities  ) {
		this.entities = entities;
	}

}
