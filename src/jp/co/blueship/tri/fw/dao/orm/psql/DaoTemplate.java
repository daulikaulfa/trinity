package jp.co.blueship.tri.fw.dao.orm.psql;

import org.apache.commons.lang.StringUtils;

import jp.co.blueship.tri.fw.dao.orm.IDaoTemplate;
import jp.co.blueship.tri.fw.dao.orm.IDaoTemplateCallback;
import jp.co.blueship.tri.fw.dao.orm.ILimit;
import jp.co.blueship.tri.fw.dao.orm.ISqlCondition;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;
import jp.co.blueship.tri.fw.sm.dao.constants.SmTables;
import jp.co.blueship.tri.fw.um.constants.IconSelectionOption;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;

/**
 * The implements of the PostgreSQL DAO template.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 * @version V4.03.00
 * @author Cuong Nguyen
 */
public class DaoTemplate implements IDaoTemplate {
	private static final String SQL_COUNT_QUERY  = "SELECT count(*) FROM ";
	private static final String SQL_SELECT_QUERY = "SELECT * FROM ";
	private static final String SQL_DELETE_QUERY = "DELETE FROM ";

	private IDaoTemplateCallback callback = null;

	/**
	 * Customized DAO Only.
	 *
	 * @param callback
	 * @return
	 */
	protected final DaoTemplate setDaoTemplateCallback( IDaoTemplateCallback callback ) {
		this.callback = callback;
		return this;
	}

	@Override
	public final ILimit getPageLimit( Integer pageNo, Integer viewRows, int count ) {

		if ( null == pageNo || null == viewRows )
			return null;

		ILimit limit = new PageLimit();
		limit.setMaxRows( count );
		limit.setViewRows( viewRows.intValue() );
		limit.getPageBar().setValue( pageNo.intValue() );

		return limit;
	}

	@Override
	public String toCountQuery(ISqlCondition condition) {
		return this.toSelectQuery(condition, true);
	}

	@Override
	public String toSelectQuery(ISqlCondition condition) {
		return this.toSelectQuery(condition, false);
	}

	@Override
	public String toDeleteQuery(ISqlCondition condition) {
		if ( null == condition ) {
			return null;
		}

		return SQL_DELETE_QUERY + condition.getTableAttribute().name() + condition.toQueryString();
	}

	private String toSelectQuery(ISqlCondition condition, boolean isCountMode) {
		if ( null == condition ) {
			return null;
		}

		String tableName = condition.getTableAttribute().name();
		StringBuffer buf = new StringBuffer();

		String sql = null;

		if ( condition.isJoinExecData() ) {
			final String tableNameId = "{@tableName}";

			String keyItem = condition.getKeyByJoinExecData().getItemName();
			final String itemNameId = "{@itemName}";
			final String alias = "{@alias}";

			String keyItemByAccs = (condition.isJoinAccsHist())? condition.getKeyByJoinAccsHist() : "";
			final String itemNameIdByAccs = "{@accs.itemName}";
			final String aliasByAccs = "{@accs.alias}";

			final String aliasByCtg = "{@ctg.alias}";
			final String aliasByMstone = "{@mstone.alias}";

			buf
			.append( (isCountMode)? SQL_COUNT_QUERY: SQL_SELECT_QUERY)
			.append(" (")
			.append("  SELECT ")
			.append(" {@tableName}.*")
			.append("  ,(CASE WHEN {@alias}.PROC_STS_ID IS NULL THEN {@tableName}.STS_ID ELSE {@alias}.PROC_STS_ID END) AS PROC_STS_ID");

			if ( condition.isJoinAccsHist() ) {
				buf.append("  ,(CASE WHEN {@accs.alias}.ACCS_TIMESTAMP IS NULL THEN '-infinity' ELSE {@accs.alias}.ACCS_TIMESTAMP END) AS ACCS_TIMESTAMP");
				buf.append("  ,{@accs.alias}.NOTICE_VIEW_TIMESTAMP AS NOTICE_VIEW_TIMESTAMP");
			}
			if ( condition.isJoinUser() ) {
				for ( ITableItem item: condition.getKeyByJoinUsers() ) {
					buf
						.append("  ,(CASE WHEN " + item.getItemName() + "_ICON_TYP = '" + IconSelectionOption.DefaultImage.value() + "' THEN " + item.getItemName() + "_DEFAULT_ICON_PATH")
						.append("         WHEN " + item.getItemName() + "_ICON_TYP = '" + IconSelectionOption.CustomImage.value() + "' THEN " + item.getItemName() + "_CUSTOM_ICON_PATH")
						.append("      ELSE NULL END) AS " + item.getItemName() + "_ICON_PATH")
						;
				}
			}
			if ( condition.isJoinCtg() ) {
				buf.append("  ,{@ctg.alias}.CTG_ID AS CTG_ID");
				buf.append("  ,{@ctg.alias}.CTG_NM AS CTG_NM");
			}
			if ( condition.isJoinMstone() ) {
				buf.append("  ,{@mstone.alias}.MSTONE_ID AS MSTONE_ID");
				buf.append("  ,{@mstone.alias}.MSTONE_NM AS MSTONE_NM");
				buf.append("  ,{@mstone.alias}.MSTONE_ST_DATE AS MSTONE_ST_DATE");
				buf.append("  ,{@mstone.alias}.MSTONE_END_DATE AS MSTONE_END_DATE");
			}
			if ( null != callback ) {
				buf.append( callback.appendOfSelectClause( condition, isCountMode ) );
			}

			buf.append("  FROM {@tableName}")
			.append("  LEFT JOIN (")
			.append("    SELECT DATA_CTG_CD AS S_DATA_CTG_CD")
			.append("      ,DATA_ID AS S_DATA_ID")
			.append("      ,MAX(STS_ID) AS PROC_STS_ID")
			.append("    FROM SM_EXEC_DATA_STS")
			.append("    WHERE DEL_STS_ID = false")
			.append("    GROUP BY DATA_CTG_CD, DATA_ID")
			.append("  ) {@alias} ON {@alias}.S_DATA_ID = {@itemName} AND {@alias}.S_DATA_CTG_CD = '{@tableName}'");

			if ( condition.isJoinAccsHist() ) {
				if(condition.isRightJoinAccsHist()) {
					buf.append(" RIGHT JOIN (");
				} else {
					buf.append(" LEFT JOIN (");
				}

				buf.append("    SELECT DATA_CTG_CD AS U_DATA_CTG_CD")
				.append("      ,DATA_ID AS U_DATA_ID")
				.append("      ,USER_ID AS U_USER_ID, ACCS_TIMESTAMP, NOTICE_VIEW_TIMESTAMP")
				.append("    FROM UM_ACCS_HIST")
				.append("    WHERE DEL_STS_ID = false AND USER_ID = '{@accs.itemName}' AND DATA_CTG_CD = '{@tableName}'");
				if(condition.isRightJoinAccsHist()) {
					buf.append(" AND ACCS_TIMESTAMP IS NOT NULL");
				}
				buf.append("  ) {@accs.alias} ON {@accs.alias}.U_DATA_ID = {@itemName}");
			}

			if ( condition.isJoinUser() ) {
				for ( ITableItem item: condition.getKeyByJoinUsers() ) {
					buf.append(" LEFT JOIN (")
						.append("  SELECT USER_ID AS " + item.getItemName() + "_USER_ID")
						.append("    ,ICON_TYP AS " + item.getItemName() + "_ICON_TYP")
						.append("    ,DEFAULT_ICON_PATH AS " + item.getItemName() + "_DEFAULT_ICON_PATH")
						.append("    ,CUSTOM_ICON_PATH AS " + item.getItemName() + "_CUSTOM_ICON_PATH")
						.append("  FROM UM_USER )")
						.append("  " + item.getItemName() + " ON " + item.getItemName() + "_USER_ID = " + item.getItemName() + " ")
						;
				}
			}

			if ( condition.isJoinCtg() ) {
				buf.append("  LEFT JOIN (")
				.append("    SELECT C.CTG_ID AS CTG_ID")
				.append("      ,C.CTG_NM AS CTG_NM")
				.append("      ,DATA_ID AS C_DATA_ID")
				.append("    FROM UM_CTG_LNK CL")
				.append("      INNER JOIN UM_CTG C ON CL.CTG_ID = C.CTG_ID AND C.DEL_STS_ID = false")
				.append("    WHERE CL.DATA_CTG_CD = '{@tableName}'")
				.append("  ) {@ctg.alias} ON {@ctg.alias}.C_DATA_ID = {@itemName}");
			}

			if ( condition.isJoinMstone() ) {
				buf.append("  LEFT JOIN (")
				.append("    SELECT M.MSTONE_ID AS MSTONE_ID")
				.append("      ,M.MSTONE_NM AS MSTONE_NM")
				.append("      ,DATA_ID AS M_DATA_ID")
				.append("      ,M.MSTONE_ST_DATE AS MSTONE_ST_DATE")
				.append("      ,M.MSTONE_END_DATE AS MSTONE_END_DATE")
				.append("    FROM UM_MSTONE_LNK ML")
				.append("      INNER JOIN UM_MSTONE M ON ML.MSTONE_ID = M.MSTONE_ID AND M.DEL_STS_ID = false")
				.append("    WHERE ML.DATA_CTG_CD = '{@tableName}'")
				.append("  ) {@mstone.alias} ON {@mstone.alias}.M_DATA_ID = {@itemName}");
			}
			if ( null != callback ) {
				buf.append( callback.appendOfFromClause( condition, isCountMode ) );
			}

			buf.append(condition.toQueryString())
			.append(") T")
			.append(condition.toQueryStringByJoinExecData());

			sql = buf.toString();
			sql = StringUtils.replaceEach(
					sql,
					new String[]{tableNameId, itemNameId, alias, itemNameIdByAccs, aliasByAccs},
					new String[]{
							tableName,
							keyItem,
							SmTables.SM_EXEC_DATA_STS.alias(),
							keyItemByAccs,
							UmTables.UM_ACCS_HIST.alias()});

			if ( condition.isJoinCtg() ) {
				sql = StringUtils.replaceEach(
						sql,
						new String[]{aliasByCtg},
						new String[]{UmTables.UM_CTG.alias()});
			}

			if ( condition.isJoinMstone() ) {
				sql = StringUtils.replaceEach(
						sql,
						new String[]{aliasByMstone},
						new String[]{UmTables.UM_MSTONE.alias()});
			}

		} else {
			final String tableNameId = "{@tableName}";

			buf
			.append((isCountMode)? SQL_COUNT_QUERY: SQL_SELECT_QUERY)
			.append(" (")
			.append("  SELECT ")
			.append(" {@tableName}.*");

			if ( null != callback ) {
				buf.append( callback.appendOfSelectClause( condition, isCountMode ) );
			}

			buf.append("  FROM {@tableName}");

			if ( null != callback ) {
				buf.append( callback.appendOfFromClause( condition, isCountMode ) );
			}

			buf
			.append(condition.toQueryString())
			.append(") T");

			sql = buf.toString();
			sql = StringUtils.replaceEach(
					sql,
					new String[]{tableNameId},
					new String[]{tableName});
		}

		return sql;
	}


}
