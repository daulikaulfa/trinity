package jp.co.blueship.tri.fw.dao.orm;



/**
 * The interface of the data access condition object.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public interface IJdbcCondition {

	/**
	 * SQL conditon instanceを取得します。
	 * @return SQL conditon instance.
	 */
	public ISqlCondition getCondition();

}
