package jp.co.blueship.tri.fw.dao.orm.shun;

import java.sql.Array;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.NClob;
import java.sql.PreparedStatement;
import java.sql.SQLClientInfoException;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Savepoint;
import java.sql.Statement;
import java.sql.Struct;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executor;

import com.fujitsu.shun.ShunConnection;
import com.fujitsu.shun.ShunPreparedKey;
import com.fujitsu.shun.ShunPreparedStatement;

import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.log.TriLogMessage;

/**
 * Shunsakuとのコネクションを表現するクラスのラッパーです。
 */
public class ShunConnectionWrapper implements Connection {

	private static final ILog log = TriLogFactory.getInstance();

	private ShunConnection sConnection = null;
	private boolean isClosed = false;

	/**
	 * Shunsakuコネクションの開放もれを防ぐため、
	 * ファイナライザーガーディアンによるShunsakuの初期化を行います。
	 */
	@SuppressWarnings("unused")
	private final Object finalizerGuardian = new Object() {
		protected void finalize() throws Throwable {
			// closeされていない状態でオブジェクトが開放されようとしたときにログ（警告）を出す
			if ( false == isClosed() ) {
				LogHandler.warn( log , TriLogMessage.LSM0025 , String.valueOf( Thread.currentThread().getId() ) );
			}

			close();
			sConnection = null;
		}
	};

	/**
	 * Propertiesに指定されたホスト名、ポート番号およびShunsaku File名を用いて、 ShunConnectionオブジェクトを作成します。
	 *
	 * @param properties Shunsakuに接続するための情報を持ったプロパティリスト
	 */
	public ShunConnectionWrapper( Properties properties ) {

		try {
			sConnection = new ShunConnection( properties );

			if ( log.isDebugEnabled() ) {
				LogHandler.debug( log , "ShunConnection.newConnection:currentThread := "
						+ Thread.currentThread().getId() +
						" connection.shunsakuFile := " + properties.getProperty("connection.shunsakuFile"));
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw new IllegalStateException(e.getMessage());
		}
	}

	/**
	 * ホスト名とポート番号を指定して、ShunConnectionオブジェクトを作成します。
	 *
	 * @param host 接続先のホスト名またはIPアドレス
	 * @param port 接続先のポート番号
	 */
	public ShunConnectionWrapper( String host, int port ) {

		try {
			sConnection = new ShunConnection( host, port );

		} catch (Exception e) {
			e.printStackTrace();
			throw new IllegalStateException(e.getMessage());
		}
	}

	/**
	 * ホスト名、ポート番号、およびShunsaku File名を指定して、ShunConnectionオブジェクトを作成します。
	 *
	 * @param host 接続先のホスト名またはIPアドレス
	 * @param port 接続先のポート番号
	 * @param shunsakuFile 接続先のShunsaku File名
	 */
	public ShunConnectionWrapper( String host, int port, String shunsakuFile ) {

		try {
			sConnection = new ShunConnection( host, port, shunsakuFile );

		} catch (Exception e) {
			e.printStackTrace();
			throw new IllegalStateException(e.getMessage());
		}
	}

	/**
	 * ShunConnectionオブジェクトを解放します。
	 *
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	public void close() throws SQLException {
		if ( ! this.isClosed() ) {
			sConnection.close();
			isClosed = true;

			if ( log.isDebugEnabled() ) {
				LogHandler.debug( log , "ShunConnection.close:currentThread := " + Thread.currentThread().getId());
			}
		}
	}

	/**
	 * ShunConnectionオブジェクトが解放済かを判定します。
	 *
	 * @return 解放済みであれば、true。それ以外はfalseを戻します。
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	public boolean isClosed() throws SQLException {
		return ( null == sConnection )? true: isClosed;
	}

	/**
	 * XML文書を追加するためのShunPreparedStatementWrapperオブジェクトを作成します。
	 *
	 * @return XML文書を追加するためのShunPreparedStatementWrapperオブジェクト
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	public ShunPreparedStatementWrapper prepareInsert() throws SQLException {

		ShunPreparedStatement pstmt = sConnection.prepareInsert();
		return new ShunPreparedStatementWrapper(pstmt);

	}

	/**
	 * 検索式を指定してデータを検索するためのShunPreparedStatementオブジェクトを作成します。
	 *
	 * @param queryExp 検索式
	 * @param returnExp リターン式
	 * @return データを検索するためのShunPreparedStatementWrapperオブジェクト
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	public ShunPreparedStatementWrapper prepareSearch(String queryExp, String returnExp) throws SQLException {

		ShunPreparedStatement pstmt = sConnection.prepareSearch(queryExp, returnExp);
		return new ShunPreparedStatementWrapper(pstmt);

	}

	/**
	 * ダイレクトアクセスキーを指定してXML文書を検索するためのShunPreparedKeyオブジェクトを作成します。
	 *
	 * @param key ダイレクトアクセスキー名
	 * @param returnExp リターン式
	 * @return ダイレクトアクセスキーによりXML文書を検索するためのShunPreparedKeyWrapperオブジェクト
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	public ShunPreparedKeyWrapper prepareSearchKey(String key, String returnExp) throws SQLException {

		ShunPreparedKey pstmt = sConnection.prepareSearchKey(key, returnExp);
		return new ShunPreparedKeyWrapper(pstmt);

	}

	/**
	 * 接続先のShunsaku File名を返却します。
	 *
	 * @return 接続先のShunsaku File名
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	public String getShunFileName() throws SQLException {

		return sConnection.getShunsakuFileName();

	}

	/**
	 * 接続先のShunsaku Fileを切り替えます。
	 *
	 * @param shunsakuFileName 切替え後のShunsaku File名
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	public void setShunFileName(String shunsakuFileName) throws SQLException {

		sConnection.setShunsakuFileName(shunsakuFileName);

	}

	/**
	 * ダイレクトアクセスキーを指定してXML文書を削除するためのShunPreparedKeyWrapperオブジェクトを作成します。
	 *
	 * @param key ダイレクトアクセスキー名
	 * @return ダイレクトアクセスキーによりXML文書を削除するためのShunPreparedKeyWrapperオブジェクト
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	public ShunPreparedKeyWrapper prepareDeleteKey(String key) throws SQLException {

		return new ShunPreparedKeyWrapper(sConnection.prepareDeleteKey(key));
	}

	/**
	 * レコードIDを指定してXML文書を削除するためのShunPreparedRecordIDWrapperオブジェクトを作成します。
	 *
	 * @return レコードIDによりXML文書を削除するためのShunPreparedRecordIDWrapperオブジェクト
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	public ShunPreparedRecordIDWrapper prepareDeleteRecordID() throws SQLException {

		return new ShunPreparedRecordIDWrapper(sConnection.prepareDeleteRecordID());
	}

	/**
	 * ダイレクトアクセスキーを指定してXML文書を更新するためのShunPreparedKeyWrapperオブジェクトを作成します。
	 *
	 * @param key ダイレクトアクセスキー名
	 * @return ダイレクトアクセスキーによりXML文書を更新するためのShunPreparedKeyWrapperオブジェクト
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	public ShunPreparedKeyWrapper prepareUpdateKey(String key) throws SQLException {

		return new ShunPreparedKeyWrapper(sConnection.prepareUpdateKey(key));
	}

	/**
	 * レコードIDを指定してXML文書を更新するためのShunPreparedRecordIDWrapperオブジェクトを作成します。
	 *
	 * @return レコードIDによりXML文書を更新するためのShunPreparedRecordIDWrapperオブジェクト
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	public ShunPreparedRecordIDWrapper prepareUpdateRecordID() throws SQLException {

		return new ShunPreparedRecordIDWrapper(sConnection.prepareUpdateRecordID());
	}

	/**
	 * 現在実行中のトランザクションを有効にして、トランザクションを終了します。
	 *
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	public void commit() throws SQLException {
		sConnection.commit();
	}

	/**
	 * 現在のコミットモードを返却します。
	 *
	 * @return 現在のコミットモード（trueの場合は自動コミットが有効、falseの場合は自動コミットが無効）
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	public boolean getAutoCommit() throws SQLException {
		return sConnection.getAutoCommit();
	}

	/**
	 * コミットモードを指定します。
	 * <br>本メソッドの呼出しが省略された場合、自動コミットは有効となります。
	 *
	 * @param autoCommit trueの場合は自動コミットが有効、falseの場合は自動コミットが無効
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	public void setAutoCommit(boolean autoCommit) throws SQLException {
		sConnection.setAutoCommit( autoCommit );
	}

	/**
	 * 現在実行中のトランザクションを無効にして、トランザクションを終了します。
	 *
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	public void rollback() throws SQLException {
		sConnection.rollback();
	}

	/**
	 * 接続先のホスト名またはIPアドレスを返却します。
	 *
	 * @return 接続先のホスト名またはIPアドレス
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	public String getHostName() throws SQLException {
		return sConnection.getHostName();
	}

	/**
	 * 接続先のポート番号を返却します。
	 *
	 * @return 接続先のポート番号
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	public int getPort() throws SQLException {
		return sConnection.getPort();
	}

	/**
	 * Shunsakuのデータの文字コードをJavaのエンコーディング名で返却します。
	 * Javaのエンコーディング名については、“Java 2 SDK, Standard Edition ドキュメント”を参照してください。
	 *
	 * @return 文字コード
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	public String getServerEncoding() throws SQLException {
		return sConnection.getServerEncoding();
	}

	public void clearWarnings() throws SQLException {
	}

	public Statement createStatement() throws SQLException {
		return null;
	}

	public Statement createStatement(int arg0, int arg1) throws SQLException {
		return null;
	}

	public Statement createStatement(int arg0, int arg1, int arg2)
			throws SQLException {
		return null;
	}

	public String getCatalog() throws SQLException {
		return null;
	}

	public int getHoldability() throws SQLException {
		return 0;
	}

	public DatabaseMetaData getMetaData() throws SQLException {
		return null;
	}

	public int getTransactionIsolation() throws SQLException {
		return 0;
	}

	public Map<String, Class<?>> getTypeMap() throws SQLException {
		return null;
	}

	public SQLWarning getWarnings() throws SQLException {
		return null;
	}

	public boolean isReadOnly() throws SQLException {
		return false;
	}

	public String nativeSQL(String arg0) throws SQLException {
		return null;
	}

	public CallableStatement prepareCall(String arg0) throws SQLException {
		return null;
	}

	public CallableStatement prepareCall(String arg0, int arg1, int arg2)
			throws SQLException {
		return null;
	}

	public CallableStatement prepareCall(String arg0, int arg1, int arg2,
			int arg3) throws SQLException {
		return null;
	}

	/**
	 * @deprecated Not compatible function
	 */
	public PreparedStatement prepareStatement(String arg0) throws SQLException {
		return null;
	}

	public PreparedStatement prepareStatement(String arg0, int arg1)
			throws SQLException {
		return null;
	}

	public PreparedStatement prepareStatement(String arg0, int[] arg1)
			throws SQLException {
		return null;
	}

	public PreparedStatement prepareStatement(String arg0, String[] arg1)
			throws SQLException {
		return null;
	}

	public PreparedStatement prepareStatement(String arg0, int arg1, int arg2)
			throws SQLException {
		return null;
	}

	public PreparedStatement prepareStatement(String arg0, int arg1, int arg2,
			int arg3) throws SQLException {
		return null;
	}

	public void releaseSavepoint(Savepoint arg0) throws SQLException {
	}
	/**
	 * @deprecated 継承関係がjava.sql.Connection(rt.jar)とshunapi.jarとでおかしいため使用禁止
	 *        2010/10/15時点で、FJに問い合わせ予定。
	 */
	public void rollback(Savepoint arg0) throws SQLException {
		//sConnection.rollback(arg0);
		sConnection.rollback();
	}

	public void setCatalog(String arg0) throws SQLException {
		sConnection.setCatalog(arg0);
	}
	/**
	 * @deprecated 継承関係がjava.sql.Connection(rt.jar)とshunapi.jarとでおかしいため使用禁止
	 *        2010/10/15時点で、FJに問い合わせ予定。
	 */
	public void setHoldability(int arg0) throws SQLException {
		//sConnection.setHoldability(arg0);
		throw new SQLException("使用禁止のメソッドです");
	}

	public void setReadOnly(boolean arg0) throws SQLException {
		sConnection.setReadOnly(arg0);
	}
	/**
	 * @deprecated 継承関係がjava.sql.Connection(rt.jar)とshunapi.jarとでおかしいため使用禁止
	 *        2010/10/15時点で、FJに問い合わせ予定。
	 */
	public Savepoint setSavepoint() throws SQLException {
		//return sConnection.setSavepoint();
		throw new SQLException("使用禁止のメソッドです");
	}
	/**
	 * @deprecated 継承関係がjava.sql.Connection(rt.jar)とshunapi.jarとでおかしいため使用禁止
	 *        2010/10/15時点で、FJに問い合わせ予定。
	 */
	public Savepoint setSavepoint(String arg0) throws SQLException {
		//return sConnection.setSavepoint(arg0);
		throw new SQLException("使用禁止のメソッドです");
	}

	public void setTransactionIsolation(int arg0) throws SQLException {
		sConnection.setTransactionIsolation(arg0);
	}

	public void setTypeMap(Map<String, Class<?>> arg0) throws SQLException {
		sConnection.setTypeMap(arg0);
	}

	/**
	 * @deprecated JRE1.6未対応
	 */
	@Override
	public <T> T unwrap(Class<T> iface) throws SQLException {
		return null;
	}

	/**
	 * @deprecated JRE1.6未対応
	 */
	@Override
	public boolean isWrapperFor(Class<?> iface) throws SQLException {
		return false;
	}

	/**
	 * @deprecated JRE1.6未対応
	 */
	@Override
	public Clob createClob() throws SQLException {
		return null;
	}

	/**
	 * @deprecated JRE1.6未対応
	 */
	@Override
	public Blob createBlob() throws SQLException {
		return null;
	}

	/**
	 * @deprecated JRE1.6未対応
	 */
	@Override
	public NClob createNClob() throws SQLException {
		return null;
	}

	/**
	 * @deprecated JRE1.6未対応
	 */
	@Override
	public SQLXML createSQLXML() throws SQLException {
		return null;
	}

	/**
	 * @deprecated JRE1.6未対応
	 */
	@Override
	public boolean isValid(int timeout) throws SQLException {
		return false;
	}

	/**
	 * @deprecated JRE1.6未対応
	 */
	@Override
	public void setClientInfo(String name, String value)
			throws SQLClientInfoException {

	}

	/**
	 * @deprecated JRE1.6未対応
	 */
	@Override
	public void setClientInfo(Properties properties)
			throws SQLClientInfoException {

	}

	/**
	 * @deprecated JRE1.6未対応
	 */
	@Override
	public String getClientInfo(String name) throws SQLException {
		return null;
	}

	/**
	 * @deprecated JRE1.6未対応
	 */
	@Override
	public Properties getClientInfo() throws SQLException {
		return null;
	}

	/**
	 * @deprecated JRE1.6未対応
	 */
	@Override
	public Array createArrayOf(String typeName, Object[] elements)
			throws SQLException {
		return null;
	}

	/**
	 * @deprecated JRE1.6未対応
	 */
	@Override
	public Struct createStruct(String typeName, Object[] attributes)
			throws SQLException {
		return null;
	}

	/**
	 * @deprecated Not compatible function
	 */
	@Override
	public void setSchema(String schema) throws SQLException {
		return;
	}

	/**
	 * @deprecated Not compatible function
	 */
	@Override
	public String getSchema() throws SQLException {
		return null;
	}

	/**
	 * @deprecated Not compatible function
	 */
	@Override
	public void abort(Executor executor) throws SQLException {
		return;
	}

	/**
	 * @deprecated Not compatible function
	 */
	@Override
	public void setNetworkTimeout(Executor executor, int milliseconds) throws SQLException {
		return;
	}

	/**
	 * @deprecated Not compatible function
	 */
	@Override
	public int getNetworkTimeout() throws SQLException {
		return 0;
	}
}
