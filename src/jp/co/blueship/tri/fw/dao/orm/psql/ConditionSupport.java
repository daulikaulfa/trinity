package jp.co.blueship.tri.fw.dao.orm.psql;

import jp.co.blueship.tri.fw.constants.JoinType;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlCondition;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;
import jp.co.blueship.tri.fw.sm.dao.constants.SmTables;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;

/**
 * The SQL condition support of the entity.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 * @version V4.03.00
 * @author Cuong Nguyen
 */
public abstract class ConditionSupport implements IJdbcCondition {

	private ConditionTemplate template;

	/**
	 * Constructor for ConditionSupport
	 * <br>
	 * @param tableAttr table attribute
	 */
	public ConditionSupport( ITableAttribute tableAttr ) {
		template = new ConditionTemplate( tableAttr );
	}

	/**
	 * SQL condition instance.を取得します。
	 * @return WhereBuilder instance.
	 */
	public ISqlCondition getCondition() {
		return template;
	}

	/**
	 * @see SmTables#SM_EXEC_DATA_STS 実行中データのステータスと外部結合（LEFT JOIN）を行うかどうかを設定します
	 *
	 * @param isJoinExecData 外部結合を行う場合true。それ以外はfalse。
	 */
	protected ConditionSupport setJoinExecData( boolean isJoinExecData ) {
		template.setJoinExecData(isJoinExecData);
		return this;
	}

	/**
	 * @see ISqlCondition#isJoinExecData() isJoinExecDataの戻り値がtrueの場合、結合キーを取得するための項目名を設定します。
	 *
	 * @param keyItem 対象レコードのキー項目
	 */
	protected ConditionSupport setKeyByJoinExecData( ITableItem keyItem ) {
		template.setKeyByJoinExecData(keyItem);
		return this;
	}

	/**
	 * アクセス履歴と外部結合（LEFT JOIN）を行うかどうかを設定します
	 * <br>isJoinExecDataの戻り値がtrueの場合のみ有効です。
	 * @see UmTables#UM_ACCS_HIST アクセス履歴
	 *
	 * @param isJoinAccsHist 外部結合を行う場合true。それ以外はfalse。
	 */
	protected ConditionSupport setJoinAccsHist( boolean isJoinAccsHist ) {
		template.setJoinAccsHist(isJoinAccsHist);
		return this;
	}

	/**
	 * Set whether to perform access history and outer join (LEFT or RIGHT JOIN)
	 * This function is valid only when the return value of isJoinExecData is true.
	 * @see UmTables#UM_ACCS_HIST access history
	 *
	 * @param isJoinAccsHist True when performing outer join. Otherwise false.
	 * @param join Specific LEFT or RIGHT JOIN
	 */
	protected ConditionSupport setJoinAccsHist( boolean isJoinAccsHist, JoinType join ) {
		template.setJoinAccsHist(isJoinAccsHist, join);
		return this;
	}


	/**
	 * @see ISqlCondition#isJoinExecData() isJoinExecDataの戻り値がtrueの場合、結合キーを取得するための項目名を設定します。
	 *
	 * @param userId 対象レコードのキー値
	 */
	protected ConditionSupport setKeyByJoinAccsHist( String userId ) {
		template.setKeyByJoinAccsHist(userId);
		return this;
	}

	/**
	 * ユーザと外部結合（LEFT JOIN）を行うかどうかを設定します
	 * <br>isJoinExecDataの戻り値がtrueの場合のみ有効です。
	 * @see UmTables#UM_USER ユーザ
	 *
	 * @param isJoinUser 外部結合を行う場合true。それ以外はfalse。
	 */
	protected ConditionSupport setJoinUser( boolean isJoinUser ) {
		template.setJoinUser(isJoinUser);
		return this;
	}


	/**
	 * @see ISqlCondition#isJoinUser() isJoinUserの戻り値がtrueの場合、結合キーを取得するための項目名を設定します。
	 *
	 * @param item 対象レコードのキー値
	 */
	protected ConditionSupport setKeyByJoinUsers( ITableItem... item ) {
		template.setKeyByJoinUsers(item);
		return this;
	}

	/**
	 * カテゴリーと外部結合（LEFT JOIN）を行うかどうかを設定します
	 * <br>isJoinCtgの戻り値がtrueの場合のみ有効です。
	 * @see UmTables#UM_CTG カテゴリー
	 *
	 * @param isJoinCtg 外部結合を行う場合true。それ以外はfalse。
	 */
	protected ConditionSupport setJoinCtg( boolean isJoinCtg ) {
		template.setJoinCtg(isJoinCtg);
		return this;
	}

	/**
	 * マイルストーンと外部結合（LEFT JOIN）を行うかどうかを設定します
	 * <br>isJoinMstoneの戻り値がtrueの場合のみ有効です。
	 * @see UmTables#UM_MSTONE マイルストーン
	 *
	 * @param isJoinMstone 外部結合を行う場合true。それ以外はfalse。
	 */
	protected ConditionSupport setJoinMstone( boolean isJoinMstone ) {
		template.setJoinMstone(isJoinMstone);
		return this;
	}

	/**
	 * Queryを組みたてるための文字列にappendします
	 *
	 * @param item テーブル項目
	 * @param value 要素に設定する値
	 * @return a reference to this instance, so it's possible to chain several calls together
	 */
	protected ConditionSupport append( ITableItem item, Object value ) {
		template.append(item, value);
		return this;
	}

	/**
	 * Queryを組みたてるための文字列にappendします
	 *
	 * @param item テーブル項目
	 * @param value 要素に設定する値
	 * @return a reference to this instance, so it's possible to chain several calls together
	 */
	protected ConditionSupport append( String item, String value ) {
		template.append(item, value);
		return this;
	}
	/**
	 * Queryを組みたてるための文字列にappendします
	 * <br>“実行中データのステータス”と外部結合を行う場合のみ使用します
	 *
	 * @param item テーブル項目
	 * @param value 要素に設定する値
	 * @return a reference to this instance, so it's possible to chain several calls together
	 */
	protected ConditionSupport appendByJoinExecData( String item, String value ) {
		template.appendByJoinExecData(item, value);
		return this;
	}


}
