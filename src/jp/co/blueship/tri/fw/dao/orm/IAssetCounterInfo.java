package jp.co.blueship.tri.fw.dao.orm;


/**
 * 貸出、追加、変更、削除の資産数を保持するエンティティのインタフェースです。
 *
 */
public interface IAssetCounterInfo extends IEntity {

	/**
	 * 貸出資産数を取得します。
	 * @return 取得した値を戻します。
	 */
	public Integer getLendCount();
	/**
	 * 貸出資産数を設定します。
	 * @param lendCount 貸出資産数
	 */
	public void setLendCount( Integer lendCount );

	/**
	 * 追加資産数を取得します。
	 * @return 取得した値を戻します。
	 */
	public Integer getAddCount();
	/**
	 * 追加資産数を設定します。
	 * @param addCount 追加資産数
	 */
	public void setAddCount( Integer addCount );

	/**
	 * 変更資産数を取得します。
	 * @return 取得した値を戻します。
	 */
	public Integer getChangeCount();
	/**
	 * 変更資産数を設定します。
	 * @param changeCount 変更資産数
	 */
	public void setChangeCount( Integer changeCount );

	/**
	 * 削除資産数を取得します。
	 * @return 取得した値を戻します。
	 */
	public Integer getDelCount();
	/**
	 * 削除資産数を設定します。
	 * @param delCount 削除資産数
	 */
	public void setDelCount( Integer delCount );
}
