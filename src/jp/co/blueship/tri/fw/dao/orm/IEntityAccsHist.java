package jp.co.blueship.tri.fw.dao.orm;

import java.sql.Timestamp;

/**
 * This is an interface that needs to entity inherits the "UM access history".
 * <br>
 * <br>業務テーブルのエンティティが継承する必要のあるインタフェースです。
 * <br>“UM - アクセス履歴”で管理対象のエンティティで実装します。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public interface IEntityAccsHist extends IEntity {

	/**
	 * Gets the Access Timestamp.
	 * <br>Access Timestampを取得します。
	 *
	 * @return Access Timestamp
	 */
	public Timestamp getAccsTimestamp();

	/**
	 * Gets the Notification Viewed Timestamp.
	 * <br>Notification Viewed Timestampを取得します。
	 *
	 * @return Notification Viewed Timestamp
	 */
	public Timestamp getNoticeViewTimestamp();

}
