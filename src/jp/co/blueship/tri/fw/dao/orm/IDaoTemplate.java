package jp.co.blueship.tri.fw.dao.orm;



/**
 * The interface of the DAO template.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 */
public interface IDaoTemplate {
	/**
	 * 指定されたページ位置、表示行数より、ページ制御クラスを取得します。
     * @param pageNo ページ番号
     * @param viewRows 表示行数
	 * @param count 対象件数
	 * @return ページ制御を戻します。取得できない場合、nullを戻します。
	 */
	public ILimit getPageLimit( Integer pageNo, Integer viewRows, int count );

	/**
	 * 指定された検索条件より、レコード件数のQueryを取得します。
	 *
	 * @param condition 検索条件
	 * @return 件数を取得するSQL文を戻します。
	 */
	public String toCountQuery( ISqlCondition condition );

	/**
	 * レコードの全項目を取得するselect queryを取得します。
	 *
	 * @param condition 検索条件
	 * @return 取得したSQL文を戻します。
	 */
	public String toSelectQuery( ISqlCondition condition );

	/**
	 * 指定された検索条件より、delete queryを取得します。
	 *
	 * @param condition 検索条件
	 * @return 取得したSQL文を戻します。
	 */
	public String toDeleteQuery( ISqlCondition condition );

}
