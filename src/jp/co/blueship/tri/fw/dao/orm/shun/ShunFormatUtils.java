package jp.co.blueship.tri.fw.dao.orm.shun;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;


/**
 * Shunsakuの文字列処理を行うユーティリティークラスです。
 *
 */
public class ShunFormatUtils {

	

	/**
	 * Shunsakuのエスケープ文字テーブル
	 */
	private static Map<String, String> escapeMap = getEscapeMap();

	/**
	 * 検索式、リターン式およびソート式に指定する値に、
	 * Shunsaku特定文字が含まれる場合、エスケープ文字を付加します。
	 * <br>
	 * <br>Interstage Shunsaku Data Manager アプリケーション開発ガイドの以下を参照のこと
	 * <br>付録B 検索式、リターン式およびソート式の書式 > B.3 検索式
	 *
	 * @param target リテラル内の値
	 * @return 変換した値を戻します。
	 */
	public static final String escape( String target ) {
		if ( null == target ) {
			return null;
		}

		Appendable outBuf = new StringBuilder();

		String beforeValue = "";

		try {
			for ( int i = 0; i < target.length(); i++ ) {
				String value = target.substring(i, i+1);

				if ( escapeMap.containsKey( value ) ) {
					if ( "\\".equals(beforeValue) ) {
						outBuf.append( value );
						continue;
					}

					outBuf.append( escapeMap.get(value) );
					continue;
				}

				outBuf.append( value );

				beforeValue = value;
			}
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}

		return outBuf.toString();
	}

	/**
	 * Shunsakuの検索式、リターン式、ソート式に適用するエスケープ文字を取得します。
	 *
	 * @return 取得したマップを戻します。
	 */
	private static final Map<String, String> getEscapeMap() {
		Map<String, String> map = new HashMap<String, String>();

		map.put("\"", "\\\"");
		map.put("$", "\\$");
		map.put("&", "\\&");
		map.put("'", "\\'");
		map.put("'", "\\'");
		map.put("(", "\\(");
		map.put(")", "\\)");
		map.put("*", "\\*");
		map.put("+", "\\+");
		map.put(",", "\\,");
		map.put("-", "\\-");
		map.put(".", "\\.");
		map.put("?", "\\?");
		map.put("[", "\\[");
		map.put("\\", "\\\\");
		map.put("]", "\\]");
		map.put("^", "\\^");
		map.put("{", "\\{");
		map.put("|", "\\|");
		map.put("}", "\\}");
		map.put("~", "\\~");

		return map;
	}

	/**
	 * from-toの値から、Shunsaku検索式を取得します。
	 *
	 * @param from 検索条件のFrom値
	 * @param to 検索条件のTo値
	 * @param element テーブルの要素名
	 * @return Shunsaku検索式を戻します。
	 */
	public static final String getFromTo( String from, String to, String element ) {
		StringBuilder buf = new StringBuilder();

		from = ("".equals(from))? null:  from;
		to = ("".equals(to))? null:  to;

		if ( null == from && null == to ) {
			return null;
		}

		String newFrom = escape( from );
		String newTo = escape( to );

		buf.append( "(" );

		if ( null == newFrom && null != newTo ) {
			buf.append( element + " <= '" + newTo + "'" );
		}
		if ( null != newFrom && null == newTo ) {
			buf.append( element + " >= '" + newFrom + "'" );
		}
		if ( null != newFrom && null != newTo ) {
			buf.append( element + " >= '" + newFrom + "' AND " );
			buf.append( element + " <= '" + newTo + "'" );
		}

		buf.append( ")" );

		return buf.toString();
	}


	/**
	 * Shunsaku検索式を取得します。（完全一致）
	 * <br>空白で区切られた条件の場合、それを複数の検索条件としてOR検索を行います。
	 * これにより、特定項目の絞込み検索を行います。
	 *
	 * @param conditionValue OR検索条件
	 * @param element テーブルの要素名
	 * @return Shunsaku検索式を戻します。
	 */
	public static final String getCondition( String conditionValue, String element ) {
		return getCondition( conditionValue, element, false, false );
	}

	/**
	 * Shunsaku検索式を取得します。（完全不一致を対象）
	 * <br>空白で区切られた条件の場合、それを複数の検索条件としてOR検索を行います。
	 * これにより、特定項目の絞込み検索を行います。
	 *
	 * @param conditionValue OR検索条件
	 * @param element テーブルの要素名
	 * @return Shunsaku検索式を戻します。
	 */
	public static final String getConditionByNotEquals( String conditionValue, String element ) {
		return getCondition( conditionValue, element, false, true );
	}

	/**
	 * Shunsaku検索式を取得します。
	 * <br>空白で区切られた条件の場合、それを複数の検索条件としてOR検索を行います。
	 * これにより、特定項目の絞込み検索を行います。
	 *
	 * @param conditionValue OR検索条件
	 * @param element テーブルの要素名
	 * @param isLike 部分検索の場合、true
	 * @return Shunsaku検索式を戻します。
	 */
	public static final String getCondition( 	String conditionValue,
												String element,
												boolean isLike ) {
		return getCondition( conditionValue, element, isLike, false );
	}

	/**
	 * Shunsaku検索式を取得します。
	 * <br>空白で区切られた条件の場合、それを複数の検索条件としてOR検索を行います。
	 * これにより、特定項目の絞込み検索を行います。
	 *
	 * @param conditionValue OR検索条件
	 * @param element テーブルの要素名
	 * @param isLike 部分検索の場合、true
	 * @param isNotEquals 一致しない条件を対象とする場合、true。それ以外はfalseを設定
	 * @return Shunsaku検索式を戻します。
	 */
	public static final String getCondition( 	String conditionValue,
												String element,
												boolean isLike,
												boolean isNotEquals ) {
		conditionValue = ("".equals(conditionValue))? null:  conditionValue;

		if ( null == conditionValue ) {
			return null;
		}

		String newConditionValue = conditionValue;

		String[] splits = StringUtils.split(conditionValue, " ");
		if ( 1 < splits.length ) {
			return getCondition( splits, element, isLike, true, isNotEquals );
		}

		newConditionValue = ( 0 == splits.length )? "" : escape( splits[0] );

		return element + ((isLike)? (isNotEquals)? "!=":"=": (isNotEquals)? "!==":"==") + "'" + newConditionValue + "'";
	}

	/**
	 * 複数ORの値から、Shunsaku検索式を取得します。
	 *
	 * @param conditionValues AND検索条件
	 * @param element テーブルの要素名
	 * @return Shunsaku検索式を戻します。
	 */
	public static final String getOr( String[] conditionValues, String element ) {
		return getCondition( conditionValues, element, false, true, false );
	}

	/**
	 * 複数ORの値から、Shunsaku検索式を取得します。
	 *
	 * @param conditionValues AND検索条件
	 * @param element テーブルの要素名
	 * @param isLike 部分検索の場合、true
	 * @return Shunsaku検索式を戻します。
	 */
	public static final String getOr( String[] conditionValues, String element, boolean isLike ) {
		return getCondition( conditionValues, element, isLike, true, false );
	}

	/**
	 * 複数ANDの値から、Shunsaku検索式を取得します。
	 *
	 * @param conditionValues OR検索条件
	 * @param element テーブルの要素名
	 * @return Shunsaku検索式を戻します。
	 */
	public static final String getAnd( String[] conditionValues, String element ) {
		return getCondition( conditionValues, element, false, false, false );
	}

	/**
	 * 複数ANDの値から、Shunsaku検索式を取得します。（不一致を対象）
	 *
	 * @param conditionValues OR検索条件
	 * @param element テーブルの要素名
	 * @return Shunsaku検索式を戻します。
	 */
	public static final String getAndByNotEquals( String[] conditionValues, String element ) {
		return getCondition( conditionValues, element, false, false, true );
	}

	/**
	 * 複数条件の値から、Shunsaku検索式を取得します。
	 *
	 * @param conditionValues OR検索条件
	 * @param element テーブルの要素名
	 * @param isLike 部分検索の場合、true
	 * @param isOr 複数条件をOR連結する場合は、true。それ以外(and)はfalseを設定
	 * @param isNotEquals 一致しない条件を対象とする場合、true。それ以外はfalseを設定
	 * @return Shunsaku検索式を戻します。
	 */
	public static final String getCondition( 	String[] conditionValues,
												String element,
												boolean isLike,
												boolean isOr,
												boolean isNotEquals) {
		StringBuilder buf = new StringBuilder();

		if ( null == conditionValues || conditionValues.length == 0 ) {
			return null;
		}

		String[] newConditionValues = new String[conditionValues.length];

		buf.append( "(" );

		for ( int i =0; i < conditionValues.length; i++ ) {
			if ( i != 0 ) {
				if ( isOr )
					buf.append(" OR ");
				else
					buf.append(" AND ");
			}

			newConditionValues[i] = escape( conditionValues[i] );
			buf.append( element + ((isLike)? ((isNotEquals)? "!=":"="): (isNotEquals)? "!==":"==") + "'" + newConditionValues[i] + "'" );
		}

		buf.append( ")" );

		return buf.toString();
	}

	/**
	 * Shunsakuソート式を取得します。
	 *
	 * @param sort ソート条件（昇順／降順）
	 * @param element テーブルの要素名
	 * @return Shunsaku検索式を戻します。
	 */
	public static final String getSort( String sort, String element ) {

		if ( null == sort ) {
			return null;
		}

		String newSort = escape( sort );

		return element + ((element.startsWith("/@"))? "/text()":"") + " " + newSort;
	}

	/**
	 * Shunsakuダイレクトアクセスキー値を取得します。
	 *
	 * @param values 条件
	 * @return ダイレクトアクセスキー値を戻します。
	 */
	public static final String getKey( String[] values ) {
		StringBuilder buf = new StringBuilder();

		if ( null == values || values.length == 0 ) {
			return null;
		}

		String[] newValues = new String[values.length];

		for ( int i =0; i < values.length; i++ ) {
			if ( i != 0 ) {
				buf.append(",");
			}

			newValues[i] = escape( values[i] );
			buf.append( newValues[i] );
		}

		return buf.toString();
	}

	/**
	 * 現在日付を文字列で取得します。
	 * <br>yyyy/MM/dd HH:mm:ss.SSSの書式で編集されます。
	 *
	 * @return 文字列の現在日付を戻します。
	 */
	public static final String newDate() {
		return TriDateUtils.getDefaultDateFormat().format( new Date() );
	}

}
