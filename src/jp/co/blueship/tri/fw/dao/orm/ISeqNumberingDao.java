package jp.co.blueship.tri.fw.dao.orm;

import jp.co.blueship.tri.fw.dao.orm.ex.TriJdbcDaoException;


/**
 * 指定された連番から採番を行うためのアクセスインタフェースです。
 *
 */
public interface ISeqNumberingDao extends IDao {

	

	/**
	 * 自動採番します。
	 *
	 * @param seq 採番編集するシーケンス番号
	 * @return 採番された値を戻します
	 */
	public String nextval( int seq ) throws TriJdbcDaoException;

}
