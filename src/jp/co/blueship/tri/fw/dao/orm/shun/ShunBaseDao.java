package jp.co.blueship.tri.fw.dao.orm.shun;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.xmlbeans.XmlObject;
import org.apache.xmlbeans.XmlOptions;

import jp.co.blueship.tri.fw.dao.orm.ILimit;
import jp.co.blueship.tri.fw.dao.orm.psql.PageLimit;
import jp.co.blueship.tri.fw.dao.orm.shun.rpc.IShunTemplateRemote;
import jp.co.blueship.tri.fw.dao.orm.shun.rpc.ShunRemoteAttributes;
import jp.co.blueship.tri.fw.dao.orm.shun.rpc.ShunTemplateRemoteServiceWrapper;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.SmMessageId;

/**
 * Shunsaku用のData Access Object(DAO)の基底抽象クラスです。
 *
 */
public class ShunBaseDao {

	private static final ILog log = TriLogFactory.getInstance();

	private IContextAdapter context = null;
	private IShunTemplate shunTemplate = null;
	private ShunRemoteAttributes attributes = null;

	private Map<String, String> xmlNamespaceMap = new HashMap<String, String>();
	private XmlOptions opt = new XmlOptions(); /* http://xmlbeans.apache.org/docs/2.0.0/reference/constant-values.html#org.apache.xmlbeans.XmlOptions.COMPILE_MDEF_NAMESPACES */

	/**
	 * ビーンを生成するコンテキストが設定されます。
	 * @param context コンテキスト
	 */
	public final void setContext( IContextAdapter context ) {
		this.context = context;
	}

	/**
	 * ビーンを生成するコンテキストを取得します。
	 * @return コンテキスト
	 */
	protected final IContextAdapter getContext() {
		if ( null == this.context )
			this.context = ContextAdapterFactory.getContextAdapter();

		return this.context;
	}

	/**
	 * Shunsakuデータソースが設定されます。
	 * <br>リモートサービスを利用する場合は、データソースを設定する必要はありません。
	 * @param datasource Shunsakuデータソース
	 */
	public void setDataSource(DataSource datasource) {
		this.setShunTemplate( new ShunTemplate( datasource ) );
	}

	/**
	 * Shunsakuリモートサービスに指定するプロパティ情報を設定します。
	 * <br>リモートサービスを利用する場合は、データソースを設定する必要はありません。
	 *
	 * @param properties Shunsakuに接続するための情報を持ったプロパティリスト
	 */
	public void setRemoteAttributes( ShunRemoteAttributes attributes ) {
		this.attributes = attributes;
	}

	/**
	 * Shunsakuを利用するためのアクセステンプレートが設定されます。
	 *
	 * @param shunTemplate アクセステンプレート
	 */
	public void setShunTemplate( IShunTemplate shunTemplate ) {
		this.shunTemplate = shunTemplate;
	}

    /**
     * Shunsakuを利用するためのアクセステンプレートを取得します。
     * @return アクセステンプレート
     */
    protected final IShunTemplate getShunTemplate() {
    	if ( null == this.shunTemplate && null != this.attributes ) {
    		try {
    	  		IContextAdapter contextAdapter = ContextAdapterFactory.getContextAdapter( new String[] { this.attributes.getConfigLocations() } );

    	  		ShunTemplateRemoteServiceWrapper remoteServiceWrapper = new ShunTemplateRemoteServiceWrapper( this.attributes.getDataSource() );
    	  		remoteServiceWrapper.setShunTemplateRemote( (IShunTemplateRemote)contextAdapter.getBean( this.attributes.getServiceName() ) );
    	   		this.setShunTemplate( remoteServiceWrapper );

    		} catch ( Exception e ) {
     			throw new TriSystemException( SmMessageId.SM005201S , e );

    		}
    	}

    	if ( null == this.shunTemplate ) {
			throw new TriSystemException( SmMessageId.SM005202S );
    	}

        return shunTemplate;
    }

	/**
	 * XML名前空間を指定します。
	 * @param key キー値
	 * @param nameSpace 名前空間
	 */
	protected void addNameSpace( String key, String nameSpace ) {
		xmlNamespaceMap.put(key, nameSpace);
		opt.setLoadSubstituteNamespaces( xmlNamespaceMap );
	}

    /**
     * 指定されたキー情報に対応する名前空間を取得します。
     * @param key 名前空間を一意に識別するキー
     * @return キーに対応する名前空間
     */
    protected final String getNameSpace( String key ) {
        return (String)xmlNamespaceMap.get( key );
    }

    /**
     * 名前空間を示すパラメータを取得します。
     *
     * @return 名前空間を示すパラメータを戻します
     */
    protected final XmlOptions getXmlOptions() {
    	return opt;
    }

	/**
	 * 指定されたページ位置、表示行数より、ページ制御クラスを取得します。
	 * @param queryExp 検索式
     * @param pageNo ページ番号
     * @param viewRows 表示行数
	 * @return ページ制御を戻します。取得できない場合、nullを戻します。
	 * @throws SQLException Shunsaku Java APIの例外
	 */
    public final ILimit getPageLimit( String queryExp, int pageNo, int viewRows ) throws SQLException {
		return this.getPageLimit( queryExp, pageNo, viewRows, 0 );
	}

	/**
	 * 指定されたページ位置、表示行数より、ページ制御クラスを取得します。
     * <br>全体の表示最大件数＜＝０の場合、デフォルト値が適用されます。
	 * @param queryExp 検索式
     * @param pageNo ページ番号
     * @param viewRows 表示行数
     * @param maxHitLimit 全体の最大表示件数
	 * @return ページ制御を戻します。取得できない場合、nullを戻します。
	 * @throws SQLException Shunsaku Java APIの例外
	 */
    public final ILimit getPageLimit( String queryExp, int pageNo, int viewRows, int maxHitLimit ) throws SQLException {

		ILimit limit = new PageLimit();
		limit.setMaxRows( this.getShunTemplate().countQuery( queryExp, maxHitLimit, true ) );
		limit.setViewRows( viewRows );
		limit.getPageBar().setValue( pageNo );

		return ( viewRows == 0 )? null: limit;
	}

	/**
     * 検索件数を取得します。
     * @param condition 検索条件
     * @return 取得した検索結果を戻します。
     */
	public int getCount( ICondition condition ) {
		try {
			return this.getShunTemplate().countQuery( condition.toQueryString(), false );
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException( e.getMessage() );
		}
	}

	/**
     * 検索件数を取得します。
     * @param condition 検索条件
	 * @param isLimit ページ制御有りの件数かどうか。trueの場合、最大値はプロパティ指定値（HitCountLimit）となります
     * @return 取得した検索結果を戻します。
     */
	protected int getCount( ICondition condition, boolean isLimit ) {
		try {
			return this.getShunTemplate().countQuery( condition.toQueryString(), isLimit );
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException( e.getMessage() );
		}
	}

	/**
	 * XML文書の文字列から余分な文字を取り除きます。
	 * <BR>・要素の終端と要素の開始の間の空白（現在無効）
	 * <BR>・名前空間
	 *
	 * @param xml XMLBeansのDocumentオブジェクト
	 * @return 編集したXML文字列を戻します。
	 */
    protected final String replaceExcessString( XmlObject xml ) {
    	String nameSpace = xml.getDomNode().getFirstChild().getNamespaceURI();
		String replaceString = xml.toString();

		replaceString = replaceString.replaceAll( " xmlns=\"" + nameSpace + "\"", "" );

		return replaceString;
	}

    protected final ILog getLog() {
    	return log;
    }

}
