package jp.co.blueship.tri.fw.dao.orm;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.dao.orm.ex.TriJdbcDaoException;
import jp.co.blueship.tri.fw.msg.SmMessageId;

/**
 * Abstract class for JDBC based data access objects.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 */
public abstract class TriJdbcDaoSupport extends JdbcDaoSupport {
	/**
	 * JdbcTemplate
	 */
	private NamedParameterJdbcTemplate namedParamTemplate = null;

	/**
	 * DAOTemplate
	 */
	private IDaoTemplate daoTemplate;

	private class CountMapper implements ParameterizedRowMapper<Integer> {
	      @Override
	      public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
		    	return new Integer(rs.getInt("count"));
	      }
	}

    /**
     * JDBCを利用するためのSpring templateを取得します。
     * @return DAO template
     */
	protected final NamedParameterJdbcTemplate getTemplate() {
    	if ( null == this.namedParamTemplate ) {
			throw new TriJdbcDaoException( SmMessageId.SM005002S );
    	}

        return namedParamTemplate;
    }

	/**
	 * JDBCを利用するためのDAO templateが設定されます。
	 *
	 * @param daoTemplate DAO template
	 */
	public void setDaoTemplate( IDaoTemplate daoTemplate ) {
		this.daoTemplate = daoTemplate;
	}

    /**
     * JDBCを利用するためのDAO templateを取得します。
     * @return DAO template
     */
    protected final IDaoTemplate getDaoTemplate() {
    	if ( null == this.daoTemplate ) {
			throw new TriJdbcDaoException( SmMessageId.SM005003S );
    	}

        return daoTemplate;
    }

	/**
	 * Concrete subclasses can override this for custom initialization behavior.
	 * Gets called after population of this instance's bean properties.
	 *
	 * @throws Exception if DAO initialization fails (will be rethrown as a BeanInitializationException)
	 */
	@Override
	protected void initDao() {
		try {
		this.namedParamTemplate = new NamedParameterJdbcTemplate( this.getDataSource() );
		} catch ( Exception e ) {
			throw new TriJdbcDaoException( SmMessageId.SM005001S, e );
		}
	}

	/**
	 * 指定されたテーブルの件数を取得します。
	 * @param condition SQL conditon object
	 * @return 件数を取得します。
	 */
	public final int countQuery( ISqlCondition condition ) {
		if ( null == condition ) {
			return 0;
		}

		return this.countQuery(this.getDaoTemplate().toCountQuery( condition ), condition.getSqlParameter());
	}

	/**
	 * 指定されたテーブルの件数を取得します。
	 * @param sql SQL query to execute
	 * @param paramSource SQL parameter source
	 * @return 件数を取得します。
	 */
	public final int countQuery( String sql, SqlParameterSource paramSource ) {
		if ( TriStringUtils.isEmpty(sql) ) {
			return 0;
		}

		List<Integer> rows = this.getTemplate().query(
				sql,
				paramSource,
				new CountMapper()
				);

		return ( 0 == rows.size() )? 0: ((Integer)rows.get(0)).intValue();
	}

	/**
	 * 指定されたページ位置、表示行数より、ページ制御クラスを取得します。
     * @param pageNo ページ番号
     * @param viewRows 表示行数
	 * @param count 対象件数
	 * @return ページ制御を戻します。取得できない場合、nullを戻します。
	 */
	public final ILimit getPageLimit( Integer pageNo, Integer viewRows, int count ) {
		return this.getDaoTemplate().getPageLimit( pageNo, viewRows, count );
	}

	/**
	 * レコードの全項目を取得するSQL Queryを実行し、エンティティを取得します。
	 *
	 * @param rowMapper object that will map one object per row
	 * @param condition SQL conditon object
	 * @return the result List, containing mapped objects
	 */
	public <E> List<E> query( RowMapper<E> rowMapper, ISqlCondition condition ) {
		List<E> rows = this.getTemplate().query(
				this.getDaoTemplate().toSelectQuery(condition),
				condition.getSqlParameter(),
				rowMapper
		);

		return rows;
	}

	/**
	 * 指定されたSQL Queryを実行し、エンティティを取得します。
	 *
	 * @param sql SQL query to execute
	 * @param rowMapper object that will map one object per row
	 * @param condition SQL conditon object
	 * @return the result List, containing mapped objects
	 */
	public <E> List<E> query( String sql, RowMapper<E> rowMapper, ISqlCondition condition ) {
		return this.query(sql, rowMapper, condition.getSqlParameter());
	}

	/**
	 * 指定されたSQL Queryを実行し、エンティティを取得します。
	 *
	 * @param sql SQL query to execute
	 * @param rowMapper object that will map one object per row
	 * @param paramSource SQL parameter source
	 * @return the result List, containing mapped objects
	 */
	public <E> List<E> query( String sql, RowMapper<E> rowMapper, SqlParameterSource paramSource ) {
		List<E> rows = this.getTemplate().query(
				sql,
				paramSource,
				rowMapper
		);

		return rows;
	}

	/**
	 * 指定されたSQL Queryを実行し、エンティティを取得します。
	 *
	 * @param sql SQL query to execute
	 * @param rowMapper object that will map one object per row
	 * @return the result List, containing mapped objects
	 */
	public <E> List<E> query( String sql, RowMapper<E> rowMapper ) {
		List<E> rows = this.getTemplate().query(
				sql,
				rowMapper
		);

		return rows;
	}

	/**
	 * 指定されたSQL Queryを実行し、Updateを行います。
	 *
	 * @param sql SQL query to execute
	 * @param paramSource SQL parameter source
	 * @return the number of rows affected
	 */
	public int update( String sql, SqlParameterSource paramSource ) {
		int count =  this.getTemplate().update(sql, paramSource);

		return count;
	}

	/**
	 * 指定されたSQL Queryを実行し、Updateを行います。
	 * <br>大量のレコードを一度に更新するために使用します。
	 *
	 * @param sql SQL query to execute
	 * @param paramSourceList SQL parameter source list
	 * @return the number of rows affected
	 */
	public int[] update( String sql, List<SqlParameterSource> paramSourceList ) {
		int[] count =  this.getTemplate().batchUpdate(sql, paramSourceList.toArray(new SqlParameterSource[]{}));

		return count;
	}

	/**
	 * 指定された条件でSQL Queryを実行し、Deleteを行います。
	 *
	 * @param condition SQL conditon object
	 * @return the number of rows affected
	 */
	public int delete( ISqlCondition condition ) {
		String sql = this.getDaoTemplate().toDeleteQuery(condition);

		int count =  this.getTemplate().update(sql, condition.getSqlParameter());

		return count;
	}


}
