package jp.co.blueship.tri.fw.dao.orm.constants;

/**
 * Sql Sort Orderの列挙型です。
 * <br>
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 */
public enum TriSortOrder {
	/**
	 * 昇順
	 */
	Asc( "ASC" ),
	/**
	 * 降順
	 */
	Desc( "DESC" );

	private String value = null;

	private TriSortOrder( String value) {
		this.value = value;
	}

	public String value() {
		return this.value;
	}

}
