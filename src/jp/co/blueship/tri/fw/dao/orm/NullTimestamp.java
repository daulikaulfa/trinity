package jp.co.blueship.tri.fw.dao.orm;

import java.sql.Timestamp;

/**
 * The implements of IEntityNullItem interface.
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class NullTimestamp extends Timestamp implements IEntityNullItem {

	public NullTimestamp() {
		super(0);
	}

}
