package jp.co.blueship.tri.fw.dao.orm.shun;

import java.sql.SQLException;

import com.fujitsu.shun.ShunResultSet;

/**
 * 検索した結果を表現するクラスです。
 */
public class ShunResultSetWrapper implements java.io.Serializable {
	
	/**
	 * 修正したらインクリメントすること
	 */
	private static final long serialVersionUID = 1L;

	private ShunResultSet resultSet = null;
	
	/**
	 * ファイナライザーガーディアンによるShunsakuの初期化を行います。
	 */
	@SuppressWarnings("unused")
	private final Object finalizerGuardian = new Object() {
		protected void finalize() throws Throwable {
			close();
		}
	};
	
	/**
	 * ShunResultSetWrapperオブジェクトを作成します。
	 * 
	 * @param resultSet ShunResultSetオブジェクト
	 */
	public ShunResultSetWrapper(ShunResultSet resultSet) {
		this.resultSet= resultSet;
	}

	/**
	 * ShunResultSetオブジェクトを解放します。
	 * 
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	public void close() throws SQLException {
		if ( null != resultSet ) {
			resultSet.close();
		}
	}
	
	/**
	 * 次のデータに移動します。
	 * 
	 * @return 次のデータが存在する場合はtrue、それ以上データがない場合はfalse
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	public boolean next() throws SQLException {
		return resultSet.next();
	}
	
	/**
	 * 検索式、ダイレクトアクセスキーまたは指定したレコードIDに一致したデータのヒット件数を返却します。
	 * 
	 * @return 検索式、ダイレクトアクセスキーまたは指定したレコードIDに一致したデータのヒット件数
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	public int getHitCount() throws SQLException {
		return resultSet.getHitCount();
	}
	
	/**
	 * Shunsakuから取り出したデータの件数を返却します。
	 * 
	 * @return Shunsakuから取り出したデータの件数 
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	public int getReturnCount() throws SQLException {
		return resultSet.getReturnCount();
	}
	
	/**
	 * 現在のデータを、Stringとして返却します。
	 * 
	 * @return 検索されたデータ
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	public String getString() throws SQLException {
		return resultSet.getString();
	}
	
	/**
	 * 現在のXML文書のレコードIDを返却します。
	 * 
	 * @return レコードID
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	public String getRecordID() throws SQLException {
		return resultSet.getRecordID();
	}
	
	/**
	 * ヒット件数が上限を超えているかどうかの状態を返却します。
	 * @return ヒット件数が上限を超えた場合はtrue、そうでない場合はfalseを戻します。
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	public boolean isHitCountLimitOver() throws SQLException {
		return resultSet.isHitCountLimitOver();
	}
	
	/**
	 * setRequestメソッドにデータの取得位置、返信要求件数および取り出し方向を指定します。
	 * 前のページを表示する検索処理では、前回検索時の先頭位置情報をgetFirstPositionメソッドで取得しておき、
	 * データの取得位置（取得終了位置）に指定します。この指定により前回取得したデータの前の部分のデータを取得します。
	 * @return 先頭位置情報を戻します。
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	public String getFirstPosition() throws SQLException {
		return resultSet.getFirstPosition();
	}
	
	/**
	 * setRequestメソッドにデータの取得位置、返信要求件数および取り出し方向を指定します。
	 * 次のページを表示する検索処理では、前回検索時の最終位置情報をgetLastPositionメソッドで取得しておき、
	 * データの取得位置（取得開始位置）に指定します。この指定により前回取得したデータの続きのデータを取得します。
	 * @return 先頭位置情報を戻します。
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	public String getLastPosition() throws SQLException {
		return resultSet.getLastPosition();
	}
	
}
