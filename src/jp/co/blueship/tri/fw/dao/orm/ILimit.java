package jp.co.blueship.tri.fw.dao.orm;

import java.util.List;


/**
 * 検索結果のページ制御を行うインタフェースです。
 * <br>行を抽出するSQLのLIMIT文を取得出来ます。
 *
 *
 * @version V3L10R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public interface ILimit extends IEntity {



	/**
	 * 行を抽出するSQLのLIMIT文を取得します。
	 * @return 取得した文字列を戻します。
	 */
	public String getSqlLimit();

	/**
	 * 指定された対象のリストから、抽出対象を絞り込み、結果を戻します。
	 * @param list 抽出対象のリスト
	 * @return 抽出済みのリストを戻します。
	 */
	public <T> List<T> getLimit( List<T> list );

	/**
	 * ページ制御を行うバーを取得します。
	 * @return 取得したベージバーを戻します。
	 */
	public PageBar getPageBar();

	/**
	 * 画面に表示する行数を設定します。
	 * @param rows 表示行数
	 */
	public void setViewRows( int rows );

	/**
	 * 画面に表示する行数を取得します。
	 * @return 表示行数
	 */
	public int getViewRows();

	/**
	 * 検索結果の行数を設定します。
	 * @param rows 検索結果の行数
	 */
	public void setMaxRows( int rows );

	/**
	 * 検索結果の行数を取得します。
	 * @return 検索結果の行数
	 */
	public int getMaxRows();

}
