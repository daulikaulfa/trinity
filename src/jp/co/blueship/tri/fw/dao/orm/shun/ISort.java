package jp.co.blueship.tri.fw.dao.orm.shun;



/**
 * テーブルのソート条件です。
 *
 */
public interface ISort {

	

	public static final String ASC = "";
	public static final String DESC = "DESC";

	/**
	 * ソート条件を取得します。
	 *
	 * @return ソート条件を戻します。
	 */
	public String toSortString();

	/**
	 * ソート条件を設定します。
	 *
	 * @param elementName ソート項目の要素名
	 * @param sort ISort.ASC、またはISort.DESC
	 * @param seq ソート順序
	 */
	public void setElement(String elementName, String sort, int seq);

}
