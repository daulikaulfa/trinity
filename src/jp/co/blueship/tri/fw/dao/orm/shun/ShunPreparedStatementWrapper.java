package jp.co.blueship.tri.fw.dao.orm.shun;

import java.io.InputStream;
import java.sql.SQLException;

import com.fujitsu.shun.ShunPreparedStatement;

/**
 * 検索式によるデータの検索、およびXML文書の追加をします。
 * <br>また、ソート式によるデータのソートおよびデータの集計をします。
 */
public class ShunPreparedStatementWrapper {
	
	private ShunPreparedStatement pstmt = null;
	
	public static int SHUN_DIRECTION_FORWARD_EXCLUSIVE = ShunPreparedStatement.SHUN_DIRECTION_FORWARD_EXCLUSIVE;
	public static int SHUN_DIRECTION_BACKWARD_EXCLUSIVE = ShunPreparedStatement.SHUN_DIRECTION_BACKWARD_EXCLUSIVE;
	
	/**
	 * ファイナライザーガーディアンによるShunsakuの初期化を行います。
	 */
	@SuppressWarnings("unused")
	private final Object finalizerGuardian = new Object() {
		protected void finalize() throws Throwable {
			close();
		}
	};
	
	/**
	 * ShunPreparedStatementWrapperオブジェクトを作成します。
	 * 
	 * @param pstmt ShunPreparedStatementオブジェクト
	 */
	public ShunPreparedStatementWrapper(ShunPreparedStatement pstmt) {
		this.pstmt= pstmt;
	}

	/**
	 * ShunPreparedStatementオブジェクトを解放します。
	 * 
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	public void close() throws SQLException {
		if ( null != pstmt ) {
			pstmt.close();
		}
	}
	
    /**
     * XML文書を、 ShunPreparedStatementオブジェクトにUnicode(UTF-8)のInputStreamで追加します。 
     * 
     * @param inputstream XML文書
     * @throws SQLException Shunsaku Java APIの例外
     */
    public void add(InputStream inputstream) throws SQLException {
    	pstmt.add(inputstream);
	}

	/**
	 * XML文書を、ShunPreparedStatementオブジェクトにStringで追加します。
	 * 
	 * @param s XML文書
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	public void add(String s) throws SQLException {
		pstmt.add(s);
	}
	
    /**
     * addメソッドで設定したXML文書を、Shunsakuに追加します。
     * 
     * @return 0
     * @throws SQLException Shunsaku Java APIの例外
     */
    public int executeInsert() throws SQLException {
		return pstmt.executeInsert();
	}
    
    /**
     * データの検索や集計時に使用します。
     * <br>一致したデータの何件目から返信するか、および返信されるデータの最大件数を何件にするかを指定します。
     * <br>返信されるデータの最大件数は、conductor用動作環境ファイルのAnsMaxで設定した値になります。
     * 
     * @param position 検索式に一致したデータの何件目から返信するか。1以上の値を指定します。
     * @param requestCount 返信されるデータの最大件数を何件にするか。0を指定した場合は、結果をすべて取得します。
     * @throws SQLException Shunsaku Java APIの例外
     */
    public void setRequest(int position, int requestCount) throws SQLException {
		pstmt.setRequest(position, requestCount);
	}
    
    /**
     * ソート条件を指定して1,000件以上の結果を取得する際に使用します。
     * 
     * @param pos Shunsakuから戻される先頭位置、または最終位置を指定します。
     * @param requestCount 返信されるデータの最大件数を何件にするか。0を指定した場合は、結果をすべて取得します。
     * @param direction 前ページ{@see ShunPreparedStatementWrapper.SHUN_DIRECTION_BACKWARD_EXCLUSIVE}／次ページ{@see ShunPreparedStatementWrapper.SHUN_DIRECTION_FORWARD_EXCLUSIVE}を指定します。
     * @throws SQLException Shunsaku Java APIの例外
     */
    public void setRequest(String pos, int requestCount, int direction) throws SQLException {
		pstmt.setRequest(pos, requestCount, direction);
	}
    
    /**
     * ソート／集計時のヒット件数の上限値を返却します。
     * 
     * @return ソート／集計実行時にヒット件数の上限値。
     * @throws SQLException Shunsaku Java APIの例外
     */
    public int getHitCountLimit() throws SQLException {
		return pstmt.getHitCountLimit();
	}
    
    /**
     * ソート／集計実行時にヒット件数の上限値を指定します。
     * 
     * @param limit limit - ソート／集計実行時にヒット件数の上限値を指定します。
     * @throws SQLException Shunsaku Java APIの例外
     */
    public void setHitCountLimit(int limit) throws SQLException {
		pstmt.setHitCountLimit(limit);
	}
    
    /**
     * ソート式を指定します。
     * 
     * @param sortExp ソート式
     * @throws SQLException Shunsaku Java APIの例外
     */
    public void setSort(String sortExp) throws SQLException {
		pstmt.setSort(sortExp);
	}
    
    /**
     * データの検索や集計を実行し、実行した結果を格納するためのShunResultSetWrapperオブジェクトを作成します。
     * 
     * @return ShunResultSetWrapperオブジェクト
     * @throws SQLException Shunsaku Java APIの例外
     */
    public ShunResultSetWrapper executeSearch() throws SQLException {
    	return new ShunResultSetWrapper(pstmt.executeSearch());
    }


}
