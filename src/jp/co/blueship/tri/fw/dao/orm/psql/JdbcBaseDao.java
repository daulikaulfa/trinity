package jp.co.blueship.tri.fw.dao.orm.psql;

import static jp.co.blueship.tri.fw.cmn.utils.TriDateUtils.*;
import static jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.cmn.utils.function.TriClojure;
import jp.co.blueship.tri.fw.dao.orm.EntityFooter;
import jp.co.blueship.tri.fw.dao.orm.EntityLimit;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;
import jp.co.blueship.tri.fw.dao.orm.ILimit;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.ISqlCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.TriJdbcDaoSupport;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.ex.TriJdbcDaoException;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.security.TriAuthenticationContext;

/**
 * Abstract class for DAO based objects.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 */
public abstract class JdbcBaseDao<E> extends TriJdbcDaoSupport implements IJdbcDao<E> {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public int count(ISqlCondition condition) throws TriJdbcDaoException {
		try {
			return super.countQuery(setConditionByJoinAccsHist(condition));

		} catch (Exception e) {
			throw new TriJdbcDaoException(SmMessageId.SM005004S, e, this.getTableAttribute().name());
		}
	}

	@Override
	public E findByPrimaryKey(ISqlCondition condition) throws TriJdbcDaoException {
		try {
			/**
			 * 通常の検索ではum_accs_histテーブルにjoinをするためのkeyの設定を行う{@link #setConditionByJoinAccsHist(ISqlCondition condition)}
			 * を実行する必要があるが、primary key検索の場合パフォーマンスを考慮しum_accs_histテーブルにjoinしないため、
			 * {@link #setConditionByJoinAccsHist(ISqlCondition condition)}を実行しない。<br>
			 *
			 * um_accs_histテーブルにjoinをしたい場合はfindメソットを使用する。
			 */

			List<E> rows = super.query(new ParameterizedRowMapper<E>() {
				@Override
				public E mapRow(ResultSet resultset, int i) throws SQLException {
					return mapping(resultset, i);
				}
			}, condition);

			return (0 == rows.size()) ? null : (E) rows.get(0);

		} catch (Exception e) {
			throw new TriJdbcDaoException(SmMessageId.SM005004S, e, this.getTableAttribute().name());
		}
	}

	@Override
	public List<E> find(ISqlCondition condition, ISqlSort sort) throws TriJdbcDaoException {
		try {
			setConditionByJoinAccsHist(condition);
			String sql = this.getDaoTemplate().toSelectQuery(condition) + SqlFormatUtils.toSortString(sort);

			List<E> rows = super.query(sql, new ParameterizedRowMapper<E>() {
				@Override
				public E mapRow(ResultSet resultset, int i) throws SQLException {
					return mapping(resultset, i);
				}
			}, condition);

			return rows;

		} catch (Exception e) {
			throw new TriJdbcDaoException(SmMessageId.SM005004S, e, this.getTableAttribute().name());
		}
	}

	@Override
	public IEntityLimit<E> find(ISqlCondition condition, ISqlSort sort, int pageNo, int viewRows, int maxHitLimit) throws TriJdbcDaoException {
		try {
			int count = this.count(condition);
			if (maxHitLimit < count){
				count = maxHitLimit;
			}
			ILimit limit = this.getPageLimit(pageNo, viewRows, count);

			String sql = this.getDaoTemplate().toSelectQuery(condition) + SqlFormatUtils.toSortString(sort)
					+ ((null == limit) ? "" : limit.getSqlLimit());
			List<E> rows = super.query(sql, new ParameterizedRowMapper<E>() {
				@Override
				public E mapRow(ResultSet resultset, int i) throws SQLException {
					return mapping(resultset, i);
				}
			}, condition);

			IEntityLimit<E> home = new EntityLimit<E>();
			home.setLimit(limit);
			home.setEntities(rows);

			return home;

		} catch (Exception e) {
			throw new TriJdbcDaoException(SmMessageId.SM005004S, e, this.getTableAttribute().name());
		}
	}

	@Override
	public IEntityLimit<E> find(ISqlCondition condition, ISqlSort sort, int pageNo, int viewRows) throws TriJdbcDaoException {

		try {
			return this.find(condition, sort, pageNo, viewRows, this.count(condition));
		} catch (Exception e) {
			throw new TriJdbcDaoException(SmMessageId.SM005004S, e, this.getTableAttribute().name());
		}
	}

	@Override
	public List<E> find(ISqlCondition condition) throws TriJdbcDaoException {
		try {
			return this.find(condition, null);
		} catch (Exception e) {
			throw new TriJdbcDaoException(SmMessageId.SM005004S, e, this.getTableAttribute().name());
		}
	}

	@Override
	public IEntityLimit<E> find(ISqlCondition condition, int pageNo, int viewRows) throws TriJdbcDaoException {
		try {
			return this.find(condition, null, pageNo, viewRows);
		} catch (Exception e) {
			throw new TriJdbcDaoException(SmMessageId.SM005004S, e, this.getTableAttribute().name());
		}
	}

	@Override
	public int insert(E entity) throws TriJdbcDaoException {
		try {
			Timestamp timestamp = getSystemTimestamp();
			setFooterOnInsert(entity, timestamp);
			setFooterOnUpdate(entity, timestamp);
			InsertBuilder builder = new InsertBuilder(this.getTableAttribute());
			this.append(builder, entity);

			return super.update(builder.toQueryString(), builder.getSqlParameter());
		} catch (Exception e) {
			throw new TriJdbcDaoException(SmMessageId.SM005005S, e, this.getTableAttribute().name());
		}
	}

	@Override
	public int[] insert(List<E> entities) throws TriJdbcDaoException {
		try {

			if (TriCollectionUtils.isEmpty(entities)) {
				return new int[0];
			}

			InsertBuilder builder = new InsertBuilder(this.getTableAttribute());
			forEach(entities, buildDmlOfInsertFrom(builder, getSystemTimestamp()));

			return super.update(builder.toQueryString(), builder.getSqlParameterList());
		} catch (Exception e) {
			throw new TriJdbcDaoException(SmMessageId.SM005005S, e, this.getTableAttribute().name());
		}
	}

	@Override
	public int update(E entity) throws TriJdbcDaoException {
		try {

			setFooterOnUpdate(entity, getSystemTimestamp());
			UpdateBuilder builder = new UpdateBuilder(this.getTableAttribute());
			this.append(builder, entity);

			return super.update(builder.toQueryString(), builder.getSqlParameter());
		} catch (Exception e) {
			throw new TriJdbcDaoException(SmMessageId.SM005006S, e, this.getTableAttribute().name());
		}
	}

	@Override
	public int[] update(List<E> entities) throws TriJdbcDaoException {
		try {
			if (TriCollectionUtils.isEmpty(entities)) {
				return new int[0];
			}

			int[] result = new int[entities.size()];
			int i = 0;

			for (E entity : entities) {
				result[i++] = this.update(entity);
			}

			return result;

			// 一括更新時に更新項目がレコードごとに不揃いだとParameterがそろわずにErrorとなる
			// BatchUpdateは元々そういう機能であるが、本製品で利用したい機能と微妙に合わないため未使用とする

			// UpdateBuilder builder = new
			// UpdateBuilder(this.getTableAttribute());
			// forEach(entities, buildDmlOfUpdateFrom(builder,
			// getSystemTimestamp(), authUserID()));

			// return super.update(builder.toQueryString(),
			// builder.getSqlParameterList());
		} catch (Exception e) {
			throw new TriJdbcDaoException(SmMessageId.SM005006S, e, this.getTableAttribute().name());
		}
	}

	@Override
	public int update(ISqlCondition condition, E entity) throws TriJdbcDaoException {
		try {

			setFooterOnUpdate(entity, getSystemTimestamp());
			UpdateBuilder builder = new UpdateBuilder(this.getTableAttribute());
			builder.setCondition(condition);
			this.append(builder, entity);

			builder.add((MapSqlParameterSource) condition.getSqlParameter());
			return super.update(builder.toQueryString(), builder.getSqlParameter());
		} catch (Exception e) {
			throw new TriJdbcDaoException(SmMessageId.SM005006S, e, this.getTableAttribute().name());
		}
	}

	@Override
	public int delete(ISqlCondition condition) throws TriJdbcDaoException {
		if (null == condition)
			return 0;

		try {
			int count = super.delete(condition);

			return count;
		} catch (Exception e) {
			throw new TriJdbcDaoException(SmMessageId.SM005007S, e, this.getTableAttribute().name());
		}
	}

	private TriClojure<E> buildDmlOfInsertFrom(final InsertBuilder builder, final Timestamp nowDate) {

		return new TriClojure<E>() {

			@Override
			public boolean apply(E entity) {
				setFooterOnInsert(entity, nowDate);
				setFooterOnUpdate(entity, nowDate);
				append(builder, entity);
				builder.add();
				return true;
			}

		};
	}

	//
	// private TriClojure<E> buildDmlOfUpdateFrom(final UpdateBuilder builder,
	// final Timestamp nowDate, final String authUserID) {
	//
	// return new TriClojure<E>() {
	//
	// @Override
	// public boolean apply(E entity) {
	// setFooterOnUpdate(entity, nowDate);
	// append(builder, entity);
	// builder.add();
	// return true;
	// }
	//
	// };
	// }

	private ISqlCondition setConditionByJoinAccsHist( ISqlCondition condition ) {
		if ( ! condition.isJoinAccsHist() ) {
			return condition;
		}

		condition.setKeyByJoinAccsHist( authUserID() );
		return condition;
	}

	private void setFooterOnInsert(E entity, Timestamp nowDate) {

		if (!hasEntityFooter(entity)) {
			return;
		}

		String authUserID = ((EntityFooter) entity).getRegUserId();
		if (authUserID == null) {
			authUserID = authUserID();
		}

		setRegTimestampIfEmpty((EntityFooter) entity, nowDate);
		setRegUserName((EntityFooter) entity);
		((EntityFooter) entity).setRegUserId(authUserID);
	}

	private void setFooterOnUpdate(E entity, Timestamp nowDate) {

		if (!hasEntityFooter(entity)) {
			return;
		}

		String authUserID = ((EntityFooter) entity).getRegUserId();
		if (authUserID == null) {
			authUserID = authUserID();
		}

		setUpdTimestampIfEmpty((EntityFooter) entity, nowDate);
		setUpdUserName((EntityFooter) entity);
		((EntityFooter) entity).setUpdUserId(authUserID);
	}

	private void setRegTimestampIfEmpty(EntityFooter entity, Timestamp nowDate) {

		if (null == entity.getRegTimestamp() || entity.isForReading()) {
			entity.setRegTimestamp(nowDate);
		}
	}

	private void setRegUserName(EntityFooter entity) {
		entity.setRegUserNm(authUserName());
	}

	private void setUpdTimestampIfEmpty(EntityFooter entity, Timestamp nowDate) {

		if (null == entity.getUpdTimestamp() || entity.isForReading()) {
			entity.setUpdTimestamp(nowDate);
		}
	}

	private void setUpdUserName(EntityFooter entity) {
		entity.setUpdUserNm(authUserName());
	}

	private boolean hasEntityFooter(E entity) {
		return entity instanceof EntityFooter;
	}

	protected String authUserID() {
		String userId = null;
		if( triAuthenticationContext().isAuthenticated() ) {
			userId = triAuthenticationContext().getAuthUserID();
		}
		return userId;
	}

	protected String authUserName() {
		String userName = null;
		if( triAuthenticationContext().isAuthenticated() ) {
			userName = triAuthenticationContext().getAuthUserName();
		}
		return userName;
		// return
		// DesignSheetFactory.getDesignSheet().getValue(UmDesignBeanId.userId,
		// authUserID);
	}

	private static TriAuthenticationContext triAuthenticationContext() {
		return (TriAuthenticationContext) ContextAdapterFactory.//
				getContextAdapter().//
				getBean(TriAuthenticationContext.BEAN_NAME);
	}
	protected abstract ITableAttribute getTableAttribute();

	protected abstract ISqlBuilder append(ISqlBuilder builder, E entity);

	/**
	 * JDBCの検索結果を任意のオブジェクトにマッピングします。 <br>
	 *
	 * @param resultSet 検索結果
	 * @param row 検索結果行
	 * @return マッピングしたオブジェクトを戻します
	 * @throws TriJdbcDaoException
	 */
	protected E mapping(ResultSet resultSet, int row) throws SQLException {
		E entity = this.entityMapping(resultSet, row);
		if (hasEntityFooter(entity)) {
			EntityFooter footer = (EntityFooter) entity;
			footer.setForReading(true);
		}

		return entity;
	}

	/**
	 * JDBCの検索結果を任意のオブジェクトにマッピングします。 <br>
	 * このコールバック処理を、高速化したい場合は、リフレクションを使用せずに 手動でマッピングを行う事で、若干の性能向上が期待できます。
	 *
	 * @param resultSet 検索結果
	 * @param row 検索結果行
	 * @return マッピングしたオブジェクトを戻します
	 * @throws TriJdbcDaoException
	 */
	protected abstract E entityMapping(ResultSet resultSet, int row) throws SQLException;

}
