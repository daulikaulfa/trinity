package jp.co.blueship.tri.fw.dao.orm.constants;

/**
 * The interface of of the enum constant for status-id.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 */
public interface IStatusId {

	/**
	 * Returns the status-id of this enum constant, exactly as declared in its enum declaration.
	 *
	 * @return the item name of this enum constant
	 */
	public String getStatusId();

	/**
	 *
	 * @return the key of message source
	 */
	public String getMessageKey();

}
