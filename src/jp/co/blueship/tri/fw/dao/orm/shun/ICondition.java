package jp.co.blueship.tri.fw.dao.orm.shun;

/**
 * 条件検索から検索条件を生成するためのインタフェースです。
 *
 */
public interface ICondition {
	
	/**
	 * 指定された検索条件から、DB用の検索条件を生成します。
	 * 
	 * @return DBの検索条件を戻します。
	 */
	public String toQueryString();
}
