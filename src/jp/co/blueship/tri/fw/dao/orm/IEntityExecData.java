package jp.co.blueship.tri.fw.dao.orm;


/**
 * 業務テーブルのエンティティが継承する必要のあるインタフェースです。
 * <br>“SM - 実行中データのステータス”で管理対象のエンティティで実装します。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public interface IEntityExecData extends IEntity {

	/**
	 * process status-idを取得します。
	 *
	 * @return process status-id
	 */
	public String getProcStsId();

}
