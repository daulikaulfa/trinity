package jp.co.blueship.tri.fw.dao.orm.psql;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.dao.orm.ILimit;
import jp.co.blueship.tri.fw.dao.orm.PageBar;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.SmMessageId;

/**
 * 検索結果のページ制御を行う実装です。
 * <br>MySQLに依存するSQLのLIMIT文を取得します。
 * <br>PostgreSQL等にも、LIMIT文がありますが、微妙な文法の違いがあります。
 *
 *
 * @version V3L10R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class PageLimit implements ILimit {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 1L;

	private PageBar bar = null;
	private int viewRows = 0;
	private int maxRows = 0;

	@Override
	public final String getSqlLimit() {
		if ( null == bar )
			return "";

		if ( 0 == bar.getValue() || 0 == viewRows )
			return "";

		return SqlFormatUtils.getSqlLimit(
							( 1 == bar.getValue() ) ? 0: ((bar.getValue() - 1) * viewRows),
							viewRows );
	}

	@Override
	public final <T> List<T> getLimit( List<T> list ) {
		if ( null == bar ){
			throw new TriSystemException( SmMessageId.SM005018S );
		}
		List<T> outList = new ArrayList<T>();

		int offset = (1 == bar.getValue() ) ? 0: ((bar.getValue() - 1) * viewRows);
		int i = 0;

		for ( T obj: list ) {
			if ( offset <= i ) {
				if ( viewRows <= outList.size() )
					break;

				outList.add( obj );
			}

			i++;
		}

		return outList;
	}

	@Override
	public final PageBar getPageBar() {
		if ( null == bar ){
			throw new TriSystemException( SmMessageId.SM005018S );
		}
		return bar;
	}

	/**
	 * 画面に表示する行数を設定します。
	 * @param rows 表示行数
	 */
	public final void setViewRows( int rows ) {
		this.viewRows = rows;
		this.adjustPageBar();
	}

	/**
	 * 画面に表示する行数を取得します。
	 * @return 表示行数
	 */
	public final int getViewRows() {
		return this.viewRows;
	}

	/**
	 * 検索結果の行数を設定します。
	 * @param rows 検索結果の行数
	 */
	public final void setMaxRows( int rows ) {
		this.maxRows = rows;
		this.adjustPageBar();
	}

	/**
	 * 検索結果の行数を取得します。
	 * @return 検索結果の行数
	 */
	public final int getMaxRows() {
		return this.maxRows;
	}

	/**
	 * ページ制御バーの最大値を調整します。
	 */
	private final void adjustPageBar() {
		if ( null == bar )
			bar = new PageBar();

		if ( 0 == viewRows || 0 == maxRows )
			return;

		bar.setMaximum( (int)Math.round(Math.ceil( (double)maxRows / (double)viewRows )) );
	}

}
