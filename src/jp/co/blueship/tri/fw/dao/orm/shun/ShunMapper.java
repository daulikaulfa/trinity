package jp.co.blueship.tri.fw.dao.orm.shun;

import java.sql.SQLException;

/**
 * 検索した結果をオブジェクトにUnmarshallするための、コールバックインタフェースです。
 *
 */
public interface ShunMapper extends java.io.Serializable {

	

	/**
	 * Shunsakuの検索結果を任意のオブジェクトにマッピングします。
	 * <br>このコールバック処理を、高速化したい場合は、リフレクションを使用せずに
	 * 手動でマッピングを行う事で、若干の性能向上が期待できます。
	 *
	 * @param resultSet 検索結果
	 * @param row 検索結果行
	 * @return マッピングしたオブジェクトを戻します
	 * @throws SQLException  DAOの例外
	 */
	public Object mapRow( ShunResultSetWrapper resultSet, int row ) throws SQLException;
}
