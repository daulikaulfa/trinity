package jp.co.blueship.tri.fw.dao.orm.constants;

/**
 * The interface of the table attribute.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 */
public interface ITableItem {

	/**
	 * Returns the item name of this enum constant, exactly as declared in its enum declaration.
	 *
	 * @return the item name of this enum constant
	 */
	public String getItemName();

	/**
	 * Returns the name of this enum constant, exactly as declared in its enum declaration.
	 *
	 * @return the name of this enum constant
	 */
	public String name();

}
