package jp.co.blueship.tri.fw.dao.orm;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;


/**
 * テーブルのソート条件です。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 */
public interface ISqlSort {
	/**
	 * ソート条件を取得します。
	 *
	 * @return ソート条件を戻します。
	 */
	public String toSortString();

	/**
	 * ソート条件を設定します。
	 *
	 * @param element ソート項目の要素
	 * @param sort ISort.ASC、またはISort.DESC
	 * @param seq ソート順序
	 * @return a reference to this instance, so it's possible to chain several calls together
	 */
	public ISqlSort setElement(ITableItem element, TriSortOrder sort, int seq);

	/**
	 * 最新のSequence numberを取得します。
	 *
	 * @return 最新のSequence number
	 */
	public int getLatestSeq();
}
