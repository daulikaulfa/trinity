package jp.co.blueship.tri.fw.dao.orm.shun;

import java.io.InputStream;
import java.sql.SQLException;

import com.fujitsu.shun.ShunPreparedKey;

/**
 * ダイレクトアクセスキーによるデータの検索、XML文書の更新およびXML文書の削除をします。
 */
public class ShunPreparedKeyWrapper {
	
	private ShunPreparedKey pstmt = null;
	
	/**
	 * ファイナライザーガーディアンによるShunsakuの初期化を行います。
	 */
	@SuppressWarnings("unused")
	private final Object finalizerGuardian = new Object() {
		protected void finalize() throws Throwable {
			close();
		}
	};
	
	/**
	 * ShunPreparedKeyWrapperオブジェクトを作成します。
	 * 
	 * @param pstmt ShunPreparedKeyオブジェクト
	 */
	public ShunPreparedKeyWrapper(ShunPreparedKey pstmt) {
		this.pstmt= pstmt;
	}

	/**
	 * ShunConnectionオブジェクトを解放します。
	 * 
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	public void close() throws SQLException {
		if ( null != pstmt ) {
			pstmt.close();
		}
	}

	/**
	 * 検索または削除対象のダイレクトアクセスキーをShunPreparedKeyオブジェクトに追加します。
	 * 
	 * @param key ダイレクトアクセスキー
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	public void add(String key) throws SQLException {
		pstmt.add(key);
	}
	
    /**
     * 更新対象のダイレクトアクセスキーをStringで、更新後の内容となるXML文書を
     * Unicode(UTF-8)のInputStreamでShunPreparedKeyオブジェクトに追加します。
     * 
     * @param key ダイレクトアクセスキー
     * @param updateData XML文書
     * @throws SQLException Shunsaku Java APIの例外
     */
    public void add(String key, InputStream updateData) throws SQLException {
    	pstmt.add(key, updateData);
	}
	
    /**
     * 更新対象のダイレクトアクセスキーおよび更新後の内容となるXML文書を
     * StringでShunPreparedKeyオブジェクトに追加します。
     * 
     * @param key ダイレクトアクセスキー
     * @param updateData XML文書
     * @throws SQLException Shunsaku Java APIの例外
     */
    public void add(String key, String updateData) throws SQLException {
    	pstmt.add(key, updateData);
	}
	
    /**
     * addメソッドで追加したダイレクトアクセスキーに対応するXML文書をすべて削除します。
     * 
     * @return 0
     * @throws SQLException Shunsaku Java APIの例外
     */
    public int deleteByKey() throws SQLException {
    	return pstmt.deleteByKey();
	}
	
    /**
     * 指定したダイレクトアクセスキーが、addメソッドで追加されているかどうかを判定します。
     * 
     * @param key ダイレクトアクセスキー
     * @throws SQLException Shunsaku Java APIの例外
     */
    public void exists(String key) throws SQLException {
    	pstmt.exists(key);
	}
	
    /**
     * addメソッドで追加したダイレクトアクセスキーの数を返却します。
     * 
     * @throws SQLException Shunsaku Java APIの例外
     */
    public void getCount() throws SQLException {
    	pstmt.getCount();
	}
	
    /**
     * addメソッドで追加したダイレクトアクセスキーに対応するデータを検索し、
     * 検索した結果を格納するための ShunResultSetオブジェクトを作成します。
     * 
     * @return ShunResultSetオブジェクト
     * @throws SQLException Shunsaku Java APIの例外
     */
    public  ShunResultSetWrapper searchByKey() throws SQLException {
    	return new ShunResultSetWrapper(pstmt.searchByKey());
	}
	
    /**
     * ダイレクトアクセスキーの比較方法を指定します。
     * <br>本メソッドの呼出しが省略された場合、SHUN_KEY_COMPLETE_MATCHが指定されたとみなします。
     * 
     * @param searchType ダイレクトアクセスキーの比較方法 
     * @throws SQLException Shunsaku Java APIの例外
     */
    public void setSearchType(int searchType) throws SQLException {
    	pstmt.setSearchType(searchType);
	}
	
    /**
     * addメソッドで追加したダイレクトアクセスキーに対応するXML文書をすべて更新します。
     * 
     * @return 0
     * @throws SQLException Shunsaku Java APIの例外
     */
    public int updateByKey() throws SQLException {
    	return pstmt.updateByKey();
	}
 

}
