package jp.co.blueship.tri.fw.dao.orm;

import jp.co.blueship.tri.fw.dao.orm.ex.TriJdbcDaoException;


/**
 * 自動採番を行うためのアクセスインタフェースです。
 *
 */
public interface INumberingDao extends IDao {

	

	/**
	 * 自動採番します。
	 *
	 * @return 採番された値を戻します
	 */
	public String nextval() throws TriJdbcDaoException;

}
