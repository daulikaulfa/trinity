package jp.co.blueship.tri.fw.dao.orm;

/**
 * The interface for Null Object.
 * <br>Class that implements this interface is automatically set to a null value at the time of database update.
 *
 * <br>Entityの属性値でNULLを識別されるためのインタフェース。
 * <br>このインタフェースをimplementsしたクラスはデータベース更新時にnull値が自動的に設定されます。
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public interface IEntityNullItem {

}
