package jp.co.blueship.tri.fw.dao.orm.shun;

import java.io.InputStream;
import java.sql.SQLException;

import com.fujitsu.shun.ShunPreparedRecordID;

/**
 * レコードIDによるXML文書の検索、XML文書の更新およびXML文書の削除をします。 
 */
public class ShunPreparedRecordIDWrapper {

	private ShunPreparedRecordID pRecordID = null;
	
	/**
	 * ファイナライザーガーディアンによるShunsakuの初期化を行います。
	 */
	@SuppressWarnings("unused")
	private final Object finalizerGuardian = new Object() {
		protected void finalize() throws Throwable {
			close();
		}
	};
	
	/**
	 * ShunPreparedRecordIDWrapperオブジェクトを作成します。
	 * 
	 * @param pRecordID ShunPreparedRecordIDオブジェクト
	 */
	public ShunPreparedRecordIDWrapper(ShunPreparedRecordID pRecordID) {
		this.pRecordID= pRecordID;
	}

	/**
	 * ShunPreparedRecordIDオブジェクトを解放します。
	 * 
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	public void close() throws SQLException {
		if ( null != pRecordID ) {
			pRecordID.close();
		}
	}
	
	/**
	 * addメソッドで追加したレコードIDに対応するXML文書をすべて削除します。
	 * 
	 * @return 0
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	public int deleteByRecordID() throws SQLException {
		return pRecordID.deleteByRecordID();
	}
	
	/**
	 * addメソッドで追加したレコードIDに対応するXML文書をすべて更新します。
	 * 
	 * @return 0
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	public int updateByRecordID() throws SQLException {
		return pRecordID.updateByRecordID();
	}
	
	/**
	 * 検索または削除対象のレコードIDをShunPreparedRecordIDオブジェクトに追加します。
	 * 
	 * @param recordID レコードID
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	public void add(String recordID) throws SQLException {
		pRecordID.add(recordID);
	}
	
	/**
	 * 更新対象のレコードIDをStringで、更新後の内容となるXML文書をUnicode(UTF-8)の
	 * InputStreamで追加します。
	 * 
	 * @param recordID レコードID
	 * @param updateData XML文書
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	public void add(String recordID, String updateData) throws SQLException {
		pRecordID.add(recordID, updateData);
	}
	
	/**
	 * 更新対象のレコードIDおよび更新後の内容となるXML文書をStringで追加します。
	 * 
	 * @param recordID レコードID
	 * @param updateData XML文書
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	public void add(String recordID, InputStream updateData) throws SQLException {
		pRecordID.add(recordID, updateData);
	}
	
	/**
	 * addメソッドで追加したレコードIDの数を返却します。
	 * 
	 * @return addメソッドで追加されたレコードIDの数
	 * @throws SQLException Shunsaku Java APIの例外
	 */
	public int getCount() throws SQLException {
		return pRecordID.getCount();
	}
	
}