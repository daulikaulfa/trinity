package jp.co.blueship.tri.fw.dao.orm;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.fw.dao.orm.ex.TriJdbcDaoException;

/**
 * The interface of the JDBC data call back object.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public interface IJdbcMapping<E> {

	/**
	 * JDBCの検索結果を任意のオブジェクトにマッピングします。 <br>
	 * このコールバック処理を、高速化したい場合は、リフレクションを使用せずに 手動でマッピングを行う事で、若干の性能向上が期待できます。
	 *
	 * @param resultSet 検索結果
	 * @param row 検索結果行
	 * @return マッピングしたオブジェクトを戻します
	 * @throws TriJdbcDaoException
	 */
	public E entityMapping(ResultSet resultSet, int row) throws SQLException;

}
