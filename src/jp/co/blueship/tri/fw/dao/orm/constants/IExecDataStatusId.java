package jp.co.blueship.tri.fw.dao.orm.constants;


/**
 * The interface of of the enum constant for SM_EXEC_DATA_STS status-id.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 */
public interface IExecDataStatusId extends IStatusId {

}
