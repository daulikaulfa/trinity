package jp.co.blueship.tri.fw.dao.orm;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;



/**
 * Queryを組みたてるためのBuilderインタフェースです
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public interface ISqlBuilder {
	/**
	 * テーブル属性インタフェースを取得します。
	 * @return 取得したテーブル属性を戻します。
	 */
	public ITableAttribute getTableAttribute();

	/**
	 * Queryを組みたてるための文字列にappendします
	 *
	 * @param element テーブル項目
	 * @param value 要素に設計する値
	 * @return a reference to this instance, so it's possible to chain several calls together
	 */
	public ISqlBuilder append( ITableItem item, Object value );

	/**
	 * Queryを組みたてるための文字列にappendします
	 *
	 * @param element テーブル項目
	 * @param value 要素に設計する値
	 * @param isPrimaryKey 主キーの場合true。それ以外はfalse。
	 * @return a reference to this instance, so it's possible to chain several calls together
	 */
	public ISqlBuilder append( ITableItem item, Object value, boolean isPrimaryKey );

	/**
	 * Queryを取得します。
	 *
	 * @return Queryを戻します。
	 */
	public String toQueryString();

}
