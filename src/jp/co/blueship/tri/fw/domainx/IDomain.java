package jp.co.blueship.tri.fw.domainx;

import jp.co.blueship.tri.fw.domain.dto.IServiceDto;


/**
 * ドメイン層のインタフェースです。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public interface IDomain<E> {

	/**
	 * ドメイン層での処理を実行します。
	 *
	 * @param serviceDto Service呼び出し時のパラメータ
	 * @return 戻り値を返します。
	 */
	public IServiceDto<E> execute( IServiceDto<E> serviceDto );

}
