package jp.co.blueship.tri.fw.domainx.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.DesignSheetUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.fw.ex.ITranslatable;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.sm.domain.product.ProductActivate;

/**
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class DomainServiceBean extends GenericServiceBean {
	private Header header = new Header();
	private MessageInfo messageInfo = new MessageInfo( this );
	private GenericServiceBean innerService;
	private GenericServiceBean innerViewService;

	/**
	 * @deprecated V3 Only
	 */
	@Override
	public String getLockServerId() {
		return super.getLockServerId();
	}
	/**
	 * @deprecated V3 Only
	 */
	@Override
	public void setLockServerId( String lockServerId ) {
		super.setLockServerId(lockServerId);
	}

	/**
	 * Front-End Header
	 */
	public class Header {
		private int windowsId = 0;
		private String projectName;
		private String projectIconPath;
		private String userIconPath;
		private String headerName;
		private String headerIconPath;

		private int progressNotification = 0;
		private int reloadInterval = 0;

		private SelectedLot selectedLot = new SelectedLot();

		public int getWindowsId() {
			return windowsId;
		}
		public Header setWindowsId(int windowsId) {
			this.windowsId = windowsId;
			return this;
		}

		/**
		 * Determines whether "Release Request" service is enabled.
		 * <br>リリース申請を有効とするか否かを返す。
		 *
		 * @return true If available to "Release Request" function. 有効とする場合はtrue。そうでない場合はfalse。
		 * @return
		 */
		public boolean isReleaseRequestEnabled() {
			return DesignSheetUtils.isRelApplyEnable();
		}

		public boolean isDmActive() {
			return ProductActivate.isDmActive();
		}

		public String getProjectName() {
			return projectName;
		}
		public Header setProjectName(String projectName) {
			this.projectName = projectName;
			return this;
		}

		public String getProjectIconPath() {
			return projectIconPath;
		}
		public Header setProjectIconPath(String projectIconPath) {
			this.projectIconPath = projectIconPath;
			return this;
		}

		public String getUserIconPath() {
			return userIconPath;
		}
		public Header setUserIconPath(String userIconPath) {
			this.userIconPath = userIconPath;
			return this;
		}

		public String getHeaderName() {
			return headerName;
		}
		public Header setHeaderName(String headerName) {
			this.headerName = headerName;
			return this;
		}

		public String getHeaderIconPath() {
			return headerIconPath;
		}
		public Header setHeaderIconPath(String headerIconPath) {
			this.headerIconPath = headerIconPath;
			return this;
		}

		public String getLogInUserId() {
			return getUserId();
		}

		public String getLogInUserName() {
			return getUserName();
		}

		/**
		 * Interval time for progress notification(millisecond).
		 *
		 * @return
		 */
		public int getProgressNotification() {
			return progressNotification;
		}
		public Header setProgressNotification(int progressNotification) {
			this.progressNotification = progressNotification;
			return this;
		}

		/**
		 * Reload Interval(millisecond).
		 *
		 * @return
		 */
		public int getReloadInterval() {
			return reloadInterval;
		}
		public Header setReloadInterval(int reloadInterval) {
			this.reloadInterval = reloadInterval;
			return this;
		}

		public SelectedLot getSelectedLot() {
			return selectedLot;
		}
		public Header setSelectedLot(SelectedLot selectedLot) {
			this.selectedLot = selectedLot;
			return this;
		}

		public class SelectedLot {
			private boolean isSelected = true;
			private boolean isEnabled = true;
			private String themeColor = "";

			public boolean isSelected() {
				return isSelected;
			}
			public SelectedLot setSelected(boolean isSelected) {
				this.isSelected = isSelected;
				return this;
			}
			public boolean isEnabled() {
				return isEnabled;
			}
			public SelectedLot setEnabled(boolean isEnabled) {
				this.isEnabled = isEnabled;
				return this;
			}
			public String getThemeColor() {
				return themeColor;
			}
			public SelectedLot setThemeColor(String themeColor) {
				this.themeColor = themeColor;
				return this;
			}
		}
	}

	/**
	 * Returns the Front-End Header Information.
	 * @return a Front-End Header
	 */
	public Header getHeader() {
		return header;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam {
		private RequestType requestType = RequestType.init;
		private String referer = null;
		private String forward = null;

		public RequestType getRequestType() {
			return requestType;
		}
		public RequestParam setRequestType(RequestType requestType) {
			this.requestType = requestType;
			return this;
		}
		public String getReferer() {
			return referer;
		}
		public RequestParam setReferer(String referer) {
			this.referer = referer;
			return this;
		}
		public String getForward() {
			return forward;
		}
		public RequestParam setForward(String forward) {
			this.forward = forward;
			return this;
		}

	}

	/**
	 * @return
	 */
	public RequestParam getParam() {
		return null;
	}

	public final void setParam(RequestParam param) {
		if ( null == param ) {
			return;
		}

		if ( null != this.getParam() ) {
			TriPropertyUtils.copyProperties(this.getParam(), param);
		}
	}

	/**
	 * Front-End Message
	 */
	public class MessageInfo {
		DomainServiceBean parentBean;

		private List<ITranslatable> flashTranslatables = new ArrayList<ITranslatable>();
		private List<String> flashMessages = new ArrayList<String>();

		protected MessageInfo( DomainServiceBean parentBean ) {
			this.parentBean = parentBean;
		}

		/**
		 * Removes all of the messages from this object instans.
		 *
		 * @return this object instans
		 */
		public MessageInfo clear() {
			flashTranslatables = new ArrayList<ITranslatable>();
			flashMessages = new ArrayList<String>();

			List<String> messages = new ArrayList<String>();
			parentBean.setMessages(messages);

			return this;
		}

		/**
		 * Returns true if no message contains.
		 *
		 * @return true if no message contains
		 */
		public boolean isEmptyFlashTranslatable() {
			if ( null == flashTranslatables || 0 == flashTranslatables.size() ) {
				return true;
			}

			return false;
		}

		public MessageInfo addFlashTranslatable(IMessageId message, String... messageArgs){
			flashTranslatables.add( new Translatable( message, messageArgs ) );
			return this;
		}
		public List<ITranslatable> getFlashTranslatable() {
			return flashTranslatables;
		}

		/**
		 * Returns true if no message contains.
		 *
		 * @return true if no message contains
		 */
		public boolean isEmptyFlashMessages() {
			if ( null == flashMessages || 0 == flashMessages.size() ) {
				return true;
			}

			return false;
		}

		public List<String> getFlashMessages() {
			return flashMessages;
		}
		public MessageInfo addFlashMessages(List<String> flashMessages) {
			this.flashMessages.addAll( flashMessages );
			return this;
		}

		public class Translatable implements ITranslatable {
			private IMessageId messageID;
			private String[] messageArgs = new String[] { "", "" };

			public Translatable(IMessageId messageID, String... messageArgs) {
				this.messageID = messageID;
				this.messageArgs = messageArgs;
			}

			@Override
			public IMessageId getMessageID() {
				return messageID;
			}

			@Override
			public String[] getMessageArgs() {
				return messageArgs;
			}
		}
	}

	/**
	 * Returns the Front-End Message Information.
	 * @return a Front-End Message
	 */
	public MessageInfo getMessageInfo() {
		return messageInfo;
	}

	@SuppressWarnings("unchecked")
	public <SB extends GenericServiceBean> SB getInnerService() {
		return (SB)this.innerService;
	}

	public <SB extends GenericServiceBean> void setInnerService( SB innerService ) {
		this.innerService = innerService;
	}

	@SuppressWarnings("unchecked")
	public <SV extends GenericServiceBean> SV getInnerViewService() {
		return (SV)this.innerViewService;
	}

	public <SV extends GenericServiceBean> void setInnerViewService( SV innerViewService ) {
		this.innerViewService = innerViewService;
	}

}
