package jp.co.blueship.tri.fw.log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import jp.co.blueship.tri.fw.cmn.io.FilePathDelimiter;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriSystemUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.SmMessageId;

/**
 * ログ出力に対する処理を提供するClass
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2007
 */
public class LogUtilFile implements LogUtil {


	public static final String LOG_EXTENSION = "log";

	private String logDir = null;
	private String logName = null;

	private AbstractLogInfo logInfo = null;
	//	public LogUtil(BuildServiceBean bean, int logType) {
	//
	//		createLogUtil(bean, logType);
	//
	//    	this.logName = getLogName(bean.getBuildNo(), null, logInfo.getBaseLogName());
	//    }
	//
	//	public LogUtil(BuildServiceBean bean, CompileParameterBean param, int logType) {
	//
	//		createLogUtil(bean, logType);
	//
	//    	this.logName = getLogName(bean.getBuildNo(), param.getCompileGroupId(), logInfo.getBaseLogName());
	//    }

	public LogUtilFile(String logPath , int logType) throws Exception {
		FilePathDelimiter filePathDelimiter = new FilePathDelimiter( logPath ) ;
		logDir = filePathDelimiter.getParentDir() ;
		createLogUtil( logType);

		//this.logName = getLogName(bean.getBuildNo(), param.getCompileGroupId(), logInfo.getBaseLogName());
		this.logName = filePathDelimiter.getObjName() ;
	}

	private void createLogUtil( int logType) throws Exception {

		//logDir = bean.getReportOutDir();

		boolean win32 = TriSystemUtils.isWindows();

		if (logType == LOG_TYPE_ANT) {
			logInfo = new AntLogInfo(win32);
		} else if (logType == LOG_TYPE_MAKE) {
			logInfo = new MakeLogInfo(win32);
		} else if (logType == LOG_TYPE_JAR) {
			logInfo = new JarLogInfo(win32);
		} else if (logType == LOG_TYPE_SHELL) {
			logInfo = new ShellLogInfo(win32);
		} else if (logType == LOG_TYPE_CHANGEPERMISSION) {
			logInfo = new ChangePermissionLogInfo(win32);
		} else if (logType == LOG_TYPE_NULL) {
			logInfo = new NullLogInfo(win32);
		} else {
//			throw new SystemException("ログの種類が判定できません。 logType = " + logType);
			throw new TriSystemException(SmMessageId.SM004200F , String.valueOf(logType) );
		}
	}

	//	/**
	//	 * 基本ログ名からログ名を作成する
	//	 */
	//    public static String getLogName(String buildNo, String compGroupId, String baseLogName) {
	//
	//    	String logName = null;
	//
	//    	if (null != baseLogName) {
	//
	//    		logName = baseLogName;
	//
	//	    	try {
	//	    	    VariableExpander ve = new VariableExpander();
	//
	//	    	    if (null != buildNo) {
	//	    	    	logName = ve.expandValue(logName, REPLACE_SUFFIX_BUILDNO, buildNo);
	//	    	    }
	//	    	    if (null != compGroupId) {
	//	    	    	logName = ve.expandValue(logName, REPLACE_SUFFIX_COMP_GROUP_ID, compGroupId);
	//	    	    }
	//	    		logName = ve.expandValue(logName, REPLACE_SUFFIX_TIMESTAMP, getTimeStamp());
	//	    	} catch (Exception e) {
	//
	//	    	}
	//    	}
	//
	//    	return logName;
	//    }

	/**
	 * ログファイルをパスを含めて作成する
	 */
	public String getLogPath() {

		if (null == logDir || logDir.trim().equals("")) {
			return "";
		}

		if (logName.equals("")) {
			return "";
		}

		String logPath = createLogPath(logDir, logName);

		return logPath;
	}

	/**
	 * ログファイルの作成が必要かどうか取得する
	 */
	public boolean isFileCreate() {
		return logInfo.isFileCreate();
	}

	/**
	 * ログファイルを作成し、logの内容を書き出す
	 */
	public void writeLog(String log, String logPath) {

		if (logPath.equals("")) {
			return;
		}

		writeString(log, logPath);
	}

	/**
	 *  ログファイルを作成し、logの内容を書き出す
	 */
	public static void writeString(String log, String logPath) {

		File logFile = new File(logPath);
		if (!logFile.getParentFile().exists()) {
			logFile.getParentFile().mkdirs();
		}

		try {
			FileWriter fw = new FileWriter(logPath, true);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(log);
			bw.flush();
			bw.close();
			fw.close();
		} catch (IOException ie) {
			throw new TriSystemException(SmMessageId.SM005177S, ie);
		}
	}

	/**
	 * logにエラーの内容が含まれているかをチェックする
	 */
	public boolean isFailed(String log) {

		String[] errorPattern = logInfo.getErrorPattern();

		for (int i = 0; i < errorPattern.length; i++) {

			if (errorPattern[i].trim().equals("")) {
				continue;
			}

			int result = log.indexOf(errorPattern[i]);
			if (result != -1) {
				return true;
			}
		}

		return false;
	}

	/**
	 * logに成功の内容が含まれているかをチェックする
	 * @param log チェック対象のログ
	 * @return 成功の内容が含まれていればtrue、そうでなければfalse
	 */
	public boolean isSucceed( String log ) {

		String[] successPattern = logInfo.getSuccessPattern();

		for ( int i = 0; i < successPattern.length; i++ ) {

			if ( successPattern[i].trim().equals( "" ) ) {
				continue;
			}

			int result = log.indexOf( successPattern[i] );
			if ( result != -1 ) {
				return true;
			}
		}

		return false;
	}

	/**
	 * ログファイル取得時のオプションを取得する
	 */
	public String getLogOption() {

		if (null == logDir || logDir.trim().equals("")) {
			return "";
		} else {
			return logInfo.getLogOption();
		}
	}

	/**
	 * bashのときに追加で必要となるログオプションを取得する
	 */
	public String getBashLogOption() {

		if (logInfo.getLogOption().trim().equals("")) {
			return "";
		} else {
			return "2>&1";
		}
	}

	/**
	 * <p>
	 * ログファイルを移動する。
	 * </p>
	 */
	public static void moveLogFile(File oldLog, File newLog) throws IOException {

		if (!newLog.exists()) {

			if (oldLog.exists()) {
				TriFileUtils.copyFileToFile(oldLog, newLog);
				oldLog.delete();
			}
		}
	}

	/**
	 * <p>
	 * ログのパスを作成する
	 * </p>
	 */
	public static String createLogPath(String logDir, String logName) {

		String log = logDir + File.separator + logName;

		// directoryが存在しなかった場合は作成。
		File logDirectory = new File(logDir);
		if(!logDirectory.exists()){
			logDirectory.mkdirs();
		}

		return log;
	}

	public static String getTimeStamp(){
		Date date = new Date();
		DateFormat df = new SimpleDateFormat("yyyyMMdd_HHmmss");

		return df.format(date);
	}

	/**
	 * ログファイルの作成をするかどうかをチェックする
	 */
	public boolean isFileExistCheck() {
		return logInfo.isFileExistCheck();
	}

	/**
	 * 指定されたログファイルの中身を読み込む
	 */
	public String readLogContents(String logPath, boolean isFileCheck) {

		File logFile = null;
		BufferedReader br = null;
		try {
			logFile = new File(logPath);
			br = new BufferedReader(new FileReader(logFile));
		} catch (FileNotFoundException e1) {
			if (isFileCheck) {
				throw new TriSystemException( SmMessageId.SM004201F, e1 );
			} else {
				return "";
			}
		}

		String line;
		StringBuilder logerr = new StringBuilder();
		try {
			while((line = br.readLine()) != null) {
				logerr.append(line);
			}
			br.close();
		} catch (IOException e2) {
			throw new TriSystemException(SmMessageId.SM005178S, e2);
		}

		return logerr.toString();
	}

	//    /**
	//     * レポートファイルパスを取得する。
	//     */
	//    public static String getReportFilePath(
	//    		String reportNameKey, String reportOutPath, String buildNo) throws IOException{
	//
	//		String baseReportName = AbstractLogInfo.getBaseLogName(reportNameKey);
	//		String reportName;
	//		String tmpReportName = "." + baseReportName;
	//		if (null == buildNo || buildNo.trim().equals("")) {
	//			reportName = tmpReportName;
	//		} else {
	//
	//			reportName = getLogName(buildNo, null, baseReportName);
	//			File logFile = new File(reportOutPath, reportName);
	//			File tmpLogFile = new File(reportOutPath, tmpReportName);
	//
	//			moveLogFile(tmpLogFile, logFile);
	//		}
	//
	//		return createLogPath(reportOutPath, reportName);
	//    }
}
