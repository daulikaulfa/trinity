package jp.co.blueship.tri.fw.log;

import java.io.File;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.DesignBusinessRuleUtils;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.SystemProps;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByLogs;
import jp.co.blueship.tri.fw.constants.BmDesignEntryKeyByLogs;
import jp.co.blueship.tri.fw.constants.RmDesignEntryKeyByLogs;
import jp.co.blueship.tri.fw.constants.UmDesignBeanId;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.msg.SmMessageId;


/**
 * 業務フローに関連するログ出力に対する共通処理を提供するClass
 * <br>
 *
 * @version V3L10.01
 * @author trinity V3
 *
 *
 * @version V3L11.01
 * @author Siti Hajar
 *
 */
public class LogBusinessFlow implements Serializable {

	private static final long serialVersionUID = 2L;
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private final String DATE_PATTERN = "yyyy-MMdd-HHmmss";

	private String logPath = null ;
	private String timestamp = null ;

	public String getLogPath() {
		return logPath;
	}
	public void setLogPath(String logPath) {
		this.logPath = logPath;
	}
	IContextAdapter ac = ContextAdapterFactory.getContextAdapter();
	/**
	 * コンストラクタ
	 */
	public LogBusinessFlow() {

		this.timestamp = this.getLogTimestamp() ;
	}

	/**
	 * コンストラクタ
	 *
	 * @param timestamp ログタイムスタンプ
	 */
	public LogBusinessFlow( Date timestamp ) {
		if ( TriStringUtils.isEmpty( timestamp ) ) {
			this.timestamp = this.getLogTimestamp();
		} else {
			final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat( DATE_PATTERN );
			this.timestamp = DATE_FORMAT.format( timestamp );
		}
	}

	/**
	 * システムの改行コードを取得する<br>
	 * @return
	 */
	public static String getSystemLineSeparator() {

		String lineSeparator = SystemProps.LineSeparator.getProperty();
		return lineSeparator ;
	}
	/**
	 * ログを出力する<br>
	 * 既存ファイルが存在する場合、追記する。<br>
	 * @param logPath ログファイルのパス
	 * @param logData ログ内容
	 */
	public void writeLog( String logData ) {
		try {

			if( TriStringUtils.isEmpty( logPath ) ) {
				throw new TriSystemException( SmMessageId.SM004210F ) ;
			}

			File logFile = new File( logPath ) ;
			logFile.getParentFile().mkdirs() ;

			TriFileUtils.writeStringFile( logFile , logData + getSystemLineSeparator() , true ) ;

		} catch ( Exception e ) {
			//ログ自体の不具合で極力システムをダウンさせない
			e.printStackTrace() ;
		}
	}
	/**
	 * ログを出力する.TriLogMessage型で受ける<br>
	 * 既存ファイルが存在する場合、追記する。<br>
	 * @param logPath ログファイルのパス
	 * @param logData ログ内容
	 */
	public void writeLog( TriLogMessage logId ) {
		writeLog( logId , (String) null );
	}

	/**
	 * ログを出力する.IMessageId型で受ける<br>
	 * 既存ファイルが存在する場合、追記する。<br>
	 * @param messageId message ID
	 * @param messageArgs message parameter
	 */
	public void writeLog( IMessageId messageId, String... messageArgs ) {
		try {
			writeLog( ac.getMessage( messageId , messageArgs ) );
		} catch ( Exception e ) {
			//ログ自体の不具合で極力システムをダウンさせない
			e.printStackTrace() ;
		}
	}

	/**
	 * ログを出力する.TriLogMessage型で受ける<br>
	 * 既存ファイルが存在する場合、追記する。<br>
	 * @param logId log message ID
	 * @param messageArgs message parameter
	 */
	public void writeLog( TriLogMessage logId , String...messageArgs ) {
		try {
			writeLog( ac.getMessage( logId , messageArgs ) );
		} catch ( Exception e ) {
			//ログ自体の不具合で極力システムをダウンさせない
			e.printStackTrace() ;
		}
	}

	/**
	 * ログを出力する（タイムスタンプ付き）<br>
	 * 既存ファイルが存在する場合、追記する。<br>
	 * @param logPath ログファイルのパス
	 * @param trace スタックとレースの配列
	 */
	public void writeLogWithDate( String logData ) {

		String systemDate = TriDateUtils.getSystemDate() ;
		writeLog( systemDate + " " + logData ) ;
	}
	/**
	 * ログを出力する（タイムスタンプ＆開始ラベル付き）<br>
	 * 既存ファイルが存在する場合、追記する。<br>
	 * @param logPath ログファイルのパス
	 * @param trace スタックとレースの配列
	 */
	public void writeLogWithDateBegin( String logData ) {

		writeLogWithDate( ac.getMessage( TriLogMessage.LSM0028 ) + logData ) ;
	}
	/**
	 * ログを出力する（タイムスタンプ＆終了ラベル付き）<br>
	 * 既存ファイルが存在する場合、追記する。<br>
	 * @param logPath ログファイルのパス
	 * @param trace スタックとレースの配列
	 * @throws Exception
	 */
	public void writeLogWithDateEnd( String logData ) {

		writeLogWithDate( ac.getMessage( TriLogMessage.LSM0029 ) + logData + getSystemLineSeparator() ) ;
	}
	/**
	 * ログを出力する<br>
	 * 既存ファイルが存在する場合、追記する。<br>
	 * @param logPath ログファイルのパス
	 * @param trace スタックとレースの配列
	 */
	public void writeLog( StackTraceElement[] trace ) {
		for( StackTraceElement element : trace ) {
			writeLog( element.toString() ) ;
		}
	}

	/**
	 * ログ出力用のタイムスタンプを取得する<br>
	 * @return タイムスタンプ
	 */
	private String getLogTimestamp() {

		final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat( DATE_PATTERN );

		String logTimestamp = DATE_FORMAT.format( new Date() ) ;

		return logTimestamp ;
	}

	/**
	 * ログ出力用 アクション名タイトルラベル生成を生成する
	 * @param flowAction アクション名
	 * @return アクション名タイトルラベル
	 */
	public static String makeFlowActionTitle( String flowAction ) {

		String flowActionLabel = sheet.getValue( UmDesignBeanId.actionId , flowAction ) ;
		String flowActionTitle = flowActionLabel + "(" + flowAction + ")" ;

		return flowActionTitle ;
	}
	/**
	 * ログファイル出力ファイルパスを生成する。<br>
	 * workspace + "/" + logRelationPath + "/" + flowAction + "/" + timestamp[YYYY-MMDD-HHMMSS].log<br>
	 * <br>
	 * @param flowAction 実行サービス名
	 * @param pjtLotEntity ロット情報
	 * @return ログファイル出力ファイルパス
	 */
	public String makeLogPath( String flowAction , ILotEntity pjtLotEntity ) {

		String fileName = this.timestamp + ".log" ;

		File logPathFile = new File( DesignBusinessRuleUtils.getWorkspaceLogPath( pjtLotEntity, flowAction ), fileName );

		String logPath = TriStringUtils.convertPath( logPathFile ) ;

		this.setLogPath( logPath ) ;

		return logPath ;
	}

	/**
	 * 出力したログ情報を削除する。
	 *
	 */
	public void deleteLog() {
		if ( TriStringUtils.isEmpty(this.getLogPath()) )
			return;

		try {
			File logFile = new File( this.getLogPath() ) ;
			if( logFile.exists() && logFile.isFile() ) {
				logFile.delete() ;
			}
		} catch ( Exception e ) {
			//ログ自体の不具合で極力システムをダウンさせない
			e.printStackTrace() ;
		}
	}

	/**
	 * 出力したログ情報を削除する。
	 * <br>カスタマイズ項目「成功時にログを削除」が{@see StatusFlg.on.value() }の場合、削除する。
	 * それ意外は何もしない。
	 *
	 * @param define
	 */
	public void deleteLog( AmDesignEntryKeyByLogs define ) {
		try {
			String deleteLogAtSuccess = sheet.getValue( define ) ;

			if( true == StatusFlg.on.value().equals( deleteLogAtSuccess ) ) {
				this.deleteLog();
			}
		} catch ( Exception e ) {
			//ログ自体の不具合で極力システムをダウンさせない
			e.printStackTrace() ;
		}
	}

	/**
	 * 出力したログ情報を削除する。
	 * <br>カスタマイズ項目「成功時にログを削除」が{@see StatusFlg.on.value() }の場合、削除する。
	 * それ意外は何もしない。
	 *
	 * @param define
	 */
	public void deleteLog( BmDesignEntryKeyByLogs define ) {
		try {
			String deleteLogAtSuccess = sheet.getValue( define ) ;

			if( true == StatusFlg.on.value().equals( deleteLogAtSuccess ) ) {
				this.deleteLog();
			}
		} catch ( Exception e ) {
			//ログ自体の不具合で極力システムをダウンさせない
			e.printStackTrace() ;
		}
	}

	/**
	 * 出力したログ情報を削除する。
	 * <br>カスタマイズ項目「成功時にログを削除」が{@see StatusFlg.on.value() }の場合、削除する。
	 * それ意外は何もしない。
	 *
	 * @param define
	 */
	public void deleteLog( RmDesignEntryKeyByLogs define ) {
		try {
			String deleteLogAtSuccess = sheet.getValue( define ) ;

			if( true == StatusFlg.on.value().equals( deleteLogAtSuccess ) ) {
				this.deleteLog();
			}
		} catch ( Exception e ) {
			//ログ自体の不具合で極力システムをダウンさせない
			e.printStackTrace() ;
		}
	}
	/**
	 * ログのヘッダ部分、実行者ラベルとロット番号ラベルをstbにappendします。stbは呼び出し元でnewしてください。
	 * @param stb
	 * @param userName
	 * @param userId
	 * @param lotName
	 * @param lotNo
	 * @return stb
	 */
/*
	public StringBuilder makeLogHeaderUserAndLot(StringBuilder stb , String userName , String userId , String lotName , String lotId ){
		IContextAdapter ac = ContextAdapterFactory.getContextAdapter();
		//実行者ラベル
		String userLabel = ac.getMessage( TriLogMessage.LAM0004 , userName , userId );
		stb.append( userLabel + getSystemLineSeparator() );
		//ロット番号ラベル
		String lotIdLabel = ac.getMessage( TriLogMessage.LAM0005 , lotName , lotId );
		stb.append(lotNoLabel + getSystemLineSeparator() );
		return stb;
	}*/
	/**
	 * ログのヘッダ部分、実行者ラベルとロット番号ラベルをstbにappendします。stbは呼び出し元でnewしてください。
	 * @param stb
	 * @param userName
	 * @param userId
	 * @param lotName
	 * @param lotNo
	 * @return stb
	 */
	public StringBuilder makeUserAndLotLabel(StringBuilder stb , IGeneralServiceBean svc , String lotName , String lotId ){

		String userLabel = ac.getMessage( TriLogMessage.LAM0004 , svc.getUserName() , svc.getUserId() );
		stb.append( userLabel + getSystemLineSeparator() );

		String lotIdLabel = ac.getMessage( TriLogMessage.LAM0005 , lotName , lotId );
		stb.append(lotIdLabel + getSystemLineSeparator() );
		return stb;
	}

	public StringBuilder makeBaseLineLabel(StringBuilder stb , String closeBaseLineTag ,String closeVersionTag ){

		String baselineLabel = ac.getMessage( TriLogMessage.LAM0006 ,
				( ( null != closeBaseLineTag ) ? closeBaseLineTag : "" ) , closeVersionTag ) ;
		stb.append( baselineLabel + getSystemLineSeparator() ) ;

		return stb;
	}

	public StringBuilder makeCheckInIdLabel(StringBuilder stb , String dataId){
		stb.append( ac.getMessage( TriLogMessage.LAM0007 , dataId ) + getSystemLineSeparator() ) ;
		return stb;
	}

}
