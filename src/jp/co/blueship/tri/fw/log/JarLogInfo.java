package jp.co.blueship.tri.fw.log;

import jp.co.blueship.tri.fw.constants.SmDesignBeanId;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;

/**
 * Jarログ出力に対する情報を保持するClass
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2007
 */
public class JarLogInfo extends AbstractLogInfo {

	/** サクセスログ */
	private static final String[] SUCCESSFUL = {""};
	/** エラーログ */
	public static final String[] FAILED =
			DesignSheetFactory.getDesignSheet().getValueList( SmDesignBeanId.jarFailed ).toArray( new String[ 0 ] ) ;
		//{"ファイルまたはディレクトリはありません","No such file or directory"};
	/**
	 * JarCreatorのkey名
	 */
	public static final String REPORT_JAR_CREATOR = "REPORT_JAR_CREATOR";

	public JarLogInfo(boolean win32) {
		super(win32);
	}

	public String getBaseLogName() {
		return super.getBaseLogName(REPORT_JAR_CREATOR);
	}

	public String[] getSuccessPattern() {
		return SUCCESSFUL;
	}

	public String[] getErrorPattern() {
		return FAILED;
	}

	public String getLogOption() {

		String logOption;

		if (win32) {
			logOption = "2>&1)>>";
    	} else {
     		logOption = ">>";
    	}
		return logOption;
	}

	public boolean isFileCreate() {
		return true;
	}

	public boolean isFileExistCheck() {
		return true;
	}
}
