package jp.co.blueship.tri.fw.log;

import java.util.ResourceBundle;

/**
 * ログ出力に対する情報を保持する抽象Class
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2007
 */
public abstract class AbstractLogInfo {


	/**
	 * reportのproperty file名。
	 */
	public static final String REPORTS_PROPS ="reports";

	protected boolean win32;

	protected AbstractLogInfo(boolean win32) {
		this.win32 = win32;
	}

	public static String getBaseLogName(String key) {
		ResourceBundle rbReport = ResourceBundle.getBundle(REPORTS_PROPS);
		return rbReport.getString(key);
	}

	public abstract String getBaseLogName();
	public abstract String getLogOption();
	public abstract String[] getErrorPattern();
	public abstract String[] getSuccessPattern();
	public abstract boolean isFileCreate();
	public abstract boolean isFileExistCheck();
}
