package jp.co.blueship.tri.fw.log;

import jp.co.blueship.tri.fw.constants.SmDesignBeanId;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;

/**
 * Antログ出力に対する情報を保持するClass
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2007
 */
public class AntLogInfo extends AbstractLogInfo {

	/** サクセスログ */
	private static final String[] SUCCESSFUL = DesignSheetFactory.getDesignSheet().getValueList( SmDesignBeanId.antSuccessful ).toArray( new String[ 0 ] ) ;//{""};
	/** エラーログ */
	private static final String[] FAILED = DesignSheetFactory.getDesignSheet().getValueList( SmDesignBeanId.antFailed ).toArray( new String[ 0 ] ) ;//{"BUILD FAILED"};
	/**
	 * AntExecuteのkey名
	 */
	public static final String REPORT_ANT = "REPORT_ANT";

	public AntLogInfo(boolean win32) {
		super(win32);
	}

	public String getBaseLogName() {
		return super.getBaseLogName(REPORT_ANT);

	}

	public String[] getSuccessPattern() {
		return SUCCESSFUL;
	}

	public String[] getErrorPattern() {
		return FAILED;
	}

	public String getLogOption() {
		return "-logfile";
	}

	public boolean isFileCreate() {
		return false;
	}

	public boolean isFileExistCheck() {
		return true;
	}
}
