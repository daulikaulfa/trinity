package jp.co.blueship.tri.fw.log;

import jp.co.blueship.tri.fw.constants.SmDesignBeanId;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;

/**
 * ChangePermissionログ出力に対する情報を保持するClass
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2007
 */
public class ChangePermissionLogInfo extends AbstractLogInfo {

	/** サクセスログ */
	private static final String[] SUCCESSFUL = {""};
	/** エラーログ */
	private static final String[] FAILED = DesignSheetFactory.getDesignSheet().getValueList(SmDesignBeanId.changePermissionFailed ).toArray( new String[ 0 ] ) ;//{"BUILD FAILED"};

	public ChangePermissionLogInfo(boolean win32) {
		super(win32);
	}

	public String getBaseLogName() {
		return "";
	}

	public String[] getSuccessPattern() {
		return SUCCESSFUL;
	}

	public String[] getErrorPattern() {
		return FAILED;
	}

	public String getLogOption() {
		return "-logfile";
	}

	public boolean isFileCreate() {
		return false;
	}

	public boolean isFileExistCheck() {
		return true;
	}
}
