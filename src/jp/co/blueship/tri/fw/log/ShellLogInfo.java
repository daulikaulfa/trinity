package jp.co.blueship.tri.fw.log;

import jp.co.blueship.tri.fw.constants.SmDesignBeanId;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;

/**
 * Shellログ出力に対する情報を保持するClass
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2007
 */
public class ShellLogInfo extends AbstractLogInfo {

	/** サクセスログ */
	private static final String[] SUCCESSFUL =
			DesignSheetFactory.getDesignSheet().getValueList( SmDesignBeanId.shellSuccessful ).toArray( new String[ 0 ] ) ;
		//{""};
	/** エラーログ */
	private static final String[] FAILED =
			DesignSheetFactory.getDesignSheet().getValueList( SmDesignBeanId.shellFailed ).toArray( new String[ 0 ] ) ;
		//{"エラー"};

	public ShellLogInfo(boolean win32) {
		super(win32);
	}

	public String getBaseLogName() {
		return "";
	}

	public String[] getSuccessPattern() {
		return SUCCESSFUL;
	}

	public String[] getErrorPattern() {
		return FAILED;
	}

	public String getLogOption() {
		return "";
	}

	public boolean isFileCreate() {
		return true ;//false;
	}

	public boolean isFileExistCheck() {
		return false;
	}
}
