package jp.co.blueship.tri.fw.log;

import jp.co.blueship.tri.fw.constants.SmDesignBeanId;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;

/**
 * Makeログ出力に対する情報を保持するClass
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2007
 */
public class MakeLogInfo extends AbstractLogInfo {

	/** サクセスログ */
	private static final String[] SUCCESSFUL = {""};
	/** エラーログ */
	private static final String[] FAILED =
			DesignSheetFactory.getDesignSheet().getValueList( SmDesignBeanId.makeFailed ).toArray( new String[ 0 ] ) ;
		//{"BUILD FAILED"};
	/**
	 * MakeExecuteのkey名
	 */
	public static final String REPORT_MAKE = "REPORT_MAKE";

	public MakeLogInfo(boolean win32) {
		super(win32);
	}

	public String getBaseLogName() {
		return super.getBaseLogName(REPORT_MAKE);
	}

	public String[] getSuccessPattern() {
		return SUCCESSFUL;
	}

	public String[] getErrorPattern() {
		return FAILED;
	}

	public String getLogOption() {

		String logOption;

		if (win32) {
    		logOption = "2>&1)>";
    	} else {
    		logOption = ">";
    	}
		return logOption;
	}

	public boolean isFileCreate() {
		return true;
	}

	public boolean isFileExistCheck() {
		return true;
	}
}
