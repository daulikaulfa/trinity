package jp.co.blueship.tri.fw.log;

import jp.co.blueship.tri.fw.cmn.io.constants.LinefeedCode;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.di.spring.ContextAdapter;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;

/**
 *
 * log4jにログ出力を行う際の体裁を加工するためのハンドラクラス。
 *
 * @version V3L10.02
 * @author Takashi Ono
 */
public class LogHandler {

	private static final String[] EMPTY_STRING_ARRAY = new String[] {};

	/**
	 * FATALレベルのログ出力を行います。
	 *
	 * @param log Logオブジェクト
	 * @param message ログ出力を行うメッセージ
	 * @param e 例外（SystemException)
	 */
	public static void fatal(ILog log, String message, Throwable e) {
		StringBuilder stb = new StringBuilder();
		if (!TriStringUtils.isEmpty(stb)) {
			stb.append(LogHeader.FATAL.getValue() + LinefeedCode.CRLF.getValue());
		}
		stb.append(message + LinefeedCode.CRLF.getValue());

		log.fatal(stb.toString(), e);
	}

	/**
	 * FATALレベルのログ出力を行います。
	 * BusinessExceptionとContinuableBusinessExceptionではログを出力しません。
	 *
	 * @param log Logオブジェクト
	 * @param e 例外（SystemException)
	 */
	public static void fatal(ILog log, Throwable e) {

		if ( BaseBusinessException.class.isInstance(e) ){
			return;
		}
		StringBuilder stb = new StringBuilder();
		if ( TriStringUtils.isNotEmpty(stb)) {
			stb.append(LogHeader.FATAL.getValue() + LinefeedCode.CRLF.getValue());
		}
		log.fatal(stb.toString(), e);
	}

	/**
	 * ERRORレベルのログ出力を行います。
	 *
	 * @param log Logオブジェクト
	 * @param e 例外（BusinessException）
	 */
	public static void error(ILog log, BaseBusinessException e) {
		ContextAdapter ca = new ContextAdapter();

		StringBuilder stb = new StringBuilder();
		if (!TriStringUtils.isEmpty(stb)) {
			stb.append(LogHeader.ERROR.getValue() + LinefeedCode.CRLF.getValue());
		}
		stb.append(ca.getMessage((ITranslatable)e) + LinefeedCode.CRLF.getValue());

		log.error(stb.toString(), e);
	}

	/**
	 * WARNレベルのログ出力を行います。
	 *
	 * @param log Logオブジェクト
	 * @param message ログ出力を行うメッセージ
	 */
	public static void warn(ILog log, String message) {
		StringBuilder stb = new StringBuilder();
		if (!TriStringUtils.isEmpty(stb)) {
			stb.append(LogHeader.WARN.getValue() + LinefeedCode.CRLF.getValue());
		}
		stb.append(message + LinefeedCode.CRLF.getValue());

		log.warn(stb.toString());
	}

	/**
	 *
	 * @param log
	 * @param logId
	 */
	public static void warn(ILog log, TriLogMessage logId) {
		warn(log, logId, EMPTY_STRING_ARRAY);
	}

	/**
	 *
	 * @param log
	 * @param logId
	 * @param arg
	 */
	public static void warn(ILog log, TriLogMessage logId, String... arg) {

		ContextAdapter ca = new ContextAdapter();
		StringBuilder stb = new StringBuilder();
		if (!TriStringUtils.isEmpty(stb)) {
			stb.append(LogHeader.WARN.getValue() + LinefeedCode.CRLF.getValue());
		}
		stb.append(ca.getMessage(logId, arg) + LinefeedCode.CRLF.getValue());

		log.info(stb.toString());
	}

	/**
	 * INFOレベルのログ出力を行います。
	 *
	 * @param log Logオブジェクト
	 * @param message ログ出力を行うメッセージ
	 */
	public static void info(ILog log, String message) {
		StringBuilder stb = new StringBuilder();
		if (!TriStringUtils.isEmpty(stb)) {
			stb.append(LogHeader.INFO.getValue() + LinefeedCode.CRLF.getValue());
		}
		stb.append(message + LinefeedCode.CRLF.getValue());

		log.info(stb.toString());
	}

	/**
	 * INFOレベルのログ出力を行います。
	 *
	 * @param log Logオブジェクト
	 * @param Logmessage ログ出力を行うメッセージのID
	 */
	public static void info(ILog log, TriLogMessage logId) {

		info(log, logId, (String) null);
	}

	/**
	 * INFOレベルのログ出力を行います。
	 *
	 * @param log Logオブジェクト
	 * @param Logmessage ログ出力を行うメッセージのID
	 */
	public static void info(ILog log, TriLogMessage logId, String... arg) {
		ContextAdapter ca = new ContextAdapter();

		StringBuilder stb = new StringBuilder();
		if (!TriStringUtils.isEmpty(stb)) {
			stb.append(LogHeader.INFO.getValue() + LinefeedCode.CRLF.getValue());
		}
		stb.append(ca.getMessage(logId, arg) + LinefeedCode.CRLF.getValue());

		log.info(stb.toString());

	}

	/**
	 * DEBUGレベルのログ出力を行います。
	 *
	 * @param log Logオブジェクト
	 * @param message ログ出力を行うメッセージ
	 */
	public static void debug(ILog log, String message) {
		StringBuilder stb = new StringBuilder();
		if (!TriStringUtils.isEmpty(stb)) {
			stb.append(LogHeader.DEBUG.getValue() + LinefeedCode.CRLF.getValue());
		}
		stb.append(message + LinefeedCode.CRLF.getValue());

		log.debug(stb.toString());
	}

	/**
	 * TRACEレベルのログ出力を行います。
	 *
	 * @param log Logオブジェクト
	 * @param message ログ出力を行うメッセージ
	 */
	// trinityでは、traceレベルの出力は行わない。
	// トレースレベル・デバッグレベルの出力は、debugに寄せることとする。2011/10/26
	/*
	 * public static void trace( Log log , String message ) { StringBuilder stb
	 * = new StringBuilder() ; if( !StringAddonUtil.isNothing( stb ) ) {
	 * stb.append( LogHeader.TRACE.getValue() + LinefeedCode.CRLF.getValue() ) ;
	 * } stb.append( message + LinefeedCode.CRLF.getValue() ) ;
	 *
	 * log.trace( stb.toString() ) ; }
	 */

	/**
	 *
	 * ログ出力時のヘッダ部分を列挙します。
	 *
	 */
	private enum LogHeader {

		FATAL("システムエラー発生"), ERROR("業務エラー発生"), WARN(""), INFO(""), DEBUG(""), TRACE("");

		private String value = null;

		private LogHeader(String value) {
			this.value = value;
		}

		public String toString() {
			return this.value;
		}

		public String getValue() {
			return this.value;
		}
	}

}
