package jp.co.blueship.tri.fw.log;


/**
 * ログ出力に対する情報を保持するダミーClass
 * <br>
 * <p>
 * AbstractLogInfoのインスタンスが特に指定されていない場合にLogUtilによって呼び出される。
 * (ログ出力をさせたくない場合に、インスタンスを設定しないことによって実現するため)
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2007
 */
public class NullLogInfo extends AbstractLogInfo {

	/** サクセスログ */
	private static final String[] SUCCESSFUL = {""};
	/** エラーログ */
	private static final String[] FAILED = {""};

	public NullLogInfo(boolean win32) {
		super(win32);
	}

	public String getBaseLogName() {
		return "";
	}

	public String[] getSuccessPattern() {
		return SUCCESSFUL;
	}

	public String[] getErrorPattern() {
		return FAILED;
	}

	public String getLogOption() {
		return "";
	}

	public boolean isFileCreate() {
		return false;
	}

	public boolean isFileExistCheck() {
		return false;
	}
}
