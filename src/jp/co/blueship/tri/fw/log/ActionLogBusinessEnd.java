package jp.co.blueship.tri.fw.log;

import java.util.Date;

import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.fw.cmn.utils.ExtractEntityAddonUtils;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;

/**
 * 業務フローに関連するログ出力の終了を宣言します
 *
 */
public class ActionLogBusinessEnd extends ActionPojoAbstract<IGeneralServiceBean> {

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		LogBusinessFlow logBusinessFlow = null;
		boolean isEnd = false;

		try {

			IGeneralServiceBean businessCommonInfo = serviceDto.getServiceBean();

			Date timestamp = TriDateUtils.convertTimestampToDate( businessCommonInfo.getSystemDate() );
			logBusinessFlow = new LogBusinessFlow( timestamp );

			ILotEntity pjtLotEntity = ExtractEntityAddonUtils.extractPjtLot( serviceDto.getParamList() ) ;
			if ( TriStringUtils.isEmpty( pjtLotEntity ) ) {
				throw new TriSystemException( SmMessageId.SM005176S ) ;
			}

			//ログの初期処理
			logBusinessFlow.makeLogPath( businessCommonInfo.getFlowAction() , pjtLotEntity );

			//ログの終了ラベル出力
			String flowActionTitle = LogBusinessFlow.makeFlowActionTitle( businessCommonInfo.getFlowAction() );
			logBusinessFlow.writeLogWithDateEnd( flowActionTitle ) ;

			isEnd = true;
		} catch ( Exception e ) {
			//ログ自体の不具合で極力システムをダウンさせない
			e.printStackTrace();
		} finally {
			if ( isEnd ) {
				if( true == StatusFlg.on.value().equals( ExtractEntityAddonUtils.getLogBusinessDeleteFlg(serviceDto.getParamList()).value() ) ) {
					logBusinessFlow.deleteLog();
				}
			}
		}

		return serviceDto;
	}
}
