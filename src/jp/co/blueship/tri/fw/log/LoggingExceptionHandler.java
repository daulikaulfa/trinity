// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3)
// Source File Name:   LoggingExceptionHandler.java

package jp.co.blueship.tri.fw.log;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ExceptionHandler;
import org.apache.struts.config.ExceptionConfig;

public class LoggingExceptionHandler extends ExceptionHandler
{

	
	private static final ILog log = TriLogFactory.getInstance();

	public LoggingExceptionHandler()
	{
	}

	public ActionForward execute(Exception ex, ExceptionConfig config, ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
		throws ServletException
	{
		ActionForward af = super.execute(ex, config, mapping, form, request, response);
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		ex.printStackTrace(pw);
		LogHandler.fatal( log , sw.toString() , ex );
		return af;
	}
}
