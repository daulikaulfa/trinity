package jp.co.blueship.tri.fw.log;


/**
 * ログ出力に対する処理を提供するClass
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2007
 */
public interface LogUtil {

    /** TIME STAMP の置換用 */
    public static final String REPLACE_SUFFIX_TIMESTAMP = "timestamp";
    /** BUILD NO の置換用 */
    public static final String REPLACE_SUFFIX_BUILDNO = "buildno";
    /** COMPILE GROUP ID の置換用 */
    public static final String REPLACE_SUFFIX_COMP_GROUP_ID = "comp_group_id";

    public static final int LOG_TYPE_ANT = 0;
    public static final int LOG_TYPE_MAKE = 1;
    public static final int LOG_TYPE_JAR = 2;
    public static final int LOG_TYPE_SHELL = 5;
    public static final int LOG_TYPE_CHANGEPERMISSION = 7;
    public static final int LOG_TYPE_NULL = 99;

    /**
     * logにエラーの内容が含まれているかをチェックする
     */
    public boolean isFailed(String log) ;

    /**
     * logに成功の内容が含まれているかをチェックする
     */
    public boolean isSucceed( String log ) ;


}
