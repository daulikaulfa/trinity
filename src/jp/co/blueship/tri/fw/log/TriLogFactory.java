package jp.co.blueship.tri.fw.log;

import org.apache.commons.logging.LogFactory;

public class TriLogFactory {

	private static final ILog log = new TriLog( LogFactory.getLog( TriLogFactory.class.getName() ) );

	public static ILog getInstance() {
		return log ;
	}

}
