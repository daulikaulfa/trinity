package jp.co.blueship.tri.fw.log;

import org.apache.commons.logging.Log;

public class TriLog implements ILog {

	private Log log = null;

	public TriLog( Log log ) {
		this.log = log;
	}

	public void debug(Object obj) {
		log.debug(obj);

	}

	public void debug(Object obj, Throwable throwable) {
		log.debug(obj, throwable);
	}

	public void error(Object obj) {
		log.equals(obj);
	}

	public void error(Object obj, Throwable throwable) {
		log.error(obj, throwable);
	}

	public void fatal(Object obj) {
		log.fatal(obj);
	}

	public void fatal(Object obj, Throwable throwable) {
		log.fatal(obj, throwable);
	}

	public void info(Object obj) {
		log.info(obj);
	}

	public void info(Object obj, Throwable throwable) {
		log.info(obj, throwable);
	}

	public boolean isDebugEnabled() {
		return log.isDebugEnabled();
	}

	public boolean isErrorEnabled() {
		return log.isErrorEnabled();
	}

	public boolean isFatalEnabled() {
		return log.isFatalEnabled();
	}

	public boolean isInfoEnabled() {
		return log.isInfoEnabled();
	}

	public boolean isTraceEnabled() {
		return log.isTraceEnabled();
	}

	public boolean isWarnEnabled() {
		return log.isWarnEnabled();
	}

	public void trace(Object obj) {
		log.trace(obj);
	}

	public void trace(Object obj, Throwable throwable) {
		log.trace(obj, throwable);
	}

	public void warn(Object obj) {
		log.warn(obj);
	}

	public void warn(Object obj, Throwable throwable) {
		log.warn(obj, throwable);
	}

}
