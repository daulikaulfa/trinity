package jp.co.blueship.tri.fw.log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import jp.co.blueship.tri.fw.cmn.utils.TriSystemUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.SmMessageId;

/**
 * ログ出力に対する処理を提供するClass
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2007
 */
public class LogUtilLog4j implements LogUtil {


	private ILog log = null;
	private AbstractLogInfo logInfo = null;

	//	public LogUtil(BuildServiceBean bean, int logType) {
	//
	//		createLogUtil(bean, logType);
	//
	//    	this.logName = getLogName(bean.getBuildNo(), null, logInfo.getBaseLogName());
	//    }
	//
	//	public LogUtil(BuildServiceBean bean, CompileParameterBean param, int logType) {
	//
	//		createLogUtil(bean, logType);
	//
	//    	this.logName = getLogName(bean.getBuildNo(), param.getCompileGroupId(), logInfo.getBaseLogName());
	//    }

	public LogUtilLog4j(ILog log , int logType) throws Exception {
		this.log = log ;
		createLogUtil(logType);
	}

	private void createLogUtil( int logType ) throws Exception {

		//logDir = bean.getReportOutDir();

		boolean win32 = TriSystemUtils.isWindows();

		if (logType == LOG_TYPE_ANT) {
			logInfo = new AntLogInfo(win32);
		} else if (logType == LOG_TYPE_MAKE) {
			logInfo = new MakeLogInfo(win32);
		} else if (logType == LOG_TYPE_JAR) {
			logInfo = new JarLogInfo(win32);
		} else if (logType == LOG_TYPE_SHELL) {
			logInfo = new ShellLogInfo(win32);
		} else if (logType == LOG_TYPE_CHANGEPERMISSION) {
			logInfo = new ChangePermissionLogInfo(win32);
		} else if (logType == LOG_TYPE_NULL) {
			logInfo = new NullLogInfo(win32);
		} else {
			throw new TriSystemException( SmMessageId.SM004200F , String.valueOf(logType) );
		}
	}

	/**
	 * ログファイルを作成し、logの内容を書き出す
	 */
	public void writeLog(String log ) {

		if ( null == this.log ) {
			return;
		}
		writeString( log, this.log ) ;
	}

	/**
	 *  ログファイルを作成し、logの内容を書き出す
	 */
	public static void writeString(String log, ILog logger ) {

		logger.info( log ) ;

	}

	/**
	 * logにエラーの内容が含まれているかをチェックする
	 */
	public boolean isFailed(String log) {

		String[] errorPattern = logInfo.getErrorPattern();

		for (int i = 0; i < errorPattern.length; i++) {

			if (errorPattern[i].trim().equals("")) {
				continue;
			}

			int result = log.indexOf(errorPattern[i]);
			if (result != -1) {
				return true;
			}
		}

		return false;
	}

	/**
	 * logに成功の内容が含まれているかをチェックする
	 * @param log チェック対象のログ
	 * @return 成功の内容が含まれていればtrue、そうでなければfalse
	 */
	public boolean isSucceed( String log ) {

		String[] successPattern = logInfo.getSuccessPattern();

		for ( int i = 0; i < successPattern.length; i++ ) {

			if ( successPattern[i].trim().equals( "" ) ) {
				continue;
			}

			int result = log.indexOf( successPattern[i] );
			if ( result != -1 ) {
				return true;
			}
		}

		return false;
	}



	/**
	 * エラーログのパターンを取得する
	 */
	public String[] getErrorLogPattern() {
		return logInfo.getErrorPattern();
	}

	public static String getTimeStamp(){
		Date date = new Date();
		DateFormat df = new SimpleDateFormat("yyyyMMdd_HHmmss");

		return df.format(date);
	}
}
