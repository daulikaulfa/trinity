package jp.co.blueship.tri.fw.ui;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.commons.beanutils.BeanUtilsBean;

import jp.co.blueship.tri.fw.cmn.utils.expression.TriResolver;

/**
 * Web アプリケーションにある Servlet コンテキストの変更に関する通知を受け取る処理の実装です。
 *
 * @version V3L03.01
 * @author Yukihiro Eguchi
 *
 */
public class TriResolverListener implements ServletContextListener {

	public void contextDestroyed(ServletContextEvent event) {
	}

	public void contextInitialized(ServletContextEvent event) {
		TriResolver resolver = new TriResolver();
		BeanUtilsBean.getInstance().getPropertyUtils().setResolver(resolver);
	}

}
