package jp.co.blueship.tri.fw.ui;

import java.util.LinkedList;
import java.util.List;

import org.directwebremoting.ScriptBuffer;
import org.directwebremoting.ScriptSession;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;

import jp.co.blueship.tri.fw.act.rb.IBaseResponseBean;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.domain.GenericService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.security.TriAuthenticationContext;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;

/**
 * プレゼンテーション層のテンプレートメソッドクラスです。
 *
 * @author kawakami
 *
 */
abstract public class SecondaryPresentationController extends BaseDWRAction {

	private IGeneralServiceBean retBLBean;
	private GenericService applicationService;

	/**
	 * プレゼンテーション層の処理を行うメソッドです。
	 */

	public final void execute(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo)
	throws Exception {

		try{
			IGeneralServiceBean info = null;
			IGeneralServiceBean bean = null;
			GenericServiceBean gsBean = null;
			IBaseResponseBean resBean = null;

			// 呼び出しサービスの取得
			IService service = this.getService();

			// 前処理を呼び出す。
			this.preProcessor(appInfo, sesInfo, reqInfo);

			// ScriptSessionを取り出す。
			WebContext wctx = WebContextFactory.get();
			ScriptSession session = wctx.getScriptSession();

			// サービスがnullの場合は何もしない。
			if (service != null){
				service.init();

				info = this.getBussinessInfo(appInfo, sesInfo, reqInfo);
				gsBean = this.getApplicationServiceBean(info);

				if( null == this.applicationService ) {
					this.applicationService = this.getApplicationService(service, info);
				}

				List<IDomain<IGeneralServiceBean>> pojoList = null;
				if( null == this.applicationService ) {
					pojoList = new LinkedList<IDomain<IGeneralServiceBean>>();
				} else {
					pojoList = this.applicationService.getPojoList();
				}

				IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
				serviceDto.setServiceBean(gsBean);

				if ( null != pojoList ) {
					for ( IDomain<IGeneralServiceBean> pojo: pojoList ) {
						bean = pojo.execute( serviceDto ).getServiceBean();
					}
				}

				if ( null == bean ) {
					throw new TriSystemException( SmMessageId.SM004031F );
				}
				this.setServiceReturnInformation(bean);
			}

			// レスポンスBeanを取得する。
			resBean = this.getRequestInfo(appInfo, sesInfo, reqInfo);

			// クライアントへの反映
			ScriptBuffer script = new ScriptBuffer();
			script.appendScript( this.getCallbackJSFuncName() )
			    .appendScript("(")
			    .appendData( resBean )
			    .appendScript(");");

			session.addScript(script);

			// 後処理を呼び出す。
			this.postProcessor(appInfo, sesInfo, reqInfo);

			// アプリケーション情報格納処理を行う。
			this.getApplicationInfo(appInfo, sesInfo, reqInfo);
			// セッション情報格納処理を行う。
			this.getSessionInfo(appInfo, sesInfo, reqInfo);

			return;

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * プレゼンテーション層の処理を停止するメソッドです。
	 */
	public final void invalidate(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo)
	throws Exception {

		try{
			// ScriptSessionを取り出す。
			WebContext wctx = WebContextFactory.get();
			ScriptSession session = wctx.getScriptSession();

			if ( false == session.isInvalidated() ) {

				// ScriptSessionのオブジェクトを無効化する
				session.invalidate();
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * サービスコールメソッドです。
	 * 継承先クラスにてサービスのコールを実行します。
	 *
	 * @param service
	 * @param info
	 * @return
	 * @throws Exception
	 * @deprecated
	 */
	abstract protected IGeneralServiceBean callApplicationService(IService service, IGeneralServiceBean info) throws Exception;

	/**
	 * サービスリスト取得メソッドです。
	 * 継承先クラスにてサービスリストを取得します。
	 *
	 * @param service
	 * @param info
	 * @return
	 * @throws Exception
	 */
	abstract protected GenericService getApplicationService(IService service, IGeneralServiceBean info) throws Exception;

	/**
	 * サービスリスト取得メソッドです。
	 * 継承先クラスにてサービスビーンを取得します。
	 *
	 * @param service
	 * @param info
	 * @return
	 * @throws Exception
	 */
	abstract protected GenericServiceBean getApplicationServiceBean(IGeneralServiceBean info) throws Exception;

	/**
	 * アプリケーション層の実行時に受け渡す情報クラスを作成します。
	 * 継承先にて必要な情報を生成し復帰します。
	 *
	 * @return
	 * @throws Exception
	 */
	abstract protected IGeneralServiceBean getBussinessInfo(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception;

	/**
	 * サービスを取得するクラスです。
	 * 継承先にて使用するサービスを復帰します。
	 * @return
	 */
	abstract protected IService getService();

	/**
	 * Forward指定メソッドです。
	 * 正常に処理が終了し、遷移先をアプリケーション層の情報を元に振り分けを行います。
	 * @param info
	 * @return
	 * @deprecated
	 */
	abstract protected String getForward(IRequestInfo reqInfo, ISessionInfo sesInfo, IApplicationInfo appInfo, IGeneralServiceBean info);

	/**
	 * アプリケーションスコープへ格納します。
	 * 継承先にてアプリケーション層の情報をアプリケーションスコープに保持したい場合処理を定義します。
	 *
	 * @param appInfo
	 * @param info
	 * @throws Exception
	 */
	abstract protected void getApplicationInfo(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception;

	/**
	 * セッションスコープへ格納します。
	 * 継承先にてアプリケーション層の情報をセッションスコープに保持したい場合処理を定義します。
	 *
	 * @param info
	 * @throws Exception
	 */
	abstract protected IBaseResponseBean getRequestInfo(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception;

	/**
	 * リクエストスコープへ格納します。
	 * 継承先にてアプリケーション層の情報をリクエストスコープに保持したい場合処理を定義します。
	 * 画面表示を行う情報はここで設定を行います。
	 * @param sesInfo
	 * @param info
	 * @throws Exception
	 */
	abstract protected void getSessionInfo(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception;

	/**
	 * 呼び出したサービスの復帰オブジェクトを取得します。
	 *
	 */
	public IGeneralServiceBean getServiceReturnInformation() {
		return retBLBean;
	}

	/**
	 * 呼び出したサービスの復帰オブジェクトを設定します。
	 *
	 */
	public void setServiceReturnInformation(IGeneralServiceBean retBLBean) {
		this.retBLBean = retBLBean;
	}

	abstract protected void preProcessor(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo);

	abstract protected void postProcessor(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo);

	abstract protected String getCallbackJSFuncName();

	/**
	 * ログイン中ユーザのユーザ名を返します。
	 *
	 * @return ユーザ名
	 */
	protected String userName() {
		TriAuthenticationContext authenticationContext = //
		(TriAuthenticationContext) ContextAdapterFactory.getContextAdapter().getBean(TriAuthenticationContext.BEAN_NAME);

		return authenticationContext.getAuthUserName();

	}
}
