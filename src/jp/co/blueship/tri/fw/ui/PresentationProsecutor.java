package jp.co.blueship.tri.fw.ui;

import jp.co.blueship.tri.fw.act.rb.IBaseResponseBean;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.security.TriAuthenticationContext;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;

public abstract class PresentationProsecutor {
	private static final ILog log = TriLogFactory.getInstance();

	private IGeneralServiceBean retBLBean;
	private BaseBusinessException bbe;

	public final String getCurrentThreadName() {
		return Thread.currentThread().getName();
	}

	public final ILog getLog() {
		return log;
	}

	public PresentationProsecutor(BaseBusinessException bbe) {
		this.bbe = bbe;
	}

	/**
	 * アプリケーション層の実行時に受け渡す情報クラスを作成します。 継承先にて必要な情報を生成し復帰します。
	 *
	 * @return
	 * @throws Exception
	 */
	abstract protected IGeneralServiceBean getBussinessInfo(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception;

	/**
	 * 呼び出したサービスの復帰オブジェクトを取得します。
	 *
	 */
	public IGeneralServiceBean getServiceReturnInformation() {
		return retBLBean;
	}

	/**
	 * 呼び出したサービスの復帰オブジェクトを設定します。
	 *
	 */
	public void setServiceReturnInformation(IGeneralServiceBean retBLBean) {
		this.retBLBean = retBLBean;
	}

	/**
	 * サービスを取得するクラスです。 継承先にて使用するサービスを復帰します。
	 *
	 * @return
	 */
	abstract protected IService getService(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo);

	/**
	 * サービスコールメソッドです。 継承先クラスにてサービスのコールを実行します。
	 *
	 * @param service
	 * @param info
	 * @return
	 * @throws Exception
	 */
	abstract protected IGeneralServiceBean callApplicationService(IService service, IGeneralServiceBean info) throws Exception;

	/**
	 * アプリケーションスコープへ格納します。 継承先にてアプリケーション層の情報をアプリケーションスコープに保持したい場合処理を定義します。
	 *
	 * @param appInfo
	 * @param info
	 * @throws Exception
	 */
	abstract protected void getApplicationInfo(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception;

	/**
	 * セッションスコープへ格納します。 継承先にてアプリケーション層の情報をセッションスコープに保持したい場合処理を定義します。
	 *
	 * @param info
	 * @throws Exception
	 */
	abstract protected IBaseResponseBean getRequestInfo(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception;

	/**
	 * リクエストスコープへ格納します。 継承先にてアプリケーション層の情報をリクエストスコープに保持したい場合処理を定義します。
	 * 画面表示を行う情報はここで設定を行います。
	 *
	 * @param sesInfo
	 * @param info
	 * @throws Exception
	 */
	abstract protected void getSessionInfo(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception;

	abstract protected void preProcessor(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo);

	abstract protected void postProcessor(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo);

	/**
	 * レスポンス情報の格納Keyを取得します。
	 *
	 * @return レスポンスKey
	 */
	abstract protected String getResponseName();

	public BaseBusinessException getBussinessException() {
		return bbe;
	}

	/**
	 * ログイン中ユーザのユーザ名を返します。
	 *
	 * @return ユーザ名
	 */
	protected String userName() {
		TriAuthenticationContext authenticationContext = //
		(TriAuthenticationContext) ContextAdapterFactory.getContextAdapter().getBean(TriAuthenticationContext.BEAN_NAME);

		return authenticationContext.getAuthUserName();

	}

}
