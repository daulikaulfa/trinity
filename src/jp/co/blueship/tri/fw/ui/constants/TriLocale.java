package jp.co.blueship.tri.fw.ui.constants;

import jp.co.blueship.tri.fw.session.constants.TriSessionAttributes;

/**
 * ロケール機能に関する定数を定義するクラスです。
 *
 *
 * @author Takayuki Kubo
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public class TriLocale {

	public static final String ATTRIBUTE_KEY_TRI_LOCAL = TriSessionAttributes.UserLanguage.value();

}
