package jp.co.blueship.tri.fw.ui.constants;

/**
 *
 * 変更管理画面ID
 *
 */
public class ChaLibScreenID {

	/**
	 * 貸出申請一覧画面
	 */
	public static final String LEND_LIST = "ChaLibLendList";

	/**
	 * 貸出申請詳細画面
	 */
	public static final String LEND_DETAIL_VIEW = "ChaLibLendDetailView";

	/**
	 * 貸出申請履歴詳細画面
	 */
	public static final String LEND_HISTORY_DETAIL_VIEW = "ChaLibLendHistoryDetailView";

	/**
	 * 貸出申請画面
	 */
	public static final String LEND_ENTRY = "ChaLibLendEntry";

	/**
	 * 貸出資産選択画面
	 */
	public static final String LEND_ENTRY_ASSET_SELECT = "ChaLibLendEntryAssetSelect";

	/**
	 * 貸出申請確認画面
	 */
	public static final String LEND_ENTRY_CONFIRM = "ChaLibLendEntryConfirm";

	/**
	 * 貸出申請完了画面
	 */
	public static final String COMP_LEND_ENTRY = "ChaLibCompLendEntry";

	/**
	 * 貸出変更画面
	 */
	public static final String LEND_MODIFY = "ChaLibLendModify";

	/**
	 * 貸出変更確認画面
	 */
	public static final String LEND_MODIFY_CONFIRM = "ChaLibLendModifyConfirm";

	/**
	 * 貸出変更完了画面
	 */
	public static final String COMP_LEND_MODIFY = "ChaLibCompLendModify";

	/**
	 * 貸出申請取消完了画面
	 */
	public static final String COMP_LEND_CANCEL = "ChaLibCompLendCancel";

	/**
	 * 返却申請一覧画面
	 */
	public static final String RTN_LIST = "ChaLibRtnList";

	/**
	 * 返却申請詳細画面
	 */
	public static final String RTN_DETAIL_VIEW = "ChaLibRtnDetailView";

	/**
	 * 返却資産確認画面
	 */
	public static final String RTN_ENTRY_CONFIRM = "ChaLibRtnEntryConfirm";

	/**
	 * 返却資産チェック画面
	 */
	public static final String RTN_ENTRY_CHECK = "ChaLibRtnEntryCheck";

	/**
	 * 返却資産申請完了画面
	 */
	public static final String COMP_RTN_ENTRY = "ChaLibCompRtnEntry";

	/**
	 * 返却申請履歴詳細画面
	 */
	public static final String RTN_HISTORY_DETAIL_VIEW = "ChaLibRtnHistoryDetailView";

	/**
	 * テキスト資産比較画面
	 */
	public static final String RTN_RESOURCE_DIFF = "CompareReturnResource";

	/**
	 * 削除申請トップ画面
	 */
	public static final String MASTER_DEL_LIST = "ChaLibMasterDelList";

	/**
	 * 削除申請履歴一覧画面
	 */
	public static final String MASTER_DEL_HISTORY_LIST = "ChaLibMasterDelHistoryList";

	/**
	 * 削除取消申請確認画面
	 */
	public static final String MASTER_DEL_DETAIL_VIEW = "ChaLibMasterDelDetailView";

	/**
	 * 削除取消申請完了画面
	 */
	public static final String COMP_MASTER_DEL_CANCEL = "ChaLibCompMasterDelCancel";

	/**
	 * 削除申請画面
	 */
	public static final String MASTER_DEL_ENTRY = "ChaLibMasterDelEntry";

	/**
	 * 削除申請確認画面
	 */
	public static final String MASTER_DEL_ENTRY_CONFIRM = "ChaLibMasterDelEntryConfirm";

	/**
	 * 削除申請完了画面
	 */
	public static final String COMP_MASTER_DEL_ENTRY = "ChaLibCompMasterDelEntry";

	/**
	 * 削除取消申請履歴詳細画面
	 */
	public static final String MASTER_DEL_HISTORY_DETAIL_VIEW = "ChaLibMasterDelHistoryView";

	/**
	 * 変更管理トップ画面
	 */
	public static final String LIB_TOP = "ChaLibTop";

	/**
	 * 変更管理承認待ち一覧画面
	 */
	public static final String PJT_APPROVE_WAIT_LIST = "ChaLibPjtApproveWaitList";

	/**
	 * 変更管理承認一覧画面
	 */
	public static final String PJT_APPROVE_LIST = "ChaLibPjtApproveList";

	/**
	 * 変更管理申請承認待ち詳細画面
	 */
	public static final String PJT_APPROVE_WAIT_ASSET_APPLY_DETAIL_VIEW = "ChaLibPjtApproveWaitAssetApplyDetailView";
	/**
	 * 変更管理申請承認待ちテキスト資産比較画面
	 */
	public static final String PJT_APPROVE_WAIT_ASSET_APPLY_RESOURCE_DIFF = "ChaLibPjtApproveWaitAssetApplyResourceDiff";

	/**
	 * 変更管理承認詳細画面
	 */
	public static final String PJT_APPROVE_DETAIL_VIEW = "ChaLibPjtApproveDetailView";

	/**
	 * 変更管理承認詳細画面
	 */
	public static final String PJT_APPROVE_ASSET_APPLY_DETAIL_VIEW = "ChaLibPjtApproveAssetApplyDetailView";
	/**
	 * 変更管理承認テキスト資産比較画面
	 */
	public static final String PJT_APPROVE_ASSET_APPLY_RESOURCE_DIFF = "ChaLibPjtApproveAssetApplyResourceDiff";


	/**
	 * 変更管理承認確認画面
	 */
	public static final String PJT_APPROVE_CONFIRM = "ChaLibPjtApproveConfirm";

	/**
	 * 変更管理承認完了画面
	 */
	public static final String COMP_PJT_APPROVE = "ChaLibCompPjtApprove";

	/**
	 * 変更管理承認却下確認画面
	 */
	public static final String PJT_APPROVE_REJECT_CONFIRM = "ChaLibPjtApproveRejectConfirm";

	/**
	 * 変更管理承認却下完了画面
	 */
	public static final String COMP_PJT_APPROVE_REJECT = "ChaLibCompPjtApproveReject";

	/**
	 * 変更管理承認取消確認画面
	 */
	public static final String PJT_APPROVE_CANCEL_CONFIRM = "ChaLibPjtApproveCancelConfirm";

	/**
	 * 変更管理承認取消完了画面
	 */
	public static final String COMP_PJT_APPROVE_CANCEL = "ChaLibCompPjtApproveCancel";

	/**
	 * ロット履歴一覧画面
	 */
	public static final String LOT_HISTORY_LIST = "ChaLibLotHistoryList";

	/**
	 * ロット詳細画面
	 */
	public static final String LOT_DETAIL_VIEW = "ChaLibLotDetailView";

	/**
	 * ロット履歴詳細画面
	 */
	public static final String LOT_HISTORY_DETAIL_VIEW = "ChaLibLotHistoryDetailView";

	/**
	 * ロット作成画面
	 */
	public static final String LOT_ENTRY = "ChaLibLotEntry";

	/**
	 * ロット作成画面
	 */
	public static final String LOT_MODIFY = "ChaLibLotModify";

	/**
	 * ロット作成・モジュール選択画面
	 */
	public static final String LOT_MODULE_SELECT = "ChaLibLotModuleSelect";

	/**
	 * ロット作成・ビルド環境選択画面
	 */
	public static final String LOT_BUILDENV_SELECT = "ChaLibLotBuildEnvSelect";

	/**
	 * ロット作成・リリース環境選択画面
	 */
	public static final String LOT_REL_ENV_SELECT = "ChaLibLotRelEnvSelect";

	/**
	 * ロット作成確認画面
	 */
	public static final String LOT_ENTRY_CONFIRM = "ChaLibLotEntryConfirm";

	/**
	 * ロット変更確認画面
	 */
	public static final String LOT_MODIFY_CONFIRM = "ChaLibLotModifyConfirm";

	/**
	 * ロット作成完了画面
	 */
	public static final String COMP_LOT_ENTRY = "ChaLibCompLotEntry";

	/**
	 * ロット作成完了画面
	 */
	public static final String COMP_LOT_MODIFY = "ChaLibCompLotModify";

	/**
	 * ロットクローズ確認画面
	 */
	public static final String LOT_CLOSE_CONFIRM = "ChaLibLotCloseConfirm";

	/**
	 * ロットクローズ完了画面
	 */
	public static final String COMP_LOT_CLOSE = "ChaLibCompLotClose";

	/**
	 * ロット取消確認画面
	 */
	public static final String LOT_CANCEL_CONFIRM = "ChaLibLotCancelConfirm";

	/**
	 * ロット取消完了画面
	 */
	public static final String COMP_LOT_CANCEL = "ChaLibCompLotCancel";

	/**
	 * 変更管理情報登録画面
	 */
	public static final String PJT_ENTRY_INPUT = "ChaLibPjtEntryInput";

	/**
	 * 変更管理情報登録確認画面
	 */
	public static final String PJT_ENTRY_CONFIRM = "ChaLibPjtEntryConfirm";

	/**
	 * 変更管理情報登録完了画面
	 */
	public static final String COMP_PJT_ENTRY = "ChaLibCompPjtEntry";

	/**
	 * 変更管理情報編集画面
	 */
	public static final String PJT_MODIFY_INPUT = "ChaLibPjtModifyInput";

	/**
	 * 変更管理情報編集確認画面
	 */
	public static final String PJT_MODIFY_CONFIRM = "ChaLibPjtModifyConfirm";

	/**
	 * 変更管理情報編集完了画面
	 */
	public static final String COMP_PJT_MODIFY = "ChaLibCompPjtModify";

	/**
	 * 変更管理詳細画面
	 */
	public static final String PJT_DETAIL_VIEW = "ChaLibPjtDetailView";

	/**
	 * 変更管理履歴詳細画面
	 */
	public static final String PJT_HISTORY_DETAIL_VIEW = "ChaLibPjtHistoryDetailView";

	/**
	 * 変更管理クローズ確認画面
	 */
	public static final String PJT_CLOSE_CONFIRM = "ChaLibPjtCloseConfirm";

	/**
	 * 変更管理クローズ完了画面
	 */
	public static final String COMP_PJT_CLOSE = "ChaLibCompPjtClose";

	/**
	 * 変更管理取消確認画面
	 */
	public static final String PJT_CANCEL_CONFIRM = "ChaLibPjtCancelConfirm";

	/**
	 * 変更管理取消完了画面
	 */
	public static final String COMP_PJT_CANCEL = "ChaLibCompPjtCancel";

	/**
	 * 変更管理テスト完了確認画面
	 */
	public static final String PJT_TEST_COMPLETE_CONFIRM = "ChaLibPjtTestCompleteConfirm";

	/**
	 * 変更管理テスト完了画面
	 */
	public static final String COMP_PJT_TEST_COMPLETE = "ChaLibCompPjtTestComplete";

	/**
	 * コンフリクトチェック ロット一覧画面
	 */
	public static final String CONFLICTCHECK_LOT_LIST = "ChaLibConflictCheckLotList";

	/**
	 * コンフリクトチェック ベースライン一覧画面
	 */
	public static final String CONFLICTCHECK_BASELINE_LIST = "ChaLibConflictCheckBaselineList";

	/**
	 * コンフリクトチェック ベースライン詳細画面
	 */
	public static final String CONFLICTCHECK_BASELINE_DETAIL_VIEW = "ChaLibConflictCheckBaselineDetailView";

	/**
	 * コンフリクトチェック 確認画面
	 */
	public static final String CONFLICTCHECK_CONFIRM = "ChaLibConflictCheckConfirm";

	/**
	 * コンフリクトチェック 完了画面
	 */
	public static final String COMP_CONFLICTCHECK = "ChaLibCompConflictCheck";

	/**
	 * マージ ロット一覧画面
	 */
	public static final String MERGE_LOT_LIST = "ChaLibMergeLotList";

	/**
	 * マージ ベースライン詳細画面
	 */
	public static final String MERGE_BASELINE_DETAIL_VIEW = "ChaLibMergeBaselineDetailView";

	/**
	 * マージ コンフリクトチェック結果一覧画面
	 */
	public static final String MERGE_CONFLICT_CHECK_RESULT = "ChaLibMergeConflictCheckResult";

	/**
	 * マージ コンフリクトチェック結果画面（２点）
	 */
	public static final String MERGE_CONFLICT_CHECK_RESULT_DIFF = "ChaLibMergeConflictCheckResultDiff";

	/**
	 * マージ コンフリクトチェック結果画面（３点）
	 */
	public static final String MERGE_CONFLICT_CHECK_RESULT_TRINOMIAL_DIFF = "ChaLibMergeConflictCheckResultTrinomialDiff";

	/**
	 * マージコミット 確認画面
	 */
	public static final String MERGECOMMIT_CONFIRM = "ChaLibMergeCommitConfirm";

	/**
	 * マージコミット 完了画面
	 */
	public static final String COMP_MERGECOMMIT = "ChaLibCompMergeCommit";

	/**
	 * マージ マージ履歴ロット一覧画面
	 */
	public static final String MERGE_HISTORY_LOT_LIST = "ChaLibMergeHistoryLotList";

	/**
	 * マージ マージ履歴ベースライン一覧画面
	 */
	public static final String MERGE_HISTORY_BASELINE_LIST = "ChaLibMergeHistoryBaselineList";

	/**
	 * マージ マージ履歴ベースライン詳細画面
	 */
	public static final String MERGE_HISTORY_BASELINE_DETAIL_VIEW = "ChaLibMergeHistoryBaselineDetailView";

	/**
	 * レポート一覧画面
	 */
	public static final String REPORT_APPLY_LIST = "ChaLibReportApplyList";

}

