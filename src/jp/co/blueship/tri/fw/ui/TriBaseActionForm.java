package jp.co.blueship.tri.fw.ui;

import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.FormFile;

public class TriBaseActionForm extends ActionForm {

	/**
	 * 修正したらインクリメントすること
	 */
	private static final long serialVersionUID = 2L;
	
	private String message = "";
	private FormFile appendFile1;
	private FormFile appendFile2;
	private FormFile appendFile3;
	private FormFile appendFile4;
	private FormFile appendFile5;
	private FormFile appendFile6;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public FormFile getAppendFile1() {
		return appendFile1;
	}

	public void setAppendFile1(FormFile appendFile1) {
		this.appendFile1 = appendFile1;
	}

	public FormFile getAppendFile2() {
		return appendFile2;
	}

	public void setAppendFile2(FormFile appendFile2) {
		this.appendFile2 = appendFile2;
	}

	public FormFile getAppendFile3() {
		return appendFile3;
	}

	public void setAppendFile3(FormFile appendFile3) {
		this.appendFile3 = appendFile3;
	}

	public FormFile getAppendFile4() {
		return appendFile4;
	}

	public void setAppendFile4(FormFile appendFile4) {
		this.appendFile4 = appendFile4;
	}

	public FormFile getAppendFile5() {
		return appendFile5;
	}

	public void setAppendFile5(FormFile appendFile5) {
		this.appendFile5 = appendFile5;
	}

	public FormFile getAppendFile6() {
		return appendFile6;
	}

	public void setAppendFile6(FormFile appendFile6) {
		this.appendFile6 = appendFile6;
	}

}
