package jp.co.blueship.tri.fw.ui;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.blueship.tri.fw.act.rb.IBaseResponseBean;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.annotations.VisibleForTesting;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionInfoFactory;
import jp.co.blueship.tri.fw.session.application.ApplicationInfoFactory;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.session.request.RequestInfoFactory;
import jp.co.blueship.tri.fw.sm.ui.portal.rb.SCRE0000ResponseBean;

/**
 *
 * @version V3L10.02
 * @author Takashi Ono
 *
 */
abstract public class BaseDWRAction {

	

	/**
	 * Application Context Instanceを取得するKeyです。
	 *
	 * @see ApplicationInfoFactory
	 */
	private static String APPILICATION_CONTEXT_KEY = ApplicationInfoFactory.ASSIGN_SERVLETCONTEXT;
	/**
	 * Session Context Instanceを取得するKeyです。
	 *
	 * @see SessionInfoFactory
	 */
	private static String SESSION_CONTEXT_KEY = SessionInfoFactory.ASSIGN_HTTPSESSION;
	/**
	 * Request Context Instanceを取得するKeyです。
	 *
	 * @see RequestInfoFactory
	 */
	private static String REQUEST_CONTEXT_KEY = RequestInfoFactory.ASSIGN_HTTPSERVLETREUEST;

	/**
	 * プレゼンテーション層の処理を行うメソッドです。
	 */
	public IBaseResponseBean execute(HttpServletRequest request, HttpServletResponse response) throws Exception {

		try {

			IApplicationInfo appInfo = getIApplicationInfo(request);
			ISessionInfo sesInfo = getISessionInfo(request);
			IRequestInfo reqInfo = getIRequestInfo(request);
			execute(appInfo, sesInfo, reqInfo);
			return (IBaseResponseBean) reqInfo.getAttribute("responseBean");
		} catch (BusinessException be) {

			be.printStackTrace();
			IRequestInfo reqInfo = getIRequestInfo(request);
			reqInfo.setAttribute("message", interpolateMessage(be));
			return (IBaseResponseBean) reqInfo.getAttribute("responseBean");
		} catch (Throwable e) {

			request.getSession().invalidate();
			e.printStackTrace();
			populateErrorMessageToRequest(e, request);
			throw new Exception(e);
		}
	}

	/**
	 * プレゼンテーション層の処理を停止するメソッドです。
	 */
	public IBaseResponseBean invalidate(HttpServletRequest request, HttpServletResponse response) throws Exception {

		try {

			IApplicationInfo appInfo = getIApplicationInfo(request);
			ISessionInfo sesInfo = getISessionInfo(request);
			IRequestInfo reqInfo = getIRequestInfo(request);
			invalidate(appInfo, sesInfo, reqInfo);
			return (IBaseResponseBean) reqInfo.getAttribute("responseBean");
		} catch (BusinessException be) {

			be.printStackTrace();
			IRequestInfo reqInfo = getIRequestInfo(request);
			reqInfo.setAttribute("message", interpolateMessage(be));
			return (IBaseResponseBean) reqInfo.getAttribute("responseBean");
		} catch (Throwable e) {

			request.getSession().invalidate();
			e.printStackTrace();
			populateErrorMessageToRequest(e, request);
			throw new Exception(e);
		}
	}

	/**
	 * 指定された例外からメッセージIDを取得し、メッセージ文言へ変換後HTTPリクエストオブジェクトに設定します。
	 *
	 * @param e 例外
	 * @param request HTTPリクエストオブジェクト
	 */
	@VisibleForTesting
	protected static final void populateErrorMessageToRequest(Throwable e, HttpServletRequest request) {

		SCRE0000ResponseBean resBean = new SCRE0000ResponseBean();
		resBean.new Utility().reflectResponseBean(toTranslatable(e));
		request.setAttribute("responseBean", resBean);
	}

	private static ITranslatable toTranslatable(Throwable e) {

		if (ITranslatable.class.isInstance(e)) {
			return (ITranslatable) e;
		}

		return new TriSystemException(SmMessageId.SM001019E, e);
	}

	private static String interpolateMessage(ITranslatable e) {

		PreConditions.assertOf(ITranslatable.class.isInstance(e), "Exception is not Inastance of Translatable");

		return ContextAdapterFactory.getContextAdapter().getMessage(e.getMessageID(), e.getMessageArgs());
	}

	/**
	 * アプリケーションコンテキストクラスを取得するクラスです。
	 *
	 * @param request HttpServletRequest
	 * @see ApplicationInfoFactory
	 * @return アプリケーションコンテキスト
	 */
	private IApplicationInfo getIApplicationInfo(HttpServletRequest request) {

		IApplicationInfo applicationInfo = ApplicationInfoFactory.getApplicationContext(BaseDWRAction.APPILICATION_CONTEXT_KEY);
		applicationInfo.init(request);
		return applicationInfo;
	}

	/**
	 * セッションコンテキストクラスを取得するクラスです。
	 *
	 * @param request HttpServletRequest
	 * @see SessionInfoFactory
	 * @return セッションコンテキスト
	 */
	private ISessionInfo getISessionInfo(HttpServletRequest request) {

		ISessionInfo sessionInfo = SessionInfoFactory.getSessionContext(BaseDWRAction.SESSION_CONTEXT_KEY);
		sessionInfo.init(request);
		return sessionInfo;
	}

	/**
	 * リクエストコンテキストクラスを取得するクラスです。
	 *
	 * @param request HttpServletRequest
	 * @see RequestInfoFactory
	 * @return リクエストコンテキスト
	 */
	private IRequestInfo getIRequestInfo(HttpServletRequest request) {

		IRequestInfo requestInfo = RequestInfoFactory.getRequestContext(BaseDWRAction.REQUEST_CONTEXT_KEY);
		requestInfo.init(request);
		return requestInfo;
	}

	abstract protected void execute(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception;

	abstract protected void invalidate(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception;
}
