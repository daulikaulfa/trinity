package jp.co.blueship.tri.fw.ui;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import jp.co.blueship.tri.fw.act.IParameterCashe;
import jp.co.blueship.tri.fw.act.NotEndProcessException;
import jp.co.blueship.tri.fw.act.NotInitilizeException;
import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;
import jp.co.blueship.tri.fw.act.rb.IBaseResponseBean;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionMultiWindows;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;

/**
 * プレゼンテーション層のテンプレートメソッドクラスです。
 *
 * @author kawakami
 *
 */
abstract public class PresentationController extends BaseStrutsAction {
	private static final ILog log = TriLogFactory.getInstance();

	/**
	 * プレゼンテーション層の処理を行うメソッドです。
	 */

	public final String execute(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo)
	throws Exception
	{
		PresentationProsecutorManager ppm = new PresentationProsecutorManager();
		SessionMultiWindows ses = new SessionMultiWindows(sesInfo, reqInfo);

		//必要に応じて、windwosIdに関連するセッション情報を初期化
		if ( ! ses.isWindwosIdInit() ) {
			ses.removeAllAttribute();
		}

		try{

			this.addPresentationProsecutores(ppm);
			// 属性の設定を開始します。
			executeAttributeStart(appInfo, sesInfo, reqInfo);
			// サービスの呼び出しを行う。
			executeCallService(ppm.presentationProsecutor, appInfo, sesInfo, reqInfo);
			// アプリケーション情報格納処理を行う。
			executeSetApplicationInformation(ppm.presentationProsecutor, appInfo, sesInfo, reqInfo);
			// セッション情報格納処理を行う。
			executeSetSessionInformation(ppm.presentationProsecutor, appInfo, sesInfo, reqInfo);
			// リクエスト情報格納処理を行う。
			executeCreateResponseInformation(ppm.presentationProsecutor, appInfo, sesInfo, reqInfo);
			// リクエスト・アプリケーション・セッションのコミットを行う。
			executeAttributeCommision(appInfo, sesInfo, reqInfo);
			// 遷移先を指定する(struts-configに紐づく文字列を復帰)
			return this.getForward(ppm, reqInfo, sesInfo, appInfo);
		}catch(BaseBusinessException bbe){
			LogHandler.debug( log , "start BussinessException Process:" + this.getClass() + "  MESSAGE:" + bbe.getMessage());
			LogHandler.error( log , bbe ) ;

			this.addBusinessErrorPresentationProsecutores(ppm, bbe);
			this.addBusinessErrorPresentationProsecutores(ppm, reqInfo, sesInfo, appInfo, bbe);
			// 属性の設定をロールバックします。
			executeAttributeRollback(appInfo, sesInfo, reqInfo);
			// 属性の設定を開始します。
			executeAttributeStart(appInfo, sesInfo, reqInfo);
			// サービスの呼び出しを行う。
			executeCallBussinessErrorService(ppm.bePresentationProsecutor, appInfo, sesInfo, reqInfo, bbe);
			// アプリケーション情報格納処理を行う。
			executeSetBussinessErrorApplicationInformation(ppm.bePresentationProsecutor, appInfo, sesInfo, reqInfo, bbe);
			// セッション情報格納処理を行う。
			executeSetBussinessErrorSessionInformation(ppm.bePresentationProsecutor, appInfo, sesInfo, reqInfo, bbe);
			// リクエスト情報格納処理を行う。
			executeCreateBussinessErrorResponseInformation(ppm.bePresentationProsecutor, appInfo, sesInfo, reqInfo, bbe);
			// リクエスト・アプリケーション・セッションのコミットを行う。
			executeAttributeCommision(appInfo, sesInfo, reqInfo);
			// 遷移先を指定する(struts-configに紐づく文字列を復帰)
			return this.getForwardForBusinessException(ppm,reqInfo, sesInfo, appInfo, bbe);
		}catch (Exception e) {
//			reqInfo.setAttribute(TriLocale.ATTRIBUTE_KEY_TRI_LOCAL, SystemProps.TriSystemLanguage.getProperty());
			LogHandler.fatal( log , e ) ;
			throw e;
		}
	}
	private void executeSetBussinessErrorSessionInformation(PresentationProsecutor bePresentationProsecutor, IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo, BaseBusinessException bbe) throws Exception {
		PresentationProsecutor pp = bePresentationProsecutor;
		// レスポンスBeanを取得する。
		pp.getSessionInfo(appInfo, sesInfo, reqInfo);
	}
	private void executeSetBussinessErrorApplicationInformation(PresentationProsecutor bePresentationProsecutor, IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo, BaseBusinessException bbe) throws Exception {
		PresentationProsecutor pp = bePresentationProsecutor;
		// レスポンスBeanを取得する。
		pp.getApplicationInfo(appInfo, sesInfo, reqInfo);
	}

	private void executeCreateBussinessErrorResponseInformation(PresentationProsecutor bePresentationProsecutor, IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo, BaseBusinessException bbe) throws Exception {
//		reqInfo.setAttribute(TriLocale.ATTRIBUTE_KEY_TRI_LOCAL, SystemProps.TriSystemLanguage.getProperty());

		PresentationProsecutor pp = bePresentationProsecutor;
		// レスポンスBeanを取得する。
		IBaseResponseBean resBean = pp.getRequestInfo(appInfo, sesInfo, reqInfo);

		//▼
		SessionMultiWindows ses = new SessionMultiWindows(sesInfo, reqInfo);
		this.mapSessionInfo(ses, resBean, reqInfo);
		//▲
		reqInfo.setAttribute(pp.getResponseName(), resBean);
	}
	private void executeAttributeStart(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) {
		try {
			if (reqInfo instanceof IParameterCashe){
					((IParameterCashe)reqInfo).start();
			}
			if (sesInfo instanceof IParameterCashe){
				((IParameterCashe)sesInfo).start();
			}
			if (appInfo instanceof IParameterCashe){
				((IParameterCashe)appInfo).start();
			}
		} catch (NotEndProcessException e) {
			throw new TriSystemException( SmMessageId.SM005067S , e , "NotEndProcessError" );
		}
	}

	private void executeAttributeRollback(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) {
		try {
			if (reqInfo instanceof IParameterCashe){
					((IParameterCashe)reqInfo).rollback();
			}
			if (sesInfo instanceof IParameterCashe){
				((IParameterCashe)sesInfo).rollback();
			}
			if (appInfo instanceof IParameterCashe){
				((IParameterCashe)appInfo).rollback();
			}
		} catch (NotInitilizeException e) {
			throw new TriSystemException( SmMessageId.SM005067S , e , "NotInitilizeError" );
		}
	}

	private void executeAttributeCommision(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) {
		try {
			if (reqInfo instanceof IParameterCashe){
					((IParameterCashe)reqInfo).commit();
			}
			if (sesInfo instanceof IParameterCashe){
				((IParameterCashe)sesInfo).commit();
			}
			if (appInfo instanceof IParameterCashe){
				((IParameterCashe)appInfo).commit();
			}
		} catch (NotInitilizeException e) {
			throw new TriSystemException( SmMessageId.SM005067S , e , "NotInitilizeError" );
		}
	}

	private void executeSetApplicationInformation(List<?> presentationProsecutor, IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception  {
		Iterator<?> ppIterator = presentationProsecutor.iterator();

		while (ppIterator.hasNext()){
			PresentationProsecutor pp = (PresentationProsecutor)ppIterator.next();
			// レスポンスBeanを取得する。
			pp.getApplicationInfo(appInfo, sesInfo, reqInfo);
		}
	}

	private void executeSetSessionInformation(List<?> presentationProsecutor, IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {
		Iterator<?> ppIterator = presentationProsecutor.iterator();

		while (ppIterator.hasNext()){
			PresentationProsecutor pp = (PresentationProsecutor)ppIterator.next();
			// レスポンスBeanを取得する。
			pp.getSessionInfo(appInfo, sesInfo, reqInfo);
		}
	}

	private void executeCreateResponseInformation(List<?> presentationProsecutor, IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {

//		reqInfo.setAttribute(TriLocale.ATTRIBUTE_KEY_TRI_LOCAL, SystemProps.TriSystemLanguage.getProperty());

		IBaseResponseBean resBean;
		Iterator<?> ppIterator = presentationProsecutor.iterator();

		while (ppIterator.hasNext()){
			PresentationProsecutor pp = (PresentationProsecutor)ppIterator.next();
			// レスポンスBeanを取得する。
			resBean = pp.getRequestInfo(appInfo, sesInfo, reqInfo);

			//▼
			SessionMultiWindows ses = new SessionMultiWindows(sesInfo, reqInfo);
			this.mapSessionInfo(ses, resBean, reqInfo);
			//▲
			reqInfo.setAttribute(pp.getResponseName(), resBean);
		}

	}
	private void executeCallBussinessErrorService(
			PresentationProsecutor bePresentationProsecutor,
			IApplicationInfo appInfo,
			ISessionInfo sesInfo,
			IRequestInfo reqInfo,
			BaseBusinessException bbe) throws Exception {
		IGeneralServiceBean bean = null;

		PresentationProsecutor pp = bePresentationProsecutor;

		if(pp == null){
			throw new TriSystemException(SmMessageId.SM005066S , bbe.getMessage() );
		}

		// 前処理を呼び出す。
		pp.preProcessor( appInfo, sesInfo, reqInfo );

		// 呼び出しサービスの取得
		IService service = pp.getService( appInfo, sesInfo, reqInfo );


		// サービスがnullの場合は何もしない。1
		if (service != null){
			service.init();
			bean = pp.callApplicationService(service, pp.getBussinessInfo(appInfo, sesInfo, reqInfo));
			pp.setServiceReturnInformation(bean);
		}

		// 後処理を呼び出す。
		pp.postProcessor(appInfo, sesInfo, reqInfo);

	}

	private void executeCallService(List<?> presentationProsecutor, IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {
		IGeneralServiceBean bean = null;

		Iterator<?> ppIterator = presentationProsecutor.iterator();
		while (ppIterator.hasNext()){
			PresentationProsecutor pp = (PresentationProsecutor)ppIterator.next();

			// 前処理を呼び出す。
			pp.preProcessor( appInfo, sesInfo, reqInfo );

			// 呼び出しサービスの取得
			IService service = pp.getService( appInfo, sesInfo, reqInfo );


			// サービスがnullの場合は何もしない。
			if (service != null){
				service.init();
				bean = pp.callApplicationService(service, pp.getBussinessInfo(appInfo, sesInfo, reqInfo));
				if ( null == bean ) {
					throw new TriSystemException( SmMessageId.SM004031F );
				}
				pp.setServiceReturnInformation(bean);
			}

			// 後処理を呼び出す。
			pp.postProcessor(appInfo, sesInfo, reqInfo);

		}
	}

	protected void addBusinessErrorPresentationProsecutores(PresentationProsecutorManager ppm, BaseBusinessException bbe) {
		// nop
	}

	protected void addBusinessErrorPresentationProsecutores(PresentationProsecutorManager ppm, IRequestInfo reqInfo, ISessionInfo sesInfo, IApplicationInfo appInfo, BaseBusinessException bbe) {
		// nop
	}

	protected void addPresentationProsecutores(PresentationProsecutorManager ppm) {
		// nop
	}

	abstract protected String getForwardForBusinessException(PresentationProsecutorManager ppm, IRequestInfo reqInfo, ISessionInfo sesInfo, IApplicationInfo appInfo, TriRuntimeException bbe);
	abstract protected String getForward(PresentationProsecutorManager ppm, IRequestInfo reqInfo, ISessionInfo sesInfo, IApplicationInfo appInfo);

	protected class PresentationProsecutorManager{

		List<PresentationProsecutor> presentationProsecutor = new ArrayList<PresentationProsecutor>();
		PresentationProsecutor bePresentationProsecutor;

		public final void addPresentationProsecutor(PresentationProsecutor addInstance){
			this.presentationProsecutor.add(addInstance);
		}

		public final PresentationProsecutor getPresentationProsecutor(int i){
			return this.presentationProsecutor.get(i);
		}

		public final void addBusinessErrorPresentationProsecutor(PresentationProsecutor addInstance){
			this.bePresentationProsecutor = addInstance;
		}

		public final PresentationProsecutor getBusinessErrorPresentationProsecutor(){
			return this.bePresentationProsecutor;
		}

	}

	private void mapSessionInfo( SessionMultiWindows sesInfo, IBaseResponseBean resBean, IRequestInfo reqInfo ) {
		((BaseResponseBean)resBean).setWindowsId( sesInfo.getWindwosId() );
		((BaseResponseBean)resBean).setSavedPageNo(reqInfo.getParameter("savedPageNo"));
		((BaseResponseBean)resBean).setFlowActionId( (String)sesInfo.getAttribute(SessionScopeKeyConsts.FLOW_ACTION_ID) );

		//▼
		((BaseResponseBean)resBean).setAction( (String)sesInfo.getAttribute(SessionScopeKeyConsts.ACTION) );
		((BaseResponseBean)resBean).setActionDetail( (String)sesInfo.getAttribute(SessionScopeKeyConsts.ACTION_DETAIL) );
		((BaseResponseBean)resBean).setActionDetailView( (String)sesInfo.getAttribute(SessionScopeKeyConsts.ACTION_DETAIL_VIEW) );
		((BaseResponseBean)resBean).setActionDownLoad( (String)sesInfo.getAttribute(SessionScopeKeyConsts.ACTION_DOWNLOAD) );
		((BaseResponseBean)resBean).setActionIssuance( (String)sesInfo.getAttribute(SessionScopeKeyConsts.ACTION_ISSUANCE) );
		((BaseResponseBean)resBean).setActionSearch( (String)sesInfo.getAttribute(SessionScopeKeyConsts.ACTION_SEARCH) );
		((BaseResponseBean)resBean).setActionSearchBtn( (String)sesInfo.getAttribute(SessionScopeKeyConsts.ACTION_SEARCH_BTN) );
		//▲
	}
}
