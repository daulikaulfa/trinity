// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3)
// Source File Name:   SjisActionServlet.java

package jp.co.blueship.tri.fw.ui;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.struts.action.ActionServlet;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import jp.co.blueship.tri.fw.agent.concurrent.TriTaskExecutor;
import jp.co.blueship.tri.fw.cmn.io.constants.Charset;
import jp.co.blueship.tri.fw.cmn.utils.SystemProps;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.annotations.VisibleForTesting;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.spring.Contexts;
import jp.co.blueship.tri.fw.session.request.RequestInfoFactory;
import jp.co.blueship.tri.fw.ui.constants.TriLocale;

/**
 *
 * @version V3L10R01
 *
 * @version SP-201511-1_V3L14R01
 * @author Yukihiro Eguchi
 */
public class SjisActionServlet extends ActionServlet {

	private static final long serialVersionUID = 1L;

	public SjisActionServlet() {
	}

	/**
	 * Springの設定ファイルロード処理の変更に伴い,
	 * ServletContextに格納されたApplicationContextをContextsクラスへsetする処理を追加 t.ono
	 */
	@Override
	public void init() throws ServletException {
		super.init();

		ApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
		Contexts.getInstance().setApplicationContext(appContext);
		getServletContext().setAttribute(TriLocale.ATTRIBUTE_KEY_TRI_LOCAL, SystemProps.TriSystemLanguage.getProperty());

	}

	protected void process(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		String charset = super.getInitParameter(RequestInfoFactory.SERVLET_CHARSET);

		if ( ServletFileUpload.isMultipartContent(request)
			|| TriStringUtils.isEmpty(charset) ) {
				request.setCharacterEncoding( Charset.getDefaultCharset().value() );
		} else {
			request.setCharacterEncoding( charset );
		}

		super.process(request, response);
	}

	/**
	 * このServlertが破棄されるタイミングでServletContainerから呼ばれます。<br/>
	 * ここでは、スレッドプール機構のシャットダウン処理を行います。
	 *
	 * @see org.apache.struts.action.ActionServlet#destroy()
	 */
	@Override
	public void destroy() {
		shutDownTriTaskExecutor();
		super.destroy();
	}

	/**
	 * スレッドプール機構のシャットダウン処理を行います。
	 */
	@VisibleForTesting
	protected final void shutDownTriTaskExecutor() {

		TriTaskExecutor executor = (TriTaskExecutor) ContextAdapterFactory.//
				getContextAdapter().//
				getBean(TriTaskExecutor.BEAN_NAME);
		if (executor == null || !executor.isActive()) {
			return;
		}

		executor.shutDown();
	}

}
