package jp.co.blueship.tri.fw.ui;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;

import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.annotations.VisibleForTesting;
import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.cmn.utils.function.TriClojureEx;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionInfoFactory;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts.AppendFileEnum;
import jp.co.blueship.tri.fw.session.application.ApplicationInfoFactory;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.session.request.RequestInfoFactory;
import jp.co.blueship.tri.fw.sm.ui.portal.rb.SCRE0000ResponseBean;

/**
 *
 * @version V3L10.02
 * @author Takashi Ono
 *
 * @version SP-201511-1_V3L14R01
 * @author Yukihiro Eguchi
 */
abstract public class BaseStrutsAction extends Action {

	private static final ILog log = TriLogFactory.getInstance();

	private static final String ACTION_FORWARD_ERROR_BUSSINESS = "error_bussiness";
	/**
	 * Application Context Instanceを取得するKeyです。
	 *
	 * @see ApplicationInfoFactory
	 */
	private static String APPILICATION_CONTEXT_KEY = ApplicationInfoFactory.ASSIGN_SERVLETCONTEXT;
	/**
	 * Session Context Instanceを取得するKeyです。
	 *
	 * @see SessionInfoFactory
	 */
	private static String SESSION_CONTEXT_KEY = SessionInfoFactory.ASSIGN_HTTPSESSION;
	/**
	 * Request Context Instanceを取得するKeyです。
	 *
	 * @see RequestInfoFactory
	 */
	private static String REQUEST_CONTEXT_KEY = RequestInfoFactory.ASSIGN_HTTPSERVLETREUEST;

	/**
	 * プレゼンテーション層の処理を行うメソッドです。
	 */

	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {

		try {

			// アップロードファイルをリクエスト情報に設定します。
			populateUploadFileToRequest(form, request);

			if (isDownload(form)) {
				download(response);
				return mapping.findForward(null);
			}

			IApplicationInfo appInfo = getIApplicationInfo(request);
			ISessionInfo sesInfo = getISessionInfo(request);
			IRequestInfo reqInfo = getIRequestInfo(request);

			return mapping.findForward(execute(appInfo, sesInfo, reqInfo));
		} catch (BusinessException be) {
			return mapping.findForward(ACTION_FORWARD_ERROR_BUSSINESS);
		} catch (Throwable e) {

			LogHandler.fatal(log, e);
			populateErrorMessageToRequest(e, request);
			request.getSession().invalidate();
			throw new Exception(e);

		}
	}

	private void populateUploadFileToRequest(ActionForm form, HttpServletRequest request) throws Exception {

		TriCollectionUtils.forEachEx(AppendFileEnum.values(), toRequest((TriBaseActionForm) form, request));
	}

	private TriClojureEx<AppendFileEnum, Exception> toRequest(final TriBaseActionForm actionForm, final HttpServletRequest request) {

		return new TriClojureEx<AppendFileEnum, Exception>() {

			@Override
			public boolean apply(AppendFileEnum appendFileEnum) throws Exception {

				FormFile file = (FormFile) TriPropertyUtils.getProperty(actionForm, "appendFile" + Integer.valueOf(appendFileEnum.getId()));
				if (null == file) {
					return true;
				}

				request.setAttribute(appendFileEnum.getFileName(), file.getFileName());
				request.setAttribute(appendFileEnum.getFileInputStream(), file.getInputStream());
				request.setAttribute(appendFileEnum.getFileSize(), new Integer(file.getFileSize()));
				return true;
			}
		};

	}

	private static boolean isDownload(ActionForm form) {
		return "download".equals(((TriBaseActionForm) form).getMessage());
	}

	/**
	 * 指定された例外からメッセージIDを取得し、メッセージ文言へ変換後HTTPリクエストオブジェクトに設定します。
	 *
	 * @param e 例外
	 * @param request HTTPリクエストオブジェクト
	 */
	@VisibleForTesting
	protected static final void populateErrorMessageToRequest(Throwable e, HttpServletRequest request) {

		SCRE0000ResponseBean resBean = new SCRE0000ResponseBean();
		resBean.new Utility().reflectResponseBean(toTranslatable(e));
		request.setAttribute("responseBean", resBean);
	}

	private static ITranslatable toTranslatable(Throwable e) {

		if (ITranslatable.class.isInstance(e)) {
			return (ITranslatable) e;
		}

		return new TriSystemException(SmMessageId.SM001019E, e);
	}

	/**
	 * アプリケーションコンテキストクラスを取得するクラスです。
	 *
	 * @param request HttpServletRequest
	 * @see ApplicationInfoFactory
	 * @return アプリケーションコンテキスト
	 */
	private IApplicationInfo getIApplicationInfo(HttpServletRequest request) {
		/*
		 * IApplicationInfo appInfo =
		 * ApplicationInfoFactory.getApplicationContext
		 * (BaseStrutsAction.APPILICATION_CONTEXT_KEY); return appInfo;
		 */
		IApplicationInfo applicationInfo = ApplicationInfoFactory.getApplicationContext(BaseStrutsAction.APPILICATION_CONTEXT_KEY);
		applicationInfo.init(request);
		return applicationInfo;
	}

	/**
	 * セッションコンテキストクラスを取得するクラスです。
	 *
	 * @param request HttpServletRequest
	 * @see SessionInfoFactory
	 * @return セッションコンテキスト
	 */
	private ISessionInfo getISessionInfo(HttpServletRequest request) {
		/*
		 * ISessionInfo sesInfo =
		 * SessionInfoFactory.getSessionContext(BaseStrutsAction
		 * .SESSION_CONTEXT_KEY); return sesInfo;
		 */
		ISessionInfo sessionInfo = SessionInfoFactory.getSessionContext(BaseStrutsAction.SESSION_CONTEXT_KEY);
		sessionInfo.init(request);
		return sessionInfo;
	}

	/**
	 * リクエストコンテキストクラスを取得するクラスです。
	 *
	 * @param request HttpServletRequest
	 * @see RequestInfoFactory
	 * @return リクエストコンテキスト
	 */
	private IRequestInfo getIRequestInfo(HttpServletRequest request) {
		/*
		 * IRequestInfo reqInfo =
		 * RequestInfoFactory.getRequestContext(BaseStrutsAction
		 * .REQUEST_CONTEXT_KEY); return reqInfo;
		 */
		IRequestInfo requestInfo = RequestInfoFactory.getRequestContext(BaseStrutsAction.REQUEST_CONTEXT_KEY);
		requestInfo.init(request);
		requestInfo.setCharacterEncoding( this.getServlet().getInitParameter(RequestInfoFactory.SERVLET_ENCODING) );

		return requestInfo;
	}

	abstract public String execute(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception;

	protected void download(HttpServletResponse response) throws Exception {
	};
}