package jp.co.blueship.tri.fw.support;

import jp.co.blueship.tri.fw.sm.support.ISmFinderSupport;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 * FW関連情報を検索するためのサービス機能を提供するインタフェースです。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public interface IFwFinderSupport {
	/**
	 * smFinderSupportを取得します。
	 * @return smFinderSupport
	 */
	public ISmFinderSupport getSmFinderSupport();
	/**
	 * umFinderSupportを取得します。
	 * @return umFinderSupport
	 */
	public IUmFinderSupport getUmFinderSupport();

}
