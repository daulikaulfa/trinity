package jp.co.blueship.tri.fw.support;

import jp.co.blueship.tri.fw.sm.support.ISmFinderSupport;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;



/**
 * データアクセスを支援するファインダー
 * <br>データアクセスを支援するサポートクラスです。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FwFinderSupport implements IFwFinderSupport {
	private ISmFinderSupport smFinderSupport = null;
	private IUmFinderSupport umFinderSupport = null;

	@Override
	public ISmFinderSupport getSmFinderSupport() {
	    return smFinderSupport;
	}
	public void setSmFinderSupport(ISmFinderSupport smFinderSupport) {
	    this.smFinderSupport = smFinderSupport;
	}
	public IUmFinderSupport getUmFinderSupport() {
	    return umFinderSupport;
	}
	public void setUmFinderSupport(IUmFinderSupport umFinderSupport) {
	    this.umFinderSupport = umFinderSupport;
	}

}
