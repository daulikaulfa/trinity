package jp.co.blueship.tri.fw.agent.core;

import java.io.File;

import jp.co.blueship.tri.fw.cmn.io.constants.Charset;
import jp.co.blueship.tri.fw.cmn.utils.SystemProps;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriSystemUtils;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogUtil;
import jp.co.blueship.tri.fw.log.LogUtilLog4j;
import jp.co.blueship.tri.fw.msg.SmMessageId;

/**
 * 外部ツール（Ant ビルド）を実行します。 </p> [概要] <br>
 * コンパイルグループ（言語種別、環境）ごとに呼び出されます。 <br>
 * Java以外の言語環境で、このクラスが呼び出されると、何もしません。
 *
 * </p>
 *
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Nguyen Van Chung
 */
public class ActionAntExecutor {

	/** BUILD FAILED flag */
	public static final String BUILD_FAILED = "BUILD FAILED";

	public static final String NEW_LINE = SystemProps.LineSeparator.getProperty();

	private String outLog = null;
	private String errLog = null;
	private Charset charset = null;

	public ActionAntExecutor(){
	}

	public ActionAntExecutor( Charset charset ){
		this.charset = charset;
	}
	public Charset getCharset() {
		return charset;
	}

	public String getErrLog() {
		return errLog;
	}

	public void setErrLog(String errLog) {
		this.errLog = errLog;
	}

	public String getOutLog() {
		return outLog;
	}

	public void setOutLog(String outLog) {
		this.outLog = outLog;
	}

	public final void execute(String buildXmlPath, ILog log) throws Exception {// BaseBusinessException
																				// {

		LogUtil logUtil = new LogUtilLog4j(log, LogUtil.LOG_TYPE_ANT);

		TaskCommandExecutor exec = null;

		if( this.charset == null ){
			exec = new TaskCommandExecutor(logUtil);

		}else{
			exec = new TaskCommandExecutor(logUtil, this.charset);
		}

		// ログ取得
		// String log = logUtil.getLogPath();
		// String logOption = logUtil.getLogOption();

		File file = this.getBuildXmlFile(TriStringUtils.linkPathBySlash(buildXmlPath, AntTemplates.getBuildXml()));
		if (true != file.isFile()) {
			throw new TriSystemException(SmMessageId.SM004118F , file.getPath() );
		}

		boolean result = false;
		if (null != file) {
			String[] val = null;

			if (TriSystemUtils.isWindows()) {
				// Windowsでの実行(テスト)
				// val = new String[] { "cmd" , "/c" , "ant" , "-f" ,
				// file.getPath() , logOption , log } ;
				val = new String[] { "cmd", "/c", "ant", "-f", file.getPath() };
			} else {
				// Linuxでの実行(本番)
				// val = new String[] { "ant" , "-f" ,file.getPath() , logOption
				// , log } ;
				val = new String[] { "ant", "-f", file.getPath() };
			}
			try {
				result = exec.executeCommand(val);
			} finally {
				this.outLog = exec.getOutLog();
				this.errLog = exec.getErrLog();
			}
		}
		if (true != result) {
			throw new BusinessException( SmMessageId.SM005129S );
		}

		// this.outputBLEndLog(info.getProjectId().toString(), info.getUserId(),
		// this.getClass().toString());
	}

	/**
	 * Antビルドに使用するXML定義ファイルを取得します。
	 *
	 * @param list コンバート間に流通させるビーン
	 * @return 取得したXML定義ファイルを戻します。
	 */
	private final File getBuildXmlFile(String buildXmlPath) {
		File file = new File(buildXmlPath);
		if (!file.isFile()) {
			throw new TriSystemException( SmMessageId.SM004119F , buildXmlPath );
		}
		return file;
	}
}
