package jp.co.blueship.tri.fw.agent.core;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import jp.co.blueship.tri.fw.cmn.io.constants.Charset;
import jp.co.blueship.tri.fw.cmn.utils.SystemProps;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.LogUtil;
import jp.co.blueship.tri.fw.log.LogUtilFile;
import jp.co.blueship.tri.fw.log.LogUtilLog4j;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.SmMessageId;

/**
 * コマンド実行に対する処理を提供するClass
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2007
 *
 * @version V3L10.02
 * @author
 *
 * @version V4.00.00
 * @author Nguyen Van Chung
 */
public class TaskCommandExecutor {

	private static final ILog log = TriLogFactory.getInstance();

    /** エラーメッセージ */
	private LogUtil logUtil = null;
	public int exitValue ;
	private String outLog = null ;
	private String errLog = null ;
	private Charset charset = null ;

	public TaskCommandExecutor(LogUtil logUtil) {
		this.logUtil = logUtil;
	}

	public TaskCommandExecutor(LogUtil logUtil, Charset charset) {
		this.logUtil = logUtil;
		this.charset = charset;
	}

    public Charset getCharset() {
		return charset;
	}

    public int getExitValue() {
		return exitValue;
	}

	public void setExitValue(int exitValue) {
		this.exitValue = exitValue;
	}

	public String getErrLog() {
		return errLog;
	}
	public void setErrLog(String errLog) {
		this.errLog = errLog;
	}
	public String getOutLog() {
		return outLog;
	}
	public void setOutLog(String outLog) {
		this.outLog = outLog;
	}

	/**
	 * コマンド実行結果判定Method
	 */
	private boolean checkCommandExecuteResult(Process process) {

		try {
			ProcessStreamReader errStreamReader = null;
			ProcessStreamReader outStreamReader = null;

			if(charset == null){
				errStreamReader = new ProcessStreamReader(process.getErrorStream());
				outStreamReader = new ProcessStreamReader(process.getInputStream());

			}else{
				errStreamReader = new ProcessStreamReader(process.getErrorStream(), charset.value());
				outStreamReader = new ProcessStreamReader(process.getInputStream(), charset.value());
			}

			errStreamReader.start();
			outStreamReader.start();
			errStreamReader.join();
			outStreamReader.join();

			process.waitFor();

			if(errStreamReader.isException()){
				throw errStreamReader.getException();
			}
			if(outStreamReader.isException()){
				throw outStreamReader.getException();
			}

			outLog = outStreamReader.getStreamString();
			errLog = errStreamReader.getStreamString();


			if( logUtil instanceof LogUtilFile ) {
				LogUtilFile logUtilFile = (LogUtilFile)logUtil ;
				String logPath = logUtilFile.getLogPath();
				if(null == logPath || logPath.trim().equals("")){
					System.out.println(outLog);
				System.err.println(errLog);
				}else{
					if (logUtilFile.isFileCreate()) {
						logUtilFile.writeLog(outLog, logPath);
						logUtilFile.writeLog(errLog, logPath);
					} else {
						errLog = logUtilFile.readLogContents(logPath, logUtilFile.isFileExistCheck());
					}
				}
			} else if( logUtil instanceof LogUtilLog4j ) {
				LogUtilLog4j logUtilLog4j = (LogUtilLog4j)logUtil ;
				logUtilLog4j.writeLog( outLog ) ;
				logUtilLog4j.writeLog( errLog ) ;
			}

			if(this.isSucceed(outLog)){
				LogHandler.debug( log , "outLog:"+outLog);
				return true;
			}
			if(this.isFailed(outLog)){
				LogHandler.debug( log , "outLog:"+outLog);
				return false;
			}
			if(this.isFailed(errLog)){
				LogHandler.debug( log , "errLog:"+errLog);
				return false;
			}
			this.exitValue = process.exitValue() ;
			if(process.exitValue() != 0){
				LogHandler.debug( log , "exitValue:"+process.exitValue());
				return false;
			}
			try {
				errStreamReader.close();
			} catch (IOException e) {
				throw new TriSystemException(SmMessageId.SM005130S, e);
			} finally {
				try {
					outStreamReader.close();
				} catch (IOException e) {
					throw new TriSystemException(SmMessageId.SM005131S, e);
				} finally {
					process.destroy();
				}
			}
		} catch (InterruptedException e1) {
			throw new TriSystemException(SmMessageId.SM005132S, e1);
		}

		return true;
	}
	/**
	 * コマンド実行Method
	 * <br>
	 * <p>
	 * 指定されたコマンドを指定された引数で実行する
	 * </p>
	 * @throws TriRuntimeException 例外
	 */
	public boolean executeCommand(String[] command, String[] env , String execDir) {

		try {
			Process process = null ;
			if( null != execDir ) {
				File execPath = new File(execDir);
				if (execPath.isFile()) {
					throw new TriSystemException(SmMessageId.SM004120F);
				}
				//				 コマンド実行：（ワークディレクトリを指定する）
				process = Runtime.getRuntime().exec(command, env, execPath);
			} else {
				//				 コマンド実行：（ワークディレクトリを指定しない）
				process = Runtime.getRuntime().exec(command , env );
			}

			return checkCommandExecuteResult(process);

		} catch (IOException e2) {
			throw new TriSystemException(SmMessageId.SM005132S, e2);
		}
	}
    /**
     * コマンド実行Method
     * <br>
     * <p>
     * 指定されたコマンドを指定された引数で実行する
     * </p>
     * @throws TriRuntimeException 例外
     */
	public boolean executeCommand(String[] command, String execDir) {

		try {
			File execPath = new File(execDir);
			if (execPath.isFile()) {
				throw new TriSystemException(SmMessageId.SM004120F);
			}
			// コマンド実行
			Process process = Runtime.getRuntime().exec(command, null, execPath);
			return checkCommandExecuteResult(process);

		} catch (IOException e2) {
			throw new TriSystemException(SmMessageId.SM005132S, e2);
		}
	}

	/**
	 * コマンド実行Method
	 * <br>
	 * <p>
	 * 指定されたコマンドを指定された引数で実行する
	 * </p>
	 * @throws TriRuntimeException 例外
	 */
	public boolean executeCommand(String[] command) {

		try{
			// コマンド実行
			Process process = Runtime.getRuntime().exec(command);
			return checkCommandExecuteResult(process);

		} catch(IOException e2) {
			throw new TriSystemException(SmMessageId.SM005132S, e2);
		}
	}

	/**
	 * コマンド実行Method
	 * <br>
	 * <p>
	 * 指定されたコマンドを指定された引数で実行する。コマンドの終了は待たない。
	 * </p>
	 * @throws TriRuntimeException 例外
	 */
	public static void executeCommandNoWait(String[] command) {

		try{
			// コマンド実行
			Runtime.getRuntime().exec(command);

		} catch(IOException e2) {
			throw new TriSystemException(SmMessageId.SM005132S, e2);
		}
	}

	/**
	 * メッセージによりビルドエラーかどうか判定する。
	 *
	 */
	private boolean isFailed(String log){

		return logUtil.isFailed(log);
	}

	/**
	 * メッセージによりビルド成功かどうか判定する。
	 *
	 */
	private boolean isSucceed(String log){

		return logUtil.isSucceed(log);
	}
}


class ProcessStreamReader extends Thread {

	private InputStream is = null;
	private StringBuilder strStream = new StringBuilder();
	private TriSystemException se = null;
	private String charset = null;

	ProcessStreamReader(InputStream is){
		this.is= is;
	}

	ProcessStreamReader(InputStream is, String charset){
		this.is = is;
		this.charset = charset;
	}

	public void run(){

		String line;
		try {
			BufferedReader br = null;
			if (TriStringUtils.isEmpty(charset)) {
				br = new BufferedReader(new InputStreamReader(is));
			} else {
				br = new BufferedReader(new InputStreamReader(is,charset));
			}
			while((line = br.readLine()) != null) {
				strStream.append(line + SystemProps.LineSeparator.getProperty());
			}
		} catch (IOException e) {
			se = new TriSystemException(SmMessageId.SM005132S, e);
		}finally{

		}
	}

	public boolean isException(){
		return se != null;
	}

	public TriSystemException getException(){
		return se;
	}

	public String getCharset() {
		return charset;
	}

	public void setCharset(String charset) {
		this.charset = charset;
	}

	public String getStreamString(){
		return strStream.toString();
	}

	void close() throws IOException{
		this.is.close();
	}
}


