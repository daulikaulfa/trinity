package jp.co.blueship.tri.fw.agent.core;

/**
 * Ant ビルドに使用するテンプレートファイルの定数クラスです。
 *
 *
 */
public class AntTemplates {

	
	
	private static String buildXmlTemplate = "BuildXmlMaker/build.xml.template";
	private static String classpathTemplate = "BuildXmlMaker/classpath.template";
	private static String classpathLibTemplate = "BuildXmlMaker/classpathlib.template";
	private static String deleteTemplate = "BuildXmlMaker/delete.template";
	private static String srcpathTemplate = "BuildXmlMaker/srcpath.template";
	private static String buildXml = "build.xml";
	
	public static String getBuildXml() {
		return buildXml;
	}
	public static void setBuildXml(String buildXml) {
		AntTemplates.buildXml = buildXml;
	}
	public static String getBuildXmlTemplate() {
		return buildXmlTemplate;
	}
	public static void setBuildXmlTemplate(String buildXmlTemplate) {
		AntTemplates.buildXmlTemplate = buildXmlTemplate;
	}
	public static String getClasspathLibTemplate() {
		return classpathLibTemplate;
	}
	public static void setClasspathLibTemplate(String classpathLibTemplate) {
		AntTemplates.classpathLibTemplate = classpathLibTemplate;
	}
	public static String getClasspathTemplate() {
		return classpathTemplate;
	}
	public static void setClasspathTemplate(String classpathTemplate) {
		AntTemplates.classpathTemplate = classpathTemplate;
	}
	public static String getDeleteTemplate() {
		return deleteTemplate;
	}
	public static void setDeleteTemplate(String deleteTemplate) {
		AntTemplates.deleteTemplate = deleteTemplate;
	}
	public static String getSrcpathTemplate() {
		return srcpathTemplate;
	}
	public static void setSrcpathTemplate(String srcpathTemplate) {
		AntTemplates.srcpathTemplate = srcpathTemplate;
	}
	
	
	
}

