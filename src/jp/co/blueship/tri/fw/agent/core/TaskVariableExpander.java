package jp.co.blueship.tri.fw.agent.core;

import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.SmMessageId;

/**
 * ${hoge}な変数を展開して置換する。
 */
public class TaskVariableExpander {

	private ResourceBundle rbDic = null;
	private Map<String, String> mapDic = null;

	/**
	 * ${}でくくられた文字列の中身だけを前方参照で取り出す正規表現の{@link Pattern}。<br>
	 * ()で括って中身だけmatchさせてるんで、Matcher.group()は1で取得する事。
	 */
	private static final Pattern replaceKeyPattern = Pattern.compile("\\$\\{([^\\}]*)\\}");

	/**
	 * {@link ResourceBundle}を辞書としてconstructする。
	 * @param rb 辞書
	 */
	public TaskVariableExpander(ResourceBundle rb){
		rbDic = rb;
	}

	/**
	 * {@link Map}を辞書としてconstructする。
	 * @param map 辞書
	 */
	public TaskVariableExpander(Map<String, String> map){
		mapDic = map;
	}

	/**
	 * 辞書を利用しない検索{@link #expandValue(String, String, String)}を
	 * する時用contructor。
	 */
	public TaskVariableExpander(){

	}


	/**
	 * value中に含まれる${honyarara}のhonyararaをkeyとしてconstructorで渡した辞書より
	 * 値を取得し、展開する。<br>
	 * <p>
	 * ex.) OUTPUT_ROOTがc:/workspace/orgmgr-trinity-base-cc/outの時、${WEB_OUT_ROOT}/foo →
	 * c:/workspace/orgmgr-trinity-base-cc/out/fooに展開される。
	 * </p>
	 * <p>
	 * また、${foo}はvalue内に何種類でも同時に存在してOK。
	 * </p>
	 * @param value 置換対象文字列
	 * @return 置換後文字列
	 * @throws TriRuntimeException 例外
	 */
	public String expandValue(String value) throws Exception{

		Matcher m = replaceKeyPattern.matcher(value);

		while(m.find()){
			String key = m.group(1);
			String repKey = "\\$\\{" + key + "\\}";
			String val = null;
			try{
				val = getDictValue(key);
				if(val.equals("")){
					throw new TriSystemException(SmMessageId.SM004125F , key);
				}

				value = value.replaceAll(repKey, val);

			}catch(MissingResourceException e){
				throw new TriSystemException( SmMessageId.SM004126F , e , key);
			}
		}

		return value;
	}

	public String expandValue(String value, String key, String replaceWord) throws Exception {

		Matcher m = replaceKeyPattern.matcher(value);
		while(m.find()){
			//String matchKey = m.group(1);
//			if(key.equals(key)){ //意味のない処理
				String repKey = "\\$\\{" + key + "\\}";
				value = value.replaceAll(repKey, replaceWord);
//			}
		}

		return value;
	}


	private String getDictValue(String key){

		if(rbDic != null){
			return rbDic.getString(key);
		}else{
			return mapDic.get(key);
		}

	}


}

