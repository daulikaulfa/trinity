package jp.co.blueship.tri.fw.agent.core;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.SmMessageId;

/**
 * タスクで使用する定義ファイルの読込処理を提供します
 */
public class TaskDefineFileUtils {

	/**
	 * comment行判別用{@link Pattern}
	 */
	private static final Pattern commentPattern = Pattern.compile("^\\p{Space}*(#.*)?$");
	/**
	 * 変数定義を示す予約語
	 */
	private static final String ALIAS = "alias ";
	/**
	 * 予約語と値のセパレータ
	 */
	private static final String SEPARATER = "=";


	/**
	 * alias行かどうかをチェックする
	 * alias行であれば変数の定義を辞書に格納する
	 * @return 格納すればtrue、そうでなければfalse
	 */
	public static boolean isAlias( String line, Map<String,String> aliasDic ) throws TriSystemException {

		if ( null == line ) {
			throw new TriSystemException( SmMessageId.SM004121F );
		}
		if ( null == aliasDic ) {
			throw new TriSystemException( SmMessageId.SM004122F );
		}

		String trimedLine = line.trim();

		if ( trimedLine.startsWith( ALIAS )) {

			// "alias "文字列を削除
			trimedLine = StringUtils.replace( trimedLine, ALIAS, "", 1 ).trim();

			// 変数と値に分離
			String[] splitLine = trimedLine.split( SEPARATER, 2 );

			// ↓の処理でタスクからの変数展開ができるが、メリットがなさそうなのでとりあえずはしない
			//   するときはパラメータ追加要
			//    		String value = TaskStringUtil.substitutionPath( this.propertyEntity , splitLine[1].trim() );

			if ( splitLine.length == 2 ) {
				aliasDic.put( splitLine[0].trim(), splitLine[1].trim() );
				return true;
			}
		}

		return false;
	}

	/**
	 * 文字列がcommentや空行では無く、実contentsの場合はtrueを返却する。
	 * @param line check対象文字列
	 * @return 判定結果
	 */
	public static boolean isContents( String line ) {

		if ( null == line ) {
			throw new TriSystemException( SmMessageId.SM004121F );
		}

		Matcher m = commentPattern.matcher( line );
		return ! m.matches();
	}

	/**
	 * alias変数を置換する
	 */
	public static String replaceAliasVariable( String line, Map<String,String> aliasDic )
			throws TriSystemException {

		if ( null == line ) {
			throw new TriSystemException( SmMessageId.SM004123F );
		}
		if ( null == aliasDic ) {
			throw new TriSystemException( SmMessageId.SM004122F );
		}


		TaskVariableExpander ve = new TaskVariableExpander( aliasDic );
		String replacedLine = null;
		try {
			replacedLine = ve.expandValue( line );
		} catch ( Exception e ) {
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( SmMessageId.SM005133S , e);
		}
		return replacedLine;
	}

	/**
	 * 定義リストファイルを読み込み、定義ファイルのリストを返す。
	 * @param defListFile 定義リストファイル
	 * @return 定義ファイルのリスト
	 */
	public static List<File> getDefFileList( File defListFile ) {

		List<File> defList	= new ArrayList<File>();

		try {

			String[] lineArray = TriFileUtils.readFileLine( defListFile , false ) ;
			for( String line : lineArray ) {
				if( !isContents( line )){
					continue;
				}

				File defFile = new File( line.trim() );
				if ( defFile.exists() ) {
					defList.add( defFile );
				}
			}

		} catch ( Exception e ) {
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( SmMessageId.SM005134S , e);
		}
		return defList;
	}

	/**
	 * 定義ファイルの内容から、キーに該当する値を取得する。
	 * @param line 定義ファイルの内容(1行分)
	 * @param key キー
	 * @return キーに該当する値、取得できない場合はnull
	 */
	public static String getValue( String line, String key ) {

		if ( null == line ) {
			throw new TriSystemException( SmMessageId.SM004124F);
		}

		String value		= null;
		String trimedLine	= line.trim();

		// 1. 文字列がキーで始まっており、
		if ( trimedLine.startsWith( key )) {

			// 2. 予約文字列を削除後に
			trimedLine = StringUtils.replace( trimedLine, key, "", 1 ).trim();

			// 3. 先頭がセパレータになっていれば、求める定義値なので
			if ( trimedLine.startsWith( SEPARATER )) {

				// 4. セパレータ文字列を削除して返す
				value = StringUtils.replace( trimedLine, SEPARATER, "", 1 ).trim();
			}
		}

		return value;
	}
}
