package jp.co.blueship.tri.fw.agent.concurrent;

/**
 * 業務シーケンスを非同期実行コントローラによるスレッド配下で実行する際に業務シーケンスをラッピングするための<br/>
 * 共通インタフェースです。
 *
 * @author Takayuki Kubo
 *
 */
public interface IASyncTask extends Runnable {

}
