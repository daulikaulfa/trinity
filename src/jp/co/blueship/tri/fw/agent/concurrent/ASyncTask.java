package jp.co.blueship.tri.fw.agent.concurrent;

import jp.co.blueship.tri.fw.agent.dto.ITaskBean;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.dao.orm.IEntity;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.security.TriAuthenticationContext;
import jp.co.blueship.tri.fw.support.IFwFinderSupport;
import jp.co.blueship.tri.fw.task.ITaskService;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 * 業務シーケンスを非同期実行コントローラによるスレッド配下で実行する際に業務シーケンスをラッピングするための<br/>
 * 抽象クラスです。
 *
 * @param T 業務シーケンス実行リクエストオブジェクトの型
 * @param P 業務シーケンス実行に必要なパラメタを格納したオブジェクトの型
 * @param L 依頼するライン（システム)の型
 *
 * @version V3L10R01
 * @author Takayuki Kubo
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public abstract class ASyncTask<T extends IEntity, P extends ITaskBean, L extends IEntity> implements IASyncTask {

	private IContextAdapter context;
	private ITaskService service;
	private T timeLine;
	private P param;
	private L line;

	@SuppressWarnings("unused")
	private IFwFinderSupport support;

	/**
	 * アクセスインタフェースがインスタンス生成時に自動的に設定されます。
	 * @param support アクセスインタフェース
	 */
	public void setFwFinderSupport(IFwFinderSupport support) {
		this.support = support;
	}

	/**
	 * ビーンを生成するコンテキストが設定されます。
	 *
	 * @param context コンテキスト
	 */
	public void setContext(IContextAdapter context) {
		this.context = context;
	}

	/**
	 * DIコンテナのコンテキストアダプタを返します。
	 *
	 * @return コンテキストアダプタ
	 */
	protected IContextAdapter context() {
		return context;
	}

	/**
	 * DIコンテナより、指定されたBeanのインスタンスを返します。
	 *
	 * @param beanName Bean名
	 * @return Beanのインスタンス
	 */
	protected Object getBean(String beanName) {
		return context().getBean(beanName);
	}

	/**
	 * 業務シーケンス実行サービスのインスタンスを設定します。
	 *
	 * @param service 業務シーケンス実行サービス
	 */
	public void setService(ITaskService service) {
		this.service = service;
	}

	/**
	 * 業務シーケンス実行サービスのインスタンスを返します。
	 *
	 * @return 業務シーケンス実行サービス
	 */
	protected ITaskService service() {
		return service;
	}

	/**
	 * 業務シーケンス実行リクエストオブジェクトを設定します。
	 *
	 * @param timeLine 業務シーケンス実行リクエスト
	 */
	public void setTimeLine(T timeLine) {
		this.timeLine = timeLine;
	}

	/**
	 * 業務シーケンス実行リクエストオブジェクトを返します。
	 *
	 * @return 業務シーケンス実行リクエスト
	 */
	protected T timeLine() {
		return timeLine;
	}

	/**
	 * 業務シーケンスで使用するパラメタ情報を設定します。
	 *
	 * @param param パラメタ
	 */
	public void setParam(P param) {
		this.param = param;
	}

	/**
	 * 業務シーケンスで使用するパラメタ情報を返します。
	 *
	 * @return メッセージ
	 */
	protected P param() {
		return param;
	}

	/**
	 * 依頼するライン（システム)を設定します。
	 *
	 * @param line ライン
	 */
	public void setLine(L line) {
		this.line = line;
	}

	/**
	 * 依頼するライン（システム)を返します。
	 *
	 * @return ライン
	 */
	protected L line() {
		return line;
	}

	@Override
	public void run() {

		try {

			authenticate();

			if (isAllTimeLine()) {
				execute(timeLine, param);
			} else {
				execute(timeLine, param, line);
			}
		} catch (Throwable e) {
			ExceptionUtils.printStackTrace(e);

			try {
				writeProcessByIrregular(timeLine, param, line, e);
			} catch (Exception e1) {
				e1.printStackTrace();
				ExceptionUtils.reThrowIfTrinityException(e1);
				throw new TriSystemException( SmMessageId.SM005054S, e1 );
			}

			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( SmMessageId.SM005054S, e );
		} finally {
			unAuthenticate();
		}

	}

	/**
	 * クライアントから設定された入力パラメタ情報より認証済みユーザIDを取得し、SecurityContextへ設定します。
	 */
	private void authenticate() {

		PreConditions.assertOf(param.getInsertUpdateUserId() != null, "UserID is not specified.");

		TriAuthenticationContext authContext = (TriAuthenticationContext) getBean(TriAuthenticationContext.BEAN_NAME);
		String userId = param.getInsertUpdateUserId();
		authContext.setAuthUser(userId, userNameOf(userId));
	}

	/**
	 * SecurityContextの認証済みユーザ情報をクリアします。
	 */
	private void unAuthenticate() {

		TriAuthenticationContext authContext = (TriAuthenticationContext) getBean(TriAuthenticationContext.BEAN_NAME);
		authContext.clearAuthenticationContext();
	}

	private String userNameOf(String userID) {

		IUmFinderSupport umFinderSupport = (IUmFinderSupport) getBean("umFinderSupport");
		IUserEntity userEntity = umFinderSupport.findUserByUserId(userID);

		return userEntity.getUserNm();
	}

	/**
	 * 業務シーケンスをタイムライン単位に実行します。
	 *
	 * @param timeLine 依頼するタイムライン
	 * @param param 業務シーケンスへの入力パラメタ
	 *
	 * @throws Exception タスク実行時エラーが発生した。
	 */
	protected abstract void execute(T timeLine, P param) throws Exception;

	/**
	 * 業務シーケンスをタイムライン単位に実行します。<br/>
	 * 引数で与えられた、タイムラインの単一システムに対し、起動指示を行います。
	 *
	 * @param timeLine 依頼するタイムライン
	 * @param param 業務シーケンスへの入力パラメタ
	 * @param line 依頼するライン（システム）
	 *
	 * @throws Exception タスク実行時エラーが発生した。
	 */
	protected abstract void execute(T timeLine, P param, L line) throws Exception;

	/**
	 * 業務シーケンスのプロセステーブルにエラー発生情報を書き込みます。
	 *
	 * @param timeLine 依頼するタイムライン
	 * @param param 業務シーケンスへの入力パラメタ
	 * @param line 依頼するライン（システム）
	 * @param e 例外情報
	 */
	protected abstract void writeProcessByIrregular(T timeLine, P param, L line, Throwable e) throws Exception;

	private boolean isAllTimeLine() {
		return line == null;
	}

}
