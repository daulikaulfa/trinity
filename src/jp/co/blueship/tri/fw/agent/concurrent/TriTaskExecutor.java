package jp.co.blueship.tri.fw.agent.concurrent;

import static java.lang.Integer.*;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.constants.SmDesignEntryKeyByTask;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.log.TriLogMessage;

/**
 * Agent内でタスクを非同期に実行するためのコントローラクラスです。<br/>
 * trinity本体から送信された１つの業務シーケンス実行リクエストに対して１つのスレッドを割り当て、<br/>
 * そのスレッド配下で業務シーケンスを実行します。<br/>
 * スレッドはAgent初期化時にある一定数起動され、スレッドプーリング機構により管理されます。<br/>
 * trinity本体からの業務シーケンス実行リクエストを受信すると、そのリクエストに空きスレッドが割り当てられます。<br/>
 * 空きスレッドが無い場合、そのリクエストは待ちキューへキューイングされ、遅延実行されます。<br/>
 * なお、本クラスのインスタンスはDIコンテナによってSingletonで管理されます。
 *
 *
 * @author Takayuki Kubo
 *
 */
public class TriTaskExecutor {

	public static final String BEAN_NAME = "triTaskExecutor";

	private static final ILog log = TriLogFactory.getInstance();
	private static final Integer DEFAULT_NUMBER_OF_THREAD = 10;
	private static final Long DEFAULT_WAIT_TIME = 60L;
	private ExecutorService executorService;

	/**
	 * デフォルトコンストラクタです。
	 */
	public TriTaskExecutor() {
	}

	/**
	 * デフォルトコンストラクタです。<br/>
	 * 非同期実行サービス機構を初期化します。
	 */
	public void init() {
		executorService = Executors.newFixedThreadPool(threadNum());
	}

	/**
	 * スレッドへ業務シーケンス実行を委譲します。
	 *
	 * @param task 実行業務シーケンスを格納したタスクオブジェクト
	 */
	public void submit(IASyncTask task) {

		PreConditions.assertOf(executorService != null, "Executor Service is not injected.");
		executorService.submit(task);
	}

	/**
	 * スレッドプール機構を終了させます。実行中のタスクが存在する場合は、指定された時間だけ終了を待ちます。
	 */
	public void shutDown() {

		if (!isActive()) {
			return;
		}

		executorService.shutdown();

		try {

			if (!executorService.awaitTermination(DEFAULT_WAIT_TIME, TimeUnit.SECONDS)) {
				executorService.shutdownNow();
				if (!executorService.awaitTermination(DEFAULT_WAIT_TIME, TimeUnit.SECONDS)) {
					LogHandler.info(log, TriLogMessage.LSM0009);
				}
			}

			LogHandler.info(log, TriLogMessage.LSM0010);
		} catch (InterruptedException e) {
			executorService.shutdownNow();
			Thread.currentThread().interrupt();
		}
	}

	/**
	 * スレッドプール機構が生きているかどうかを返します。
	 *
	 * @return スレッドプール機構が生きている場合はtrue。
	 */
	public boolean isActive() {
		return !executorService.isShutdown();
	}

	private int threadNum() {

		try {
			return parseInt(
					DesignSheetFactory.getDesignSheet().getValue(SmDesignEntryKeyByTask.maxConcurrentBusinessSequenceNum));
		} catch (TriSystemException e) {
			return DEFAULT_NUMBER_OF_THREAD;
		}

	}

}
