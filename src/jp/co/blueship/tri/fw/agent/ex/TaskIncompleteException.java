package jp.co.blueship.tri.fw.agent.ex;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class TaskIncompleteException extends Exception {

	private static final long serialVersionUID = 1L;

	

	/**
	 * メッセージ付きコンストラクタ。<br>
	 * @param message メッセージ
	 */
	public TaskIncompleteException(String message){
		super(message);
	}

	/**
	 * メッセージ、例外チェーンコンストラクタ。<br>
	 * @param message メッセージ
	 * @param cause 起因例外
	 */
	public TaskIncompleteException(String message, Throwable cause){
		super(message, cause);
	}

	/**
	 * メッセージ、例外チェーンコンストラクタ。<br>
	 * @param cause 起因例外
	 */
	public TaskIncompleteException(Throwable cause){
		super(cause);
	}

	public String getStackTraceString() {

		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(stream);

		this.printStackTrace(ps);
		ps.flush();
		ps.close();
		return stream.toString();
	}


}
