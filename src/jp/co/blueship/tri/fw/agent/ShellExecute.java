package jp.co.blueship.tri.fw.agent;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.agent.core.TaskCommandExecutor;
import jp.co.blueship.tri.fw.cmn.io.constants.Charset;
import jp.co.blueship.tri.fw.cmn.utils.TriSystemUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogUtil;
import jp.co.blueship.tri.fw.log.LogUtilLog4j;
import jp.co.blueship.tri.fw.msg.SmMessageId;

/**
 * Shell実行処理を行うClass
 * <br>
 * <p>
 * 指定されたShellスクリプトを実行する。
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2007
 *
 * @version V3L10.02
 * @author Satoshi Sasaki
 *
 * @version V4.00.00
 * @author Nguyen Van Chung
 */
public class ShellExecute /*implements IPojo */{

	public static final int DEFAULT_ERROR_LEVEL = 999;

	private String execShellPath = null ;
	private String workDir = null ;

	private int exitValue ;
	private String outLog = null ;
	private String errLog = null ;

	private ILog log = null ;
	private String[] env = null ;

	private Charset charset = null ;

	public ShellExecute(){
	}

	public ShellExecute( Charset charset ){
		this.charset = charset;
	}

	public Charset getCharset() {
		return charset;
	}

	public void setExecShellPath(String execShellPath) {
		this.execShellPath = execShellPath;
	}
	public void setWorkDir(String workDir) {
		this.workDir = workDir;
	}
	public void setLog(ILog log) {
		this.log = log;
	}
    public void setEnv(String[] env) {
		this.env = env;
	}


    public int getExitValue() {
		return exitValue;
	}
	public void setExitValue(int exitValue) {
		this.exitValue = exitValue;
	}
	public String getErrLog() {
		return errLog;
	}
	public void setErrLog(String errLog) {
		this.errLog = errLog;
	}
	public String getOutLog() {
		return outLog;
	}
	public void setOutLog(String outLog) {
		this.outLog = outLog;
	}
	/**
     * Shell実行Method
     * <br>
     * <p>
     * 指定されたShellスクリプトを実行するMethod<br>
     * </p>
     * @throws Exception
     */
	public void execute() throws Exception {

		LogUtil logUtil = new LogUtilLog4j( log , LogUtil.LOG_TYPE_SHELL ) ;

		// 実行環境の取得
		TaskCommandExecutor exec = null;

		if( this.charset == null ){
			exec = new TaskCommandExecutor( logUtil ) ;

		}else{
			exec = new TaskCommandExecutor( logUtil, this.charset ) ;
		}

//		 ログ取得


		boolean win32 = TriSystemUtils.isWindows();

		boolean execResult = false;
		if (win32) {
			String[] val = { "cmd" , "/c" , execShellPath } ;
			execResult = exec.executeCommand( val , env , workDir ) ;
		} else {
			// Linuxでの実行(本番)
			String[] val = getSpritParameter( execShellPath ) ;//{ execShellPath } ;
			execResult = exec.executeCommand( val , env , workDir ) ;
		}

		this.exitValue = exec.exitValue ;
		this.outLog = exec.getOutLog() ;
		this.errLog = exec.getErrLog() ;

		if( 0 == this.exitValue && true != execResult ) {
			this.exitValue = DEFAULT_ERROR_LEVEL;
		}

	}
	/**
	 * １つの文字列で渡された、引数を含むコマンド文字列を、「半角スペース" "」で区切り、配列で返す。<br>
	 * 但し、「ダブルクオート""」で挟まれた中に存在する「半角スペース" "」はそのまま連続しているものと見なす。<br>
	 * <pre>
	 * 【例１：普通パターン】
	 *
	 * String param = "aaa.sh aaa bbb ccc" ;
	 * 出力： [aaa.sh] [aaa] [bbb] [ccc]
	 *
	 * 【例２：半角空白を「ダブルクオート""」で囲ってあるパターン】
	 *
	 * String param = "aaa.sh aaa \"Program Files\" bbb ccc" ;
	 * 出力：[aaa.sh] [aaa] ["Program Files"] [bbb] [ccc]
	 *
	 * </pre>
	 * @param parameters  引数を含むコマンド文字列
	 * @return コマンド・引数パラメータの配列
	 */
	private String[] getSpritParameter( String parameters ) {
		boolean skipFlg = false ;
		List<String> splitList = new ArrayList<String>() ;
		StringBuilder stbTemp = new StringBuilder() ;

		for( int i = 0 ; i < parameters.length() ; i++ ) {
			char ch = parameters.charAt( i ) ;

			if( '\"' == ch ) {
				skipFlg = ( true != skipFlg ) ? true : false ;
			}
			if( false == skipFlg && ' ' == ch ) {
				splitList.add( stbTemp.toString() ) ;
				stbTemp = new StringBuilder() ;
				continue ;
			}
			stbTemp.append( ch ) ;
		}
		if( 0 != stbTemp.length() ) {
			splitList.add( stbTemp.toString() ) ;
		}
		if( true == skipFlg ) {//ダブルクオートが閉じられていない？
			throw new TriSystemException( SmMessageId.SM004151F , parameters ) ;
		}
		return splitList.toArray( new String[ 0 ] ) ;
	}
}
