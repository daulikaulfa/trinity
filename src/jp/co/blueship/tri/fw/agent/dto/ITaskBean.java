package jp.co.blueship.tri.fw.agent.dto;

import jp.co.blueship.tri.fw.dao.orm.IEntity;

/**
 * 業務シーケンス側で判断できない情報／レポート情報をダイレクトするための
 * 共通メッセージインタフェースです。
 *
 * @author Takayuki Kubo
 *
 */
public interface ITaskBean extends IEntity {

	/**
	 * 登録／更新者を取得します。
	 *
	 * @return 取得した値を戻します。
	 */
	public String getInsertUpdateUser();

	/**
	 * 登録／更新者ＩＤを取得します。
	 *
	 * @return 取得した値を戻します。
	 */
	public String getInsertUpdateUserId();

}
