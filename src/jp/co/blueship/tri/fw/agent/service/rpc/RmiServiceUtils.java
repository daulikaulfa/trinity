package jp.co.blueship.tri.fw.agent.service.rpc;

import java.util.Arrays;
import java.util.List;

import jp.co.blueship.tri.agent.bm.svc.flow.IBuildTaskService;
import jp.co.blueship.tri.agent.rm.svc.flow.IReleaseTaskService;
import jp.co.blueship.tri.bm.dao.bldenv.IBldEnvSrvDao;
import jp.co.blueship.tri.bm.dao.bldenv.eb.BldEnvSrvCondition;
import jp.co.blueship.tri.bm.dao.bldenv.eb.BldEnvSrvEntity;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvSrvEntity;
import jp.co.blueship.tri.bm.dao.rmisvc.IRmiSvcDao;
import jp.co.blueship.tri.bm.dao.rmisvc.eb.IRmiSvcEntity;
import jp.co.blueship.tri.bm.dao.rmisvc.eb.RmiSvcCondition;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.spec.Constraint;
import jp.co.blueship.tri.fw.cmn.utils.spec.Specifications;
import jp.co.blueship.tri.fw.constants.RemoteService;
import jp.co.blueship.tri.fw.constants.RemoteServiceType;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.msg.SmMessageId;

/**
 * RMIを扱うユーティリティークラスです。
 *
 */
public class RmiServiceUtils {

	private static final ILog log = TriLogFactory.getInstance();

	/**
	 * RmiServiceExporterのリストを取得する
	 *
	 * @param serverId サーバ番号
	 * @param serviceId サービスＩＤ
	 * @return RmiServiceExporterWrapperのインスタンス
	 */
	public static RmiServiceExporterWrapper getRmiServiceExporter(String serverId, RemoteService serviceId) {

		try {

			IRmiSvcEntity rmiSvcEntity = rerieveRmiSvcEntity(serverId, serviceId);

			RmiServiceExporterWrapper rmiServiceExporter = createRmiServiceExporter(rmiSvcEntity);
			rmiServiceExporter.setServiceInterface(deriveRmiSvcIf(serviceId));
			rmiServiceExporter.setService(context().getBean(serviceId.getExecSvc()));
			rmiServiceExporter.setServiceName(serviceId.getServiceName());
			rmiServiceExporter.setRegistryPort(rmiSvcEntity.getRmiRegPort());
			rmiServiceExporter.setServicePort(rmiSvcEntity.getRmiSvcPort());

			return rmiServiceExporter;
		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(SmMessageId.SM005189S, e);
		}

	}

	private static RmiServiceExporterWrapper createRmiServiceExporter(IRmiSvcEntity rmiSvcEntity) {

		return new RmiServiceExporterWrapper(rmiSvcEntity.getBldSrvId(), //
				rmiSvcEntity.getRmiSvcId());
	}

	/**
	 * Agent接続情報管理テーブルの検索結果を検証するバリデーション。<br/>
	 * 検索結果が0件または、2件以上の場合は例外を送出する。
	 *
	 * @param entityList Agent接続情報簡易テーブルの検索結果
	 * @param serverId サーバID
	 * @param serviceId サービスID
	 */
	private static void validateRmiSvcEntityList(List<IRmiSvcEntity> entityList, String serverId, RemoteService serviceId) {

		Constraint.validate(entityList, Specifications.NOT_EMPTY_LIST, BmMessageId.BM004080F, serverId,
				serviceId.getId());
		Constraint.validate(entityList, Specifications.HAS_SINGLE_ELEMENT, BmMessageId.BM004081F, serverId,
				serviceId.getId());
	}

	/**
	 * RmiProxyFactoryBeanのリストを取得する
	 *
	 * @param serverNo サーバ番号
	 * @param envNo 環境番号
	 * @param serviceType サービスタイプ
	 * @param serviceId サービスＩＤ
	 * @param beanId 呼び出すサービスのビーンＩＤ
	 * @return RmiProxyFactoryBeanのリスト
	 * @throws ClassNotFoundException
	 * @deprecated {@link #getRmiProxyFactoryBean(String,String)} の代用
	 */
	public static List<RmiProxyFactoryWrapperBean> getRmiProxyFactoryBeanList(String serverNo, String envNo, RemoteServiceType serviceType,
			String serviceId) throws TriSystemException {

		// serviceType使いません
		return getRmiProxyFactoryBeanList(serverNo, serviceId);
	}

	/**
	 * RmiProxyFactoryBeanのリストを取得する
	 *
	 * @param serverId サーバ番号
	 * @param serviceId サービスＩＤ
	 * @return RmiProxyFactoryBeanのリスト
	 * @throws ClassNotFoundException
	 * @deprecated {@link #getRmiProxyFactoryBean(String,String)} の代用
	 */
	private static List<RmiProxyFactoryWrapperBean> getRmiProxyFactoryBeanList(String serverNo, String serviceId) throws TriSystemException {
		return Arrays.asList(getRmiProxyFactoryBean(serverNo, RemoteService.getValueById(serviceId)));
	}

	/**
	 * RmiProxyFactoryBeanのリストを取得する
	 *
	 * @param serverId サーバID
	 * @param serviceId サービスID
	 * @return RmiProxyFactoryBeanのリスト
	 * @throws ClassNotFoundException
	 */
	public static RmiProxyFactoryWrapperBean getRmiProxyFactoryBean(String serverId, RemoteService serviceId) throws TriSystemException {

		try {

			IRmiSvcEntity rmiSvcEntity = rerieveRmiSvcEntity(serverId, serviceId);

			List<IBldEnvSrvEntity> bldEntity = retrieveBldEnvSrvEntities(rmiSvcEntity.getBldSrvId());
			RmiProxyFactoryWrapperBean rmiProxyFactoryBean = new RmiProxyFactoryWrapperBean(rmiSvcEntity.getBldSrvId(), rmiSvcEntity.getRmiSvcId(),
					bldEntity.toArray(new BldEnvSrvEntity[0]));

			String url = "rmi://" + rmiSvcEntity.getRmiHostNm() + ":" + rmiSvcEntity.getRmiRegPort() + "/" + serviceId.getServiceName();

			rmiProxyFactoryBean.setServiceUrl(url);
			rmiProxyFactoryBean.setServiceInterface(deriveRmiSvcIf(serviceId));

			return rmiProxyFactoryBean;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(SmMessageId.SM005190S, e);
		}

	}

	private static IRmiSvcEntity rerieveRmiSvcEntity(String serverId, RemoteService serviceId) {

		RmiSvcCondition condition = new RmiSvcCondition();
		condition.setBldSrvId(serverId);
		condition.setRmiSvcId(serviceId.getId());
		condition.setDelStsId(StatusFlg.off);
		IEntityLimit<IRmiSvcEntity> serviceLimit = ((IRmiSvcDao) context().getBean("bmRmiSvcDao")).find(condition.getCondition(), null, 1, 0);

		validateRmiSvcEntityList(serviceLimit.getEntities(), serverId, serviceId);

		return serviceLimit.getEntities().get(0);
	}

	private static List<IBldEnvSrvEntity> retrieveBldEnvSrvEntities(String bldSrvId) {

		BldEnvSrvCondition bldCondition = new BldEnvSrvCondition();
		bldCondition.setBldSrvId(bldSrvId);
		List<IBldEnvSrvEntity> bldEntity = ((IBldEnvSrvDao) context().getBean("bmBldEnvSrvDao")).find(bldCondition.getCondition());

		return bldEntity;
	}

	private static Class<?> deriveRmiSvcIf(RemoteService serviceId) {

		if (serviceId == RemoteService.BUILD_TASK_SERVICE) {
			return IBuildTaskService.class;
		}

		return IReleaseTaskService.class;
	}

	private static IContextAdapter context() {
		return ContextAdapterFactory.getContextAdapter();
	}

}