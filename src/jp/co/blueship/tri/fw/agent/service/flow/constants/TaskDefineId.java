package jp.co.blueship.tri.fw.agent.service.flow.constants;


/**
 * タスクで使用する一意に識別するためのIDの列挙型です。
 * <p>
 * <pre>
 *		たとえば、バイナリ資産の実際に削除された資産数などをタスクから集計する場合、
 *		DIFFタスクのtaskIdに識別IDを埋め込みます。
 *
 *		このようにタスクに特定の識別IDを埋め込んで、業務ルールで処理を行いたい場合に利用されます。
 * </pre>
 * 
 * @author Yukihiro Eguchi
 *
 */
public enum TaskDefineId {
	
	deleteBinaryAssetCount		( "{@deleteBinaryAssetCount}" );	//バイナリ資産削除集計タスクを一意に識別する任意の識別子
	
	
	
	private String value = null ;
	
	private TaskDefineId( String value ) {
		this.value = value ;
	}
	
	public String value() {
		return this.value ;
	}
	
	public static TaskDefineId value( String value ) {
		for ( TaskDefineId charset : values() ) {
			if ( charset.value().equals( value ) ) {
				return charset;
			}
		}
	
		return null;
	}
}
