package jp.co.blueship.tri.fw.agent.service.flow.beans;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.agent.bm.svc.flow.IBuildTaskService;
import jp.co.blueship.tri.agent.rm.svc.flow.IReleaseTaskService;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;

/**
 * Agentの疎通チェックを行います。
 *
 */
public class AgentLinkCheckTask implements IDomain<IGeneralServiceBean> {

	

	private static final ILog log = TriLogFactory.getInstance();

	private IBuildTaskService buildTaskService = null;
	private IReleaseTaskService releaseTaskService = null;

	public void setBuildTaskService( IBuildTaskService buildTaskService ) {
		this.buildTaskService = buildTaskService;
	}

	public void setReleaseTaskService( IReleaseTaskService releaseTaskService ) {
		this.releaseTaskService = releaseTaskService;
	}

	/**
	 * agentとの疎通チェックを行う。
	 *
	 */
	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		LogHandler.debug( log , this.getClass().getName() + " " + "process start!!" ) ;

		//List<String>	messageList		= new ArrayList<String>();
		//List<String[]>	messageArgsList	= new ArrayList<String[]>();

		List<AgentStatusTask> checkedList = new ArrayList<AgentStatusTask>() ;

		try {

			LogHandler.debug( log , "ＲＰ実行に使用するagentの疎通チェック" + " " + "開始" ) ;
			try {
				List<AgentStatusTask> buildCheckedList = buildTaskService.executeLinkCheck() ;
				checkedList.addAll( buildCheckedList ) ;
			} catch ( Exception e ){
				//messageList.add		( MessageId.MESREL0016 );
				//messageArgsList.add	( new String[] { "ビルド" } );
			}
			LogHandler.debug( log , "ＲＰ実行に使用するagentの疎通チェック" + " " + "終了" ) ;


			LogHandler.debug( log , "ＲＣ実行に使用するagentの疎通チェック" + " " + "開始" ) ;
			try {
				List<AgentStatusTask> releaseCheckedList = releaseTaskService.executeLinkCheck() ;
				checkedList.addAll( releaseCheckedList ) ;
			} catch ( Exception e ){
				//messageList.add		( MessageId.MESREL0016 );
				//messageArgsList.add	( new String[] { "リリース" } );
			}
			LogHandler.debug( log , "ＲＣ実行に使用するagentの疎通チェック" + " " + "終了" ) ;


			//if ( 0 != messageList.size() ) {
				//throw new ContinuableBusinessException( messageList, messageArgsList );
			//} else {
			//	LogHandler.debug( log , "agent疎通チェックに異常はありませんでした。" ) ;
			//}

			return serviceDto.add( checkedList.toArray( new AgentStatusTask[ 0 ] ) ) ;


		} finally {

			LogHandler.debug( log , this.getClass().getName() + " " + "process end!!" ) ;

		}
	}

}
