package jp.co.blueship.tri.fw.agent.service.rpc;

import org.springframework.remoting.rmi.RmiServiceExporter;

/**
 * RmiServiceExporterのラッパークラスです。
 *
 */
public class RmiServiceExporterWrapper extends RmiServiceExporter {

	private String serverId = null;
	private String serviceId = null;

	public RmiServiceExporterWrapper(String serverId, String serviceId) {

		super();

		this.serverId = serverId;
		this.serviceId = serviceId;
	}

	public String getServerId() {
		return this.serverId;
	}

	public String getServiceId() {
		return this.serviceId;
	}

}
