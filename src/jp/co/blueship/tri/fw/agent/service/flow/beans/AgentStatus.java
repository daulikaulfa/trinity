package jp.co.blueship.tri.fw.agent.service.flow.beans;

import java.io.Serializable;

import jp.co.blueship.tri.bm.dao.rmisvc.eb.IRmiSvcDto;

public class AgentStatus implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;


	private IRmiSvcDto rmiSvcDto = null ;
	private boolean status = false ;			//ステータス

	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public IRmiSvcDto getRemoteServiceEntity() {
		return rmiSvcDto;
	}
	public void setRemoteServiceEntity(IRmiSvcDto remoteServiceEntity) {
		this.rmiSvcDto = remoteServiceEntity;
	}

}
