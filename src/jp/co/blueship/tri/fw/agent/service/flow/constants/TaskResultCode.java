package jp.co.blueship.tri.fw.agent.service.flow.constants;

/**
 * タスク単位の処理結果の定数クラスです。
 * <p>
 * 全タスク共通のcode値です。
 * タスク内の詳細結果コード（たとえば、diffの資産単位の変更結果）は、別途タスクごとのルールで規定します。
 *
 */
public class TaskResultCode {

	

	/**
	 * 成功
	 */
	public static final String SUCCESS = "0";
	/**
	 * 処理を実行する必要無し
	 */
	public static final String UN_PROCESS = "1";
	/**
	 * なんらかのエラー
	 */
	public static final String ERROR_PROCESS = "2";
}
