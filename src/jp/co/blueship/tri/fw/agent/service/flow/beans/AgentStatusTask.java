package jp.co.blueship.tri.fw.agent.service.flow.beans;

import java.io.Serializable;

public class AgentStatusTask implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private String serviceUrl = null ;	//コンテキストのパス
	private String envNo = null ;				//環境名
	private String serverNo = null ;			//サーバＮｏ
	private boolean status = false ;			//ステータス
	
	public String getServiceUrl() {
		return serviceUrl;
	}
	public void setServiceUrl( String serviceUrl ) {
		this.serviceUrl = serviceUrl;
	}
	public String getEnvNo() {
		return envNo;
	}
	public void setEnvNo(String envNo) {
		this.envNo = envNo;
	}
	public String getServerNo() {
		return serverNo;
	}
	public void setServerNo(String serverNo) {
		this.serverNo = serverNo;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}

}
