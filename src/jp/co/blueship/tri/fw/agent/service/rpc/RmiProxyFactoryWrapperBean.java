package jp.co.blueship.tri.fw.agent.service.rpc;

import org.springframework.remoting.rmi.RmiProxyFactoryBean;

import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvSrvEntity;

/**
 * RmiProxyFactoryBeanのラッパークラスです。
 *
 */
public class RmiProxyFactoryWrapperBean extends RmiProxyFactoryBean {

	private String serverId = null;
	private String serviceId = null;
	private IBldEnvSrvEntity[] envServer = null;

	public RmiProxyFactoryWrapperBean(
			String serverId, String serviceId, IBldEnvSrvEntity[] envServer ) {

		super();

		this.serverId = serverId;
		this.serviceId = serviceId;
		this.envServer = envServer;

	}


	public String getServerId() {
		return this.serverId;
	}

	public String getServiceId() {
		return this.serviceId;
	}

	public IBldEnvSrvEntity[] getEnvServer() {
		return this.envServer;
	}
}
