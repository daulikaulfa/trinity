package jp.co.blueship.tri.fw.msg;

/**
 * The interface of the messageID.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 */
public interface IMessageId {
    /**
     * MessageIDの文字列を取得します。
     * @return DAO template
     */
	public String getMessageId();
}
