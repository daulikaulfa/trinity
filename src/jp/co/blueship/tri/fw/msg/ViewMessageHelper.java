package jp.co.blueship.tri.fw.msg;

import jp.co.blueship.tri.fw.di.ContextAdapterFactory;

/**
 * プレゼンテーション層からメッセージを文言を直接取得するためのヘルパクラスです。<br/>
 * Mayaaのテンプレートから使用されることを想定しています。
 *
 * @author Takayuki Kubo
 *
 */
public class ViewMessageHelper {

	private ViewMessageHelper() {
		// NOP
	}

	/**
	 * メッセージUM001052Eのメッセージ文言を返します。<br/>
	 * 「入力したユーザIDかパスワードが間違っています。」
	 *
	 * @return メッセージ文言
	 */
	public static final String msgOfInvalidLoginIdOrPassword() {
		return ContextAdapterFactory.getContextAdapter().getMessage(UmMessageId.UM001052E);
	}

	/**
	 * メッセージUM001053Eのメッセージ文言を返します。<br/>
	 * 「この接続は他のユーザによって解除されました。」
	 *
	 * @return メッセージ文言
	 */
	public static final String msgOfInvalidConcurrencySession() {
		return ContextAdapterFactory.getContextAdapter().getMessage(UmMessageId.UM001053E);
	}
	
	public static final String msgOfInvalidConcurrencySession(String lang) {
		return ContextAdapterFactory.getContextAdapter().getMessage(lang,UmMessageId.UM001053E);
	}
}
