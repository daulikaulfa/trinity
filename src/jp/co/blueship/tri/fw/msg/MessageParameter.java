package jp.co.blueship.tri.fw.msg;

/**
 * エラーメッセージに補完するメッセージパラメータを定義します。
 *
 * @author Takayuki Kubo
 *
 */
public enum MessageParameter implements IMessageParameter {

	/**
	 * リポジトリのパス情報
	 */
	REPOSITORY("repository"),
	/**
	 * モジュール名
	 */
	MODULE_NAME("moduleName"),
	/**
	 * モジュール名の配列
	 */
	MODULE_NAME_ARRAY("moduleNameArray"),
	/**
	 * 取得先パス
	 */
	PATH_DEST("pathDest"),
	/**
	 * モジュール名以下の資産パス
	 */
	OBJECT_PATH("objectPath"),
	/**
	 * バージョンタグ
	 */
	VERSION_TAG("versionPath"),
	/**
	 * タグ名
	 */
	LABEL_TAG("labelTag"),
	/**
	 * ブランチ名
	 */
	LABEL_BRANCH("labelBranch"),
	/**
	 * マージ元
	 */
	MERGE_FROM("mergeFrom"),
	/**
	 * マージ先
	 */
	MERGE_TO("mergeTo"),
	/**
	 * 取得するリビジョン番号(Long型)
	 */
	REVISION_NO("revisionNo"),
	/**
	 * 追加する資産パスの配列（フルパス）
	 */
	PATH_ADD_ASSETS("pathAddAssets"),
	/**
	 * 削除する資産パスの配列（フルパス）
	 */
	PATH_DELETE_ASSETS("pathDeleteAssets"),

	/**
	 * 対象申請情報
	 */
	AREQ_INFO("targetAreqInfomation"),
	/**
	 * ベース
	 */
	BASE("base"),
	/**
	 * 変更管理番号
	 */
	CHANGE_MANAGEMENT_NO("changeManagementNo"),
	/**
	 * 対象ロット
	 */
	TARGET_LOT("targetLot"),
	/**
	 * 対象リリース申請
	 */
	TARGET_REL_AREQ("targetReleaseAreq"),
	/**
	 * 対象変更管理
	 */
	TARGET_CHANGE_MANAGEMENT("targetChangeManagement"),
	/**
	 * 対象リリースパッケージ
	 */
	TARGET_RP("targetReleasePackage"),
	TARGET_BP("targetBuildPackage"),
	/**
	 * リリースパッケージ
	 */
	RP("releasePackage"),
	/**
	 * コンフリクトチェック
	 */
	CONFLICT_CHECK("conflictCheck"),
	/**
	 * 対象リリース
	 */
	TARGET_REL("targetRelease"),
	/**
	 * 資産配付
	 */
	ASSET_DISTRIBUTION("assetDistribution"),
	/**
	 * 資産配付ベース
	 */
	ASSET_DISTRIBUTION_BASE("assetDistributionBase"),
	/**
	 * 資源配付登録が完了しました。
	 */
	REGISTERED_ASSET_DISTRIBUTION("registeredAssetDistribution"),
	/**
	 * 資源配付タイマー設定が完了しました。
	 */
	SETTING_TIMER_ASSET_DISTRIBUTION("settingTimerAssetDistribution"),
	/**
	 * 資源配付登録取消が完了しました。
	 */
	CANCELED_REGISTERE_ASSET_DISTRIBUTION("canceledRegisteredAssetDistribution"),
	/**
	 * 日間
	 */
	DAYS("days"),
	/**
	 * アクセス許可のないグループにもロットを一覧表示する
	 */
	ALLOW_LIST_VIEW_CONTENT("allowListViewContent"),
	/**
	 * アクセス許可のないグループにはロットを一覧表示しない
	 */
	DENY_LIST_VIEW_CONTENT("denyListViewContent"),
	/**
	 * Notification
	 */
	ProgressNotificationResultMessage1("ProgressNotification.result.message1"),
	ProgressNotificationResultMessage2("ProgressNotification.result.message2"),
	ProgressNotificationResultMessage3("ProgressNotification.result.message3"),
	ProgressNotificationResultMessage4("ProgressNotification.result.message4"),
	ProgressNotificationResultTime1("ProgressNotification.result.date1"),
	/**
	 * Processing status
	 */
	ProcessiongStatusRemainingTime1("ProcessiongStatus.remaining.time1"),
	ProcessiongStatusRemainingTime2("ProcessiongStatus.remaining.time2"),
	ProcessiongStatusRemainingTime3("ProcessiongStatus.remaining.time3"),
	/**
	 * Recent Updates
	 */
	RecentUpdatesTime1("RecentUpdates.time1"),
	RecentUpdatesTime2("RecentUpdates.time2"),
	RecentUpdatesTime3("RecentUpdates.time3"),
	RecentUpdatesTime4("RecentUpdates.time4"),
	RecentUpdatesTime5("RecentUpdates.time5"),
	RecentUpdatesTime6("RecentUpdates.time6"),

	/**
	 * status check matrix err message
	 */
	StatusCheckMatrixTablesReportName("statusCheckMatrix.tables.report.name"),
	;

	private String key = null;

	/**
	 * コンストラクタ
	 *
	 * @param key メッセージパラメタリソースキー
	 */
	private MessageParameter(String key) {
		this.key = key;
	}

	/**
	 * このメッセージパラメタの項目値をメッセージパラメタリソースファイルより取得する際に使用する、<br/>
	 * リソースキー値を返します。
	 *
	 * @return メッセージパラメタリソースキー
	 */
	@Override
	public String getKey() {
		return key;
	}

	/**
	 * メッセージパラメタータIDを文字列形式で取得します。
	 *
	 * @return メッセージパラメタータID
	 */
	@Override
	public String getParameterId() {
		return name();
	}

}
