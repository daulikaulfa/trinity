package jp.co.blueship.tri.fw.msg;

public enum UmMessageId implements IMessageId {

	/**
	 * Error
	 */
	UM001000E,//ユーザIDを入力してください。
	UM001002E,//ユーザを選択してください。
	UM001003E,//グループを選択してください。
	UM001004E,//ユーザIDは20文字以内で入力してください。
	UM001005E,//ユーザ名を入力してください。
	UM001006E,//ユーザ名は80文字以内で入力してください。
	UM001007E,//パスワードを入力してください。
	UM001008E,//パスワードは{0}文字以内で入力してください。
	UM001009E,//確認用パスワードを入力してください。
	UM001010E,//パスワードと確認用パスワードが異なります。
	UM001011E,//メールアドレスは128文字以内で入力してください。
	UM001012E,//部署名を選択してください。
	UM001013E,//部署名を入力してください。
	UM001014E,//部署名は80文字以内で入力してください。
	UM001015E,//該当する部署が存在しません。削除された可能性があります。
	UM001016E,//ロール名を入力してください。
	UM001017E,//ロール名は40文字以内で入力してください。
	UM001022E,//グループ名を入力してください。
	UM001023E,//グループ名は40文字以内で入力してください。
	UM001024E,//ユーザIDが重複しています。ユーザIDを変更してください。
	UM001027E,//グループユーザを更新しました。
	UM001028E,//管理者(admin)はシステム予約アカウントであるため、その操作はできません。
	UM001029E,//ログイン中のユーザであるため、その操作はできません。
	UM001034E,//ユーザIDは半角英数字で入力してください。
	UM001035E,//パスワード有効期間を選択してください。
	UM001036E,//パスワードは{0}文字以上で入力してください。
	UM001037E,//ロール名が重複しています。ロール名を変更してください。
	UM001038E,//グループ名が重複しています。グループ名を変更してください。
	UM001039E,//ログイン中のユーザに紐付くロールであるため、その操作はできません。
	UM001040E,//管理者(admin)に紐付くロールであるため、その操作はできません。
	UM001041E,//ログイン中のユーザに紐付く部署であるため、その操作はできません。
	UM001042E,//管理者(admin)に紐付く部署であるため、その操作はできません。
	UM001043E,//メールアドレスが正しい形式ではありません。
	UM001044E,//部署名が重複しています。部署名を変更してください。
	UM001045E,//ユーザ名が重複しています。ユーザ名を変更してください。
	UM001046E,//ユーザ名に半角カナを使用することはできません。
	UM001052E,//入力したユーザIDかパスワードが間違っています。
	UM001053E,//この接続は他のユーザによって解除されました。
	UM001054E,//Please write category name
	UM001055E,//Please write Category Name within 140 characters.
	UM001056E,//Category Name is duplicated. Please change Category Name.
	UM001058E,//Please write milestone name
	UM001059E,//Please write Milestone Name within 140 characters.
	UM001060E,//Milestone Name is duplicated. Please change Milestone Name.
	UM001061E,//Please enter the Start Date in YYYY/MM/DD format.
	UM001062E,//Please enter the Due Date in YYYY/MM/DD format.
	UM001063E,//Start Date must be set before Due Date.
	UM001064E,//User does not exist in database.
	UM001065E,//Role does not exist in database.
	UM001066E,//Group does not exist in database.
	UM001067E,//Department does not exist in database.
	UM001068E,// Please select an icon.
	UM001069E,// Please select language.
	UM001070E,// Please select TimeZone.
	UM001071E,// No file upload
	UM001072E,// File type is not allowed
	UM001080E,// Please select data attribute.
	UM001081E,// Please input the Project Name.
	UM001082E,//Please input the Wiki Page Name.
	UM001083E,//Please input the Wiki Contents.
	UM001090E,//The file type is not CSV file.
	UM001091E,//The header columns in the uploaded CSV file is empty or incorrect. Please see the template.
	UM001092E,//Command should be left blank or specify one of add, edit, delete.
	UM001093E,//The number of users that can process at one time is exceeded, the maximum number of users is {0}
	UM001094E,//A duplicate user ID {0} exists in the CSV file.
	UM001095E,//A user corresponding to the specified user ID = {0} does not exist.
	UM001096E,//A duplicate user name {0} exists in the CSV file.
	UM001097E,//Please enter the password expiration date for user ID {0}
	UM001098E,//Password expiration time is 1: {0} days, -1: no expiration time limit, 0: change at the time of first login. Please specify.
	UM001099E,//Language for user {0} is invalid
	UM001100E,//Timezone for user {0} is invalid
	UM001101E,//User icon for user {0} is invalid
	UM001102E,//The structure of CSV file is incorrect. Please check
	UM001103E,//Could not remove Admin user from Admin group
	UM001104E,//Could not remove Admin role from Admin group
	UM001105E,//Unable to perform this action because it is system reserved role
	UM001106E,//Unable to perform this action because it is system reserved group


	/**
	 * Infomation
	 */
	UM003010I,//パスワードに指定できる文字は、半角英字(a-z,A-Z)、半角数字(0-9)、ダッシュ(-)、アンダースコア(_)、ピリオド(.)のみです。
	UM003011I,//({0})を新規作成しました
	UM003012I,//({0})を編集しました
	UM003013I,//({0})を削除しました
	UM003015I,//変更管理情報の登録を行いました
	UM003014I,//検索条件を保存しました
	UM003016I,//ロット作成を実行しています
	UM003017I,//ロット作成に失敗しました
	UM003018I,//ロット作成が完了しました
	UM003019I,//ビルドパッケージ作成を実行しています
	UM003020I,//ビルドパッケージ作成に失敗しました
	UM003021I,//ビルドパッケージ作成が完了しました
	UM003022I,//ビルドパッケージクローズを実行しています
	UM003023I,//ビルドパッケージクローズに失敗しました
	UM003024I,//ビルドパッケージクローズが完了しました
	UM003025I,//リリースパッケージ作成を実行しています
	UM003026I,//リリースパッケージ作成に失敗しました
	UM003027I,//リリースパッケージ作成が完了しました
	UM003028I,//コンフリクトチェックを実行しています
	UM003029I,//コンフリクトチェックに失敗しました
	UM003030I,//コンフリクトチェックが完了しました
	UM003031I,//コンフリクトチェックに失敗しました
	UM003032I,//マージ処理を実行しています
	UM003033I,//マージ処理に失敗しました
	UM003034I,//マージ処理が完了しました
	UM003048I,//{0}年{1}月{2}日
	UM003049I,//{0}さんから承認待ちの申請があります
	UM003050I,//{0}さんから全体連絡があります
	UM003051I,//パスワードの有効期限が近づいています
	UM003052I,//あなたのご使用のパスワードの有効期限は{0}年{1}月{2}日です。個人設定からパスワードの変更をお願いします。
	UM003053I,//製品ラインセンスの有効期限が近づいています
	UM003054I,//ご使用のtrinityの製品ライセンスの有効期限は{0}年{1}月{2}日です。有効期限を延長される場合は、管理者設定から新しいプロダクトキーを登録してください。
	UM003055I,//テーマの設定を更新しました
	UM003056I,//パスワードを変更しました
	UM003057I,//ロット作成を受け付けました
	UM003058I,//Language and Timezone has been updated
	UM003059I,//Notification has been updated
	UM003060I, //Category ({0}) has been removed
	UM003061I, //Category ({0}) has been created
	UM003062I, //Category ({0}) has been edited
	UM003063I, //Milestone ({0}) has been removed
	UM003064I, //Milestone ({0}) has been created
	UM003065I, //Milestone ({0}) has been edited
	UM003066I, //Wiki ({0}) has been created
	UM003067I, //Wiki ({0}) has been edited
	UM003068I, //Wiki ({0}) has been removed
	UM003069I, //Project Setting has been updated
	UM003070I, //User ({0}) has been created
	UM003071I, //User ({0}) has been edited
	UM003072I, //User ({0}) has been removed
	UM003073I, //Department ({0}) has been created
	UM003074I, //Department ({0}) has been edited
	UM003075I, //Department ({0}) has been removed
	UM003076I, //Group ({0}) has been created
	UM003077I, //Group ({0}) has been edited
	UM003078I, //Group ({0}) has been removed
	UM003079I, //Role ({0}) has been created
	UM003080I, //Role ({0}) has been edited
	UM003081I, //Role ({0}) has been removed
	UM003082I, //User Setting has been updated
	UM003083I, //Profile has been updated
	UM003084I, //Look And Feel has been updated.
	UM003085I, //Multiple Users has been created
	/**
	 * Fatal
	 */
	UM004000F,//指定されたグループIDに該当するグループ情報が取得できませんでした。groupId:{0}
	UM004001F,//指定されたユーザIDに該当するユーザ情報が取得できませんでした。userId:{0}
	UM004002F,//グループ情報が取得できませんでした。
	UM004003F,//指定された部署IDに該当する部署情報が取得できませんでした。departmentId:{0}
	UM004004F,//部署情報が取得できませんでした。
	UM004005F,//ロール情報が取得できませんでした。
	UM004006F,//指定されたロールIDに該当するロール情報が取得できませんでした。roleId:{0}
	UM004007F,//指定されたグループ名に該当するグループ情報が取得できませんでした。groupName:{0}
	/**
	 * System Error
	 */
	UM005001S,//メソッドの使用方法に誤り : パラメタが指定されていません。
	UM005002S,//部署削除処理中にエラーが発生しました。
	UM005003S,//部署削除画面の表示情報設定処理中にエラーが発生しました。
	UM005004S,//部署登録処理中にエラーが発生しました。
	UM005005S,//部署処理中にエラーが発生しました。
	UM005006S,//部署一覧画面の表示情報設定処理中にエラーが発生しました。
	UM005007S,//部署変更処理中にエラーが発生しました。
	UM005008S,//部署変更画面の表示情報設定処理中にエラーが発生しました。
	UM005009S,//グループ削除処理中にエラーが発生しました。
	UM005010S,//グループ削除画面の表示情報設定処理中にエラーが発生しました。
	UM005011S,//グループ登録処理中にエラーが発生しました。
	UM005012S,//グループ処理中にエラーが発生しました。
	UM005013S,//グループ一覧画面の表示情報設定処理中にエラーが発生しました。"
	UM005014S,//グループ変更処理中にエラーが発生しました。
	UM005015S,//グループ変更画面の表示情報設定処理中にエラーが発生しました。
	UM005019S,//グループユーザ登録画面の表示情報設定処理中にエラーが発生しました。
	UM005020S,//グループユーザ変更処理中にエラーが発生しました。
	UM005021S,//グループユーザ変更画面の表示情報設定処理中にエラーが発生しました。
	UM005028S,//ロール削除処理中にエラーが発生しました。
	UM005029S,//ロール削除画面の表示情報設定処理中にエラーが発生しました。
	UM005030S,//ロール登録処理中にエラーが発生しました。
	UM005031S,//ロール処理中にエラーが発生しました。
	UM005032S,//ロール一覧画面の表示情報設定処理中にエラーが発生しました。
	UM005033S,//ロール変更処理中にエラーが発生しました。
	UM005034S,//ロール変更画面の表示情報設定処理中にエラーが発生しました。
	UM005035S,//ユーザ削除処理中にエラーが発生しました。
	UM005036S,//ユーザ削除画面の表示情報設定処理中にエラーが発生しました。
	UM005037S,//ユーザ登録処理中にエラーが発生しました。
	UM005038S,//ユーザ登録画面の表示情報設定処理中にエラーが発生しました。
	UM005039S,//ユーザ一覧画面の表示情報設定処理中にエラーが発生しました。
	UM005040S,//ユーザ変更処理中にエラーが発生しました。
	UM005041S,//ユーザ変更画面の表示情報設定処理中にエラーが発生しました。
	UM005050S,//Wiki画面の表示情報設定中にエラーが発生しました。
	UM005057S,// Error occurred during the processing of {0} event service.
	DEFAULT;// デフォルト 特定の用途で使用する。

	@Override
	public String getMessageId() {
		return name();
	}

}
