package jp.co.blueship.tri.fw.msg;

/**
 * エラーメッセージに補完するメッセージパラメータにIDを付与するための共通インタフェースです。
 *
 * @author Takayuki Kubo
 */
public interface IMessageParameter {

	/**
	 * メッセージパラメタータIDを文字列形式で取得します。
	 *
	 * @return メッセージパラメタータID
	 */
	public String getParameterId();

	/**
	 * このメッセージパラメタの項目値をメッセージパラメタリソースファイルより取得する際に使用する、<br/>
	 * リソースキー値を返します。
	 *
	 * @return メッセージパラメタリソースキー
	 */
	public String getKey();
}
