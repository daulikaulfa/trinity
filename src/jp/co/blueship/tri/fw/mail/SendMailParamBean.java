/*
 * SendMailParamBean.java
 *
 * Created on 2006/12/04
 */
package jp.co.blueship.tri.fw.mail;

/**
 * メール送信に必要な各種パラメタを格納するためのクラスです。
 *
 */
public class SendMailParamBean {

    

    private String mailSmtpHost = null;
    private Integer mailSmtpPort = new Integer( 25 );

    private MailSecurityType mailSecurityType = MailSecurityType.NONE ;
    private boolean isAuth = false;
    private String authUserID = null;
    private String authPassword = null;

    private String sendFrom = null;
    private String sendFromName = null;
    private String[] sendTo = null;
    private String[] sendCC = null;
    private String[] sendBCC = null;

    private String subject = null;
    private String contents = null;

    private boolean isAttachmentFile = false;
    private String[] attachmentFilePaths = null;

    /**
     * デフォルトコンストラクタ
     *
     */
    public SendMailParamBean() {

    }

	/**
	 * SMTPサーバのアドレスを返します。
	 *
	 * @return SMTPサーバのアドレス
	 */
	public String getMailSmtpHost() {
		return mailSmtpHost;
	}

	/**
	 * SMTPサーバのアドレスを設定します。
	 *
	 * @param mailSmtpHost SMTPサーバのアドレス
	 */
	public void setMailSmtpHost(String mailSmtpHost) {
		this.mailSmtpHost = mailSmtpHost;
	}

	/**
	 * SMTPの接続先ポートを返します。
	 * <br>この項目が設定されない場合、25がデフォルトとして戻されます。
	 *
	 * @return 接続先ポート
	 */
	public Integer getMailSmtpPort() {
		return mailSmtpPort;
	}

	/**
	 * SMTPの接続先ポートを設定します。
	 * <br>この項目は省略可能です。
	 * <br>何も設定しない場合、25が適用されます。
	 *
	 * @param mailSmtpPort 接続先ポート
	 */
	public void setMailSmtpPort(Integer mailSmtpPort) {
		this.mailSmtpPort = mailSmtpPort;
	}

	/**
	 * メールサーバへの認証が必要かどうかを返します。
	 *
	 * @return メールサーバへの認証が必要な場合はtrue
	 */
	public boolean isAuth() {
		return isAuth;
	}

	/**
	 * メールサーバへの認証が必要かどうかを設定します。
	 *
	 * @param isAuth メールサーバへの認証が必要な場合はtrue
	 */
	public void setAuth(boolean isAuth) {
		this.isAuth = isAuth;
	}

	/**
	 * 認証ユーザIDを返します。
	 *
	 * @return 認証ユーザID
	 */
	public String getAuthUserID() {
		return authUserID;
	}

	/**
	 * 認証ユーザIDを設定します。
	 *
	 * @param authUserID 認証ユーザID
	 */
	public void setAuthUserID(String authUserID) {
		this.authUserID = authUserID;
	}

	/**
	 * 認証パスワードを返します。
	 *
	 * @return 認証パスワード
	 */
	public String getAuthPassword() {
		return authPassword;
	}

	/**
	 * 認証パスワードを設定します。
	 *
	 * @param authPassword 認証パスワード
	 */
	public void setAuthPassword(String authPassword) {
		this.authPassword = authPassword;
	}

	/**
	 * 送信元を返します。
	 *
	 * @return 送信元
	 */
	public String getSendFrom() {
		return sendFrom;
	}

	/**
	 * 送信元を設定します。
	 *
	 * @param sendFrom 送信元
	 */
	public void setSendFrom(String sendFrom) {
		this.sendFrom = sendFrom;
	}

	/**
	 * 送信元名称を返します。
	 *
	 * @return 送信元名称
	 */
	public String getSendFromName() {
		return sendFromName;
	}

	/**
	 * 送信元名称を設定します。
	 *
	 * @param sendFromName 送信元名称
	 */
	public void setSendFromName(String sendFromName) {
		this.sendFromName = sendFromName;
	}

	/**
	 * 送信先を返します。
	 *
	 * @return sendTo 送信先
	 */
	public String[] getSendTo() {
		return sendTo;
	}

	/**
	 * 送信先を設定します。
	 *
	 * @param sendTo 送信先
	 */
	public void setSendTo(String[] sendTo) {
		this.sendTo = sendTo;
	}

	/**
	 * CCを返します。
	 *
	 * @return CC
	 */
	public String[] getSendCC() {
		return sendCC;
	}

	/**
	 * CCを設定します。
	 *
	 * @param sendCC CC
	 */
	public void setSendCC(String[] sendCC) {
		this.sendCC = sendCC;
	}

	/**
	 * BCCを返します。
	 *
	 * @return BCC
	 */
	public String[] getSendBCC() {
		return sendBCC;
	}

	/**
	 * BCCを設定します。
	 *
	 * @param sendBCC BCC
	 */
	public void setSendBCC(String[] sendBCC) {
		this.sendBCC = sendBCC;
	}

	/**
	 * 題名を返します。
	 *
	 * @return 題名
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * 題名を設定します。
	 *
	 * @param subject 題名
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * メール本文を返します。
	 *
	 * @return contents メール本文
	 */
	public String getContents() {
		return contents;
	}

	/**
	 * メール本文を設定します。
	 *
	 * @param contents メール本文
	 */
	public void setContents(String contents) {
		this.contents = contents;
	}

	/**
	 * ファイルを添付するかどうかを返します。
	 *
	 * @return ファイルを添付する場合はtrue
	 */
	public boolean isAttachmentFile() {
		return isAttachmentFile;
	}

	/**
	 * ファイルを添付するかどうかを設定します。
	 *
	 * @param isAttachmentFile ファイルを添付する場合はtrue
	 */
	public void setAttachmentFile(boolean isAttachmentFile) {
		this.isAttachmentFile = isAttachmentFile;
	}

	/**
	 * 添付ファイルへのパスを返します。
	 *
	 * @return 添付ファイルへのパス
	 */
	public String[] getAttachmentFilePaths() {
		return attachmentFilePaths;
	}

	/**
	 * 添付ファイルへのパスを設定します。
	 *
	 * @param attachmentFile 添付ファイルへのパス
	 */
	public void setAttachmentFilePaths(String[] attachmentFilePaths) {
		this.attachmentFilePaths = attachmentFilePaths;
	}
	/**
	 * メールのセキュリティ方式の種類を取得します。
	 * @return メールのセキュリティ方式の種類
	 */
	public MailSecurityType getMailSecurityType() {
		return mailSecurityType;
	}
	/**
	 * メールのセキュリティ方式の種類をセットします。
	 * @param mailSecurityType メールのセキュリティ方式の種類
	 */
	public void setMailSecurityType(MailSecurityType mailSecurityType) {
		this.mailSecurityType = mailSecurityType;
	}
}
