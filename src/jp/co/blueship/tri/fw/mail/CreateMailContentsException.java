/*
 * CreateMailContentsException.java
 *
 * Created on 2006/12/04
 */
package jp.co.blueship.tri.fw.mail;

/**
 * メールコンテンツ生成に関する例外クラスです。
 */
public class CreateMailContentsException extends Exception {

	
	private static final long serialVersionUID = 1L;

    /**
	 * 詳細メッセージを指定して本クラスを構築します。
	 *
	 * @param message 詳細メッセージ
	 */
	public CreateMailContentsException(String message) {
		super(message);
	}

	/**
	 * 詳細メッセージと、原因を指定して本クラスを構築します。
	 *
	 * @param message 詳細メッセージ
	 * @param cause 原因
	 */
	public CreateMailContentsException(String message, Throwable cause) {
		super(message, cause);
	}

}
