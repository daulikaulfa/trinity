package jp.co.blueship.tri.fw.mail;

/**
 * メールのセキュリティ方式種類の定義名の列挙型です。<br>
 *
 */
public enum MailSecurityType {

	NONE( "NONE" ),
	TLS	( "TLS" ),
	SSL	( "SSL" );

	

	private String value = null;

	private MailSecurityType( String value ) {
		this.value = value;
	}

	public String value() {
		return value;
	}

	public static MailSecurityType value( String name ) {
		for ( MailSecurityType define : values() ) {
			if ( define.value().equals( name ) )
				return define;
		}

		return null;
	}

}