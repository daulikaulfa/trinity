/*
 * MailContentsCreater.java
 *
 * Created on 2006/12/04
 */
package jp.co.blueship.tri.fw.mail;

import java.io.StringWriter;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.exception.MethodInvocationException;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;

import jp.co.blueship.tri.fw.cmn.io.constants.Charset;
import jp.co.blueship.tri.fw.cmn.utils.VelocityUtils;

/**
 * 各業務処理内で行うメール送信処理用のメール本文作成クラスです。
 */
public class MailContentsCreater {

    private String vmResourcePath = null;
    private String vmTemplatePath = null;
    private IMailContentsDataDao iMailContentsDataDao = null;

    /**
     * デフォルトコンストラクタ
     */
    public MailContentsCreater() {
    }

    /**
     * 指定された業務データをもとに、メール本文を生成します。
     *
     * @param source 業務データ
     *
     * @return メール本文
     *
     * @throws CreateMailContentsException メールコンテンツが生成できないできない
     */
    public String createMailContents( Object source) throws CreateMailContentsException {

        /* DAOより、メール本文に挿入するデータを取得する */
    	Map<String, Object> data;
		try {
			data = iMailContentsDataDao.getData(source);
		} catch (Exception e) {
            throw new CreateMailContentsException("Getting Mail Contents Data from DAO is failed.", e);
		}

        /* Velocityテンプレートとデータをマージして、メール本文を生成する */
        String mailContents = marge( data );

    	return mailContents;
    }

    /**
	 * リソースローダがリソースを見つけるためのルートパスを設定します。
	 *
	 * @param vmResourcePath Velocityリソースパス
	 */
	public void setVmResourceLoaderPath(String vmResourcePath) {
		this.vmResourcePath = vmResourcePath;
	}

    /**
	 * Velocityテンプレートパスを設定します。
	 *
	 * @param vmTemplatePath Velocityテンプレートパス
	 */
	public void setVmTemplatePath(String vmTemplatePath) {
		this.vmTemplatePath = vmTemplatePath;
	}

    /**
	 * メール本文作成に必要な業務データを抽出するためのデータアクセスオブジェクト(DAO)を設定します。
	 *
	 * @param iMailContentsDataDao データアクセスオブジェクト(DAO)
	 */
    public void setIMailContentsDataDao( IMailContentsDataDao iMailContentsDataDao) {
        this.iMailContentsDataDao = iMailContentsDataDao;
    }

    /**
     * Velocityを使用してメール本文を生成する
     *
     * @param inputMergedData テンプレートへマージするデータ
     *
     * @return メール本文
     *
     * @throws CreateMailContentsException
     */
    public String marge( Map<String, Object> inputMergedData ) throws CreateMailContentsException {

    	Map<String, Object> mergedData = this.initContentsMap( inputMergedData );

        Properties p = new Properties();

        VelocityUtils.addProperties(Velocity.RESOURCE_LOADER, "file", p);
        p.put("file.resource.loader.class", "org.apache.velocity.runtime.resource.loader.FileResourceLoader");
        VelocityUtils.addProperties("file.resource.loader.path", vmResourcePath, p);
        p.put("file.resource.loader.cache", "true");
        p.put("file.resource.loader.modificationCheckInterval", "2");
        p.put(Velocity.INPUT_ENCODING, Charset.UTF_8.value());
        try {
            Velocity.init(p);
        } catch (Exception e) {
            throw new CreateMailContentsException("Initialize of Velocity is failed.", e);
        }

        VelocityContext context = new VelocityContext();

        if ( mergedData != null ) {
            Iterator<String> it = mergedData.keySet().iterator();
            while ( it.hasNext() ) {
                String key = it.next();
                context.put( key, mergedData.get( key ) );
            }
        }

        Template template;
		try {
			template = Velocity.getTemplate(vmTemplatePath);
		} catch (ResourceNotFoundException e) {
            throw new CreateMailContentsException("Getting Velocity Template is failed.", e);
		} catch (ParseErrorException e) {
            throw new CreateMailContentsException("Getting Velocity Template is failed.", e);
		} catch (Exception e) {
            throw new CreateMailContentsException("Getting Velocity Template is failed.", e);
		}

        StringWriter w = new StringWriter();
        try {
			template.merge( context, w );
		} catch (ResourceNotFoundException e) {
            throw new CreateMailContentsException("Merging Velocity Template is failed.", e);
		} catch (ParseErrorException e) {
            throw new CreateMailContentsException("Merging Velocity Template is failed.", e);
		} catch (MethodInvocationException e) {
            throw new CreateMailContentsException("Merging Velocity Template is failed.", e);
		} catch (Exception e) {
            throw new CreateMailContentsException("Merging Velocity Template is failed.", e);
		}

        return w.toString();
    }

	/**
	 * メールコンテンツを任意の値で置換する際、nullの項目が置換対象外となるため、
	 * 事前に空文字で初期化する
	 *
	 * @param map メールコンテンツの置換マップ
	 * @return 空文字に置換したマップ
	 */
	private final Map<String, Object> initContentsMap( Map<String, Object> map ) {
		if ( null == map )
			return map;

		for ( String key : map.keySet() ) {
			if ( null == map.get(key) ) {
				map.put(key, "");
			}
		}

		return map;
	}

}
