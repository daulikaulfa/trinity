package jp.co.blueship.tri.fw.mail.beans;

import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;
import jp.co.blueship.tri.fw.svc.beans.MailGenericService;

/**
 * 業務詳細フロー（Action）内で、メール送信を行うための、ラッパークラスです。
 *
 * @author Yukihiro Eguchi
 *
 */
public class ActionMail extends ActionPojoAbstract<IGeneralServiceBean> {

	private MailGenericService mailGenericService = null;

	public void setMail(MailGenericService mailGenericService) {
		this.mailGenericService = mailGenericService;
	}

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {
		IGeneralServiceBean info = serviceDto.getServiceBean();

		if ( null == info ) {
			return serviceDto;
		}

		this.mailGenericService.execute( serviceDto );
		return serviceDto;
	}

}
