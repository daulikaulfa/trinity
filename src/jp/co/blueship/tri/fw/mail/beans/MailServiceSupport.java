package jp.co.blueship.tri.fw.mail.beans;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.mail.beans.dto.MailServiceBean;
import jp.co.blueship.tri.fw.msg.SmMessageId;

/**
 * 業務メールを送信するためのPojoをサポートするクラスです。
 * <br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2008
 */
public abstract class MailServiceSupport implements IDomain<IGeneralServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private MailServiceTemplate template = new MailServiceTemplate();

	private List<String> column = new ArrayList<String>();
	private List<IDomain<MailServiceBean>> fromList = new ArrayList<IDomain<MailServiceBean>>();
	private List<IDomain<MailServiceBean>> toList = new ArrayList<IDomain<MailServiceBean>>();
	private List<IDomain<MailServiceBean>> ccList = new ArrayList<IDomain<MailServiceBean>>();
	private List<IDomain<MailServiceBean>> bccList = new ArrayList<IDomain<MailServiceBean>>();

	/**
	 * インスタンス生成時に自動的に設定されます。
	 * @param column 該当項目名
	 */
	public final void setColumn( List<String> column ) {
		this.column = column;
	}
	/**
	 * メール本文に設定する値の該当項目名を戻します。
	 * @return 取得したインスタンスを戻します。
	 */
	public final List<String> getColumn() {
		return column;
	}

	/**
	 * インスタンス生成時に自動的に設定されます。
	 * @param from Mail送信先
	 */
	public final void setFrom( List<IDomain<MailServiceBean>> from ) {
		this.fromList = from;
	}
	/**
	 * fromのＥアドレスを取得するPojoを戻します。
	 * @return 取得したインスタンスを戻します。
	 */
	public final List<IDomain<MailServiceBean>> getFrom() {
		return fromList;
	}

	/**
	 * インスタンス生成時に自動的に設定されます。
	 * @param to Mail送信先
	 */
	public final void setTo( List<IDomain<MailServiceBean>> to ) {
		this.toList = to;
	}
	/**
	 * toのＥアドレスを取得するPojoを戻します。
	 * @return 取得したインスタンスを戻します。
	 */
	public final List<IDomain<MailServiceBean>> getTo() {
		return toList;
	}

	/**
	 * インスタンス生成時に自動的に設定されます。
	 * @param cc Mail送信先
	 */
	public final void setCc( List<IDomain<MailServiceBean>> cc ) {
		this.ccList = cc;
	}
	/**
	 * ccのＥアドレスを取得するPojoを戻します。
	 * @return 取得したインスタンスを戻します。
	 */
	public final List<IDomain<MailServiceBean>> getCc() {
		return ccList;
	}

	/**
	 * インスタンス生成時に自動的に設定されます。
	 * @param bcc Mail送信先
	 */
	public final void setBcc( List<IDomain<MailServiceBean>> bcc ) {
		this.bccList = bcc;
	}
	/**
	 * bccのＥアドレスを取得するPojoを戻します。
	 * @return 取得したインスタンスを戻します。
	 */
	public final List<IDomain<MailServiceBean>> getBcc() {
		return bccList;
	}

	/**
	 * メール送信を利用するためのサービステンプレートのインスタンスを取得します。
	 * @return 取得したインスタンスを戻します。
	 */
	public final MailServiceTemplate getMailServiceTemplate() {
		return template;
	}

	/**
     * 指定された各種情報に基づいて、メール送信を行います。<BR>
     *
	 * @param dto サービスビーンDTO
	 * @param callback コールバック
	 */
	public void sendMail( IServiceDto<MailServiceBean> dto, IMailCallback callback ) {

		try {
			this.getMailServiceTemplate().sendMail(	dto,
													this.getFrom(),
													this.getTo(),
													this.getCc(),
													this.getBcc(),
													this.getColumn(),
													callback );

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( SmMessageId.SM005188S, e );
		}
	}

}
