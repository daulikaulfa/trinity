package jp.co.blueship.tri.fw.mail.beans.dto;

import java.util.List;

import jp.co.blueship.tri.fw.dao.orm.IEntity;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.fw.svc.beans.dto.SendMailBean;


public class MailServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;

	private SendMailBean from = new SendMailBean();
	private SendMailBean to = new SendMailBean();
	private SendMailBean cc = new SendMailBean();
	private SendMailBean bcc = new SendMailBean();
	private SendMailBean target = new SendMailBean();
    private String vmResourcePath = null;
    private List<IEntity> entity = null;

	public SendMailBean getBcc() {
		return bcc;
	}
	public void setBcc(SendMailBean bcc) {
		this.bcc = bcc;
	}
	public SendMailBean getCc() {
		return cc;
	}
	public void setCc(SendMailBean cc) {
		this.cc = cc;
	}
	public SendMailBean getFrom() {
		return from;
	}
	public void setFrom(SendMailBean from) {
		this.from = from;
	}
	public SendMailBean getTo() {
		return to;
	}
	public void setTo(SendMailBean to) {
		this.to = to;
	}
	public SendMailBean getTarget() {
		return target;
	}
	public void setTarget(SendMailBean target) {
		this.target = target;
	}

    /**
	 * リソースローダがリソースを見つけるためのルートパスを設定します。
	 *
	 * @param vmResourcePath Velocityリソースパス
	 */
	public void setVmResourceLoaderPath(String vmResourcePath) {
		this.vmResourcePath = vmResourcePath;
	}
	/**
	 * リソースローダがリソースを見つけるためのルートパスを取得します。
	 *
	 * @return Velocityリソースパスを戻します
	 */
	public String getVmResourceLoaderPath() {
		return this.vmResourcePath;
	}

	/**
	 * 本メールの送信対象エンティティを取得します。
	 *
	 * @return
	 */
	public List<IEntity> getEntity() {
		return entity;
	}
	/**
	 *  本メールの送信対象エンティティを設定します。
	 *
	 * @param entity
	 */
	public void setEntity(List<IEntity> entity) {
		this.entity = entity;
	}

}
