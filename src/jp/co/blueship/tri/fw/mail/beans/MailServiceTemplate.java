package jp.co.blueship.tri.fw.mail.beans;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import jp.co.blueship.tri.fw.cmn.utils.SystemProps;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.UmDesignEntryKeyByCommon;
import jp.co.blueship.tri.fw.constants.UmDesignEntryKeyByMail;
import jp.co.blueship.tri.fw.dao.orm.IEntity;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.mail.CreateMailContentsException;
import jp.co.blueship.tri.fw.mail.MailContentsCreater;
import jp.co.blueship.tri.fw.mail.MailSecurityType;
import jp.co.blueship.tri.fw.mail.MailSender;
import jp.co.blueship.tri.fw.mail.SendMailException;
import jp.co.blueship.tri.fw.mail.SendMailParamBean;
import jp.co.blueship.tri.fw.mail.beans.dto.MailServiceBean;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.security.constants.PasswordCategory;
import jp.co.blueship.tri.fw.sm.dao.passmgt.IPassMgtDao;
import jp.co.blueship.tri.fw.sm.dao.passmgt.eb.IPassMgtEntity;
import jp.co.blueship.tri.fw.sm.dao.passmgt.eb.PassMgtCondition;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;

/**
 * メールを利用するためのサービステンプレートクラスです。
 */
public class MailServiceTemplate {

	private static final IContextAdapter ac = ContextAdapterFactory.getContextAdapter();
	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private static final String SUBJECT_SUCCESS_TEMPLATE = "subject.success";
	private static final String SUCCESS_TEMPLATE = "content.success";

	private static final String SUBJECT_ERROR_TEMPLATE = "subject.error";
	private static final String ERROR_TEMPLATE = "content.error";

	private static final String SUBJECT_SYSTEM_ERROR_TEMPLATE = "FlowSystemErrorService.subject.syserror";
	private static final String SYSTEM_ERROR_TEMPLATE = "FlowSystemErrorService.content.syserror";

	private enum DaoName {
		SM_PASS_MGT( "smPassMgtDao" );

		private String value = null;

		private DaoName( String value ) {
			this.value = value;
		}

		public String value() {
			return this.value;
		}
	}

	/**
     * 指定された各種情報に基づいて、メール送信を行います。<BR>
     *
	 * @param dto サービスビーンDTO
	 * @param fromList fromアドレスを取得するPojo
	 * @param toList toアドレスを取得するPojo
	 * @param ccList ccアドレスを取得するPojo
	 * @param bccList bccアドレスを取得するPojo
	 * @param columnList メール本文に設定する値を取得するPojo
	 * @param callback コールバック
	 */
	public void sendMail( IServiceDto<MailServiceBean> dto,
							List<IDomain<MailServiceBean>> fromList,
							List<IDomain<MailServiceBean>> toList,
							List<IDomain<MailServiceBean>> ccList,
							List<IDomain<MailServiceBean>> bccList,
							List<String> columnList,
							IMailCallback callback ) {

		try {
			SendMailParamBean param = this.getSendMailParamBean(
															dto,
															fromList,
															toList,
															ccList,
															bccList );

			param.setSubject( this.getSubject( dto.getServiceBean(), callback ) );
			param.setContents( this.getContents( dto.getServiceBean(), columnList, callback ) );

			MailSender.sendMail( param );

		} catch (CreateMailContentsException e) {
			LogHandler.fatal( log , e ) ;
			throw new TriSystemException( SmMessageId.SM005192S , e , "CreateMailContentsError" );
		} catch (SendMailException e) {
			LogHandler.fatal( log , e ) ;
			throw new TriSystemException( SmMessageId.SM005192S , e , "SendMailError" );
		}

	}

	private SendMailParamBean getSendMailParamBean( 	IServiceDto<MailServiceBean> dto,
														List<IDomain<MailServiceBean>> fromList,
														List<IDomain<MailServiceBean>> toList,
														List<IDomain<MailServiceBean>> ccList,
														List<IDomain<MailServiceBean>> bccList ) {

		MailServiceBean bean = dto.getServiceBean();
		SendMailParamBean param = new SendMailParamBean();

		param.setMailSmtpHost( sheet.getValue(UmDesignEntryKeyByMail.mailSmtpHost) );
		param.setMailSmtpPort( sheet.intValue(UmDesignEntryKeyByMail.mailSmtpPort) );
		param.setAuthUserID( sheet.getValue(UmDesignEntryKeyByMail.mailAuthUserID) );
		param.setMailSecurityType( getMailSecutiryType() ) ;

		if ( ! TriStringUtils.isEmpty( param.getAuthUserID() ) ) {
			param.setAuth		( true );

			String category = PasswordCategory.MAIL_SECURITY.getValue();
			param.setAuthPassword( this.getPassword( category, param.getMailSmtpHost(), param.getAuthUserID() ) );
		} else {
			param.setAuth		( false );
		}

		//メール送信者の名前・アドレス
		param.setSendFrom	( sheet.getValue(UmDesignEntryKeyByMail.mailOwnerAddress) );
		param.setSendFromName( sheet.getValue(UmDesignEntryKeyByMail.mailOwnerName) );

		for ( IDomain<MailServiceBean> pojo : fromList ) {
			bean.setTarget( bean.getFrom() );
			pojo.execute(dto);
		}
		if( 0 == bean.getFrom().getUsersView().size() ) {
			throw new TriSystemException( SmMessageId.SM004202F ) ;
		}


		List<IUserEntity> userList = new ArrayList<IUserEntity>() ;
		List<String> addressList = new ArrayList<String>();
		for ( IDomain<MailServiceBean> pojo : toList ) {
			bean.setTarget( bean.getTo() );
			pojo.execute(dto);
			addressList.addAll		( this.getUserMailAddress(bean.getTo().getUsersView()) );
			addressList = this.delDuplicateUserMailAddress(addressList);
			userList.addAll( bean.getTo().getUsersView() ) ;
		}
		param.setSendTo((String[])addressList.toArray( new String[]{} ));
		bean.getTo().setUsersView( this.delDuplicateUser( userList ) ) ;
		if( 0 == bean.getTo().getUsersView().size() ) {
			throw new TriSystemException( SmMessageId.SM004203F ) ;
		}

		userList = new ArrayList<IUserEntity>() ;
		addressList = new ArrayList<String>();
		for ( IDomain<MailServiceBean> pojo : ccList ) {
			bean.setTarget( bean.getCc() );
			pojo.execute( dto ) ;
			addressList.addAll		(  this.getUserMailAddress(bean.getCc().getUsersView())  );
			addressList = this.delDuplicateUserMailAddress(addressList);
			userList.addAll( bean.getCc().getUsersView() ) ;
		}
		Collections.sort( addressList ) ;
		param.setSendCC((String[])addressList.toArray( new String[]{} ));
		List<IUserEntity> userEntityList = this.delDuplicateUser( userList ) ;


		Collections.<IUserEntity>sort( userEntityList , this.getComparator() ) ;
		bean.getCc().setUsersView( userEntityList ) ;

		userList = new ArrayList<IUserEntity>() ;
		addressList = new ArrayList<String>();
		for ( IDomain<MailServiceBean> pojo : bccList ) {
			bean.setTarget( bean.getBcc() );
			pojo.execute(dto);
			addressList.addAll		(  this.getUserMailAddress(bean.getBcc().getUsersView())  );
			addressList = this.delDuplicateUserMailAddress(addressList);
			userList.addAll( bean.getBcc().getUsersView() ) ;
		}
		param.setSendBCC((String[])addressList.toArray( new String[]{} ));
		bean.getBcc().setUsersView( this.delDuplicateUser( userList ) ) ;

		return param;
	}


	/**
	 * 複数のpojoから取得したメールアドレスの重複を除外します。
	 * @param preList
	 * @return
	 */
	private List<String> delDuplicateUserMailAddress(List<String> list ){
		List<String> newList = new ArrayList<String>();
		Set<String> existSet = new HashSet<String>() ;

		for( String address : list ) {
			if( ! existSet.contains( address ) ) {
				existSet.add( address ) ;
				newList.add( address ) ;
			}
		}

		return newList;
	}

	/**
	 * 複数のpojoから取得したUserを除外します。
	 * @param preList
	 * @return
	 */
	private List<IUserEntity> delDuplicateUser(List<IUserEntity> list ){
		List<IUserEntity> newList = new ArrayList<IUserEntity>();
		Set<String> existSet = new HashSet<String>() ;

		for( IUserEntity user : list ) {
			if( ! existSet.contains( user.getUserId() ) ) {
				existSet.add( user.getUserId() ) ;
				newList.add( user ) ;
			}
		}

		return newList;
	}
	/**
	 * ユーザ情報リストから、メールアドレスを抽出します。
	 * <br>重複するメールアドレスがあれば、同じアドレスは、１つに絞り込まれます。
	 *
	 * @param users
	 * @return 取得したメールアドレス
	 */
	private List<String> getUserMailAddress( List<IUserEntity> users ) {
		List<String> list = new ArrayList<String>();
		Map<String, Boolean> map = new HashMap<String, Boolean>();

		for ( IUserEntity user : users ) {
			if ( TriStringUtils.isEmpty(user.getEmailAddr() ) )
				continue;

			map.put(user.getEmailAddr(), true);
		}

		for ( IUserEntity user : users ) {
			if ( map.containsKey(user.getEmailAddr()) && map.get(user.getEmailAddr()).booleanValue()  ) {
				list.add( user.getEmailAddr() );
				map.put(user.getEmailAddr(), false);
			}
		}

		return list;
	}

	/**
	 * メール送信の件名を取得します。
	 *
	 * @param bean メールサービス情報
	 * @param callback コールバック
	 * @return 件名を戻します。
	 * @throws CreateMailContentsException
	 */
	private final String getSubject( MailServiceBean bean, IMailCallback callback )  throws CreateMailContentsException {
		MailContentsCreater contents = new MailContentsCreater();
		Map<String, Object> data = new HashMap<String, Object>();

		contents.setVmResourceLoaderPath( bean.getVmResourceLoaderPath() );

		String mailTemplatePath = sheet.getValue( UmDesignEntryKeyByCommon.mailTemplatesRelativePath );
		if ( !StringUtils.endsWith( mailTemplatePath, "/" ) ) {
			mailTemplatePath = mailTemplatePath + "/";
		}

		if ( null != bean.getSystemThrowable() ) {
			contents.setVmTemplatePath( mailTemplatePath + addToLocale( SUBJECT_SYSTEM_ERROR_TEMPLATE ) );
		}

		if ( null != bean.getBusinessThrowable() ) {
			contents.setVmTemplatePath( mailTemplatePath + bean.getFlowAction() + "." + addToLocale( SUBJECT_ERROR_TEMPLATE ) );
		}

		if ( null == bean.getSystemThrowable() && null == bean.getBusinessThrowable() ) {
			contents.setVmTemplatePath( mailTemplatePath + bean.getFlowAction() + "." + addToLocale(SUBJECT_SUCCESS_TEMPLATE) );
		}

		if ( null != callback )
			data = callback.getSubject(bean, data);

		return contents.marge( data );
	}

	/**
	 * メール送信の内容を取得します。
	 *
	 * @param bean メールサービス情報
	 * @param columnList メール本文に設定する値を取得するPojo
	 * @param callback コールバック
	 * @return 件名を戻します。
	 * @throws CreateMailContentsException
	 */
	private final String getContents( MailServiceBean bean,
										List<String> columnList,
										IMailCallback callback )
			throws CreateMailContentsException {

		MailContentsCreater contents = new MailContentsCreater();
		Map<String, Object> data = new HashMap<String, Object>();

		contents.setVmResourceLoaderPath( bean.getVmResourceLoaderPath() );

		String mailTemplatePath = sheet.getValue( UmDesignEntryKeyByCommon.mailTemplatesRelativePath );
		if ( !StringUtils.endsWith( mailTemplatePath, "/" ) ) {
			mailTemplatePath = mailTemplatePath + "/";
		}

		if ( null != bean.getSystemThrowable() ) {
			contents.setVmTemplatePath( mailTemplatePath + addToLocale( SYSTEM_ERROR_TEMPLATE ) );
		}

		if ( null != bean.getBusinessThrowable() ) {
			contents.setVmTemplatePath( mailTemplatePath + bean.getFlowAction() + "." + addToLocale( ERROR_TEMPLATE ) );
		}

		if ( null == bean.getSystemThrowable() && null == bean.getBusinessThrowable() ) {
			contents.setVmTemplatePath( mailTemplatePath + bean.getFlowAction() + "." + addToLocale( SUCCESS_TEMPLATE ) );
		}

		for ( String column : columnList ) {
			for ( IEntity entity : bean.getEntity() ) {
				if ( null != entity ) {

					if(! TriPropertyUtils.isReadable( entity, column ) ) {
						continue;
					}

					String value = (String)TriPropertyUtils.getProperty( entity, column );
					if(null==value){
						value="";
					}

					// entityが複数指定されることを考慮し、項目が空の場合には空として設定する
					// 後勝ちのため、優先したい情報をリストの後ろに詰めること
					data.put(column, value);

				}
			}
		}

		if ( null != callback )
			data = callback.getContents(bean, data);

		return contents.marge( data );
	}

	/**
	 * passwordテーブルから、対応する業務のパスワードを取得する
	 *
	 * @param category 業務コード
	 * @param server サーバ名
	 * @param passUser ユーザ名
	 * @return 取得したパスワード
	 */
	private final String getPassword( String category , String server , String passUser ) {

		if ( TriStringUtils.isEmpty(category) ||
				TriStringUtils.isEmpty(server) ||
				TriStringUtils.isEmpty(passUser) ) {
			throw new TriSystemException( SmMessageId.SM005179S , category , server , passUser );
		}

		IPassMgtDao dao = (IPassMgtDao)ac.getBean( DaoName.SM_PASS_MGT.value() );
		PassMgtCondition condition = new PassMgtCondition();
		condition.setPassCtgCd(category);
		condition.setHostNm(server);
		condition.setUserId(passUser);
		IPassMgtEntity entity = dao.findByPrimaryKey( condition.getCondition() );

		if ( null == entity ) {
			throw new TriSystemException( SmMessageId.SM004204F , category , server , passUser);
		}

		//セキュリティのため、デバックオフ
		//LogHandler.debug( log , "取得パスワード： " + entity.getPassword() ) ;

		return entity.getPassword();
	}

	private Comparator<IUserEntity> getComparator() {

		Comparator<IUserEntity> comparator = new Comparator<IUserEntity>() {
			public int compare( IUserEntity obj0 , IUserEntity obj1 ) {
				String name0 = obj0.getUserNm() ;
				String name1 = obj1.getUserNm() ;

				int ret = 0 ;
				if( ( ret = name0.compareTo( name1 ) ) == 0 ) {
					String adr0 = obj0.getEmailAddr() ;
					String adr1 = obj1.getEmailAddr() ;
					ret = adr0.compareTo( adr1 ) ;
				}
				return ret ;
			}
		};
		return comparator ;
	}

	/**
	 * メール送信のセキュリティ認証の種類を取得する
	 *
	 * @return 取得した情報
	 */
	private MailSecurityType getMailSecutiryType() {

		String value = sheet.getValue( UmDesignEntryKeyByMail.mailSecurityType );

		if ( TriStringUtils.isEmpty(value) )
			return MailSecurityType.NONE;

		return MailSecurityType.value( value );
	}

	/**
	 * 入力されたfileNameにLocaleと拡張子.vmをつけて返します。
	 * @param fileName
	 * @return flileName + Locale + .vm
	 */
	private String addToLocale(String fileName){

		StringBuilder result = new StringBuilder( fileName );
		result.append("_").append( SystemProps.TriSystemLanguage.getProperty() ).append(".vm");
		return result.toString();
	}

}
