package jp.co.blueship.tri.fw.mail.beans;

import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;
import jp.co.blueship.tri.fw.svc.beans.MailSystemErrorService;

/**
 * 業務詳細フロー（Action）内で、システムエラーメール送信を行うための、ラッパークラスです。
 *
 * @author Yukihiro Eguchi
 *
 */
public class ActionMailSystemError extends ActionPojoAbstract<IGeneralServiceBean> {

	private MailSystemErrorService mailSystemErrorService = null;

	public void setMail(MailSystemErrorService mailSystemErrorService) {
		this.mailSystemErrorService = mailSystemErrorService;
	}

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {
		IGeneralServiceBean info = serviceDto.getServiceBean();

		if ( null == info ) {
			return serviceDto;
		}

		this.mailSystemErrorService.execute( serviceDto );
		return serviceDto;
	}

}
