package jp.co.blueship.tri.fw.mail.beans;

import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;
import jp.co.blueship.tri.fw.svc.beans.MailBusinessErrorService;

/**
 * 業務詳細フロー（Action）内で、業務エラーメール送信を行うための、ラッパークラスです。
 *
 * @author Yukihiro Eguchi
 *
 */
public class ActionMailBusinessError extends ActionPojoAbstract<IGeneralServiceBean> {

	private MailBusinessErrorService mailBusinessErrorService = null;

	public void setMail(MailBusinessErrorService mailBusinessErrorService) {
		this.mailBusinessErrorService = mailBusinessErrorService;
	}

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {
		IGeneralServiceBean info = serviceDto.getServiceBean();

		if ( null == info ) {
			return serviceDto;
		}

		this.mailBusinessErrorService.execute( serviceDto );
		return serviceDto;
	}

}
