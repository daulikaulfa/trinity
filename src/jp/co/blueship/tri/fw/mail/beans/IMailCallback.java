package jp.co.blueship.tri.fw.mail.beans;

import java.util.Map;

import jp.co.blueship.tri.fw.mail.CreateMailContentsException;
import jp.co.blueship.tri.fw.mail.beans.dto.MailServiceBean;

/**
 * コールバックインタフェースです。
 *
 */
public interface IMailCallback {

	

	/**
	 * メール件名を戻します。
	 *
	 * @param bean メールサービス情報
	 * @param data テンプレートへマージするデータ
	 * @return テンプレートへマージするデータ
	 * @throws CreateMailContentsException
	 */
	public Map<String, Object> getSubject( MailServiceBean bean, Map<String, Object> data )  throws CreateMailContentsException;

	/**
	 * メール本文を戻します。
	 *
	 * @param bean メールサービス情報
	 * @param data テンプレートへマージするデータ
	 * @return テンプレートへマージするデータ
	 * @throws CreateMailContentsException
	 */
	public Map<String, Object> getContents( MailServiceBean bean, Map<String, Object> data )  throws CreateMailContentsException;

}
