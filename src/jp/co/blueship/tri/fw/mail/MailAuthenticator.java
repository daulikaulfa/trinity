/*
 * MailAuthenticator.java
 *
 * Created on 2006/12/04
 */
package jp.co.blueship.tri.fw.mail;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

/**
 * メールサーバ認証を行うための認証ハンドラです。
 */
public class MailAuthenticator extends Authenticator {

    

    private String userID = null;
    private String password = null;


	/**
	 * ユーザID, パスワードを指定して本クラスを構築します。
	 *
	 * @param userID ユーザID
	 * @param password パスワード
	 */
	public MailAuthenticator( String userID, String password ) {
    	this.userID = userID;
    	this.password = password;
    }


	/**
	 * パスワード認証が必要な場合に呼び出されます。
	 *
	 * @return ユーザID, パスワードが格納されたPasswordAuthenticationオブジェクト
	 *
	 * @see javax.mail.Authenticator#getPasswordAuthentication()
	 */
	protected PasswordAuthentication getPasswordAuthentication() {
		return new PasswordAuthentication(userID, password);
	}



}
