/*
 * SendMailException.java
 *
 * Created on 2006/12/04
 */
package jp.co.blueship.tri.fw.mail;

/**
 * メール送信に関する例外クラスです。
 */
public class SendMailException extends Exception {

	
	private static final long serialVersionUID = 1L;

    /**
	 * 詳細メッセージを指定して本クラスを構築します。
	 *
	 * @param message 詳細メッセージ
	 */
	public SendMailException(String message) {
		super(message);
	}

	/**
	 * 詳細メッセージと、原因を指定して本クラスを構築します。
	 *
	 * @param message 詳細メッセージ
	 * @param cause 原因
	 */
	public SendMailException(String message, Throwable cause) {
		super(message, cause);
	}

}
