/*
 * IMailContentsDataDao.java
 *
 * Created on 2006/12/04
 */
package jp.co.blueship.tri.fw.mail;

import java.util.Map;


/**
 * メール本文作成に必要な業務データを抽出するためのデータアクセスオブジェクト(DAO)インタフェースです。
 */
public interface IMailContentsDataDao {

    

    /**
     * 指定された業務データをもとに、メール本文に挿入するためのデータを返します。
     *
     * @param source 業務データ
     *
     * @return メール本文に挿入するためのデータ
     *
     * @throws Exception データが抽出できない
     */
    public Map<String, Object> getData( Object source ) throws Exception;

}
