/*
 * MailSender.java
 *
 * Created on 2006/12/04
 */
package jp.co.blueship.tri.fw.mail;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Properties;
import java.util.Set;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.SendFailedException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.log.TriLogMessage;

/**
 * メール送信クラス
 *
 */
public class MailSender {

	private static final ILog log = TriLogFactory.getInstance();

	/**
	 * 指定された各種情報に基づいて、メール送信を行います。<BR>
	 * メール送信に必要なすべての情報はSendMailParamBeanに格納してください。
	 * なお、このメソッドはsendMail( param, newSession)の第２引数にfalseを指定した場合と同等です。
	 *
	 * @param param メール送信に必要な各種情報を格納したオブジェクト
	 *
	 * @throws SendMailException メールが送信できない
	 *
	 * @see SendMailParamBean
	 */
	public static void sendMail( SendMailParamBean param ) throws SendMailException {

		IContextAdapter ac = ContextAdapterFactory.getContextAdapter();
		try {
			sendMail(param, true);
		} catch ( SendFailedException e ) {//メールの送信先アドレスが不正の場合には、不正なアドレスを除外して再度送信する。
			try {

				Address[] invalidAddressArray = (Address[])e.getInvalidAddresses() ;
				if( null != invalidAddressArray && 0 != invalidAddressArray.length ) {
					StringBuilder stb = new StringBuilder() ;
					Set<String> invalidAddressSet = new LinkedHashSet<String>() ;

					for( Address invalidAddress : invalidAddressArray ) {
						String adressStr = ( (InternetAddress)invalidAddress).getAddress() ;
						invalidAddressSet.add( adressStr ) ;
						if( 0 != stb.length() ) stb.append( " , " ) ;
						stb.append( adressStr ) ;
					}
					log.info( ac.getMessage( TriLogMessage.LSM0036 , stb.toString() ) ) ;

					String[] toAddressArrayRemoved = removeInvalidAddressByAddressArray( param.getSendTo() , invalidAddressSet ) ;
					param.setSendTo( toAddressArrayRemoved ) ;

					String[] ccAddressArrayRemoved = removeInvalidAddressByAddressArray( param.getSendCC() , invalidAddressSet ) ;
					param.setSendCC( ccAddressArrayRemoved ) ;

					String[] bccAddressArrayRemoved = removeInvalidAddressByAddressArray( param.getSendBCC() , invalidAddressSet ) ;
					param.setSendBCC( bccAddressArrayRemoved ) ;

					sendMail(param, false);
				}

			} catch ( MessagingException ex ) {
				throw new SendMailException("Can not to send Mail.", ex );
			}
		}
	}
	/**
	 * 指定された各種情報に基づいて、メール送信を行います。<BR>
	 * メール送信に必要なすべての情報はSendMailParamBeanに格納してください。
	 *
	 * @param param メール送信に必要な各種情報を格納したオブジェクト
	 * @param newSession 既存のメールセッションを使用せず、新規にセションを作成する場合true
	 *
	 * @throws SendMailException メールが送信できない
	 *
	 * @see SendMailParamBean
	 */
	public static void sendMail( SendMailParamBean param, boolean newSession) throws SendMailException , SendFailedException {

		/**********************************/
		/* Mail Sessionのプロパティの構築 */
		/**********************************/
		Properties props = new Properties();
		/* SMTPサーバのアドレス */
		props.put("mail.smtp.host", param.getMailSmtpHost());
		props.put("mail.host", param.getMailSmtpHost());
		String portNo = param.getMailSmtpPort().toString() ;
		props.setProperty( "mail.smtp.port" , portNo ) ;

		/* SMTP_AUTHを使用するかどうか */
		props.put("mail.smtp.auth", Boolean.toString(param.isAuth()));

		/* デバッグオプション */
		props.put("mail.debug", Boolean.toString(true));

		//セキュリティ認証
		if( MailSecurityType.NONE.equals( param.getMailSecurityType() ) ) {//セキュリティ認証なし
			;
		} else if( MailSecurityType.TLS.equals( param.getMailSecurityType() ) ) {//TLS使用
			props.setProperty( "mail.smtp.starttls.enable" , "true" ) ;
		} else if( MailSecurityType.SSL.equals( param.getMailSecurityType() ) ) {//SSL使用
			props.setProperty( "mail.smtp.ssl" , "true" ) ;
			props.setProperty( "mail.smtp.socketFactory.class" , "javax.net.ssl.SSLSocketFactory" ) ;
			props.setProperty( "mail.smtp.socketFactory.fallback" , "false" ) ;
			props.setProperty( "mail.smtp.socketFactory.port" , portNo ) ;
		}

		Authenticator authenticator = null;
		if( param.isAuth() ) {
			/* メールサーバ認証が必要な場合 */
			String userID = param.getAuthUserID();
			String password = param.getAuthPassword();
			authenticator = new MailAuthenticator(userID, password);
		}

		Session session = null;
		if( newSession ) {
			session = Session.getInstance(props, authenticator);
		} else {
			session = Session.getDefaultInstance(props, authenticator);
		}
		MimeMessage mimeMessage=new MimeMessage(session);

		/*****************************************/
		/* Mail送信元、送信先(TO, CC, BCC)の設定 */
		/*****************************************/
		/* 送信元の設定 */
		try {
			InternetAddress address = new InternetAddress(param.getSendFrom());
			String sendFromName = param.getSendFromName();
			if( sendFromName != null && sendFromName.length() > 0 )
				address.setPersonal(sendFromName, "iso-2022-jp");
			mimeMessage.setFrom(address);
		} catch (AddressException e) {
			throw new SendMailException("Can not to set send from address.", e);
		} catch (MessagingException e) {
			throw new SendMailException("Can not to set send from address.", e);
		} catch (UnsupportedEncodingException e) {
			throw new SendMailException("Can not to set send from Name.", e);
		}

		/* 送信先の設定 */
		String[] sendToArray = param.getSendTo();
		if( sendToArray == null )
			throw new SendMailException("Can not to send mail.send to address is not specified.", null);

		InternetAddress[] toAddress = new InternetAddress[sendToArray.length];
		try{
			for( int i = 0 ; i < sendToArray.length ; i++ ){
				toAddress[i] = new InternetAddress(sendToArray[i]);
			}

			mimeMessage.setRecipients(RecipientType.TO, toAddress);
		}catch( AddressException e ){
			throw new SendMailException("Can not to set send to address.", e);
		} catch (MessagingException e) {
			throw new SendMailException("Can not to set send to address.", e);
		}

		/* CCの設定 */
		String[] ccArray = param.getSendCC();
		if( ccArray != null && ccArray.length > 0 ) {
			InternetAddress[] ccAddress = new InternetAddress[ccArray.length];
			try{
				for( int i = 0 ; i < ccArray.length ; i++ ){
					ccAddress[i] = new InternetAddress(ccArray[i]);
				}

				mimeMessage.setRecipients(RecipientType.CC, ccAddress);
			}catch( AddressException e ){
				throw new SendMailException("Can not to set send CC address.", e);
			} catch (MessagingException e) {
				throw new SendMailException("Can not to set send CC address.", e);
			}
		}

		/* BCCの設定 */
		String[] bccArray = param.getSendBCC();
		if( bccArray != null && bccArray.length > 0 ) {
			InternetAddress[] bccAddress = new InternetAddress[bccArray.length];
			try{
				for( int i = 0 ; i < bccArray.length ; i++ ){
					bccAddress[i] = new InternetAddress(bccArray[i]);
				}

				mimeMessage.setRecipients(RecipientType.BCC, bccAddress);
			}catch( AddressException e ){
				throw new SendMailException("Can not to set send CC address.", e);
			} catch (MessagingException e) {
				throw new SendMailException("Can not to set send CC address.", e);
			}
		}

		/**************/
		/* Mailの題名 */
		/**************/
		try {
			mimeMessage.setSubject(param.getSubject(), "iso-2022-jp");
		} catch (MessagingException e) {
			throw new SendMailException("Can not to set Subject.", e);
		}

		/**********************************/
		/* Mailの本文、添付ファイルの設定 */
		/**********************************/
		try {
			/* マルチパートメッセージ作成 */
			MimeMultipart content = new MimeMultipart();
			/* テキスト部作成 */
			MimeBodyPart text = new MimeBodyPart();
			text.setText(param.getContents(), "iso-2022-jp");
			content.addBodyPart(text);

			if( param.isAttachmentFile() ) {
				/* 添付ファイルありの場合 */
				/* 添付ファイル部 */
				String[] attachmentFilePaths = param.getAttachmentFilePaths();
				if( attachmentFilePaths == null || attachmentFilePaths.length < 1 )
					throw new SendMailException("AttachmentFilePath is not specified.", null);
				for( int i= 0 ; i < attachmentFilePaths.length; i++ ) {
					MimeBodyPart attachmentFile = new MimeBodyPart();
					File file = new File(attachmentFilePaths[i]);
					attachmentFile.setDataHandler( new DataHandler(new FileDataSource(file)) );
					attachmentFile.setFileName(file.getName());
					content.addBodyPart(attachmentFile);
				}
			}

			/* メールにマルチパートメッセージをセット */
			mimeMessage.setContent(content);
		} catch (MessagingException e) {
			throw new SendMailException("Can not to set Mail Contents or AttachmentFile.", e);
		}

		/******************************/
		/* Mailの送信日時を設定 */
		/******************************/
		try {
			mimeMessage.setSentDate(new Date());
		} catch (MessagingException e) {
			throw new SendMailException("Can not to set send date.", e);
		}

		/************/
		/* Mail送信 */
		/************/
		send( mimeMessage ) ;
	}

	/**
	 *
	 * @param mimeMessage
	 */
	private static void send( MimeMessage mimeMessage ) throws SendMailException , SendFailedException {

		try {
			mimeMessage.saveChanges() ;
			Transport.send( mimeMessage ) ;

		} catch( SendFailedException e ) {
			throw e ;
		} catch( MessagingException e ) {
			throw new SendMailException("Can not to send Mail.", e);
		}
	}

	/**
	 * 送信先アドレスの配列から、無効なアドレスを取り除き、配列で返す。
	 * @param addressArray 送信先アドレスの配列
	 * @param invalidAddressSet 無効なアドレス文字列のSet
	 * @return 無効なアドレスを取り除いた、編集後の送信先アドレスの配列
	 */
	private static String[] removeInvalidAddressByAddressArray( String[] addressArray , Set<String> invalidAddressSet ) {

		Set<String> addressSet = new LinkedHashSet<String>() ;
		for( String address : addressArray ) {
			if( ! invalidAddressSet.contains( address ) ) {
				addressSet.add( address ) ;
			}
		}
		return addressSet.toArray( new String[ 0 ] ) ;

	}
}

