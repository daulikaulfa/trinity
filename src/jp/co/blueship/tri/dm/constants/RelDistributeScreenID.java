package jp.co.blueship.tri.dm.constants;

/**
 * リリース管理（資源配付）画面ID
 * <br>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class RelDistributeScreenID {

	/**
	 * リリース（資源配付）トップ画面
	 */
	public static final String TOP = "RelDistributeTop";
	/**
	 * リリース（資源配付） リリース環境・ロット選択画面
	 */
	public static final String ENV_SELECT = "RelDistributeEnvSelect";
	/**
	 * リリース（資源配付） リリース選択画面
	 */
	public static final String SELECT = "RelDistributeControlSelect";
	/**
	 * リリース（資源配付） 基本情報入力画面
	 */
	public static final String INFO_INPUT = "RelDistributeInfoInput";
	/**
	 * リリース（資源配付） 確認画面
	 */
	public static final String ENTRY_CONFIRM = "RelDistributeEntryConfirm";
	/**
	 * リリース（資源配付） 完了画面
	 */
	public static final String COMP_ENTRY = "RelDistributeCompEntry";
	/**
	 * リリース一覧画面
	 */
	public static final String LIST = "RelDistributeList";
	/**
	 * リリース編集画面
	 */
	public static final String MODIFY = "RelDistributeModify";
	/**
	 * リリース編集確認画面
	 */
	public static final String MODIFY_CONFIRM = "RelDistributeModifyConfirm";
	/**
	 * リリース編集完了画面
	 */
	public static final String COMP_MODIFY = "RelDistributeCompModify";
	/**
	 * タイマー設定画面
	 */
	public static final String TIMER_SET = "RelDistributeTimerSet";
	/**
	 * タイマー設定確認画面
	 */
	public static final String TIMER_CONFIRM = "RelDistributeTimerConfirm";
	/**
	 * タイマー設定完了画面
	 */
	public static final String COMP_TIMER = "RelDistributeCompTimer";
	/**
	 * 配布登録確認画面
	 */
	public static final String CANCEL_CONFIRM = "RelDistributeCancelConfirm";
	/**
	 * 配布登録完了画面
	 */
	public static final String COMP_CANCEL = "RelDistributeCompCancel";

}
