package jp.co.blueship.tri.dm.constants;

/**
 * ＤＲＭＳステータスＩＤの定義名の列挙型です。
 *
 */
public enum DrmsStatusId {

	/**
	 * 適用
	 */
	APPLY( "適用" ),
	/**
	 * 適用エラー
	 */
	APPLY_ERROR( "適用エラー" ) ;

	String value ;

	private DrmsStatusId( String value ) {
		this.value = value ;
	}

	/**
	 * 該当するＤＲＭＳステータスの定義名を検索し、そのラベル値を取得する
	 * @param name 定義名
	 * @return ラベル
	 */
	public static DrmsStatusId getValue( String value ) {
		for ( DrmsStatusId define : values() ) {
			if ( define.toString().equals(value) )
				return valueOf( value );
		}

		return null;
	}
	/**
	 * ラベル値を取得する
	 * @return ラベル値
	 */
	public String getValue() {
		return value ;
	}
}
