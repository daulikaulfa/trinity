package jp.co.blueship.tri.dm.constants;

/**
 * リリース管理画面ID
 * <br>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class RelDistributeFtpSrcPathDefineID {

	/**
	 * 対象環境No
	 */
	public static final String REL_ENV_NO = "{@relEnvNo}";

	/**
	 * 対象環境名
	 */
	public static final String REL_ENV_NAME = "{@relEnvName}";

	/**
	 * ロットNo
	 */
	public static final String LOT_NO = "{@lotNo}";

	/**
	 * ロット名
	 */
	public static final String LOT_NAME = "{@lotName}";

	/**
	 * ＤＲＭＳ管理バージョン
	 */
	public static final String RC_NO = "{@relNo}";

	/**
	 * 拡張子
	 */
	public static final String ZIP = "{@zip}";
	/**
	 * 拡張子
	 */
	public static final String TAR = "{@tar}";
	/**
	 * 拡張子
	 */
	public static final String EXT = "{@ext}";


}
