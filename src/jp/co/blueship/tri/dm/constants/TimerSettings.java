package jp.co.blueship.tri.dm.constants;


/**
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public enum TimerSettings {
	/**
	 * To be set later.
	 */
	none( "", "" ),
	/**
	 * Immediately.
	 */
	Immediately( "immediately", "now" ),
	/**
	 * Timer Settins.
	 */
	TimerSettins( "timerSettins", "set" ),
	/**
	 * Cancel the timer.
	 */
	Cancel( "cancel", "cancel" ),
	;

	private String value = null ;
	private String legacy = null ;

	private TimerSettings( String value, String legacy ) {
		this.value = value;
		this.legacy = legacy;
	}

	public String value() {
		return this.value;
	}

	/**
	 * V3 Compatibility
	 *
	 * @return
	 */
	public String legacy() {
		return this.legacy;
	}

	public static TimerSettings value( String value ) {
		for ( TimerSettings type : values() ) {
			if ( type.value().equals( value ) ) {
				return type;
			}
		}

		return none;
	}
}
