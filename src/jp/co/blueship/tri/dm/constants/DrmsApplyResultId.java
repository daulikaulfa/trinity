package jp.co.blueship.tri.dm.constants;

/**
 * ＤＲＭＳ適用結果の定義名の列挙型です。
 *
 */
public enum DrmsApplyResultId {

	/**
	 * 適用
	 */
	SUCCESS( "成功" ),
	/**
	 * 適用エラー
	 */
	FAIL( "失敗" ) ;

	String value ;

	private DrmsApplyResultId( String value ) {
		this.value = value ;
	}

	/**
	 * 該当するＤＲＭＳ適用結果の定義名を検索し、そのラベル値を取得する
	 * @param name 定義名
	 * @return ラベル
	 */
	public static DrmsApplyResultId getValue( String value ) {
		for ( DrmsApplyResultId define : values() ) {
			if ( define.toString().equals(value) )
				return valueOf( value );
		}

		return null;
	}
	/**
	 * ラベル値を取得する
	 * @return ラベル値
	 */
	public String getValue() {
		return value ;
	}
}
