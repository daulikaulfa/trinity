package jp.co.blueship.tri.dm.constants;

/**
 * リリース管理画面ID
 * <br>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class RelDistributeCommercialShellPropertyDefineID {

	/**
	 * 対象環境
	 */
	public static final String GROUP_NAME = "GROUP_NAME";

	/**
	 * ＤＲＭＳ管理バージョン
	 */
	public static final String RC_NO = "RC_NO";

	/**
	 * 資源転送先パス
	 */
	public static final String DIR_PATH = "DIR_PATH";

	/**
	 * 配布日時
	 */
	public static final String TIMER = "TIMER";

	/**
	 * 新規／変更フラグ
	 */
	public static final String SET_FLG = "SET_FLG";


}
