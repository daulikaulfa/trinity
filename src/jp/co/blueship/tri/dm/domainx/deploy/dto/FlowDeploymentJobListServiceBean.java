package jp.co.blueship.tri.dm.domainx.deploy.dto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.annotations.Expose;

import jp.co.blueship.tri.dm.constants.TimerSettings;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.svc.beans.dto.IPageNoInfo;
import jp.co.blueship.tri.fw.svc.beans.dto.ISearchFilter;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;
import jp.co.blueship.tri.fw.svc.beans.dto.PageNoInfo;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 * @version V4.01.00
 * @author Akahoshi
 *
 */
public class FlowDeploymentJobListServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();

	private List<DeploymentJobListView> jobViews = new ArrayList<DeploymentJobListView>();
	private IPageNoInfo page = new PageNoInfo();

	/**
	 * this is cache data for deployment information.<br>
	 * <br>
	 * key = management version<br>
	 * value = deployment information({@link DeploeymntInfoView}).
	 */
	private Map<String, DeploymentInfoView> cacheDeploymentInfo = new HashMap<String, DeploymentInfoView>();

	public RequestParam getParam() {
		return param;
	}

	public List<DeploymentJobListView> getJobViews() {
		return jobViews;
	}
	public FlowDeploymentJobListServiceBean setJobViews(
			List<DeploymentJobListView> jobViews) {
		this.jobViews = jobViews;
		return this;
	}

	public IPageNoInfo getPage() {
		return page;
	}
	public FlowDeploymentJobListServiceBean setPage(IPageNoInfo page) {
		this.page = page;
		return this;
	}

	public Map<String, DeploymentInfoView> getCacheDeploymentInfo() {
		return cacheDeploymentInfo;
	}
	public FlowDeploymentJobListServiceBean setCacheDeploymentInfo( Map<String, DeploymentInfoView> cacheDeploymentInfo) {
		this.cacheDeploymentInfo = cacheDeploymentInfo;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String lotId;
		private DeploymentJobLisInputBean inputInfo = new DeploymentJobLisInputBean();
		private SearchCondition searchCondition = new SearchCondition();
		private int linesPerPage = 20;
		private RequestOption requestOption = RequestOption.none;

		public String getSelectedLotId() {
			return lotId;
		}
		public RequestParam setSelectedLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}

		public DeploymentJobLisInputBean getInputInfo() {
			return inputInfo;
		}
		public RequestParam setInputInfo(DeploymentJobLisInputBean inputInfo) {
			this.inputInfo = inputInfo;
			return this;
		}

		public SearchCondition getSearchCondition() {
			return searchCondition;
		}
		public RequestParam setSearchCondition(SearchCondition searchCondition) {
			this.searchCondition = searchCondition;
			return this;
		}

		public int getLinesPerPage() {
			return linesPerPage;
		}
		public RequestParam setLinesPerPage(int linesPerPage) {
			this.linesPerPage = linesPerPage;
			return this;
		}

		public RequestOption getRequestOption() {
			return requestOption;
		}
		public RequestParam setRequestOption(RequestOption requestOption) {
			this.requestOption = requestOption;
			return this;
		}
	}

	public enum RequestOption {
		none( "" ),
		search("search"),
		getDeploymentInfo("getDeploymentInfo"),
		;

		private String value = null;

		private RequestOption( String value) {
			this.value = value;
		}

		public boolean equals( String value ) {
			RequestOption type = value( value );

			if ( null == type ) return false;
			if ( ! this.equals(type) ) return false;

			return true;
		}

		public String value() {
			return this.value;
		}

		public static RequestOption value( String value ) {
			if ( TriStringUtils.isEmpty( value ) ) {
				return null;
			}

			for ( RequestOption type : values() ) {
				if ( type.value().equals( value ) ) {
					return type;
				}
			}

			return none;
		}
	}


	public class DeploymentJobLisInputBean {
		private boolean manual = true;

		public boolean isSelectedManual(){
			return manual;
		}
		public DeploymentJobLisInputBean setManual(boolean manual){
			this.manual = manual;
			return this;
		}
	}

	/**
	 * Search Condition
	 */
	public class SearchCondition implements ISearchFilter{
		@Expose private String envId = null;
		@Expose private String stsId = null;
		@Expose private String ctgId = null;
		@Expose private String mstoneId = null;
		@Expose private String keyword = null;
		@Expose private Integer selectedPageNo = 1;

		private List<ItemLabelsBean> releaseEnvViews = new ArrayList<ItemLabelsBean>();
		private List<ItemLabelsBean> statusViews = new ArrayList<ItemLabelsBean>();
		private List<ItemLabelsBean> ctgViews = new ArrayList<ItemLabelsBean>();
		private List<ItemLabelsBean> mstoneViews = new ArrayList<ItemLabelsBean>();

		public String getEnvId() {
			return envId;
		}
		public SearchCondition setEnvId(String envId) {
			this.envId = envId;
			return this;
		}

		public String getStsId() {
			return stsId;
		}
		public SearchCondition setStsId(String stsId) {
			this.stsId = stsId;
			return this;
		}

		public String getCtgId() {
			return ctgId;
		}
		public SearchCondition setCtgId(String ctgId) {
			this.ctgId = ctgId;
			return this;
		}

		public String getMstoneId() {
			return mstoneId;
		}
		public SearchCondition setMstoneId(String mstoneId) {
			this.mstoneId = mstoneId;
			return this;
		}

		public String getKeyword() {
			return keyword;
		}
		public SearchCondition setKeyword(String keyword) {
			this.keyword = keyword;
			return this;
		}

		public Integer getSelectedPageNo() {
			return selectedPageNo;
		}
		public SearchCondition setSelectedPageNo(Integer selectedPageNo) {
			this.selectedPageNo = selectedPageNo;
			return this;
		}

		public List<ItemLabelsBean> getReleaseEnvViews() {
			return releaseEnvViews;
		}
		public SearchCondition setReleaseEnvViews(List<ItemLabelsBean> releaseEnvViews) {
			this.releaseEnvViews = releaseEnvViews;
			return this;
		}

		public List<ItemLabelsBean> getStatusViews() {
			return statusViews;
		}
		public SearchCondition setStatusViews(List<ItemLabelsBean> statusViews) {
			this.statusViews = statusViews;
			return this;
		}

		public List<ItemLabelsBean> getCtgViews() {
			return ctgViews;
		}
		public SearchCondition setCtgViews(List<ItemLabelsBean> ctgViews) {
			this.ctgViews = ctgViews;
			return this;
		}

		public List<ItemLabelsBean> getMstoneViews() {
			return mstoneViews;
		}
		public SearchCondition setMstoneViews(List<ItemLabelsBean> mstoneViews) {
			this.mstoneViews = mstoneViews;
			return this;
		}

	}

	public class DeploymentJobListView {
		private String deploymentId;
		private String mgtVer;
		private String rpId;
		private String envId;
		private String environmentNm;
		private String stsId;
		private String status;
		private String createdBy;
		private String createdByIconPath;
		private TimerSettings timerSettings = TimerSettings.none;
		private String timerSettingsTime;
		private String deploymentRegistedTime;
		private String deploymentAppliedTime;
		private String deploymentStsId;
		private String deploymentStatus;
		private boolean isJobCancelled = false;

		public String getDeploymentId() {
			return deploymentId;
		}
		public DeploymentJobListView setDeploymentId(String deploymentId) {
			this.deploymentId = deploymentId;
			return this;
		}
		public String getMgtVer() {
			return mgtVer;
		}
		public DeploymentJobListView setMgtVer(String mgtVer) {
			this.mgtVer = mgtVer;
			return this;
		}
		public String getRpId() {
			return rpId;
		}
		public DeploymentJobListView setRpId(String rpId) {
			this.rpId = rpId;
			return this;
		}
		public String getEnvId() {
			return envId;
		}
		public DeploymentJobListView setEnvId(String envId) {
			this.envId = envId;
			return this;
		}
		public String getEnvironmentNm() {
			return environmentNm;
		}
		public DeploymentJobListView setEnvironmentNm(String environmentNm) {
			this.environmentNm = environmentNm;
			return this;
		}
		public String getStsId() {
			return stsId;
		}
		public DeploymentJobListView setStsId(String stsId) {
			this.stsId = stsId;
			return this;
		}
		public String getStatus() {
			return status;
		}
		public DeploymentJobListView setStatus(String status) {
			this.status = status;
			return this;
		}
		public String getCreatedBy() {
			return createdBy;
		}
		public DeploymentJobListView setCreatedBy(String createdBy) {
			this.createdBy = createdBy;
			return this;
		}
		public String getCreatedByIconPath() {
			return createdByIconPath;
		}
		public DeploymentJobListView setCreatedByIconPath(String createdByIconPath) {
			this.createdByIconPath = createdByIconPath;
			return this;
		}
		public TimerSettings getTimerSettings() {
			return timerSettings;
		}
		public DeploymentJobListView setTimerSettings(TimerSettings timerSettings) {
			this.timerSettings = timerSettings;
			return this;
		}
		public String getTimerSettingsTime() {
			return timerSettingsTime;
		}
		public DeploymentJobListView setTimerSettingsTime(String timerSettingsTime) {
			this.timerSettingsTime = timerSettingsTime;
			return this;
		}

		public String getDeploymentRegistedTime() {
			return deploymentRegistedTime;
		}
		public DeploymentJobListView setDeploymentRegistedTime(String deploymentRegistedTime) {
			this.deploymentRegistedTime = deploymentRegistedTime;
			return this;
		}
		public String getDeploymentAppliedTime() {
			return deploymentAppliedTime;
		}
		public DeploymentJobListView setDeploymentAppliedTime(String deploymentAppliedTime) {
			this.deploymentAppliedTime = deploymentAppliedTime;
			return this;
		}
		public String getDeploymentStsId() {
			return deploymentStsId;
		}
		public DeploymentJobListView setDeploymentStsId(String deploymentStsId) {
			this.deploymentStsId = deploymentStsId;
			return this;
		}
		public String getDeploymentStatus() {
			return deploymentStatus;
		}
		public DeploymentJobListView setDeploymentStatus(String deploymentStatus) {
			this.deploymentStatus = deploymentStatus;
			return this;
		}
		public boolean isJobCancelled(){
			return isJobCancelled;
		}
		public DeploymentJobListView setJobCancelled( boolean isJobCancelled ){
			this.isJobCancelled = isJobCancelled;
			return this;
		}
	}

	public class DeploymentInfoView {
		private String mgtVer;
		private String deploymentRegistedTime;
		private String deploymentAppliedTime;
		private String deploymentStsId;
		private String deploymentStatus;

		public String getMgtVer() {
			return mgtVer;
		}
		public DeploymentInfoView setMgtVer(String mgtVer) {
			this.mgtVer = mgtVer;
			return this;
		}

		public String getDeploymentRegistedTime() {
			return deploymentRegistedTime;
		}
		public DeploymentInfoView setDeploymentRegistedTime(String deploymentRegistedTime) {
			this.deploymentRegistedTime = deploymentRegistedTime;
			return this;
		}

		public String getDeploymentAppliedTime() {
			return deploymentAppliedTime;
		}
		public DeploymentInfoView setDeploymentAppliedTime(String deploymentAppliedTime) {
			this.deploymentAppliedTime = deploymentAppliedTime;
			return this;
		}

		public String getDeploymentStsId() {
			return deploymentStsId;
		}
		public DeploymentInfoView setDeploymentStsId(String deploymentStsId) {
			this.deploymentStsId = deploymentStsId;
			return this;
		}

		public String getDeploymentStatus() {
			return deploymentStatus;
		}
		public DeploymentInfoView setDeploymentStatus(String deploymentStatus) {
			this.deploymentStatus = deploymentStatus;
			return this;
		}
	}

}
