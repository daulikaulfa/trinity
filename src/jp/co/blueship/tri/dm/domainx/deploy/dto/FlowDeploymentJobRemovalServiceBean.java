package jp.co.blueship.tri.dm.domainx.deploy.dto;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 * @version V4.01.00
 * @author Akahoshi
 *
 */
public class FlowDeploymentJobRemovalServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private DeploymentJobRemovalDetailsView detailsView = new DeploymentJobRemovalDetailsView();

	private RequestsCompletion result = new RequestsCompletion();

	public RequestParam getParam() {
		return param;
	}

	public DeploymentJobRemovalDetailsView getDetailsView() {
		return detailsView;
	}
	public FlowDeploymentJobRemovalServiceBean setDetailsView( DeploymentJobRemovalDetailsView detailsView ) {
		this.detailsView = detailsView;
		return this;
	}

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowDeploymentJobRemovalServiceBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String mgtVer;
		private DeploymentJobRemovalInputInfo inputInfo = new DeploymentJobRemovalInputInfo();
		private SubmitOption submitOption = SubmitOption.none;

		public String getSelectedMgtVer() {
			return mgtVer;
		}
		public RequestParam setSelectedMgtVer(String mgtVer) {
			this.mgtVer = mgtVer;
			return this;
		}

		public DeploymentJobRemovalInputInfo getInputInfo(){
			return inputInfo;
		}
		public RequestParam setInputInfo(DeploymentJobRemovalInputInfo inputInfo){
			this.inputInfo = inputInfo;
			return this;
		}

		public SubmitOption getSubmitOption() {
			return submitOption;
		}
		public RequestParam setSubmitOption(SubmitOption submitOption) {
			this.submitOption = submitOption;
			return this;
		}
	}

	public enum SubmitOption {
		none,
		cancelTimer,
		cancelJob,
		removeJob,
		;

		public boolean equals( String value ) {
			SubmitOption type = value( value );

			if ( null == type ) return false;
			if ( ! this.equals(type) ) return false;

			return true;
		}
		public static SubmitOption value( String value ) {
			if ( TriStringUtils.isEmpty( value ) ) {
				return null;
			}

			for ( SubmitOption type : values() ) {
				if ( type.equals( value ) ) {
					return type;
				}
			}

			return none;
		}
	}

	/**
	 * Information
	 */
	public class DeploymentJobRemovalInputInfo {
		private String comment;
		private boolean manual = true;

		public String getComment(){
			return comment;
		}
		public DeploymentJobRemovalInputInfo setComment(String comment){

			this.comment = comment;
			return this;
		}

		public boolean isSelectedManual() {
			return manual;
		}
		public DeploymentJobRemovalInputInfo setManual(boolean manual) {
			this.manual = manual;
			return this;
		}
	}

	public class DeploymentJobRemovalDetailsView {

		private boolean isJobCancelled = false;

		public boolean isJobCancelled(){
			return isJobCancelled;
		}
		public DeploymentJobRemovalDetailsView setJobCancelled( boolean isJobCancelled ){
			this.isJobCancelled = isJobCancelled;
			return this;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private String deploymentId;
		private boolean completed = false;

		public String getDeploymentId() {
			return deploymentId;
		}
		public RequestsCompletion setDeploymentId(String deploymentId) {
			this.deploymentId = deploymentId;
			return this;
		}

		public boolean isCompleted() {
			return completed;
		}
		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}
}
