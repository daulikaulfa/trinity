package jp.co.blueship.tri.dm.domainx.deploy.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.dm.constants.TimerSettings;
import jp.co.blueship.tri.dm.domain.beans.dop.dto.ReleasePackageViewBean;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 * @version V4.01.00
 * @author Akahoshi
 *
 */
public class FlowDeploymentJobCreationServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private RequestsCompletion result = new RequestsCompletion();

	public RequestParam getParam() {
		return param;
	}

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowDeploymentJobCreationServiceBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String lotId = null;
		private DeploymentJobEditInputBean inputInfo = new DeploymentJobEditInputBean();
		private RequestOption requestOption = RequestOption.none;


		public String getSelectedLotId() {
			return lotId;
		}
		public RequestParam setSelectedLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}

		public DeploymentJobEditInputBean getInputInfo() {
			return inputInfo;
		}
		public RequestParam setInputInfo(DeploymentJobEditInputBean inputInfo) {
			this.inputInfo = inputInfo;
			return this;
		}

		public RequestOption getRequestOption() {
			return requestOption;
		}
		public RequestParam setRequestOption(RequestOption requestOption) {
			this.requestOption = requestOption;
			return this;
		}
	}



	/**
	 * This is the class of enumeration types for 'onChange request'.
	 */
	public enum RequestOption {
		none( "" ),
		selectEnvironment("selectEnvironment"),
		refreshCategory("refreshCategory"),
		refreshMilestone("refreshMilestone")
		;

		private String value = null;

		private RequestOption( String value) {
			this.value = value;
		}

		public boolean equals( String value ) {
			RequestOption type = value( value );

			if ( null == type ) return false;
			if ( ! this.equals(type) ) return false;

			return true;
		}

		public String value() {
			return this.value;
		}

		public static RequestOption value( String value ) {
			if ( TriStringUtils.isEmpty( value ) ) {
				return null;
			}

			for ( RequestOption type : values() ) {
				if ( type.value().equals( value ) ) {
					return type;
				}
			}

			return none;
		}
	}

	/**
	 * Deployment Job Information
	 */
	public class DeploymentJobEditInputBean {
		private String summary;
		private String contents;
		private String relEnvId;
		private String ctgId;
		private String mstoneId;
		private String rpId;
		private TimerSettings timerSettings = TimerSettings.none;
		private String timerDate;
		private String timerHour;
		private String timerMinute;
		private String timerSummary;
		private String timerContents;
		private boolean isManual = true;

		private List<ItemLabelsBean> releaseEnvViews = new ArrayList<ItemLabelsBean>();
		private List<ItemLabelsBean> ctgViews = new ArrayList<ItemLabelsBean>();
		private List<ItemLabelsBean> mstoneViews = new ArrayList<ItemLabelsBean>();
		private List<ReleasePackageViewBean> releasePackageViews = new ArrayList<ReleasePackageViewBean>();

		public String getSummary() {
			return summary;
		}
		public DeploymentJobEditInputBean setSummary(String summary) {
			this.summary = summary;
			return this;
		}

		public String getContents() {
			return contents;
		}
		public DeploymentJobEditInputBean setContents(String contents) {
			this.contents = contents;
			return this;
		}

		public String getRelEnvId() {
			return relEnvId;
		}
		public DeploymentJobEditInputBean setRelEnvId(String relEnvId) {
			this.relEnvId = relEnvId;
			return this;
		}

		public String getCtgId() {
			return ctgId;
		}
		public DeploymentJobEditInputBean setCtgId(String ctgId) {
			this.ctgId = ctgId;
			return this;
		}

		public String getMstoneId() {
			return mstoneId;
		}
		public DeploymentJobEditInputBean setMstoneId(String mstoneId) {
			this.mstoneId = mstoneId;
			return this;
		}

		public String getRpId() {
			return rpId;
		}
		public DeploymentJobEditInputBean setRpId(String rpId) {
			this.rpId = rpId;
			return this;
		}

		public TimerSettings getTimerSettings() {
			return timerSettings;
		}
		public DeploymentJobEditInputBean setTimerSettings(TimerSettings timerSettings) {
			this.timerSettings = timerSettings;
			return this;
		}

		public String getTimerDate() {
			return timerDate;
		}
		public DeploymentJobEditInputBean setTimerDate(String timerDate) {
			this.timerDate = timerDate;
			return this;
		}

		public String getTimerHour() {
			return timerHour;
		}
		public DeploymentJobEditInputBean setTimerHour(String timerHour) {
			this.timerHour = timerHour;
			return this;
		}

		public String getTimerMinute() {
			return timerMinute;
		}
		public DeploymentJobEditInputBean setTimerMinute(String timerMinute) {
			this.timerMinute = timerMinute;
			return this;
		}

		public String getTimerSummary() {
			return timerSummary;
		}
		public DeploymentJobEditInputBean setTimerSummary(String timerSummary) {
			this.timerSummary = timerSummary;
			return this;
		}

		public String getTimerContents() {
			return timerContents;
		}
		public DeploymentJobEditInputBean setTimerContents(String timerContents) {
			this.timerContents = timerContents;
			return this;
		}

		public List<ItemLabelsBean> getReleaseEnvViews() {
			return releaseEnvViews;
		}
		public DeploymentJobEditInputBean setReleaseEnvViews(List<ItemLabelsBean> releaseEnvViews) {
			this.releaseEnvViews = releaseEnvViews;
			return this;
		}

		public List<ItemLabelsBean> getCtgViews() {
			return ctgViews;
		}
		public DeploymentJobEditInputBean setCtgViews(List<ItemLabelsBean> ctgViews) {
			this.ctgViews = ctgViews;
			return this;
		}

		public List<ItemLabelsBean> getMstoneViews() {
			return mstoneViews;
		}
		public DeploymentJobEditInputBean setMstoneViews(List<ItemLabelsBean> mstoneViews) {
			this.mstoneViews = mstoneViews;
			return this;
		}

		public List<ReleasePackageViewBean> getReleasePackageViews() {
			return releasePackageViews;
		}
		public DeploymentJobEditInputBean setReleasePackageViews(List<ReleasePackageViewBean> releasePackageViews) {
			this.releasePackageViews = releasePackageViews;
			return this;
		}
		public boolean isManual() {
			return isManual;
		}
		public DeploymentJobEditInputBean setManual(boolean isManual) {
			this.isManual = isManual;
			return this;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private String deploymentId;
		private boolean completed = false;

		public String getDeploymentId() {
			return deploymentId;
		}
		public RequestsCompletion setDeploymentId(String deploymentId) {
			this.deploymentId = deploymentId;
			return this;
		}
		public boolean isCompleted() {
			return completed;
		}
		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}
}
