package jp.co.blueship.tri.dm.domainx.deploy;

import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvEntity;
import jp.co.blueship.tri.dm.DmBusinessJudgUtils;
import jp.co.blueship.tri.dm.constants.RelDistributeScreenID;
import jp.co.blueship.tri.dm.constants.TimerSettings;
import jp.co.blueship.tri.dm.dao.dmdo.eb.IDmDoDto;
import jp.co.blueship.tri.dm.dao.dmdo.eb.IDmDoEntity;
import jp.co.blueship.tri.dm.domain.dop.dto.FlowRelDistributeCancelServiceBean;
import jp.co.blueship.tri.dm.domain.dop.dto.FlowRelDistributeTimerServiceBean;
import jp.co.blueship.tri.dm.domainx.deploy.dto.FlowDeploymentJobRemovalServiceBean;
import jp.co.blueship.tri.dm.domainx.deploy.dto.FlowDeploymentJobRemovalServiceBean.SubmitOption;
import jp.co.blueship.tri.dm.support.FlowRelDistributeEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.DesignSheetUtils;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.status.DmDoStatusId;
import jp.co.blueship.tri.fw.constants.status.UmActStatusId;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.DmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.fw.um.dao.hist.eb.HistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;

/**
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 * @version V4.01.00
 * @author Cuong Nguyen
 */
public class FlowDeploymentJobRemovalService implements IDomain<FlowDeploymentJobRemovalServiceBean>  {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowRelDistributeEditSupport support = null;

	private IDomain<IGeneralServiceBean> cancelConfirmService = null;
	private IDomain<IGeneralServiceBean> compCancelService = null;

	private IDomain<IGeneralServiceBean> setTimerConfirmService = null;
	private IDomain<IGeneralServiceBean> compTimerSettingService = null;

	private ActionStatusMatrixList statusMatrixAction;


	public void setSupport(FlowRelDistributeEditSupport support) {
		this.support = support;
	}

	public void setCancelConfirmService(IDomain<IGeneralServiceBean> cancelConfirmService) {
		this.cancelConfirmService = cancelConfirmService;
	}
	public void setCompCancelService(IDomain<IGeneralServiceBean> compCancelService) {
		this.compCancelService = compCancelService;
	}

	public void setSetTimerConfirmService(IDomain<IGeneralServiceBean> setTimerConfirmService) {
		this.setTimerConfirmService = setTimerConfirmService;
	}
	public void setCompTimerSettingService(IDomain<IGeneralServiceBean> compTimerSettingService) {
		this.compTimerSettingService = compTimerSettingService;
	}

	public void setStatusMatrixAction( ActionStatusMatrixList action ) {
		this.statusMatrixAction = action;
	}

	@Override
	public IServiceDto<FlowDeploymentJobRemovalServiceBean> execute(
			IServiceDto<FlowDeploymentJobRemovalServiceBean> serviceDto) {

		FlowDeploymentJobRemovalServiceBean paramBean = serviceDto.getServiceBean();
		try {

			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");
			String mgtVer = paramBean.getParam().getSelectedMgtVer();
			PreConditions.assertOf(TriStringUtils.isNotEmpty(mgtVer), "SelectedMgtVer is not specified");

			if( RequestType.init.equals( paramBean.getParam().getRequestType() )  ){
				this.init( paramBean );
			}

			if( RequestType.validate.equals( paramBean.getParam().getRequestType() ) ){
				this.validation( paramBean );
			}

			if ( RequestType.submitChanges.equals(paramBean.getParam().getRequestType()) ) {
				this.submitChanges( paramBean );
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( DmMessageId.DM005025S, e, paramBean.getFlowAction() );
		}
	}


	private void init( FlowDeploymentJobRemovalServiceBean paramBean ) {
		String mgtVer = paramBean.getParam().getSelectedMgtVer();
		IDmDoEntity entity = this.support.findDmDoEntity( mgtVer );

		boolean isJobCancelled = DmDoStatusId.JobCancelled.equals( entity.getStsId())? true : false;

		paramBean.getDetailsView().setJobCancelled( isJobCancelled );
		paramBean.getResult().setDeploymentId( entity.getDeploymentId() );
	}


	private void validation( FlowDeploymentJobRemovalServiceBean paramBean ) {
		String mgtVer = paramBean.getParam().getSelectedMgtVer();
		IDmDoEntity entity = this.support.findDmDoEntity( mgtVer );

		StatusCheckDto statusDto = new StatusCheckDto()
				.setServiceBean( paramBean )
				.setFinder( support )
				.setActionList( statusMatrixAction )
				.setLotIds( entity.getLotId() )
				.setRpIds( entity.getRpId() )
				;
		StatusMatrixCheckUtils.checkStatusMatrix( statusDto );

		if( SubmitOption.cancelTimer.equals( paramBean.getParam().getSubmitOption() ) &&
				DmDoStatusId.TimerConfigured.equals( entity.getStsId() ) ){

			if( DmBusinessJudgUtils.isPastSetTime(entity) ){
				throw new ContinuableBusinessException( DmMessageId.DM001019E );
			}
		}
	}


	private void submitChanges( FlowDeploymentJobRemovalServiceBean paramBean ) {

		String mgtVer = paramBean.getParam().getSelectedMgtVer();
		IDmDoEntity entity = this.support.findDmDoEntity( mgtVer );

		{
			StatusCheckDto statusDto = new StatusCheckDto()
					.setServiceBean( paramBean )
					.setFinder( support )
					.setActionList( statusMatrixAction )
					.setLotIds( entity.getLotId() )
					.setRpIds( entity.getRpId() )
					;
			StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
		}


		if( SubmitOption.cancelTimer.equals( paramBean.getParam().getSubmitOption() ) &&
				DmDoStatusId.TimerConfigured.equals( entity.getStsId() ) ){

			if( DmBusinessJudgUtils.isPastSetTime(entity) ){
				throw new ContinuableBusinessException( DmMessageId.DM001019E );
			}

			this.cancelTimer( paramBean );
		}

		if( SubmitOption.cancelJob.equals( paramBean.getParam().getSubmitOption() ) ){

			this.cancelJob( paramBean );
		}

		if( SubmitOption.removeJob.equals( paramBean.getParam().getSubmitOption() ) ){
			this.removeJob( paramBean );
		}
	}


	private void cancelTimer( FlowDeploymentJobRemovalServiceBean paramBean ) {

		paramBean.setInnerService(new FlowRelDistributeTimerServiceBean());

		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowRelDistributeTimerServiceBean serviceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(serviceBean, paramBean);
		dto.setServiceBean(serviceBean);

		this.cancelTimerBeforeExecution( paramBean , serviceBean );

		serviceBean.setReferer(RelDistributeScreenID.TIMER_SET);
		serviceBean.setForward(RelDistributeScreenID.TIMER_CONFIRM);
		setTimerConfirmService.execute(dto);

		serviceBean.setReferer(RelDistributeScreenID.TIMER_CONFIRM);
		serviceBean.setForward(RelDistributeScreenID.COMP_TIMER);
		setTimerConfirmService.execute(dto);
		compTimerSettingService.execute(dto);

		paramBean.getResult().setCompleted(true);
	}


	private void cancelTimerBeforeExecution( FlowDeploymentJobRemovalServiceBean src,
												FlowRelDistributeTimerServiceBean dest) {

		src.getResult().setCompleted(false);

		String mgtVer = src.getParam().getSelectedMgtVer();

		IDmDoEntity dmDoEntity = this.support.findDmDoEntity( mgtVer );

		String rpId = dmDoEntity.getRpId();
		IRpEntity rpEntity = this.support.findRpEntity(rpId);

		IBldEnvEntity bldEnvEntity = this.support.getBmFinderSupport().findBldEnvEntity(rpEntity.getBldEnvId());

		dest.setDistNo			( rpId );
		dest.setSelectedDistNo	( rpId ) ;
		dest.setRmi				( !src.getParam().getInputInfo().isSelectedManual() ) ;
		dest.setTimerSetting	( TimerSettings.Cancel.legacy() );
		dest.getConfViewBean().setConfName(bldEnvEntity.getBldEnvNm());
	}


	private void cancelJob( FlowDeploymentJobRemovalServiceBean paramBean ) {

		String mgtVer = paramBean.getParam().getSelectedMgtVer();
		IDmDoEntity dmDoEntity = this.support.findDmDoEntity( mgtVer );

		paramBean.setInnerService(new FlowRelDistributeCancelServiceBean());

		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowRelDistributeCancelServiceBean serviceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(serviceBean, paramBean);
		dto.setServiceBean(serviceBean);

		this.cancelBeforeExecution( paramBean , serviceBean, dmDoEntity );

		serviceBean.setReferer(RelDistributeScreenID.CANCEL_CONFIRM);
		serviceBean.setForward(RelDistributeScreenID.COMP_CANCEL);
		cancelConfirmService.execute(dto);
		compCancelService.execute(dto);

		paramBean.getResult().setCompleted(true);
		paramBean.getMessageInfo().addFlashTranslatable(DmMessageId.DM003003I);
	}


	private void cancelBeforeExecution( FlowDeploymentJobRemovalServiceBean src,
											FlowRelDistributeCancelServiceBean dest,
											IDmDoEntity dmDoEntity){

		String rpId = dmDoEntity.getRpId();

		IRpEntity rpEntity = this.support.findRpEntity(rpId);
		IBldEnvEntity bldEnvEntity = this.support.getBmFinderSupport().findBldEnvEntity(rpEntity.getBldEnvId());

		dest.setDistNo		  ( rpId );
		dest.setRmi			  ( !src.getParam().getInputInfo().isSelectedManual() );
		dest.setCancelComment ( src.getParam().getInputInfo().getComment() );
		dest.getConfViewBean().setConfName( bldEnvEntity.getBldEnvNm() );
	}


	private void removeJob( FlowDeploymentJobRemovalServiceBean paramBean ) {

		paramBean.getResult().setCompleted(false);

		String mgtVer = paramBean.getParam().getSelectedMgtVer();
		IDmDoEntity dmDoEntity = this.support.findDmDoEntity( mgtVer );

		if (DmDoStatusId.JobCancelled.getStatusId().equals( dmDoEntity.getStsId() )) {
			dmDoEntity.setDelStsId( StatusFlg.on );
			dmDoEntity.setDelUserId( paramBean.getUserId() );
			dmDoEntity.setDelUserNm( paramBean.getUserName() );
		}
		this.support.getDmDoDao().update(dmDoEntity);

		paramBean.getResult().setCompleted( true );
		paramBean.getMessageInfo().addFlashTranslatable( DmMessageId.DM003002I );

		if ( DesignSheetUtils.isRecord() ){
			IDmDoDto dmDoDto = support.getDmDoDto(dmDoEntity.getRpId());
			dmDoDto.setDmDoEntity(dmDoEntity);
			IHistEntity histEntity = new HistEntity();
			histEntity.setActSts( UmActStatusId.none.getStatusId() );

			support.getDmDoHistDao().insert( histEntity , dmDoDto);
		}
	}
}
