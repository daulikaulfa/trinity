package jp.co.blueship.tri.dm.domainx.deploy;

import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvEntity;
import jp.co.blueship.tri.dm.DmBusinessJudgUtils;
import jp.co.blueship.tri.dm.constants.RelDistributeScreenID;
import jp.co.blueship.tri.dm.constants.TimerSettings;
import jp.co.blueship.tri.dm.dao.dmdo.eb.IDmDoEntity;
import jp.co.blueship.tri.dm.domain.dop.dto.FlowRelDistributeTimerServiceBean;
import jp.co.blueship.tri.dm.domainx.deploy.dto.FlowDeploymentJobCancellationServiceBean;
import jp.co.blueship.tri.dm.support.FlowRelDistributeEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.DmMessageId;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;


/**
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 * @version V4.01.00
 * @author Cuong Nguyen
 *
 */
public class FlowDeploymentJobCancellationService implements IDomain<FlowDeploymentJobCancellationServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowRelDistributeEditSupport support = null;

	private IDomain<IGeneralServiceBean> setTimerConfirmService = null;
	private IDomain<IGeneralServiceBean> compTimerSettingService = null;

	private ActionStatusMatrixList statusMatrixAction;

	public void setSupport(FlowRelDistributeEditSupport support) {
		this.support = support;
	}

	public void setSetTimerConfirmService(IDomain<IGeneralServiceBean> setTimerConfirmService) {
		this.setTimerConfirmService = setTimerConfirmService;
	}
	public void setCompTimerSettingService(IDomain<IGeneralServiceBean> compTimerSettingService) {
		this.compTimerSettingService = compTimerSettingService;
	}

	public void setStatusMatrixAction( ActionStatusMatrixList action ) {
		this.statusMatrixAction = action;
	}

	@Override
	public IServiceDto<FlowDeploymentJobCancellationServiceBean> execute(
			IServiceDto<FlowDeploymentJobCancellationServiceBean> serviceDto) {

		FlowDeploymentJobCancellationServiceBean paramBean = serviceDto.getServiceBean();

		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");
			String mgtVer = paramBean.getParam().getSelectedMgtVer();
			PreConditions.assertOf(TriStringUtils.isNotEmpty(mgtVer), "SelectedMgtVer is not specified");

			if ( RequestType.submitChanges.equals(paramBean.getParam().getRequestType()) ) {
				this.submitChanges(paramBean);
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(DmMessageId.DM005025S, e, paramBean.getFlowAction());
		} finally {
		}
	}


	private void submitChanges(FlowDeploymentJobCancellationServiceBean paramBean){

		String mgtVer = paramBean.getParam().getSelectedMgtVer();
		IDmDoEntity entity = this.support.findDmDoEntity( mgtVer );

		//validation
		{
			StatusCheckDto statusDto = new StatusCheckDto()
					.setServiceBean( paramBean )
					.setFinder( support )
					.setActionList( statusMatrixAction )
					.setLotIds( entity.getLotId() )
					.setRpIds( entity.getRpId() )
					;
			StatusMatrixCheckUtils.checkStatusMatrix( statusDto );

			if( DmBusinessJudgUtils.isPastSetTime(entity) ){
				throw new ContinuableBusinessException( DmMessageId.DM001023E );
			}
		}

		paramBean.setInnerService(new FlowRelDistributeTimerServiceBean());

		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowRelDistributeTimerServiceBean serviceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(serviceBean, paramBean);
		dto.setServiceBean(serviceBean);

		this.beforeExecution( paramBean , serviceBean, entity );

		serviceBean.setReferer(RelDistributeScreenID.TIMER_SET);
		serviceBean.setForward(RelDistributeScreenID.TIMER_CONFIRM);
		setTimerConfirmService.execute(dto);

		serviceBean.setReferer(RelDistributeScreenID.TIMER_CONFIRM);
		serviceBean.setForward(RelDistributeScreenID.COMP_TIMER);
		setTimerConfirmService.execute(dto);
		compTimerSettingService.execute(dto);
		
		String errMessage = serviceBean.getDrmsMessage();
		if ( TriStringUtils.isNotEmpty(errMessage) ){
			IMessageId infoMessage = serviceBean.getInfoMessageId();
			if ( infoMessage  != null ){
				throw new ContinuableBusinessException( infoMessage );
			}		
		}

		this.afterExecution( paramBean, entity );
	}


	private void beforeExecution( FlowDeploymentJobCancellationServiceBean src,
									FlowRelDistributeTimerServiceBean dest,
									IDmDoEntity dmDoEntity){

		src.getResult().setCompleted(false);

		String rpId = dmDoEntity.getRpId();
		IRpEntity rpEntity = this.support.findRpEntity(rpId);

		IBldEnvEntity bldEnvEntity = this.support.getBmFinderSupport().findBldEnvEntity(rpEntity.getBldEnvId());

		dest.setDistNo			( rpId );
		dest.setSelectedDistNo	( rpId ) ;
		dest.setRmi				( !src.getParam().getInputInfo().isSelectedManual() ) ;
		dest.setTimerSetting	( TimerSettings.Cancel.legacy() );
		dest.getConfViewBean().setConfName(bldEnvEntity.getBldEnvNm());
	}


	private void afterExecution( FlowDeploymentJobCancellationServiceBean paramBean,
									IDmDoEntity dmDoEntity) {

		paramBean.getResult().setCompleted(true);
		paramBean.getMessageInfo().addFlashTranslatable(DmMessageId.DM003004I);
	}
}
