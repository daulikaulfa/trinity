package jp.co.blueship.tri.dm.domainx.deploy.dto;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.dm.domainx.deploy.dto.FlowDeploymentJobListServiceBean.DeploymentJobListView;
import jp.co.blueship.tri.dm.domainx.deploy.dto.FlowDeploymentJobListServiceBean.RequestOption;
import jp.co.blueship.tri.dm.domainx.deploy.dto.FlowDeploymentJobListServiceBean.SearchCondition;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.um.constants.ExportToFileSubmitOption;
import jp.co.blueship.tri.fw.um.domainx.cmn.beans.dto.ExportToFileBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 * @version V4.01.00
 * @author Akahoshi
 *
 */
public class FlowDeploymentJobListServiceExportToFileBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private RequestsCompletion result = new RequestsCompletion();
	private FileResponse fileResponse = new FileResponse();
	private List<DeploymentJobListView> jobViews = new ArrayList<DeploymentJobListView>();

	{
		this.setInnerService( new FlowDeploymentJobListServiceBean() );
	}

	public List<DeploymentJobListView> getJobViews() {
		return jobViews;
	}
	public FlowDeploymentJobListServiceExportToFileBean setJobViews(List<DeploymentJobListView> jobViews) {
		this.jobViews = jobViews;
		return this;
	}

	public RequestParam getParam() {
		return param;
	}

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowDeploymentJobListServiceExportToFileBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	public FileResponse getFileResponse() {
		return fileResponse;
	}
	public FlowDeploymentJobListServiceExportToFileBean setFileResponse(FileResponse fileResponse) {
		this.fileResponse = fileResponse;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String lotId = null;
		private DeploymentJobListServiceExportToFileInputBean inputInfo = new DeploymentJobListServiceExportToFileInputBean();
		private SearchCondition searchCondition = new FlowDeploymentJobListServiceBean().new SearchCondition();
		private ExportToFileSubmitOption submitOption = ExportToFileSubmitOption.ExportToCsv;
		private ExportToFileBean exportBean = null;
		private RequestOption requestOption = RequestOption.none;

		public RequestOption getRequestOption() {
			return requestOption;
		}
		public RequestParam setRequestOption(RequestOption requestOption) {
			this.requestOption = requestOption;
			return this;
		}

		public String getLotId() {
			return lotId;
		}
		public RequestParam setLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}

		public DeploymentJobListServiceExportToFileInputBean getInputInfo() {
			return inputInfo;
		}
		public RequestParam setInputInfo(DeploymentJobListServiceExportToFileInputBean inputInfo) {
			this.inputInfo = inputInfo;
			return this;
		}

		public SearchCondition getSearchCondition() {
			return searchCondition;
		}
		public RequestParam setSearchCondition(SearchCondition searchCondition) {
			this.searchCondition = searchCondition;
			return this;
		}

		public ExportToFileSubmitOption getSubmitOption() {
			return submitOption;
		}
		public RequestParam setSubmitOption(ExportToFileSubmitOption submitOption) {
			this.submitOption = submitOption;
			return this;
		}
		public ExportToFileBean getExportBean() {
			return exportBean;
		}
		public RequestParam setExportBean(ExportToFileBean exportBean) {
			this.exportBean = exportBean;
			return this;
		}
	}

	public class DeploymentJobListServiceExportToFileInputBean {
		private boolean manual = true;

		public boolean isSelectedManual() {
			return manual;
		}
		public boolean isSelectedAutomatic(){
			return !manual;
		}
		public DeploymentJobListServiceExportToFileInputBean setManual(boolean manual) {
			this.manual = manual;
			return this;
		}
	}

	public class FileResponse {
		private File file;

		public File getFile() {
			return file;
		}
		public void setFile(File file) {
			this.file = file;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private boolean completed = false;

		public boolean isCompleted() {
			return completed;
		}
		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}
}
