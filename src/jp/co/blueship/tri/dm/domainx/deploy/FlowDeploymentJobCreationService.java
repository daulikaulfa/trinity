package jp.co.blueship.tri.dm.domainx.deploy;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.bm.dao.bldenv.constants.BldEnvItems;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvEntity;
import jp.co.blueship.tri.dm.DmFluentFunctionUtils;
import jp.co.blueship.tri.dm.constants.RelDistributeScreenID;
import jp.co.blueship.tri.dm.dao.dmdo.eb.DmDoCondition;
import jp.co.blueship.tri.dm.dao.dmdo.eb.IDmDoEntity;
import jp.co.blueship.tri.dm.domain.beans.dop.dto.ControlViewBean;
import jp.co.blueship.tri.dm.domain.beans.dop.dto.ReleasePackageViewBean;
import jp.co.blueship.tri.dm.domain.dop.dto.FlowRelDistributeEntryServiceBean;
import jp.co.blueship.tri.dm.domain.dop.dto.FlowRelDistributeTimerServiceBean;
import jp.co.blueship.tri.dm.domain.dop.dto.FlowRelDistributeTimerServiceBean.TimerSetViewBean;
import jp.co.blueship.tri.dm.domainx.deploy.dto.FlowDeploymentJobCreationServiceBean;
import jp.co.blueship.tri.dm.domainx.deploy.dto.FlowDeploymentJobCreationServiceBean.DeploymentJobEditInputBean;
import jp.co.blueship.tri.dm.domainx.deploy.dto.FlowDeploymentJobCreationServiceBean.RequestOption;
import jp.co.blueship.tri.dm.support.FlowRelDistributeEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.cmn.utils.function.TriFunction;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.status.DmDoStatusId;
import jp.co.blueship.tri.fw.constants.status.RmRpStatusId;
import jp.co.blueship.tri.fw.dao.orm.DBSearchSortAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.DmMessageId;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;
import jp.co.blueship.tri.fw.um.UmFluentFunctionUtils;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.ICtgEntity;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.IMstoneEntity;
import jp.co.blueship.tri.rm.RmDBSearchConditionAddonUtils;
import jp.co.blueship.tri.rm.dao.constants.RmTables;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpBpLnkEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpDto;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.RpCondition;
/**
 *
 * @version V4.00.00
 * @author Akahoshi
 */
public class FlowDeploymentJobCreationService implements IDomain<FlowDeploymentJobCreationServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowRelDistributeEditSupport support = null;

	private IDomain<IGeneralServiceBean> envSelectService = null;
	private IDomain<IGeneralServiceBean> infoInputService = null;
	private IDomain<IGeneralServiceBean> controlSelectService = null;
	private IDomain<IGeneralServiceBean> confirmService = null;
	private IDomain<IGeneralServiceBean> createService = null;

	private IDomain<IGeneralServiceBean> setTimerConfirmService = null;
	private IDomain<IGeneralServiceBean> compTimerSettingService = null;

	private ActionStatusMatrixList statusMatrixAction;


	public void setSupport(FlowRelDistributeEditSupport support) {
		this.support = support;
	}

	public void setEnvSelectService(IDomain<IGeneralServiceBean> envSelectService) {
		this.envSelectService = envSelectService;
	}

	public void setInfoInputService(IDomain<IGeneralServiceBean> infoInputService) {
		this.infoInputService = infoInputService;
	}

	public void setControlSelectService(IDomain<IGeneralServiceBean> controlSelectService) {
		this.controlSelectService = controlSelectService;
	}

	public void setConfirmService(IDomain<IGeneralServiceBean> confirmService) {
		this.confirmService = confirmService;
	}

	public void setCreateService(IDomain<IGeneralServiceBean> createService) {
		this.createService = createService;
	}

	public void setSetTimerConfirmService(IDomain<IGeneralServiceBean> setTimerConfirmService) {
		this.setTimerConfirmService = setTimerConfirmService;
	}

	public void setCompTimerSettingService(IDomain<IGeneralServiceBean> compTimerSettingService) {
		this.compTimerSettingService = compTimerSettingService;
	}

	public void setStatusMatrixAction( ActionStatusMatrixList action ) {
		this.statusMatrixAction = action;
	}

	@Override
	public IServiceDto<FlowDeploymentJobCreationServiceBean> execute(
			IServiceDto<FlowDeploymentJobCreationServiceBean> serviceDto) {

		FlowDeploymentJobCreationServiceBean paramBean = serviceDto.getServiceBean();
		try {



			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");
			String lotId = paramBean.getParam().getSelectedLotId();
			PreConditions.assertOf(TriStringUtils.isNotEmpty(lotId), "SelectedLotId is not specified");

			if ( RequestType.init.equals(paramBean.getParam().getRequestType()) ){
				this.init(paramBean);

			}else if( RequestType.onChange.equals(paramBean.getParam().getRequestType()) ){
				this.onChange(paramBean);

			}else if ( RequestType.submitChanges.equals(paramBean.getParam().getRequestType()) ) {
				this.submitChanges(paramBean);
			}



			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(DmMessageId.DM005025S, e, paramBean.getFlowAction());
		} finally {

		}
	}



	private void init(FlowDeploymentJobCreationServiceBean paramBean){
		this.onChange(paramBean);
	}


	private void onChange(FlowDeploymentJobCreationServiceBean paramBean){
		paramBean.getResult().setCompleted(false);
		this.setReleaseEnvViews		(paramBean);
		this.setCtgViews			(paramBean);
		this.setMstoneViews			(paramBean);
		RequestOption selectedRequestCtg = paramBean.getParam().getRequestOption();
		if (RequestOption.refreshCategory.equals(selectedRequestCtg)) {
			// Set Category Views

			List<ICtgEntity> ctgEntities = this.support.getUmFinderSupport().findCtgByLotId(paramBean.getParam().getSelectedLotId());
			paramBean.getParam().getInputInfo().setCtgViews(FluentList.from( ctgEntities ).map( UmFluentFunctionUtils.toItemLabelsFromCtgEntity ).asList());
		}

		if (RequestOption.refreshMilestone.equals(selectedRequestCtg)) {
			List<IMstoneEntity> mstoneEntities = this.support.getUmFinderSupport().findMstoneByLotId(paramBean.getParam().getSelectedLotId());
			paramBean.getParam().getInputInfo().setMstoneViews(FluentList.from( mstoneEntities ).map( UmFluentFunctionUtils.toItemLabelsFromMstoneEntity ).asList());
		}
		
		if (RequestOption.selectEnvironment.equals(selectedRequestCtg)) {
			this.setReleasePackageViews	(paramBean);
		}
	}


	private void submitChanges(FlowDeploymentJobCreationServiceBean paramBean){
		String mgtVer = paramBean.getResult().getDeploymentId();

		if(TriStringUtils.isEmpty(mgtVer))
			this.createDeploymentJob(paramBean);
		else
			this.setTimer(paramBean);

		if(paramBean.getResult().isCompleted())
			paramBean.getMessageInfo().addFlashTranslatable(DmMessageId.DM003000I);
	}


	private void createDeploymentJob(FlowDeploymentJobCreationServiceBean paramBean){

		// Status Matrix Check
		{
			String rpId = paramBean.getParam().getInputInfo().getRpId();

			StatusCheckDto statusDto = new StatusCheckDto()
					.setServiceBean( paramBean )
					.setFinder( support )
					.setActionList( statusMatrixAction )
					.setLotIds( paramBean.getParam().getSelectedLotId() )
					.setRpIds( rpId )
					;

			StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
		}

		paramBean.setInnerService(new FlowRelDistributeEntryServiceBean());

		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowRelDistributeEntryServiceBean serviceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(serviceBean, paramBean);
		dto.setServiceBean(serviceBean);

		creationBeforeExecution( paramBean , serviceBean );

		serviceBean.setReferer(RelDistributeScreenID.ENV_SELECT);
		serviceBean.setForward(RelDistributeScreenID.SELECT);
		envSelectService.execute(dto);

		serviceBean.setReferer(RelDistributeScreenID.SELECT);
		serviceBean.setForward(RelDistributeScreenID.INFO_INPUT);
		infoInputService.execute(dto);

		serviceBean.setReferer(RelDistributeScreenID.INFO_INPUT);
		serviceBean.setForward(RelDistributeScreenID.ENTRY_CONFIRM);
		controlSelectService.execute(dto);

		serviceBean.setReferer(RelDistributeScreenID.ENTRY_CONFIRM);
		serviceBean.setForward(RelDistributeScreenID.COMP_ENTRY);
		confirmService.execute(dto);
		createService.execute(dto);
		
		
		String errMessage = serviceBean.getDrmsMessage();
		if ( TriStringUtils.isNotEmpty(errMessage) ){
			IMessageId infoMessage = serviceBean.getInfoMessageId();
			if ( infoMessage  != null ){
				throw new ContinuableBusinessException( infoMessage );
			}		
		}
		
		String mgtVer =  this.support.makeLinkNoLabel(paramBean.getParam().getInputInfo().getRpId());
		paramBean.getResult().setCompleted(true);
		paramBean.getResult().setDeploymentId(mgtVer);
	}


	private void creationBeforeExecution( FlowDeploymentJobCreationServiceBean src ,
										  FlowRelDistributeEntryServiceBean dest){

		src.getResult().setCompleted(false);

		DeploymentJobEditInputBean srcInfo = src.getParam().getInputInfo();

		dest.getReleaseConfirmBean().getConfViewBean().setConfName(srcInfo.getRelEnvId());

		dest.setSelectedLotNo		( src.getParam().getSelectedLotId() );
		dest.setSelectedControlNo	( srcInfo.getRpId() );
		dest.setSelectedEnvNo		( srcInfo.getRelEnvId() );
		dest.setRmi					( !srcInfo.isManual() );

		dest.getBaseInfoBean().getBaseInfoInputBean().setRelContent( srcInfo.getContents() );
		dest.getBaseInfoBean().getBaseInfoInputBean().setRelSummary( srcInfo.getSummary() );

		dest.getBaseInfoBean().getBaseInfoInputBean().setCtgId(srcInfo.getCtgId());
		dest.getBaseInfoBean().getBaseInfoInputBean().setMstoneId(srcInfo.getMstoneId());
		
		String systemDate = TriDateUtils.getSystemDate();
		String timmerDate = TriDateUtils.convertDateFormat(
				srcInfo.getTimerDate(),
				srcInfo.getTimerHour() + ":" +
				srcInfo.getTimerMinute());
				
		if ( TriDateUtils.checkDateOrderToMinute( systemDate, timmerDate  ) ) {
			throw new ContinuableBusinessException( DmMessageId.DM001005E );
		}
		
	}


	//timerSettings
	private void setTimer(FlowDeploymentJobCreationServiceBean paramBean){
		paramBean.setInnerService(new FlowRelDistributeTimerServiceBean());

		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowRelDistributeTimerServiceBean serviceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(serviceBean, paramBean);
		dto.setServiceBean(serviceBean);

		this.setTimerBeforeExecution( paramBean , serviceBean );

		serviceBean.setReferer(RelDistributeScreenID.TIMER_SET);
		serviceBean.setForward(RelDistributeScreenID.TIMER_CONFIRM);
		setTimerConfirmService.execute(dto);

		serviceBean.setReferer(RelDistributeScreenID.TIMER_CONFIRM);
		serviceBean.setForward(RelDistributeScreenID.COMP_TIMER);
		setTimerConfirmService.execute(dto);
		compTimerSettingService.execute(dto);
		
		
		String errMessage = serviceBean.getDrmsMessage();
		if ( TriStringUtils.isNotEmpty(errMessage) ){
			IMessageId infoMessage = serviceBean.getInfoMessageId();
			if ( infoMessage  != null ){
				throw new ContinuableBusinessException( infoMessage );
			}		
		}

		paramBean.getResult().setCompleted(true);
	}


	private void setTimerBeforeExecution(FlowDeploymentJobCreationServiceBean src,
										 FlowRelDistributeTimerServiceBean dest){

		src.getResult().setCompleted(false);

		DeploymentJobEditInputBean srcInfo = src.getParam().getInputInfo();

		IBldEnvEntity bldEnvEntity = this.support.getBmFinderSupport().findBldEnvEntity(srcInfo.getRelEnvId());

		dest.setDistNo			( srcInfo.getRpId() );
		dest.setSelectedDistNo	( srcInfo.getRpId()) ;
		dest.setRmi				( !srcInfo.isManual() );
		dest.setTimerSetting	( srcInfo.getTimerSettings().legacy() );
		dest.setTimerDate		(TriDateUtils.convertDateFormat(
											srcInfo.getTimerDate(),
											srcInfo.getTimerHour() + ":" +
											srcInfo.getTimerMinute()));
		dest.getConfViewBean().setConfName(bldEnvEntity.getBldEnvNm());
		dest.setTimerSummary(srcInfo.getTimerSummary());
		dest.setCheckTimerDate(srcInfo.getTimerDate());
		dest.setTimerContent(srcInfo.getTimerContents());

		TimerSetViewBean timeViewBean = dest.getTimerSetViewBean();
		String[] ymd =	TriDateUtils.mapDefaultDateTimeForTimer(dest.getCheckTimerDate());


		timeViewBean.setYear( ymd[0] );
		timeViewBean.setMonth( ymd[1] );
		timeViewBean.setDay( ymd[2] );
		timeViewBean.setHour( ymd[3] );
		timeViewBean.setMinuteHigh( ymd[4] );
		timeViewBean.setMinuteLow( ymd[5] );
		dest.setTimerSetViewBean(timeViewBean);


	}


	private void setReleaseEnvViews(FlowDeploymentJobCreationServiceBean paramBean){

		IJdbcCondition condition = RmDBSearchConditionAddonUtils.getActiveReleaseEnvCondition();
		ISqlSort sort = new SortBuilder();
		sort.setElement(BldEnvItems.bldEnvNm, TriSortOrder.Asc, 1);

		List<IBldEnvEntity> envList = this.support.getBmFinderSupport().getBldEnvDao().find(condition.getCondition(), sort);
		List<ItemLabelsBean> views =  FluentList.from( envList ).map(
										new TriFunction<IBldEnvEntity, ItemLabelsBean>() {
											@Override
											public ItemLabelsBean apply(IBldEnvEntity input) {
												return new ItemLabelsBean(input.getBldEnvNm() , input.getBldEnvId());
											}}
										).asList();

		paramBean.getParam().getInputInfo().setReleaseEnvViews(views);
	}


	private void setCtgViews(FlowDeploymentJobCreationServiceBean paramBean){
		String lotId = paramBean.getParam().getSelectedLotId();

		List<ICtgEntity> ctgEntities = this.support.getUmFinderSupport().findCtgByLotId(lotId);
		List<ItemLabelsBean> views = FluentList.from( ctgEntities ).map( UmFluentFunctionUtils.toItemLabelsFromCtgEntity ).asList();

		paramBean.getParam().getInputInfo().setCtgViews(views);
	}


	private void setMstoneViews(FlowDeploymentJobCreationServiceBean paramBean){
		String lotId = paramBean.getParam().getSelectedLotId();

		List<IMstoneEntity> mstoneEntities = this.support.getUmFinderSupport().findMstoneByLotId(lotId);
		List<ItemLabelsBean> views = FluentList.from( mstoneEntities ).map( UmFluentFunctionUtils.toItemLabelsFromMstoneEntity ).asList();

		paramBean.getParam().getInputInfo().setMstoneViews(views);
	}


	private void setReleasePackageViews(FlowDeploymentJobCreationServiceBean paramBean){

		SimpleDateFormat formatYMD = TriDateUtils.getYMDDateFormat( paramBean.getLanguage(), paramBean.getTimeZone() );
		List<ControlViewBean> controleViews = this.getControlList(paramBean);

		List<ReleasePackageViewBean> views = new ArrayList<ReleasePackageViewBean>();


		for(ControlViewBean controleView : controleViews){

			IRpEntity rpEntity = this.support.findRpEntity(controleView.getRelNo());

			IBldEnvEntity bldEnvEntity = this.support.getBmFinderSupport().findBldEnvEntity(rpEntity.getBldEnvId());

			IRpDto rpDto = this.support.findRpDto(rpEntity, RmTables.RM_RP_BP_LNK);
			List<IRpBpLnkEntity> rpBpLnkEntity = rpDto.getRpBpLnkEntities();
			String bpId = (rpBpLnkEntity.size() > 0) ? rpBpLnkEntity.get(0).getBpId() : null;

			ReleasePackageViewBean view = new ReleasePackageViewBean()
					.setRpId			 ( rpEntity.getRpId() )
					.setBpId			 ( bpId )
					.setEnvironmentNm	 ( bldEnvEntity.getBldEnvNm() )
					.setCreatedBy		 ( rpEntity.getRegUserNm() )
					.setCreatedByIconPath( this.support.getUmFinderSupport().getIconPath(rpEntity.getRegUserId()) )
					.setStartTime		 ( TriDateUtils.convertViewDateFormat(rpEntity.getRegTimestamp(), formatYMD) )
					.setEndTime			 ( TriDateUtils.convertViewDateFormat(rpEntity.getProcEndTimestamp(), formatYMD) )
					;
			views.add(view);
		}
		paramBean.getParam().getInputInfo().setReleasePackageViews(views);
	}


	private List<ControlViewBean> getControlList(FlowDeploymentJobCreationServiceBean paramBean) {
		String lotId = paramBean.getParam().getSelectedLotId();

		String bldEnvId = paramBean.getParam().getInputInfo().getRelEnvId();
		if( TriStringUtils.isEmpty(bldEnvId) ) bldEnvId = "";

		RpCondition condition = new RpCondition();
		condition.setLotId( lotId );
		condition.setStsIds( new String[] { RmRpStatusId.ReleasePackageCreated.getStatusId() } );
		condition.setBldEnvId(bldEnvId);

		ISqlSort sort = DBSearchSortAddonUtil.getRelSortFromDesignDefineByRelHistoryList();

		List<IRpEntity> rpEntityList = this.support.getRpDao().find( condition.getCondition(), sort );
		List<String> rpIds = this.getRpIdsFromDmDoStsIds();

		List<IRpEntity> rpEntities = new ArrayList<IRpEntity>();

		for ( IRpEntity rpEntity : rpEntityList ) {
			if( !rpIds.contains( rpEntity.getRpId() ) )
				rpEntities.add(rpEntity);
		}

		IRpEntity[] relEntityArray = rpEntities.toArray(new IRpEntity[0]);

		ILotEntity lotEntity = this.support.getAmFinderSupport().findLotEntity(lotId);
		List<ControlViewBean> controlList = this.support.getControlViewBeanList( relEntityArray , lotEntity);

		return controlList;
	}


	private List<String> getRpIdsFromDmDoStsIds(){

		DmDoCondition dmDoCondition = new DmDoCondition();

		dmDoCondition.setStsIds(
				new String[] {
						DmDoStatusId.JobRegistered.getStatusId(),
						DmDoStatusId.TimerConfigured.getStatusId(),
						DmDoStatusId.JobCancelled.getStatusId() } );
		dmDoCondition.setDelStsId( null );
		List<IDmDoEntity> dmDoEntityList = this.support.getDmDoDao().find( dmDoCondition.getCondition() );

		return FluentList.from(dmDoEntityList).map( DmFluentFunctionUtils.toRpIdFromDmDo ).asList();
	}
	
}
