package jp.co.blueship.tri.dm.domainx.deploy;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import jp.co.blueship.tri.agent.dm.opt.beans.dto.DealAssetRegistrationHistoryBean;
import jp.co.blueship.tri.agent.dm.opt.beans.dto.DealAssetServiceBean;
import jp.co.blueship.tri.agent.dm.opt.beans.dto.DealAssetServiceResponseBean;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvEntity;
import jp.co.blueship.tri.dm.DmBusinessJudgUtils;
import jp.co.blueship.tri.dm.constants.DrmsApplyResultId;
import jp.co.blueship.tri.dm.constants.DrmsStatusId;
import jp.co.blueship.tri.dm.dao.dmdo.eb.DmDoCondition;
import jp.co.blueship.tri.dm.dao.dmdo.eb.IDmDoEntity;
import jp.co.blueship.tri.dm.domain.beans.dop.dto.ReleasePackageViewBean;
import jp.co.blueship.tri.dm.domainx.deploy.dto.FlowDeploymentJobDetailsServiceBean;
import jp.co.blueship.tri.dm.domainx.deploy.dto.FlowDeploymentJobDetailsServiceBean.DeploymentJobDetailsView;
import jp.co.blueship.tri.dm.support.FlowRelDistributeEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.status.DmDoStatusId;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.DmMessageId;
import jp.co.blueship.tri.rm.dao.constants.RmTables;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpBpLnkEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpDto;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;

/**
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 * @version V4.01.00
 * @author Cuong Nguyen
 */
public class FlowDeploymentJobDetailsService implements IDomain<FlowDeploymentJobDetailsServiceBean>{

	private static final ILog log = TriLogFactory.getInstance();
	private List<IDomain<IGeneralServiceBean>> actions = new ArrayList<IDomain<IGeneralServiceBean>>();

	private FlowRelDistributeEditSupport support = null;

	public void setSupport(FlowRelDistributeEditSupport support) {
		this.support = support;
	}

	public void setActions(List<IDomain<IGeneralServiceBean>> actions) {
		this.actions = actions;
	}

	@Override
	public IServiceDto<FlowDeploymentJobDetailsServiceBean> execute(
			IServiceDto<FlowDeploymentJobDetailsServiceBean> serviceDto) {

		FlowDeploymentJobDetailsServiceBean paramBean = serviceDto.getServiceBean();
		try {

			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");
			String mgtVer = paramBean.getParam().getSelectedMgtVer();
			PreConditions.assertOf(TriStringUtils.isNotEmpty(mgtVer), "SelectedMgtVer is not specified");



			if ( RequestType.init.equals(paramBean.getParam().getRequestType()) ) {
				this.init(paramBean);
			}
			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(DmMessageId.DM005025S, e, paramBean.getFlowAction());
		}
	}


	private void init(FlowDeploymentJobDetailsServiceBean paramBean){

		String mgtVer = paramBean.getParam().getSelectedMgtVer();
		SimpleDateFormat formatYMDHM = TriDateUtils.getYMDHMDateFormat( paramBean.getLanguage(), paramBean.getTimeZone() );

		DmDoCondition dmCondition = new DmDoCondition();
		dmCondition.setMgtVer(mgtVer);

		IDmDoEntity dmDoEntity = this.support.getDmDoDao().findByPrimaryKey(dmCondition.getCondition());

		String rpId = dmDoEntity.getRpId();

		IRpEntity rpEntity = this.support.findRpEntity(rpId);
		IBldEnvEntity bldEnvEntity = this.support.getBmFinderSupport().findBldEnvEntity(rpEntity.getBldEnvId());

		IRpDto rpDto = this.support.findRpDto(rpEntity, RmTables.RM_RP_BP_LNK);
		List<IRpBpLnkEntity> rpBpLnkEntity = rpDto.getRpBpLnkEntities();
		String bpId = (rpBpLnkEntity.size() > 0) ? rpBpLnkEntity.get(0).getBpId() : null;
		boolean editable = true;
		boolean cancelled = false;
		cancelled = DmDoStatusId.JobCancelled.getStatusId().equals( dmDoEntity.getStsId() );

		if ( cancelled ) {
			editable = false;

		} else {
			editable = DmBusinessJudgUtils.isPastSetTime( dmDoEntity )? false : true ;
		}


		DeploymentJobDetailsView detailsView = paramBean.getDetailsView()
				.setMgtVer					( dmDoEntity.getMgtVer() )
				.setDeploymentId			( dmDoEntity.getDeploymentId() )
				.setSummary					( dmDoEntity.getSummary() )
				.setContents				( dmDoEntity.getContent() )
				.setEnvId					( rpEntity.getBldEnvId() )
				.setEnvironmentNm			( bldEnvEntity.getBldEnvNm() )
				.setCategoryNm				( dmDoEntity.getCtgNm() )
				.setMstoneNm				( dmDoEntity.getMstoneNm() )
				.setStsId					( dmDoEntity.getProcStsId() )
				.setTimerSettingsTime		( TriDateUtils.convertViewDateFormat(
												TriDateUtils.convertDateFormat(
														dmDoEntity.getTimerSettingDate(),
														dmDoEntity.getTimerSettingTime())))
				.setTimerSummary			( dmDoEntity.getTimerSettingSummary() )
				.setTimerContents			( dmDoEntity.getTimerSettingContent() )
				.setEditEnabled				( editable )
				.setJobCancelled			( cancelled )
				;

		if( !paramBean.getParam().getInputInfo().isSelectedManual() ) {
			List<DealAssetRegistrationHistoryBean> historyBeanList = this.getHistoryBeanList(paramBean, rpEntity.getBldEnvId());
			if ( TriCollectionUtils.isNotEmpty(historyBeanList) ) {
				this.setDeploymentInfo(historyBeanList, detailsView);
			}
		}

		ReleasePackageViewBean rpView = detailsView.getReleasePackage()
				.setRpId			 ( rpEntity.getRpId() )
				.setBpId			 ( bpId )
				.setEnvironmentNm	 ( bldEnvEntity.getBldEnvNm() )
				.setCreatedBy		 ( rpEntity.getRegUserNm() )
				.setCreatedByIconPath( this.support.getUmFinderSupport().getIconPath(rpEntity.getRegUserId() ))
				.setStartTime( TriDateUtils.convertViewDateFormat( rpEntity.getProcStTimestamp(), formatYMDHM) )
				.setEndTime( TriDateUtils.convertViewDateFormat( rpEntity.getProcEndTimestamp(), formatYMDHM) )
				;
		detailsView.setReleasePackage(rpView);

		paramBean.setDetailsView(detailsView);
	}

	private List<DealAssetRegistrationHistoryBean> getHistoryBeanList(FlowDeploymentJobDetailsServiceBean paramBean, String envId) {
		// 配付履歴確認実行
		List<DealAssetRegistrationHistoryBean> historyBeanList = null;

		List<Object> paramList = new CopyOnWriteArrayList<Object>() ;

		DealAssetServiceResponseBean resBean = null;

		DealAssetServiceBean dealBean = new DealAssetServiceBean();
		dealBean.setTargetEnvironment(envId);
		paramList.add(dealBean);

		IServiceDto<IGeneralServiceBean> innerServiceDto = new ServiceDto<IGeneralServiceBean>()
				.setParamList( paramList );

		// 下層のアクションを実行する
		try{
			for ( IDomain<IGeneralServiceBean> action : actions ) {
				action.execute( innerServiceDto );
			}	
		}catch ( TriSystemException e){
			if ( !e.getMessageID().equals(DmMessageId.DM005004S) ){
				LogHandler.fatal(log, e);
				ExceptionUtils.reThrowIfTrinityException(e);
			}
			
		}
		
		//戻り値のチェック
		resBean = this.support.getDealAssetServiceResponseBean( paramList ) ;
		IContextAdapter contextAdapter = ContextAdapterFactory.getContextAdapter();
		if ( TriStringUtils.isEmpty( resBean ) ) {
			paramBean.getDetailsView().getWarningMessages().add( contextAdapter.getMessage(paramBean.getLanguage() , false , DmMessageId.DM001006E ) );
		} else {
			int returnCode = resBean.getReturnCode();
			if ( returnCode != 0 ) {
				
				if (returnCode == 1) {
					paramBean.getDetailsView().getWarningMessages().add( contextAdapter.getMessage(paramBean.getLanguage() , false , DmMessageId.DM001007E ) );
				} else {
					paramBean.getDetailsView().getWarningMessages().add( contextAdapter.getMessage(paramBean.getLanguage() , false , DmMessageId.DM001011E ) );
				}
			}
			historyBeanList = resBean.getRegistrationHistoryList();
		}

		return historyBeanList;
	}

	private void setDeploymentInfo(List<DealAssetRegistrationHistoryBean> historyBeanList, DeploymentJobDetailsView viewBean) {

		String latestDate = FlowRelDistributeEditSupport.TIMER_ALL_ZERO ;
		for( DealAssetRegistrationHistoryBean historyBean : historyBeanList) {
			// RC番号が一致

			if (null != viewBean.getMgtVer()) {
				if (viewBean.getMgtVer().equals(historyBean.getDrmsManagementNo())) {
					String date = historyBean.getRegistrationDate();
					// 適用済み
					if (null != date) {
						// 登録日が新しい
						if (latestDate.compareTo(date) < 0) {
							latestDate = date;
							String inputDate = this.support.convertViewDateFormat(
									historyBean.getRegistrationDate(), TriDateUtils.getViewDateFormat() );
							viewBean.setDeploymentRegistedTime(inputDate);
							String applyDate = this.support.convertViewDateFormat(
									historyBean.getDealDate(),  TriDateUtils.getViewDateFormat() );
							viewBean.setDeploymentAppliedTime(applyDate);
							if (null != historyBean.getStatus()) {
								String status = historyBean.getStatus();
								if ( DrmsApplyResultId.SUCCESS.getValue().equals( status ) ) {
									viewBean.setDeploymentStatus( DrmsStatusId.APPLY .getValue() ) ;
									viewBean.setDeploymentStsId( DrmsStatusId.APPLY.name() );
								} else if ( DrmsApplyResultId.FAIL.getValue().equals( status ) ) {
									viewBean.setDeploymentStatus( DrmsStatusId.APPLY_ERROR .getValue() ) ;
									viewBean.setDeploymentStsId( DrmsStatusId.APPLY_ERROR.name() );
								}
							}
						}
					}
				}
			}
		}
	}
}
