package jp.co.blueship.tri.dm.domainx.deploy;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.dcm.DcmExportToFileUtils;
import jp.co.blueship.tri.dm.domainx.deploy.dto.FlowDeploymentJobListServiceBean;
import jp.co.blueship.tri.dm.domainx.deploy.dto.FlowDeploymentJobListServiceBean.DeploymentJobListView;
import jp.co.blueship.tri.dm.domainx.deploy.dto.FlowDeploymentJobListServiceBean.SearchCondition;
import jp.co.blueship.tri.dm.domainx.deploy.dto.FlowDeploymentJobListServiceExportToFileBean;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.DmMessageId;
import jp.co.blueship.tri.fw.um.constants.ExportToFileSubmitOption;

/**
 * Provide the following backend services.
 * <br> - ExportToFile
 *
 * @version V4.00.00
 * @author Syaza Hanapi
 * 
 * @version V4.01.00
 * @author chung 
 */
public class FlowDeploymentJobListServiceExportToFile implements IDomain<FlowDeploymentJobListServiceExportToFileBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IDomain<IGeneralServiceBean> deploymentJobListService = null;

	public void setDeploymentJobList(IDomain<IGeneralServiceBean> deploymentJobListService) {
		this.deploymentJobListService = deploymentJobListService;
	}

	@Override
	public IServiceDto<FlowDeploymentJobListServiceExportToFileBean> execute(
			IServiceDto<FlowDeploymentJobListServiceExportToFileBean> serviceDto) {
		FlowDeploymentJobListServiceExportToFileBean paramBean = serviceDto.getServiceBean();
		FlowDeploymentJobListServiceBean innerServiceBean = paramBean.getInnerService();

		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");
			if(RequestType.onChange.equals(paramBean.getParam().getRequestType())){
				this.onChange(paramBean);
			}
			if (RequestType.submitChanges.equals(paramBean.getParam().getRequestType())) {
				this.submitChanges(paramBean);
			}
			return serviceDto;
		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(DmMessageId.DM005025S, e, paramBean.getFlowAction());
		} finally {
			ExceptionUtils.copyToParent(paramBean, innerServiceBean);
		}

	}
	
	private void submitChanges(FlowDeploymentJobListServiceExportToFileBean paramBean) {
		File file = null;
		ExportToFileSubmitOption submitOption = paramBean.getParam().getSubmitOption();

		if (ExportToFileSubmitOption.ExportToExcel.equals(submitOption)) {
			file = DcmExportToFileUtils.createExcel(paramBean, paramBean.getParam().getExportBean());
		} else if (ExportToFileSubmitOption.ExportToCsv.equals(submitOption)) {
			file = DcmExportToFileUtils.createCSV(paramBean, paramBean.getParam().getExportBean());
		}

		paramBean.getFileResponse().setFile(file);
	}

	private void onChange(FlowDeploymentJobListServiceExportToFileBean paramBean) {
		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowDeploymentJobListServiceBean serviceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(serviceBean, paramBean);
		dto.setServiceBean(serviceBean);

		{
			serviceBean.getParam().setRequestType(RequestType.init);
			serviceBean.getParam().setSelectedLotId(paramBean.getParam().getLotId());
			deploymentJobListService.execute(dto);
		}

		serviceBean.getParam().setRequestType(RequestType.onChange);
		this.beforeExecution(paramBean, serviceBean);
		deploymentJobListService.execute(dto);
		this.afterExecution(serviceBean, paramBean);

	}

	private void beforeExecution(FlowDeploymentJobListServiceExportToFileBean src, FlowDeploymentJobListServiceBean dest) {
		SearchCondition srcSearchConditon = src.getParam().getSearchCondition();
		SearchCondition destSearchCondition = dest.getParam().getSearchCondition();

		if (RequestType.onChange.equals(src.getParam().getRequestType())) {
			dest.getParam().setRequestType(RequestType.onChange);

			destSearchCondition.setEnvId(srcSearchConditon.getEnvId());
			destSearchCondition.setStsId(srcSearchConditon.getStsId());
			destSearchCondition.setCtgId(srcSearchConditon.getCtgId());
			destSearchCondition.setMstoneId(srcSearchConditon.getMstoneId());
			destSearchCondition.setKeyword(srcSearchConditon.getKeyword());
			destSearchCondition.setSelectedPageNo(1);
			dest.getParam().setLinesPerPage(0);
			
			dest.getParam().getInputInfo().setManual(src.getParam().getInputInfo().isSelectedManual());
			dest.getParam().setRequestOption(src.getParam().getRequestOption());
		}
	}

	private void afterExecution(FlowDeploymentJobListServiceBean src, FlowDeploymentJobListServiceExportToFileBean dest) {
		List<DeploymentJobListView> srcSearchSiteViews = src.getJobViews();
		List<DeploymentJobListView> destSearchSiteViews = new ArrayList<DeploymentJobListView>();

		if (RequestType.onChange.equals(src.getParam().getRequestType())) {
			for (int i = 0; i < srcSearchSiteViews.size(); i++) {
				DeploymentJobListView view = srcSearchSiteViews.get(i);
				destSearchSiteViews.add(view);
			}
			dest.setJobViews(destSearchSiteViews);
		}
	}
}
