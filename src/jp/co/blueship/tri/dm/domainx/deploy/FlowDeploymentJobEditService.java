package jp.co.blueship.tri.dm.domainx.deploy;

import java.text.SimpleDateFormat;
import java.util.List;

import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvEntity;
import jp.co.blueship.tri.dm.DmBusinessJudgUtils;
import jp.co.blueship.tri.dm.constants.RelDistributeScreenID;
import jp.co.blueship.tri.dm.constants.TimerSettings;
import jp.co.blueship.tri.dm.dao.constants.DmTables;
import jp.co.blueship.tri.dm.dao.dmdo.eb.DmDoCondition;
import jp.co.blueship.tri.dm.dao.dmdo.eb.IDmDoDto;
import jp.co.blueship.tri.dm.dao.dmdo.eb.IDmDoEntity;
import jp.co.blueship.tri.dm.domain.beans.dop.dto.ReleasePackageViewBean;
import jp.co.blueship.tri.dm.domain.dop.dto.FlowRelDistributeModifyServiceBean;
import jp.co.blueship.tri.dm.domain.dop.dto.FlowRelDistributeTimerServiceBean;
import jp.co.blueship.tri.dm.domain.dop.dto.FlowRelDistributeTimerServiceBean.TimerSetViewBean;
import jp.co.blueship.tri.dm.domainx.deploy.dto.FlowDeploymentJobEditServiceBean;
import jp.co.blueship.tri.dm.domainx.deploy.dto.FlowDeploymentJobEditServiceBean.DeploymentJobDetailsView;
import jp.co.blueship.tri.dm.domainx.deploy.dto.FlowDeploymentJobEditServiceBean.DeploymentJobEditInputBean;
import jp.co.blueship.tri.dm.domainx.deploy.dto.FlowDeploymentJobEditServiceBean.RequestOption;
import jp.co.blueship.tri.dm.support.FlowRelDistributeEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.DesignSheetUtils;
import jp.co.blueship.tri.fw.cmn.utils.ItemCheckAddonUtils;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.RelCommonAddonUtil;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.status.DmDoStatusId;
import jp.co.blueship.tri.fw.constants.status.UmActStatusId;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.DmMessageId;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;
import jp.co.blueship.tri.fw.um.UmFluentFunctionUtils;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.ICtgEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.HistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.IMstoneEntity;
import jp.co.blueship.tri.rm.dao.constants.RmTables;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpBpLnkEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpDto;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.BaseInfoInputBean;

/**
 *
 * @version V4.00.00
 *
 */
public class FlowDeploymentJobEditService implements IDomain<FlowDeploymentJobEditServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowRelDistributeEditSupport support = null;

	private IDomain<IGeneralServiceBean> modifyConfirmService = null;

	private IDomain<IGeneralServiceBean> setTimerConfirmService = null;
	private IDomain<IGeneralServiceBean> compTimerSettingService = null;

	private ActionStatusMatrixList statusMatrixAction;

	public void setSupport(FlowRelDistributeEditSupport support) {
		this.support = support;
	}

	public void setModifyConfirmService(IDomain<IGeneralServiceBean> modifyConfirmService) {
		this.modifyConfirmService = modifyConfirmService;
	}

	public void setSetTimerConfirmService(IDomain<IGeneralServiceBean> setTimerConfirmService) {
		this.setTimerConfirmService = setTimerConfirmService;
	}

	public void setCompTimerSettingService(IDomain<IGeneralServiceBean> compTimerSettingService) {
		this.compTimerSettingService = compTimerSettingService;
	}

	public void setStatusMatrixAction( ActionStatusMatrixList action ) {
		this.statusMatrixAction = action;
	}

	@Override
	public IServiceDto<FlowDeploymentJobEditServiceBean> execute(
			IServiceDto<FlowDeploymentJobEditServiceBean> serviceDto) {

		FlowDeploymentJobEditServiceBean paramBean = serviceDto.getServiceBean();
		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");
			String mgtVer = paramBean.getParam().getSelectedMgtVer();
			PreConditions.assertOf(TriStringUtils.isNotEmpty(mgtVer), "SelectedMgtVer is not specified");

			if ( RequestType.init.equals(paramBean.getParam().getRequestType()) ){
				this.init(paramBean);
			}else if (RequestType.onChange.equals(paramBean.getParam().getRequestType()) ) {
				this.onChange(paramBean);
			}else if ( RequestType.submitChanges.equals(paramBean.getParam().getRequestType()) ) {
				this.submitChanges(paramBean);
			}
			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(DmMessageId.DM005025S, e, paramBean.getFlowAction());
		} finally {

		}
	}

	private void onChange(FlowDeploymentJobEditServiceBean paramBean) {
		String mgtVer = paramBean.getParam().getSelectedMgtVer();

		DmDoCondition dmCondition = new DmDoCondition();
		dmCondition.setMgtVer(mgtVer);

		IDmDoEntity dmDoEntity = this.support.getDmDoDao().findByPrimaryKey(dmCondition.getCondition());
		String lotId = dmDoEntity.getLotId();
		RequestOption selectedRequestCtg = paramBean.getParam().getRequestOption();
		if (RequestOption.refreshCategory.equals(selectedRequestCtg)) {
			// Set Category Views
			List<ICtgEntity> ctgEntities = this.support.getUmFinderSupport().findCtgByLotId( lotId );
			paramBean.getParam().getInputInfo().setCtgViews(FluentList.from( ctgEntities ).map( UmFluentFunctionUtils.toItemLabelsFromCtgEntity ).asList());
		}

		if (RequestOption.refreshMilestone.equals(selectedRequestCtg)) {
			List<IMstoneEntity> mstoneEntities = this.support.getUmFinderSupport().findMstoneByLotId( lotId );
			paramBean.getParam().getInputInfo().setMstoneViews(FluentList.from( mstoneEntities ).map( UmFluentFunctionUtils.toItemLabelsFromMstoneEntity ).asList());

		}
	}

	 private void init(FlowDeploymentJobEditServiceBean paramBean){

		String mgtVer = paramBean.getParam().getSelectedMgtVer();
		SimpleDateFormat formatYMDHM = TriDateUtils.getYMDHMDateFormat( paramBean.getLanguage(), paramBean.getTimeZone() );

		DmDoCondition dmCondition = new DmDoCondition();
		dmCondition.setMgtVer(mgtVer);

		IDmDoEntity dmDoEntity = this.support.getDmDoDao().findByPrimaryKey(dmCondition.getCondition());
		this.setCtgViews			(paramBean, dmDoEntity);
		this.setMstoneViews			(paramBean, dmDoEntity);

		String rpId = dmDoEntity.getRpId();

		IRpEntity rpEntity = this.support.findRpEntity(rpId);
		IBldEnvEntity bldEnvEntity = this.support.getBmFinderSupport().findBldEnvEntity(rpEntity.getBldEnvId());

		IRpDto rpDto = this.support.findRpDto(rpEntity, RmTables.RM_RP_BP_LNK);
		List<IRpBpLnkEntity> rpBpLnkEntity = rpDto.getRpBpLnkEntities();
		String bpId = (rpBpLnkEntity.size() > 0) ? rpBpLnkEntity.get(0).getBpId() : null;


		ReleasePackageViewBean rpView =new ReleasePackageViewBean()
				.setRpId			( rpId )
				.setBpId			( bpId )
				.setEnvironmentNm	( bldEnvEntity.getBldEnvNm() )
				.setCreatedBy		( rpEntity.getRegUserNm() )
				.setCreatedByIconPath( this.support.getUmFinderSupport().getIconPath(rpEntity.getRegUserId() ))
				.setStartTime( TriDateUtils.convertViewDateFormat( rpEntity.getProcStTimestamp(), formatYMDHM) )
				.setEndTime( TriDateUtils.convertViewDateFormat( rpEntity.getProcEndTimestamp(), formatYMDHM) )
				;

		DeploymentJobDetailsView view = paramBean.getDetailsView()
				.setMgtVer			( mgtVer )
				.setDeploymentId	( dmDoEntity.getDeploymentId() )
				.setStsId			( dmDoEntity.getProcStsId() )
				.setEnvId			( bldEnvEntity.getBldEnvId() )
				.setEnvironmentNm	( bldEnvEntity.getBldEnvNm() )
				.setCategoryNm		( dmDoEntity.getCtgNm() )
				.setMstoneNm		( dmDoEntity.getMstoneNm() )
				.setTimerConfigured ( DmDoStatusId.TimerConfigured.equals( dmDoEntity.getStsId() )? true : false  )
				.setReleasePackage	( rpView )
				;
		paramBean.setDetailsView(view);

		String timerSettingTime = dmDoEntity.getTimerSettingTime();
		String timerHour = null;
		String timerMinute = null;

		TimerSettings timerSetting = TimerSettings.none;

		if(TriStringUtils.isNotEmpty(timerSettingTime)){
			timerHour = timerSettingTime.substring(0, timerSettingTime.lastIndexOf(":"));
			timerMinute = timerSettingTime.substring(timerSettingTime.lastIndexOf(":")+1,timerSettingTime.length());
			timerSetting = TimerSettings.TimerSettins;
		}

		paramBean.getParam().getInputInfo().setSummary(dmDoEntity.getSummary())
			.setContents		( dmDoEntity.getContent() )
			.setTimerSettings	( timerSetting )
			.setTimerDate		( dmDoEntity.getTimerSettingDate() )
			.setTimerHour		( timerHour )
			.setTimerMinute		( timerMinute )
			.setCtgId			( dmDoEntity.getCtgId() )
			.setMstoneId		( dmDoEntity.getMstoneId() )
			.setTimerSummary	(dmDoEntity.getTimerSettingSummary())
			.setTimerContents	(dmDoEntity.getTimerSettingContent())
			;
	}

	private void submitChanges(FlowDeploymentJobEditServiceBean paramBean){

		String mgtVer = paramBean.getParam().getSelectedMgtVer();

		IDmDoEntity entity =  this.support.findDmDoEntity( mgtVer );

		//validation
		{
			StatusCheckDto statusDto = new StatusCheckDto()
					.setServiceBean( paramBean )
					.setFinder( support )
					.setActionList( statusMatrixAction )
					.setLotIds( entity.getLotId() )
					.setRpIds( entity.getRpId() )
					;
			StatusMatrixCheckUtils.checkStatusMatrix( statusDto );

			if ( DmDoStatusId.TimerConfigured.getStatusId().equals( entity.getStsId() ) &&
					DmBusinessJudgUtils.isPastSetTime( entity ) ) {

				throw new ContinuableBusinessException( DmMessageId.DM001022E );
			}
		}

		this.edit(paramBean);

		TimerSettings timerSettings = paramBean.getParam().getInputInfo().getTimerSettings();

		if( TimerSettings.Immediately.equals( timerSettings) ||
				TimerSettings.TimerSettins.equals( timerSettings )){

			this.setTimer( paramBean );
		}

		paramBean.getResult().setCompleted(true);
		paramBean.getMessageInfo().addFlashTranslatable(DmMessageId.DM003001I);

		entity =  this.support.findDmDoEntity( mgtVer );
		paramBean.getDetailsView().setStsId( entity.getProcStsId() );
	}


	private void edit(FlowDeploymentJobEditServiceBean paramBean){

		paramBean.setInnerService(new FlowRelDistributeModifyServiceBean());

		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowRelDistributeModifyServiceBean serviceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(serviceBean, paramBean);
		dto.setServiceBean(serviceBean);

		this.editBeforeExecution( paramBean , serviceBean );

		serviceBean.setReferer(RelDistributeScreenID.MODIFY);
		serviceBean.setForward(RelDistributeScreenID.MODIFY_CONFIRM);
		modifyConfirmService.execute(dto);

		serviceBean.setReferer(RelDistributeScreenID.MODIFY_CONFIRM);
		serviceBean.setForward(RelDistributeScreenID.COMP_MODIFY);
		modifyConfirmService.execute(dto);
		executeUpdate(serviceBean);
	}


	private void editBeforeExecution( FlowDeploymentJobEditServiceBean src,
								  FlowRelDistributeModifyServiceBean dest){

		src.getResult().setCompleted(false);

		DeploymentJobEditInputBean srcInfo = src.getParam().getInputInfo();
		BaseInfoInputBean inputBean = dest.getBaseInfoBean().getBaseInfoInputBean();

		dest.setDistNo(  src.getDetailsView().getReleasePackage().getRpId() );

		inputBean.setRelSummary	( srcInfo.getSummary() );
		inputBean.setRelContent	( srcInfo.getContents() );
		inputBean.setCtgId		( srcInfo.getCtgId() );
		inputBean.setMstoneId	( srcInfo.getMstoneId() );
	}

	//timerSettings
	private void setTimer(FlowDeploymentJobEditServiceBean paramBean){
		paramBean.setInnerService(new FlowRelDistributeTimerServiceBean());

		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowRelDistributeTimerServiceBean serviceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(serviceBean, paramBean);
		dto.setServiceBean(serviceBean);

		this.setTimerBeforeExecution( paramBean , serviceBean );

		serviceBean.setReferer(RelDistributeScreenID.TIMER_SET);
		serviceBean.setForward(RelDistributeScreenID.TIMER_CONFIRM);
		setTimerConfirmService.execute(dto);

		serviceBean.setReferer(RelDistributeScreenID.TIMER_CONFIRM);
		serviceBean.setForward(RelDistributeScreenID.COMP_TIMER);
		setTimerConfirmService.execute(dto);
		compTimerSettingService.execute(dto);
		
		String errMessage = serviceBean.getDrmsMessage();
		if ( TriStringUtils.isNotEmpty(errMessage) ){
			IMessageId infoMessage = serviceBean.getInfoMessageId();
			if ( infoMessage  != null ){
				throw new ContinuableBusinessException( infoMessage );
			}		
		}

		paramBean.getResult().setCompleted(true);
	}


	private void setTimerBeforeExecution(FlowDeploymentJobEditServiceBean src,
										 FlowRelDistributeTimerServiceBean dest){

		src.getResult().setCompleted(false);

		DeploymentJobEditInputBean srcInfo = src.getParam().getInputInfo();

		IBldEnvEntity bldEnvEntity = this.support.getBmFinderSupport().findBldEnvEntity(src.getDetailsView().getEnvId());

		dest.setDistNo			( src.getDetailsView().getDeploymentId() );
		dest.setSelectedDistNo	( src.getDetailsView().getReleasePackage().getRpId() ) ;
		dest.setRmi				( !srcInfo.isSelectedManual() );
		dest.setTimerSetting	( srcInfo.getTimerSettings().legacy() );
		dest.setTimerDate		( TriDateUtils.convertDateFormat(
											srcInfo.getTimerDate(),
											srcInfo.getTimerHour() + ":" +
											srcInfo.getTimerMinute()));
		dest.getConfViewBean().setConfName(bldEnvEntity.getBldEnvNm());
		dest.setTimerSummary(srcInfo.getTimerSummary());
		dest.setTimerContent(srcInfo.getTimerContents());
		dest.setCheckTimerDate(srcInfo.getTimerDate());
		TimerSetViewBean timeViewBean = dest.getTimerSetViewBean();
		String[] ymd =	TriDateUtils.mapDefaultDateTimeForTimer(dest.getCheckTimerDate());
		timeViewBean.setYear( ymd[0] );
		timeViewBean.setMonth( ymd[1] );
		timeViewBean.setDay( ymd[2] );
		timeViewBean.setHour( ymd[3] );
		timeViewBean.setMinuteHigh( ymd[4] );
		timeViewBean.setMinuteLow( ymd[5] );
		dest.setTimerSetViewBean(timeViewBean);
	}


	private FlowRelDistributeModifyServiceBean executeUpdate( FlowRelDistributeModifyServiceBean paramBean ){


		try {

			String refererID = paramBean.getReferer();
			String forwordID = paramBean.getForward();

			if ( true != RelDistributeScreenID.COMP_MODIFY.equals( refererID ) &&
					true != RelDistributeScreenID.COMP_MODIFY.equals( forwordID ) ) {
				return paramBean;
			}

			if ( RelDistributeScreenID.COMP_MODIFY.equals( refererID ) ) {

			}

			if ( RelDistributeScreenID.COMP_MODIFY.equals( forwordID ) ) {

				if ( ! ScreenType.bussinessException.equals( paramBean.getScreenType() ) ) {

					String distNo = paramBean.getDistNo();
					ItemCheckAddonUtils.checkRelNo( distNo );

					BaseInfoInputBean inputBean = paramBean.getBaseInfoBean().getBaseInfoInputBean();

					IDmDoDto dmDoDto = this.support.getDmDoDto(distNo);
					// グループの存在チェック
					ILotDto dto = this.support.getAmFinderSupport().findLotDto( dmDoDto.getRpEntity().getLotId() );
					List<IGrpUserLnkEntity> groupUsers	= support.getUmFinderSupport().findGrpUserLnkByUserId( paramBean.getUserId() );
					RelCommonAddonUtil.checkAccessableGroup( dto, this.support.getUmFinderSupport().getGrpDao(), groupUsers );

					dmDoDto.getDmDoEntity().setSummary	( inputBean.getRelSummary() );
					dmDoDto.getDmDoEntity().setContent	( inputBean.getRelContent() );
					this.support.getDmDoDao().update( dmDoDto.getDmDoEntity() );

					dmDoDto.getRpEntity().setUpdUserNm	( paramBean.getUserName() );
					dmDoDto.getRpEntity().setUpdUserId	( paramBean.getUserId() );
					this.support.getRpDao().update( dmDoDto.getRpEntity() );

					this.support.getUmFinderSupport().updateCtgLnk(inputBean.getCtgId(), DmTables.DM_DO , dmDoDto.getDmDoEntity().getMgtVer() );
					this.support.getUmFinderSupport().updateMstoneLnk(inputBean.getMstoneId(), DmTables.DM_DO , dmDoDto.getDmDoEntity().getMgtVer() );

					if ( DesignSheetUtils.isRecord() ){

						IHistEntity histEntity = new HistEntity();
						histEntity.setActSts( UmActStatusId.none.getStatusId() );

						support.getDmDoHistDao().insert( histEntity , dmDoDto);
					}

				}
			}

			return paramBean;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(DmMessageId.DM005012S , e);
		}

	}

	private void setCtgViews(FlowDeploymentJobEditServiceBean paramBean, IDmDoEntity dmDoEntity){
		String lotId = dmDoEntity.getLotId();

		List<ICtgEntity> ctgEntities = this.support.getUmFinderSupport().findCtgByLotId(lotId);
		List<ItemLabelsBean> views = FluentList.from( ctgEntities ).map( UmFluentFunctionUtils.toItemLabelsFromCtgEntity ).asList();

		paramBean.getParam().getInputInfo().setCtgViews(views);
	}


	private void setMstoneViews(FlowDeploymentJobEditServiceBean paramBean, IDmDoEntity dmDoEntity){
		String lotId = dmDoEntity.getLotId();

		List<IMstoneEntity> mstoneEntities = this.support.getUmFinderSupport().findMstoneByLotId(lotId);
		List<ItemLabelsBean> views = FluentList.from( mstoneEntities ).map( UmFluentFunctionUtils.toItemLabelsFromMstoneEntity ).asList();

		paramBean.getParam().getInputInfo().setMstoneViews(views);
	}


}
