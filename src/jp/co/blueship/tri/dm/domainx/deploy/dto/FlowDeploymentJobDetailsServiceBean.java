package jp.co.blueship.tri.dm.domainx.deploy.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.dm.constants.TimerSettings;
import jp.co.blueship.tri.dm.domain.beans.dop.dto.ReleasePackageViewBean;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 * @version V4.01.00
 * @author Akahoshi
 *
 */
public class FlowDeploymentJobDetailsServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private DeploymentJobDetailsView detailsView = new DeploymentJobDetailsView();

	public RequestParam getParam() {
		return param;
	}

	public DeploymentJobDetailsView getDetailsView() {
		return detailsView;
	}
	public FlowDeploymentJobDetailsServiceBean setDetailsView(DeploymentJobDetailsView detailsView) {
		this.detailsView = detailsView;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String mgtVer;
		private DeploymentJobDetailInputBean inputInfo = new DeploymentJobDetailInputBean();


		public String getSelectedMgtVer() {
			return mgtVer;
		}
		public RequestParam setSelectedMgtVer(String mgtVer) {
			this.mgtVer = mgtVer;
			return this;
		}

		public DeploymentJobDetailInputBean getInputInfo() {
			return inputInfo;
		}

		public void setInputInfo(DeploymentJobDetailInputBean inputInfo) {
			this.inputInfo = inputInfo;
		}
	}

	public class DeploymentJobDetailInputBean {
		private boolean manual = true;

		public boolean isSelectedManual(){
			return manual;
		}
		public DeploymentJobDetailInputBean setManual(boolean manual){
			this.manual = manual;
			return this;
		}
	}

	public class DeploymentJobDetailsView {
		private String deploymentId;
		private String mgtVer;
		private String summary;
		private String contents;
		private String envId;
		private String environmentNm;
		private String stsId;
		private String status;
		private String categoryNm;
		private String mstoneNm;
		private ReleasePackageViewBean releasePackage = new ReleasePackageViewBean();
		private TimerSettings timerSettings = TimerSettings.none;
		private String timerSettingsTime;
		private String timerSummary;
		private String timerContents;
		private String deploymentRegistedTime;
		private String deploymentAppliedTime;
		private String deploymentStsId;
		private String deploymentStatus;
		private boolean isJobCancelled = false;
		private boolean isEditEnabled = true;

		private List<String> warningMessages = new ArrayList<String>();

		public String getDeploymentId() {
			return deploymentId;
		}
		public DeploymentJobDetailsView setDeploymentId(String deploymentId) {
			this.deploymentId = deploymentId;
			return this;
		}

		public String getMgtVer() {
			return mgtVer;
		}
		public DeploymentJobDetailsView setMgtVer(String mgtVer) {
			this.mgtVer = mgtVer;
			return this;
		}

		public String getSummary() {
			return summary;
		}
		public DeploymentJobDetailsView setSummary(String summary) {
			this.summary = summary;
			return this;
		}

		public String getContents() {
			return contents;
		}
		public DeploymentJobDetailsView setContents(String contents) {
			this.contents = contents;
			return this;
		}

		public String getEnvId() {
			return envId;
		}
		public DeploymentJobDetailsView setEnvId(String envId) {
			this.envId = envId;
			return this;
		}

		public String getEnvironmentNm() {
			return environmentNm;
		}
		public DeploymentJobDetailsView setEnvironmentNm(String environmentNm) {
			this.environmentNm = environmentNm;
			return this;
		}

		public String getStsId() {
			return stsId;
		}
		public DeploymentJobDetailsView setStsId(String stsId) {
			this.stsId = stsId;
			return this;
		}

		public String getStatus() {
			return status;
		}
		public DeploymentJobDetailsView setStatus(String status) {
			this.status = status;
			return this;
		}

		public String getCategoryNm() {
			return categoryNm;
		}
		public DeploymentJobDetailsView setCategoryNm(String categoryNm) {
			this.categoryNm = categoryNm;
			return this;
		}

		public String getMstoneNm() {
			return mstoneNm;
		}
		public DeploymentJobDetailsView setMstoneNm(String mstoneNm) {
			this.mstoneNm = mstoneNm;
			return this;
		}

		public ReleasePackageViewBean getReleasePackage() {
			return releasePackage;
		}
		public DeploymentJobDetailsView setReleasePackage(ReleasePackageViewBean releasePackage) {
			this.releasePackage = releasePackage;
			return this;
		}

		public TimerSettings getTimerSettings() {
			return timerSettings;
		}
		public DeploymentJobDetailsView setTimerSettings(TimerSettings timerSettings) {
			this.timerSettings = timerSettings;
			return this;
		}

		public String getTimerSettingsTime() {
			return timerSettingsTime;
		}
		public DeploymentJobDetailsView setTimerSettingsTime(String timerSettingsTime) {
			this.timerSettingsTime = timerSettingsTime;
			return this;
		}

		public String getTimerSummary() {
			return timerSummary;
		}
		public DeploymentJobDetailsView setTimerSummary(String timerSummary) {
			this.timerSummary = timerSummary;
			return this;
		}

		public String getTimerContents() {
			return timerContents;
		}
		public DeploymentJobDetailsView setTimerContents(String timerContents) {
			this.timerContents = timerContents;
			return this;
		}

		public String getDeploymentRegistedTime() {
			return deploymentRegistedTime;
		}
		public DeploymentJobDetailsView setDeploymentRegistedTime(String deploymentRegistedTime) {
			this.deploymentRegistedTime = deploymentRegistedTime;
			return this;
		}

		public String getDeploymentAppliedTime() {
			return deploymentAppliedTime;
		}
		public DeploymentJobDetailsView setDeploymentAppliedTime(String deploymentAppliedTime) {
			this.deploymentAppliedTime = deploymentAppliedTime;
			return this;
		}

		public String getDeploymentStsId() {
			return deploymentStsId;
		}
		public DeploymentJobDetailsView setDeploymentStsId(String deploymentStsId) {
			this.deploymentStsId = deploymentStsId;
			return this;
		}

		public String getDeploymentStatus() {
			return deploymentStatus;
		}
		public DeploymentJobDetailsView setDeploymentStatus(String deploymentStatus) {
			this.deploymentStatus = deploymentStatus;
			return this;
		}

		public boolean isJobCancelled(){
			return isJobCancelled;
		}
		public DeploymentJobDetailsView setJobCancelled( boolean isJobCancelled ){
			this.isJobCancelled = isJobCancelled;
			return this;
		}

		public boolean isEditEnabled() {
			return isEditEnabled;
		}
		public DeploymentJobDetailsView setEditEnabled(boolean isEditEnabled) {
			this.isEditEnabled = isEditEnabled;
			return this;
		}

		public List<String> getWarningMessages(){
			return warningMessages;
		}
		public DeploymentJobDetailsView setWarningMessages( List<String> warningMessages ){
			this.warningMessages = warningMessages;
			return this;
		}

		public boolean hasWarningMessages(){

			if( warningMessages.size() == 0 ){
				return false;

			}else{
				return true;
			}
		}
	}

}
