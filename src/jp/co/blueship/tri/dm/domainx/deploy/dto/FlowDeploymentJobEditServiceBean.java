package jp.co.blueship.tri.dm.domainx.deploy.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.dm.constants.TimerSettings;
import jp.co.blueship.tri.dm.domain.beans.dop.dto.ReleasePackageViewBean;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 * @version V4.01.00
 * @author Akahoshi
 *
 */
public class FlowDeploymentJobEditServiceBean extends DomainServiceBean {
	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private DeploymentJobDetailsView detailsView = new DeploymentJobDetailsView();
	private RequestsCompletion result = new RequestsCompletion();

	public RequestParam getParam() {
		return param;
	}

	public RequestsCompletion getResult() {
		return result;
	}

	public FlowDeploymentJobEditServiceBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	public DeploymentJobDetailsView getDetailsView() {
		return detailsView;
	}
	public FlowDeploymentJobEditServiceBean setDetailsView(DeploymentJobDetailsView detailsView) {
		this.detailsView = detailsView;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String mgtVer;
		private DeploymentJobEditInputBean inputInfo = new DeploymentJobEditInputBean();
		private RequestOption requestOption = RequestOption.none;
		private SubmitOption submitOption = SubmitOption.none;

		public String getSelectedMgtVer() {
			return mgtVer;
		}
		public RequestParam setSelectedMgtVer(String mgtVer) {
			this.mgtVer = mgtVer;
			return this;
		}

		public DeploymentJobEditInputBean getInputInfo() {
			return inputInfo;
		}
		public RequestParam setInputInfo(DeploymentJobEditInputBean inputInfo) {
			this.inputInfo = inputInfo;
			return this;
		}

		public RequestOption getRequestOption() {
			return requestOption;
		}
		public RequestParam setRequestOption(RequestOption requestOption) {
			this.requestOption = requestOption;
			return this;
		}

		public SubmitOption getSubmitOption() {
			return submitOption;
		}
		public RequestParam setSubmitOption(SubmitOption submitOption) {
			this.submitOption = submitOption;
			return this;
		}
	}

	/**
	 * This is the class of enumeration types for 'onChange request'.
	 */
	public enum RequestOption {
		none( "" ),
		refreshCategory("refreshCategory"),
		refreshMilestone("refreshMilestone"),
		;

		private String value = null;

		private RequestOption( String value) {
			this.value = value;
		}

		public boolean equals( String value ) {
			RequestOption type = value( value );

			if ( null == type ) return false;
			if ( ! this.equals(type) ) return false;

			return true;
		}

		public String value() {
			return this.value;
		}

		public static RequestOption value( String value ) {
			if ( TriStringUtils.isEmpty( value ) ) {
				return null;
			}

			for ( RequestOption type : values() ) {
				if ( type.value().equals( value ) ) {
					return type;
				}
			}

			return none;
		}
	}

	/**
	 * This is the class of enumeration types for 'submitChanges request'.
	 */
	public enum SubmitOption {
		none,
		cancelTimer,
		submitChanges
		;

		public boolean equals( String value ) {
			SubmitOption type = value( value );

			if ( null == type ) return false;
			if ( ! this.equals(type) ) return false;

			return true;
		}
		public static SubmitOption value( String value ) {
			if ( TriStringUtils.isEmpty( value ) ) {
				return null;
			}

			for ( SubmitOption type : values() ) {
				if ( type.equals( value ) ) {
					return type;
				}
			}

			return none;
		}
	}

	/**
	 * Deployment Job Information
	 */
	public class DeploymentJobEditInputBean {
		private String summary;
		private String contents;
		private TimerSettings timerSettings = TimerSettings.none;
		private String timerDate;
		private String timerHour;
		private String timerMinute;
		private String timerSummary;
		private String timerContents;
		private boolean manual = true;
		private String ctgId;
		private String mstoneId;

		private List<ItemLabelsBean> ctgViews = new ArrayList<ItemLabelsBean>();
		private List<ItemLabelsBean> mstoneViews = new ArrayList<ItemLabelsBean>();

		public String getSummary() {
			return summary;
		}
		public DeploymentJobEditInputBean setSummary(String summary) {
			this.summary = summary;
			return this;
		}

		public String getContents() {
			return contents;
		}
		public DeploymentJobEditInputBean setContents(String contents) {
			this.contents = contents;
			return this;
		}

		public TimerSettings getTimerSettings() {
			return timerSettings;
		}
		public DeploymentJobEditInputBean setTimerSettings(TimerSettings timerSettings) {
			this.timerSettings = timerSettings;
			return this;
		}

		public String getTimerDate() {
			return timerDate;
		}
		public DeploymentJobEditInputBean setTimerDate(String timerDate) {
			this.timerDate = timerDate;
			return this;
		}

		public String getTimerHour() {
			return timerHour;
		}
		public DeploymentJobEditInputBean setTimerHour(String timerHour) {
			this.timerHour = timerHour;
			return this;
		}

		public String getTimerMinute() {
			return timerMinute;
		}
		public DeploymentJobEditInputBean setTimerMinute(String timerMinute) {
			this.timerMinute = timerMinute;
			return this;
		}

		public String getTimerSummary() {
			return timerSummary;
		}
		public DeploymentJobEditInputBean setTimerSummary(String timerSummary) {
			this.timerSummary = timerSummary;
			return this;
		}

		public String getTimerContents() {
			return timerContents;
		}
		public DeploymentJobEditInputBean setTimerContents(String timerContents) {
			this.timerContents = timerContents;
			return this;
		}

		public boolean isSelectedManual(){
			return manual;
		}
		public DeploymentJobEditInputBean setManual(boolean manual){
			this.manual = manual;
			return this;
		}

		public String getCtgId() {
			return ctgId;
		}
		public DeploymentJobEditInputBean setCtgId(String ctgId) {
			this.ctgId = ctgId;
			return this;
		}

		public String getMstoneId() {
			return mstoneId;
		}
		public DeploymentJobEditInputBean setMstoneId(String mstoneId) {
			this.mstoneId = mstoneId;
			return this;
		}

		public List<ItemLabelsBean> getCtgViews() {
			return ctgViews;
		}
		public DeploymentJobEditInputBean setCtgViews(List<ItemLabelsBean> ctgViews) {
			this.ctgViews = ctgViews;
			return this;
		}

		public List<ItemLabelsBean> getMstoneViews() {
			return mstoneViews;
		}
		public DeploymentJobEditInputBean setMstoneViews(List<ItemLabelsBean> mstoneViews) {
			this.mstoneViews = mstoneViews;
			return this;
		}
	}

	public class DeploymentJobDetailsView {
		private String deploymentId;
		private String mgtVer;
		private String envId;
		private String environmentNm;
		private String stsId;
		private String status;
		private String categoryNm;
		private String mstoneNm;
		private boolean isTimerConfigured = false;
		private ReleasePackageViewBean releasePackage = new ReleasePackageViewBean();

		public String getDeploymentId() {
			return deploymentId;
		}
		public DeploymentJobDetailsView setDeploymentId(String deploymentId) {
			this.deploymentId = deploymentId;
			return this;
		}

		public String getMgtVer() {
			return mgtVer;
		}
		public DeploymentJobDetailsView setMgtVer(String mgtVer) {
			this.mgtVer = mgtVer;
			return this;
		}

		public String getEnvId() {
			return envId;
		}
		public DeploymentJobDetailsView setEnvId(String envId) {
			this.envId = envId;
			return this;
		}

		public String getEnvironmentNm() {
			return environmentNm;
		}
		public DeploymentJobDetailsView setEnvironmentNm(String environmentNm) {
			this.environmentNm = environmentNm;
			return this;
		}

		public String getStsId() {
			return stsId;
		}
		public DeploymentJobDetailsView setStsId(String stsId) {
			this.stsId = stsId;
			return this;
		}

		public String getStatus() {
			return status;
		}
		public DeploymentJobDetailsView setStatus(String status) {
			this.status = status;
			return this;
		}

		public String getCategoryNm() {
			return categoryNm;
		}
		public DeploymentJobDetailsView setCategoryNm(String categoryNm) {
			this.categoryNm = categoryNm;
			return this;
		}

		public String getMstoneNm() {
			return mstoneNm;
		}
		public DeploymentJobDetailsView setMstoneNm(String mstoneNm) {
			this.mstoneNm = mstoneNm;
			return this;
		}

		public boolean isTimerConfigured() {
			return isTimerConfigured;
		}
		public DeploymentJobDetailsView setTimerConfigured( boolean isTimerConfigured ) {
			this.isTimerConfigured = isTimerConfigured;
			return this;
		}

		public ReleasePackageViewBean getReleasePackage() {
			return releasePackage;
		}
		public DeploymentJobDetailsView setReleasePackage(ReleasePackageViewBean releasePackage) {
			this.releasePackage = releasePackage;
			return this;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private String deploymentId;
		private boolean completed = false;

		public String getDeploymentId() {
			return deploymentId;
		}
		public RequestsCompletion setDeploymentId(String deploymentId) {
			this.deploymentId = deploymentId;
			return this;
		}

		public boolean isCompleted() {
			return completed;
		}
		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}
}
