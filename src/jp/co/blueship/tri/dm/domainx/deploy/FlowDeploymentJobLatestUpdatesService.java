package jp.co.blueship.tri.dm.domainx.deploy;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.CopyOnWriteArrayList;
import jp.co.blueship.tri.agent.dm.opt.beans.dto.DealAssetServiceBean;
import jp.co.blueship.tri.agent.dm.opt.beans.dto.DealAssetServiceResponseBean;
import jp.co.blueship.tri.agent.dm.opt.beans.dto.DealAssetStatusBean;
import jp.co.blueship.tri.bm.dao.bldenv.constants.BldEnvItems;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvEntity;
import jp.co.blueship.tri.dm.DmDesignBusinessRuleUtils;
import jp.co.blueship.tri.dm.constants.DrmsStatusId;
import jp.co.blueship.tri.dm.dao.dmdo.eb.IDmDoEntity;
import jp.co.blueship.tri.dm.domain.beans.CommercialShellID;
import jp.co.blueship.tri.dm.domain.beans.dop.dto.CommercialShellBean;
import jp.co.blueship.tri.dm.domainx.deploy.dto.FlowDeploymentJobLatestUpdatesServiceBean;
import jp.co.blueship.tri.dm.domainx.deploy.dto.FlowDeploymentJobLatestUpdatesServiceBean.DeploymentJobLatestUpdatesView;
import jp.co.blueship.tri.dm.support.FlowRelDistributeEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.DmDesignEntryKeyByCommercialShell;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.DmMessageId;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.rm.RmDBSearchConditionAddonUtils;
/**
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 * @version V4.01.00
 * @author Cuong Nguyen
 */
public class FlowDeploymentJobLatestUpdatesService implements IDomain<FlowDeploymentJobLatestUpdatesServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();
	private FlowRelDistributeEditSupport support = null;
	public void setSupport(FlowRelDistributeEditSupport support) {
		this.support = support;
	}
	private List<IDomain<IGeneralServiceBean>> actions = new ArrayList<IDomain<IGeneralServiceBean>>();
	private List<IDomain<IGeneralServiceBean>> makeShellActions = new ArrayList<IDomain<IGeneralServiceBean>>();
	public void setActions(List<IDomain<IGeneralServiceBean>> actions) {
		this.actions = actions;
	}
	public void setMakeShellActions(List<IDomain<IGeneralServiceBean>> actions) {
		this.makeShellActions = actions;
	}
	@Override
	public IServiceDto<FlowDeploymentJobLatestUpdatesServiceBean> execute(
			IServiceDto<FlowDeploymentJobLatestUpdatesServiceBean> serviceDto) {
		FlowDeploymentJobLatestUpdatesServiceBean paramBean = serviceDto.getServiceBean();
		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");
			String lotId = paramBean.getParam().getSelectedLotId();
			PreConditions.assertOf(TriStringUtils.isNotEmpty(lotId), "SelectedLotId is not specified");
			if ( RequestType.init.equals(paramBean.getParam().getRequestType()) )
				this.init( paramBean );
			return serviceDto;
		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(DmMessageId.DM005025S, e, paramBean.getFlowAction());
		}
	}
	private void init(FlowDeploymentJobLatestUpdatesServiceBean paramBean){
		List<IBldEnvEntity> envList = this.getReleaseEnvs();
		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]>	messageArgsList	= new ArrayList<String[]>();
		List<String> envNames = new ArrayList<String>();
		for (IBldEnvEntity environment : envList) {
			envNames.add(environment.getBldEnvNm());
		}
		List<DeploymentJobLatestUpdatesView> views = new ArrayList<DeploymentJobLatestUpdatesView>();
		if ( !paramBean.getParam().getInputInfo().isSelectedManual() ) {
			List<DealAssetStatusBean> latestDeploymentBeans = this.getLatestDeploymentStatus(paramBean);
			Map<String, DealAssetStatusBean> latestDeploymentBeanMap  = new HashMap<>();
			
			{
				boolean duplicateEnv = false;
				for (DealAssetStatusBean dealAssetStatusBean : latestDeploymentBeans) {
					boolean isValid = true;
					String envName = dealAssetStatusBean.getTargetEnvironment();
					IDmDoEntity dmDoEntity = support.findDmDoEntity(dealAssetStatusBean.getDrmsManagementNo());

					if ( TriStringUtils.isEmpty(dmDoEntity) ) {
						messageList.add(DmMessageId.DM001024E);
						messageArgsList.add(new String[] { dealAssetStatusBean.getDrmsManagementNo() });
						isValid = false;
					}

					if ( ! envNames.contains( envName ) ) {
						messageList.add(DmMessageId.DM001020E);
						messageArgsList.add(new String[]{ envName });
						isValid = false;
					}

					if ( isValid ) {
						 if ( ! latestDeploymentBeanMap.containsKey( envName ) ) {
								latestDeploymentBeanMap.put(envName, dealAssetStatusBean);
						} else {
							if ( latestDeploymentBeanMap.get(envName).getDealDate()
									.compareTo(dealAssetStatusBean.getDealDate() ) < 0 ) {
								latestDeploymentBeanMap.put(envName, dealAssetStatusBean);
							}
							duplicateEnv = true;
						}
					}
				}
				if ( duplicateEnv ) {
					messageList.add(DmMessageId.DM001021E);
					messageArgsList.add(new String[]{});
				}
			}
			
			for (String envName : latestDeploymentBeanMap.keySet()) {
				DealAssetStatusBean dealAssetStatusBean = latestDeploymentBeanMap.get(envName);
				IDmDoEntity dmDoEntity = support.findDmDoEntity(dealAssetStatusBean.getDrmsManagementNo());
				DeploymentJobLatestUpdatesView view = new FlowDeploymentJobLatestUpdatesServiceBean().new DeploymentJobLatestUpdatesView();
				view.setDeploymentId			( dmDoEntity.getDeploymentId() );
				view.setMgtVer					( dmDoEntity.getMgtVer() );
				view.setRpId					( dmDoEntity.getRpId() );
				view.setEnvironmentNm			( envName );
				view.setStsId					( dmDoEntity.getProcStsId() );
				view.setTimerSettingsTime		( TriDateUtils.convertViewDateFormat(
													TriDateUtils.convertDateFormat(
															dmDoEntity.getTimerSettingDate(),
															dmDoEntity.getTimerSettingTime()))
												);
				view.setCreatedBy				( dmDoEntity.getRegUserNm() );
				view.setCreatedByIconPath		( this.support.getUmFinderSupport().getIconPath(dmDoEntity.getRegUserId() ) );
				String inputDate = this.support.convertViewDateFormat(
						dealAssetStatusBean.getRegistrationDate(), TriDateUtils.getViewDateFormat() );
				view.setDeploymentRegistedTime(inputDate);
				String applyDate = this.support.convertViewDateFormat(
						dealAssetStatusBean.getDealDate(),  TriDateUtils.getViewDateFormat() );
				view.setDeploymentAppliedTime(applyDate);
				view.setDeploymentStatus( DrmsStatusId.APPLY.getValue() );
				view.setDeploymentStsId( DrmsStatusId.APPLY.name() );
				views.add(view);
			}
		} else {
			IContextAdapter contextAdapter = ContextAdapterFactory.getContextAdapter();
			String shellOutputPath = DmDesignBusinessRuleUtils.getShellOutputPath().getAbsolutePath();
			paramBean.getDetailsView().getWarningMessages().add( contextAdapter.getMessage(paramBean.getLanguage() , false , DmMessageId.DM002000W) );
			paramBean.getDetailsView().getWarningMessages()
					.add( contextAdapter.getMessage(paramBean.getLanguage() , false , DmMessageId.DM003005I, shellOutputPath ) );
			this.outputCommercialShell( paramBean );
		}
		paramBean.setJobViews(views);
		if ( 0 != messageList.size() ) {
			throw new ContinuableBusinessException( messageList, messageArgsList );
		}
			
	}
	private List<DealAssetStatusBean> getLatestDeploymentStatus( FlowDeploymentJobLatestUpdatesServiceBean paramBean ) {
		int returnCode = 0;
		DealAssetServiceBean dealBean = new DealAssetServiceBean();
		List<Object> paramList = new CopyOnWriteArrayList<Object>() ;
		List<DealAssetStatusBean> statusBeanList = null;
		paramList.add(dealBean);
		DealAssetServiceResponseBean resBean = null;
		IServiceDto<IGeneralServiceBean> innerServiceDto = new ServiceDto<IGeneralServiceBean>()
				.setServiceBean( paramBean )
				.setParamList( paramList );
		// 下層のアクションを実行する
		for ( IDomain<IGeneralServiceBean> action : actions ) {
			action.execute( innerServiceDto );
		}
		resBean = this.support.getDealAssetServiceResponseBean( paramList ) ;
		if ( TriStringUtils.isEmpty( resBean ) ) {
			throw new ContinuableBusinessException( DmMessageId.DM001006E );
		} else {
			returnCode = resBean.getReturnCode();
			statusBeanList = resBean.getDealStatusList();
		}
		if ( returnCode != 0 ) {
			if ( 1 == returnCode ) {
				throw new ContinuableBusinessException( DmMessageId.DM001007E );
			} else {
				throw new ContinuableBusinessException( DmMessageId.DM001011E );
			}
		}
		return statusBeanList;
	}
	private List<IBldEnvEntity> getReleaseEnvs() {
		IJdbcCondition condition = RmDBSearchConditionAddonUtils.getActiveReleaseEnvCondition();
		ISqlSort sort = new SortBuilder();
		sort.setElement(BldEnvItems.bldEnvNm, TriSortOrder.Desc, 1);
		List<IBldEnvEntity> envList = this.support.getBmFinderSupport().getBldEnvDao().find(condition.getCondition(), sort);
		return envList;
	}
	private void outputCommercialShell( FlowDeploymentJobLatestUpdatesServiceBean paramBean ) {
		{
			List<Object> paramList = new CopyOnWriteArrayList<Object>() ;
			CommercialShellBean shellBean = new CommercialShellBean();
			shellBean.setShellId(DmDesignEntryKeyByCommercialShell.shellAssetDealStatusChk);
			String shellFileName = this.support.makeShellFileName( CommercialShellID.SHELL_ASSET_DEAL_STATUS_CHK , "" /*envEntity.getEnvNo()*/ ) ;
			shellBean.setShellFileName(shellFileName);
			Properties properties = new Properties() ;
			shellBean.setProperties(properties);
			paramList.add(shellBean);
			IServiceDto<IGeneralServiceBean> innerServiceDto = new ServiceDto<IGeneralServiceBean>()
					.setParamList( paramList );
			for ( IDomain<IGeneralServiceBean> action : makeShellActions ) {
				action.execute( innerServiceDto );
			}
		}
	}
}