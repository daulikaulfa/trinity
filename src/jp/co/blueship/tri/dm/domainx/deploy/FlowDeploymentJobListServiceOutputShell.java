package jp.co.blueship.tri.dm.domainx.deploy;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.CopyOnWriteArrayList;

import jp.co.blueship.tri.bm.dao.bldenv.eb.BldEnvCondition;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvEntity;
import jp.co.blueship.tri.dm.DmDesignBusinessRuleUtils;
import jp.co.blueship.tri.dm.constants.RelDistributeCommercialShellPropertyDefineID;
import jp.co.blueship.tri.dm.domain.beans.CommercialShellID;
import jp.co.blueship.tri.dm.domain.beans.dop.dto.CommercialShellBean;
import jp.co.blueship.tri.dm.domainx.deploy.dto.FlowDeploymentJobListServiceOutputShellBean;
import jp.co.blueship.tri.dm.support.FlowRelDistributeEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.DmDesignEntryKeyByCommercialShell;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.DmMessageId;
import jp.co.blueship.tri.fw.msg.RmMessageId;

/**
*
* @version V4.01.00
* @author Cuong Nguyen
*
*/

public class FlowDeploymentJobListServiceOutputShell implements IDomain<FlowDeploymentJobListServiceOutputShellBean> {

	private static final ILog log = TriLogFactory.getInstance();
	
	private FlowRelDistributeEditSupport support = null;	
	public void setSupport(FlowRelDistributeEditSupport support) {
		this.support = support;
	}
	
	private List<IDomain<IGeneralServiceBean>> makeShellActions = new ArrayList<IDomain<IGeneralServiceBean>>();
	public void setMakeShellActions(List<IDomain<IGeneralServiceBean>> actions) {
		this.makeShellActions = actions;
	}
	
	@Override
	public IServiceDto<FlowDeploymentJobListServiceOutputShellBean> execute(
			IServiceDto<FlowDeploymentJobListServiceOutputShellBean> serviceDto) {
		
		FlowDeploymentJobListServiceOutputShellBean paramBean = serviceDto.getServiceBean();

		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");

			if ( RequestType.submitChanges.equals(paramBean.getParam().getRequestType()) ) {
				this.submitChanges( paramBean );
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(DmMessageId.DM005025S, e, paramBean.getFlowAction());
		}
	}
	
	private void submitChanges(FlowDeploymentJobListServiceOutputShellBean paramBean){
		{
			this.outputCommercialShell(paramBean);
		}
	}
	
	private void outputCommercialShell( FlowDeploymentJobListServiceOutputShellBean paramBean ) {
		String envNo = paramBean.getParam().getSelectedEnvId();
		if ( TriStringUtils.isEmpty(envNo) ) {
			throw new ContinuableBusinessException( RmMessageId.RM001021E );
		}
		
		List<Object> paramList = new CopyOnWriteArrayList<Object>() ;

		CommercialShellBean shellBean = new CommercialShellBean();
		shellBean.setShellId(DmDesignEntryKeyByCommercialShell.shellRegHistory);
		BldEnvCondition condition = new BldEnvCondition();
		condition.setBldEnvId(envNo);
		IBldEnvEntity envEntity = this.support.getBmFinderSupport().getBldEnvDao().find( condition.getCondition() ).get(0);

		String shellFileName = this.support.makeShellFileName( CommercialShellID.SHELL_REG_HISTORY , envEntity.getBldEnvId() ) ;
		shellBean.setShellFileName( shellFileName );
		Properties properties = new Properties() ;
		properties.put( RelDistributeCommercialShellPropertyDefineID.GROUP_NAME,
				envEntity.getBldEnvNm() ) ;
		shellBean.setProperties(properties);
		paramList.add(shellBean);

		IServiceDto<IGeneralServiceBean> innerServiceDto = new ServiceDto<IGeneralServiceBean>()
				.setParamList( paramList );

		for ( IDomain<IGeneralServiceBean> action : makeShellActions ) {
			action.execute( innerServiceDto );
		}
		
		paramBean.getResult().setCompleted(true);
		paramBean.getMessageInfo().addFlashTranslatable( DmMessageId.DM003005I, DmDesignBusinessRuleUtils.getShellOutputPath().getAbsolutePath() );
	}

}
