package jp.co.blueship.tri.dm.domainx.deploy.dto;

import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;


/**
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 */
public class FlowDeploymentJobCancellationServiceBean  extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private RequestsCompletion result = new RequestsCompletion();

	public RequestParam getParam() {
		return param;
	}

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowDeploymentJobCancellationServiceBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String mgtVer;
		private DeploymentJobCancellationInputInfo inputInfo = new DeploymentJobCancellationInputInfo();

		public String getSelectedMgtVer() {
			return mgtVer;
		}
		public RequestParam setSelectedMgtVer(String mgtVer) {
			this.mgtVer = mgtVer;
			return this;
		}

		public DeploymentJobCancellationInputInfo getInputInfo(){
			return inputInfo;
		}
		public RequestParam setInputInfo(DeploymentJobCancellationInputInfo inputInfo){
			this.inputInfo = inputInfo;
			return this;

		}
	}

	/**
	 * Information
	 */
	public class DeploymentJobCancellationInputInfo {
		private boolean manual = true;

		public boolean isSelectedManual() {
			return manual;
		}
		public DeploymentJobCancellationInputInfo setManual(boolean manual) {
			this.manual = manual;
			return this;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private boolean completed = false;

		public boolean isCompleted() {
			return completed;
		}
		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}
}
