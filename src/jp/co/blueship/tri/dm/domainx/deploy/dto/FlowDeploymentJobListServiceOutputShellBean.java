package jp.co.blueship.tri.dm.domainx.deploy.dto;

import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;

/**
 *
 * @version V4.01.00
 * @author Akahoshi
 *
 */
public class FlowDeploymentJobListServiceOutputShellBean extends DomainServiceBean {
	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private RequestsCompletion result = new RequestsCompletion();

	public RequestParam getParam() {
		return this.param;
	}
	public FlowDeploymentJobListServiceOutputShellBean getParam( RequestParam param ) {
		this.param = param;
		return this;
	}

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowDeploymentJobListServiceOutputShellBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}


	public class RequestParam extends DomainServiceBean.RequestParam {
		private String envId;

		public String getSelectedEnvId() {
			return this.envId;
		}
		public RequestParam setSelectedEnvId( String envId ) {
			this.envId = envId;
			return this;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private boolean completed = false;

		public boolean isCompleted() {
			return completed;
		}
		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}
}
