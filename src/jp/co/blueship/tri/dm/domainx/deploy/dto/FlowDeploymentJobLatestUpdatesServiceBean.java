package jp.co.blueship.tri.dm.domainx.deploy.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.dm.constants.TimerSettings;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 * @version V4.01.00
 * @author Akahoshi
 *
 */
public class FlowDeploymentJobLatestUpdatesServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();

	private DeploymentJobLatestUpdatesDetailsView detailsView = new DeploymentJobLatestUpdatesDetailsView();
	private List<DeploymentJobLatestUpdatesView> jobViews = new ArrayList<DeploymentJobLatestUpdatesView>();

	public RequestParam getParam() {
		return param;
	}

	public DeploymentJobLatestUpdatesDetailsView getDetailsView() {
		return detailsView;
	}
	public FlowDeploymentJobLatestUpdatesServiceBean setDetailsView(DeploymentJobLatestUpdatesDetailsView detailsView) {
		this.detailsView = detailsView;
		return this;
	}

	public List<DeploymentJobLatestUpdatesView> getJobViews() {
		return jobViews;
	}
	public FlowDeploymentJobLatestUpdatesServiceBean setJobViews(List<DeploymentJobLatestUpdatesView> jobViews) {
		this.jobViews = jobViews;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String lotId;
		private DeploymentJobLatestUpdatesInputBean inputInfo = new DeploymentJobLatestUpdatesInputBean();

		public String getSelectedLotId() {
			return lotId;
		}
		public RequestParam setSelectedLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}

		public DeploymentJobLatestUpdatesInputBean getInputInfo() {
			return inputInfo;
		}
		public RequestParam setInputInfo(DeploymentJobLatestUpdatesInputBean inputInfo) {
			this.inputInfo = inputInfo;
			return this;
		}
	}

	public class DeploymentJobLatestUpdatesInputBean {
		private boolean manual = true;

		public boolean isSelectedManual() {
			return manual;
		}
		public DeploymentJobLatestUpdatesInputBean setManual(boolean manual) {
			this.manual = manual;
			return this;
		}
	}

	public class DeploymentJobLatestUpdatesDetailsView {
		private List<String> warningMessages = new ArrayList<String>();

		public List<String> getWarningMessages(){
			return warningMessages;
		}
		public DeploymentJobLatestUpdatesDetailsView setWarningMessages( List<String> warningMessages ){
			this.warningMessages = warningMessages;
			return this;
		}

		public boolean hasWarningMessages(){

			if( warningMessages.size() == 0 ){
				return false;

			}else{
				return true;
			}
		}
	}

	public class DeploymentJobLatestUpdatesView {
		private String deploymentId;
		private String mgtVer;
		private String rpId;
		private String envId;
		private String environmentNm;
		private String stsId;
		private String status;
		private String createdBy;
		private String createdByIconPath;
		private TimerSettings timerSettings = TimerSettings.none;
		private String timerSettingsTime;
		private String deploymentRegistedTime;
		private String deploymentAppliedTime;
		private String deploymentStsId;
		private String deploymentStatus;

		public String getDeploymentId() {
			return deploymentId;
		}
		public DeploymentJobLatestUpdatesView setDeploymentId(String deploymentId) {
			this.deploymentId = deploymentId;
			return this;
		}
		public String getMgtVer() {
			return mgtVer;
		}
		public DeploymentJobLatestUpdatesView setMgtVer(String mgtVer) {
			this.mgtVer = mgtVer;
			return this;
		}
		public String getRpId() {
			return rpId;
		}
		public DeploymentJobLatestUpdatesView setRpId(String rpId) {
			this.rpId = rpId;
			return this;
		}
		public String getEnvId() {
			return envId;
		}
		public DeploymentJobLatestUpdatesView setEnvId(String envId) {
			this.envId = envId;
			return this;
		}
		public String getEnvironmentNm() {
			return environmentNm;
		}
		public DeploymentJobLatestUpdatesView setEnvironmentNm(String environmentNm) {
			this.environmentNm = environmentNm;
			return this;
		}
		public String getStsId() {
			return stsId;
		}
		public DeploymentJobLatestUpdatesView setStsId(String stsId) {
			this.stsId = stsId;
			return this;
		}
		public String getStatus() {
			return status;
		}
		public DeploymentJobLatestUpdatesView setStatus(String status) {
			this.status = status;
			return this;
		}
		public String getCreatedBy() {
			return createdBy;
		}
		public DeploymentJobLatestUpdatesView setCreatedBy(String createdBy) {
			this.createdBy = createdBy;
			return this;
		}
		public String getCreatedByIconPath() {
			return createdByIconPath;
		}
		public DeploymentJobLatestUpdatesView setCreatedByIconPath(String createdByIconPath) {
			this.createdByIconPath = createdByIconPath;
			return this;
		}
		public TimerSettings getTimerSettings() {
			return timerSettings;
		}
		public DeploymentJobLatestUpdatesView setTimerSettings(TimerSettings timerSettings) {
			this.timerSettings = timerSettings;
			return this;
		}
		public String getTimerSettingsTime() {
			return timerSettingsTime;
		}
		public DeploymentJobLatestUpdatesView setTimerSettingsTime(String timerSettingsTime) {
			this.timerSettingsTime = timerSettingsTime;
			return this;
		}

		public String getDeploymentRegistedTime() {
			return deploymentRegistedTime;
		}
		public DeploymentJobLatestUpdatesView setDeploymentRegistedTime(String deploymentRegistedTime) {
			this.deploymentRegistedTime = deploymentRegistedTime;
			return this;
		}
		public String getDeploymentAppliedTime() {
			return deploymentAppliedTime;
		}
		public DeploymentJobLatestUpdatesView setDeploymentAppliedTime(String deploymentAppliedTime) {
			this.deploymentAppliedTime = deploymentAppliedTime;
			return this;
		}
		public String getDeploymentStsId() {
			return deploymentStsId;
		}
		public DeploymentJobLatestUpdatesView setDeploymentStsId(String deploymentStsId) {
			this.deploymentStsId = deploymentStsId;
			return this;
		}
		public String getDeploymentStatus() {
			return deploymentStatus;
		}
		public DeploymentJobLatestUpdatesView setDeploymentStatus(String deploymentStatus) {
			this.deploymentStatus = deploymentStatus;
			return this;
		}
	}

}
