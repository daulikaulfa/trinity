package jp.co.blueship.tri.dm.domainx.deploy;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

import jp.co.blueship.tri.agent.dm.opt.beans.dto.DealAssetRegistrationHistoryBean;
import jp.co.blueship.tri.agent.dm.opt.beans.dto.DealAssetServiceBean;
import jp.co.blueship.tri.agent.dm.opt.beans.dto.DealAssetServiceResponseBean;
import jp.co.blueship.tri.bm.dao.bldenv.constants.BldEnvItems;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvEntity;
import jp.co.blueship.tri.dm.DmDesignBusinessRuleUtils;
import jp.co.blueship.tri.dm.constants.DrmsApplyResultId;
import jp.co.blueship.tri.dm.constants.DrmsStatusId;
import jp.co.blueship.tri.dm.dao.constants.DmDoItems;
import jp.co.blueship.tri.dm.dao.dmdo.eb.DmDoCondition;
import jp.co.blueship.tri.dm.dao.dmdo.eb.IDmDoEntity;
import jp.co.blueship.tri.dm.domainx.deploy.dto.FlowDeploymentJobListServiceBean;
import jp.co.blueship.tri.dm.domainx.deploy.dto.FlowDeploymentJobListServiceBean.DeploymentInfoView;
import jp.co.blueship.tri.dm.domainx.deploy.dto.FlowDeploymentJobListServiceBean.DeploymentJobListView;
import jp.co.blueship.tri.dm.domainx.deploy.dto.FlowDeploymentJobListServiceBean.RequestOption;
import jp.co.blueship.tri.dm.domainx.deploy.dto.FlowDeploymentJobListServiceBean.SearchCondition;
import jp.co.blueship.tri.dm.support.FlowRelDistributeEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.cmn.utils.function.TriFunction;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.status.DmDoStatusId;
import jp.co.blueship.tri.fw.dao.orm.DBSearchConditionAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.DmMessageId;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.svc.beans.dto.IPageNoInfo;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;
import jp.co.blueship.tri.fw.svc.beans.dto.PageNoInfo;
import jp.co.blueship.tri.fw.um.UmFluentFunctionUtils;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.ICtgEntity;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.IMstoneEntity;
import jp.co.blueship.tri.rm.RmDBSearchConditionAddonUtils;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.RpCondition;

/**
 *
 * @version V4.00.00
 * @author Akahoshi
 * 
 * @version V4.01.00
 * @author Cuong Nguyen
 */
public class FlowDeploymentJobListService implements IDomain<FlowDeploymentJobListServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowRelDistributeEditSupport support = null;	
	public void setSupport(FlowRelDistributeEditSupport support) {
		this.support = support;
	}
	
	private List<IDomain<IGeneralServiceBean>> actions = new ArrayList<IDomain<IGeneralServiceBean>>();


	public void setActions(List<IDomain<IGeneralServiceBean>> actions) {
		this.actions = actions;
	}

	@Override
	public IServiceDto<FlowDeploymentJobListServiceBean> execute(
			IServiceDto<FlowDeploymentJobListServiceBean> serviceDto) {

		FlowDeploymentJobListServiceBean paramBean = serviceDto.getServiceBean();
		try {


			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");
			String lotId = paramBean.getParam().getSelectedLotId();
			PreConditions.assertOf(TriStringUtils.isNotEmpty(lotId), "SelectedLotId is not specified");

			if ( RequestType.init.equals(paramBean.getParam().getRequestType()) ) {
				this.init( paramBean );

			} else if ( RequestType.onChange.equals(paramBean.getParam().getRequestType()) ) {
				this.onChange( paramBean );
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(DmMessageId.DM005025S, e, paramBean.getFlowAction());
		}
	}

	private void init(FlowDeploymentJobListServiceBean paramBean){

		SearchCondition searchCondition	= paramBean.getParam().getSearchCondition();
		String lotId= paramBean.getParam().getSelectedLotId();

		this.generateDropBox(lotId, searchCondition);

		this.onChange(paramBean);
	}


	private void onChange(FlowDeploymentJobListServiceBean paramBean){
		{
			List<DeploymentJobListView> deploymentJobListViews = getDeploymentJobViews(paramBean);
			paramBean.setJobViews(deploymentJobListViews);
		}
	}
	
	private List<DeploymentJobListView> getDeploymentJobViews ( FlowDeploymentJobListServiceBean paramBean){
		
		IEntityLimit<IRpEntity> limit = this.getRelEntityLimit( paramBean , this.getRpIds(paramBean) );
		List<IRpEntity> entities = limit.getEntities();
	
		IPageNoInfo page = DmDesignBusinessRuleUtils.convertPageNoInfo(new PageNoInfo(), limit.getLimit());
		paramBean.setPage( page );
				
		setDeploymentInfoCache( paramBean );
		
		List<DeploymentJobListView> views = new ArrayList<DeploymentJobListView>();

		for(IRpEntity rpEntity : entities){
			IDmDoEntity dmDoEntity = this.support.getDmDoEntity(rpEntity);
			IBldEnvEntity bldEnvEntity = this.support.getBmFinderSupport().findBldEnvEntity(rpEntity.getBldEnvId());

			DeploymentJobListView view = new FlowDeploymentJobListServiceBean().new DeploymentJobListView();
			view.setDeploymentId			( dmDoEntity.getDeploymentId() );
			view.setEnvId					( rpEntity.getBldEnvId());
			view.setEnvironmentNm			( bldEnvEntity.getBldEnvNm() );
			view.setRpId					( rpEntity.getRpId() );
			view.setMgtVer					( dmDoEntity.getMgtVer() );
			view.setCreatedBy				( dmDoEntity.getRegUserNm() );
			view.setCreatedByIconPath		( this.support.getUmFinderSupport().getIconPath( dmDoEntity.getRegUserId() ) );

			view.setStsId					( dmDoEntity.getProcStsId() );
			view.setTimerSettingsTime		( TriDateUtils.convertViewDateFormat(
												TriDateUtils.convertDateFormat(
														dmDoEntity.getTimerSettingDate(),
														dmDoEntity.getTimerSettingTime()))
											);
			view.setJobCancelled			( DmDoStatusId.JobCancelled.getStatusId().equals( dmDoEntity.getStsId() ));
			
			if ( !paramBean.getParam().getInputInfo().isSelectedManual() ) {
				DeploymentInfoView deploymentInfoView = paramBean.getCacheDeploymentInfo().get( view.getMgtVer() );
				if( TriStringUtils.isNotEmpty(deploymentInfoView) ) {
					view.setDeploymentAppliedTime(this.support.convertViewDateFormat(
									deploymentInfoView.getDeploymentAppliedTime(), TriDateUtils.getViewDateFormat()) );
					view.setDeploymentRegistedTime(this.support.convertViewDateFormat(
							deploymentInfoView.getDeploymentRegistedTime(), TriDateUtils.getViewDateFormat() ) );
					view.setDeploymentStatus( deploymentInfoView.getDeploymentStatus() );
					view.setDeploymentStsId( deploymentInfoView.getDeploymentStsId() );
				}
			}
						
			views.add(view);
		}

		return views;
	}

	private IEntityLimit<IRpEntity> getRelEntityLimit(FlowDeploymentJobListServiceBean paramBean , String[] rpIds){

		String lotId = paramBean.getParam().getSelectedLotId();
		String envId = paramBean.getParam().getSearchCondition().getEnvId();
		int pageNo	 = paramBean.getParam().getSearchCondition().getSelectedPageNo();
		int maxPage	 = paramBean.getParam().getLinesPerPage();
		
		if(TriStringUtils.isEmpty(envId)) {
			envId = "";
		}
		IJdbcCondition condition = DBSearchConditionAddonUtil.getRpConditionByEnvNoRpIds(envId, rpIds);
		((RpCondition)condition).setLotId( lotId );

		ISqlSort sort = new SortBuilder();
		sort.setElement(DmDoItems.regTimestamp, TriSortOrder.Desc, 1);

		IEntityLimit<IRpEntity> limit =
				this.support.getRpDao().find( condition.getCondition() , sort , pageNo , maxPage);

		return limit;
		
	}


	private String[] getRpIds(FlowDeploymentJobListServiceBean paramBean){
		List<IDmDoEntity> dmDoEntityList = this.support.getDmDoDao().find( this.getDmDoCondition( paramBean ).getCondition() );

		List<String> rpIds = new ArrayList<String>();

		for ( IDmDoEntity entity : dmDoEntityList )
			rpIds.add(entity.getRpId());

		return rpIds.toArray(new String[0]);
	}


	private DmDoCondition getDmDoCondition(FlowDeploymentJobListServiceBean paramBean ){
		DmDoCondition dmCondition = new DmDoCondition();

		SearchCondition searchCondition = paramBean.getParam().getSearchCondition();


		if(TriStringUtils.isNotEmpty(searchCondition.getStsId())){
			String status [] = new String[] {searchCondition.getStsId()};
			dmCondition.setProcStsIds(status);

		}else{
			String status [] = new String[] { DmDoStatusId.JobRegistered.getStatusId() ,
											  DmDoStatusId.TimerConfigured.getStatusId(),
											  DmDoStatusId.JobCancelled.getStatusId() } ;
			dmCondition.setProcStsIds(status);
		}

		if(TriStringUtils.isNotEmpty(searchCondition.getCtgId()))
			dmCondition.setCtgId(searchCondition.getCtgId());

		if(TriStringUtils.isNotEmpty(searchCondition.getMstoneId()))
			dmCondition.setMstoneId(searchCondition.getMstoneId());

		if(TriStringUtils.isNotEmpty(searchCondition.getKeyword()))
			dmCondition.setContainsByKeyword(searchCondition.getKeyword());

		return dmCondition;
	}


	private void generateDropBox( String lotId , SearchCondition searchCondition){

		List<ItemLabelsBean> releaseEnvView = this.getReleaseEnvLabels();
		searchCondition.setReleaseEnvViews	( releaseEnvView );

		List<ICtgEntity> ctgEntities = this.support.getUmFinderSupport().findCtgByLotId(lotId);
		searchCondition.setCtgViews(FluentList.from( ctgEntities ).map( UmFluentFunctionUtils.toItemLabelsFromCtgEntity ).asList());

		List<IMstoneEntity> mstoneEntities = this.support.getUmFinderSupport().findMstoneByLotId(lotId);
		searchCondition.setMstoneViews	(FluentList.from( mstoneEntities ).map( UmFluentFunctionUtils.toItemLabelsFromMstoneEntity ).asList());

		List<ItemLabelsBean> statusViews = new ArrayList<ItemLabelsBean>();
		statusViews.add(new ItemLabelsBean("" , DmDoStatusId.JobRegistered.getStatusId()));
		statusViews.add(new ItemLabelsBean("" , DmDoStatusId.TimerConfigured.getStatusId()));
		statusViews.add(new ItemLabelsBean("" , DmDoStatusId.JobCancelled.getStatusId()));

		searchCondition.setStatusViews	(statusViews);
	}


	private List<ItemLabelsBean> getReleaseEnvLabels() {

		IJdbcCondition condition = RmDBSearchConditionAddonUtils.getActiveReleaseEnvCondition();
		ISqlSort sort = new SortBuilder();
		sort.setElement(BldEnvItems.bldEnvNm, TriSortOrder.Asc, 1);

		List<IBldEnvEntity> envList = this.support.getBmFinderSupport().getBldEnvDao().find(condition.getCondition(), sort);
		return FluentList.from( envList ).map(
				new TriFunction<IBldEnvEntity, ItemLabelsBean>() {
					@Override
					public ItemLabelsBean apply(IBldEnvEntity input) {
						return new ItemLabelsBean(input.getBldEnvNm() , input.getBldEnvId());
					}}
				).asList();
	}
	
	private List<DealAssetRegistrationHistoryBean> getHistoryBeanList( String envId ) {

		if ( TriStringUtils.isEmpty(envId) ) {
			throw new ContinuableBusinessException( RmMessageId.RM001021E );
		}
		// 配付履歴確認実行
		List<DealAssetRegistrationHistoryBean> historyBeanList = new ArrayList<DealAssetRegistrationHistoryBean>();
		
		List<Object> paramList = new CopyOnWriteArrayList<Object>() ;
		DealAssetServiceResponseBean resBean = null;
		
		DealAssetServiceBean dealBean = new DealAssetServiceBean();
		dealBean.setTargetEnvironment(envId);
		paramList.add(dealBean);

		IServiceDto<IGeneralServiceBean> innerServiceDto = new ServiceDto<IGeneralServiceBean>()
				.setParamList( paramList );

		// 下層のアクションを実行する
		for ( IDomain<IGeneralServiceBean> action : actions ) {
			action.execute( innerServiceDto );
		}
		//戻り値のチェック
		resBean = this.support.getDealAssetServiceResponseBean( paramList ) ;
		if ( TriStringUtils.isEmpty( resBean ) ) {
			throw new ContinuableBusinessException( DmMessageId.DM001006E );
		} else {
			int returnCode = resBean.getReturnCode();
			if ( returnCode != 0 ) {
				if (returnCode == 1) {
					throw new ContinuableBusinessException( DmMessageId.DM001007E );
				} else {
					throw new ContinuableBusinessException( DmMessageId.DM001011E );
				}
			}
			historyBeanList = resBean.getRegistrationHistoryList();
		}
		
		return historyBeanList;
	}
	
	private void setDeploymentInfoCache( FlowDeploymentJobListServiceBean paramBean ) {
		Map<String, DeploymentInfoView> deploymentInfoMap = paramBean.getCacheDeploymentInfo();
		
		if ( RequestOption.getDeploymentInfo.equals( paramBean.getParam().getRequestOption() )
				&& !paramBean.getParam().getInputInfo().isSelectedManual()) {
			deploymentInfoMap.clear();
			List<DealAssetRegistrationHistoryBean> historyBeans = this.getHistoryBeanList( paramBean.getParam().getSearchCondition().getEnvId() );
			if ( TriCollectionUtils.isNotEmpty(historyBeans) ) {
				for (DealAssetRegistrationHistoryBean historyBean : historyBeans) {
					String mgtVer = historyBean.getDrmsManagementNo();
					String registrationDate = historyBean.getRegistrationDate();
					if (deploymentInfoMap.containsKey( mgtVer )
							&& deploymentInfoMap.get( mgtVer ).getDeploymentRegistedTime().compareTo(registrationDate) > 0) {
						continue;
					}
					
					DeploymentInfoView deploymentInfoView = paramBean.new DeploymentInfoView();
					deploymentInfoView.setDeploymentRegistedTime( registrationDate );
					deploymentInfoView.setDeploymentAppliedTime( historyBean.getDealDate() );
					deploymentInfoView.setMgtVer( mgtVer );
					
					String status = historyBean.getStatus();
					if ( DrmsApplyResultId.SUCCESS.getValue().equals( status ) ) {
						deploymentInfoView.setDeploymentStatus( DrmsStatusId.APPLY .getValue() ) ;
						deploymentInfoView.setDeploymentStsId( DrmsStatusId.APPLY.name() );
					} else if ( DrmsApplyResultId.FAIL.getValue().equals( status ) ) {
						deploymentInfoView.setDeploymentStatus( DrmsStatusId.APPLY_ERROR .getValue() ) ;
						deploymentInfoView.setDeploymentStsId( DrmsStatusId.APPLY_ERROR.name() );
					}
					
					deploymentInfoMap.put( deploymentInfoView.getMgtVer(), deploymentInfoView );
				}
			}
		}
		
	}

}
