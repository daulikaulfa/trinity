package jp.co.blueship.tri.dm.support;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import jp.co.blueship.tri.agent.dm.opt.beans.dto.DealAssetServiceResponseBean;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvEntity;
import jp.co.blueship.tri.dm.DmDesignBusinessRuleUtils;
import jp.co.blueship.tri.dm.dao.dmdo.eb.IDmDoEntity;
import jp.co.blueship.tri.dm.domain.beans.CommercialShellID;
import jp.co.blueship.tri.dm.domain.beans.dop.dto.CommercialShellBean;
import jp.co.blueship.tri.dm.domain.beans.dop.dto.ControlViewBean;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.DmDesignEntryKeyByCommercialShell;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.DmMessageId;
import jp.co.blueship.tri.rm.RmViewInfoAddonUtils;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.RpCondition;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.BaseInfoInputBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.ConfigurationViewBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.LotViewBean;
import jp.co.blueship.tri.rm.support.RmFinderSupport;


/**
 * FlowRelDistributeEntry等の6zカスタマイズ統合監視系のイベントのサポートClass
 * <br>
 * <p>
 * 統合監視 配布資源登録等のための業務サービス処理を支援するサポートクラスです。
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowRelDistributeEditSupport extends RmFinderSupport {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	public static final String TIMER_ALL_ZERO = "00000000000000" ;
	public static final int TIMER_LENGTH = TIMER_ALL_ZERO.length() ;
	public static final String TIMER_DOUBLE_ZERO = "00" ;
	public static final String TIMER_ZERO = "0" ;

	/**
	 * リリースエンティティからリリース画面用ビーンを作成します。
	 * @param entities リリースエンティティ
	 * @return リリース画面用ビーンのリスト
	 */
	public ControlViewBean getControlViewBean( IRpEntity[] entities ) {

		ControlViewBean controlViewBean = new ControlViewBean();
		for ( IRpEntity entity : entities ) {
			setControlViewBeanRelEntity( controlViewBean, entity );
		}

		return controlViewBean;
	}

	/**
	 * リリースエンティティからリリース画面用ビーンを作成します。
	 * @param entities リリースエンティティ
	 * @return リリース画面用ビーンのリスト
	 */
	public List<ControlViewBean> getControlViewBeanList( IRpEntity[] entities , ILotEntity lotEntity ) {

		List <ControlViewBean> viewBeanList = new ArrayList<ControlViewBean>();

		for (IRpEntity entity : entities) {
			ControlViewBean controlViewBean = new ControlViewBean();
			boolean ret = isExist(entity, lotEntity);
			if (ret) {
				setControlViewBeanRelEntity( controlViewBean, entity );
				viewBeanList.add(controlViewBean);
			}
		}

		return viewBeanList;
	}

	/**
	 * ＲＣの実体ファイルを確認します。
	 * @param entities リリースエンティティ
	 * @return boolean
	 */
	public boolean isExist(IRpEntity relEntity, ILotEntity lotEntity) {

		boolean exist = false;
		File srcFile = DmDesignBusinessRuleUtils.getHistoryRelDSLFilePath(lotEntity, relEntity) ;
//		exist = srcFile.isFile();
		exist = srcFile.exists();
		return exist;

	}

	/**
	 *
	 * @param paramList
	 * @return
	 */
	public CommercialShellBean getCommercialShellBean( List<Object> paramList ) {
		for( Object obj : paramList ) {
			if( obj instanceof CommercialShellBean ) {
				return (CommercialShellBean)obj ;
			}
		}
		return null ;
	}


	/**
	 * ロットの情報をロット表示ビーンに値をセットして返す。
	 *
	 */
	public LotViewBean getLotReleaseConfirm( String lotId ) {

		ILotEntity pjtLotEntity = this.getAmFinderSupport().findLotEntity( lotId ) ;

		LotViewBean lotViewBean = new LotViewBean() ;
		RmViewInfoAddonUtils.setLotViewBeanPjtLotEntity( lotViewBean , pjtLotEntity, this.getBmFinderSupport().findBldSrvEntityByController().getBldSrvId() ) ;

		return lotViewBean ;
	}

	/**
	 * 環境の情報を環境表示ビーンに値をセットして返す。
	 *
	 */
	public ConfigurationViewBean getEnvReleaseConfirm( String envNo ) {

		IBldEnvEntity envEntity = this.getBmFinderSupport().findBldEnvEntity( envNo ) ;

		ConfigurationViewBean confViewBean = new ConfigurationViewBean() ;
		RmViewInfoAddonUtils.setConfViewBeanRelEnvEntity( confViewBean, envEntity ) ;

		return confViewBean ;
	}

	/**
	 * リリースパッケージの情報をリリースパッケージ表示ビーンに値をセットして返す。
	 *
	 */
	public ControlViewBean getControlReleaseConfirm( String relNo ) {

		RpCondition condition = new RpCondition();
		condition.setRpId(relNo);
		IRpEntity relEntity = this.getRpDao().findByPrimaryKey(condition.getCondition()) ;
		ControlViewBean controlViewBean = new ControlViewBean() ;
		setControlViewBeanRelEntity( controlViewBean , relEntity ) ;

		return controlViewBean ;
	}

	/**
	 * ロット一覧画面の表示項目を、ロットエンティティから取得して設定する。
	 * @param viewBean ロット一覧画面の表示項目
	 * @param entity ロットエンティティ
	 */
	private void setControlViewBeanRelEntity(

		ControlViewBean viewBean, IRpEntity entity ) {

		viewBean.setRelNo		( entity.getRpId() );
		viewBean.setLotNo		( entity.getLotId() );
		viewBean.setEnvNo		( entity.getBldEnvId() );
		viewBean.setSummary		( entity.getSummary() );
		viewBean.setGenerateDate( TriDateUtils.convertViewDateFormat( entity.getRegTimestamp() ) );
	}
	/**
	 * リリース番号から、リンク番号を生成する。<br>
	 * <br>
	 * リンク番号はYYMM(年月) + NNNN(数字4桁)の８桁。<br>
	 * @param distNo リリース番号
	 * @return リンク番号
	 */
	public String makeLinkNoLabel( String distNo ) {
		String linkNo = distNo;
		if ( true != TriStringUtils.isEmpty( distNo ) ) {

			int yymmPos = distNo.length() - 10 ;
			int yymmLen = 4 ;
			int uniqueNoPos = distNo.length() - 4 ;
			int uniqueNoLen = 4 ;

			String yymm = distNo.substring( yymmPos , yymmPos + yymmLen ) ;//YYMM
			String uniqueNo = distNo.substring( uniqueNoPos , uniqueNoPos + uniqueNoLen ) ;//下４桁

			linkNo = yymm + uniqueNo ;
		}
		return linkNo ;
	}

	private static final String SHELL_DATE_PATTERN = "yyMMddHHmm";
	//private static final SimpleDateFormat SHELL_DATE_FORMAT = new SimpleDateFormat( SHELL_DATE_PATTERN );
	private static final ThreadLocal<SimpleDateFormat> dfShellDateFormat = new ThreadLocal<SimpleDateFormat>() {
		@Override
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat(SHELL_DATE_PATTERN);
		}
	};

	/**
	 * 商用シェルのファイル名を生成する
	 * @param shellId シェルＩＤ
	 * @param no シェル種別番号
	 * @return 生成された商用シェルファイル名
	 */
	public String makeShellFileName( CommercialShellID shellId , String no ) {

		//ファイル拡張子を取得
		String fileExtension = sheet.getValue( DmDesignEntryKeyByCommercialShell.fileExtension ).trim() ;
		//「/」の場合拡張子なし
		if( "/".equals(fileExtension) ) {
			fileExtension = "";
		}

		StringBuilder stb = new StringBuilder() ;

		stb.append( shellId.getCode() ) ;
		stb.append( "_" ) ;
		if( true != TriStringUtils.isEmpty( no ) ) {
			stb.append( no ) ;
			stb.append( "_" ) ;
		}
		String date = dfShellDateFormat.get().format( new Date() ) ;
		stb.append( date ) ;
		stb.append( fileExtension ) ;

		return stb.toString() ;
	}

	private static final String DRMS_DATE_PATTERN = "yyyyMMddHHmmss";
	//private static final SimpleDateFormat DRMS_DATE_FORMAT = new SimpleDateFormat( DRMS_DATE_PATTERN );
	private static final ThreadLocal<SimpleDateFormat> dfDrmsDateFormat = new ThreadLocal<SimpleDateFormat>() {
		@Override
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat(DRMS_DATE_PATTERN);
		}
	};

	/**
	 * 日付を示す文字列(標準の日時フォーマット)を、表示形式に従った
	 * フォーマットに変換する。
	 * @param strDate 日付を示す文字列(標準の日時フォーマット)
	 * @param format 日付書式
	 * @return 日付を示す文字列(表示形式に従ったフォーマット)
	 */
	public final String convertViewDateFormat( String strDate, SimpleDateFormat format ) {

		if ( TriStringUtils.isEmpty( strDate ) ) {
			return "";
		}

		Date date = null;

		try {
			date = dfDrmsDateFormat.get().parse( strDate );

			return format.format( date );
		} catch ( ParseException pe ) {
			LogHandler.fatal( log , pe ) ;
			throw new TriSystemException( DmMessageId.DM004020F , pe , dfDrmsDateFormat.get().toString() , strDate);
		}
	}

	/**
	 * ConfigurationViewBeanのリストを作成して返す<br>
	 * @param relEnvEntityArray リリース情報の配列
	 * @return ConfigurationViewBeanのリスト
	 */
	public List<ConfigurationViewBean> makeConfigurationViewBeanList( IBldEnvEntity[] relEnvEntityArray ) {
		List<ConfigurationViewBean> confViewBeanList = new ArrayList<ConfigurationViewBean>();

		for ( IBldEnvEntity entity : relEnvEntityArray ) {
			ConfigurationViewBean viewBean = new ConfigurationViewBean();
			RmViewInfoAddonUtils.setConfViewBeanRelEnvEntity( viewBean, entity );
			confViewBeanList.add( viewBean );
		}
		return confViewBeanList ;
	}
	/**
	 *
	 * @param paramList
	 * @return
	 */
	public DealAssetServiceResponseBean getDealAssetServiceResponseBean( List<Object> paramList ) {

		for( Object obj : paramList ) {
			if( obj instanceof DealAssetServiceResponseBean ) {
				return (DealAssetServiceResponseBean)obj ;
			}
		}
		return null ;
	}

	/**
	 *
	 * @param relEntity
	 * @return
	 */
	public BaseInfoInputBean makeBaseInfoInputBean( IDmDoEntity relEntity ) {
		BaseInfoInputBean inputBean = new BaseInfoInputBean();
		inputBean.setRelContent( relEntity.getContent() );
		inputBean.setRelSummary( relEntity.getSummary() );
		return inputBean ;
	}
	/**
	 *
	 * @param outLog
	 * @param errLog
	 * @return
	 */
	public String makeRmiErrorMessage( String outLog , String errLog ) {
		StringBuilder stb = new StringBuilder();

		stb.append("エラーメッセージ:");
		stb.append( outLog );
		stb.append( errLog );

		return stb.toString() ;
	}
}
