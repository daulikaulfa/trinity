package jp.co.blueship.tri.dm;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.dm.constants.RelDistributeScreenItemID;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.DmDesignBeanId;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.BaseInfoBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.BaseInfoInputBean;

/**
 * 画面入力項目の必須チェック、形式チェックの共通処理Class
 * ItemCheckAddonUtilのDm版
 *
 */
public class DmItemCheckAddonUtils {
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	/**
	 * リリース（デプロイメント登録）基本情報入力項目チェック
	 * @param baseInfoBean リリース基本入力情報
	 */

	public static void checkRelDistributeBaseInfoInput( BaseInfoBean baseInfoBean ) {

		BaseInfoInputBean baseInfoInputBean = baseInfoBean.getBaseInfoInputBean();

		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]>	messageArgsList	= new ArrayList<String[]>();

		try {

			if ( !StatusFlg.off.value().equals(
					sheet.getValue(
							DmDesignBeanId.flowRelDistInfoInputCheck,
							RelDistributeScreenItemID.RelBaseInfoInput.SUMMARY.toString() )) ) {

				if ( TriStringUtils.isEmpty( baseInfoInputBean.getRelSummary() )) {
					messageList.add		( RmMessageId.RM001018E );
					messageArgsList.add	( new String[] {} );
				}
			}

			if ( !StatusFlg.off.value().equals(
					sheet.getValue(
							DmDesignBeanId.flowRelDistInfoInputCheck,
							RelDistributeScreenItemID.RelBaseInfoInput.CONTENT.toString() )) ) {

				if ( TriStringUtils.isEmpty( baseInfoInputBean.getRelContent() )) {
					messageList.add		( RmMessageId.RM001019E );
					messageArgsList.add	( new String[] {} );
				}
			}

		} finally {
			if ( 0 != messageList.size() )
				throw new ContinuableBusinessException( messageList, messageArgsList );
		}
	}

}