package jp.co.blueship.tri.dm;

import jp.co.blueship.tri.dm.dao.dmdo.eb.IDmDoEntity;
import jp.co.blueship.tri.fw.cmn.utils.function.TriFunction;

/**
 * {@link jp.co.blueship.tri.fw.cmn.utils.collections.FluentList}のサポートUtils
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public class DmFluentFunctionUtils {

	public static final TriFunction<IDmDoEntity, String> toRpIdFromDmDo = new TriFunction<IDmDoEntity, String>() {
		public String apply(IDmDoEntity input) {
			return input.getRpId();
		}
	};

}
