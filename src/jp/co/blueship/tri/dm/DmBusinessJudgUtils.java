package jp.co.blueship.tri.dm;

import java.sql.Timestamp;

import jp.co.blueship.tri.dm.dao.dmdo.eb.IDmDoEntity;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;

/**
 *
 * 指定した値の判定を行うユーティリティークラスです。<br>
 * BusinessJudgUtilsの派生クラスです。依存性の解消のため、作成します。<br>
 * <br>
 * this class is  utility class for judging the specified value.<br>
 * This is a derived class of BusinessJudgUtils<br>
 * It creates it to solve the dependency.<br>
 *
 * @author Akahoshi
 * @version V4.01.00
 *
 */
public class DmBusinessJudgUtils {

	/**
	 *
	 * 対象の配布連携のタイマー設定日時が過去の日時かどうかを判定します。<br>
	 * <br>
	 * judges whether the set time is past.<br>
	 *
	 * @param entity DmDoEntity
	 * @return return true if set time is past.
	 */
	public static boolean isPastSetTime( IDmDoEntity entity ) {

		if ( TriStringUtils.isNotEmpty( entity.getTimerSettingDate() ) &&
				TriStringUtils.isNotEmpty( entity.getTimerSettingTime() ) ) {

			Timestamp settingTimer = TriDateUtils.convertStringToTimestampWithDateTime ( TriDateUtils.convertDateFormat(
					entity.getTimerSettingDate(),
					entity.getTimerSettingTime() ) );

			if ( settingTimer.before( TriDateUtils.getSystemTimestamp() ) ) {
				return true;
			}
		}

		return false;
	}

}
