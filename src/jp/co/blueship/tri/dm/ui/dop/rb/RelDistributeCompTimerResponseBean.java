package jp.co.blueship.tri.dm.ui.dop.rb;

import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;

public class RelDistributeCompTimerResponseBean extends BaseResponseBean {

	/** リリース配布番号 */
	private String distNo = null;

	/** 統合監視メッセージ */
	private String drmsMessage = null;

	/** RMI接続 */
	private boolean rmi = true;

	public String getDistNo() {
		return distNo;
	}

	public void setDistNo(String distNo) {
		this.distNo = distNo;
	}

	public String getDrmsMessage() {
		return drmsMessage;
	}

	public void setDrmsMessage(String errorMessage) {
		this.drmsMessage = errorMessage;
	}

	public void setRmi(boolean rmi) {
		this.rmi = rmi;
	}
	
	public boolean isRmi(){
		return rmi;
	}
	
}
