package jp.co.blueship.tri.dm.ui.dop.beans;

import jp.co.blueship.tri.dm.constants.RelDistributeScreenID;
import jp.co.blueship.tri.dm.domain.dop.dto.FlowRelDistributeTimerServiceBean;
import jp.co.blueship.tri.dm.domain.dop.dto.FlowRelDistributeTimerServiceBean.TimerSetViewBean;
import jp.co.blueship.tri.dm.ui.DateRangeProcUtils;
import jp.co.blueship.tri.dm.ui.DmCnvServiceToActionUtils;
import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;
import jp.co.blueship.tri.fw.act.rb.IBaseResponseBean;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.constants.status.RmRpStatusId;
import jp.co.blueship.tri.fw.constants.status.RmRpStatusIdForExecData;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionMultiWindows;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.PresentationProsecutor;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.BaseInfoBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.BaseInfoInputBean;

public class FlowRelDistributeTimerProsecutor extends PresentationProsecutor {

	

	public static final String FLOW_ACTION_ID = "FlowRelDistributeTimerService";
	public static final String[] screenFlows = new String[] {
														RelDistributeScreenID.TIMER_SET,
														RelDistributeScreenID.TIMER_CONFIRM,
														RelDistributeScreenID.COMP_TIMER };

	public FlowRelDistributeTimerProsecutor( BaseBusinessException bbe ) {
		super( bbe );
	}
	public FlowRelDistributeTimerProsecutor() {
		super( null );
	}


	@Override
	protected IGeneralServiceBean callApplicationService(IService service,
			IGeneralServiceBean info) throws Exception {
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( info );

		return ((IGenericTransactionService)service).execute(FLOW_ACTION_ID, serviceDto).getServiceBean();
	}

	@Override
	protected void getApplicationInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {
	}

	@Override
	protected IGeneralServiceBean getBussinessInfo(
			IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {

		SessionMultiWindows session = new SessionMultiWindows( sesInfo, reqInfo );
		session.setAttribute( SessionScopeKeyConsts.FLOW_ACTION_ID, FLOW_ACTION_ID );
		FlowRelDistributeTimerServiceBean seBean = (FlowRelDistributeTimerServiceBean)session.getAttribute(FLOW_ACTION_ID);

		FlowRelDistributeTimerServiceBean bean = new FlowRelDistributeTimerServiceBean();
		if ( null != seBean ) {
			bean = seBean;
			//ワーニングメッセージの初期化
			bean.setInfoMessage( null );
		}

		String referer		= session.getReferer( this );
		String forward		= session.getForward( this );
		String screenType	= session.getScreenType( this );

		bean.setReferer			( referer );
		bean.setForward			( forward );
		bean.setScreenType		( screenType );

		if( RelDistributeScreenID.TOP.equals( referer ) ) {

		} else if ( RelDistributeScreenID.TIMER_SET.equals( referer ) ) {

			String timer[] = null;
			if ( "search".equals( screenType ) ) {
				timer = reqInfo.getParameterValues("timer");
				if (null != timer) {
					String [] status = timer;
					int i = 0;
					for (String value : timer) {
						if ( "REL_UN_PROCESS".equals( value ) ) {
							status[i] = RmRpStatusId.Unprocessed.getStatusId();
						} else if ( "REL_ACTIVE".equals( value ) ) {
							status[i] = RmRpStatusIdForExecData.CreatingReleasePackage.getStatusId();
						} else if ( "REL_COMPLETE".equals( value ) ) {
							status[i] = RmRpStatusId.ReleasePackageCreated.getStatusId();
						}
						i++;
					}
					bean.setTimer(status);
				} else {
					bean.setTimer(timer);
				}
				String selectedEnvNo = reqInfo.getParameter( "relEnv" );
				String selectedLotNo = reqInfo.getParameter( "lot" );
				bean.setSelectedEnvNo(selectedEnvNo);
				bean.setSelectedLotNo(selectedLotNo);
			}

			bean.setRelNo			( reqInfo.getParameter( "distNo" ));
			bean.setDistNo			( reqInfo.getParameter( "distNo" ) );
			String timerCancel = reqInfo.getParameter( "timerCancel" );

			String timerDateYear = reqInfo.getParameter( "timerDateYear") ;
			String timerDateMonth = reqInfo.getParameter( "timerDateMonth") ;
			String timerDateDay = reqInfo.getParameter( "timerDateDay") ;
			String timerDateHour = reqInfo.getParameter( "timerDateHour") ;
			String timerDateMinuteHigh = reqInfo.getParameter( "timerDateMinuteHigh") ;
			String timerDateMinuteLow = reqInfo.getParameter( "timerDateMinuteLow") ;

			TimerSetViewBean timeViewBean = bean.getTimerSetViewBean();
			timeViewBean.setYear( timerDateYear );
			timeViewBean.setMonth( timerDateMonth );
			timeViewBean.setDay( timerDateDay );
			timeViewBean.setHour( timerDateHour );
			timeViewBean.setMinuteHigh( timerDateMinuteHigh );
			timeViewBean.setMinuteLow( timerDateMinuteLow );
			bean.setTimerSetViewBean(timeViewBean);

			String timerDate = DateRangeProcUtils.getFromValueForBL( timerDateYear , timerDateMonth, timerDateDay ) ;

			String timerDateMinute = timerDateMinuteHigh + timerDateMinuteLow ;
			String timerTime = DateRangeProcUtils.getFromValueTimeForBL( timerDateHour , timerDateMinute , null ) ;

			bean.setCheckTimerDate( timerDate ) ;
			bean.setTimerDate(TriDateUtils.convertFromTime( timerDate + " " + timerTime ) ) ;

			if (null != timerCancel) {
				if ( "cancel".equals( timerCancel ) ) {
					bean.setTimerDate("");
				}
			}
			bean.setSelectedControlNo( reqInfo.getParameter("selectedRelNo" ));
			bean.setSelectedDistNo( reqInfo.getParameter( "selectedDistNo" ));
			bean.setTimerSummary( reqInfo.getParameter("timerSummary") );
			bean.setTimerContent( reqInfo.getParameter("timerContent") );
			bean.setTimerSetting( reqInfo.getParameter("timerSetting") );
			BaseInfoInputBean baseInputBean = new BaseInfoInputBean();
			baseInputBean.setRelContent( reqInfo.getParameter("timerContent") );
			baseInputBean.setRelSummary( reqInfo.getParameter("timerSummary") );
			BaseInfoBean baseInfoBean = new BaseInfoBean();
			baseInfoBean.setBaseInfoInputBean(baseInputBean);
			bean.setBaseInfoBean(baseInfoBean);

		} else if ( RelDistributeScreenID.TIMER_CONFIRM.equals( referer ) ) {

//			bean.setRelNo			( reqInfo.getParameter( "distNo" ));
//			bean.setSelectedDistNo	( reqInfo.getParameter( "selectedDistNo" ));
//			bean.setTimerSummary( reqInfo.getParameter("timerSummary") );
//			bean.setTimerContent( reqInfo.getParameter("timerContent") );

			String [] rmi = reqInfo.getParameterValues("rmi");
			if (null != rmi) {
				for (String value : rmi) {
					if ( StatusFlg.off.name().equals( value ) ) {
						bean.setRmi(false);
					}
				}
			}

		} else if ( RelDistributeScreenID.COMP_TIMER.equals( referer ) ) {

		}


		String userId = (String)sesInfo.getAttribute( SessionScopeKeyConsts.FLWC00_USER_ID );
		bean.setUserId(userId);
		bean.setUserName(userName());

		//RelDistributeConverterServiceMessageUtil.saveSelectedRelNoForReport( session, reqInfo );
		session.setAttribute( FLOW_ACTION_ID , bean ) ;

		return bean;
	}

	@Override
	protected IBaseResponseBean getRequestInfo(
			IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {

		FlowRelDistributeTimerServiceBean bean =
			(FlowRelDistributeTimerServiceBean)getServiceReturnInformation();

		BaseResponseBean retBean = null;
		String forward = bean.getForward();

		if ( RelDistributeScreenID.TIMER_SET.equals( forward ) ) {

			retBean =DmCnvServiceToActionUtils.convertRelDistributeTimerSetResponseBean(bean, reqInfo, sesInfo);

		} else if ( RelDistributeScreenID.TIMER_CONFIRM.equals( forward ) ) {

			retBean =DmCnvServiceToActionUtils.convertRelDistributeTimerConfirmResponseBean(bean, reqInfo, sesInfo);

		} else if ( RelDistributeScreenID.COMP_TIMER.equals( forward ) ) {

			retBean =DmCnvServiceToActionUtils.convertRelDistributeCompTimerResponseBean(bean, reqInfo, sesInfo);

		}

		if (null != retBean) {
			retBean.new MessageUtility().reflectMessage( bean );
			retBean.new MessageUtility().reflectMessage( getBussinessException() );

		}

		return retBean;
	}

	@Override
	protected String getResponseName() {
		return "responseBean";
	}

	@Override
	protected IService getService(
			IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo ) {
		IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
		return (IService)ca.getBean("generalService");
	}

	@Override
	protected void getSessionInfo(
			IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {
	}

	@Override
	protected void postProcessor(
			IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo ) {

		SessionMultiWindows session = new SessionMultiWindows(sesInfo,reqInfo);
		String forward = session.getForward( this );

		if ( session.isRemoveAttribute(forward, screenFlows) )
			session.removeAttribute( FLOW_ACTION_ID );

		if ( null == this.getBussinessException()
			&& screenFlows[ screenFlows.length - 1 ].equals( forward ) ) {
			session.removeAttribute( FLOW_ACTION_ID );
		}

	}

	@Override
	protected void preProcessor( IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo ) {

		SessionMultiWindows session = new SessionMultiWindows(sesInfo,reqInfo);
		String referer = session.getReferer( this );

		if ( session.isRemoveAttribute(referer, screenFlows) )
			session.removeAttribute( FLOW_ACTION_ID );

	}

}
