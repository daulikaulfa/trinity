package jp.co.blueship.tri.dm.ui.dop.beans;

import jp.co.blueship.tri.dm.constants.RelDistributeScreenID;
import jp.co.blueship.tri.dm.domain.dop.dto.FlowRelDistributeTopMenuServiceBean;
import jp.co.blueship.tri.dm.ui.DmCnvServiceToActionUtils;
import jp.co.blueship.tri.dm.ui.dop.rb.RelDistributeTopResponseBean;
import jp.co.blueship.tri.fw.act.rb.IBaseResponseBean;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionMultiWindows;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.PresentationProsecutor;

public class FlowRelDistributeTopMenuProsecutor extends PresentationProsecutor {

	

	public static final String FLOW_ACTION_ID = "FlowRelDistributeTopMenuService";

	/**
	 * 参照系の遷移フロー。
	 * <br>業務エラー発生時、PresentationControllerの遷移先制御に影響するため、合わせてメンテナンスすること。
	 */
	public static final String[] screenViewFlows = new String[] {
															RelDistributeScreenID.TOP,
															RelDistributeScreenID.LIST,
														 };

	public FlowRelDistributeTopMenuProsecutor(BaseBusinessException bbe) {
		super(bbe);
	}


	public FlowRelDistributeTopMenuProsecutor() {
		super(null);
	}

	@Override
	protected IGeneralServiceBean callApplicationService(IService service,
			IGeneralServiceBean info) throws Exception {
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( info );

		return ((IGenericTransactionService)service).execute(FLOW_ACTION_ID, serviceDto).getServiceBean();
	}

	@Override
	protected void getApplicationInfo(IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {
	}

	@Override
	protected IGeneralServiceBean getBussinessInfo(
			IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {

		SessionMultiWindows session = new SessionMultiWindows( sesInfo, reqInfo );
		session.setAttribute	( SessionScopeKeyConsts.FLOW_ACTION_ID, FLOW_ACTION_ID );

		sesInfo.removeAttribute	( SessionScopeKeyConsts.TOP_SCREEN_NAME );

		FlowRelDistributeTopMenuServiceBean bean = new FlowRelDistributeTopMenuServiceBean();

		String userId = (String)sesInfo.getAttribute( SessionScopeKeyConsts.FLWC00_USER_ID );
		bean.setUserId( userId );
		bean.setUserName(userName());

		String referer		= session.getReferer( this );
		String forward		= session.getForward( this );
		String screenType	= session.getScreenType( this );

		bean.setReferer			( referer );
		bean.setForward			( forward );
		bean.setScreenType		( screenType );

		String [] rmi = reqInfo.getParameterValues("rmi");
		if (null != rmi) {
			for (String value : rmi) {
				if ( StatusFlg.off.name().equals( value ) ) {
					bean.setRmi(false);
				}
			}
		}
		return bean;
	}

	@Override
	protected IBaseResponseBean getRequestInfo(
			IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {

		ISessionInfo session = new SessionMultiWindows( sesInfo, reqInfo );

		sesInfo.setAttribute( SessionScopeKeyConsts.TOP_SCREEN_NAME, RelDistributeScreenID.TOP );

		FlowRelDistributeTopMenuServiceBean bean	= (FlowRelDistributeTopMenuServiceBean)getServiceReturnInformation();

		RelDistributeTopResponseBean resBean		=
			DmCnvServiceToActionUtils.convertRelDistributeTopResponseBean( bean, reqInfo, session );

		resBean.new MessageUtility().reflectMessage( bean );
		resBean.new MessageUtility().reflectMessage (getBussinessException() );

		return resBean;
	}

	@Override
	protected String getResponseName() {
		return "responseBean";
	}

	@Override
	protected IService getService(
			IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo ) {
		IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
		return (IService)ca.getBean("generalService");
	}

	@Override
	protected void getSessionInfo(IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {
	}

	@Override
	protected void postProcessor(IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo) {
		ISessionInfo session = new SessionMultiWindows(sesInfo,reqInfo);
		session.removeAttribute( FLOW_ACTION_ID );
	}

	@Override
	protected void preProcessor(IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo) {
	}

}
