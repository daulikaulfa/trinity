package jp.co.blueship.tri.dm.ui.dop.beans;

import jp.co.blueship.tri.dm.constants.RelDistributeScreenID;
import jp.co.blueship.tri.dm.domain.dop.dto.FlowRelDistributeEntryServiceBean;
import jp.co.blueship.tri.dm.ui.DmCnvActionToServiceUtils;
import jp.co.blueship.tri.dm.ui.DmCnvServiceToActionUtils;
import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;
import jp.co.blueship.tri.fw.act.rb.IBaseResponseBean;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionMultiWindows;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.PresentationProsecutor;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.BaseInfoBean;


public class FlowRelDistributeEntryProsecutor extends PresentationProsecutor {

	public static final String FLOW_ACTION_ID = "FlowRelDistributeEntryService";

	public static final String[] screenFlows = new String[] {
														RelDistributeScreenID.ENV_SELECT,
														RelDistributeScreenID.SELECT,
														RelDistributeScreenID.INFO_INPUT,
														RelDistributeScreenID.ENTRY_CONFIRM,
														RelDistributeScreenID.COMP_ENTRY };

	public FlowRelDistributeEntryProsecutor() {
		super(null);
	}

	public FlowRelDistributeEntryProsecutor(BaseBusinessException bbe) {
		super(bbe);
	}

	@Override
	protected IGeneralServiceBean callApplicationService(IService service,
			IGeneralServiceBean info) throws Exception {
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( info );

		return ((IGenericTransactionService)service).execute(FLOW_ACTION_ID, serviceDto).getServiceBean();
	}

	@Override
	protected void getApplicationInfo(IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {
	}

	@Override
	protected IGeneralServiceBean getBussinessInfo(IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {

		SessionMultiWindows session = new SessionMultiWindows( sesInfo, reqInfo );
		session.setAttribute(SessionScopeKeyConsts.FLOW_ACTION_ID, FLOW_ACTION_ID);

		FlowRelDistributeEntryServiceBean seBean = (FlowRelDistributeEntryServiceBean)session.getAttribute(FLOW_ACTION_ID);

		FlowRelDistributeEntryServiceBean bean = new FlowRelDistributeEntryServiceBean();

		if ( null != seBean ) {
			bean = seBean;
			//ワーニングメッセージの初期化
			bean.setInfoMessage( null );
		}

		String userId = (String)sesInfo.getAttribute(SessionScopeKeyConsts.FLWC00_USER_ID);
		bean.setUserId(userId);
		bean.setUserName(userName());

		String referer = session.getReferer( this );
		String forward = session.getForward( this );
		String screenType = session.getScreenType( this );

		bean.setReferer(referer);
		bean.setForward(forward);
		bean.setScreenType(screenType);


		if( RelDistributeScreenID.TOP.equals( referer ) ) {
		} else if ( RelDistributeScreenID.ENV_SELECT.equals( referer ) ) {

			bean.setSelectedLotNo(reqInfo.getParameter("selectedLotNo"));
			bean.setSelectedEnvNo(reqInfo.getParameter("selectedEnvNo"));

		} else if ( RelDistributeScreenID.SELECT.equals( referer ) ) {

			bean.setSelectedControlNo( reqInfo.getParameter( "selectedRelNo" ));

		} else if ( RelDistributeScreenID.INFO_INPUT.equals( referer ) ) {

			BaseInfoBean baseInfo = DmCnvActionToServiceUtils.convertInputBaseInfoBean(reqInfo);
			bean.setBaseInfoBean(baseInfo);
			session.setAttribute(SessionScopeKeyConsts.REL_SUMMARY, reqInfo.getParameter("relSummary"));
			session.setAttribute(SessionScopeKeyConsts.REL_CONTENT, reqInfo.getParameter("relContent"));

		} else if ( RelDistributeScreenID.ENTRY_CONFIRM.equals( referer ) ) {

//			ReleaseConfirmBean confirm = RelDistributeConverterServiceMessageUtil.convertReleaseConfirmBean(session);
//			bean.setReleaseConfirmBean(confirm);

			//商用シェルの場合、falseになる
			bean.setRmi(  ( null == reqInfo.getParameter("rmi") ) ? true : false  );

		} else if ( RelDistributeScreenID.COMP_ENTRY.equals( referer ) ) {


		}

		session.setAttribute( FLOW_ACTION_ID , bean );

		return bean;
	}

	@Override
	protected IBaseResponseBean getRequestInfo(IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {

		ISessionInfo session = new SessionMultiWindows( sesInfo, reqInfo );
		FlowRelDistributeEntryServiceBean bean = (FlowRelDistributeEntryServiceBean) getServiceReturnInformation();

		BaseResponseBean retBean = null;
		String forward = bean.getForward();

		if ( RelDistributeScreenID.INFO_INPUT.equals( forward ) ) {

			retBean = DmCnvServiceToActionUtils.convertRelBaseInfoInputResponseBean(bean, session);

		} else if ( RelDistributeScreenID.ENV_SELECT.equals( forward ) ) {

			retBean = DmCnvServiceToActionUtils.convertRelLotSelectResponseBean(bean, reqInfo, session);

		} else if ( RelDistributeScreenID.SELECT.equals( forward ) ) {

			retBean = DmCnvServiceToActionUtils.convertRelControlSelectResponseBean(bean, reqInfo, session);

		} else if ( RelDistributeScreenID.ENTRY_CONFIRM.equals( forward ) ) {

			retBean = DmCnvServiceToActionUtils.convertRelEditConfirmResponseBean(bean, reqInfo, session);

		} else if ( RelDistributeScreenID.COMP_ENTRY.equals( forward ) ) {

			retBean = DmCnvServiceToActionUtils.convertRelCompRequestResponseBean(bean, reqInfo, session);

		}

		if (null != retBean) {
			retBean.new MessageUtility().reflectMessage( bean );
			retBean.new MessageUtility().reflectMessage(getBussinessException());
		}

		return retBean;
	}

	@Override
	protected String getResponseName() {
		return "responseBean";
	}

	@Override
	protected IService getService(
			IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo ) {
		IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
		return (IService)ca.getBean("generalService");
	}

	@Override
	protected void getSessionInfo(IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {

	}

	@Override
	protected void postProcessor(IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo) {

		SessionMultiWindows session = new SessionMultiWindows(sesInfo,reqInfo);
		String forward = session.getForward( this );

		if ( session.isRemoveAttribute(forward, screenFlows) )
			session.removeAttribute( FLOW_ACTION_ID );

		if ( null == this.getBussinessException()
			&& screenFlows[ screenFlows.length - 1 ].equals( forward ) ) {
			session.removeAttribute( FLOW_ACTION_ID );
		}

	}

	@Override
	protected void preProcessor(IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo) {
		SessionMultiWindows session = new SessionMultiWindows(sesInfo,reqInfo);
		String referer = session.getReferer( this );

		if ( session.isRemoveAttribute(referer, screenFlows) )
			session.removeAttribute( FLOW_ACTION_ID );
	}

}
