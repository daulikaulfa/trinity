package jp.co.blueship.tri.dm.ui.dop.rb;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.dm.domain.beans.dop.dto.DistributeViewBean;
import jp.co.blueship.tri.dm.domain.dop.dto.FlowRelDistributeListServiceBean.ReleaseViewBean;
import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.ConfigurationViewBean;

public class RelDistributeListResponseBean extends BaseResponseBean {

	/** ページ番号数 */
	private Integer pageNoNum = null;
	/** 選択ページ番号 */
	private Integer selectPageNo = null;
	/** 遷移元画面ID */
	private String referer = null;
	/** 遷移先画面ID */
	private String forward = null;
	/** 遷移タイプ */
	private String screenType = null;
	/** リリースパッケージ情報 */
	private List<ReleaseViewBean> releaseViewBeanList = null;
	/** リリース配布情報 */
	private List<DistributeViewBean> distributeViewBeanList = null;
	/** リリース環境番号 */
	private String selectedEnvNo = null;
	/** 環境一覧表示のリスト */
	private List<ConfigurationViewBean> confViewBeanList = null;
	/** 統合監視のメッセージ */
	private String drmsMessage = null;
	/** RMI接続 */
	private boolean rmi = true;


	public String getForward() {
		return forward;
	}
	public void setForward(String forward) {
		this.forward = forward;
	}

	public String getReferer() {
		return referer;
	}
	public void setReferer(String referer) {
		this.referer = referer;
	}

	public String getScreenType() {
		return screenType;
	}
	public void setScreenType(String screenType) {
		this.screenType = screenType;
	}

	public Integer getPageNoNum() {
		return pageNoNum;
	}
	public void setPageNoNum(Integer pageNoNum) {
		this.pageNoNum = pageNoNum;
	}

	public Integer getSelectPageNo() {
		return selectPageNo;
	}
	public void setSelectPageNo(Integer selectPageNo) {
		this.selectPageNo = selectPageNo;
	}

	public List<ReleaseViewBean> getReleaseViewBeanList() {
		if ( null == releaseViewBeanList ) {
			releaseViewBeanList = new ArrayList<ReleaseViewBean>();
		}
		return releaseViewBeanList;
	}
	public void setReleaseViewBeanList(
			List<ReleaseViewBean> releaseViewBeanList ) {
		this.releaseViewBeanList = releaseViewBeanList;
	}

	public List<DistributeViewBean> getDistributeViewBeanList() {
		if ( null == distributeViewBeanList ) {
			distributeViewBeanList = new ArrayList<DistributeViewBean>();
		}
		return distributeViewBeanList;
	}
	public void setDistributeViewBeanList(
			List<DistributeViewBean> distributeViewBeanList ) {
		this.distributeViewBeanList = distributeViewBeanList;
	}

	public List<ConfigurationViewBean> getConfViewBeanList() {
		if ( null == confViewBeanList ) {
			confViewBeanList = new ArrayList<ConfigurationViewBean>();
		}
		return confViewBeanList;
	}
	public void setConfViewBeanList(List<ConfigurationViewBean> confViewBeanList) {
		this.confViewBeanList = confViewBeanList;
	}

	public String getSelectedEnvNo() {
		return selectedEnvNo;
	}
	public void setSelectedEnvNo(String selectedEnvNo) {
		this.selectedEnvNo = selectedEnvNo;
	}

	public String getDrmsMessage() {
		return drmsMessage;
	}
	public void setDrmsMessage(String drmsMessage) {
		this.drmsMessage = drmsMessage;
	}

	public void setRmi(boolean rmi) {
		this.rmi = rmi;
	}
	public boolean isRmi(){
		return rmi;
	}


}
