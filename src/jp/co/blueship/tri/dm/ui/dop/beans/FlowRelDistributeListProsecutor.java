package jp.co.blueship.tri.dm.ui.dop.beans;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import jp.co.blueship.tri.dm.domain.beans.dop.dto.DistributeViewBean;
import jp.co.blueship.tri.dm.domain.dop.dto.FlowRelDistributeListServiceBean;
import jp.co.blueship.tri.dm.domain.dop.dto.FlowRelDistributeListServiceBean.ReleaseViewBean;
import jp.co.blueship.tri.dm.ui.dop.rb.RelDistributeListResponseBean;
import jp.co.blueship.tri.fw.act.rb.IBaseResponseBean;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionMultiWindows;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.PresentationProsecutor;

public class FlowRelDistributeListProsecutor extends PresentationProsecutor {

	

	public static final String FLOW_ACTION_ID = "FlowRelDistributeListService";

	private static final int defaultPageNo = 1;

	public FlowRelDistributeListProsecutor() {
		super( null );
	}

	public FlowRelDistributeListProsecutor( BaseBusinessException bbe ) {
		super( bbe );
	}

	@Override
	protected IGeneralServiceBean callApplicationService(IService service,
			IGeneralServiceBean info) throws Exception {
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( info );

		return ((IGenericTransactionService)service).execute(FLOW_ACTION_ID, serviceDto).getServiceBean();
	}

	@Override
	protected void getApplicationInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {
	}

	@Override
	protected IGeneralServiceBean getBussinessInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {

		SessionMultiWindows session = new SessionMultiWindows(sesInfo,reqInfo);
		session.setAttribute(SessionScopeKeyConsts.FLOW_ACTION_ID, FLOW_ACTION_ID);

		FlowRelDistributeListServiceBean seBean = (FlowRelDistributeListServiceBean)session.getAttribute(FLOW_ACTION_ID);

		FlowRelDistributeListServiceBean bean = new FlowRelDistributeListServiceBean();
		String userId = (String)sesInfo.getAttribute( SessionScopeKeyConsts.FLWC00_USER_ID );
		bean.setUserId( userId );
		bean.setUserName(userName());

		if ( null != seBean ) {
			bean = seBean;
			//ワーニングメッセージの初期化
			bean.setInfoMessage( null );
		}

		String referer		= session.getReferer( this );
		String forward		= session.getForward( this );
		String screenType	= session.getScreenType( this );

		bean.setReferer			( referer );
		bean.setForward			( forward );
		bean.setScreenType		( screenType );

		bean.setSelectedEnvNo	( reqInfo.getParameter( "envNo" ));

		String [] rmi = reqInfo.getParameterValues("rmi");

		if (null != rmi) {
			for (String value : rmi) {
				if ( StatusFlg.off.name().equals( value ) ) {
					bean.setRmi(false);
				}
			}
		}

		String savedPageNo = reqInfo.getParameter( "savedPageNo" ) ;
		if( ! TriStringUtils.isEmpty( savedPageNo ) ) {
			bean.setSelectPageNo(Integer.parseInt( savedPageNo ) ) ;
		}

		//RelDistributeConverterServiceMessageUtil.saveSelectedRelNoForReport( session, reqInfo );
		session.setAttribute( FLOW_ACTION_ID , bean ) ;

		return bean;
	}

	@Override
	protected IBaseResponseBean getRequestInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {

		ISessionInfo session = new SessionMultiWindows( sesInfo, reqInfo );

		RelDistributeListResponseBean resBean = new RelDistributeListResponseBean();
		FlowRelDistributeListServiceBean bean =
			(FlowRelDistributeListServiceBean)getServiceReturnInformation();

		resBean.setSelectedEnvNo		( bean.getSelectedEnvNo() );
		resBean.setReleaseViewBeanList	( setLinkNoList(bean.getReleaseViewBeanList()) );
		resBean.setConfViewBeanList		( bean.getConfViewBeanList() );
		resBean.setDrmsMessage			( bean.getDrmsMessage() );

		if (null == bean.getPageInfoView()) {
			resBean.setPageNoNum(defaultPageNo);
			resBean.setSelectPageNo(defaultPageNo);
		} else {
			resBean.setPageNoNum			( bean.getPageInfoView().getMaxPageNo() );
			resBean.setSelectPageNo			( bean.getPageInfoView().getSelectPageNo() );
		}


		session.setAttribute(
				SessionScopeKeyConsts.VIEW_REPORT_NO, getRelNoSet( bean.getDistributeViewBeanList() ));

		resBean.new MessageUtility().reflectMessage( bean );
		resBean.new MessageUtility().reflectMessage( getBussinessException() );

		return resBean;
	}

	/**
	 * リリース番号のセットを取得する
	 * @param distributeViewBeanList DistributeViewBeanのリスト
	 * @return リリース番号のセット
	 */
	private Set<String> getRelNoSet( List<DistributeViewBean> distributeViewBeanList ) {

		Set<String> relNoSet = new HashSet<String>();
		for ( DistributeViewBean viewBean : distributeViewBeanList ) {
			relNoSet.add( viewBean.getRelNo() );
		}

		return relNoSet;
	}

	/**
	 * RC番号とDRMSのリンク番号を表示する。
	 * @param ReleaseViewBeanList ReleaseViewBeanのリスト
	 * @return 画面表示のセット
	 */
	private List<ReleaseViewBean> setLinkNoList( List<ReleaseViewBean> releaseViewBeanList ) {

		List<ReleaseViewBean> viewBeanList = new ArrayList<ReleaseViewBean>();
		for ( ReleaseViewBean viewBean : releaseViewBeanList ) {
			if (null != viewBean.getLinkNo() && viewBean.getLinkNo().length() > 0) {
				StringBuilder sb = new StringBuilder();
				sb.append("(");
				sb.append(viewBean.getLinkNo());
				sb.append(")");
				viewBean.setLinkNo(sb.toString());
			}
			viewBeanList.add( viewBean );
		}

		return viewBeanList;
	}

	@Override
	protected String getResponseName() {
		return "responseBean";
	}

	@Override
	protected IService getService(
			IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo ) {

		IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
		return (IService)ca.getBean( "generalService" );
	}

	@Override
	protected void getSessionInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {
	}

	@Override
	protected void postProcessor(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) {

		ISessionInfo session = new SessionMultiWindows( sesInfo,reqInfo );
		session.removeAttribute( FLOW_ACTION_ID );
	}

	@Override
	protected void preProcessor(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) {
	}

}
