package jp.co.blueship.tri.dm.ui.dop.rb;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.dm.domain.beans.dop.dto.ControlViewBean;
import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;

public class RelDistributeTimerConfirmResponseBean extends BaseResponseBean{

	/** リリース配布番号 */
	private String distNo = null;

	/** 概要 */
	private String distSummary = null;

	/** リリース内容 */
	private String distContent;

	/** 環境番号 */
	private String confNo = null;

	/** 環境名 */
	private String confName = null;

	/** 環境概要 */
	private String confSummary = null;

	/** ロット番号 */
	private String lotId = null;

	/** ロット名 */
	private String lotName = null;

	/** 作成者 */
	private String inputUser = null;

	/** 作成者ＩＤ */
	private String inputUserId = null;

	/** 作成日 */
	private String inputDate = null;

	/** リリース番号 */
	private String relNo = null;

	/** リリース概要 */
	private String relSummary = null;

	/** 作成日時 */
	private String generateDate = null;

	/** 設定日時 */
	private String timerDate = null;

	/** 設定概要 */
	private String timerSummary = null;

	/** 設定内容 */
	private String timerContent = null;

	/** リリースパッケージ情報 */
	private List<ControlViewBean> controlViewBeanList = null;

	public String getDistNo() {
		return distNo;
	}
	public void setDistNo(String distNo) {
		this.distNo = distNo;
	}

	public String getDistSummary() {
		return distSummary;
	}
	public void setDistSummary(String distSummary) {
		this.distSummary = distSummary;
	}

	public String getDistContent() {
		return distContent;
	}

	public void setDistContent(String distContent) {
		this.distContent = distContent;
	}

	public String getConfNo() {
		return confNo;
	}
	public void setConfNo(String confNo) {
		this.confNo = confNo;
	}

	public String getConfName() {
		return confName;
	}
	public void setConfName(String confName) {
		this.confName = confName;
	}

	public String getConfSummary() {
		return confSummary;
	}
	public void setConfSummary(String confSummary) {
		this.confSummary = confSummary;
	}

	public String getLotNo() {
		return lotId;
	}
	public void setLotNo(String lotId) {
		this.lotId = lotId;
	}

	public String getLotName() {
		return lotName;
	}
	public void setLotName(String lotName) {
		this.lotName = lotName;
	}

	public String getInputUser() {
		return inputUser;
	}
	public void setInputUser(String inputUser) {
		this.inputUser = inputUser;
	}

	public String getInputDate() {
		return inputDate;
	}
	public void setInputDate( String inputDate) {
		this.inputDate = inputDate;
	}

	public String getRelNo() {
		return relNo;
	}
	public void setRelNo(String relNo) {
		this.relNo = relNo;
	}

	public String getRelSummary() {
		return relSummary;
	}

	public void setRelSummary(String relSummary) {
		this.relSummary = relSummary;
	}

	public String getGenerateDate() {
		return generateDate;
	}
	public void setGenerateDate( String generateDate ) {
		this.generateDate = generateDate;
	}

	public String getTimerDate() {
		return timerDate;
	}
	public void setTimerDate( String timerDate) {
		this.timerDate = timerDate;
	}

	public String getTimerSummary() {
		return timerSummary;
	}
	public void setTimerSummary(String timerSummary) {
		this.timerSummary = timerSummary;
	}

	public String getTimerContent() {
		return timerContent;
	}
	public void setTimerContent(String timerContent) {
		this.timerContent = timerContent;
	}

	public List<ControlViewBean> getControlViewBeanList() {
		if ( null == controlViewBeanList ) {
			controlViewBeanList = new ArrayList<ControlViewBean>();
		}
		return controlViewBeanList;
	}
	public void setControlViewBeanList( List<ControlViewBean> controlViewBeanList ) {
		this.controlViewBeanList = controlViewBeanList;
	}
	public String getInputUserId() {
		return inputUserId;
	}
	public void setInputUserId(String inputUserId) {
		this.inputUserId = inputUserId;
	}

}
