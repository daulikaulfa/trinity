package jp.co.blueship.tri.dm.ui.dop.beans;

import jp.co.blueship.tri.dm.constants.RelDistributeScreenID;
import jp.co.blueship.tri.dm.domain.beans.dop.dto.ControlViewBean;
import jp.co.blueship.tri.dm.domain.dop.dto.FlowRelDistributeCancelServiceBean;
import jp.co.blueship.tri.dm.ui.dop.rb.RelDistributeCancelConfirmResponseBean;
import jp.co.blueship.tri.fw.act.rb.IBaseResponseBean;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionMultiWindows;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.PresentationProsecutor;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.BaseInfoInputBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.ConfigurationViewBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.LotViewBean;

/**
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowRelDistributeCancelProsecutor extends PresentationProsecutor {



	public static final String FLOW_ACTION_ID = "FlowRelDistributeCancelService";
	public static final String[] screenFlows = new String[] {
														RelDistributeScreenID.CANCEL_CONFIRM,
														RelDistributeScreenID.COMP_CANCEL };

	public FlowRelDistributeCancelProsecutor( BaseBusinessException bbe ) {
		super( bbe );
	}
	public FlowRelDistributeCancelProsecutor() {
		super( null );
	}


	@Override
	protected IGeneralServiceBean callApplicationService(IService service,
			IGeneralServiceBean info) throws Exception {
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( info );

		return ((IGenericTransactionService)service).execute(FLOW_ACTION_ID, serviceDto).getServiceBean();
	}

	@Override
	protected void getApplicationInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {
	}

	@Override
	protected IGeneralServiceBean getBussinessInfo(
			IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {

		SessionMultiWindows session = new SessionMultiWindows( sesInfo, reqInfo );
		session.setAttribute( SessionScopeKeyConsts.FLOW_ACTION_ID, FLOW_ACTION_ID );

		FlowRelDistributeCancelServiceBean seBean = (FlowRelDistributeCancelServiceBean)session.getAttribute(FLOW_ACTION_ID);

		FlowRelDistributeCancelServiceBean bean = new FlowRelDistributeCancelServiceBean();

		if ( null != seBean ) {
			bean = seBean;
			//ワーニングメッセージの初期化
			bean.setInfoMessage( null );
		}

		String userId = (String)sesInfo.getAttribute( SessionScopeKeyConsts.FLWC00_USER_ID );
		bean.setUserId(userId);
		bean.setUserName(userName());

		String referer		= session.getReferer( this );
		String forward		= session.getForward( this );
		String screenType	= session.getScreenType( this );

		bean.setReferer			( referer );
		bean.setForward			( forward );
		bean.setScreenType		( screenType );

		if( RelDistributeScreenID.LIST.equals( referer ) ) {

			bean.setDistNo			( reqInfo.getParameter( "distNo" ));

		} else if (referer.equals(RelDistributeScreenID.CANCEL_CONFIRM)) {

			bean.setCancelComment	( reqInfo.getParameter( "cancelComment" ));
			bean.setDistNo			( reqInfo.getParameter( "distNo" ));

			String [] rmi = reqInfo.getParameterValues("rmi");

			if (null != rmi) {
				for (String value : rmi) {
					if (value.equals(StatusFlg.off.name())) {
						bean.setRmi(false);
					}
				}
			}

		} else if ( RelDistributeScreenID.COMP_CANCEL.equals( referer ) ) {

		}

		//RelDistributeConverterServiceMessageUtil.saveSelectedRelNoForReport( session, reqInfo );
		session.setAttribute( FLOW_ACTION_ID , bean ) ;

		return bean;
	}

	@Override
	protected IBaseResponseBean getRequestInfo(
			IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {

		FlowRelDistributeCancelServiceBean bean =
			(FlowRelDistributeCancelServiceBean)getServiceReturnInformation();

		RelDistributeCancelConfirmResponseBean resBean = new RelDistributeCancelConfirmResponseBean();
		String forward = bean.getForward();

		if( forward.equals(RelDistributeScreenID.CANCEL_CONFIRM ) ) {

			BaseInfoInputBean baseBean = bean.getBaseInfoBean().getBaseInfoInputBean();
			ConfigurationViewBean confBean = bean.getConfViewBean();
			LotViewBean lotBean = bean.getLotViewBean();
			ControlViewBean controlBean = bean.getControlViewBean();
			resBean.setDistNo		( bean.getDistNo( ));
			resBean.setDistSummary	( baseBean.getRelSummary() );
			resBean.setDistContent	( baseBean.getRelContent() );
			resBean.setConfNo		( confBean.getConfNo() );
			resBean.setConfName		( confBean.getConfName() );
			resBean.setConfSummary	( confBean.getConfSummary() );
			resBean.setLotNo		( lotBean.getLotNo() );
			resBean.setLotName		( lotBean.getLotName() );
			resBean.setInputUser	( lotBean.getInputUser() );
			resBean.setInputUserId	( lotBean.getInputUserId() );
			resBean.setInputDate	( lotBean.getInputDate() );
			resBean.setRelNo		( controlBean.getRelNo() );
			resBean.setRelSummary	( controlBean.getSummary() );
			resBean.setGenerateDate	( controlBean.getGenerateDate() );

		} else if (forward.equals(RelDistributeScreenID.COMP_CANCEL)) {

			resBean.setDistNo( bean.getDistNo() );
			resBean.setDrmsMessage( bean.getDrmsMessage());

		}

		resBean.setCancelComment( bean.getCancelComment() ) ;

		resBean.new MessageUtility().reflectMessage( bean );
		resBean.new MessageUtility().reflectMessage( getBussinessException() );

		return resBean;
	}

	@Override
	protected String getResponseName() {
		return "responseBean";
	}

	@Override
	protected IService getService(
			IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo ) {
		IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
		return (IService)ca.getBean("generalService");
	}

	@Override
	protected void getSessionInfo(
			IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {
	}

	@Override
	protected void postProcessor(
			IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo ) {

		SessionMultiWindows session = new SessionMultiWindows(sesInfo,reqInfo);
		String forward = session.getForward( this );

		if ( session.isRemoveAttribute(forward, screenFlows) )
			session.removeAttribute( FLOW_ACTION_ID );

		if ( null == this.getBussinessException()
			&& screenFlows[ screenFlows.length - 1 ].equals( forward ) ) {
			session.removeAttribute( FLOW_ACTION_ID );
		}

	}

	@Override
	protected void preProcessor( IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo ) {

		SessionMultiWindows session = new SessionMultiWindows(sesInfo,reqInfo);
		String referer = session.getReferer( this );

		if ( session.isRemoveAttribute(referer, screenFlows) )
			session.removeAttribute( FLOW_ACTION_ID );

	}

}
