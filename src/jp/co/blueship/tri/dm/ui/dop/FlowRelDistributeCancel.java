package jp.co.blueship.tri.dm.ui.dop;

import jp.co.blueship.tri.dm.ui.dop.beans.FlowRelDistributeCancelProsecutor;
import jp.co.blueship.tri.dm.ui.dop.beans.FlowRelDistributeListProsecutor;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.PresentationController;

public class FlowRelDistributeCancel extends PresentationController {

	
	
	protected void addPresentationProsecutores( PresentationProsecutorManager ppm ) {
		ppm.addPresentationProsecutor( new FlowRelDistributeCancelProsecutor() );
	}
	
	protected void addBusinessErrorPresentationProsecutores(PresentationProsecutorManager ppm, IRequestInfo reqInfo, ISessionInfo sesInfo, IApplicationInfo appInfo, BaseBusinessException bbe) {
		
		String referer = reqInfo.getParameter("referer");
		if (null != referer) {
			for (String screenFlow : FlowRelDistributeCancelProsecutor.screenFlows) {
				if ( screenFlow.equals( referer ) ) {
					ppm.addBusinessErrorPresentationProsecutor(new FlowRelDistributeCancelProsecutor(bbe));
					return;
				}
			}
		}
		//フロー外からの遷移／又は排他制御エラーであれば、以下の画面に遷移する
		ppm.addBusinessErrorPresentationProsecutor(new FlowRelDistributeListProsecutor(bbe));
	}
	
	@Override
	protected String getForward( PresentationProsecutorManager ppm,
			IRequestInfo reqInfo, ISessionInfo sesInfo, IApplicationInfo appInfo ) {
		return reqInfo.getParameter( "forward" );
	}

	@Override
	protected String getForwardForBusinessException(
			PresentationProsecutorManager ppm, IRequestInfo reqInfo,
			ISessionInfo sesInfo, IApplicationInfo appInfo, TriRuntimeException bbe ) {

		String referer = reqInfo.getParameter("referer");
		if ( null != referer ) {
			return reqInfo.getParameter("referer");
		}
		//フロー外からの遷移／又は排他制御エラーであれば、以下の画面に遷移する
		return "RelDistributeList";
	}

}
