package jp.co.blueship.tri.dm.ui.dop.rb;

import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;

public class RelDistributeCompModifyResponseBean extends BaseResponseBean {

	/** リリース番号 */
	private String distNo = null;

	public String getDistNo() {
		return distNo;
	}

	public void setDistNo(String distNo) {
		this.distNo = distNo;
	}
}
