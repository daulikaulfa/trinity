package jp.co.blueship.tri.dm.ui.dop.rb;

import java.util.List;

import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.ConfigurationViewBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.LotViewBean;

public class RelDistributeEnvSelectResponseBean extends BaseResponseBean {

	/** 環境一覧表示のリスト */
	private List<ConfigurationViewBean> confViewBeanList;
	/** 選択環境番号 */
	private String selectedEnvNo;

	/** ロット一覧表示のリスト */
	private List<LotViewBean> lotViewBeanList = null;
	/** 選択ロット情報番号 */
	private String selectedLotNo = null;

	public List<ConfigurationViewBean> getConfViewBeanList() {
		return confViewBeanList;
	}
	public void setConfViewBeanList(List<ConfigurationViewBean> confViewBeanList) {
		this.confViewBeanList = confViewBeanList;
	}

	public String getSelectedEnvNo() {
		return selectedEnvNo;
	}
	public void setSelectedEnvNo(String selectedEnvNo) {
		this.selectedEnvNo = selectedEnvNo;
	}

	public List<LotViewBean> getLotViewBeanList() {
		return lotViewBeanList;
	}
	public void setLotViewBeanList( List<LotViewBean> lotViewBeanList ) {
		this.lotViewBeanList = lotViewBeanList;
	}

	public String getSelectedLotNo() {
		return selectedLotNo;
	}
	public void setSelectedLotNo(String selectedLotNo) {
		this.selectedLotNo = selectedLotNo;
	}

}
