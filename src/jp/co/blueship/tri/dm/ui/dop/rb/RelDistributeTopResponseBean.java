package jp.co.blueship.tri.dm.ui.dop.rb;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.dm.domain.dop.dto.FlowRelDistributeTopMenuServiceBean.ReleaseViewBean;
import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;

public class RelDistributeTopResponseBean extends BaseResponseBean {

	
	
	/** ページ番号数 */
	private Integer pageNoNum = null;
	/** 選択ページ番号 */
	private Integer selectPageNo = null;
	/** 遷移元画面ID */
	private String referer = null;
	/** 遷移先画面ID */
	private String forward = null;
	/** 遷移タイプ */
	private String screenType = null;
	/** 統合監視のメッセージ */
	private String drmsMessage = null;
	/** RMI接続 */
	private boolean rmi = true;
	/** リリースパッケージ情報 */
	private List<ReleaseViewBean> releaseViewBeanList = null;
	
	
	public String getForward() {
		return forward;
	}
	public void setForward(String forward) {
		this.forward = forward;
	}
	
	public String getReferer() {
		return referer;
	}
	public void setReferer(String referer) {
		this.referer = referer;
	}
	
	public String getScreenType() {
		return screenType;
	}
	public void setScreenType(String screenType) {
		this.screenType = screenType;
	}
	
	public Integer getPageNoNum() {
		return pageNoNum;
	}
	public void setPageNoNum(Integer pageNoNum) {
		this.pageNoNum = pageNoNum;
	}

	public Integer getSelectPageNo() {
		return selectPageNo;
	}
	public void setSelectPageNo(Integer selectPageNo) {
		this.selectPageNo = selectPageNo;
	}

	public String getDrmsMessage() {
		return drmsMessage;
	}
	public void setDrmsMessage(String drmsMessage) {
		this.drmsMessage = drmsMessage;
	}

	public void setRmi(boolean rmi) {
		this.rmi = rmi;
	}
	public boolean isRmi(){
		return rmi;
	}
	
	public List<ReleaseViewBean> getReleaseViewBeanList() {
		if ( null == releaseViewBeanList ) {
			releaseViewBeanList = new ArrayList<ReleaseViewBean>();
		}
		return releaseViewBeanList;
	}
	public void setReleaseViewBeanList(
			List<ReleaseViewBean> releaseViewBeanList ) {
		this.releaseViewBeanList = releaseViewBeanList;
	}
	
}
