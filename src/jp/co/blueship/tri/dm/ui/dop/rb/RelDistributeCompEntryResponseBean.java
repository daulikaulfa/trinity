package jp.co.blueship.tri.dm.ui.dop.rb;

import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;

public class RelDistributeCompEntryResponseBean extends BaseResponseBean {

	/** リリース番号 */
	private String relNo = null;

	/**
	 *  統合監視のメッセージ 
	 */
	private String drmsMessage = null;

	/** RMI接続 */
	private boolean rmi = true;

	public String getRelNo() {
		return relNo;
	}

	public void setRelNo(String relNo) {
		this.relNo = relNo;
	}

	public String getDrmsMessage() {
		return drmsMessage;
	}

	public void setDrmsMessage(String drmsMessage) {
		this.drmsMessage = drmsMessage;
	}

	public void setRmi(boolean rmi) {
		this.rmi = rmi;
	}
	
	public boolean isRmi(){
		return rmi;
	}
	
}
