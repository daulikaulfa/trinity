package jp.co.blueship.tri.dm.ui.dop.rb;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;

public class RelDistributeInfoInputResponseBean extends BaseResponseBean {

	/** ステータス */
	private String statusView;

	/** ステータスリスト */
	private List<String> relStatusList = new ArrayList<String>();

	/** リリース概要 */
	private String relSummary;

	/** リリース内容 */
	private String relContent;

	/** 対応部署 */
	private String relCopeSection;

	/** 対応部署リスト */
	private List<String> relCopeSectionList = new ArrayList<String>();

	/** 配布形式 */
	private String distributionForm;

	/** 配布形式リスト */
	private List<String> distributionFormList = new ArrayList<String>();

	/** 対応者 */
	private String relCopeUser;

	/** 対応者ＩＤ */
	private String relCopeUserId;

	/** 承認者 */
	private String relApproveUser;

	/** 承認者ＩＤ */
	private String relApproveUserId;

	/** 承認日 */
	private String relApproveDate;


	public String getRelCopeUser() {
		return relCopeUser;
	}

	public void setRelCopeUser(String relCopeUser) {
		this.relCopeUser = relCopeUser;
	}

	public String getDistributionForm() {
		return distributionForm;
	}

	public void setDistributionForm(String distributionForm) {
		this.distributionForm = distributionForm;
	}

	public List<String> getDistributionFormList() {
		return distributionFormList;
	}

	public void setDistributionFormList(List<String> distributionFormList) {
		this.distributionFormList = distributionFormList;
	}

	public String getRelContent() {
		return relContent;
	}

	public void setRelContent(String relContent) {
		this.relContent = relContent;
	}

	public String getRelCopeSection() {
		return relCopeSection;
	}

	public void setRelCopeSection(String relCopeSection) {
		this.relCopeSection = relCopeSection;
	}

	public List<String> getRelCopeSectionList() {
		return relCopeSectionList;
	}

	public void setRelCopeSectionList(List<String> relCopeSectionList) {
		this.relCopeSectionList = relCopeSectionList;
	}

	public String getRelSummary() {
		return relSummary;
	}

	public void setRelSummary(String relSummary) {
		this.relSummary = relSummary;
	}

	public String getStatusView() {
		return statusView;
	}

	public void setStatusView(String relstatus) {
		this.statusView = relstatus;
	}

	public List<String> getRelStatusList() {
		return relStatusList;
	}

	public void setRelStatusList(List<String> relStatusList) {
		this.relStatusList = relStatusList;
	}

	public String getRelApproveDate() {
		return relApproveDate;
	}

	public void setRelApproveDate( String relApproveDate) {
		this.relApproveDate = relApproveDate;
	}

	public String getRelApproveUser() {
		return relApproveUser;
	}

	public void setRelApproveUser(String relApproveUser) {
		this.relApproveUser = relApproveUser;
	}

	public String getRelApproveUserId() {
		return relApproveUserId;
	}

	public void setRelApproveUserId(String relApproveUserId) {
		this.relApproveUserId = relApproveUserId;
	}

	public String getRelCopeUserId() {
		return relCopeUserId;
	}

	public void setRelCopeUserId(String relCopeUserId) {
		this.relCopeUserId = relCopeUserId;
	}
}
