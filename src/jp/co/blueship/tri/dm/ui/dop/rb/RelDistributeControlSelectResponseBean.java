package jp.co.blueship.tri.dm.ui.dop.rb;

import java.util.List;

import jp.co.blueship.tri.dm.domain.beans.dop.dto.ControlViewBean;
import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;

public class RelDistributeControlSelectResponseBean extends BaseResponseBean {

	/** 一覧表示のリスト */
	private List<ControlViewBean> controlViewBeanList;
	/** 選択ビルドパッケージ番号 */
	private String selectedRelNo;
	
	public List<ControlViewBean> getControlViewBeanList() {
		return controlViewBeanList;
	}
	public void setControlViewBeanList(List<ControlViewBean> controlViewBeanList) {
		this.controlViewBeanList = controlViewBeanList;
	}

	public String getSelectedRelNo() {
		return selectedRelNo;
	}
	public void setSelectedRelNo( String selectedRelNo ) {
		this.selectedRelNo = selectedRelNo;
	}
	
}
