package jp.co.blueship.tri.dm.ui.dop;

import jp.co.blueship.tri.dm.constants.RelDistributeScreenID;
import jp.co.blueship.tri.dm.ui.dop.beans.FlowRelDistributeListProsecutor;
import jp.co.blueship.tri.dm.ui.dop.beans.FlowRelDistributeTopMenuProsecutor;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.PresentationController;


public class FlowRelDistributeList extends PresentationController {

	
	
	protected void addPresentationProsecutores(PresentationProsecutorManager ppm) {
		ppm.addPresentationProsecutor(new FlowRelDistributeListProsecutor());
	}

	protected void addBusinessErrorPresentationProsecutores(PresentationProsecutorManager ppm, IRequestInfo reqInfo, ISessionInfo sesInfo, IApplicationInfo appInfo, BaseBusinessException bbe) {
		String referer = reqInfo.getParameter("referer");
		if (null != referer) {
			if (referer.equals(RelDistributeScreenID.LIST)) {
				ppm.addBusinessErrorPresentationProsecutor(new FlowRelDistributeListProsecutor(bbe));
				return;
			}
		}
		//フロー外からの遷移／又は排他制御エラーであれば、以下の画面に遷移する
		ppm.addBusinessErrorPresentationProsecutor(new FlowRelDistributeTopMenuProsecutor(bbe));
	}
	
	@Override
	protected String getForward(PresentationProsecutorManager ppm,
			IRequestInfo reqInfo, ISessionInfo sesInfo, IApplicationInfo appInfo) {
		return reqInfo.getParameter("forward");
//		return "RelDistributeList";
	}

	@Override
	protected String getForwardForBusinessException(
			PresentationProsecutorManager ppm, IRequestInfo reqInfo,
			ISessionInfo sesInfo, IApplicationInfo appInfo, TriRuntimeException bbe ) {
		String referer = reqInfo.getParameter("referer");
		if ( null != referer ) {
			return reqInfo.getParameter("referer");
		}
		//フロー外からの遷移／又は排他制御エラーであれば、以下の画面に遷移する
		return "RelDistributeTop";
	}

}
