package jp.co.blueship.tri.dm.ui.dop.rb;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.dm.domain.beans.dop.dto.ControlViewBean;
import jp.co.blueship.tri.dm.domain.beans.dop.dto.DistributeViewBean;
import jp.co.blueship.tri.dm.domain.dop.dto.FlowRelDistributeTimerServiceBean.TimerSetViewBean;
import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.ConfigurationViewBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.LotViewBean;

public class RelDistributeTimerSetResponseBean extends BaseResponseBean {

	/** タイマー設定検索 */
	private boolean searchTimer;

	/** 配布検索 */
	private boolean searchDistribute;

	/** 環境一覧表示のリスト */
	private List<ConfigurationViewBean> confViewBeanList;

	/** 選択環境番号 */
	private String selectedEnvNo;

	/** ロット一覧表示のリスト */
	private List<LotViewBean> lotViewBeanList = null;

	/** 選択ロット情報番号 */
	private String selectedLotNo = null;

	/** 一覧表示のリスト */
	private List<DistributeViewBean> distributeViewBeanList;

	/** リリースパッケージ情報 */
	private List<ControlViewBean> controlViewBeanList = null;

	/** 選択ビルドパッケージ番号 */
	private String selectedRelNo;

	/** タイマー検索条件 */
	private String timer;

	/** タイマー設定 */
	private String timerSetting;

	/** タイマー設定日 */
	private String timerDate;

	/** タイマー概要 */
	private String timerSummary;

	/** タイマー内容 */
	private String timerContent;

	/** タイマー設定 */
	private TimerSetViewBean timerSetViewBean = null;

	public List<ConfigurationViewBean> getConfViewBeanList() {
		return confViewBeanList;
	}
	public void setConfViewBeanList(List<ConfigurationViewBean> confViewBeanList) {
		this.confViewBeanList = confViewBeanList;
	}

	public String getSelectedEnvNo() {
		return selectedEnvNo;
	}
	public void setSelectedEnvNo(String selectedEnvNo) {
		this.selectedEnvNo = selectedEnvNo;
	}

	public List<LotViewBean> getLotViewBeanList() {
		return lotViewBeanList;
	}
	public void setLotViewBeanList( List<LotViewBean> lotViewBeanList ) {
		this.lotViewBeanList = lotViewBeanList;
	}

	public String getSelectedLotNo() {
		return selectedLotNo;
	}
	public void setSelectedLotNo(String selectedLotNo) {
		this.selectedLotNo = selectedLotNo;
	}

	public List<DistributeViewBean> getDistributeViewBeanList() {
		return distributeViewBeanList;
	}
	public void setDistributeViewBeanList(List<DistributeViewBean> distributeViewBeanList) {
		this.distributeViewBeanList = distributeViewBeanList;
	}

	public String getSelectedRelNo() {
		return selectedRelNo;
	}
	public void setSelectedRelNo( String selectedRelNo ) {
		this.selectedRelNo = selectedRelNo;
	}

	public String getTimerSummary() {
		return timerSummary;
	}

	public void setTimerSummary(String timerSummary) {
		this.timerSummary = timerSummary;
	}

	public String getTimerContent() {
		return timerContent;
	}

	public void setTimerContent(String timerContent) {
		this.timerContent = timerContent;
	}

	public String getTimerDate() {
		return timerDate;
	}

	public void setTimerDate( String timerDate) {
		this.timerDate = timerDate;
	}

	public boolean getSearchTimer() {
		return searchTimer;
	}

	public boolean isSearchTimer() {
		return searchTimer;
	}

	public void setSearchTimer(boolean searchTimer) {
		this.searchTimer = searchTimer;
	}

	public boolean getSearchDistribute() {
		return searchDistribute;
	}

	public boolean isSearchDistribute() {
		return searchDistribute;
	}

	public void setSearchDistribute(boolean searchDistribute) {
		this.searchDistribute = searchDistribute;
	}

	public String getTimer() {
		return timer;
	}

	public void setTimer(String timer) {
		this.timer = timer;
	}

	public String getTimerSetting() {
		return timerSetting;
	}

	public void setTimerSetting(String timerSetting) {
		this.timerSetting = timerSetting;
	}

	public List<ControlViewBean> getControlViewBeanList() {
		if ( null == controlViewBeanList ) {
			controlViewBeanList = new ArrayList<ControlViewBean>();
		}
		return controlViewBeanList;
	}
	public void setControlViewBeanList( List<ControlViewBean> controlViewBeanList ) {
		this.controlViewBeanList = controlViewBeanList;
	}

	public TimerSetViewBean getTimerSetViewBean() {
		return timerSetViewBean;
	}

	public void setTimerSetViewBean(TimerSetViewBean timerSetViewBean) {
		this.timerSetViewBean = timerSetViewBean;
	}

}
