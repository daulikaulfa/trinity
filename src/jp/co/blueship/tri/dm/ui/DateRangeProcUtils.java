package jp.co.blueship.tri.dm.ui;

import java.text.SimpleDateFormat;
import java.util.Date;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;

/**
 * 日付範囲指定部品に対する共通処理を行う。
 * <br> 
 * <p>
 * </p>
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2008
 */
public class DateRangeProcUtils {

	/**
	 * 日付Fromを業務ロジック向けに編集する
	 * 
	 * @param strYear
	 * @param strMonth
	 * @param strDay
	 * @return
	 */	
	public static String getFromValueForBL(String strYear, String strMonth, String strDay ) {

		String retDateValue = "";
		
		// 年月日いずれかが指定されている場合
		if ( 	(strYear != null && strYear.length() > 0) || 
				(strMonth != null && strMonth.length() > 0) || 
				(strDay != null && strDay.length() > 0) ) {
			
			if ( strYear == null || strYear.length() == 0 ) {
				retDateValue = new SimpleDateFormat("yyyy").format(new Date());
			} else {
				retDateValue = strYear;
			}
			
			if ( strMonth == null || strMonth.length() == 0 ) {
				retDateValue = retDateValue + "/01";
			} else {
				retDateValue = retDateValue + "/" + strMonth;
			}
			
			if ( strDay == null || strDay.length() == 0 ) {
				retDateValue = retDateValue + "/01";
			} else {
				retDateValue = retDateValue + "/" + strDay;
			}			
		}
		
		return retDateValue;
	}
	
	/**
	 * 日付Toを業務ロジック向けに編集する
	 * 
	 * @param strYear
	 * @param strMonth
	 * @param strDay
	 * @return
	 */	
	public static String getToValueForBL(String strYear, String strMonth, String strDay ) {

		String retDateValue = "";
		
		// 年月日いずれかが指定されている場合
		if ( 	(strYear != null && strYear.length() > 0) || 
				(strMonth != null && strMonth.length() > 0) || 
				(strDay != null && strDay.length() > 0) ) {
			
			if ( strYear == null || strYear.length() == 0 ) {
				retDateValue = new SimpleDateFormat("yyyy").format(new Date());
			} else {
				retDateValue = strYear;
			}
			
			if ( strMonth == null || strMonth.length() == 0 ) {
				retDateValue = retDateValue + "/12";
			} else {
				retDateValue = retDateValue + "/" + strMonth;
			}
			
			if ( strDay == null || strDay.length() == 0 ) {
				retDateValue = retDateValue + "/31";
			} else {
				retDateValue = retDateValue + "/" + strDay;
			}			
		}
		
		return retDateValue;
	}

	/**
	 * 日付を画面初期表示向けに編集する
	 * 
	 * @param strYear
	 * @param strMonth
	 * @param strDay
	 * @return
	 */	
	public static String getDefaultDateValue(String strYear, String strMonth, String strDay ) {
		
		String retDateValue;
		
		if ( strYear != null && strYear.length() > 0 ) {
			retDateValue = strYear; 
		} else {
			retDateValue = "----"; 
		}
		
		if ( strMonth != null && strMonth.length() > 0 ) {
			retDateValue = retDateValue + strMonth; 
		} else {
			retDateValue = retDateValue + "--"; 
		}
		
		if ( strDay != null && strDay.length() > 0 ) {
			retDateValue = retDateValue + strDay; 
		} else {
			retDateValue = retDateValue + "--"; 
		}
		
		if (retDateValue.length() != 8 ) retDateValue = "--------";
		
		return retDateValue;
	}

	/**
	 * 時刻Fromを業務ロジック向けに編集する
	 * 
	 * @param strHour
	 * @param strMinute
	 * @param strSecond
	 * @return
	 */	
	public static String getFromValueTimeForBL(String strHour, String strMinute, String strSecond ) {

		StringBuilder stb = new StringBuilder() ;
		stb.append( "" ) ;
		
		// 時分秒いずれかが指定されている場合
		if ( true != TriStringUtils.isEmpty( strHour ) || 
			true != TriStringUtils.isEmpty( strMinute ) || 
			true != TriStringUtils.isEmpty( strSecond ) ) {
			
			stb.append( ( true == TriStringUtils.isEmpty( strHour ) ) ? "00" : strHour ) ;
			stb.append( ":" ) ;
			stb.append( ( true == TriStringUtils.isEmpty( strMinute ) ) ? "00" : strMinute ) ;
			stb.append( ":" ) ;
			stb.append( ( true == TriStringUtils.isEmpty( strSecond ) ) ? "00" : strSecond ) ;
		}
		
		return stb.toString() ;
	}
	
	/**
	 * 日付Toを業務ロジック向けに編集する
	 * 
	 * @param strHour
	 * @param strMinute
	 * @param strSecond
	 * @return
	 */	
	public static String getToValueTimeForBL(String strHour, String strMinute, String strSecond ) {

		StringBuilder stb = new StringBuilder() ;
		stb.append( "" ) ;
		
		// 時分秒いずれかが指定されている場合
		if ( true != TriStringUtils.isEmpty( strHour ) || 
			true != TriStringUtils.isEmpty( strMinute ) || 
			true != TriStringUtils.isEmpty( strSecond ) ) {
			
			stb.append( ( true == TriStringUtils.isEmpty( strHour ) ) ? "23" : strHour ) ;
			stb.append( ":" ) ;
			stb.append( ( true == TriStringUtils.isEmpty( strMinute ) ) ? "59" : strMinute ) ;
			stb.append( ":" ) ;
			stb.append( ( true == TriStringUtils.isEmpty( strSecond ) ) ? "59" : strSecond ) ;
		}
		
		return stb.toString() ;
	}
	/**
	 * 時分Fromを業務ロジック向けに編集する
	 * 
	 * @param strHour
	 * @param strMinute
	 * @return
	 */	
	public static String getFromTimeValueForBL(String strHour, String strMinute ) {

		String retTimeValue = "";
		
		// 年月日いずれかが指定されている場合
		if ( (strHour != null && strHour.length() > 0) || (strMinute != null && strMinute.length() > 0) ) {
			
			if ( strHour == null || strHour.length() == 0 ) {
				retTimeValue = "00";
			} else {
				retTimeValue = strHour;
			}
			
			if ( strMinute == null || strMinute.length() == 0 ) {
				retTimeValue = retTimeValue + ":00";
			} else {
				retTimeValue = retTimeValue + ":" + strMinute;
			}
			
		}
		
		return retTimeValue;
	}
	
	/**
	 * 時分Toを業務ロジック向けに編集する
	 * 
	 * @param strHour
	 * @param strMinute
	 * @return
	 */	
	public static String getToTimeValueForBL(String strHour, String strMinute ) {

		String retTimeValue = "";
		
		// 年月日いずれかが指定されている場合
		if ( (strHour != null && strHour.length() > 0) || (strMinute != null && strMinute.length() > 0) ) {
			
			if ( strHour == null || strHour.length() == 0 ) {
				retTimeValue = "23";
			} else {
				retTimeValue = strHour;
			}
			
			if ( strMinute == null || strMinute.length() == 0 ) {
				retTimeValue = retTimeValue + ":59";
			} else {
				retTimeValue = retTimeValue + ":" + strMinute;
			}
			
		}
		
		return retTimeValue;
	}

	/**
	 * 日付を画面初期表示向けに編集する
	 * 
	 * @param strYear
	 * @param strMonth
	 * @param strDay
	 * @return
	 */	
	public static String getDefaultTimeValue(String strHour, String strMinute ) {
		
		String retTimeValue;
		
		if ( strHour != null && strHour.length() > 0 ) {
			retTimeValue = strHour; 
		} else {
			retTimeValue = "--"; 
		}
		
		if ( strMinute != null && strMinute.length() > 0 ) {
			retTimeValue = retTimeValue + strMinute; 
		} else {
			retTimeValue = retTimeValue + "--"; 
		}
		
		if (retTimeValue.length() != 4 ) retTimeValue = "----";
		
		return retTimeValue;
	}

}
