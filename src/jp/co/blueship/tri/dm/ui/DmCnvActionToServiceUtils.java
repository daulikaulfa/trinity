package jp.co.blueship.tri.dm.ui;

import java.util.HashSet;
import java.util.Set;

import jp.co.blueship.tri.dm.domain.beans.dop.dto.ReleaseConfirmBean;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.rm.constants.RelCtlScreenID;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.BaseInfoBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.BaseInfoInputBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.ConfigurationSelectBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.LotSelectBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.UnitSelectBean;
import jp.co.blueship.tri.rm.domain.rp.dto.FlowRelCtlEntryServiceBean;

/**
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public class DmCnvActionToServiceUtils {

	private static final ILog log = TriLogFactory.getInstance();
	public static final String sessionBean = "FlowRelDistributeEntryServiceBean";

	public static BaseInfoBean convertInputBaseInfoBean( IRequestInfo reqInfo ) {

		BaseInfoBean baseInfo = new BaseInfoBean();
		BaseInfoInputBean inputBean = new BaseInfoInputBean();

		LogHandler.debug( log , "  relSummary:=" + reqInfo.getParameter("relSummary") );
		LogHandler.debug( log , "  relContent:=" + reqInfo.getParameter("relContent") );
		inputBean.setRelSummary(reqInfo.getParameter("relSummary"));
		inputBean.setRelContent(reqInfo.getParameter("relContent"));
		baseInfo.setBaseInfoInputBean(inputBean);

		if ( log.isDebugEnabled() ) {
			LogHandler.debug( log , "FlowRelEntry:RelBaseInfoInput:referer" );
			LogHandler.debug( log , "  relSummary:=" + reqInfo.getParameter("relSummary") );
			LogHandler.debug( log , "  relContent:=" + reqInfo.getParameter("relContent") );
		}

		return baseInfo;
	}

	public static BaseInfoBean convertInputBaseInfoBean( ISessionInfo sesInfo ) {

		FlowRelCtlEntryServiceBean seBean = (FlowRelCtlEntryServiceBean)sesInfo.getAttribute(sessionBean);

		BaseInfoBean baseInfo = new BaseInfoBean();
		if (seBean != null){
			baseInfo = seBean.getBaseInfoBean();
		}

		if ( log.isDebugEnabled() ) {
			LogHandler.debug( log , "FlowRelEntry:RelBaseInfoInput:forward" );
			BaseInfoInputBean inputBean = baseInfo.getBaseInfoInputBean();
			if (inputBean != null) {
				LogHandler.debug( log , "  relSummary:=" + inputBean.getRelSummary() );
				LogHandler.debug( log , "  relContent:=" + inputBean.getRelContent() );
			}
		}

		return baseInfo;
	}

	public static ReleaseConfirmBean convertReleaseConfirmBean( ISessionInfo sesInfo ) {

		ReleaseConfirmBean confirm = new ReleaseConfirmBean();

		confirm.setRelSummary((String)sesInfo.getAttribute(SessionScopeKeyConsts.REL_SUMMARY));
		confirm.setRelContent((String)sesInfo.getAttribute(SessionScopeKeyConsts.REL_CONTENT));


		if ( log.isDebugEnabled() ) {
			LogHandler.debug( log , "FlowRelEntry:RelEntryBaseInfo:" );
			LogHandler.debug( log , "  relSummary:=" +  (String)sesInfo.getAttribute(SessionScopeKeyConsts.REL_SUMMARY)  );
			LogHandler.debug( log , "  relContent:=" + (String)sesInfo.getAttribute(SessionScopeKeyConsts.REL_CONTENT) );
			LogHandler.debug( log , "  relCopeSection:=" + (String)sesInfo.getAttribute(SessionScopeKeyConsts.REL_COPE_SECTION) );
			LogHandler.debug( log , "  relCopeUser:=" + (String)sesInfo.getAttribute(SessionScopeKeyConsts.REL_COPE_USER) );
			LogHandler.debug( log , "  distributionForm:=" + (String)sesInfo.getAttribute(SessionScopeKeyConsts.DISTRIBUTION_FORM) );
			LogHandler.debug( log , "  relApproveUser:=" +  (String)sesInfo.getAttribute(SessionScopeKeyConsts.REL_APPROVE_USER)  );
			LogHandler.debug( log , "  relApproveDate:=" +  (String)sesInfo.getAttribute(SessionScopeKeyConsts.REL_APPROVE_DATE)  );
		}

		return confirm;
	}

	public static BaseInfoBean convertEntryBaseInfoBean( ISessionInfo sesInfo ) {

		BaseInfoBean baseInfo = new BaseInfoBean();
		BaseInfoInputBean inputBean = new BaseInfoInputBean();

		inputBean.setRelSummary((String)sesInfo.getAttribute(SessionScopeKeyConsts.REL_SUMMARY));
		inputBean.setRelContent((String)sesInfo.getAttribute(SessionScopeKeyConsts.REL_CONTENT));
		baseInfo.setBaseInfoInputBean(inputBean);

		if ( log.isDebugEnabled() ) {
			LogHandler.debug( log , "FlowRelEntry:RelEntryBaseInfo:" );
			LogHandler.debug( log , "  relSummary:=" +  (String)sesInfo.getAttribute(SessionScopeKeyConsts.REL_SUMMARY)  );
			LogHandler.debug( log , "  relContent:=" + (String)sesInfo.getAttribute(SessionScopeKeyConsts.REL_CONTENT) );
			LogHandler.debug( log , "  relCopeSection:=" + (String)sesInfo.getAttribute(SessionScopeKeyConsts.REL_COPE_SECTION) );
			LogHandler.debug( log , "  relCopeUser:=" + (String)sesInfo.getAttribute(SessionScopeKeyConsts.REL_COPE_USER) );
			LogHandler.debug( log , "  distributionForm:=" + (String)sesInfo.getAttribute(SessionScopeKeyConsts.DISTRIBUTION_FORM) );
			LogHandler.debug( log , "  relApproveUser:=" +  (String)sesInfo.getAttribute(SessionScopeKeyConsts.REL_APPROVE_USER)  );
			LogHandler.debug( log , "  relApproveDate:=" +  (String)sesInfo.getAttribute(SessionScopeKeyConsts.REL_APPROVE_DATE)  );
		}

		return baseInfo;
	}

	public static ConfigurationSelectBean convertEntryConfigurationSelectBean( IRequestInfo reqInfo ) {

		ConfigurationSelectBean conf = new ConfigurationSelectBean();

		String savedPageNo = reqInfo.getParameter( "savedPageNo" ) ;
		if( ! TriStringUtils.isEmpty( savedPageNo ) ) {
//			conf.setSelectPageNo(Integer.parseInt(savedPageNo));
		}

		return conf;
	}

	public static LotSelectBean convertLotSelectBean( IRequestInfo reqInfo ) {

		LotSelectBean lot = new LotSelectBean();

		String savedPageNo = reqInfo.getParameter( "savedPageNo" ) ;
		if( ! TriStringUtils.isEmpty( savedPageNo ) ) {
//			conf.setSelectPageNo(Integer.parseInt(savedPageNo));
		}

		return lot;
	}

	public static UnitSelectBean convertUnitSelectBean( IRequestInfo reqInfo ) {

		UnitSelectBean unit = new UnitSelectBean();

		String savedPageNo = reqInfo.getParameter( "savedPageNo" ) ;
		if( ! TriStringUtils.isEmpty( savedPageNo ) ) {
//			conf.setSelectPageNo(Integer.parseInt(savedPageNo));
		}

		return unit;
	}

	/**
	 * レポート用にチェックを入れたリリース番号を保持する
	 * @param session ISessionInfo
	 * @param reqInfo IRequestInfo
	 * @return 保持されたリリース番号のセット
	 */
	public static Set<String> saveSelectedRelNoForReport(
										ISessionInfo session, IRequestInfo reqInfo ) {

		String forward = reqInfo.getParameter( "forward" );
		String referer = reqInfo.getParameter( "referer" );

		// リリース履歴一覧内遷移、またはリリース履歴詳細閲覧画面からの遷移以外はクリア
		if ( !RelCtlScreenID.HISTORY_LIST.equals( referer ) &&
				!RelCtlScreenID.HISTORY_DETAIL_VIEW.equals( referer )) {

			session.removeAttribute( SessionScopeKeyConsts.SELECTED_REPORT_NO );
		}
		if ( !"register".equals( forward ) && !RelCtlScreenID.HISTORY_LIST.equals( referer )) {

			session.removeAttribute( SessionScopeKeyConsts.VIEW_REPORT_NO );
		}


		// 選択されたリリース番号
		String[] relNoArray				= reqInfo.getParameterValues( "relNo[]" );
		Set<String> tmpSelectedRelNo	= new HashSet<String>();
		if ( null != relNoArray) {
			for ( String pjtNo : relNoArray ) {
				tmpSelectedRelNo.add( pjtNo );
			}
		}

		if ( "register".equals( forward ) && RelCtlScreenID.HISTORY_DETAIL_VIEW.equals( referer )) {
			return tmpSelectedRelNo;
		}


		// 画面に表示されていたリリース番号
		@SuppressWarnings("unchecked")
		Set<String> viewRelNoSet		=
			(Set<String>)session.getAttribute( SessionScopeKeyConsts.VIEW_REPORT_NO );
		// 選択済みのリリース番号
		@SuppressWarnings("unchecked")
		Set<String> selectedRelNoSet	=
			(Set<String>)session.getAttribute( SessionScopeKeyConsts.SELECTED_REPORT_NO );
		if ( null == selectedRelNoSet ) {
			selectedRelNoSet = tmpSelectedRelNo;
		}

		if ( null != viewRelNoSet ) {

			// 今回、選択されなかったリリース番号
			Set<String> unSelectedRelNo = new HashSet<String>();
			for ( String relNo : viewRelNoSet ) {

				if ( !tmpSelectedRelNo.contains(relNo )) {
					unSelectedRelNo.add( relNo );
				}
			}

			selectedRelNoSet.addAll		( tmpSelectedRelNo );
			selectedRelNoSet.removeAll	( unSelectedRelNo );
		}
		session.setAttribute( SessionScopeKeyConsts.SELECTED_REPORT_NO, selectedRelNoSet );

		return selectedRelNoSet;
	}
}
