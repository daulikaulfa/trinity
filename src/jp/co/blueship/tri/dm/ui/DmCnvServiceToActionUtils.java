package jp.co.blueship.tri.dm.ui;

import java.util.List;

import jp.co.blueship.tri.dm.domain.beans.dop.dto.ControlSelectBean;
import jp.co.blueship.tri.dm.domain.beans.dop.dto.ControlViewBean;
import jp.co.blueship.tri.dm.domain.beans.dop.dto.ReleaseConfirmBean;
import jp.co.blueship.tri.dm.domain.dop.dto.FlowRelDistributeEntryServiceBean;
import jp.co.blueship.tri.dm.domain.dop.dto.FlowRelDistributeTimerServiceBean;
import jp.co.blueship.tri.dm.domain.dop.dto.FlowRelDistributeTopMenuServiceBean;
import jp.co.blueship.tri.dm.domain.dop.dto.FlowRelDistributeTopMenuServiceBean.ReleaseViewBean;
import jp.co.blueship.tri.dm.ui.dop.rb.RelDistributeCompEntryResponseBean;
import jp.co.blueship.tri.dm.ui.dop.rb.RelDistributeCompTimerResponseBean;
import jp.co.blueship.tri.dm.ui.dop.rb.RelDistributeControlSelectResponseBean;
import jp.co.blueship.tri.dm.ui.dop.rb.RelDistributeEntryConfirmResponseBean;
import jp.co.blueship.tri.dm.ui.dop.rb.RelDistributeEnvSelectResponseBean;
import jp.co.blueship.tri.dm.ui.dop.rb.RelDistributeInfoInputResponseBean;
import jp.co.blueship.tri.dm.ui.dop.rb.RelDistributeTimerConfirmResponseBean;
import jp.co.blueship.tri.dm.ui.dop.rb.RelDistributeTimerSetResponseBean;
import jp.co.blueship.tri.dm.ui.dop.rb.RelDistributeTopResponseBean;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.constants.status.RmRpStatusId;
import jp.co.blueship.tri.fw.constants.status.RmRpStatusIdForExecData;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.BaseInfoBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.BaseInfoInputBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.ConfigurationSelectBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.ConfigurationViewBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.LotSelectBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.LotViewBean;
import jp.co.blueship.tri.rm.domain.rp.dto.FlowRelCtlUnitDetailViewServiceBean;
import jp.co.blueship.tri.rm.ui.rp.rb.RelCtlUnitDetailViewResponseBean;

/**
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class DmCnvServiceToActionUtils {

	private static final ILog log = TriLogFactory.getInstance();

	public static RelDistributeTopResponseBean convertRelDistributeTopResponseBean(
			FlowRelDistributeTopMenuServiceBean bean, IRequestInfo reqInfo, ISessionInfo sesInfo ) {

		List<ReleaseViewBean> viewBeanList = bean.getReleaseViewBeanList();
		for ( ReleaseViewBean viewBean : viewBeanList ) {
			if ( null == viewBean.getEnvName() || viewBean.getEnvName().trim().equals( "" )) {
				// ちゃんとした環境ではないと思うが、環境名が設定されていないとテーブルがつぶれてしまう
				viewBean.setEnvName( "N/A" );
			}
			if (null != viewBean.getLinkNo() && viewBean.getLinkNo().length() > 0) {
				StringBuilder sb = new StringBuilder();
				sb.append("(");
				sb.append(viewBean.getLinkNo());
				sb.append(")");
				viewBean.setLinkNo(sb.toString());
			}
		}

		RelDistributeTopResponseBean resBean = new RelDistributeTopResponseBean();
		resBean.setReleaseViewBeanList( viewBeanList );
		resBean.setDrmsMessage(bean.getDrmsMessage());

		return resBean;

	}

	public static RelDistributeInfoInputResponseBean convertRelBaseInfoInputResponseBean(FlowRelDistributeEntryServiceBean bean, ISessionInfo sesInfo) {

		RelDistributeInfoInputResponseBean resBean = new RelDistributeInfoInputResponseBean();

		BaseInfoBean baseInfoBean = new BaseInfoBean();
		BaseInfoInputBean inputBean = new BaseInfoInputBean();
		if (bean != null) {
			baseInfoBean = bean.getBaseInfoBean();
			inputBean = baseInfoBean.getBaseInfoInputBean();
		}

		if (inputBean != null ) {
			resBean.setRelContent(inputBean.getRelContent());
			resBean.setRelSummary(inputBean.getRelSummary());
			LogHandler.debug( log , "convertRelBaseInfoInputResponseBean  relContent:=" + inputBean.getRelContent() );
			LogHandler.debug( log , "convertRelBaseInfoInputResponseBean  relSummary:=" + inputBean.getRelSummary() );
		}

		return resBean;

	}

	public static RelDistributeEnvSelectResponseBean convertRelLotSelectResponseBean(FlowRelDistributeEntryServiceBean bean, IRequestInfo reqInfo, ISessionInfo sesInfo) {

		RelDistributeEnvSelectResponseBean resBean = new RelDistributeEnvSelectResponseBean();
		LotSelectBean lotBean = bean.getLotSelectBean();
		resBean.setLotViewBeanList(lotBean.getLotViewBeanList());
		resBean.setSelectedLotNo(bean.getSelectedLotNo());

		ConfigurationSelectBean confBean = bean.getConfSelectBean();
		resBean.setConfViewBeanList(confBean.getConfViewBeanList());
		resBean.setSelectedEnvNo(bean.getSelectedEnvNo());

		LogHandler.debug( log , "convertRelLotSelectResponseBean  windowsId:=" + resBean.getWindowsId() );

		return resBean;

	}

	public static RelDistributeControlSelectResponseBean convertRelControlSelectResponseBean(
			FlowRelDistributeEntryServiceBean bean, IRequestInfo reqInfo, ISessionInfo sesInfo ) {

		ControlSelectBean controlSelectBean = bean.getControlSelectBean();

		RelDistributeControlSelectResponseBean resBean = new RelDistributeControlSelectResponseBean();
		resBean.setControlViewBeanList( controlSelectBean.getControlViewBeanList() );

		if ( bean.getSelectedControlNo() != null ) {
			resBean.setSelectedRelNo		( bean.getSelectedControlNo() );
		}

//		resBean.setPageNoNum(resourceInfo.getPageNoInfoView().getMaxPageNo());
//		resBean.setSelectPageNo(resourceInfo.getPageNoInfoView().getSelectPageNo());

//		if ( StatusFlg.on.value().equals( DesignDefineUtil.getValue(
//				DesignRelDefineId.multiSelectable, RelScreenID.RELEASE_UNIT_SELECT ))) {
//			resBean.setMultiSelectable( true );
//		}

		return resBean;

	}

	public static RelDistributeEntryConfirmResponseBean convertRelEditConfirmResponseBean(FlowRelDistributeEntryServiceBean bean, IRequestInfo reqInfo, ISessionInfo sesInfo) {

		RelDistributeEntryConfirmResponseBean resBean = new RelDistributeEntryConfirmResponseBean();

		// リリース基本情報を表示
		BaseInfoInputBean baseBean = bean.getBaseInfoBean().getBaseInfoInputBean();
		resBean.setDistSummary(baseBean.getRelSummary());
		resBean.setDistContent(baseBean.getRelContent());

		// リリース基本情報を表示
		ReleaseConfirmBean relBean = bean.getReleaseConfirmBean();
		resBean.setDistSummary(relBean.getRelSummary());
		resBean.setDistContent(relBean.getRelContent());
		resBean.setConfNo(relBean.getConfViewBean().getConfNo());
		resBean.setConfName(relBean.getConfViewBean().getConfName());
		resBean.setConfSummary(relBean.getConfViewBean().getConfSummary());
		resBean.setLotNo(relBean.getLotViewBean().getLotNo());
		resBean.setLotName(relBean.getLotViewBean().getLotName());
		resBean.setInputUser(relBean.getLotViewBean().getInputUser());
		resBean.setInputUserId(relBean.getLotViewBean().getInputUserId());
		resBean.setInputDate( relBean.getLotViewBean().getInputDate() );
		resBean.setRelNo(relBean.getControlViewBean().getRelNo());
		resBean.setRelSummary(relBean.getControlViewBean().getSummary());
		resBean.setGenerateDate( relBean.getControlViewBean().getGenerateDate() );

		//rmi通信＝商用シェルではない
		resBean.setRmiCheck( ! bean.isRmi() );

		// ビルドパッケージ選択で選択された値を表示
//		resBean.setResourceViewBeanList(bean.getConfSelectBean().getConfViewBeanList());

//		RmCnvServiceToActionUtilsからコピーしてきたゴミコードの模様.コピー元では既にコメントアウトされている
//		また"MESREL0008"も既に存在しない
//		if( reqInfo.getParameter( "referer" ).equals( RelCtlScreenID.ENTRY_CONFIRM )){
//			List<String> msg =
//				ContextAdapterFactory.getMessageContextAdapter().getMessage( bean.getLanguage(),
//					new BusinessException("MESREL0008", new String[]{ bean.getRelNo() }));
//
//			resBean.setMessageList(msg);
//			sesInfo.setAttribute(SessionScopeKeyConsts.FLOW_ACTION_ID, "FlowRelModifyService");
//		}

		return resBean;
	}

	public static RelDistributeCompEntryResponseBean convertRelCompRequestResponseBean(FlowRelDistributeEntryServiceBean bean, IRequestInfo reqInfo, ISessionInfo sesInfo) {

		RelDistributeCompEntryResponseBean resBean = new RelDistributeCompEntryResponseBean();

		// リリース番号を表示
		resBean.setRelNo(bean.getRelNo());
		resBean.setDrmsMessage(bean.getDrmsMessage());

		return resBean;
	}

	public static RelDistributeTimerSetResponseBean convertRelDistributeTimerSetResponseBean(FlowRelDistributeTimerServiceBean bean, IRequestInfo reqInfo, ISessionInfo sesInfo) {

		RelDistributeTimerSetResponseBean resBean = new RelDistributeTimerSetResponseBean();

		// リリース番号を表示
		resBean.setLotViewBeanList(bean.getLotViewBeanList());
		resBean.setConfViewBeanList(bean.getConfViewBeanList());
		resBean.setSelectedEnvNo(bean.getSelectedEnvNo());
		resBean.setSelectedLotNo(bean.getSelectedLotNo());
		String timer[] = bean.getTimer();
		if (null != timer) {
			StringBuilder sb = new StringBuilder();
			sb.append("[");
			int i = 0;
			for (String value : timer) {
				if ( RmRpStatusId.Unprocessed.equals( value ) ) {
					if (i >0) {
						sb.append(",");
					}
					sb.append("REL_UN_PROCESS");
				} else if ( RmRpStatusIdForExecData.CreatingReleasePackage.equals( value ) ) {
					if (i >0) {
						sb.append(",");
					}
					sb.append("REL_ACTIVE");
				} else if ( RmRpStatusId.ReleasePackageCreated.equals( value ) ) {
					if (i >0) {
						sb.append(",");
					}
					sb.append("REL_COMPLETE");
				}
				i++;
			}
			sb.append("]");
			String status = sb.toString();
			resBean.setTimer(status);
		} else {
			resBean.setTimer("");
		}
		String timerSetting = bean.getTimerSetting();
		if (null == timerSetting) {
			timerSetting = "set";
		}
		resBean.setTimerSetting(timerSetting);
		resBean.setTimerSetViewBean(bean.getTimerSetViewBean());
		resBean.setTimerDate(bean.getTimerDate());
		resBean.setTimerSummary(bean.getTimerSummary());
		resBean.setTimerContent(bean.getTimerContent());
		resBean.setControlViewBeanList(bean.getControlViewBeanList());
		resBean.setDistributeViewBeanList(bean.getDistributeViewBeanList());
		resBean.setSelectedRelNo(bean.getSelectedDistNo());

		return resBean;
	}

	public static RelDistributeTimerConfirmResponseBean convertRelDistributeTimerConfirmResponseBean(FlowRelDistributeTimerServiceBean bean, IRequestInfo reqInfo, ISessionInfo sesInfo) {

		// 設定確認情報を表示
		RelDistributeTimerConfirmResponseBean resBean = new RelDistributeTimerConfirmResponseBean();
		BaseInfoInputBean baseBean = bean.getBaseInfoBean().getBaseInfoInputBean();
		ConfigurationViewBean confBean = bean.getConfViewBean();
		LotViewBean lotBean = bean.getLotViewBean();
		ControlViewBean controlBean = bean.getControlViewBean();
		resBean.setDistNo		( bean.getDistNo( ) );
		if (bean.getTimerSetting().equals("now")) {
			resBean.setTimerDate("即時");
		} else if (bean.getTimerSetting().equals("cancel")) {
			resBean.setTimerDate("取消");
		} else {

			resBean.setTimerDate	( TriDateUtils.convertViewDateFormat(bean.getTimerDate()) );
		}
		resBean.setTimerSummary	( bean.getTimerSummary());
		resBean.setTimerContent	( bean.getTimerContent());
		resBean.setDistSummary	( baseBean.getRelSummary() );
		resBean.setDistContent	( baseBean.getRelContent() );
		resBean.setConfNo		( confBean.getConfNo() );
		resBean.setConfName		( confBean.getConfName() );
		resBean.setConfSummary	( confBean.getConfSummary() );
		resBean.setLotNo		( lotBean.getLotNo() );
		resBean.setLotName		( lotBean.getLotName() );
		resBean.setInputUser	( lotBean.getInputUser() );
		resBean.setInputUserId	( lotBean.getInputUserId() );
		resBean.setInputDate	( lotBean.getInputDate() );
		resBean.setRelNo		( controlBean.getRelNo() );
		resBean.setRelSummary	( controlBean.getSummary() );
		resBean.setGenerateDate	( controlBean.getGenerateDate() );

		// リリース番号を表示

		return resBean;
	}

	public static RelDistributeCompTimerResponseBean convertRelDistributeCompTimerResponseBean(FlowRelDistributeTimerServiceBean bean, IRequestInfo reqInfo, ISessionInfo sesInfo) {

		RelDistributeCompTimerResponseBean resBean = new RelDistributeCompTimerResponseBean();

		// リリース番号を表示
		resBean.setDistNo( bean.getDistNo() );
		resBean.setDrmsMessage( bean.getDrmsMessage() );
		resBean.setRmi( bean.isRmi());

		return resBean;
	}

	public static RelCtlUnitDetailViewResponseBean convertRelUnitDetailViewResponseBean(
			FlowRelCtlUnitDetailViewServiceBean bean ) {

		RelCtlUnitDetailViewResponseBean resBean = new RelCtlUnitDetailViewResponseBean();

		resBean.setLotName				( bean.getLotName() );
		resBean.setSelectedBuildNo		( bean.getSelectedBuildNo() );
		resBean.setReleaseViewBeanList	( bean.getReleaseViewBeanList() );

		return resBean;
	}

/*
	public static RelGenerateDetailViewResponseBean convertRelGenerateDetailViewResponseBean(
			FlowRelReleaseDetailViewServiceBean bean ) {

		RelGenerateDetailViewResponseBean resBean = new RelGenerateDetailViewResponseBean();

		resBean.setReleaseStatusMsg	( bean.getReleaseDetailViewBean().getRelStatusMsg() );
		resBean.setRelUser			( bean.getReleaseDetailViewBean().getCopeUser() );
		resBean.setRelUserId			( bean.getReleaseDetailViewBean().getCopeUserId() );
		resBean.setRelNo			( bean.getReleaseDetailViewBean().getRelNo() );

		List<List<String>> imgPathList = new ArrayList<List<String>>();
		List<RelProcessViewBean> procBeanList = bean.getReleaseDetailViewBean().getRelProcessViewBeanList();
		for ( RelProcessViewBean procBean : procBeanList ) {
			imgPathList.add(FluentList.from().asList()procBean.getImg()));
		}
		resBean.setImagePathList	( imgPathList );
		resBean.setReloadInterval	( DesignProjectUtil.getValue( DesignProjectRelUnitDefineId.reloadInterval ));

		return resBean;
	}
*/

}
