package jp.co.blueship.tri.dm;

import java.io.File;

import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByDirectory;
import jp.co.blueship.tri.fw.constants.DmDesignEntryKeyByCommercialShell;
import jp.co.blueship.tri.fw.constants.DmDesignEntryKeyByDealAssetFtp;
import jp.co.blueship.tri.fw.constants.UmDesignEntryKeyByCommon;
import jp.co.blueship.tri.fw.dao.orm.ILimit;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.DmMessageId;
import jp.co.blueship.tri.fw.svc.beans.dto.IPageNoInfo;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;

/**
 * 業務を扱うユーティリティークラスです。
 * <br>返却申請の資産格納パスを取得など、各AddOn共通の機能が定義されています。
 * DesignBusinessRuleUtilsの派生クラスです。依存性を解消するために作成しました。
 *
 * @version V3L10R02
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 */
public class DmDesignBusinessRuleUtils {
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	/**
	 * ページ制御情報からページ番号情報に設定します。
	 * @param dest ページ番号情報
	 * @param src ページ制御
	 * @return ページ番号情報を戻します。
	 */
	public static final IPageNoInfo convertPageNoInfo( IPageNoInfo dest, ILimit src ) {

		if ( null == dest ){
			throw new TriSystemException( DmMessageId.DM005005S );
		}
		if ( null == src ) {
			dest.setMaxPageNo( new Integer(0) );
			dest.setSelectPageNo( new Integer(0) );
			return dest;
		}

		dest.setMaxPageNo( new Integer(src.getPageBar().getMaximum()) );
		dest.setSelectPageNo( new Integer(src.getPageBar().getValue()) );

		dest.setViewRows(src.getViewRows());
		dest.setMaxRows(src.getMaxRows());

		return dest;
	}

	/**
	 * ワークスペースフォルダを取得します。
	 *
	 * @param entity プロジェクトエンティティ
	 * @return 取得した情報を戻します。
	 * @throws TriSystemException マスタ情報にシステム運用上の誤りがある場合
	 */
	private static final File getWorkspacePath( ILotEntity entity ) throws TriSystemException {

		if ( null == entity ){
			throw new TriSystemException( DmMessageId.DM005006S );
		}
		if ( null == entity.getWsPath() ){
			throw new TriSystemException( DmMessageId.DM004017F );
		}
		return new File( entity.getWsPath() );
	}

	/**
	 * 履歴フォルダを取得します。
	 *
	 * @param entity プロジェクトエンティティ
	 * @return 取得した情報を戻します。
	 * @throws TriSystemException マスタ情報にシステム運用上の誤りがある場合
	 */
	private static final File getHistoryPath( ILotEntity entity ) throws TriSystemException {
		File file = null;

		if ( null == entity ){
			throw new TriSystemException( DmMessageId.DM005006S );
		}
		if ( null == entity.getHistPath() ){
			throw new TriSystemException( DmMessageId.DM004018F , entity.getHistPath());
		}
		file = new File( entity.getHistPath() );

		if ( ! file.isDirectory() ) {
			throw new TriSystemException( DmMessageId.DM004019F , file.getPath());
		}

		return file;
	}


	/**
	 * リリースパッケージのＤＳＬ資産の管理フォルダを取得します。
	 *
	 * @param lotEntity ロットエンティティ
	 * @param relEntity リリースエンティティ
	 * @return 取得した情報を戻します。
	 * @throws TriSystemException マスタ情報にシステム運用上の誤りがある場合
	 */
	private static final File getHistoryRelDSLPath( ILotEntity lotEntity, IRpEntity relEntity ) throws TriSystemException {
		File file = null;

		File historyFile = getHistoryPath( lotEntity );

		if ( null == relEntity ){
			throw new TriSystemException( DmMessageId.DM005007S );
		}
		String relativePath = TriStringUtils.linkPath( relEntity.getBldEnvId(), relEntity.getRpId() );

		file = new File( historyFile, TriStringUtils.linkPath(
				sheet.getValue( AmDesignEntryKeyByDirectory.relDSLRelationPath ),
				relativePath )
				);

		return file;
	}

	/**
	 * リリースパッケージのＤＳＬ資産の配付資源ファイル名を取得します。
	 *
	 * @param lotEntity ロットエンティティ
	 * @param relEntity リリースエンティティ
	 * @return 取得した情報を戻します。
	 * @throws TriSystemException マスタ情報にシステム運用上の誤りがある場合
	 */
	public static final File getHistoryRelDSLFilePath( ILotEntity lotEntity, IRpEntity relEntity ) throws TriSystemException {
		File file = null;

		File historyFile = getHistoryRelDSLPath( lotEntity, relEntity );

		String historyFileName = TriStringUtils.linkPath(historyFile.getPath(), relEntity.getBldEnvId());
		file = new File( historyFileName +  sheet.getValue( DmDesignEntryKeyByDealAssetFtp.srcExtension ) );

		return file;
	}

	/**
	 * サービス単位のログの管理フォルダを取得します。
	 *
	 * @param lotEntity ロットエンティティ
	 * @param flowAction 実行サービス名ID
	 * @return 取得した情報を戻します。
	 * @throws TriSystemException マスタ情報にシステム運用上の誤りがある場合
	 */
	//log
	public static final File getWorkspaceLogPath( ILotEntity lotEntity, String flowActionId ) throws TriSystemException {
		File file = null;

		File workspaceFile = getWorkspacePath( lotEntity );

		if ( null == flowActionId ){
			throw new TriSystemException( DmMessageId.DM005008S );
		}
		file = new File( workspaceFile, TriStringUtils.linkPath(
												sheet.getValue( UmDesignEntryKeyByCommon.logRelationPath ),
												flowActionId )
										 );

		return file;
	}

	/**
	 * テンプレートフォルダを取得します。
	 *
	 * @return 取得した情報を戻します。
	 * @throws TriSystemException マスタ情報にシステム運用上の誤りがある場合
	 */
	public static final File getTemplatePath() throws TriSystemException {

		return new File(	sheet.getValue( UmDesignEntryKeyByCommon.homeTopPath ),
							sheet.getValue( UmDesignEntryKeyByCommon.templatesRelativePath ));
	}
	
	public static final File getShellOutputPath() throws TriSystemException {

		return new File(	sheet.getValue( UmDesignEntryKeyByCommon.homeTopPath ),
							sheet.getValue( DmDesignEntryKeyByCommercialShell.shellOutputBaseRelativePath ));
	}

}
