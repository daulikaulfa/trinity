package jp.co.blueship.tri.dm.uix.deploy;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import jp.co.blueship.tri.dm.domainx.deploy.dto.FlowDeploymentJobListServiceBean;
import jp.co.blueship.tri.dm.domainx.deploy.dto.FlowDeploymentJobListServiceBean.RequestOption;
import jp.co.blueship.tri.dm.domainx.deploy.dto.FlowDeploymentJobListServiceBean.SearchCondition;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriModelAttributes;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowSearchFilterCreationServiceBean;

/**
 *
 * @version V4.00.00
 * @author Akahoshi
 * 
 * @version V4.01.00
 * @author Cuong Nguyen
 *
 */
@Controller
@RequestMapping("/job/deploy")
public class DeploymentJobListController extends TriControllerSupport<FlowDeploymentJobListServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.DmFlowDeploymentJobListService;
	}


	@Override
	protected FlowDeploymentJobListServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowDeploymentJobListServiceBean bean = new FlowDeploymentJobListServiceBean();
		return bean;
	}


	@RequestMapping("/list/refresh")
	public String refresh(FlowDeploymentJobListServiceBean bean , TriModel model){
		String view = TriTemplateView.MainTemplate.value();

		try {
			bean.getParam().setRequestType(RequestType.onChange);
			this.execute(this.getServiceId(), bean, model.setRedirect(true));

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("pagination",bean.getPage())
			.addAttribute("selectedMenu", "releaseMenu")
			.addAttribute("selectedSubMenu"  ,"deployjobSubmenu")
			.addAttribute("view", TriView.DeploymentJobList.value())
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}


	@RequestMapping("/list/redirect")
	public String redirect(FlowDeploymentJobListServiceBean bean , TriModel model){
		return index(bean , model.setRedirect(true));
	}


	@RequestMapping("/list/search")
	public String search(FlowDeploymentJobListServiceBean bean , TriModel model){
		return index(bean , model);
	}


	@RequestMapping(value = {"","/", "/list"})
	public String index(FlowDeploymentJobListServiceBean bean , TriModel model){
		String view = TriTemplateView.MainTemplate.value();

		try {
			this.mapping(bean, model);
			this.execute(this.getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("pagination",bean.getPage())
			.addAttribute("selectedMenu", "releaseMenu")
			.addAttribute("selectedSubMenu"  ,"deployjobSubmenu")
			.addAttribute("view", TriView.DeploymentJobList.value())
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}


	private void mapping(FlowDeploymentJobListServiceBean bean , TriModel model){

		ISessionInfo sesInfo = model.getSessionInfo();
		IRequestInfo requestInfo = model.getRequestInfo();

		if(RequestType.init.equals(bean.getParam().getRequestType())){
			if( TriStringUtils.isNotEmpty(bean.getParam().getSelectedLotId()) && 
					this.getSessionSelectedLot(sesInfo, bean) != bean.getParam().getSelectedLotId()){
				bean.getParam().setSearchCondition(bean.new SearchCondition() );
			}
			bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
			bean.getParam().setRequestOption( RequestOption.none );
		}else if(RequestType.onChange.equals(bean.getParam().getRequestType())){
			boolean isManual = true;
			String requestJob =	requestInfo.getParameter("requestJob");
			if(requestJob != null && requestJob.equals("automatic")){
			isManual = false;
		}
			SearchCondition condition = bean.getParam().getSearchCondition()
					.setEnvId			( requestInfo.getParameter("envId"))
					.setStsId			( requestInfo.getParameter("stsId"))
					.setCtgId			( requestInfo.getParameter("ctgId"))
					.setMstoneId		( requestInfo.getParameter("mstoneId"))
					.setKeyword			( requestInfo.getParameter("keyword"))
					;
			if( TriStringUtils.isNotEmpty(requestInfo.getParameter("selectedPageNo") )){
				condition.setSelectedPageNo(Integer.parseInt(requestInfo.getParameter("selectedPageNo")));
			}else{
				condition.setSelectedPageNo(0);
			}
			bean.getParam().setSearchCondition(condition);
			bean.getParam().getInputInfo().setManual(isManual);
			String requestOption = requestInfo.getParameter("requestOption");
			if (RequestOption.search.equals(requestOption)) {
				bean.getParam().setRequestOption( RequestOption.search );
			} else if (RequestOption.getDeploymentInfo.equals(requestOption)) {
				bean.getParam().setRequestOption( RequestOption.getDeploymentInfo );
			} else {
				bean.getParam().setRequestOption( RequestOption.none );
			}
			
		}

	}

	@RequestMapping(value = "/filter/save" )
	public String save(FlowDeploymentJobListServiceBean bean, TriModel model ){
		String view = "redirect:/searchfilter/create";


		FlowDeploymentJobListServiceBean.SearchCondition searchConditionTemp = (FlowDeploymentJobListServiceBean.SearchCondition)
				bean.getParam().getSearchCondition();

		try{
			FlowSearchFilterCreationServiceBean searchFilterBean = new FlowSearchFilterCreationServiceBean();

			ISessionInfo sesInfo = model.getSessionInfo();
			searchFilterBean.getParam().getInputInfo()
				.setLotId(this.getSessionSelectedLot(sesInfo, bean))
				.setFilterNm( model.getRequestInfo().getParameter("filterNm") )
				.setServiceId( getServiceId().value() )
				.setSearchFilter( searchConditionTemp );
			model.getRedirectAttributes().addFlashAttribute( TriModelAttributes.RedirectFilter.value() , searchFilterBean );

		} catch ( Exception e ) {
			ExceptionUtils.printStackTrace(e);
		}



		return view;
	}


	@RequestMapping(value = "/filter/search" )
	public String receive(FlowDeploymentJobListServiceBean bean, TriModel model ){

		model.setRedirect(true);
		String json = null;
		if ( model.isRedirect() && model.containsKey(TriModelAttributes.RedirectFilter) ) {
			json = model.valueOf(TriModelAttributes.RedirectFilter);
			ISessionInfo sesInfo = model.getSessionInfo();
			bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
		}

		Gson gson = new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create();
		FlowDeploymentJobListServiceBean.SearchCondition condition = gson.fromJson(json, FlowDeploymentJobListServiceBean.SearchCondition.class);

		bean.getParam().setSearchCondition(condition);

		String view = TriTemplateView.MainTemplate.value();
		try {
			this.mapping(bean, model);
			this.execute(this.getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}
		model.getModel().addAttribute("pagination",bean.getPage());
		
		model.getModel()
			.addAttribute("selectedMenu", "releaseMenu")
			.addAttribute("selectedSubMenu"  ,"deployjobSubmenu")
			.addAttribute("view", TriView.DeploymentJobList.value())
			.addAttribute("result", bean)
		;
		setPrev(model);
		return view;
	}
}
