package jp.co.blueship.tri.dm.uix.deploy;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.dm.domainx.deploy.dto.FlowDeploymentJobDetailsServiceBean;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;

/**
 *
 * @version V4.01.00
 * @author Chung
 *
 */
@Controller
@RequestMapping("/job/deploy/details/print")
public class PrintDeploymentJobDetailsController extends TriControllerSupport<FlowDeploymentJobDetailsServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.DmFlowPrintDeploymentJobDetailsService;
	}

	@Override
	protected FlowDeploymentJobDetailsServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowDeploymentJobDetailsServiceBean bean = new FlowDeploymentJobDetailsServiceBean();
		return bean;
	}

	@RequestMapping
	public String print(FlowDeploymentJobDetailsServiceBean bean , TriModel model){
		String view = TriTemplateView.PrintTemplate.value();

		try {
			this.mapping(bean, model);
			this.execute(this.getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.PrintDeploymentJobDetails.value())
			.addAttribute("result", bean)
		;

		return view;
	}

	private void mapping(FlowDeploymentJobDetailsServiceBean bean , TriModel model){
		IRequestInfo requestInfo = model.getRequestInfo();
		bean.getParam().setSelectedMgtVer(requestInfo.getParameter("mgtVer"));
		
		boolean manual = true;
			String requestJob =	requestInfo.getParameter("requestJob");
		if(requestJob != null && requestJob.equalsIgnoreCase("automatic")){
			manual = false;
		}
		bean.getParam().getInputInfo().setManual( manual );
	}
}
