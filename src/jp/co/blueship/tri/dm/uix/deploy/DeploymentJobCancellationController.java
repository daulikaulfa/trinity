package jp.co.blueship.tri.dm.uix.deploy;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.dm.domainx.deploy.dto.FlowDeploymentJobCancellationServiceBean;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;


/**
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 * @version V4.01.00
 * @author Cuong Nguyen
 *
 */
@Controller
@RequestMapping("/job/deploy/cancel")
public class DeploymentJobCancellationController extends TriControllerSupport<FlowDeploymentJobCancellationServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.DmFlowDeploymentJobCancellationService;
	}

	@Override
	protected FlowDeploymentJobCancellationServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowDeploymentJobCancellationServiceBean bean = new FlowDeploymentJobCancellationServiceBean();
		return bean;
	}

	@RequestMapping()
	public String cancel(FlowDeploymentJobCancellationServiceBean bean , TriModel model){

		String view="redirect:/job/deploy/edit/redirect";

		try {
			this.mapping(bean, model);
			this.execute(this.getServiceId(), bean, model);


		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}
		
		model.getRedirectAttributes().addFlashAttribute( "result", bean );

		return view;
	}

	private void mapping(FlowDeploymentJobCancellationServiceBean bean , TriModel model){
		IRequestInfo requestInfo = model.getRequestInfo();

		bean.getParam().setSelectedMgtVer(requestInfo.getParameter("mgtVer"));

		boolean manual = true;
		String requestJob =	requestInfo.getParameter("requestJob");
		if(requestJob != null && requestJob.equalsIgnoreCase("automatic")){
			manual = false;
		}
		bean.getParam().getInputInfo().setManual( manual );
	}
}
