package jp.co.blueship.tri.dm.uix.deploy;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.dm.constants.TimerSettings;
import jp.co.blueship.tri.dm.domainx.deploy.dto.FlowDeploymentJobCreationServiceBean;
import jp.co.blueship.tri.dm.domainx.deploy.dto.FlowDeploymentJobCreationServiceBean.DeploymentJobEditInputBean;
import jp.co.blueship.tri.dm.domainx.deploy.dto.FlowDeploymentJobCreationServiceBean.RequestOption;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;

/**
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 */
@Controller
@RequestMapping("/job/deploy/create")
public class DeploymentJobCreationController extends TriControllerSupport<FlowDeploymentJobCreationServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.DmFlowDeploymentJobCreationService;
	}

	@Override
	protected FlowDeploymentJobCreationServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowDeploymentJobCreationServiceBean bean = new FlowDeploymentJobCreationServiceBean();
		return bean;
	}


	@RequestMapping("/refresh")
	public String refresh(FlowDeploymentJobCreationServiceBean bean , TriModel model){
		String view = TriTemplateView.MainTemplate.value();

		try {
			this.execute(this.getServiceId(), bean, model.setRedirect(true));

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("selectedMenu", "releaseMenu")
			.addAttribute("selectedSubMenu"  ,"deployjobSubmenu")
			.addAttribute("view", TriView.DeploymentJobCreation.value())
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}

	@RequestMapping(value = "/refreshCategory")
	public String refreshCategory( @ModelAttribute("ctgId") String ctgId, FlowDeploymentJobCreationServiceBean bean, TriModel model ){
		bean.getParam().setRequestType(RequestType.onChange);
		bean.getParam().setRequestOption(RequestOption.refreshCategory);
		bean.getParam().getInputInfo().setCtgId( ctgId );
		LogHandler.debug( TriLogFactory.getInstance(), "ctgId:=" + ctgId );
		bean.getParam().getInputInfo().setCtgId( ctgId );

		return creation( bean , model.setRedirect(true) );
	}

	@RequestMapping(value = "/refreshMilestone")
	public String refreshMilestone( @ModelAttribute("mstoneId") String mstoneId, FlowDeploymentJobCreationServiceBean bean, TriModel model ){
		bean.getParam().setRequestType(RequestType.onChange);
		bean.getParam().setRequestOption(RequestOption.refreshMilestone);
		bean.getParam().getInputInfo().setMstoneId( mstoneId );
		LogHandler.debug( TriLogFactory.getInstance(), "mstoneId:=" + mstoneId );
		bean.getParam().getInputInfo().setMstoneId( mstoneId );

		return creation( bean , model.setRedirect(true) );
	}

	@RequestMapping
	public String creation(FlowDeploymentJobCreationServiceBean bean , TriModel model){
		String view = TriTemplateView.MainTemplate.value();

		try {
			this.mapping(bean, model);
			this.execute(this.getServiceId(), bean, model);

			if(bean.getResult().isCompleted()){

				TimerSettings timerSetting = bean.getParam().getInputInfo().getTimerSettings();

				//timerSetting
				if( timerSetting.value().equals("immediately") ||
					timerSetting.value().equals("timerSettins")){

					this.execute(this.getServiceId(), bean, model);
				}

				model.getRedirectAttributes().addFlashAttribute( "result", bean );
				view = "redirect:/job/deploy/list/refresh";
			}

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("selectedMenu", "releaseMenu")
			.addAttribute("selectedSubMenu"  ,"deployjobSubmenu")
			.addAttribute("view", TriView.DeploymentJobCreation.value())
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}

	private void mapping(FlowDeploymentJobCreationServiceBean bean , TriModel model){

		

		ISessionInfo sesInfo = model.getSessionInfo();
		IRequestInfo requestInfo = model.getRequestInfo();

		if(RequestType.init.equals(bean.getParam().getRequestType())){
			bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
			String requestJob = requestInfo.getParameter("requestJob");
			boolean manual = true;
		    if(requestJob != null && requestJob.equalsIgnoreCase("automatic")){
		            manual = false;
		        }
		    bean.getParam().getInputInfo().setManual( manual );
		}

		if(RequestType.submitChanges.equals(bean.getParam().getRequestType())){
			DeploymentJobEditInputBean inputInfo = bean.getParam().getInputInfo()
					.setSummary			( requestInfo.getParameter("summary"))
					.setContents		( requestInfo.getParameter("contents"))
					.setRelEnvId		( requestInfo.getParameter("relEnvId"))
					.setCtgId			( requestInfo.getParameter("catgId"))
					.setMstoneId		( requestInfo.getParameter("mistoneId"))
					.setRpId			( requestInfo.getParameter("rpId"))
					.setTimerSettings	( TimerSettings.value(requestInfo.getParameter("timerSettings")))
					.setTimerDate		( requestInfo.getParameter("timerDate"))
					.setTimerHour		( requestInfo.getParameter("timerHour"))
					.setTimerMinute		( requestInfo.getParameter("timerMinute"))
					.setTimerSummary	( requestInfo.getParameter("timerSummary"))
					.setTimerContents	( requestInfo.getParameter("timerDetail"))
					.setManual			( "true".equals(requestInfo.getParameter("manualJob")))
			;
			bean.getParam().setInputInfo(inputInfo);
		}

		if(RequestType.onChange.equals(bean.getParam().getRequestType())){
			DeploymentJobEditInputBean inputInfo = bean.getParam().getInputInfo()
					.setRelEnvId		( requestInfo.getParameter("relEnvId"))
			;
			String requestOption = requestInfo.getParameter("requestOption");
			if ( RequestOption.selectEnvironment.equals(requestOption) ) {
				bean.getParam().setRequestOption( RequestOption.selectEnvironment );
				
			} else if ( RequestOption.refreshCategory.equals(requestOption) ) {
				bean.getParam().setRequestOption( RequestOption.refreshCategory );
				
			} else if ( RequestOption.refreshMilestone.equals(requestOption) ) {
				bean.getParam().setRequestOption( RequestOption.refreshMilestone );
				
			}
			bean.getParam().setInputInfo(inputInfo);
		}
	}
}
