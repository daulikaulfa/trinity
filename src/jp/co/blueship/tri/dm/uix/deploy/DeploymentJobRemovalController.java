package jp.co.blueship.tri.dm.uix.deploy;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.dm.domainx.deploy.dto.FlowDeploymentJobRemovalServiceBean;
import jp.co.blueship.tri.dm.domainx.deploy.dto.FlowDeploymentJobRemovalServiceBean.SubmitOption;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriView;

/**
*
* @version V4.01.00
* @author Cuong Nguyen
*
*/

@Controller
@RequestMapping("/job/deploy/remove")
public class DeploymentJobRemovalController extends TriControllerSupport<FlowDeploymentJobRemovalServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.DmFlowDeploymentJobRemovalService;
	}

	@Override
	protected FlowDeploymentJobRemovalServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowDeploymentJobRemovalServiceBean bean = new FlowDeploymentJobRemovalServiceBean();
		return bean;
	}


	@RequestMapping
	public String removal(FlowDeploymentJobRemovalServiceBean bean , TriModel model){
		String view = "redirect:/job/deploy/list/refresh";

		try {
			this.mapping(bean, model);

			bean.getParam().setRequestType( RequestType.init );
			this.execute(getServiceId(), bean , model);

			bean.getParam().setRequestType( RequestType.submitChanges );

			if( bean.getDetailsView().isJobCancelled() ){
				bean.getParam().setSubmitOption( SubmitOption.removeJob );
				this.execute(getServiceId(), bean , model);

			}else{
				bean.getParam().setSubmitOption( SubmitOption.cancelTimer );
				this.execute(getServiceId(), bean , model);

				bean.getParam().setSubmitOption( SubmitOption.cancelJob );
				this.execute(getServiceId(), bean , model);
			}

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getRedirectAttributes().addFlashAttribute( "result", bean );

		return view;
	}


	@RequestMapping(value = "/validate")
	public String validation(FlowDeploymentJobRemovalServiceBean bean , TriModel model){
		String view = "common/Comment::deploymentJobRemovalComment";

		try {
			this.mapping(bean, model);

			bean.getParam().setRequestType( RequestType.init );
			this.execute(getServiceId(), bean , model);

			bean.getParam().setRequestType( RequestType.validate );

			if( bean.getDetailsView().isJobCancelled() ){
				bean.getParam().setSubmitOption( SubmitOption.removeJob );
				this.execute(getServiceId(), bean , model);

			}else{
				bean.getParam().setSubmitOption( SubmitOption.cancelTimer );
				this.execute(getServiceId(), bean , model);

				bean.getParam().setSubmitOption( SubmitOption.cancelJob );
				this.execute(getServiceId(), bean , model);
			}

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
			view = TriView.Messages.value() + "::message";
		}

		model.getModel().addAttribute("result", bean);
		return view;
	}


	private void mapping(FlowDeploymentJobRemovalServiceBean bean , TriModel model){
		IRequestInfo requestInfo = model.getRequestInfo();

		bean.getParam().setSelectedMgtVer(requestInfo.getParameter("mgtVer"));
		bean.getParam().getInputInfo().setComment(requestInfo.getParameter("comment"));

		boolean manual = true;
		String requestJob =	requestInfo.getParameter("requestJob");
		if(requestJob != null && requestJob.equalsIgnoreCase("automatic")){
			manual = false;
		}
		bean.getParam().getInputInfo().setManual( manual );
	}
}
