package jp.co.blueship.tri.dm.uix.deploy;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.dm.domainx.deploy.dto.FlowDeploymentJobDetailsServiceBean;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;

/**
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 * @version V4.01.00
 * @author Cuong Nguyen
 */
@Controller
@RequestMapping("/job/deploy")
public class DeploymentJobDetailsController extends TriControllerSupport<FlowDeploymentJobDetailsServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.DmFlowDeploymentJobDetailsService;
	}

	@Override
	protected FlowDeploymentJobDetailsServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowDeploymentJobDetailsServiceBean bean = new FlowDeploymentJobDetailsServiceBean();
		return bean;
	}


	@RequestMapping("/details/redirect")
	public String redirect(FlowDeploymentJobDetailsServiceBean bean , TriModel model){
		//bean.getParam().setRequestType(RequestType.init);
		return this.details(bean, model.setRedirect(true));
	}


	@RequestMapping("/details")
	public String details(FlowDeploymentJobDetailsServiceBean bean , TriModel model){
		String view = TriTemplateView.MainTemplate.value();

		try {
			this.mapping(bean, model);
			this.execute(this.getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("selectedMenu", "releaseMenu")
			.addAttribute("selectedSubMenu"  ,"deployjobSubmenu")
			.addAttribute("view", TriView.DeploymentJobDetails.value())
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}

	private void mapping(FlowDeploymentJobDetailsServiceBean bean , TriModel model){
		IRequestInfo requestInfo = model.getRequestInfo();
		bean.getParam().setSelectedMgtVer(requestInfo.getParameter("mgtVer"));
		
		boolean manual = true;
			String requestJob =	requestInfo.getParameter("requestJob");
		if(requestJob != null && requestJob.equalsIgnoreCase("automatic")){
			manual = false;
		}
		bean.getParam().getInputInfo().setManual( manual );
	}
}
