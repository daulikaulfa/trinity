package jp.co.blueship.tri.dm.uix.deploy;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.dcm.DcmExportToFileUtils;
import jp.co.blueship.tri.dm.domainx.deploy.dto.FlowDeploymentJobListServiceBean.RequestOption;
import jp.co.blueship.tri.dm.domainx.deploy.dto.FlowDeploymentJobListServiceBean.SearchCondition;
import jp.co.blueship.tri.dm.domainx.deploy.dto.FlowDeploymentJobListServiceExportToFileBean;
import jp.co.blueship.tri.dm.domainx.deploy.dto.FlowDeploymentJobListServiceExportToFileBean.RequestParam;
import jp.co.blueship.tri.fw.cmn.io.StreamUtils;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.io.constants.Charset;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.constants.ExportToFileSubmitOption;
import jp.co.blueship.tri.fw.um.domainx.cmn.beans.dto.ExportToFileBean;

/**
 * Provide the following backend services.
 * <br> - ExportToFile
 *
 * @version V4.00.00
 * @author Syaza Hanapi
 * 
 * @version V4.01.00
 * @author chung
 */
@Controller
@RequestMapping("/job/deploy/export")
public class DeploymentJobListServiceExportToFileController
		extends TriControllerSupport<FlowDeploymentJobListServiceExportToFileBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.DmFlowDeploymentJobListServiceExportToFile;
	}

	@Override
	protected FlowDeploymentJobListServiceExportToFileBean getServiceBean(ISessionInfo sesInfo) {
		FlowDeploymentJobListServiceExportToFileBean bean = new FlowDeploymentJobListServiceExportToFileBean();
		return bean;
	}

	@RequestMapping
	public String export(FlowDeploymentJobListServiceExportToFileBean bean, TriModel model) {

		String view = TriView.DeploymentJobList.value()+"::tableResult";

		try {
			this.insertMapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		} finally {
			this.removeAttribute(model, bean.getHeader().getWindowsId());
		}

		model.getModel().addAttribute("result", bean);
		return view;

	}

	@RequestMapping("/validate")
	public String validate(FlowDeploymentJobListServiceExportToFileBean bean, TriModel model) {
		String view = TriView.Messages.value() + "::message";
		try {
			this.execute(getServiceId(), bean, model);
		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}
		model.getModel().addAttribute("result", bean);
		return view;
	}

	@RequestMapping("/file")
	public void exportFile(FlowDeploymentJobListServiceExportToFileBean bean, TriModel model,
			HttpServletResponse response) {
		try {
			this.insertMapping(bean, model);
			this.execute(getServiceId(), bean, model);

			String newFileName =
					DcmExportToFileUtils.getDownloadFileName(
							getServiceId(),
							bean.getParam().getSubmitOption().extention(),
							bean.getLanguage(),
							bean.getTimeZone());

			StreamUtils.download(
					response,
					bean.getFileResponse().getFile(),
					newFileName );

			TriFileUtils.delete(bean.getFileResponse().getFile());

		} catch (Exception e) {
			ExceptionUtils.printStackTrace(e);
		} finally {
			this.removeAttribute(model, bean.getHeader().getWindowsId());
		}

		return;
	}

	private void insertMapping(FlowDeploymentJobListServiceExportToFileBean bean, TriModel model)
			throws UnsupportedEncodingException {

		RequestParam param = bean.getParam();
		IRequestInfo requestInfo = model.getRequestInfo();
		ISessionInfo sesInfo = model.getSessionInfo();

		bean.getParam().setLotId(this.getSessionSelectedLot(sesInfo, bean));

		if(RequestType.onChange.equals(bean.getParam().getRequestType())){
			boolean isManual = true;
			String requestJob =	requestInfo.getParameter("requestJob");
			if(requestJob != null && requestJob.equals("automatic")){
				isManual = false;
			}
			
			SearchCondition condition = bean.getParam().getSearchCondition()
					.setEnvId			( requestInfo.getParameter("envId"))
					.setStsId			( requestInfo.getParameter("stsId"))
					.setCtgId			( requestInfo.getParameter("ctgId"))
					.setMstoneId		( requestInfo.getParameter("mstoneId"))
					.setKeyword			( requestInfo.getParameter("keyword"))
					;
			bean.getParam().setSearchCondition(condition);
			bean.getParam().getInputInfo().setManual(isManual);
			String requestOption = requestInfo.getParameter("requestOption");
			if (RequestOption.search.equals(requestOption)) {
				bean.getParam().setRequestOption( RequestOption.search );
			} else if (RequestOption.getDeploymentInfo.equals(requestOption)) {
				bean.getParam().setRequestOption( RequestOption.getDeploymentInfo );
			} else {
				bean.getParam().setRequestOption( RequestOption.none );
			}
		} else if ((RequestType.submitChanges).equals(param.getRequestType())) {
				String htmlTable = URLDecoder.decode(model.getRequestInfo().getParameter("table"), Charset.UTF_8.value());
				ExportToFileBean fileBean = DcmExportToFileUtils.createExportToFileBean(htmlTable);
				param.setExportBean(fileBean);
				ExportToFileSubmitOption option = ExportToFileSubmitOption.valueOf(requestInfo.getParameter("submitOption"));
				param.setSubmitOption(option);
		}

	}
}