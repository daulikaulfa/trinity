package jp.co.blueship.tri.dm.uix.deploy;

import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.dm.domainx.deploy.dto.FlowDeploymentJobListServiceOutputShellBean;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;

/**
*
* @version V4.01.00
* @author Cuong Nguyen
*
*/

@RequestMapping("/job/deploy/list")
public class DeploymentJobListServiceOutputShellController 
	extends TriControllerSupport<FlowDeploymentJobListServiceOutputShellBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.DmFlowDeploymentJobListServiceOutputShell;
	}

	@Override
	protected FlowDeploymentJobListServiceOutputShellBean getServiceBean(ISessionInfo sesInfo) {
		FlowDeploymentJobListServiceOutputShellBean bean = new FlowDeploymentJobListServiceOutputShellBean();
		return bean;
	}
	
	@RequestMapping(value = {"/output/shell"})
	public String index(FlowDeploymentJobListServiceOutputShellBean bean , TriModel model){
		String view = TriTemplateView.MainTemplate.value();

		try {
			this.mapping(bean, model);
			this.execute(this.getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getRedirectAttributes().addFlashAttribute( "result", bean );
		view = "redirect:/job/deploy/list/refresh";

		setPrev(model);
		return view;
	}
	
	private void mapping(FlowDeploymentJobListServiceOutputShellBean bean , TriModel model){
		IRequestInfo requestInfo = model.getRequestInfo();
		bean.getParam().setSelectedEnvId( requestInfo.getParameter("envId") );
	}

}
