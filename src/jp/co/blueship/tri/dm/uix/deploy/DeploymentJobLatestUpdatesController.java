package jp.co.blueship.tri.dm.uix.deploy;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.dm.domainx.deploy.dto.FlowDeploymentJobLatestUpdatesServiceBean;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;

/**
 *
 * @version V4.00.00
 * @author Akahoshi
 * 
 * @version V4.01.00
 * @author Cuong Nguyen
 */
@Controller
@RequestMapping("/job/deploy")
public class DeploymentJobLatestUpdatesController extends TriControllerSupport<FlowDeploymentJobLatestUpdatesServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.DmFlowDeploymentJobLatestUpdatesService;
	}

	@Override
	protected FlowDeploymentJobLatestUpdatesServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowDeploymentJobLatestUpdatesServiceBean bean = new FlowDeploymentJobLatestUpdatesServiceBean();
		return bean;
	}


	@RequestMapping(value = "/latest")
	public String latestUpdates(FlowDeploymentJobLatestUpdatesServiceBean bean , TriModel model){
		String view = TriTemplateView.MainTemplate.value();
		try {
			this.mapping(bean, model);
			this.execute(this.getServiceId(), bean, model);

		} catch (Exception e) {
		}

		model.getModel()
			.addAttribute("selectedMenu", "releaseMenu")
			.addAttribute("selectedSubMenu"  ,"deployjobSubmenu")
			.addAttribute("view", TriView.DeploymentJobLatestUpdates.value())
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}


	private void mapping(FlowDeploymentJobLatestUpdatesServiceBean bean , TriModel model){
		ISessionInfo sesInfo = model.getSessionInfo();
		bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
		boolean manual = true;
		String requestJob =	model.getRequestInfo().getParameter("requestJob");
		if(requestJob != null && requestJob.equalsIgnoreCase("automatic")){
			manual = false;
		}
		bean.getParam().getInputInfo().setManual( manual );
	}
}
