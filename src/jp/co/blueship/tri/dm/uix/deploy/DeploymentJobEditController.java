package jp.co.blueship.tri.dm.uix.deploy;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.dm.constants.TimerSettings;
import jp.co.blueship.tri.dm.domainx.deploy.dto.FlowDeploymentJobEditServiceBean;
import jp.co.blueship.tri.dm.domainx.deploy.dto.FlowDeploymentJobEditServiceBean.DeploymentJobEditInputBean;
import jp.co.blueship.tri.dm.domainx.deploy.dto.FlowDeploymentJobEditServiceBean.RequestOption;
import jp.co.blueship.tri.dm.domainx.deploy.dto.FlowDeploymentJobEditServiceBean.SubmitOption;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;

/**
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 */
@Controller
@RequestMapping("/job/deploy")
public class DeploymentJobEditController extends TriControllerSupport<FlowDeploymentJobEditServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.DmFlowDeploymentJobEditService;
	}

	@Override
	protected FlowDeploymentJobEditServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowDeploymentJobEditServiceBean bean = new FlowDeploymentJobEditServiceBean();
		return bean;
	}
	
	@RequestMapping(value = "/edit/redirect")
	public String redirect( FlowDeploymentJobEditServiceBean bean, TriModel model ){
		bean.getParam().setRequestType(RequestType.init);
		return edit( bean , model.setRedirect(true) );
	}



	@RequestMapping("/edit")
	public String edit(FlowDeploymentJobEditServiceBean bean , TriModel model){
		String view = TriTemplateView.MainTemplate.value();
		try {
			this.mapping(bean, model);
			this.execute(this.getServiceId(), bean, model);
			if ( bean.getResult().isCompleted() ) {
				model.getRedirectAttributes().addFlashAttribute( "result", bean );
				view = "redirect:/job/deploy/list/refresh";
			}

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("selectedMenu", "releaseMenu")
			.addAttribute("selectedSubMenu"  ,"deployjobSubmenu")
			.addAttribute("view", TriView.DeploymentJobEdit.value())
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}

	@RequestMapping(value = "/edit/refreshCategory")
	public String refreshCategory( @ModelAttribute("ctgId") String ctgId, FlowDeploymentJobEditServiceBean bean, TriModel model ){
		bean.getParam().setRequestType(RequestType.onChange);
		bean.getParam().setRequestOption(RequestOption.refreshCategory);
		bean.getParam().getInputInfo().setCtgId( ctgId );
		return edit( bean , model.setRedirect(true) );
	}

	@RequestMapping(value = "/edit/refreshMilestone")
	public String refreshMilestone( @ModelAttribute("mstoneId") String mstoneId, FlowDeploymentJobEditServiceBean bean, TriModel model ){
		bean.getParam().setRequestType(RequestType.onChange);
		bean.getParam().setRequestOption(RequestOption.refreshMilestone);
		bean.getParam().getInputInfo().setMstoneId( mstoneId );

		return edit( bean , model.setRedirect(true) );
	}

	private void mapping(FlowDeploymentJobEditServiceBean bean , TriModel model){

		IRequestInfo requestInfo = model.getRequestInfo();

		if(RequestType.init.equals(bean.getParam().getRequestType())){
			if ( requestInfo.getParameter("mgtVer") != null ){
				bean.getParam().setSelectedMgtVer(requestInfo.getParameter("mgtVer"));
			}
			if ( requestInfo.getParameter("requestJob") != null ){
				String requestJob = requestInfo.getParameter("requestJob");
				boolean manual = true;
			    if(requestJob != null && requestJob.equalsIgnoreCase("automatic")){
			            manual = false;
			        }
			    bean.getParam().getInputInfo().setManual( manual );
			}

		}

		if(RequestType.submitChanges.equals(bean.getParam().getRequestType())){
			DeploymentJobEditInputBean inputInfo = bean.getParam().getInputInfo()
					.setSummary			( requestInfo.getParameter("summary"))
					.setContents		( requestInfo.getParameter("contents"))
					.setTimerSettings	( TimerSettings.value(requestInfo.getParameter("timerSettings")))
					.setTimerDate		( requestInfo.getParameter("timerDate"))
					.setTimerHour		( requestInfo.getParameter("timerHour"))
					.setTimerMinute		( requestInfo.getParameter("timerMinute"))
					.setManual			( "true".equals(requestInfo.getParameter("manualJob")))
					.setMstoneId		( requestInfo.getParameter( "mistoneId" ))
					.setCtgId			( requestInfo.getParameter( "catgId" ))
					.setTimerSummary	( requestInfo.getParameter( "timerSummary" ))
					.setTimerContents	( requestInfo.getParameter( "timerDetail" ))
					;
			bean.getParam().setInputInfo(inputInfo);
		}


	}
}
