package jp.co.blueship.tri.dm.uix.deploy;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.dm.domainx.deploy.dto.FlowDeploymentJobListServiceBean;
import jp.co.blueship.tri.dm.domainx.deploy.dto.FlowDeploymentJobListServiceBean.RequestOption;
import jp.co.blueship.tri.dm.domainx.deploy.dto.FlowDeploymentJobListServiceBean.SearchCondition;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;

/**
 *
 * @version V4.01.00
 * @author Chung
 *
 */
@Controller
@RequestMapping("/job/deploy/list/print")
public class PrintDeploymentJobListController extends TriControllerSupport<FlowDeploymentJobListServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.DmFlowPrintDeploymentJobListService;
	}


	@Override
	protected FlowDeploymentJobListServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowDeploymentJobListServiceBean bean = new FlowDeploymentJobListServiceBean();
		return bean;
	}

	@RequestMapping
	public String print(FlowDeploymentJobListServiceBean bean , TriModel model){
		String view = TriTemplateView.PrintTemplate.value();

		try {
			this.mapping(bean, model);
			this.execute(this.getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.PrintDeploymentJobList.value())
			.addAttribute("result", bean)
		;

		return view;
	}


	private void mapping(FlowDeploymentJobListServiceBean bean , TriModel model){
		ISessionInfo sesInfo = model.getSessionInfo();
		IRequestInfo requestInfo = model.getRequestInfo();

		bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
		boolean isManual = true;
			String requestJob =	requestInfo.getParameter("requestJob");
		if(requestJob != null && requestJob.equals("automatic")){
			isManual = false;
		}
		
		SearchCondition condition = bean.getParam().getSearchCondition()
				.setEnvId			( requestInfo.getParameter("envId"))
				.setStsId			( requestInfo.getParameter("stsId"))
				.setCtgId			( requestInfo.getParameter("ctgId"))
				.setMstoneId		( requestInfo.getParameter("mstoneId"))
				.setKeyword			( requestInfo.getParameter("keyword"))
				;
		bean.getParam().setLinesPerPage( 0 );
		bean.getParam().setSearchCondition(condition);
		bean.getParam().getInputInfo().setManual(isManual);
		
		String requestOption = requestInfo.getParameter("requestOption");
		if (RequestOption.search.equals(requestOption)) {
			bean.getParam().setRequestOption( RequestOption.search );
		} else if (RequestOption.getDeploymentInfo.equals(requestOption)) {
			bean.getParam().setRequestOption( RequestOption.getDeploymentInfo );
		} else {
			bean.getParam().setRequestOption( RequestOption.none );
		}
	}
}
