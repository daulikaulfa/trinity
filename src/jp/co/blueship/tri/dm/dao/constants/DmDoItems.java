package jp.co.blueship.tri.dm.dao.constants;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;

public enum DmDoItems implements ITableItem {
	mgtVer("mgt_ver"),
	rpId("rp_id"),
	lotId("lot_id"),
	summary("summary"),
	content("content"),
	timerDate("timer_date"),
	timerTime("timer_time"),
	timerSummary("timer_summary"),
	timerContent("timer_content"),
	stsId("sts_id"),
	procStsId("proc_sts_id"),
	delStsId("del_sts_id"),
	delUserId("del_user_id"),
	delUserNm("del_user_nm"),
	delTimestamp("del_timestamp"),
	delCmt("del_cmt"),
	regUserId("reg_user_id"),
	regUserNm("reg_user_nm"),
	regTimestamp("reg_timestamp"),
	updUserId("upd_user_id"),
	updUserNm("upd_user_nm"),
	updTimestamp("upd_timestamp");

	private String element = null;

	private DmDoItems( String element) {
		this.element = element;
	}

	public String getItemName() {
		return this.element;
	}

}
