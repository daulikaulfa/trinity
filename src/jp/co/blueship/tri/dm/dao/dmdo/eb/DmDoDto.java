package jp.co.blueship.tri.dm.dao.dmdo.eb;

import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;

/**
 * デプロイメント連携のDTOです。
 * @author Takashi Ono
 *
 */
public class DmDoDto implements IDmDoDto {

	private IRpEntity rpEntity;
	private IDmDoEntity dmDoEntity;
	@Override
	public IRpEntity getRpEntity() {
		return this.rpEntity;
	}

	@Override
	public void setRpEntity(IRpEntity entity) {
		this.rpEntity = entity;
	}

	@Override
	public IDmDoEntity getDmDoEntity() {
		return this.dmDoEntity;
	}

	@Override
	public void setDmDoEntity(IDmDoEntity entity) {
		this.dmDoEntity = entity;
	}

}
