package jp.co.blueship.tri.dm.dao.dmdo.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.dm.dao.constants.DmDoItems;
import jp.co.blueship.tri.dm.dao.constants.DmTables;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.dao.orm.psql.SqlFormatUtils;
import jp.co.blueship.tri.fw.um.dao.ctg.constants.CtgItems;
import jp.co.blueship.tri.fw.um.dao.mstone.constants.MstoneItems;
/**
 * デプロイメント連携エンティティのSQLコンディションクラスです。
 *
 * @author Takashi Ono
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class DmDoCondition extends ConditionSupport {

	private static final ITableAttribute attr = DmTables.DM_DO;

	private String mgtVer;
	private String rpId;
	private String[] rpIds;
	private String[] lotIds;
	private String regUserId;
	private String summary;
	private String content;
	private String timerSettingDate;
	private String timerSettingTime;
	private String timerSettingSummary;
	private String timerSettingContent;
	private String stsId;
	private String[] stsIds;
	private String[] procStsIds;
	private String delUserId;
	private String delUserNm;
	private Timestamp delTimestamp;
	private String delCmt;

	/**
	 * category ID
	 */
	private String ctgId = null;
	/**
	 * category ID's
	 */
	private String[] ctgIds = null;
	/**
	 * milestone ID
	 */
	private String mstoneId = null;
	/**
	 * milestone ID's
	 */
	private String[] mstoneIds = null;

	/**
	 * keyword
	 */
	private String[] keywords = null;

	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		   super.append(DmDoItems.delStsId, StatusFlg.off.parseBoolean() );
	}

	public DmDoCondition(){
		super(attr);
		super.setJoinExecData( true );
		super.setKeyByJoinExecData( DmDoItems.mgtVer );
		super.setJoinCtg(true);
		super.setJoinMstone(true);
	}

	@Override
	public ConditionSupport setJoinAccsHist( boolean isJoinAccsHist ) {
		super.setJoinAccsHist(isJoinAccsHist);
		return this;
	}

	public String getMgtVer() {
		return mgtVer;
	}

	public void setMgtVer( String mgtVer ) {
		this.mgtVer = mgtVer;
		super.append(DmDoItems.mgtVer, mgtVer);
	}

	public String getRpId() {
		return rpId;
	}

	public void setRpId( String rpId ) {
		this.rpId = rpId;
		super.append(DmDoItems.rpId, rpId);
	}

	public String[] getRpIds() {
		return rpIds;
	}

	public void setRpIds( String[] rpIds ) {
		this.rpIds = rpIds;
		super.append( DmDoItems.rpId.getItemName() + "[]",
				SqlFormatUtils.getCondition(rpIds, DmDoItems.rpId.getItemName(), false, true, false) );
	}

	public String[] getLotIds() {
		return lotIds;
	}

	public void setLotIds(String[] lotIds) {
		this.lotIds = lotIds;
		super.appendByJoinExecData( DmDoItems.lotId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(lotIds, DmDoItems.lotId.getItemName(), false, true, false) );
	}

	public String getRegUserId() {
		return regUserId;
	}

	public void setRegUserId( String regUserId ) {
		this.regUserId = regUserId;
		super.append(DmDoItems.regUserId, regUserId);
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary( String summary ) {
		this.summary = summary;
		super.append(DmDoItems.summary, summary);
	}

	public String getContent() {
		return content;
	}

	public void setContent( String content ) {
		this.content = content;
		super.append(DmDoItems.content, content);
	}

	public String getTimerSettingDate() {
		return timerSettingDate;
	}

	public void setTimerSettingDate( String date ) {
		this.timerSettingDate = date;
		super.append(DmDoItems.timerDate, date);
	}

	public String getTimerSettingTime() {
		return timerSettingTime;
	}

	public void setTimerSettingTime( String time ) {
		this.timerSettingTime = time;
		super.append(DmDoItems.timerTime, time);
	}

	public String getTimerSettingSummary() {
		return timerSettingSummary;
	}

	public void setTimerSettingSummary( String summary ) {
		this.timerSettingSummary = summary;
		super.append(DmDoItems.timerSummary, summary);
	}

	public String getTimerSettingContent() {
		return timerSettingContent;
	}

	public void setTimerSettingContent( String content ) {
		this.timerSettingContent = content;
		super.append(DmDoItems.timerContent, content);
	}

	public String getStsId() {
		return stsId;
	}

	public void setStsId( String id ) {
		this.stsId = id;
		super.append(DmDoItems.stsId, id);
	}
	public void setStsIdsNotEquals( String[] ids ) {
		this.stsIds = ids;
		super.appendByJoinExecData( DmDoItems.stsId.getItemName() + "[]",
				SqlFormatUtils.getCondition(stsIds, DmDoItems.procStsId.getItemName(), false, false, true) );
	}
	public void setStsIds( String[] ids ) {
		this.stsIds = ids;
		super.appendByJoinExecData( DmDoItems.stsId.getItemName() + "[]",
				SqlFormatUtils.getCondition(stsIds, DmDoItems.stsId.getItemName(), false, true, false) );
	}

	public String[] getProcStsIds() {
		return this.procStsIds;
	}

	public void setProcStsIds( String[] ids ) {
		this.procStsIds = ids;
		super.appendByJoinExecData( DmDoItems.procStsId.getItemName() + "[]",
				SqlFormatUtils.getCondition(procStsIds, DmDoItems.procStsId.getItemName(), false, true, false) );
	}

	public void setRegTimeFromTo(String from, String to) {
		super.append(DmDoItems.regTimestamp.getItemName() ,
				SqlFormatUtils.getFromTo(from, to, DmDoItems.regTimestamp.getItemName(), true));
	}

	public String getDelUserId() {
		return delUserId;
	}

	public void setDelUserId( String id ) {
		this.delUserId = id;
		super.append(DmDoItems.delUserId, id);
	}

	public String getDelUserNm() {
		return delUserNm;
	}

	public void setDelUserNm( String name ) {
		this.delUserNm = name;
		super.append(DmDoItems.delUserNm, name);
	}

	public Timestamp getDelTimestamp() {
		return delTimestamp;
	}

	public void setDelTimestamp( Timestamp time ) {
		this.delTimestamp = time;
		super.append(DmDoItems.delTimestamp, time );
	}

	public String getDelCmt() {
		return delCmt;
	}

	public void setDelCmt( String cmt ) {
		this.delCmt = cmt;
		super.append(DmDoItems.delCmt, cmt);
	}

	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	    super.append( DmDoItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}

	/**
	 * keyword
	 * @param keyword
	 */
	public void setContainsByKeyword(String... keywords) {
		if ( TriStringUtils.isEmpty(keywords) ) {
			return;
		}

		this.keywords = keywords;
		super.append( DmDoItems.rpId.getItemName() + "Other",
				SqlFormatUtils.joinCondition( true,
						new String[]{
						SqlFormatUtils.getCondition(this.keywords, DmDoItems.rpId.getItemName(), true, false, false),
						SqlFormatUtils.getCondition(this.keywords, DmDoItems.mgtVer.getItemName(), true, false, false),
						SqlFormatUtils.getCondition(this.keywords, DmDoItems.regUserNm.getItemName(), true, false, false),
						SqlFormatUtils.getCondition(this.keywords, DmDoItems.summary.getItemName(), true, false, false),
						SqlFormatUtils.getCondition(this.keywords, DmDoItems.content.getItemName(), true, false, false),

				}));
	}

	/**
	 * category IDを取得します。
	 * @return category ID
	 */
	public String getCtgId() {
	    return ctgId;
	}

	/**
	 * category IDを設定します。
	 * @param ctgId category ID
	 */
	public void setCtgId(String ctgId) {
	    this.ctgId = ctgId;
	    super.append(CtgItems.ctgId, ctgId );
	}

	/**
	 * @return
	 */
	public String[] getCtgIds() {
		return ctgIds;
	}

	/**
	 * @param ctgIds
	 */
	public void setCtgIds(String[] ctgIds) {
		this.ctgIds = ctgIds;
		super.appendByJoinExecData( CtgItems.ctgId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(ctgIds, CtgItems.ctgId.getItemName(), false, true, false) );
	}

	/**
	 * milestone IDを取得します。
	 * @return milestone ID
	 */
	public String getMstoneId() {
	    return mstoneId;
	}

	/**
	 * milestone IDを設定します。
	 * @param mstoneId milestone ID
	 */
	public void setMstoneId(String mstoneId) {
	    this.mstoneId = mstoneId;
	    super.append(MstoneItems.mstoneId, mstoneId );
	}

	/**
	 * @return
	 */
	public String[] getMstoneIds() {
		return mstoneIds;
	}

	/**
	 * @param mstoneIds
	 */
	public void setMstoneIds(String[] mstoneIds) {
		this.mstoneIds = mstoneIds;
		super.appendByJoinExecData( MstoneItems.mstoneId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(mstoneIds, MstoneItems.mstoneId.getItemName(), false, true, false) );
	}

	/**
	 * @param mstoneStartDate
	 * @param mstoneEndDate
	 */
	public void setMstoneFromTo(String mstoneStartDate, String mstoneEndDate) {
		super.append(MstoneItems.mstoneStDate.getItemName() + "[]",
	    		SqlFormatUtils.getFromTo( mstoneStartDate, mstoneEndDate, MstoneItems.mstoneStDate, MstoneItems.mstoneEndDate ) );
	}

}
