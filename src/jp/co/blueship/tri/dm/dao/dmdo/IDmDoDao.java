package jp.co.blueship.tri.dm.dao.dmdo;

import jp.co.blueship.tri.dm.dao.dmdo.eb.IDmDoEntity;
import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;

/**
 * デプロイメント連携Daoのインターフェースです。
 * @version V3L10.01
 * @author Takashi Ono
 *
 */
public interface IDmDoDao extends IJdbcDao<IDmDoEntity> {

}
