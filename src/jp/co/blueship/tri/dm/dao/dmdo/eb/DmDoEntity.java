package jp.co.blueship.tri.dm.dao.dmdo.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.fw.dao.orm.EntityFooter;

/**
 * デプロイメント連携Entityです。
 *
 * @version V3L10.01
 * @author Takashi.Ono
 *
 * @version V4.01.00
 * @author Akahoshi
 *
 */
public class DmDoEntity extends EntityFooter implements IDmDoEntity {

	/**
	 *
	 */
	private static final long serialVersionUID = -6202853442995233997L;

	public String deploymentId;
	public String mgtVer;
	public String rpId;
	public String lotId;
	public String summary;
	public String content;
	public String timerSettingDate;
	public String timerSettingTime;
	public String timerSettingSummary;
	public String timerSettingContent;
	public String stsId;
	public String procStsId;
	public String delUserId;
	public String delUserNm;
	public Timestamp delTimestamp;
	public String delCmt;

	public String ctgId;
	public String ctgNm;
	public String mstoneId;
	public String mstoneNm;


	@Override
	public String getDeploymentId() {
		return deploymentId;
	}
	public void setDeploymentId(String deploymentId) {
		this.deploymentId = deploymentId;
	}

	public String getMgtVer() {
		return mgtVer;
	}

	public void setMgtVer(String version) {
		this.mgtVer = version;
	}

	public String getRpId() {
		return rpId;
	}

	public void setRpId(String id) {
		this.rpId = id;
	}

	public String getLotId() {
	    return lotId;
	}
	public void setLotId(String lotId) {
	    this.lotId = lotId;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getTimerSettingDate() {
		return timerSettingDate;
	}

	public void setTimerSettingDate(String timerSettingDate) {
		this.timerSettingDate = timerSettingDate;
	}

	public String getTimerSettingTime() {
		return timerSettingTime;
	}

	public void setTimerSettingTime(String timerSettingTime) {
		this.timerSettingTime = timerSettingTime;
	}

	public String getTimerSettingSummary() {
		return timerSettingSummary;
	}

	public void setTimerSettingSummary(String timerSettingSummary) {
		this.timerSettingSummary = timerSettingSummary;
	}

	public String getTimerSettingContent() {
		return timerSettingContent;
	}

	public void setTimerSettingContent(String timerSettingContent) {
		this.timerSettingContent = timerSettingContent;
	}

	public String getStsId() {
		return stsId;
	}

	public void setStsId(String stsId) {
		this.stsId = stsId;
	}

	public String getProcStsId() {
		return procStsId;
	}

	public void setProcStsId(String procStsId) {
		this.procStsId = procStsId;
	}

	public String getDelUserId() {
		return delUserId;
	}

	public void setDelUserId(String delUserId) {
		this.delUserId = delUserId;
	}

	public String getDelUserNm() {
		return delUserNm;
	}

	public void setDelUserNm(String delUserNm) {
		this.delUserNm = delUserNm;
	}

	public Timestamp getDelTimestamp() {
		return delTimestamp;
	}

	public void setDelTimestamp(Timestamp delTimestamp) {
		this.delTimestamp = delTimestamp;
	}

	public String getDelCmt() {
		return delCmt;
	}

	public void setDelCmt(String delCmt) {
		this.delCmt = delCmt;
	}

	public String getCtgId() {
		return ctgId;
	}
	public void setCtgId(String ctgId) {
		this.ctgId = ctgId;
	}

	public String getCtgNm() {
		return ctgNm;
	}
	public void setCtgNm(String ctgNm) {
		this.ctgNm = ctgNm;
	}

	public String getMstoneId() {
		return mstoneId;
	}
	public void setMstoneId(String mstoneId) {
		this.mstoneId = mstoneId;
	}

	public String getMstoneNm() {
		return mstoneNm;
	}
	public void setMstoneNm(String mstoneNm) {
		this.mstoneNm = mstoneNm;
	}

}
