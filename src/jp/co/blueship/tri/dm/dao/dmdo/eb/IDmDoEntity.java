package jp.co.blueship.tri.dm.dao.dmdo.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * デプロイメント連携Entityのインターフェースです。
 *
 * @version V3L10.01
 * @author Takashi.Ono
 *
 * @version V4.01.00
 * @author Akahoshi
 *
 */
public interface IDmDoEntity extends IEntityFooter {


	/**
	 * Deployment Id <br>
	 * DB don't have this column. <br>
	 * Create it in Dao for display on screen. <br>
	 * <br>
	 * リリース連携番号<br>
	 * DB上には存在せず画面の表示の為だけにDao内で生成されます。<br>
	 *
	 * @return
	 */
	public String getDeploymentId();

	/**
	 * 管理バージョン
	 * 主keyです。
	 * @return
	 */
	public String getMgtVer();
	public void setMgtVer(String version);
	/**
	 * ビルドパッケージID
	 * 外部keyです。
	 * @return
	 */
	public String getRpId();
	public void setRpId(String id);
	/**
	 * lot-ID
	 * @return lot-ID
	 */
	public String getLotId();
	/**
	 * 概要
	 * @return
	 */
	public String getSummary();
	public void setSummary(String summary);
	/**
	 * 内容
	 * @return
	 */
	public String getContent();
	public void setContent(String content);
	/**
	 * タイマー設定日
	 * @return
	 */
	public String getTimerSettingDate();
	public void setTimerSettingDate(String timerSettingDate);
	/**
	 * タイマー設定時間
	 * @return
	 */
	public String getTimerSettingTime();
	public void setTimerSettingTime(String timerSettingTime);
	/**
	 * タイマー設定概要
	 * @return
	 */
	public String getTimerSettingSummary();
	public void setTimerSettingSummary(String timerSettingSummary);
	/**
	 * タイマー設定内容
	 * @return
	 */
	public String getTimerSettingContent();
	public void setTimerSettingContent(String timerSettingContent);
	/**
	 * ステータスID
	 * @return
	 */
	public String getStsId();
	public void setStsId(String stsId);
	/**
	 * procステータスID
	 */
	public String getProcStsId();
	public void setProcStsId(String procStsId );
	/**
	 * 取消ユーザID
	 * @return
	 */
	public String getDelUserId();
	public void setDelUserId(String delUserId);
	/**
	 * 取消ユーザ名
	 * @return
	 */
	public String getDelUserNm();
	public void setDelUserNm(String delUserNm);
	/**
	 * 取消日時
	 * @return
	 */
	public Timestamp getDelTimestamp();
	public void setDelTimestamp(Timestamp delTimestamp);
	/**
	 * 取消コメント
	 * @return
	 */
	public String getDelCmt();
	public void setDelCmt(String delCmt);
	/**
	 * カテゴリーID
	 * @return
	 */
	public String getCtgId();
	public void setCtgId(String ctgId);
	/**
	 * カテゴリー名
	 * @return
	 */
	public String getCtgNm();
	public void setCtgNm(String ctgNm);
	/**
	 * マイルストーンID
	 * @return
	 */
	public String getMstoneId();
	public void setMstoneId(String mstoneId);
	/**
	 * マイルストーン名
	 * @return
	 */
	public String getMstoneNm();
	public void setMstoneNm(String mstoneNm);

}
