package jp.co.blueship.tri.dm.dao.dmdo;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.dm.dao.constants.DmDoItems;
import jp.co.blueship.tri.dm.dao.constants.DmTables;
import jp.co.blueship.tri.dm.dao.dmdo.eb.DmDoEntity;
import jp.co.blueship.tri.dm.dao.dmdo.eb.IDmDoEntity;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.constants.DmDesignEntryKeyByDeploymentJob;
import jp.co.blueship.tri.fw.dao.orm.IDaoTemplate;
import jp.co.blueship.tri.fw.dao.orm.IDaoTemplateCallback;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.ISqlCondition;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.DaoTemplate;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.sm.dao.execdatasts.constants.ExecDataStsItems;
import jp.co.blueship.tri.fw.um.dao.ctg.constants.CtgItems;
import jp.co.blueship.tri.fw.um.dao.mstone.constants.MstoneItems;

/**
 * The implements of the deployment job DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 * @version V4.01.00
 * @author Akahoshi
 *
 */
public class DmDoDao extends JdbcBaseDao<IDmDoEntity> implements IDmDoDao {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected final ITableAttribute getTableAttribute() {
		return DmTables.DM_DO;
	}

	@Override
	public void setDaoTemplate( IDaoTemplate daoTemplate ) {
		super.setDaoTemplate( new CustomTemplate( new CustomCallback() ) );
	}

	@Override
	protected final ISqlBuilder append(ISqlBuilder builder, IDmDoEntity entity) {
		builder.append(DmDoItems.mgtVer, entity.getMgtVer(), true)
				.append(DmDoItems.rpId, entity.getRpId())
				.append(DmDoItems.summary, entity.getSummary())
				.append(DmDoItems.content, entity.getContent())
				.append(DmDoItems.timerDate, entity.getTimerSettingDate())
				.append(DmDoItems.timerTime, entity.getTimerSettingTime())
				.append(DmDoItems.timerSummary, entity.getTimerSettingSummary())
				.append(DmDoItems.timerContent, entity.getTimerSettingContent())
				.append(DmDoItems.stsId, entity.getStsId())
				.append(DmDoItems.delUserId, entity.getDelUserId())
				.append(DmDoItems.delUserNm, entity.getDelUserNm())
				.append(DmDoItems.delTimestamp, entity.getDelTimestamp())
				.append(DmDoItems.delCmt, entity.getDelCmt())
				.append(DmDoItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
				.append(DmDoItems.regTimestamp, entity.getRegTimestamp())
				.append(DmDoItems.regUserId, entity.getRegUserId())
				.append(DmDoItems.regUserNm, entity.getRegUserNm())
				.append(DmDoItems.updTimestamp, entity.getUpdTimestamp())
				.append(DmDoItems.updUserId, entity.getUpdUserId())
				.append(DmDoItems.updUserNm, entity.getUpdUserNm());

		return builder;
	}

	@Override
	protected final IDmDoEntity entityMapping(ResultSet rs, int row)
			throws SQLException {
		DmDoEntity entity = new DmDoEntity();

		String prefix = DesignSheetFactory.getDesignSheet().getValue( DmDesignEntryKeyByDeploymentJob.numberingPrefix );

		entity.setDeploymentId(prefix + rs.getString(DmDoItems.mgtVer.getItemName()));
		entity.setMgtVer(rs.getString(DmDoItems.mgtVer.getItemName()));
		entity.setRpId(rs.getString(DmDoItems.rpId.getItemName()));
		entity.setLotId( rs.getString(DmDoItems.lotId.getItemName()) );
		entity.setSummary(rs.getString(DmDoItems.summary.getItemName()));
		entity.setContent(rs.getString(DmDoItems.content.getItemName()));
		entity.setTimerSettingDate(rs.getString(DmDoItems.timerDate.getItemName()));
		entity.setTimerSettingTime(rs.getString(DmDoItems.timerTime.getItemName()));
		entity.setTimerSettingSummary(rs.getString(DmDoItems.timerSummary.getItemName()));
		entity.setTimerSettingContent(rs.getString(DmDoItems.timerContent.getItemName()));
		entity.setStsId(rs.getString(DmDoItems.stsId.getItemName()));
		entity.setProcStsId(rs.getString(ExecDataStsItems.procStsId.getItemName()));
		entity.setDelUserId(rs.getString(DmDoItems.delUserId.getItemName()));
		entity.setDelUserNm(rs.getString(DmDoItems.delUserNm.getItemName()));
		entity.setDelTimestamp(rs.getTimestamp(DmDoItems.delTimestamp
				.getItemName()));
		entity.setDelCmt(rs.getString(DmDoItems.delCmt.getItemName()));
		entity.setDelStsId(StatusFlg.value(rs.getBoolean(DmDoItems.delStsId.getItemName())));
		entity.setRegTimestamp( rs.getTimestamp(DmDoItems.regTimestamp.getItemName()) );
		entity.setRegUserId( rs.getString(DmDoItems.regUserId.getItemName()) );
		entity.setRegUserNm( rs.getString(DmDoItems.regUserNm.getItemName()) );
		entity.setUpdTimestamp( rs.getTimestamp(DmDoItems.updTimestamp.getItemName()) );
		entity.setUpdUserId( rs.getString(DmDoItems.updUserId.getItemName()) );
		entity.setUpdUserNm( rs.getString(DmDoItems.updUserNm.getItemName()) );

		entity.setCtgId( rs.getString(CtgItems.ctgId.getItemName()) );
		entity.setCtgNm( rs.getString(CtgItems.ctgNm.getItemName()) );
		entity.setMstoneId( rs.getString(MstoneItems.mstoneId.getItemName()) );
		entity.setMstoneNm( rs.getString(MstoneItems.mstoneNm.getItemName()) );

		return entity;
	}

	private class CustomTemplate extends DaoTemplate {
		protected CustomTemplate( IDaoTemplateCallback callback ) {
			super.setDaoTemplateCallback(callback);
		}
	}

	private class CustomCallback implements IDaoTemplateCallback {
		@Override
		public String appendOfSelectClause(ISqlCondition condition, boolean isCountMode) {
			StringBuffer buf = new StringBuffer()
				.append("  ,R.LOT_ID")
			;

			return buf.toString();
		}

		@Override
		public String appendOfFromClause(ISqlCondition condition, boolean isCountMode) {
			StringBuffer buf = new StringBuffer()
				.append(" LEFT JOIN (SELECT LOT_ID, RP_ID AS R_RP_ID FROM RM_RP) R ON RP_ID = R.R_RP_ID")
			;

			return buf.toString();
		}
	}

}
