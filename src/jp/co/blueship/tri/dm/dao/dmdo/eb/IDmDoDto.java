package jp.co.blueship.tri.dm.dao.dmdo.eb;

import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;

/**
 * デプロイメント連携のDTOのインターフェースです。
 * @author takashi.ono
 *
 */
public interface IDmDoDto {

	/**
	 * ビルドパッケージEntityを取得します。
	 */
	public IRpEntity getRpEntity();
	/**
	 * ビルドパッケージEntityを設定します。
	 * @param entity
	 */
	public void setRpEntity( IRpEntity entity );
	/**
	 * デプロイメント連携Entityを取得します。
	 */
	public IDmDoEntity getDmDoEntity();
	/**
	 * デプロイメント連携Entityを設定します。
	 * @param entity
	 */
	public void setDmDoEntity( IDmDoEntity entity );
}
