package jp.co.blueship.tri.dm.domain.beans.dop.dto;

import jp.co.blueship.tri.rm.domain.rp.beans.dto.ConfigurationViewBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.LotViewBean;

/**
 * リリースパッケージ・リリース確認画面用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class ReleaseConfirmBean {

	/** リリース概要 */
	private String relSummary = null;
	/** リリース内容 */
	private String relContent = null;
	/** リリース環境情報情報 */
	private ConfigurationViewBean confViewBean = null;
	/** ロット情報 */
	private LotViewBean lotViewBean = null;
	/** リリースパッケージ情報 */
	private ControlViewBean controlViewBean = null;
//	/** 選択ページ */
//	private int selectPageNo = 0;
//	/** ページ制御 */
//	private IPageNoInfo pageInfoView = null;
	
	public String getRelSummary() {
		return relSummary;
	}
	public void setRelSummary(String relSummary) {
		this.relSummary = relSummary;
	}

	public String getRelContent() {
		return relContent;
	}
	public void setRelContent(String relContent) {
		this.relContent = relContent;
	}
	
	public ConfigurationViewBean getConfViewBean() {
		if ( null == confViewBean ) {
			confViewBean = new ConfigurationViewBean();
		}
		return confViewBean;
	}
	public void setConfViewBean( ConfigurationViewBean confViewBean ) {
		this.confViewBean = confViewBean;
	}
	
	public LotViewBean getLotViewBean() {
		if ( null == lotViewBean ) {
			lotViewBean = new LotViewBean();
		}
		return lotViewBean;
	}
	public void setLotViewBean( LotViewBean lotViewBean ) {
		this.lotViewBean = lotViewBean;
	}
	
	public ControlViewBean getControlViewBean() {
		if ( null == controlViewBean ) {
			controlViewBean = new ControlViewBean();
		}
		return controlViewBean;
	}
	public void setControlViewBean( ControlViewBean controlViewBean ) {
		this.controlViewBean = controlViewBean;
	}
	
//	public IPageNoInfo getPageInfoView() {
//		return pageInfoView;
//	}
//	public void setPageInfoView(IPageNoInfo pageInfoView) {
//		this.pageInfoView = pageInfoView;
//	}
//
//	public int getSelectPageNo() {
//		return selectPageNo;
//	}
//	public void setSelectPageNo(int selectPageNo) {
//		this.selectPageNo = selectPageNo;
//	}
}
