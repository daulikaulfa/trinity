package jp.co.blueship.tri.dm.domain.beans.dop.dto;

import java.util.Properties;

import jp.co.blueship.tri.fw.constants.DmDesignEntryKeyByCommercialShell;

/**
 * 配布・リリースパッケージ一覧用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class CommercialShellBean {

	/** 商用シェルのファイル名 */
	private String shellFileName = null;
	/** 設定項目のプロパティ */
	private Properties properties = null;
	/** 商用シェルの種別 */
	
	private DmDesignEntryKeyByCommercialShell shellId = null ;
	
	public DmDesignEntryKeyByCommercialShell getShellId() {
		return shellId;
	}
	public void setShellId(DmDesignEntryKeyByCommercialShell shellId) {
		this.shellId = shellId;
	}
	public Properties getProperties() {
		return properties;
	}
	public void setProperties(Properties properties) {
		this.properties = properties;
	}
	public String getShellFileName() {
		return shellFileName;
	}
	public void setShellFileName(String shellFileName) {
		this.shellFileName = shellFileName;
	}

}
