package jp.co.blueship.tri.dm.domain.beans.dop.dto;

/**
 * 配布・リリースパッケージ一覧用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class DistributeViewBean {

	/** リリース番号 */
	private String relNo = null;
	/** リリース配布番号 */
	private String distNo = null;
	/** DRMS管理バージョン */
	private String drmsVersion = null;
	/** リリース環境名 */
	private String confName = null;
	/** 概要 */
	private String summary = null;
	/** ロット名 */
	private String lotName = null;
	/** 設定日時 */
	private String timerDateTime = null ;
	/** 設定日付 */
	//private String timerDateTimeDate = null;
	/** 設定時刻 */
	//private String timerDateTimeTime = null ;
	/** ステータス */
	private String status = null;
	
	
	public String getRelNo() {
		return relNo;
	}
	public void setRelNo(String relNo) {
		this.relNo = relNo;
	}

	public String getDistNo() {
		return distNo;
	}
	public void setDistNo(String distNo) {
		this.distNo = distNo;
	}

	public String getdrmsVersion() {
		return drmsVersion;
	}
	public void setDrmsVersion(String drmsVersion) {
		this.drmsVersion = drmsVersion;
	}

	public String getConfName() {
		return confName;
	}
	public void setConfName(String confName) {
		this.confName = confName;
	}

	public String getSummaryStatus() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	public String getLotName() {
		return lotName;
	}
	public void setLotName(String lotName) {
		this.lotName = lotName;
	}
//	public String getTimerDateTimeDate() {
//		return timerDateTimeDate;
//	}
//	public void setTimerDateTimeDate(String timerDateTimeDate) {
//		this.timerDateTimeDate = timerDateTimeDate;
//	}
//	public String getTimerDateTimeTime() {
//		return timerDateTimeTime;
//	}
//	public void setTimerDateTimeTime(String timerDateTimeTime) {
//		this.timerDateTimeTime = timerDateTimeTime;
//	}
	public String getTimerDateTime() {
		return timerDateTime;
	}
	public void setTimerDateTime(String timerDateTime) {
		this.timerDateTime = timerDateTime;
	}

}
