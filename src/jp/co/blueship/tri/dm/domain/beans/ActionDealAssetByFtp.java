package jp.co.blueship.tri.dm.domain.beans;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import jp.co.blueship.tri.agent.dm.opt.beans.FlowDealAssetEditSupport;
import jp.co.blueship.tri.agent.dm.opt.beans.dto.DealAssetFtpBean;
import jp.co.blueship.tri.fw.cmn.io.FileCopy;
import jp.co.blueship.tri.fw.cmn.io.FileCopyOverridePreserveLastModified;
import jp.co.blueship.tri.fw.cmn.io.FilePathDelimiter;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.io.constants.Charset;
import jp.co.blueship.tri.fw.cmn.io.constants.FileIoType;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.DmDesignEntryKeyByDealAssetFtp;
import jp.co.blueship.tri.fw.constants.DmDesignEntryKeyByRelFtp;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.ftp.FtpIo;
import jp.co.blueship.tri.fw.ftp.FtpParamBean;
import jp.co.blueship.tri.fw.msg.DmMessageId;
import jp.co.blueship.tri.fw.security.constants.PasswordCategory;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;

/**
 * 統合監視サーバに接続、資源配付プロセス「ＦＴＰ転送」処理を行います。
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public class ActionDealAssetByFtp extends ActionPojoAbstract<IGeneralServiceBean> {

	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();
	private FlowDealAssetEditSupport support = null;

	public void setSupport( FlowDealAssetEditSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		//デプロイメント登録サーバ側にFTP Server設置の場合
		{
			String ftpServer = sheet.getValue( DmDesignEntryKeyByDealAssetFtp.ftpServer ) ;

			if ( StatusFlg.on.value().equals( ftpServer ) ) {
				this.putFile( serviceDto.getParamList() );
			}
		}

		//リリース管理側にFTP Server設置の場合
		{
			String ftpServer = sheet.getValue( DmDesignEntryKeyByRelFtp.ftpServer ) ;

			if ( StatusFlg.on.value().equals( ftpServer ) ) {
				this.copyFile( serviceDto.getParamList() );
			}
		}

		return serviceDto;
	}

	/**
	 * DSL保管庫のRC成果物をデプロイメント登録サーバ側に転送する。
	 *
	 * @param paramList
	 */

	private void putFile( List<Object> paramList ) {

		FtpIo ftpIo = null;
		try {

			DealAssetFtpBean ftpBean = support.getDealAssetFtpBean( paramList ) ;
			if( true == TriStringUtils.isEmpty( ftpBean ) ) {
				throw new TriSystemException( DmMessageId.DM004003F );
			}

			/** 既存資産が存在していれば削除する */
			String srcPath = ftpBean.getSrcPath() ;
			File srcFile = new File( srcPath ) ;
			if( true != srcFile.exists() ) {
				throw new TriSystemException( DmMessageId.DM004004F , srcPath);
			}

			String destBasePath = sheet.getValue( DmDesignEntryKeyByDealAssetFtp.destBasePath ) ;
			{
				destBasePath = TriStringUtils.convertPath( destBasePath );
				String[] paths = StringUtils.split( destBasePath, FilePathDelimiter.DELIMITER );

				if ( 2 > paths.length ) {
					throw new BusinessException( DmMessageId.DM001014E ,  destBasePath );
				}
			}

			ftpIo = new FtpIo( this.getFtpParamBean() );
			ftpIo.openConnection();

			if( true != ftpIo.isDirectory( destBasePath ) ) {
				throw new TriSystemException( DmMessageId.DM004005F , destBasePath);
			}
			ftpIo.setFileType( FileIoType.BINARY_FILE_TYPE );
			ftpIo.changeDirectory( destBasePath ) ;

			/** 同名の既存資産が既にあれば削除する */
			String destFilePath = TriStringUtils.linkPathBySlash( destBasePath , srcFile.getName() ) ;
			if( true == ftpIo.isExist( destFilePath ) ) {
				ftpIo.deleteObject( destFilePath ) ;
			}
			/** 資産を転送する */
			ftpIo.putObject( srcPath , destBasePath );

		} catch( Exception e ) {
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( DmMessageId.DM005000S, e);
		} finally {
			if( null != ftpIo ) {
				try {
					ftpIo.closeConnection() ;
				} catch( Exception e ) {
					throw new TriSystemException( DmMessageId.DM005001S, e);
				}
			}
		}
	}

	/**
	 * Ftp接続に用いるパラメータ群をセットする
	 * @return パラメータをセットしたFtpParamBean
	 * @throws Exception
	 */
	private FtpParamBean getFtpParamBean() throws Exception {

		FtpParamBean ftpParam = new FtpParamBean();

		String host = sheet.getValue( DmDesignEntryKeyByDealAssetFtp.host ) ;
		if( TriStringUtils.isEmpty( host ) ) {
			throw new TriSystemException( DmMessageId.DM004006F );
		}
		String port = sheet.getValue( DmDesignEntryKeyByDealAssetFtp.port ) ;
		if( TriStringUtils.isEmpty( port ) ) {
			throw new TriSystemException( DmMessageId.DM004007F );
		}
		String user = sheet.getValue( DmDesignEntryKeyByDealAssetFtp.user ) ;
		if( TriStringUtils.isEmpty( user ) ) {
			throw new TriSystemException( DmMessageId.DM004008F );
		}
		String pasv = sheet.getValue( DmDesignEntryKeyByDealAssetFtp.pasv ) ;
		if( TriStringUtils.isEmpty( pasv ) ) {
			throw new TriSystemException( DmMessageId.DM004009F );
		}
		String controlEncoding = sheet.getValue( DmDesignEntryKeyByDealAssetFtp.controlEncoding ) ;
		if( TriStringUtils.isEmpty( controlEncoding ) ) {
			throw new TriSystemException( DmMessageId.DM004010F );
		}

		ftpParam.setHost( host );
		if( true != TriStringUtils.isEmpty( port ) ) {
			ftpParam.setPort( Integer.parseInt( port ) );
		}
		ftpParam.setUser( user );
		ftpParam.setPass( support.getSmFinderSupport().findPasswordEntity( PasswordCategory.FTP , host , user ).getPassword() ) ;

		if( true != TriStringUtils.isEmpty( pasv ) ) {
			ftpParam.setPasvMode( StatusFlg.on.value().toString().equals( pasv.toLowerCase() ) ? true : false ) ;
		}
		if( true != TriStringUtils.isEmpty( controlEncoding ) ) {
			ftpParam.setControlEncoding( this.getCharset( controlEncoding ) );
		}

		return ftpParam ;
	}
	/**
	 * 「文字コードセット」文字列をenum型Charsetにコンバートする
	 * <br>
	 * @param encoding 「文字コードセット」文字列
	 * @return enum型Charset
	 * @throws Exception
	 */
	private Charset getCharset( String encoding ) throws Exception {
		Charset charset = null;
		try {
			charset = Charset.value( encoding );
		} catch ( Exception e ) {
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( DmMessageId.DM004011F, e , encoding );
		}
		return charset;
	}

	/**
	 * DSL保管庫のRC成果物をFTP公開フォルダにコピーする。
	 *
	 * @param paramList
	 */

	private void copyFile( List<Object> paramList ) {

		try {
			DealAssetFtpBean ftpBean = support.getDealAssetFtpBean( paramList ) ;
			if( true == TriStringUtils.isEmpty( ftpBean ) ) {
				throw new TriSystemException( DmMessageId.DM004003F );
			}

			String srcPath = ftpBean.getSrcPath();
			File srcFile = new File( srcPath );
			if( TriStringUtils.isEmpty( srcPath ) ) {
				throw new TriSystemException( DmMessageId.DM004002F );
			}

			//カスタマイズ定義をリモート情報に転記する(配布管理側でカスタマイズ定義を参照させないため)
			{
				ftpBean.setFtpServer		( sheet.getValue( DmDesignEntryKeyByRelFtp.ftpServer ) );
				ftpBean.setHost				( sheet.getValue( DmDesignEntryKeyByRelFtp.host ) );
				ftpBean.setPort				( sheet.getValue( DmDesignEntryKeyByRelFtp.port ) );
				ftpBean.setUser				( sheet.getValue( DmDesignEntryKeyByRelFtp.user ) );
				ftpBean.setPasv				( sheet.getValue( DmDesignEntryKeyByRelFtp.pasv ) );
				ftpBean.setControlEncoding	( sheet.getValue( DmDesignEntryKeyByRelFtp.controlEncoding ) );
				ftpBean.setSrcFtpPath		( sheet.getValue( DmDesignEntryKeyByRelFtp.srcFtpPath ) );
				ftpBean.setSrcLocalPath		( sheet.getValue( DmDesignEntryKeyByRelFtp.srcLocalPath ) );
				ftpBean.setDestLocalPath	( sheet.getValue( DmDesignEntryKeyByRelFtp.destLocalPath ) );
				ftpBean.setSrcExtension		( sheet.getValue( DmDesignEntryKeyByRelFtp.srcExtension ) );
			}

			String srcLocalPath = ftpBean.getSrcLocalPath();
			File srcLocalFile = new File( srcLocalPath );

			if( true != srcLocalFile.exists() || true != srcLocalFile.isDirectory()  ) {
				throw new TriSystemException( DmMessageId.DM004012F , srcLocalPath);
			}

			{
				String srcFtpPath = ftpBean.getSrcFtpPath();
				srcFtpPath = TriStringUtils.convertPath( srcFtpPath );
				String[] paths = StringUtils.split( srcFtpPath, FilePathDelimiter.DELIMITER );

				if ( 2 > paths.length ) {
					throw new BusinessException( DmMessageId.DM001014E , sheet.getValue( DmDesignEntryKeyByRelFtp.srcFtpPath ) );
				}
			}

			/** 既存資産が存在していれば削除する */
			srcLocalFile = new File( TriStringUtils.linkPath( srcLocalPath, srcFile.getName() ) );

			if ( srcLocalFile.exists() ) {
				TriFileUtils.delete( srcLocalFile );
			}

			FileCopy fileCopy = new FileCopyOverridePreserveLastModified();
			fileCopy.copyFileToFile( srcFile, srcLocalFile, new ArrayList<File>() );

		} catch (IOException e) {
			throw new TriSystemException( DmMessageId.DM005002S , e );
		}

	}

}
