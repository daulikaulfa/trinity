package jp.co.blueship.tri.dm.domain.beans.dop.dto;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public class ReleasePackageViewBean {
	private String rpId;
	private String bpId;
	private String environmentNm;
	private String createdBy;
	private String createdByIconPath;
	private String startTime;
	private String endTime;

	public String getRpId() {
		return rpId;
	}
	public ReleasePackageViewBean setRpId(String rpId) {
		this.rpId = rpId;
		return this;
	}

	public String getBpId() {
		return bpId;
	}
	public ReleasePackageViewBean setBpId(String bpId) {
		this.bpId = bpId;
		return this;
	}

	public String getEnvironmentNm() {
		return environmentNm;
	}
	public ReleasePackageViewBean setEnvironmentNm(String environmentNm) {
		this.environmentNm = environmentNm;
		return this;
	}

	public String getCreatedBy() {
		return createdBy;
	}
	public ReleasePackageViewBean setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
		return this;
	}

	public String getCreatedByIconPath() {
		return createdByIconPath;
	}
	public ReleasePackageViewBean setCreatedByIconPath(String createdByIconPath) {
		this.createdByIconPath = createdByIconPath;
		return this;
	}

	public String getStartTime() {
		return startTime;
	}
	public ReleasePackageViewBean setStartTime(String startTime) {
		this.startTime = startTime;
		return this;
	}

	public String getEndTime() {
		return endTime;
	}
	public ReleasePackageViewBean setEndTime(String endTime) {
		this.endTime = endTime;
		return this;
	}
}
