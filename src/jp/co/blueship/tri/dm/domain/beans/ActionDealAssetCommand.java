package jp.co.blueship.tri.dm.domain.beans;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import jp.co.blueship.tri.agent.dm.opt.DealAssetServiceController;
import jp.co.blueship.tri.agent.dm.opt.beans.FlowDealAssetEditSupport;
import jp.co.blueship.tri.agent.dm.opt.beans.dto.DealAssetServiceBean;
import jp.co.blueship.tri.agent.dm.opt.beans.dto.DealAssetServiceResponseBean;
import jp.co.blueship.tri.fw.cmn.utils.DesignUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.di.spring.ContextServiceAttributes;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.DmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;

/**
 * 統合監視サーバに接続、資源配付コマンド実行 処理を行います。
 *
 */
public class ActionDealAssetCommand extends ActionPojoAbstract<IGeneralServiceBean> {

	private FlowDealAssetEditSupport support = null;
	/**
	 *
	 * @param support
	 */
	public void setSupport( FlowDealAssetEditSupport support ) {
		this.support = support;
	}

	ContextServiceAttributes contextServiceAttributes = null ;

	public void setContextServiceAttributes(
			ContextServiceAttributes contextServiceAttributes) {
		this.contextServiceAttributes = contextServiceAttributes;
	}

	@SuppressWarnings("resource")
	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		try {

			List<String> fileSystemList	= new ArrayList<String>();
			List<String> classPathList	= new ArrayList<String>();

			String configLocation = contextServiceAttributes.getConfigLocations();
			File configFile = new File( DesignUtils.getPriorityDefinePath() , configLocation );

			if ( configFile.exists() ) {

				fileSystemList.add( configFile.getAbsolutePath() );

			} else {

				classPathList.add( configLocation );
			}

			ApplicationContext beanFactory = null;
			try {
				beanFactory = new ClassPathXmlApplicationContext(
						(String[])classPathList.toArray( new String[]{} ),
						new FileSystemXmlApplicationContext(
								(String[])fileSystemList.toArray( new String[]{} )) );

			} catch( BeansException be ) {

				throw new BusinessException( DmMessageId.DM001015E , be , configLocation );
			}

			List<Object> paramList = serviceDto.getParamList();

			ApplicationContext context = beanFactory;
			DealAssetServiceController controller = (DealAssetServiceController) context.getBean( contextServiceAttributes.getDataSource() ) ;

			String serviceName = contextServiceAttributes.getServiceName() ;

			DealAssetServiceBean bean = this.support.getDealAssetServiceBean( paramList ) ;
			if( TriStringUtils.isEmpty( bean ) ) {
				throw new TriSystemException( DmMessageId.DM005003S ) ;
			}

			DealAssetServiceResponseBean retBean = controller.execute( serviceName , bean ) ;
			paramList.add( retBean ) ;

		} catch ( Exception e ) {
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( DmMessageId.DM005004S , e );
		}

		return serviceDto;
	}

}
