package jp.co.blueship.tri.dm.domain.beans;


/**
 * 商用シェルの定義名の列挙型です。
 *
 */
public enum CommercialShellID {

	/**
	 * 正常終了
	 */
	SHELL_ASSET_REG( "01" ),
	/**
	 * 正常終了
	 */
	SHELL_ASSET_DELETE( "02" ),
	/**
	 * 正常終了
	 */
	SHELL_REG_HISTORY( "03" ),
	/**
	 * 正常終了
	 */
	SHELL_ASSET_DEAL_TIMER_SET( "04" ),
	/**
	 * 正常終了（業務例外）
	 */
	SHELL_ASSET_DEAL_TIMER_CANCEL( "05" ),
	/**
	 * 正常終了（業務例外）
	 */
	SHELL_ASSET_DEAL_STATUS_CHK( "06" ) ;

	String code ;

	private CommercialShellID( String code ) {
		this.code = code ;
	}

	/**
	 * 該当する商用シェルコードの定義名を検索し、そのコード値を取得する
	 * @param name 商用シェル定義名
	 * @return 商用シェルコード
	 */
	public static CommercialShellID getValue( String code ) {
		for ( CommercialShellID define : values() ) {
			if ( define.toString().equals( code) ) {
				return valueOf( code ) ;
			}
		}

		return null;
	}
	/**
	 * 商用シェルコード値を取得する
	 * @return 商用シェルコード値
	 */
	public String getCode() {
		return code ;
	}
}
