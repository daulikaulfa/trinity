package jp.co.blueship.tri.dm.domain.beans;

import java.io.File;
import java.util.List;

import jp.co.blueship.tri.dm.DmDesignBusinessRuleUtils;
import jp.co.blueship.tri.dm.domain.beans.dop.dto.CommercialShellBean;
import jp.co.blueship.tri.dm.support.FlowRelDistributeEditSupport;
import jp.co.blueship.tri.fw.cmn.io.constants.Charset;
import jp.co.blueship.tri.fw.cmn.io.constants.LinefeedCode;
import jp.co.blueship.tri.fw.cmn.utils.TextMakerFromTemplate;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.DmDesignEntryKeyByCommercialShell;
import jp.co.blueship.tri.fw.constants.UmDesignEntryKeyByCommon;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.DmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;


/**
 * 資源配付に用いる商用シェルの生成処理を行います。
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public class ActionMakeCommercialShell extends ActionPojoAbstract<IGeneralServiceBean> {
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowRelDistributeEditSupport support = null;
	/**
	 *
	 * @param support
	 */
	public void setSupport( FlowRelDistributeEditSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		try {

			List<Object> paramList = serviceDto.getParamList();

			CommercialShellBean commercialShellBean = support.getCommercialShellBean( paramList ) ;

			String homeTopPath	= sheet.getValue( UmDesignEntryKeyByCommon.homeTopPath ) ;
			File templateDir	= DmDesignBusinessRuleUtils.getTemplatePath();

			//商用シェルテンプレートの参照先パス設定
			String templateBasepath = sheet.getValue( DmDesignEntryKeyByCommercialShell.templateBaseRelativePath ) ;
			File templateBaseDir	= new File( templateDir, templateBasepath );
			if( TriStringUtils.isEmpty( templateBasepath ) ||
				true != templateBaseDir.isDirectory() ) {
				throw new TriSystemException( DmMessageId.DM004013F , templateBaseDir.getCanonicalPath()) ;
			}

			//商用シェルの出力先ディレクトリパス設定
			String shellOutputBasepath	= sheet.getValue( DmDesignEntryKeyByCommercialShell.shellOutputBaseRelativePath ) ;
			File shellOutputBaseDir		= new File( homeTopPath, shellOutputBasepath );
			if( TriStringUtils.isEmpty( shellOutputBasepath ) ||
					true != shellOutputBaseDir.isDirectory() ) {
				throw new TriSystemException( DmMessageId.DM004014F , shellOutputBaseDir.getCanonicalPath()) ;
			}

			//文字コードの設定
			String charsetCodeValue = sheet.getValue( DmDesignEntryKeyByCommercialShell.charset ) ;
			Charset charset = Charset.value( charsetCodeValue ) ;
			if( TriStringUtils.isEmpty( charset ) ) {
				throw new TriSystemException( DmMessageId.DM004015F , charsetCodeValue) ;
			}

			//改行コードの設定
			String linefeedCodeValue = sheet.getValue( DmDesignEntryKeyByCommercialShell.linefeedCode ) ;
			LinefeedCode linefeedCode = LinefeedCode.getLabel( linefeedCodeValue ) ;
			if( TriStringUtils.isEmpty( linefeedCode ) ) {
				throw new TriSystemException( DmMessageId.DM004016F , linefeedCodeValue) ;
			}

			TextMakerFromTemplate textMaker = new TextMakerFromTemplate() ;
//			textMaker.setTemplatePath( StringAddonUtil.linkPathBySlash( templateBasepath , commercialShellBean.getShellId().toString() ) ) ;
			textMaker.setTemplatePath( TriStringUtils.linkPathBySlash( templateBaseDir.toString() , sheet.getValue( commercialShellBean.getShellId() ) ) ) ; //RFC-1203019
			textMaker.setOutputFilePath( TriStringUtils.linkPathBySlash( shellOutputBaseDir.toString() , commercialShellBean.getShellFileName() ) ) ; //RFC-1203019
			textMaker.setCharset( charset ) ;
			textMaker.setLinefeedCode( linefeedCode ) ;

			textMaker.setParamMap( commercialShellBean.getProperties() ) ;
			textMaker.execute() ;

		} catch ( Exception e ) {
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( DmMessageId.DM005009S , e );
		}

		return serviceDto;
	}

}
