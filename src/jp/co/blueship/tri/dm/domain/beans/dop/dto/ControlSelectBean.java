package jp.co.blueship.tri.dm.domain.beans.dop.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * リリース選択画面用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class ControlSelectBean {

//	/** 選択ページ */
//	private int selectPageNo = 0;
//	/** ページ制御 */
//	private IPageNoInfo pageInfoView = null;
	/** ビルドパッケージ情報 */
	private List<ControlViewBean> controlViewBeanList = null;

	
	public List<ControlViewBean> getControlViewBeanList() {
		if ( null == controlViewBeanList ) {
			controlViewBeanList = new ArrayList<ControlViewBean>();
		}
		return controlViewBeanList;
	}
	public void setControlViewBeanList( List<ControlViewBean> controlViewBeanList ) {
		this.controlViewBeanList = controlViewBeanList;
	}

//	public IPageNoInfo getPageInfoView() {
//		return pageInfoView;
//	}
//	public void setPageInfoView(IPageNoInfo pageInfoView) {
//		this.pageInfoView = pageInfoView;
//	}
//
//	public int getSelectPageNo() {
//		return selectPageNo;
//	}
//	public void setSelectPageNo(int selectPageNo) {
//		this.selectPageNo = selectPageNo;
//	}
	
}
