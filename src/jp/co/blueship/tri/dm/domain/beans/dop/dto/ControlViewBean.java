package jp.co.blueship.tri.dm.domain.beans.dop.dto;


/**
 * 配布・リリースパッケージ一覧用
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class ControlViewBean {

	/** リリース番号 */
	private String relNo = null;
	/** 環境番号 */
	private String envNo = null;
	/** ロット番号 */
	private String lotId = null;
	/** 概要 */
	private String summary = null;
	/** 作成日時 */
	private String generateDate = null;


	public String getRelNo() {
		return relNo;
	}
	public void setRelNo(String relNo) {
		this.relNo = relNo;
	}

	public String getEnvNo() {
		return envNo;
	}
	public void setEnvNo(String envNo) {
		this.envNo = envNo;
	}

	public String getLotNo() {
		return lotId;
	}
	public void setLotNo(String lotId) {
		this.lotId = lotId;
	}

	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getGenerateDate() {
		return generateDate;
	}
	public void setGenerateDate( String generateDate ) {
		this.generateDate = generateDate;
	}

}
