package jp.co.blueship.tri.dm.domain.dop;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.CopyOnWriteArrayList;

import jp.co.blueship.tri.agent.dm.opt.beans.dto.DealAssetServiceBean;
import jp.co.blueship.tri.agent.dm.opt.beans.dto.DealAssetServiceResponseBean;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.dm.constants.RelDistributeCommercialShellPropertyDefineID;
import jp.co.blueship.tri.dm.constants.RelDistributeScreenID;
import jp.co.blueship.tri.dm.dao.dmdo.eb.IDmDoDto;
import jp.co.blueship.tri.dm.domain.beans.CommercialShellID;
import jp.co.blueship.tri.dm.domain.beans.dop.dto.CommercialShellBean;
import jp.co.blueship.tri.dm.domain.dop.dto.FlowRelDistributeCancelServiceBean;
import jp.co.blueship.tri.dm.support.FlowRelDistributeEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.DesignSheetUtils;
import jp.co.blueship.tri.fw.cmn.utils.DesignUtils;
import jp.co.blueship.tri.fw.cmn.utils.ItemCheckAddonUtils;
import jp.co.blueship.tri.fw.cmn.utils.RelCommonAddonUtil;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.constants.DmDesignEntryKeyByCommercialShell;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.status.DmDoStatusId;
import jp.co.blueship.tri.fw.constants.status.UmActStatusId;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.DmMessageId;
import jp.co.blueship.tri.fw.msg.MessageParameter;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.HistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;

/**
 * リリースパッケージ・リリース状況画面の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public class FlowRelDistributeCompCancelService implements IDomain<FlowRelDistributeCancelServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowRelDistributeEditSupport support = null;
	public void setSupport(FlowRelDistributeEditSupport support) {
		this.support = support;
	}

	private List<IDomain<IGeneralServiceBean>> actions = new ArrayList<IDomain<IGeneralServiceBean>>();

	private List<IDomain<IGeneralServiceBean>> makeShellActions = new ArrayList<IDomain<IGeneralServiceBean>>();

	public void setActions(List<IDomain<IGeneralServiceBean>> actions) {
		this.actions = actions;
	}

	public void setMakeShellActions(List<IDomain<IGeneralServiceBean>> actions) {
		this.makeShellActions = actions;
	}
	@Override
	public IServiceDto<FlowRelDistributeCancelServiceBean> execute( IServiceDto<FlowRelDistributeCancelServiceBean> serviceDto ) {

		FlowRelDistributeCancelServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

//			String refererID = paramBean.getReferer();//使用されていないローカル変数
			String forwordID = paramBean.getForward();

			if ( true != RelDistributeScreenID.COMP_CANCEL.equals( forwordID ) ) {
				return serviceDto;
			}

			if ( RelDistributeScreenID.COMP_CANCEL.equals( forwordID ) ) {

				if ( ! ScreenType.bussinessException.equals( paramBean.getScreenType() ) ) {

					List<Object> paramList = new CopyOnWriteArrayList<Object>() ;

					String distNo = paramBean.getDistNo();
					ItemCheckAddonUtils.checkRelNo( distNo );

					IDmDoDto dmDoDto = this.support.getDmDoDto( distNo );
					// グループの存在チェック
					ILotDto dto = this.support.getAmFinderSupport().findLotDto( dmDoDto.getRpEntity().getLotId() );
					List<IGrpUserLnkEntity> groupUsers	= support.getUmFinderSupport().findGrpUserLnkByUserId( paramBean.getUserId() );
					RelCommonAddonUtil.checkAccessableGroup( dto, this.support.getUmFinderSupport().getGrpDao(), groupUsers );

//					 下層のアクションを実行する
					String linkNo = this.support.makeLinkNoLabel( distNo ) ;
					String confName = paramBean.getConfViewBean().getConfName() ;

					DealAssetServiceBean dealServiceBean = new DealAssetServiceBean();

					dealServiceBean.setTargetEnvironment(confName);
					dealServiceBean.setDrmsManagementNo(linkNo);
					paramList.add(dealServiceBean);

					IServiceDto<IGeneralServiceBean> innerServiceDto = new ServiceDto<IGeneralServiceBean>()
							.setServiceBean( paramBean )
							.setParamList( paramList );

					// 商用シェル出力の設定
					if ( !paramBean.isRmi() ) {
						this.outputCommercialShell( paramBean, distNo , linkNo , confName ) ;
					}

					// RMI接続設定
					DealAssetServiceResponseBean resBean = null;
					if (paramBean.isRmi()) {

						// 配付資源取消
						for ( IDomain<IGeneralServiceBean> action : actions ) {
							action.execute( innerServiceDto );
						}
						// 戻り値を確認
						resBean = this.support.getDealAssetServiceResponseBean( paramList ) ;
						if ( null == resBean ) {
							throw new BusinessException( DmMessageId.DM001006E );
						} else  {
							if ( 0 != resBean.getReturnCode() ) {
								String errMessage = this.support.makeRmiErrorMessage( resBean.getOutLog() , resBean.getErrLog() ) ;
								paramBean.setDrmsMessage( errMessage );
								if ( 1 == resBean.getReturnCode() ) {
									paramBean.setInfoMessage(DmMessageId.DM001007E);
								} else {
									paramBean.setInfoMessage(DmMessageId.DM001011E);
								}
							}
						}
					}
					// trinity登録
					if (paramBean.isRmi() && resBean.getReturnCode() != 0) {
						;// RMI接続を行い、戻り値が正常でない場合は何もしない。
					} else {
						// trinity登録処理
						// リリース情報のステータスを取消に更新
						dmDoDto.getDmDoEntity().setDelCmt	( paramBean.getCancelComment() );
						dmDoDto.getDmDoEntity().setUpdUserNm( paramBean.getUserName() );
						dmDoDto.getDmDoEntity().setUpdUserId( paramBean.getUserId() );
						dmDoDto.getDmDoEntity().setStsId	( DmDoStatusId.JobCancelled.getStatusId() );
						dmDoDto.getDmDoEntity().setDelStsId	( StatusFlg.off );
						this.support.getDmDoDao().update( dmDoDto.getDmDoEntity() );
						// リリース番号を返却
						paramBean.setRelNo( distNo );
						paramBean.setDrmsMessage(DesignUtils.getMessageParameter( MessageParameter.CANCELED_REGISTERE_ASSET_DISTRIBUTION.getKey() ) );

						//履歴登録
						if ( DesignSheetUtils.isRecord() ){


							IHistEntity histEntity = new HistEntity();
							histEntity.setActSts( UmActStatusId.none.getStatusId() );

							support.getDmDoHistDao().insert( histEntity , dmDoDto);
						}
					}
				}
			}
			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( DmMessageId.DM005011S , e );
		}
	}

	/**
	 *
	 * @param distNo
	 * @param linkNo
	 * @param confName
	 */
	private void outputCommercialShell(
			FlowRelDistributeCancelServiceBean paramBean,String distNo , String linkNo , String confName ) {

		List<Object> paramList = new CopyOnWriteArrayList<Object>() ;

		CommercialShellBean shellBean = new CommercialShellBean();
		shellBean.setShellId(DmDesignEntryKeyByCommercialShell.shellAssetDelete);
		String shellFileName = this.support.makeShellFileName( CommercialShellID.SHELL_ASSET_DELETE , distNo ) ;
		shellBean.setShellFileName(shellFileName);

		Properties properties = new Properties() ;
		properties.put( RelDistributeCommercialShellPropertyDefineID.GROUP_NAME, confName ) ;
//		properties.put( RelDistributeCommercialShellPropertyDefineID.RC_NO , distNo ) ;
		properties.put( RelDistributeCommercialShellPropertyDefineID.RC_NO , linkNo ) ;
		shellBean.setProperties(properties);
		paramList.add(shellBean);

		IServiceDto<IGeneralServiceBean> innerServiceDto = new ServiceDto<IGeneralServiceBean>()
				.setServiceBean( paramBean )
				.setParamList( paramList );


		for ( IDomain<IGeneralServiceBean> action : makeShellActions ) {
			action.execute( innerServiceDto );
		}
	}

}
