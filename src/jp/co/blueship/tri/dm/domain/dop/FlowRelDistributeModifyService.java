package jp.co.blueship.tri.dm.domain.dop;

import java.util.List;

import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.dm.DmItemCheckAddonUtils;
import jp.co.blueship.tri.dm.constants.RelDistributeScreenID;
import jp.co.blueship.tri.dm.dao.dmdo.eb.IDmDoDto;
import jp.co.blueship.tri.dm.domain.beans.dop.dto.ControlViewBean;
import jp.co.blueship.tri.dm.domain.dop.dto.FlowRelDistributeModifyServiceBean;
import jp.co.blueship.tri.dm.support.FlowRelDistributeEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.ItemCheckAddonUtils;
import jp.co.blueship.tri.fw.cmn.utils.RelCommonAddonUtil;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.DmMessageId;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.BaseInfoBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.BaseInfoInputBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.ConfigurationViewBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.LotViewBean;


/**
 * 配付資源・編集画面の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public class FlowRelDistributeModifyService implements IDomain<FlowRelDistributeModifyServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowRelDistributeEditSupport support;
	public void setSupport(FlowRelDistributeEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowRelDistributeModifyServiceBean> execute( IServiceDto<FlowRelDistributeModifyServiceBean> serviceDto ) {

		FlowRelDistributeModifyServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID = paramBean.getReferer();
			String forwordID = paramBean.getForward();
			String screenType	= paramBean.getScreenType();

			if ( true != RelDistributeScreenID.MODIFY.equals( refererID ) &&
					true != RelDistributeScreenID.MODIFY.equals( forwordID ) ){
				return serviceDto;
			}

			String selectedDistNo = paramBean.getDistNo();

			if ( RelDistributeScreenID.MODIFY.equals( refererID )) {

				ItemCheckAddonUtils.checkRelNo	( selectedDistNo );

				if ( ScreenType.next.equals( screenType )) {
					DmItemCheckAddonUtils.checkRelDistributeBaseInfoInput( paramBean.getBaseInfoBean() );
				}

				// 基本情報
				paramBean.setDistNo(selectedDistNo);
				IRpEntity relEntity = this.support.findRpEntity( selectedDistNo );
				paramBean.setBaseInfoBean( paramBean.getBaseInfoBean() );

				// グループの存在チェック
				ILotDto dto = this.support.getAmFinderSupport().findLotDto( relEntity.getLotId() );

				List<IGrpUserLnkEntity> groupUsers	= support.getUmFinderSupport().findGrpUserLnkByUserId( paramBean.getUserId() );
				RelCommonAddonUtil.checkAccessableGroup( dto, this.support.getUmFinderSupport().getGrpDao(), groupUsers );

				// 環境情報
//				String envNo = relEntity.getEnvNo();
//				ConfigurationViewBean confViewBean =  setEnvReleaseConfirm(envNo);
//				paramBean.setConfViewBean(confViewBean);
				paramBean.setConfViewBean( paramBean.getConfViewBean() );

				// ロット情報
//				String lotId = relEntity.getLotNo();
//				LotViewBean lotViewBean = setLotReleaseConfirm(lotId);
//				paramBean.setLotViewBean(lotViewBean);
				paramBean.setLotViewBean( paramBean.getLotViewBean() );

				// リリース情報
//				String relNo = relEntity.getRelNo();
//				ControlViewBean controlViewBean = setControlReleaseConfirm(relNo);
//				paramBean.setControlViewBean(controlViewBean);
				paramBean.setControlViewBean( paramBean.getControlViewBean() );

			}

			if ( RelDistributeScreenID.MODIFY.equals( forwordID )) {

				if ( null != paramBean.getDistNo() ) {

					ItemCheckAddonUtils.checkRelNo	( selectedDistNo );

					// 基本情報
					paramBean.setDistNo(selectedDistNo);
					IDmDoDto dmDoDto = this.support.getDmDoDto( selectedDistNo );

					// グループの存在チェック
					ILotDto dto = this.support.getAmFinderSupport().findLotDto( dmDoDto.getRpEntity().getLotId() );
					List<IGrpUserLnkEntity> groupUsers	= support.getUmFinderSupport().findGrpUserLnkByUserId( paramBean.getUserId() );
					RelCommonAddonUtil.checkAccessableGroup( dto, this.support.getUmFinderSupport().getGrpDao(), groupUsers );

					BaseInfoBean baseInfoBean = new BaseInfoBean();
					BaseInfoInputBean inputBean = this.support.makeBaseInfoInputBean( dmDoDto.getDmDoEntity() ) ;
					baseInfoBean.setBaseInfoInputBean(inputBean);

					if ( RelDistributeScreenID.MODIFY_CONFIRM.equals( refererID ) ) {
						baseInfoBean = paramBean.getBaseInfoBean();
					}
					paramBean.setBaseInfoBean(baseInfoBean);

					// 環境情報
					String envNo = dmDoDto.getRpEntity().getBldEnvId();
					ConfigurationViewBean confViewBean =  this.support.getEnvReleaseConfirm(envNo);
					paramBean.setConfViewBean(confViewBean);

					// ロット情報
					String lotId = dmDoDto.getRpEntity().getLotId();
					LotViewBean lotViewBean = this.support.getLotReleaseConfirm(lotId);
					paramBean.setLotViewBean(lotViewBean);

					// リリース情報
					String relNo = dmDoDto.getRpEntity().getRpId();
					ControlViewBean controlViewBean = this.support.getControlReleaseConfirm(relNo);
					paramBean.setControlViewBean( controlViewBean );

				}
			}

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(DmMessageId.DM005020S, e);
		}
	}
}
