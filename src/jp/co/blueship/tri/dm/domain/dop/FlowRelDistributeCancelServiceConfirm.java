package jp.co.blueship.tri.dm.domain.dop;

import java.util.List;

import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.dm.constants.RelDistributeScreenID;
import jp.co.blueship.tri.dm.dao.dmdo.eb.IDmDoDto;
import jp.co.blueship.tri.dm.domain.beans.dop.dto.ControlViewBean;
import jp.co.blueship.tri.dm.domain.dop.dto.FlowRelDistributeCancelServiceBean;
import jp.co.blueship.tri.dm.support.FlowRelDistributeEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.ItemCheckAddonUtils;
import jp.co.blueship.tri.fw.cmn.utils.RelCommonAddonUtil;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.DmMessageId;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.BaseInfoBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.BaseInfoInputBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.ConfigurationViewBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.LotViewBean;


/**
 * 登録取消確認画面の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public class FlowRelDistributeCancelServiceConfirm implements IDomain<FlowRelDistributeCancelServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowRelDistributeEditSupport support = null;
	public void setSupport(FlowRelDistributeEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowRelDistributeCancelServiceBean> execute( IServiceDto<FlowRelDistributeCancelServiceBean> serviceDto ) {

		FlowRelDistributeCancelServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID	= paramBean.getReferer();
			String forwordID	= paramBean.getForward();
//			String screenType	= paramBean.getScreenType();//未使用のローカル変数

			if ( true != RelDistributeScreenID.CANCEL_CONFIRM.equals( refererID ) &&
					true != RelDistributeScreenID.CANCEL_CONFIRM.equals( forwordID ) ){
				return serviceDto;
			}

			String selectedControlNo = paramBean.getDistNo();

			if( RelDistributeScreenID.CANCEL_CONFIRM.equals( forwordID ) ) {

				ItemCheckAddonUtils.checkRelNo	( selectedControlNo );

				// 基本情報
				paramBean.setDistNo( selectedControlNo );
				IDmDoDto dmDoDto = this.support.getDmDoDto( selectedControlNo );
				if ( true != TriStringUtils.isEmpty( dmDoDto.getDmDoEntity().getTimerSettingDate() ) ) {
					throw new BusinessException( DmMessageId.DM001003E );
				}

				// グループの存在チェック
				ILotDto dto = this.support.getAmFinderSupport().findLotDto( dmDoDto.getRpEntity().getLotId() );
				List<IGrpUserLnkEntity> groupUsers	= support.getUmFinderSupport().findGrpUserLnkByUserId( paramBean.getUserId() );
				RelCommonAddonUtil.checkAccessableGroup( dto, this.support.getUmFinderSupport().getGrpDao(), groupUsers );

				BaseInfoInputBean inputBean = this.support.makeBaseInfoInputBean( dmDoDto.getDmDoEntity() ) ;
				BaseInfoBean baseInfoBean = new BaseInfoBean();
				baseInfoBean.setBaseInfoInputBean(inputBean);
				paramBean.setBaseInfoBean(baseInfoBean);

				// 環境情報
				String envNo = dmDoDto.getRpEntity().getBldEnvId();
				ConfigurationViewBean confViewBean = this.support.getEnvReleaseConfirm(envNo);
				paramBean.setConfViewBean(confViewBean);

				// ロット情報
				String lotId = dmDoDto.getRpEntity().getLotId();
				LotViewBean lotViewBean = this.support.getLotReleaseConfirm(lotId);
				paramBean.setLotViewBean(lotViewBean);

				// リリースパッケージ情報
				String relNo = dmDoDto.getRpEntity().getRpId();
				ControlViewBean controlViewBean = this.support.getControlReleaseConfirm(relNo);
				paramBean.setControlViewBean( controlViewBean );

			}

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( DmMessageId.DM005010S, e );
		}
	}
}
