package jp.co.blueship.tri.dm.domain.dop.dto;

import jp.co.blueship.tri.dm.domain.beans.dop.dto.ControlSelectBean;
import jp.co.blueship.tri.dm.domain.beans.dop.dto.ReleaseConfirmBean;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.BaseInfoBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.ConfigurationSelectBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.LotSelectBean;

/**
 * リリース配布・選択条件～リリース選択～基本情報入力～リリース作成確認用
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class FlowRelDistributeEntryServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;

	/**
	 * リリース基本情報
	 */
	private BaseInfoBean baseInfoBean = null;
	/**
	 * 環境選択
	 */
	private ConfigurationSelectBean confSelectBean = null;
	/**
	 * 選択された環境番号
	 */
	private String selectedEnvNo = null;
	/**
	 * ロット選択
	 */
	private LotSelectBean lotSelectBean = null;
	/**
	 * 選択されたロット番号
	 */
	private String selectedLotNo = null;
	/**
	 * リリース選択
	 */
	private ControlSelectBean controlSelectBean = null;
	/**
	 * 選択されたリリース番号
	 */
	private String selectedControlNo = null;
	/**
	 * リリースパッケージ確認
	 */
	private ReleaseConfirmBean releaseConfirmBean = null;
	/**
	 * リリース番号
	 */
	private String relNo = null;
	/**
	 * リリース配布番号
	 */
	private String distNo = null;
	/**
	 *  統合監視のメッセージ
	 */
	private String drmsMessage = null;
	/**
	 * RMI接続
	 */
	private boolean rmi = false;


	public BaseInfoBean getBaseInfoBean() {
		if ( null == baseInfoBean ) {
			baseInfoBean = new BaseInfoBean();
		}
		return baseInfoBean;
	}
	public void setBaseInfoBean( BaseInfoBean baseInfoBean ) {
		this.baseInfoBean = baseInfoBean;
	}

	public ConfigurationSelectBean getConfSelectBean() {
		if ( null == confSelectBean ) {
			confSelectBean = new ConfigurationSelectBean();
		}
		return confSelectBean;
	}
	public void setConfSelectBean(ConfigurationSelectBean confSelectBean) {
		this.confSelectBean = confSelectBean;
	}

	public String getSelectedEnvNo() {
		return selectedEnvNo;
	}
	public void setSelectedEnvNo( String selectedEnvNo ) {
		this.selectedEnvNo = selectedEnvNo;
	}

	public LotSelectBean getLotSelectBean() {
		if ( null == lotSelectBean ) {
			lotSelectBean = new LotSelectBean();
		}
		return lotSelectBean;
	}
	public void setLotSelectBean(LotSelectBean lotSelectBean) {
		this.lotSelectBean = lotSelectBean;
	}

	public String getSelectedLotNo() {
		return selectedLotNo;
	}
	public void setSelectedLotNo(String selectedLotNo) {
		this.selectedLotNo = selectedLotNo;
	}

	public ControlSelectBean getControlSelectBean() {
		if ( null == controlSelectBean ) {
			controlSelectBean = new ControlSelectBean();
		}
		return controlSelectBean;
	}
	public void setControlSelectBean( ControlSelectBean controlSelectBean ) {
		this.controlSelectBean = controlSelectBean;
	}

	public String getSelectedControlNo() {
		return selectedControlNo;
	}
	public void setSelectedControlNo( String selectedControlNo ) {
		this.selectedControlNo = selectedControlNo;
	}

	public ReleaseConfirmBean getReleaseConfirmBean() {
		if ( null == releaseConfirmBean ) {
			releaseConfirmBean = new ReleaseConfirmBean();
		}
		return releaseConfirmBean;
	}
	public void setReleaseConfirmBean( ReleaseConfirmBean releaseConfirmBean ) {
		this.releaseConfirmBean = releaseConfirmBean;
	}

	public String getRelNo() {
		return relNo;
	}
	public void setRelNo(String relNo) {
		this.relNo = relNo;
	}

	public String getDistNo() {
		return distNo;
	}
	public void setDistNo(String distNo) {
		this.distNo = distNo;
	}

	public String getDrmsMessage() {
		return drmsMessage;
	}
	public void setDrmsMessage(String drmsMessage) {
		this.drmsMessage = drmsMessage;
	}

	public void setRmi(boolean rmi) {
		this.rmi = rmi;
	}
	public boolean isRmi(){
		return rmi;
	}

}
