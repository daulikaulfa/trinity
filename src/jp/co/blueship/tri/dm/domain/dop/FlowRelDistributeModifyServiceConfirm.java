package jp.co.blueship.tri.dm.domain.dop;

import jp.co.blueship.tri.dm.constants.RelDistributeScreenID;
import jp.co.blueship.tri.dm.domain.dop.dto.FlowRelDistributeModifyServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.DmMessageId;


/**
 * リリース配布・リリース確認画面の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public class FlowRelDistributeModifyServiceConfirm implements IDomain<FlowRelDistributeModifyServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
/*	未使用
	private FlowRelDistributeEditSupport support = null;

	public void setSupport(FlowRelDistributeEditSupport support) {
		this.support = support;
	}
	*/
/* 未使用
	private ActionList statusMatrixAction = null;
	public void setStatusMatrixAction( ActionList action ) {
		this.statusMatrixAction = action;
	}*/

	@Override
	public IServiceDto<FlowRelDistributeModifyServiceBean> execute( IServiceDto<FlowRelDistributeModifyServiceBean> serviceDto ) {

		FlowRelDistributeModifyServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID	= paramBean.getReferer();
			String forwordID	= paramBean.getForward();

			if ( true != RelDistributeScreenID.MODIFY_CONFIRM.equals( refererID ) &&
					true != RelDistributeScreenID.MODIFY_CONFIRM.equals( forwordID ) ){
				return serviceDto;
			}

			if( RelDistributeScreenID.MODIFY_CONFIRM.equals( forwordID ) ) {

			}

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(DmMessageId.DM005021S, e);
		}
	}
}
