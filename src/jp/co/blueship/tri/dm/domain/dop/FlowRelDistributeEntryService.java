package jp.co.blueship.tri.dm.domain.dop;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.CopyOnWriteArrayList;

import jp.co.blueship.tri.agent.dm.opt.beans.dto.DealAssetFtpBean;
import jp.co.blueship.tri.agent.dm.opt.beans.dto.DealAssetServiceBean;
import jp.co.blueship.tri.agent.dm.opt.beans.dto.DealAssetServiceResponseBean;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.dm.DmDesignBusinessRuleUtils;
import jp.co.blueship.tri.dm.constants.RelDistributeCommercialShellPropertyDefineID;
import jp.co.blueship.tri.dm.constants.RelDistributeScreenID;
import jp.co.blueship.tri.dm.dao.constants.DmTables;
import jp.co.blueship.tri.dm.dao.dmdo.eb.DmDoEntity;
import jp.co.blueship.tri.dm.dao.dmdo.eb.IDmDoDto;
import jp.co.blueship.tri.dm.dao.dmdo.eb.IDmDoEntity;
import jp.co.blueship.tri.dm.domain.beans.CommercialShellID;
import jp.co.blueship.tri.dm.domain.beans.dop.dto.CommercialShellBean;
import jp.co.blueship.tri.dm.domain.dop.dto.FlowRelDistributeEntryServiceBean;
import jp.co.blueship.tri.dm.support.FlowRelDistributeEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.DesignSheetUtils;
import jp.co.blueship.tri.fw.cmn.utils.DesignUtils;
import jp.co.blueship.tri.fw.cmn.utils.RelCommonAddonUtil;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.DmDesignEntryKeyByCommercialShell;
import jp.co.blueship.tri.fw.constants.DmDesignEntryKeyByDealAssetFtp;
import jp.co.blueship.tri.fw.constants.DmDesignEntryKeyByRelFtp;
import jp.co.blueship.tri.fw.constants.status.RmRpStatusId;
import jp.co.blueship.tri.fw.constants.status.UmActStatusId;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.DmMessageId;
import jp.co.blueship.tri.fw.msg.MessageParameter;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.HistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.BaseInfoInputBean;

/**
 * リリースパッケージ・リリース状況画面の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public class FlowRelDistributeEntryService implements IDomain<FlowRelDistributeEntryServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowRelDistributeEditSupport support = null;
	public void setSupport(FlowRelDistributeEditSupport support) {
		this.support = support;
	}

	private List<IDomain<IGeneralServiceBean>> actions = new ArrayList<IDomain<IGeneralServiceBean>>();

	private List<IDomain<IGeneralServiceBean>> makeShellActions = new ArrayList<IDomain<IGeneralServiceBean>>();

	public void setActions(List<IDomain<IGeneralServiceBean>> actions) {
		this.actions = actions;
	}

	public void setMakeShellActions(List<IDomain<IGeneralServiceBean>> actions) {
		this.makeShellActions = actions;
	}

	@Override
	public IServiceDto<FlowRelDistributeEntryServiceBean> execute( IServiceDto<FlowRelDistributeEntryServiceBean> serviceDto ) {

		FlowRelDistributeEntryServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

//			String refererID = paramBean.getReferer();
			String forwordID = paramBean.getForward();

			if ( true != RelDistributeScreenID.COMP_ENTRY.equals( forwordID ) ) {
				return serviceDto;
			}

			if ( RelDistributeScreenID.COMP_ENTRY.equals( forwordID ) ) {

				if ( null != paramBean.getSelectedControlNo() ) {

					String controlNo = paramBean.getSelectedControlNo();
					String lotId = paramBean.getSelectedLotNo() ;

					// ftp転送の設定
					//String srcBasePath = DesignProjectUtil.getValue( DesignProjectDealAssetFtpDefineId.srcExtension ) ;
//					if( StringAddonUtil.isNothing( srcBasePath ) ) {
//						throw new SystemException( "カスタマイズ項目の不備：「転送元ベースパス」が設定されていません。" ) ;
//					}

					{
						int ftpCount = 0;
						ftpCount += StatusFlg.on.value().equals(sheet.getValue( DmDesignEntryKeyByDealAssetFtp.ftpServer))? 1: 0;
						ftpCount += StatusFlg.on.value().equals(sheet.getValue( DmDesignEntryKeyByRelFtp.ftpServer ))? 1: 0;

						if ( 1 != ftpCount ) {
							throw new BusinessException( DmMessageId.DM001016E );
						}
					}

					String destPath = null;

					if ( StatusFlg.on.value().equals( sheet.getValue( DmDesignEntryKeyByDealAssetFtp.ftpServer ) ) ) {
						destPath = sheet.getValue( DmDesignEntryKeyByDealAssetFtp.destBasePath ) ;
						if( TriStringUtils.isEmpty( destPath ) ) {
							throw new TriSystemException( DmMessageId.DM004000F ) ;
						}
					}

					if ( StatusFlg.on.value().equals( sheet.getValue( DmDesignEntryKeyByRelFtp.ftpServer ) ) ) {
						destPath = sheet.getValue( DmDesignEntryKeyByRelFtp.destLocalPath ) ;
						destPath = TriStringUtils.convertPath( destPath );
						if( TriStringUtils.isEmpty( destPath ) ) {
							throw new TriSystemException( DmMessageId.DM004001F ) ;
						}
					}

					ILotDto lotDto = this.support.getAmFinderSupport().findLotDto( lotId );
					IDmDoDto dmDoDto = this.support.getDmDoDto(controlNo);
					if( dmDoDto.getDmDoEntity()==null ) {
						// dmDoEntityの生成をする
						IDmDoEntity doEntity = new DmDoEntity();
						doEntity.setRpId(controlNo);
						dmDoDto.setDmDoEntity(doEntity);
					}

					File srcFile = DmDesignBusinessRuleUtils.getHistoryRelDSLFilePath(lotDto.getLotEntity(), dmDoDto.getRpEntity() ) ;
					String srcPath = TriStringUtils.convertPath( srcFile.getPath() );
					if( TriStringUtils.isEmpty( srcPath ) ) {
						throw new TriSystemException( DmMessageId.DM004002F ) ;
					}
					if( true != srcFile.exists() || true != srcFile.isFile()  ) {
						throw new BusinessException( DmMessageId.DM001013E , srcPath );
					}

					String linkNo = this.support.makeLinkNoLabel( controlNo ) ;

					// グループの存在チェック
					List<IGrpUserLnkEntity> groupUsers	= support.getUmFinderSupport().findGrpUserLnkByUserId( paramBean.getUserId() );
					RelCommonAddonUtil.checkAccessableGroup( lotDto, this.support.getUmFinderSupport().getGrpDao(), groupUsers );

					// 商用シェル出力
					String confName = paramBean.getReleaseConfirmBean().getConfViewBean().getConfName() ;
					if ( !paramBean.isRmi() ) {
						this.outputCommercialShell( paramBean, controlNo , confName , linkNo , destPath ) ;
					}

					List<Object> paramList = new CopyOnWriteArrayList<Object>() ;

					//Ftpによる資源転送設定
					DealAssetFtpBean ftpBean = new DealAssetFtpBean();
					ftpBean.setSrcPath( srcPath ) ;
					paramList.add(ftpBean);
					//資源登録サービス設定
					DealAssetServiceBean dealServiceBean = new DealAssetServiceBean();
					dealServiceBean.setTargetEnvironment(paramBean.getSelectedEnvNo());

					dealServiceBean.setDrmsManagementNo(linkNo);
					dealServiceBean.setPath( destPath );
					dealServiceBean.setName( srcFile.getName() );
					dealServiceBean.setFtpBean( ftpBean );
					paramList.add(dealServiceBean);

					IServiceDto<IGeneralServiceBean> innerServiceDto = new ServiceDto<IGeneralServiceBean>()
							.setServiceBean( paramBean )
							.setParamList( paramList );

					// RMI接続設定
					int retCode = 0;
					DealAssetServiceResponseBean resBean = null;
					if (paramBean.isRmi()) {
						// 配付資源登録
						for ( IDomain<IGeneralServiceBean> action : actions ) {
							action.execute( innerServiceDto );
						}
						// 戻り値を確認
						resBean = this.support.getDealAssetServiceResponseBean( paramList ) ;
						if ( null == resBean ) {
							throw new BusinessException( DmMessageId.DM001006E );
						} else  {
							retCode = resBean.getReturnCode();
							if ( 0 != retCode ) {
								String errMessage = this.support.makeRmiErrorMessage( resBean.getOutLog() , resBean.getErrLog() ) ;
								paramBean.setDrmsMessage( errMessage );
								if ( 1 == retCode ) {
									paramBean.setInfoMessage(DmMessageId.DM001007E);
								} else {
									paramBean.setInfoMessage(DmMessageId.DM001011E);
								}
							}
						}
					}
					// trinity登録
					if ( paramBean.isRmi() && 0 != retCode ) {
						;// RMI接続を行い、戻り値が正常でない場合は何もしない。
					} else {
						//※要チェックポイント
						//if ( null == relEntity ) {
						//	// 配布情報の登録
						//	insertDistEntity( paramBean, systemDate );
						//} else {
						//	// 配付情報のステータスを更新
						//	IRelPinpoint pinpoint =
						//		setRelEntityStatus( relEntity, paramBean.getUserName() );
						//
						//	this.support.getRelDao().update( relEntity, pinpoint );
						//}
						this.updateDmDoEntity( paramBean, dmDoDto.getDmDoEntity() ) ;
						paramBean.setDrmsMessage( DesignUtils.getMessageParameter(MessageParameter.REGISTERED_ASSET_DISTRIBUTION.getKey() ) );
					}
					// リリース番号を配付番号として通知
					paramBean.setRelNo( controlNo );
				}
			}

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( DmMessageId.DM005014S, e );
		}
	}


	private void outputCommercialShell(
			FlowRelDistributeEntryServiceBean paramBean,String controlNo , String confName , String linkNo , String destPath ) {
		CommercialShellBean shellBean = new CommercialShellBean();
		shellBean.setShellId(DmDesignEntryKeyByCommercialShell.shellAssetReg);
		String shellFileName = this.support.makeShellFileName( CommercialShellID.SHELL_ASSET_REG , controlNo ) ;
		shellBean.setShellFileName(shellFileName);

		List<Object> paramList = new CopyOnWriteArrayList<Object>() ;

		Properties properties = new Properties() ;
		properties.put( RelDistributeCommercialShellPropertyDefineID.GROUP_NAME, confName ) ;
//		properties.put( RelDistributeCommercialShellPropertyDefineID.RC_NO , paramBean.getSelectedControlNo() ) ;
		properties.put( RelDistributeCommercialShellPropertyDefineID.RC_NO , linkNo ) ;
		properties.put( RelDistributeCommercialShellPropertyDefineID.DIR_PATH , destPath ) ;
		shellBean.setProperties(properties);
		paramList.add(shellBean);

		IServiceDto<IGeneralServiceBean> innerServiceDto = new ServiceDto<IGeneralServiceBean>()
				.setServiceBean( paramBean )
				.setParamList( paramList );

		for ( IDomain<IGeneralServiceBean> action : makeShellActions ) {
			action.execute( innerServiceDto );
		}
	}


	private void updateDmDoEntity( FlowRelDistributeEntryServiceBean paramBean, IDmDoEntity dmDoEntity ) {

		dmDoEntity.setUpdUserNm		( paramBean.getUserName() );
		dmDoEntity.setUpdUserId		( paramBean.getUserId() );

		if( null == dmDoEntity.getStsId() ) {
			dmDoEntity.setStsId( RmRpStatusId.Unprocessed.getStatusId());
		}
		if( null == dmDoEntity.getProcStsId() ) {
			dmDoEntity.setProcStsId	( RmRpStatusId.Unprocessed.getStatusId());
		}
		dmDoEntity.setDelStsId		( StatusFlg.off );
		String linkNo = this.support.makeLinkNoLabel( dmDoEntity.getRpId() ) ;
		dmDoEntity.setMgtVer( linkNo );
		dmDoEntity.setContent( paramBean.getBaseInfoBean().getBaseInfoInputBean().getRelContent() ) ;
		dmDoEntity.setSummary( paramBean.getBaseInfoBean().getBaseInfoInputBean().getRelSummary() ) ;
		dmDoEntity.setProcStsId( RmRpStatusId.Unprocessed.getStatusId() ) ;
		dmDoEntity.setStsId( RmRpStatusId.Unprocessed.getStatusId() ) ;
		this.support.getDmDoDao().insert( dmDoEntity ) ;

		IUmFinderSupport umSupport = this.support.getUmFinderSupport();
		BaseInfoInputBean baseInfo = paramBean.getBaseInfoBean().getBaseInfoInputBean();

		umSupport.registerCtgLnk(baseInfo.getCtgId(), DmTables.DM_DO, linkNo);
		umSupport.registerMstoneLnk(baseInfo.getMstoneId(), DmTables.DM_DO, linkNo);

		if ( DesignSheetUtils.isRecord() ){

			IDmDoDto dmDoDto = support.getDmDoDto(dmDoEntity.getRpId());

			IHistEntity histEntity = new HistEntity();
			histEntity.setActSts( UmActStatusId.none.getStatusId() );

			support.getDmDoHistDao().insert( histEntity , dmDoDto);
		}
	}
}
