package jp.co.blueship.tri.dm.domain.dop;

import jp.co.blueship.tri.dm.DmItemCheckAddonUtils;
import jp.co.blueship.tri.dm.constants.RelDistributeScreenID;
import jp.co.blueship.tri.dm.domain.dop.dto.FlowRelDistributeEntryServiceBean;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.DmMessageId;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.BaseInfoBean;


/**
 * リリース配布・基本情報入力画面の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public class FlowRelDistributeEntryServiceInfoInput implements IDomain<FlowRelDistributeEntryServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	@Override
	public IServiceDto<FlowRelDistributeEntryServiceBean> execute( IServiceDto<FlowRelDistributeEntryServiceBean> serviceDto ) {

		FlowRelDistributeEntryServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID	= paramBean.getReferer();
			String forwordID	= paramBean.getForward();
			String screenType	= paramBean.getScreenType();

			if ( true != RelDistributeScreenID.INFO_INPUT.equals( refererID ) &&
					true != RelDistributeScreenID.INFO_INPUT.equals( forwordID ) ){
				return serviceDto;
			}


			BaseInfoBean baseInfoBean = paramBean.getBaseInfoBean();

			if( RelDistributeScreenID.INFO_INPUT.equals( refererID ) ) {

				if ( ScreenType.next.equals( screenType )) {
//				if ( ScreenType.next.equals( screenType ) &&
//						!forwordID.equals( RelDistributeScreenID.INFO_INPUT )) {

					DmItemCheckAddonUtils.checkRelDistributeBaseInfoInput( baseInfoBean );
				}

				paramBean.setBaseInfoBean( baseInfoBean );
			}


			if( RelDistributeScreenID.INFO_INPUT.equals( forwordID ) ) {
				if ( ! ScreenType.bussinessException.equals( screenType ) ) {

				}

			}

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(DmMessageId.DM005018S, e);
		}
	}
}
