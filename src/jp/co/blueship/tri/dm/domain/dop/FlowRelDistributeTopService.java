package jp.co.blueship.tri.dm.domain.dop;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.CopyOnWriteArrayList;

import jp.co.blueship.tri.agent.dm.opt.beans.dto.DealAssetServiceBean;
import jp.co.blueship.tri.agent.dm.opt.beans.dto.DealAssetServiceResponseBean;
import jp.co.blueship.tri.agent.dm.opt.beans.dto.DealAssetStatusBean;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotCondition;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvEntity;
import jp.co.blueship.tri.dm.DmFluentFunctionUtils;
import jp.co.blueship.tri.dm.constants.DrmsStatusId;
import jp.co.blueship.tri.dm.dao.dmdo.eb.DmDoCondition;
import jp.co.blueship.tri.dm.dao.dmdo.eb.IDmDoDto;
import jp.co.blueship.tri.dm.dao.dmdo.eb.IDmDoEntity;
import jp.co.blueship.tri.dm.domain.beans.CommercialShellID;
import jp.co.blueship.tri.dm.domain.beans.dop.dto.CommercialShellBean;
import jp.co.blueship.tri.dm.domain.dop.dto.FlowRelDistributeTopMenuServiceBean;
import jp.co.blueship.tri.dm.domain.dop.dto.FlowRelDistributeTopMenuServiceBean.ReleaseViewBean;
import jp.co.blueship.tri.dm.support.FlowRelDistributeEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.DmDesignEntryKeyByCommercialShell;
import jp.co.blueship.tri.fw.constants.status.DmDoStatusId;
import jp.co.blueship.tri.fw.dao.orm.DBSearchConditionAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.DBSearchSortAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.DmMessageId;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.rm.dao.rp.constants.RpItems;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.RpCondition;


/**
 * リリース配布・トップ画面の表示情報設定Class<br>
 *
 * @version V3L10.02
 * @author Takashi Ono
 *
 */
public class FlowRelDistributeTopService implements IDomain<FlowRelDistributeTopMenuServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowRelDistributeEditSupport support = null;
	public void setSupport( FlowRelDistributeEditSupport support ) {
		this.support = support;
	}

	private List<IDomain<IGeneralServiceBean>> actions = new ArrayList<IDomain<IGeneralServiceBean>>();

	private List<IDomain<IGeneralServiceBean>> makeShellActions = new ArrayList<IDomain<IGeneralServiceBean>>();

	public void setActions(List<IDomain<IGeneralServiceBean>> actions) {
		this.actions = actions;
	}

	public void setMakeShellActions(List<IDomain<IGeneralServiceBean>> actions) {
		this.makeShellActions = actions;
	}

	@Override
	public IServiceDto<FlowRelDistributeTopMenuServiceBean> execute( IServiceDto<FlowRelDistributeTopMenuServiceBean> serviceDto ) {

		FlowRelDistributeTopMenuServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String screenType = paramBean.getScreenType();

			if ( null == screenType || true != "list".equals( screenType ) ){
				return serviceDto;
			}

			IBldEnvEntity[] envAllEntities = this.getRelEnvEntity();
			if ( true == TriStringUtils.isEmpty( envAllEntities ) ) {
				throw new BusinessException( RmMessageId.RM001020E );
			}

			// 商用シェル出力
			this.outputCommercialShell( paramBean ) ;

			// 配付履歴確認実行
			List<DealAssetStatusBean> statusBeanList = null;
			DealAssetServiceResponseBean resBean = null;
			int returnCode = 0;

			if ( paramBean.isRmi() ) {
				//RMI接続設定
				DealAssetServiceBean dealBean = new DealAssetServiceBean();
				List<Object> paramList = new CopyOnWriteArrayList<Object>() ;
				paramList.add(dealBean);

				IServiceDto<IGeneralServiceBean> innerServiceDto = new ServiceDto<IGeneralServiceBean>()
						.setServiceBean( paramBean )
						.setParamList( paramList );

				// 下層のアクションを実行する
				for ( IDomain<IGeneralServiceBean> action : actions ) {
					action.execute( innerServiceDto );
				}

				resBean = this.support.getDealAssetServiceResponseBean( paramList ) ;
				if ( true != TriStringUtils.isEmpty( resBean ) ) {
					returnCode = resBean.getReturnCode();
					statusBeanList = resBean.getDealStatusList();
				} else {
					paramBean.setInfoMessage(DmMessageId.DM001006E);
				}
			}

			// 活動中の全ロットを取得
			List<ILotEntity> accessableLotList = null;
			{
				LotCondition lotCondition = (LotCondition)DBSearchConditionAddonUtil.getActiveLotCondition();
				List<String> disableLinkLotNumbers	= new ArrayList<String>();

				accessableLotList = this.support.getAmFinderSupport().getAccessableLotNumbers( lotCondition, null,paramBean, disableLinkLotNumbers, true );
			}
			List<ILotDto> lotDtoEntities = this.support.getAmFinderSupport().findLotDto(accessableLotList);

			IBldEnvEntity[] envEntities =
				this.support.getAmFinderSupport().getAccessableRelEnvEntity( envAllEntities, lotDtoEntities );

			List<ReleaseViewBean> releaseViewBeanList = new ArrayList<ReleaseViewBean>();

			for ( IBldEnvEntity envEntity : envEntities ) {

				ReleaseViewBean viewBean = paramBean.newReleaseViewBean();
				viewBean.setEnvName	( envEntity.getBldEnvNm() );
				viewBean.setEnvNo	( envEntity.getBldEnvId() );

				IRpEntity[] relEntityArray = this.getRpEntity( envEntity.getBldEnvId(), this.support.getAmFinderSupport().convertToLotNo( accessableLotList ) ) ;
				if ( true != TriStringUtils.isEmpty( relEntityArray ) ) {

					// trinity登録
					if ( paramBean.isRmi() && 0 != returnCode ) {
						// エラーメッセージ出力
						String errMessage = this.support.makeRmiErrorMessage( resBean.getOutLog() , resBean.getErrLog() ) ;
						paramBean.setDrmsMessage( errMessage );
						if ( 1 == returnCode ) {
							paramBean.setInfoMessage(DmMessageId.DM001007E);
						} else {
							paramBean.setInfoMessage(DmMessageId.DM001011E);
						}
					}
//					 最新のリリースを取得
					IDmDoDto recentRelEntity = this.support.getDmDoDto( relEntityArray[0].getRpId() );
					if( null!=recentRelEntity.getDmDoEntity() ) {
						viewBean.setRelNo		( recentRelEntity.getRpEntity().getRpId() );
//						viewBean.setInputDate(
//							DateAddonUtil.convertViewDateFormat( recentRelEntity.getInputDate() ));
						viewBean.setDrmsVer(  recentRelEntity.getRpEntity().getRpId() );
						viewBean.setLinkNo(  recentRelEntity.getDmDoEntity().getMgtVer() );
//						viewBean.setDistDate(
//							DateAddonUtil.convertViewDateFormat( recentRelEntity.getDealDate() ));
//						viewBean.setDistStatus		(
//							DesignDefineUtil.getValue(
//								DesignRelDefineId.statusId, recentRelEntity.getStatusId() ));
						String lotId = recentRelEntity.getRpEntity().getLotId();
						ILotEntity lotEntity = this.support.getAmFinderSupport().findLotEntity( lotId );
						viewBean.setLotNo( lotId ) ;
						viewBean.setLotName( lotEntity.getLotNm());
						if (null != statusBeanList) {
							viewBean = this.setLatest(statusBeanList, viewBean);
						}
					}

				}

				releaseViewBeanList.add( viewBean );

			}

			paramBean.setReleaseViewBeanList( releaseViewBeanList );

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( DmMessageId.DM005024S, e );
		}
	}

	/**
	 * 最新の適用リリースを取得する
	 * @param statusBeanList 配付履歴情報
	 * @return 資源情報
	 */
	private ReleaseViewBean  setLatest(List<DealAssetStatusBean> statusBeanList, ReleaseViewBean viewBean) {

		String latestDate = FlowRelDistributeEditSupport.TIMER_ALL_ZERO ;
		for( DealAssetStatusBean statusBean : statusBeanList) {
			// リンク番号が一致
//			if (viewBean.getRelNo().equals(statusBean.getDrmsManagementNo())) {
			if (null != viewBean.getLinkNo()) {
				if (viewBean.getLinkNo().equals(statusBean.getDrmsManagementNo())) {
					String date = statusBean.getDealDate();
					// 適用済み
					if (null != date) {
						// 適用日が新しい
						if ( 0 > latestDate.compareTo(date) ) {
							latestDate = date;
							String inputDate = this.support.convertViewDateFormat(
									statusBean.getRegistrationDate(), TriDateUtils.getViewDateFormat() );
							viewBean.setDrmsInputDate(inputDate);
							String applyDate = this.support.convertViewDateFormat(
									statusBean.getDealDate(),  TriDateUtils.getViewDateFormat() );
							viewBean.setApplyDate(applyDate);
							viewBean.setDrmsStatus( DrmsStatusId.APPLY.getValue() );
						}
					}
				}
			}
		}

		return viewBean;

	}

	/**
	 * リリース環境情報を取得する。
	 * @return 全環境情報
	 */
	private IBldEnvEntity[] getRelEnvEntity() {

		IJdbcCondition condition	= DBSearchConditionAddonUtil.getActivelRelEnvCondition();
		ISqlSort sort			= DBSearchSortAddonUtil.getRelEnvSortFromDesignDefineByRelTop();

		IEntityLimit<IBldEnvEntity> limit =
			this.support.getBmFinderSupport().getBldEnvDao().find( condition.getCondition(), sort, 1, 0 );

		return limit.getEntities().toArray(new IBldEnvEntity[0]);
	}
	/**
	 *
	 * @param envNo 対象となるリリース環境
	 * @param lotNoArrays アクセス可能なロット番号の配列
	 * @return
	 */
	private IRpEntity[] getRpEntity( String envNo, String[] lotNoArrays ) {
		String status [] = new String[] { DmDoStatusId.JobRegistered.getStatusId() ,
				DmDoStatusId.TimerConfigured.getStatusId(),
				DmDoStatusId.JobCancelled.getStatusId() } ;

		// 対象のdoレコードを検索
		DmDoCondition dmCondition = new DmDoCondition();
		dmCondition.setProcStsIds(status);
		List<IDmDoEntity> doEntities = this.support.getDmDoDao().find(dmCondition.getCondition());
		String[] rpIds = FluentList.from(doEntities).map( DmFluentFunctionUtils.toRpIdFromDmDo ).toArray(new String[0]);

		RpCondition condition = new RpCondition();
		condition.setBldEnvId( envNo );
		if (TriStringUtils.isEmpty(rpIds)){
			condition.setRpIds( "" );
		}else{
			condition.setRpIds( rpIds );
		}
		//アクセス可能なロット番号
		{
			if ( TriStringUtils.isEmpty( lotNoArrays ) ) {
				//ダミーを設定して、全件検索を防止する。
				condition.setLotIds( new String[]{ "" });
			} else {

				condition.setLotIds( lotNoArrays );
			}
		}

		//リリース設定日の降順
		ISqlSort sort = new SortBuilder();
		sort.setElement(RpItems.rpId, TriSortOrder.Desc, 1);

		IEntityLimit<IRpEntity> limit =
			this.support.getRpDao().find( condition.getCondition(), sort, 1, 0 );

		return limit.getEntities().toArray(new IRpEntity[0]) ;
	}
	/**
	 * 商用シェルの出力
	 *
	 */
	private void outputCommercialShell( FlowRelDistributeTopMenuServiceBean paramBean ) {
		{
			List<Object> paramList = new CopyOnWriteArrayList<Object>() ;

			CommercialShellBean shellBean = new CommercialShellBean();
			shellBean.setShellId(DmDesignEntryKeyByCommercialShell.shellAssetDealStatusChk);
			String shellFileName = this.support.makeShellFileName( CommercialShellID.SHELL_ASSET_DEAL_STATUS_CHK , "" /*envEntity.getEnvNo()*/ ) ;
			shellBean.setShellFileName(shellFileName);
			Properties properties = new Properties() ;
			shellBean.setProperties(properties);
			paramList.add(shellBean);

			IServiceDto<IGeneralServiceBean> innerServiceDto = new ServiceDto<IGeneralServiceBean>()
					.setServiceBean( paramBean )
					.setParamList( paramList );

			for ( IDomain<IGeneralServiceBean> action : makeShellActions ) {
				action.execute( innerServiceDto );
			}
		}
	}
}
