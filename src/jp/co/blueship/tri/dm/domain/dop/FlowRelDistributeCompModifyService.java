package jp.co.blueship.tri.dm.domain.dop;

import java.util.List;

import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.dm.constants.RelDistributeScreenID;
import jp.co.blueship.tri.dm.dao.dmdo.eb.IDmDoDto;
import jp.co.blueship.tri.dm.domain.dop.dto.FlowRelDistributeModifyServiceBean;
import jp.co.blueship.tri.dm.support.FlowRelDistributeEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.DesignSheetUtils;
import jp.co.blueship.tri.fw.cmn.utils.ItemCheckAddonUtils;
import jp.co.blueship.tri.fw.cmn.utils.RelCommonAddonUtil;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.status.UmActStatusId;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.DmMessageId;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.HistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.BaseInfoInputBean;

/**
 * リリース配付・編集完了画面の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public class FlowRelDistributeCompModifyService implements IDomain<FlowRelDistributeModifyServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowRelDistributeEditSupport support = null;
	public void setSupport(FlowRelDistributeEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowRelDistributeModifyServiceBean> execute( IServiceDto<FlowRelDistributeModifyServiceBean> serviceDto ) {

		FlowRelDistributeModifyServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID = paramBean.getReferer();
			String forwordID = paramBean.getForward();

			if ( true != RelDistributeScreenID.COMP_MODIFY.equals( refererID ) &&
					true != RelDistributeScreenID.COMP_MODIFY.equals( forwordID ) ) {
				return serviceDto;
			}

			if ( RelDistributeScreenID.COMP_MODIFY.equals( refererID ) ) {

			}

			if ( RelDistributeScreenID.COMP_MODIFY.equals( forwordID ) ) {

				if ( ! ScreenType.bussinessException.equals( paramBean.getScreenType() ) ) {

					String distNo = paramBean.getDistNo();
					ItemCheckAddonUtils.checkRelNo( distNo );

					BaseInfoInputBean inputBean = paramBean.getBaseInfoBean().getBaseInfoInputBean();

					IDmDoDto dmDoDto = this.support.getDmDoDto(distNo);
					// グループの存在チェック
					ILotDto dto = this.support.getAmFinderSupport().findLotDto( dmDoDto.getRpEntity().getLotId() );
					List<IGrpUserLnkEntity> groupUsers	= support.getUmFinderSupport().findGrpUserLnkByUserId( paramBean.getUserId() );
					RelCommonAddonUtil.checkAccessableGroup( dto, this.support.getUmFinderSupport().getGrpDao(), groupUsers );

					dmDoDto.getDmDoEntity().setSummary	( inputBean.getRelSummary() );
					dmDoDto.getDmDoEntity().setContent	( inputBean.getRelContent() );
					this.support.getDmDoDao().update( dmDoDto.getDmDoEntity() );

					dmDoDto.getRpEntity().setUpdUserNm	( paramBean.getUserName() );
					dmDoDto.getRpEntity().setUpdUserId	( paramBean.getUserId() );
					this.support.getRpDao().update( dmDoDto.getRpEntity() );

					if ( DesignSheetUtils.isRecord() ){

						IHistEntity histEntity = new HistEntity();
						histEntity.setActSts( UmActStatusId.none.getStatusId() );

						support.getDmDoHistDao().insert( histEntity , dmDoDto);
					}

				}
			}

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(DmMessageId.DM005012S , e);
		}
	}
}
