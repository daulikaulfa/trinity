package jp.co.blueship.tri.dm.domain.dop;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.CopyOnWriteArrayList;

import jp.co.blueship.tri.agent.dm.opt.beans.dto.DealAssetServiceBean;
import jp.co.blueship.tri.agent.dm.opt.beans.dto.DealAssetServiceResponseBean;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.dm.constants.RelDistributeCommercialShellPropertyDefineID;
import jp.co.blueship.tri.dm.constants.RelDistributeScreenID;
import jp.co.blueship.tri.dm.constants.TimerSettings;
import jp.co.blueship.tri.dm.dao.dmdo.eb.IDmDoDto;
import jp.co.blueship.tri.dm.domain.beans.CommercialShellID;
import jp.co.blueship.tri.dm.domain.beans.dop.dto.CommercialShellBean;
import jp.co.blueship.tri.dm.domain.dop.dto.FlowRelDistributeTimerServiceBean;
import jp.co.blueship.tri.dm.domain.dop.dto.FlowRelDistributeTimerServiceBean.TimerSetViewBean;
import jp.co.blueship.tri.dm.support.FlowRelDistributeEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.DesignSheetUtils;
import jp.co.blueship.tri.fw.cmn.utils.DesignUtils;
import jp.co.blueship.tri.fw.cmn.utils.ItemCheckAddonUtils;
import jp.co.blueship.tri.fw.cmn.utils.RelCommonAddonUtil;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.DmDesignEntryKeyByCommercialShell;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.status.DmDoStatusId;
import jp.co.blueship.tri.fw.constants.status.UmActStatusId;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.DmMessageId;
import jp.co.blueship.tri.fw.msg.MessageParameter;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.HistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;


/**
 * 配布資源・タイマー設定完了画面の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 *
 * @version V4.03.00
 * @author Cuong Nguyen
 */
public class FlowRelDistributeCompTimerService implements IDomain<FlowRelDistributeTimerServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowRelDistributeEditSupport support = null;
	public void setSupport(FlowRelDistributeEditSupport support) {
		this.support = support;
	}

	private List<IDomain<IGeneralServiceBean>> settingActions = new ArrayList<IDomain<IGeneralServiceBean>>();
	private List<IDomain<IGeneralServiceBean>> cancelActions = new ArrayList<IDomain<IGeneralServiceBean>>();
	private List<IDomain<IGeneralServiceBean>> makeShellActions = new ArrayList<IDomain<IGeneralServiceBean>>();

	public void setSettingActions(List<IDomain<IGeneralServiceBean>> actions) {
		this.settingActions = actions;
	}

	public void setCancelActions(List<IDomain<IGeneralServiceBean>> actions) {
		this.cancelActions = actions;
	}

	public void setMakeShellActions(List<IDomain<IGeneralServiceBean>> actions) {
		this.makeShellActions = actions;
	}

	@Override
	public IServiceDto<FlowRelDistributeTimerServiceBean> execute( IServiceDto<FlowRelDistributeTimerServiceBean> serviceDto ) {

		FlowRelDistributeTimerServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID = paramBean.getReferer();
			String forwordID = paramBean.getForward();

			if ( true != RelDistributeScreenID.COMP_TIMER.equals( refererID ) &&
					true != RelDistributeScreenID.COMP_TIMER.equals( forwordID ) ) {
				return serviceDto;
			}

			if ( RelDistributeScreenID.COMP_TIMER.equals( forwordID ) ) {
				if ( ! ScreenType.bussinessException.equals( paramBean.getScreenType() ) ) {

					String distNo = paramBean.getDistNo();
					String linkNo = this.support.makeLinkNoLabel( distNo ) ;

					ItemCheckAddonUtils.checkRelNo( distNo );

					List<Object> paramList = new CopyOnWriteArrayList<Object>() ;

					//trinity情報の取得
					IDmDoDto dmDoDto = this.support.getDmDoDto( distNo );
					// グループの存在チェック
					ILotDto dto = this.support.getAmFinderSupport().findLotDto( dmDoDto.getRpEntity().getLotId() );

					List<IGrpUserLnkEntity> groupUsers	= support.getUmFinderSupport().findGrpUserLnkByUserId( paramBean.getUserId() );
					RelCommonAddonUtil.checkAccessableGroup( dto, this.support.getUmFinderSupport().getGrpDao(), groupUsers );

					String timerSettingDate = dmDoDto.getDmDoEntity().getTimerSettingDate() ;
					String timerSettingTime = dmDoDto.getDmDoEntity().getTimerSettingTime() ;
					boolean isSet = ( null != timerSettingDate && null != timerSettingTime ) ? true : false ;

					String timerDate = convertTimerDate(paramBean);

					String timerSetting = paramBean.getTimerSetting();

					//商用シェル出力
					if ( !paramBean.isRmi() ) {
						this.outputCommercialShell( paramBean , timerSetting, timerDate, distNo , linkNo , isSet ) ;
					}
					DealAssetServiceBean dealServiceBean = new DealAssetServiceBean();
					dealServiceBean.setTargetEnvironment(paramBean.getConfViewBean().getConfName());
//					serviceBean.setDrmsManagementNo(distNo);
					dealServiceBean.setDrmsManagementNo(linkNo);

					String status = DmDoStatusId.TimerConfigured.getStatusId();//初期値

					if ( null != timerSetting ) {
						if ( TimerSettings.Cancel.legacy().equals( timerSetting ) ) {
							status = DmDoStatusId.JobRegistered.getStatusId();
						}
						if ( TimerSettings.TimerSettins.legacy().equals( timerSetting ) ||
								TimerSettings.Immediately.legacy().equals( timerSetting ) ) {
							status = DmDoStatusId.TimerConfigured.getStatusId();
						}

						if ( false == TimerSettings.Cancel.legacy().equals( timerSetting ) ) {
							if ( TimerSettings.Immediately.legacy().equals( timerSetting ) ) {
								dealServiceBean.setTimerDate( FlowRelDistributeEditSupport.TIMER_ALL_ZERO ) ;
							} else {
								dealServiceBean.setTimerDate( timerDate );
							}
						}
						dealServiceBean.setNewOrUpdateFlg( ( isSet ) ? StatusFlg.off.value() : StatusFlg.on.value() );

						paramList.add(dealServiceBean);

						IServiceDto<IGeneralServiceBean> innerServiceDto = new ServiceDto<IGeneralServiceBean>()
								.setServiceBean( paramBean )
								.setParamList( paramList );

						//タイマー設定
						if (paramBean.isRmi()) {
							if ( TimerSettings.Cancel.legacy().equals( timerSetting ) ) {
								for ( IDomain<IGeneralServiceBean> action : cancelActions ) {
									action.execute( innerServiceDto );
								}
							}
							if ( TimerSettings.TimerSettins.legacy().equals( timerSetting ) ||
									TimerSettings.Immediately.legacy().equals( timerSetting ) ) {
								for ( IDomain<IGeneralServiceBean> action : settingActions ) {
									action.execute( innerServiceDto );
								}
							}
						}
					}
					// 戻り値を確認
					DealAssetServiceResponseBean resBean = null;
					resBean = this.support.getDealAssetServiceResponseBean( paramList ) ;
					if (paramBean.isRmi()) {
						if ( null == resBean ) {
							throw new BusinessException( DmMessageId.DM001006E );
						} else  {
							if ( 0 == resBean.getReturnCode() ) {

								// タイマー情報の更新
								this.setRelEntityTimer( dmDoDto, paramBean, status, paramBean.getUserName() , paramBean.getUserId() );
								this.support.getRpDao().update( dmDoDto.getRpEntity() );
								this.support.getDmDoDao().update(dmDoDto.getDmDoEntity() );
								paramBean.setDrmsMessage( DesignUtils.getMessageParameter( MessageParameter.SETTING_TIMER_ASSET_DISTRIBUTION.getKey() ) );

								if ( DesignSheetUtils.isRecord() ){
									IHistEntity histEntity = new HistEntity();
									histEntity.setActSts( UmActStatusId.Edit.getStatusId() );

									support.getDmDoHistDao().insert( histEntity , dmDoDto);
								}

							} else {
								String errMessage = this.support.makeRmiErrorMessage( resBean.getOutLog() , resBean.getErrLog() ) ;
								paramBean.setDrmsMessage( errMessage );
								if (resBean.getReturnCode() == 1) {
									paramBean.setInfoMessage(DmMessageId.DM001007E);
								} else {
									paramBean.setInfoMessage(DmMessageId.DM001011E);
								}
							}
						}
					} else {
						// タイマー情報の更新
						this.setRelEntityTimer( dmDoDto, paramBean, status, paramBean.getUserName() , paramBean.getUserId() );
						this.support.getRpDao().update( dmDoDto.getRpEntity() );
						this.support.getDmDoDao().update(dmDoDto.getDmDoEntity() );
						paramBean.setDrmsMessage( DesignUtils.getMessageParameter( MessageParameter.SETTING_TIMER_ASSET_DISTRIBUTION.getKey() ) );

						if ( DesignSheetUtils.isRecord() ){
							IHistEntity histEntity = new HistEntity();
							histEntity.setActSts( UmActStatusId.Edit.getStatusId() );

							support.getDmDoHistDao().insert( histEntity , dmDoDto);
						}
					}
					paramBean.setDistNo( distNo );
				}
			}

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(DmMessageId.DM005013S , e);
		}
	}

	/**
	 * タイマー設定情報を更新する。
	 * @param entity 配付エンティティ
	 * @param inputBean 基本情報
	 * @param userName 登録／更新ユーザ名
	 * @return
	 */
	private final void setRelEntityTimer(
			IDmDoDto dto , FlowRelDistributeTimerServiceBean paramBean, String status , String userName , String userId ) {

		String timerSetting = paramBean.getTimerSetting();
		String timerDate;
		String timerSummary = paramBean.getTimerSummary();
		String timerContent = paramBean.getTimerContent();
		if ( TimerSettings.Immediately.legacy().equals( timerSetting ) ) {
			timerDate = TriDateUtils.getSystemDate();
		} else {
			if ( TimerSettings.Cancel.legacy().equals( timerSetting ) ) {
				timerDate = null;
				timerSummary = "";
				timerContent = "";
			} else {
				timerDate = paramBean.getTimerDate() ;
			}
		}
		//timerDate=DEFAULT_DATE_PATTERN をYYYY/MM/DD と HH:MM に分割して格納。
		String timerDateTimeDate = ( null != timerDate ) ? timerDate.substring( 0 , 10 ) : "" ;
		String timerDateTimeTime = ( null != timerDate ) ? timerDate.substring( 11 , 16 ) : "" ;

		dto.getDmDoEntity().setTimerSettingDate		( timerDateTimeDate );
		dto.getDmDoEntity().setTimerSettingTime		( timerDateTimeTime );
		dto.getDmDoEntity().setTimerSettingSummary	( timerSummary );
		dto.getDmDoEntity().setTimerSettingContent	( timerContent );
		dto.getDmDoEntity().setStsId				( status );
		dto.getRpEntity().setUpdUserNm				( userName );
		dto.getRpEntity().setUpdUserId				( userId );
		dto.getRpEntity().setDelStsId				( StatusFlg.off );
	}

	/**
	 * タイマー設定日時をCentricManager向けに変更する。
	 * @param  FlowRelDistributeTimerServiceBean paramBean
	 * @return
	 */
	private final String convertTimerDate(FlowRelDistributeTimerServiceBean paramBean) {

		String timerSetting = paramBean.getTimerSetting();
		if ( TimerSettings.Immediately.legacy().equals( timerSetting ) ) {
			return FlowRelDistributeEditSupport.TIMER_ALL_ZERO ;
		}

		TimerSetViewBean timerBean = paramBean.getTimerSetViewBean();
		StringBuilder stb = new StringBuilder();
		stb.append(timerBean.getYear());
		stb.append(timerBean.getMonth());
		stb.append(timerBean.getDay());

		if ( true == TriStringUtils.isEmpty( timerBean.getHour() ) ||
				2 != timerBean.getHour().length() ) {
			stb.append( FlowRelDistributeEditSupport.TIMER_DOUBLE_ZERO ) ;
		} else {
			stb.append(timerBean.getHour());
		}

		if ( true == TriStringUtils.isEmpty( timerBean.getMinuteHigh() ) ||
				1 != timerBean.getMinuteHigh().length() ) {
			stb.append( FlowRelDistributeEditSupport.TIMER_ZERO ) ;
		} else {
			stb.append(timerBean.getMinuteHigh());
		}
		if ( true == TriStringUtils.isEmpty( timerBean.getMinuteLow() ) ||
				1 != timerBean.getMinuteLow().length() ) {
			stb.append( FlowRelDistributeEditSupport.TIMER_ZERO ) ;
		} else {
			stb.append(timerBean.getMinuteLow());
		}

		stb.append( FlowRelDistributeEditSupport.TIMER_DOUBLE_ZERO );//秒

		String ret = stb.toString();
		return ret;
	}

	/**
	 *
	 * @param paramBean
	 * @param timerSetting
	 * @param timerDate
	 * @param distNo
	 * @param linkNo
	 * @param isSet
	 */
	private void outputCommercialShell( FlowRelDistributeTimerServiceBean paramBean ,
											String timerSetting ,
											String timerDate ,
											String distNo ,
											String linkNo ,
											boolean isSet ) {

		if ( null != timerSetting ) {

			List<Object> paramList = new CopyOnWriteArrayList<Object>() ;

			CommercialShellBean shellBean = new CommercialShellBean();
			shellBean.setShellId(DmDesignEntryKeyByCommercialShell.shellAssetDealTimerSet);

			String shellFileName = null ;

			if ( TimerSettings.Cancel.legacy().equals( timerSetting ) ) {
				shellBean.setShellId(DmDesignEntryKeyByCommercialShell.shellAssetDealTimerCancel);
				shellFileName = this.support.makeShellFileName( CommercialShellID.SHELL_ASSET_DEAL_TIMER_CANCEL , distNo ) ;
			} else {
				shellFileName = this.support.makeShellFileName( CommercialShellID.SHELL_ASSET_DEAL_TIMER_SET , distNo ) ;
			}
			shellBean.setShellFileName(shellFileName);

			Properties properties = new Properties() ;
			properties.put( RelDistributeCommercialShellPropertyDefineID.GROUP_NAME , paramBean.getConfViewBean().getConfName() ) ;
//						properties.put( RelDistributeCommercialShellPropertyDefineID.RC_NO , distNo ) ;
			properties.put( RelDistributeCommercialShellPropertyDefineID.RC_NO , linkNo ) ;
			properties.put( RelDistributeCommercialShellPropertyDefineID.TIMER , timerDate ) ;

			if ( isSet ) {
				properties.put( RelDistributeCommercialShellPropertyDefineID.SET_FLG , StatusFlg.off.value() );
			} else {
				properties.put( RelDistributeCommercialShellPropertyDefineID.SET_FLG , StatusFlg.on.value() );
			}
			shellBean.setProperties(properties);
			paramList.add(shellBean);

			IServiceDto<IGeneralServiceBean> innerServiceDto = new ServiceDto<IGeneralServiceBean>()
					.setServiceBean( paramBean )
					.setParamList( paramList );

			// 下層のアクションを実行する
			for ( IDomain<IGeneralServiceBean> action : makeShellActions ) {
				action.execute( innerServiceDto );
			}
		}
	}
}
