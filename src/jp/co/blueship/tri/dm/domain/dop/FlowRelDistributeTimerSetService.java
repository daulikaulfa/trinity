package jp.co.blueship.tri.dm.domain.dop;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvEntity;
import jp.co.blueship.tri.dm.constants.RelDistributeScreenID;
import jp.co.blueship.tri.dm.dao.dmdo.eb.DmDoCondition;
import jp.co.blueship.tri.dm.dao.dmdo.eb.IDmDoEntity;
import jp.co.blueship.tri.dm.domain.beans.dop.dto.DistributeViewBean;
import jp.co.blueship.tri.dm.domain.dop.dto.FlowRelDistributeTimerServiceBean;
import jp.co.blueship.tri.dm.support.FlowRelDistributeEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.ItemCheckAddonUtils;
import jp.co.blueship.tri.fw.cmn.utils.RelCommonAddonUtil;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RmDesignBeanId;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.status.DmDoStatusId;
import jp.co.blueship.tri.fw.dao.orm.DBSearchConditionAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.DBSearchSortAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.DmMessageId;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.rm.RmViewInfoAddonUtils;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.ConfigurationSelectBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.ConfigurationViewBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.LotViewBean;


/**
 * 統合監視・タイマー設定画面の表示情報設定Class<br>
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowRelDistributeTimerSetService implements IDomain<FlowRelDistributeTimerServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowRelDistributeEditSupport support;
	public void setSupport(FlowRelDistributeEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowRelDistributeTimerServiceBean> execute( IServiceDto<FlowRelDistributeTimerServiceBean> serviceDto ) {

		FlowRelDistributeTimerServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID = paramBean.getReferer();
			String forwordID = paramBean.getForward();
			String screenType = paramBean.getScreenType();

			if ( true != RelDistributeScreenID.TIMER_SET.equals( forwordID ) ) {
				return serviceDto;
			}
			if ( null != screenType && ScreenType.bussinessException.equals( screenType ) ) {
				return serviceDto;
			}

			// 選択コンボ用ロットリスト
			int selectedPageNo	= 1;
			int maxPageNumber	= 0;

			List<ILotEntity> accessableLotList = this.getPjtLotEntity( paramBean, selectedPageNo, maxPageNumber ) ;
			if ( true == TriStringUtils.isEmpty( accessableLotList ) ) {
				paramBean.setInfoMessage(DmMessageId.DM001017E);
			}
			List<ILotDto> lotDtoEntities = this.support.getAmFinderSupport().findLotDto( accessableLotList );

			//アクセス許可
			List<IGrpUserLnkEntity> groupUsers	= support.getUmFinderSupport().findGrpUserLnkByUserId( paramBean.getUserId() );
			RelCommonAddonUtil.checkAccessableGroup( lotDtoEntities, this.support.getUmFinderSupport().getGrpDao(), groupUsers );

			//LotViewBeanリストの作成
			List<LotViewBean> lotViewBeanList = this.makeLotViewBeanList( accessableLotList ) ;
			paramBean.setLotViewBeanList	( lotViewBeanList );

			// 選択コンボ用環境リスト
			IBldEnvEntity[] envAllEntities = this.getRelEnvEntity( selectedPageNo, maxPageNumber ) ;
			if ( true == TriStringUtils.isEmpty( envAllEntities ) ) {
				throw new BusinessException( RmMessageId.RM001020E );
			}

			IBldEnvEntity[] envEntities =
				this.support.getAmFinderSupport().getAccessableRelEnvEntity( envAllEntities, lotDtoEntities );
			if ( true == TriStringUtils.isEmpty( envAllEntities ) ) {
				throw new BusinessException( DmMessageId.DM001018E );
			}

			//ConfigurationViewBeanリストの作成
			List<ConfigurationViewBean> confViewBeanList = this.support.makeConfigurationViewBeanList( envEntities ) ;
			ConfigurationSelectBean confSelectBean = paramBean.getConfSelectBean();
			confSelectBean.setConfViewBeanList	( confViewBeanList );
			paramBean.setConfViewBeanList(confViewBeanList);

			// 配布一覧（リリース一覧）
			String selectedEnvNo = paramBean.getSelectedEnvNo();
			String selectedLotNo = paramBean.getSelectedLotNo();
			String timer[] = paramBean.getTimer();
			String conditionTimer[] = paramBean.getTimer();
			if ( null != refererID &&
				true != RelDistributeScreenID.TIMER_SET.equals( refererID ) ) {
				conditionTimer = new String[] { DmDoStatusId.JobRegistered.getStatusId() } ;
				timer = new String[] { DmDoStatusId.JobRegistered.getStatusId() } ;
			} else {
				if ( null == timer ) {//ステータスが指定されない場合、配布登録済みのすべてのレコードを対象とする
					conditionTimer = new String[] { DmDoStatusId.JobRegistered.getStatusId() ,
											DmDoStatusId.TimerConfigured.getStatusId() } ;
				}
				// 環境選択チェック
				ItemCheckAddonUtils.checkConfNo( selectedEnvNo );

				paramBean.setSelectedEnvNo(selectedEnvNo);
				paramBean.setSelectedLotNo(selectedLotNo);
			}

			if ( RelDistributeScreenID.TOP.equals( refererID ) ){
				//リリース情報の取得
				IRpEntity[] relEntityArray = this.getRpEntity(
						selectedEnvNo ,
						selectedLotNo ,
						new String[] { DmDoStatusId.JobRegistered.getStatusId() , DmDoStatusId.TimerConfigured.getStatusId() } ,
						selectedPageNo ,
						maxPageNumber ) ;

				if ( 0 == relEntityArray.length ) {
					List<DistributeViewBean> distributeViewBeanList = new ArrayList<DistributeViewBean>();
					paramBean.setDistributeViewBeanList( distributeViewBeanList );
					throw new BusinessException( DmMessageId.DM001012E );
				}

				//DistributeViewBeanリストの作成
				Map<String,ILotEntity> pjtLotNoEntityMap = this.makePjtLotMap(accessableLotList ) ;
				Map<String,IBldEnvEntity> relEnvNoEntityMap = this.makeRelEnvMap( envEntities ) ;
				List<DistributeViewBean> distributeViewBeanList = this.makeDistributeViewBeanList( relEntityArray , pjtLotNoEntityMap, relEnvNoEntityMap ) ;
				paramBean.setDistributeViewBeanList( distributeViewBeanList );
				if ( (null == distributeViewBeanList) ||
						distributeViewBeanList.isEmpty() ) {
					throw new BusinessException( DmMessageId.DM001012E );
				}
			}

			//リリース情報の取得
			IRpEntity[] relEntityArray = this.getRpEntity( selectedEnvNo , selectedLotNo , conditionTimer , selectedPageNo , maxPageNumber ) ;
			if ( ! RelDistributeScreenID.TOP.equals( refererID ) ){
				if ( 0 == relEntityArray.length ) {
					List<DistributeViewBean> distributeViewBeanList = new ArrayList<DistributeViewBean>();
					paramBean.setDistributeViewBeanList( distributeViewBeanList );
					throw new BusinessException( DmMessageId.DM001012E );
				}
			}

			//DistributeViewBeanリストの作成
			Map<String,ILotEntity> pjtLotNoEntityMap = this.makePjtLotMap(accessableLotList ) ;
			Map<String,IBldEnvEntity> relEnvNoEntityMap = this.makeRelEnvMap( envEntities ) ;
			List<DistributeViewBean> distributeViewBeanList = this.makeDistributeViewBeanList( relEntityArray , pjtLotNoEntityMap, relEnvNoEntityMap ) ;
			paramBean.setDistributeViewBeanList( distributeViewBeanList );

			if ( ! RelDistributeScreenID.TOP.equals( refererID ) ){
				if ( (null == distributeViewBeanList) ||
						distributeViewBeanList.isEmpty() ) {
					throw new BusinessException( DmMessageId.DM001012E );
				}
			}


			if ( paramBean.getTimerDate() == null ||
					paramBean.getTimerDate().length() < TriDateUtils.getDefaultDatePattern().length() ) {
				paramBean.setTimerDate( "                        " ) ;
			}
			paramBean.setTimer(timer);

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( DmMessageId.DM005023S , e );
		}
	}
	/**
	 *
	 * @param envNo
	 * @param lotNo
	 * @param timer
	 * @param selectedPageNo
	 * @param maxPageNumber
	 * @return
	 */
	private IRpEntity[] getRpEntity( String envNo , String lotId , String[] timer , int selectedPageNo , int maxPageNumber ) {

		String[] rpIds = getRpIdsFromDmDoTimer( timer );
		if ( TriStringUtils.isEmpty(rpIds) ) {
			return new IRpEntity[0] ;
		}
		IJdbcCondition relCondition	= DBSearchConditionAddonUtil.getRelDistributeConditionByEnvNoLotNo( envNo, lotId, rpIds );
		ISqlSort relSort			=  DBSearchSortAddonUtil.getRelSortFromDesignDefineByRelDistributeTimerList();
		IEntityLimit<IRpEntity> relEntityLimit =
			this.support.getRpDao().find( relCondition.getCondition(), relSort, selectedPageNo, maxPageNumber );

		return relEntityLimit.getEntities().toArray(new IRpEntity[0]);
	}

	/**
	 * ステータス条件timerに当てはまるDmDoEntityからrpIdsを返します。
	 * @param timer
	 * @return rpIds
	 */
	private String[] getRpIdsFromDmDoTimer(String[] timer ) {

		DmDoCondition dmCondition = new DmDoCondition();
		dmCondition.setProcStsIds(timer);
		List<IDmDoEntity> dmDoEntityList = this.support.getDmDoDao().find(dmCondition.getCondition());
		List<String> rpIds = new ArrayList<String>();
		for ( IDmDoEntity entity : dmDoEntityList ) {
			rpIds.add( entity.getRpId() );
		}
		return rpIds.toArray( new String[0] );
	}
	/**
	 * LotViewBeanのリストを作成して返す<br>
	 * @param pjtLotEntityArray ロット情報の配列
	 * @return LotViewBeanのリスト
	 */
	private List<LotViewBean> makeLotViewBeanList( List<ILotEntity> pjtLotEntityArray ) {
		List<LotViewBean> lotViewBeanList = new ArrayList<LotViewBean>();

		Map<String, ILotEntity> pjtLotNoEntityMap = new HashMap<String, ILotEntity>();
		for ( ILotEntity entity : pjtLotEntityArray ) {

			pjtLotNoEntityMap.put(entity.getLotId(), entity);
			LotViewBean viewBean = new LotViewBean();
			RmViewInfoAddonUtils.setLotViewBeanPjtLotEntity(viewBean, entity, this.support.getBmFinderSupport().findBldSrvEntityByController().getBldSrvId() );
			lotViewBeanList.add(viewBean);
		}
		return lotViewBeanList ;
	}
	/**
	 * DistributeViewBeanのリストを作成して返す<br>
	 * @param relEntityArray リリース情報の配列
	 * @param pjtLotNoEntityMap ロット情報のMap
	 * @param relEnvNoEntityMap リリース環境情報のMap
	 * @return DistributeViewBeanのリスト
	 */
	private List<DistributeViewBean> makeDistributeViewBeanList(
			IRpEntity[] relEntityArray ,
			Map<String,ILotEntity> pjtLotNoEntityMap ,
			Map<String,IBldEnvEntity> relEnvNoEntityMap ) throws ParseException {

		List<DistributeViewBean> distributeViewBeanList = new ArrayList<DistributeViewBean>();

		for ( IRpEntity entity : relEntityArray ) {
			DistributeViewBean viewBean = new DistributeViewBean();
			IDmDoEntity dmDoEntity = this.support.getDmDoEntity(entity);
			setRelDistributeViewBeanRpEntity
				( viewBean, entity, dmDoEntity,pjtLotNoEntityMap ,relEnvNoEntityMap );
			// 無効なロットや環境のリリース情報は表示しない
			if ( TriStringUtils.isEmpty(viewBean.getLotName())
					||  TriStringUtils.isEmpty(viewBean.getConfName()) ) {
				continue;
			}
			// 設定時間の過ぎたリリース情報は表示しない
			String timerDate = viewBean.getTimerDateTime();
			if ( TriStringUtils.isNotEmpty( timerDate ) ) {
				if ( TriDateUtils.checkDateOrderToMinute( TriDateUtils.getSystemDate() , timerDate ) ) {
					continue;
				}
			}
			viewBean.setTimerDateTime(TriDateUtils.convertViewDateFormat(viewBean.getTimerDateTime()));
			distributeViewBeanList.add( viewBean );
		}
		return distributeViewBeanList ;
	}
	/**
	 * ロット情報のMapを作成して返す<br>
	 * @param pjtLotEntityArray ロット情報の配列
	 * @return ロット情報のMap
	 * <pre>
	 * 		Key		:	LotNo
	 * 		Value	:	ILotEntity
	 * </pre>
	 */
	private Map<String,ILotEntity> makePjtLotMap( List<ILotEntity> pjtLotEntityArray ) {
		Map<String, ILotEntity> map = new HashMap<String, ILotEntity>();
		for ( ILotEntity entity : pjtLotEntityArray ) {
			map.put(entity.getLotId(), entity);
		}
		return map ;
	}
	/**
	 * リリース環境情報のMapを作成して返す<br>
	 * @param relEnvEntityArray リリース情報の配列
	 * @return リリース環境情報のMap
	 * <pre>
	 * 		Key		:	LotNo
	 * 		Value	:	ILotEntity
	 * </pre>
	 */
	private Map<String,IBldEnvEntity> makeRelEnvMap( IBldEnvEntity[] relEnvEntityArray ) {
		Map<String, IBldEnvEntity> map = new HashMap<String, IBldEnvEntity>();
		for ( IBldEnvEntity entity : relEnvEntityArray ) {
			map.put(entity.getBldEnvId(), entity);
		}
		return map ;
	}
	/**
	 *
	 * @param paramBean GenericServiceBean オブジェクト
	 * @param selectedPageNo
	 * @param maxPageNumber
	 * @return
	 */
	@SuppressWarnings("deprecation")
	private List<ILotEntity> getPjtLotEntity( GenericServiceBean paramBean, int selectedPageNo , int maxPageNumber ) {

		IJdbcCondition lotCondition	= DBSearchConditionAddonUtil.getActiveLotCondition();
		ISqlSort lotSort			= DBSearchSortAddonUtil.getPjtLotSortFromDesignDefineByRelUnitEntityLotSelect();
		List<String> disableLinkLotNumbers	= new ArrayList<String>();

		// 活動中の全ロットを取得
		// 旧互換機能のため、当該箇所に問題はない。
		this.support.getAmFinderSupport().setAccessableLotNumbers( lotCondition, paramBean, false, disableLinkLotNumbers, true );

		IEntityLimit<ILotEntity> entityLotLimit = this.support.getAmFinderSupport().getLotDao().find( lotCondition.getCondition(), lotSort, selectedPageNo , maxPageNumber );

		return entityLotLimit.getEntities() ;
	}
	/**
	 *
	 * @param selectedPageNo
	 * @param maxPageNumber
	 * @return
	 */
	private IBldEnvEntity[] getRelEnvEntity( int selectedPageNo , int maxPageNumber ) {

		IJdbcCondition envCondition	= DBSearchConditionAddonUtil.getActivelRelEnvCondition();
		ISqlSort envSort			= DBSearchSortAddonUtil.getRelEnvSortFromDesignDefineByRelEntryConfigurationSelect();

		IEntityLimit<IBldEnvEntity> envEntityLimit = this.support.getBmFinderSupport().getBldEnvDao().find( envCondition.getCondition(), envSort, selectedPageNo , maxPageNumber );

		return envEntityLimit.getEntities().toArray(new IBldEnvEntity[0]);
	}
	/**
	 * 画面の表示項目を、配付エンティティから取得して設定する。
	 * @param viewBean 配付画面の表示項目
	 * @param entity ロットエンティティ
	 */
	private void setRelDistributeViewBeanRpEntity(
			DistributeViewBean viewBean, IRpEntity entity, IDmDoEntity dmDoEntity , Map<String, ILotEntity> pjtLotNoEntityMap, Map<String, IBldEnvEntity> relEnvNoEntityMap) {

		viewBean.setDistNo		( entity.getRpId() );
		viewBean.setRelNo		( entity.getRpId() );
		viewBean.setDrmsVersion	( entity.getRpId() );
		String lotId = entity.getLotId();
		ILotEntity pjtLotEntity = pjtLotNoEntityMap.get(lotId);
		if (null != pjtLotEntity) {
			String lotName = pjtLotEntity.getLotNm();
			viewBean.setLotName(lotName);
		}
		String envNo = entity.getBldEnvId();
		IBldEnvEntity relEnvEntity = relEnvNoEntityMap.get(envNo);
		if (null != relEnvEntity) {
			String confName = relEnvEntity.getBldEnvNm();
			viewBean.setConfName(confName);
		}
		viewBean.setSummary		( entity.getSummary() );
		if (null != dmDoEntity) {
			String dateTime = TriDateUtils.convertDateFormat( dmDoEntity.getTimerSettingDate() , dmDoEntity.getTimerSettingTime() ) ;
			viewBean.setTimerDateTime( dateTime ) ;
		}
		viewBean.setStatus		(
				sheet.getValue(
						RmDesignBeanId.statusId, entity.getProcStsId() ));
	}
}
