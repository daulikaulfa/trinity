package jp.co.blueship.tri.dm.domain.dop;

import java.util.List;

import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.dm.constants.RelDistributeScreenID;
import jp.co.blueship.tri.dm.constants.TimerSettings;
import jp.co.blueship.tri.dm.dao.dmdo.eb.IDmDoDto;
import jp.co.blueship.tri.dm.domain.beans.dop.dto.ControlViewBean;
import jp.co.blueship.tri.dm.domain.dop.dto.FlowRelDistributeTimerServiceBean;
import jp.co.blueship.tri.dm.support.FlowRelDistributeEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.RelCommonAddonUtil;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.DmMessageId;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.BaseInfoBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.BaseInfoInputBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.ConfigurationViewBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.LotViewBean;

/**
 * リリース配布・タイマー設定確認画面の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public class FlowRelDistributeTimerServiceConfirm implements IDomain<FlowRelDistributeTimerServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowRelDistributeEditSupport support = null;
	public void setSupport(FlowRelDistributeEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowRelDistributeTimerServiceBean> execute( IServiceDto<FlowRelDistributeTimerServiceBean> serviceDto ) {

		FlowRelDistributeTimerServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID	= paramBean.getReferer();
			String forwordID	= paramBean.getForward();

			if ( true != RelDistributeScreenID.TIMER_CONFIRM.equals( refererID ) &&
					true != RelDistributeScreenID.TIMER_CONFIRM.equals( forwordID ) ){
				return serviceDto;
			}


			String selectedDistNo = paramBean.getSelectedDistNo();

			if( RelDistributeScreenID.TIMER_CONFIRM.equals( forwordID ) ) {

				// 暫定
				checkDistNo	( selectedDistNo );

				// 基本情報
				paramBean.setDistNo(selectedDistNo);
				IDmDoDto dmDoDto = this.support.getDmDoDto( selectedDistNo );
				// グループの存在チェック
				ILotDto dto = this.support.getAmFinderSupport().findLotDto( dmDoDto.getRpEntity().getLotId() );
				List<IGrpUserLnkEntity> groupUsers	= support.getUmFinderSupport().findGrpUserLnkByUserId( paramBean.getUserId() );
				RelCommonAddonUtil.checkAccessableGroup( dto, this.support.getUmFinderSupport().getGrpDao(), groupUsers );

				BaseInfoInputBean inputBean = new BaseInfoInputBean();
				inputBean.setRelContent( dmDoDto.getDmDoEntity().getContent() );
				inputBean.setRelSummary( dmDoDto.getDmDoEntity().getSummary() );
				BaseInfoBean baseInfoBean = new BaseInfoBean();
				baseInfoBean.setBaseInfoInputBean(inputBean);
				paramBean.setBaseInfoBean(baseInfoBean);

				// タイマー情報
				checkRelDistributeTimer( paramBean, dmDoDto );

				String timerSetting = paramBean.getTimerSetting();

				if ( TimerSettings.TimerSettins.legacy().equals( timerSetting ) ) {
					paramBean.setTimerDate( paramBean.getTimerDate() );
				} else if ( TimerSettings.Immediately.legacy().equals( timerSetting ) ) {
				} else if ( TimerSettings.Cancel.legacy().equals( timerSetting ) ) {
					paramBean.setTimerDate( null );
				}

				paramBean.setTimerSetting( timerSetting ) ;
				paramBean.setTimerSummary( paramBean.getTimerSummary() );
				paramBean.setTimerContent( paramBean.getTimerContent() );

				// 環境情報
				String envNo = dmDoDto.getRpEntity().getBldEnvId();
				ConfigurationViewBean confViewBean =  this.support.getEnvReleaseConfirm(envNo);
				paramBean.setConfViewBean(confViewBean);

				// ロット情報
				String lotId = dmDoDto.getRpEntity().getLotId();
				LotViewBean lotViewBean = this.support.getLotReleaseConfirm(lotId);
				paramBean.setLotViewBean(lotViewBean);

				// リリースパッケージ情報
				String relNo = dmDoDto.getRpEntity().getRpId();
				ControlViewBean controlViewBean = this.support.getControlReleaseConfirm(relNo);
				paramBean.setControlViewBean( controlViewBean );

			}

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( DmMessageId.DM005022S, e );
		}
	}

	/**
	 * 連携先管理世代が選択されているかどうかチェックする
	 *
	 * @param distNo 連携先管理世代番号
	 */

	private void checkDistNo( String distNo ) {

		if ( TriStringUtils.isEmpty( distNo ) ) {
			throw new ContinuableBusinessException( DmMessageId.DM001010E );
		}
	}

	/**
	 * 資源配付・タイマー設定の入力情報をチェックする
	 *
	 * @param paramBean
	 * @param dmDoEntity
	 */

	private static void checkRelDistributeTimer(
							FlowRelDistributeTimerServiceBean paramBean,
							IDmDoDto dmDoDto ) {

		String timerSettingDate = dmDoDto.getDmDoEntity().getTimerSettingDate() ;
		String timerSettingTime = dmDoDto.getDmDoEntity().getTimerSettingTime() ;

		String timerSetting = paramBean.getTimerSetting();

		if ( TimerSettings.TimerSettins.legacy().equals( timerSetting ) ) {

			if ( !TriDateUtils.checkYMD( paramBean.getCheckTimerDate() ) ) {
				throw new ContinuableBusinessException( DmMessageId.DM001004E );
			}

			String systemDate = TriDateUtils.getSystemDate();
			if ( TriDateUtils.checkDateOrderToMinute( systemDate, paramBean.getTimerDate()) ) {
				throw new ContinuableBusinessException( DmMessageId.DM001005E );
			}

		} else if ( TimerSettings.Immediately.legacy().equals( timerSetting ) ) {
			if ( true != TriStringUtils.isEmpty( timerSettingDate ) ||
				true != TriStringUtils.isEmpty( timerSettingTime ) ) {//タイマー時刻セット済み⇒即時設定不可
				throw new BusinessException( DmMessageId.DM001008E );
			}
		} else if ( TimerSettings.Cancel.legacy().equals( timerSetting ) ) {
			if ( true == TriStringUtils.isEmpty( timerSettingDate ) &&
				true == TriStringUtils.isEmpty( timerSettingTime ) ) {//タイマー未設定⇒キャンセル不可
				throw new BusinessException( DmMessageId.DM001009E );
			}
		}
	}

}
