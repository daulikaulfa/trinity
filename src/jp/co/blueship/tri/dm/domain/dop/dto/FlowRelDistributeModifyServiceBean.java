package jp.co.blueship.tri.dm.domain.dop.dto;

import jp.co.blueship.tri.dm.domain.beans.dop.dto.ControlViewBean;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.BaseInfoBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.ConfigurationViewBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.LotViewBean;

/**
 * リリースパッケージ・基本情報入力～ビルドパッケージ作成確認用
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class FlowRelDistributeModifyServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;

	/**
	 * リリース基本情報
	 */
	private BaseInfoBean baseInfoBean = null;
	/**
	 * 環境情報
	 */
	private ConfigurationViewBean confViewBean = null;
	/**
	 * ロット情報
	 */
	private LotViewBean lotViewBean = null;
	/**
	 * リリース情報
	 */
	private ControlViewBean controlViewBean = null;
	/**
	 * リリース番号
	 */
	private String relNo = null;
	/**
	 * リリース配布番号
	 */
	private String distNo = null;


	public BaseInfoBean getBaseInfoBean() {
		if ( null == baseInfoBean ) {
			baseInfoBean = new BaseInfoBean();
		}
		return baseInfoBean;
	}
	public void setBaseInfoBean( BaseInfoBean baseInfoBean ) {
		this.baseInfoBean = baseInfoBean;
	}

	public ConfigurationViewBean getConfViewBean() {
		if ( null == confViewBean ) {
			confViewBean = new ConfigurationViewBean();
		}
		return confViewBean;
	}
	public void setConfViewBean(ConfigurationViewBean confViewBean) {
		this.confViewBean = confViewBean;
	}

	public LotViewBean getLotViewBean() {
		if ( null == lotViewBean ) {
			lotViewBean = new LotViewBean();
		}
		return lotViewBean;
	}
	public void setLotViewBean(LotViewBean lotViewBean) {
		this.lotViewBean = lotViewBean;
	}

	public ControlViewBean getControlViewBean() {
		if ( null == controlViewBean ) {
			controlViewBean = new ControlViewBean();
		}
		return controlViewBean;
	}
	public void setControlViewBean(ControlViewBean controlViewBean) {
		this.controlViewBean = controlViewBean;
	}

	public String getRelNo() {
		return relNo;
	}
	public void setRelNo(String relNo) {
		this.relNo = relNo;
	}

	public String getDistNo() {
		return distNo;
	}
	public void setDistNo(String distNo) {
		this.distNo = distNo;
	}

}
