package jp.co.blueship.tri.dm.domain.dop.dto;

import java.util.List;

import jp.co.blueship.tri.dm.domain.beans.dop.dto.ControlViewBean;
import jp.co.blueship.tri.dm.domain.beans.dop.dto.DistributeViewBean;
import jp.co.blueship.tri.dm.domain.beans.dop.dto.ReleaseConfirmBean;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.BaseInfoBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.ConfigurationSelectBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.ConfigurationViewBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.CtlDetailViewBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.LotSelectBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.LotViewBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.UnitSelectBean;

/**
 * リリースパッケージ・基本情報入力～ビルドパッケージ作成確認用
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class FlowRelDistributeTimerServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;

	/**
	 * タイマー設定日
	 */
	private String timerDate;
	/**
	 * タイマー設定概要
	 */
	private String timerSummary = null;
	/**
	 * タイマー設定内容
	 */
	private String timerContent = null;

	/**
	 * リリース基本情報
	 */
	private BaseInfoBean baseInfoBean = null;
	/**
	 * 環境選択
	 */
	private ConfigurationSelectBean confSelectBean = null;
	/**
	 * 選択された環境番号
	 */
	private String selectedEnvNo = null;
	/**
	 * 環境情報
	 */
	private ConfigurationViewBean confViewBean = null;
	/**
	 * ロット選択
	 */
	private LotSelectBean lotSelectBean = null;
	/**
	 * 選択されたロット番号
	 */
	private String selectedLotNo = null;
	/**
	 * ロット情報
	 */
	private LotViewBean lotViewBean = null;
	/**
	 * ビルドパッケージ選択
	 */
	private UnitSelectBean releaseUnitSelectBean = null;
	/**
	 * 選択されたビルドパッケージ番号
	 */
	private String selectedBuildNo[] = null;
	/**
	 * リリースパッケージ確認
	 */
	private ReleaseConfirmBean releaseConfirmBean = null;
	/**
	 * リリース情報
	 */
	private ControlViewBean controlViewBean = null;
	/**
	 * 選択されたリリース番号
	 */
	private String selectedControlNo = null;
	/**
	 * 選択されたリリース配付番号
	 */
	private String selectedDistNo = null;
	/**
	 * リリース番号
	 */
	private String relNo = null;
	/**
	 * リリース状況照会
	 */
	private CtlDetailViewBean releaseDetailViewBean = null;
	/**
	 * リリース配布番号
	 */
	private String distNo = null;
	/**
	 * ロット情報
	 */
	private List<LotViewBean> lotViewBeanList = null;
	/**
	 * 環境情報
	 */
	private List<ConfigurationViewBean> confViewBeanList = null;
	/**
	 * リリース情報
	 */
	private List<ControlViewBean> controlViewBeanList = null;
	/**
	 * 配付情報
	 */
	private List<DistributeViewBean> distributeViewBeanList = null;
	/**
	 * タイマー設定
	 */
	private String timer[] = null;
	/**
	 * タイマーチェック
	 */
	private String checkTimerDate;
	/**
	 * タイマー設定
	 */
	private TimerSetViewBean timerSetViewBean =null;

	/** タイマー設定 */
	private String timerSetting;

	/** 統合監視のメッセージ */
	private String drmsMessage;

	/** リリース配布番号 */
	private boolean rmi = true;


	public String getTimerDate() {
		return timerDate;
	}

	public void setTimerDate(String timerDate) {
		this.timerDate = timerDate;
	}

	public String getTimerSummary() {
		return timerSummary;
	}
	public void setTimerSummary(String timerSummary) {
		this.timerSummary = timerSummary;
	}

	public String getTimerContent() {
		return timerContent;
	}
	public void setTimerContent(String timerContent) {
		this.timerContent = timerContent;
	}

	public BaseInfoBean getBaseInfoBean() {
		if ( null == baseInfoBean ) {
			baseInfoBean = new BaseInfoBean();
		}
		return baseInfoBean;
	}
	public void setBaseInfoBean( BaseInfoBean baseInfoBean ) {
		this.baseInfoBean = baseInfoBean;
	}

	public ConfigurationSelectBean getConfSelectBean() {
		if ( null == confSelectBean ) {
			confSelectBean = new ConfigurationSelectBean();
		}
		return confSelectBean;
	}
	public void setConfSelectBean(ConfigurationSelectBean confSelectBean) {
		this.confSelectBean = confSelectBean;
	}

	public String getSelectedEnvNo() {
		return selectedEnvNo;
	}
	public void setSelectedEnvNo( String selectedEnvNo ) {
		this.selectedEnvNo = selectedEnvNo;
	}

	public LotSelectBean getLotSelectBean() {
		if ( null == lotSelectBean ) {
			lotSelectBean = new LotSelectBean();
		}
		return lotSelectBean;
	}
	public void setLotSelectBean(LotSelectBean lotSelectBean) {
		this.lotSelectBean = lotSelectBean;
	}

	public String getSelectedLotNo() {
		return selectedLotNo;
	}
	public void setSelectedLotNo(String selectedLotNo) {
		this.selectedLotNo = selectedLotNo;
	}

	public UnitSelectBean getReleaseUnitSelectBean() {
		if ( null == releaseUnitSelectBean ) {
			releaseUnitSelectBean = new UnitSelectBean();
		}
		return releaseUnitSelectBean;
	}
	public void setReleaseUnitSelectBean( UnitSelectBean releaseUnitSelectBean ) {
		this.releaseUnitSelectBean = releaseUnitSelectBean;
	}

	public String[] getSelectedBuildNo() {
		return selectedBuildNo;
	}
	public void setSelectedBuildNo( String[] selectedBuildNo ) {
		this.selectedBuildNo = selectedBuildNo;
	}

	public ReleaseConfirmBean getReleaseConfirmBean() {
		if ( null == releaseConfirmBean ) {
			releaseConfirmBean = new ReleaseConfirmBean();
		}
		return releaseConfirmBean;
	}
	public void setReleaseConfirmBean( ReleaseConfirmBean releaseConfirmBean ) {
		this.releaseConfirmBean = releaseConfirmBean;
	}

	public String getRelNo() {
		return relNo;
	}
	public void setRelNo(String relNo) {
		this.relNo = relNo;
	}

	public String getSelectedControlNo() {
		return selectedControlNo;
	}
	public void setSelectedControlNo(String selectedControlNo) {
		this.selectedControlNo = selectedControlNo;
	}

	public CtlDetailViewBean getReleaseDetailViewBean() {
		if ( null == releaseDetailViewBean ) {
			releaseDetailViewBean = new CtlDetailViewBean();
		}
		return releaseDetailViewBean;
	}
	public void setReleaseDetailViewBean(
			CtlDetailViewBean releaseDetailViewBean ) {
		this.releaseDetailViewBean = releaseDetailViewBean;
	}

	public String getDistNo() {
		return distNo;
	}
	public void setDistNo(String distNo) {
		this.distNo = distNo;
	}

	public ConfigurationViewBean getConfViewBean() {
		if ( null == confViewBean ) {
			confViewBean = new ConfigurationViewBean();
		}
		return confViewBean;
	}
	public void setConfViewBean(ConfigurationViewBean confViewBean) {
		this.confViewBean = confViewBean;
	}

	public LotViewBean getLotViewBean() {
		if ( null == lotViewBean ) {
			lotViewBean = new LotViewBean();
		}
		return lotViewBean;
	}
	public void setLotViewBean(LotViewBean lotViewBean) {
		this.lotViewBean = lotViewBean;
	}

	public ControlViewBean getControlViewBean() {
		if ( null == controlViewBean ) {
			controlViewBean = new ControlViewBean();
		}
		return controlViewBean;
	}
	public void setControlViewBean(ControlViewBean controlViewBean) {
		this.controlViewBean = controlViewBean;
	}

	public List<LotViewBean> getLotViewBeanList() {
		return lotViewBeanList;
	}
	public void setLotViewBeanList( List<LotViewBean> lotViewBeanList ) {
		this.lotViewBeanList = lotViewBeanList;
	}

	public List<ConfigurationViewBean> getConfViewBeanList() {
		return confViewBeanList;
	}
	public void setConfViewBeanList( List<ConfigurationViewBean> envViewBeanList ) {
		this.confViewBeanList = envViewBeanList;
	}

	public List<ControlViewBean> getControlViewBeanList() {
		return controlViewBeanList;
	}
	public void setControlViewBeanList( List<ControlViewBean> controlViewBeanList ) {
		this.controlViewBeanList = controlViewBeanList;
	}

	public List<DistributeViewBean> getDistributeViewBeanList() {
		return distributeViewBeanList;
	}
	public void setDistributeViewBeanList( List<DistributeViewBean> distributeViewBeanList ) {
		this.distributeViewBeanList = distributeViewBeanList;
	}

	public String getSelectedDistNo() {
		return selectedDistNo;
	}
	public void setSelectedDistNo(String selectedDistNo) {
		this.selectedDistNo = selectedDistNo;
	}

	public String[] getTimer() {
		return timer;
	}
	public void setTimer(String[] timer) {
		this.timer = timer;
	}

	public String getCheckTimerDate() {
		return checkTimerDate;
	}

	public void setCheckTimerDate(String checkTimerDate) {
		this.checkTimerDate = checkTimerDate;
	}

	/**
	 * リリース配布情報
	 */
	public TimerSetViewBean newTimerSetViewBean() {
		TimerSetViewBean bean = new TimerSetViewBean();
		return bean;
	}

	public TimerSetViewBean getTimerSetViewBean() {
		if (null ==timerSetViewBean  ) {
			timerSetViewBean = newTimerSetViewBean();
		}
		return timerSetViewBean;
	}

	public void setTimerSetViewBean(TimerSetViewBean timerSetViewBean) {
		this.timerSetViewBean = timerSetViewBean;
	}

	public String getTimerSetting() {
		return timerSetting;
	}

	public void setTimerSetting(String timerSetting) {
		this.timerSetting = timerSetting;
	}

	public String getDrmsMessage() {
		return drmsMessage;
	}

	public void setDrmsMessage(String drmsMessage) {
		this.drmsMessage = drmsMessage;
	}

	public void setRmi(boolean rmi) {
		this.rmi = rmi;
	}

	public boolean isRmi(){
		return rmi;
	}

	public class TimerSetViewBean {

		/** 年 */
		private String year = null;
		/** 月 */
		private String month = null;
		/** 日 */
		private String day = null;
		/** 時 */
		private String hour = null;
		/** 分（十の位） */
		private String minuteHigh = null;
		/** 分（一の位） */
		private String minuteLow = null;

		public void setYear( String year ) {
			this.year = year;
		}
		public String getYear() {
			return year;
		}
		public void setMonth( String month ) {
			this.month = month;
		}
		public String getMonth() {
			return month;
		}
		public void setDay( String day ) {
			this.day = day;
		}
		public String getDay() {
			return day;
		}
		public String getHour() {
			return hour;
		}
		public void setHour(String hour) {
			this.hour = hour;
		}
		public String getMinuteLow() {
			return minuteLow;
		}
		public void setMinuteLow(String minuteLow) {
			this.minuteLow = minuteLow;
		}
		public String getMinuteHigh() {
			return minuteHigh;
		}
		public void setMinuteHigh(String minuteHigh) {
			this.minuteHigh = minuteHigh;
		}
	}
}
