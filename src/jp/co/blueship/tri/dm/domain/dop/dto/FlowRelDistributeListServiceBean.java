package jp.co.blueship.tri.dm.domain.dop.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.dm.domain.beans.dop.dto.DistributeViewBean;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.fw.svc.beans.dto.IPageNoInfo;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.ConfigurationSelectBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.ConfigurationViewBean;

/**
 * リリース・配布最新一覧画面用
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class FlowRelDistributeListServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;

	/** 選択ページ */
	private int selectPageNo = 0;
	/** ページ制御 */
	private IPageNoInfo pageInfoView = null;
	/**
	 * 環境選択
	 */
	private ConfigurationSelectBean confSelectBean = null;
	/**
	 * 選択された環境番号
	 */
	private String selectedEnvNo = null;
	/**
	 * リリース配布情報
	 */
	private List<DistributeViewBean> distributeViewBeanList = null;
	/**
	 * リリース配布情報
	 */
	private List<ReleaseViewBean> releaseViewBeanList = null;
	/**
	 * 環境情報
	 */
	private List<ConfigurationViewBean> confViewBeanList = null;
	/**
	 *  統合監視のメッセージ
	 */
	private String drmsMessage = null;
	/**
	 * RMI接続
	 */
	private boolean rmi = true;

	public IPageNoInfo getPageInfoView() {
		return pageInfoView;
	}
	public void setPageInfoView(IPageNoInfo pageInfoView) {
		this.pageInfoView = pageInfoView;
	}

	public int getSelectPageNo() {
		return selectPageNo;
	}
	public void setSelectPageNo(int selectPageNo) {
		this.selectPageNo = selectPageNo;
	}

	public ConfigurationSelectBean getConfSelectBean() {
		if ( null == confSelectBean ) {
			confSelectBean = new ConfigurationSelectBean();
		}
		return confSelectBean;
	}
	public void setConfSelectBean(ConfigurationSelectBean confSelectBean) {
		this.confSelectBean = confSelectBean;
	}

	public String getSelectedEnvNo() {
		return selectedEnvNo;
	}
	public void setSelectedEnvNo( String selectedEnvNo ) {
		this.selectedEnvNo = selectedEnvNo;
	}

	public List<DistributeViewBean> getDistributeViewBeanList() {
		if ( null == distributeViewBeanList ) {
			distributeViewBeanList = new ArrayList<DistributeViewBean>();
		}
		return distributeViewBeanList;
	}
	public void setDistributeViewBeanList(
			List<DistributeViewBean> distributeViewBeanList ) {
		this.distributeViewBeanList = distributeViewBeanList;
	}

	public List<ReleaseViewBean> getReleaseViewBeanList() {
		if ( null == releaseViewBeanList ) {
			releaseViewBeanList = new ArrayList<ReleaseViewBean>();
		}
		return releaseViewBeanList;
	}
	public void setReleaseViewBeanList(
			List<ReleaseViewBean> releaseViewBeanList ) {
		this.releaseViewBeanList = releaseViewBeanList;
	}

	public List<ConfigurationViewBean> getConfViewBeanList() {
		return confViewBeanList;
	}
	public void setConfViewBeanList( List<ConfigurationViewBean> envViewBeanList ) {
		this.confViewBeanList = envViewBeanList;
	}

	public String getDrmsMessage() {
		return drmsMessage;
	}
	public void setDrmsMessage(String drmsMessage) {
		this.drmsMessage = drmsMessage;
	}

	public void setRmi(boolean rmi) {
		this.rmi = rmi;
	}
	public boolean isRmi(){
		return rmi;
	}


	/**
	 * リリース配布情報
	 */
	public ReleaseViewBean newReleaseViewBean() {
		ReleaseViewBean bean = new ReleaseViewBean();
		return bean;
	}

	public class ReleaseViewBean {

		/** リリース番号 */
		private String relNo = null;
		/** 配布番号 */
		private String distNo = null;
		/** 環境番号 */
		private String envNo = null;
		/** リリース環境名 */
		private String envName = null;
		/** DRMS管理バージョン */
		private String drmsVer = null;
		/** ロット番号 */
		private String lotId = null;
		/** ロット名 */
		private String lotName = null;
		/** 配布設定日付 */
		private String distDateTimeDate = null;
		/** 配布設定時刻 */
		private String distDateTimeTime = null;
		private String distDateTime = null ;

		/** 配布テータス */
		private String distStatus = null;
		/** DRMS資源登録日時 */
		private String drmsInputDate = null;
		/** 適用日時 */
		private String applyDate = null;
		/** DRMSテータス */
		private String drmsStatus = null;
		/** 取消ボタンの表示の有無 */
		private boolean cancelView = false;
		/** リンク番号 */
		private String linkNo = null;
		/**
		 * このロットの閲覧リンクを有効にするかどうか
		 */
		private boolean isViewLinkEnabled = false;


		public void setRelNo( String relNo ) {
			this.relNo = relNo;
		}
		public String getRelNo() {
			return relNo;
		}

		public void setDistNo( String distNo ) {
			this.distNo = distNo;
		}
		public String getDistNo() {
			return distNo;
		}

		public void setEnvNo( String envNo ) {
			this.envNo = envNo;
		}
		public String getEnvNo() {
			return envNo;
		}

		public void setEnvName( String envName ) {
			this.envName = envName;
		}
		public String getEnvName() {
			return envName;
		}

		public void setDrmsVer( String drmsVer ) {
			this.drmsVer = drmsVer;
		}
		public String getDrmsVer() {
			return drmsVer;
		}

		public void setLotName( String lotName ) {
			this.lotName = lotName;
		}
		public String getLotName() {
			return lotName;
		}

		public void setDistDateTimeDate( String distDateTimeDate ) {
			this.distDateTimeDate = distDateTimeDate;
		}
		public String getDistDateTimeDate() {
			return distDateTimeDate;
		}

		public void setDistDateTimeTime( String distDateTimeTime ) {
			this.distDateTimeTime = distDateTimeTime;
		}
		public String getDistDateTimeTime() {
			return distDateTimeTime;
		}

		public void setDistStatus( String distStatus ) {
			this.distStatus = distStatus;
		}
		public String getDistStatus() {
			return distStatus;
		}
		public void setDrmsInputDate( String drmsInputDate ) {
			this.drmsInputDate = drmsInputDate;
		}
		public String getDrmsInputDate() {
			return drmsInputDate;
		}

		public void setApplyDate( String applyDate ) {
			this.applyDate = applyDate;
		}
		public String getApplyDate() {
			return applyDate;
		}

		public void setDrmsStatus( String drmsStatus ) {
			this.drmsStatus = drmsStatus;
		}
		public String getDrmsStatus() {
			return drmsStatus;
		}

		public boolean getCancelView() {
			return cancelView;
		}
		public void setCancelView( boolean cancelView ) {
			this.cancelView = cancelView;
		}

		public void setLinkNo( String linkNo ) {
			this.linkNo = linkNo;
		}
		public String getLinkNo() {
			return linkNo;
		}
		public String getDistDateTime() {
			return distDateTime;
		}
		public void setDistDateTime(String distDateTime) {
			this.distDateTime = distDateTime;
		}
		public String getLotNo() {
			return lotId;
		}
		public void setLotNo(String lotId) {
			this.lotId = lotId;
		}
		public boolean isViewLinkEnabled() {
			return isViewLinkEnabled;
		}
		public void setViewLinkEnabled( boolean isViewLinkEnabled ) {
			this.isViewLinkEnabled = isViewLinkEnabled;
		}

	}
}
