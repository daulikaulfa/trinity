package jp.co.blueship.tri.dm.domain.dop.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.fw.svc.beans.dto.IPageNoInfo;
/**
 * リリース・配布最新一覧画面用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class FlowRelDistributeTopMenuServiceBean extends GenericServiceBean {
	
	private static final long serialVersionUID = 1L;
	
	/** 選択ページ */
	private int selectPageNo = 0;
	/** リリース番号長 */
	protected static final int LENGTH_REL_NO = 14 ;//RC番号のパラメータ長
	/** ページ制御 */
	private IPageNoInfo pageInfoView = null;
	/** リリース配布情報 */
	private List<ReleaseViewBean> releaseViewBeanList = null;
	/** RMI接続 */
	private boolean rmi = true;
	/** 統合監視のメッセージ */
	private String drmsMessage = null;

	public IPageNoInfo getPageInfoView() {
		return pageInfoView;
	}
	public void setPageInfoView(IPageNoInfo pageInfoView) {
		this.pageInfoView = pageInfoView;
	}
	
	public int getSelectPageNo() {
		return selectPageNo;
	}
	public void setSelectPageNo(int selectPageNo) {
		this.selectPageNo = selectPageNo;
	}

	public void setRmi(boolean rmi) {
		this.rmi = rmi;
	}
	public boolean isRmi(){
		return rmi;
	}
	
	public List<ReleaseViewBean> getReleaseViewBeanList() {
		if ( null == releaseViewBeanList ) {
			releaseViewBeanList = new ArrayList<ReleaseViewBean>();
		}
		return releaseViewBeanList;
	}
	public void setReleaseViewBeanList(
			List<ReleaseViewBean> releaseViewBeanList ) {
		this.releaseViewBeanList = releaseViewBeanList;
	}
	
	public String getDrmsMessage() {
		return drmsMessage;
	}
	public void setDrmsMessage(String drmsMessage) {
		this.drmsMessage = drmsMessage;
	}

	/**
	 * リリース配布情報
	 */
	public ReleaseViewBean newReleaseViewBean() {
		ReleaseViewBean bean = new ReleaseViewBean();
		return bean;
	}
	
	public class ReleaseViewBean {

		/** リリース番号 */
		private String relNo = null;
		/** 配布番号 */
		private String distNo = null;
		/** 環境番号 */
		private String envNo = null;
		/** リリース環境名 */
		private String envName = null;
		/** DRMS管理バージョン */
		private String drmsVer = null;
		/** ロット番号 */
		private String lotId = null ;
		/** ロット名 */
		private String lotName = null;
		/** 作成日時 */
		private String inputDate = null;
		/** 配布設定日時 */
		private String distDate = null;
		/** 配布テータス */
		private String distStatus = null;
		/** DRMS資源登録日時 */
		private String drmsInputDate = null;
		/** 適用日時 */
		private String applyDate = null;
		/** DRMSテータス */
		private String drmsStatus = null;
		/** リンク番号 */
		private String linkNo = null;
		
		public void setRelNo( String relNo ) {
			this.relNo = relNo;
		}
		public String getRelNo() {
			return relNo;
		}

		public void setDistNo( String distNo ) {
			this.distNo = distNo;
		}
		public String getDistNo() {
			return distNo;
		}

		public void setEnvNo( String envNo ) {
			this.envNo = envNo;
		}
		public String getEnvNo() {
			return envNo;
		}

		public void setEnvName( String envName ) {
			this.envName = envName;
		}
		public String getEnvName() {
			return envName;
		}

		public void setDrmsVer( String drmsVer ) {
			this.drmsVer = drmsVer;
		}
		public String getDrmsVer() {
			return drmsVer;
		}
		
		public void setLotName( String lotName ) {
			this.lotName = lotName;
		}
		public String getLotName() {
			return lotName;
		}

		public void setDistDate( String distDate ) {
			this.distDate = distDate;
		}
		public String getDistDate() {
			return distDate;
		}

		public void setInputDate( String inputDate ) {
			this.inputDate = inputDate;
		}
		public String getInputDate() {
			return inputDate;
		}

		public void setDistStatus( String distStatus ) {
			this.distStatus = distStatus;
		}
		public String getDistStatus() {
			return distStatus;
		}

		public void setDrmsInputDate( String drmsInputDate ) {
			this.drmsInputDate = drmsInputDate;
		}
		public String getDrmsInputDate() {
			return drmsInputDate;
		}

		public void setApplyDate( String applyDate ) {
			this.applyDate = applyDate;
		}
		public String getApplyDate() {
			return applyDate;
		}

		public void setDrmsStatus( String drmsStatus ) {
			this.drmsStatus = drmsStatus;
		}
		public String getDrmsStatus() {
			return drmsStatus;
		}
		
		public void setLinkNo( String linkNo ) {
			this.linkNo = linkNo;
		}
		public String getLinkNo() {
			return linkNo;
		}
		public String getLotNo() {
			return lotId;
		}
		public void setLotNo(String lotId) {
			this.lotId = lotId;
		}
		
	}
}
