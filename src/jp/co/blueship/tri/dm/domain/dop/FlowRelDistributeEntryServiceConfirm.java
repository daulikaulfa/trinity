package jp.co.blueship.tri.dm.domain.dop;

import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvEntity;
import jp.co.blueship.tri.dm.constants.RelDistributeScreenID;
import jp.co.blueship.tri.dm.domain.beans.dop.dto.ReleaseConfirmBean;
import jp.co.blueship.tri.dm.domain.dop.dto.FlowRelDistributeEntryServiceBean;
import jp.co.blueship.tri.dm.support.FlowRelDistributeEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.dao.orm.DBSearchConditionAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.DBSearchSortAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.DmMessageId;
import jp.co.blueship.tri.rm.RmViewInfoAddonUtils;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.BaseInfoInputBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.ConfigurationViewBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.LotViewBean;


/**
 * リリース配布・リリース確認画面の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public class FlowRelDistributeEntryServiceConfirm implements IDomain<FlowRelDistributeEntryServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowRelDistributeEditSupport support = null;

	public void setSupport(FlowRelDistributeEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowRelDistributeEntryServiceBean> execute( IServiceDto<FlowRelDistributeEntryServiceBean> serviceDto ) {

		FlowRelDistributeEntryServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID	= paramBean.getReferer();
			String forwordID	= paramBean.getForward();

			if ( true != RelDistributeScreenID.ENTRY_CONFIRM.equals( refererID ) &&
					true != RelDistributeScreenID.ENTRY_CONFIRM.equals( forwordID ) ){
				return serviceDto;
			}


			String selectedControlNo = paramBean.getSelectedControlNo();

			if( RelDistributeScreenID.ENTRY_CONFIRM.equals( forwordID ) ) {

				if ( ! ScreenType.bussinessException.equals( paramBean.getScreenType() ) ) {
				}

				if ( ! TriStringUtils.isEmpty( selectedControlNo ) ) {
					ReleaseConfirmBean releaseComfirmBean = paramBean.getReleaseConfirmBean();

					// 基本情報
					setBaseInfoInputReleaseConfirm(
							paramBean.getBaseInfoBean().getBaseInfoInputBean(), releaseComfirmBean );

					// 環境情報
					String envNo = paramBean.getSelectedEnvNo();
					setEnvReleaseConfirm( envNo, releaseComfirmBean );

					// ロット情報
					String lotId = paramBean.getSelectedLotNo();
					setLotReleaseConfirm( lotId, releaseComfirmBean );

					// リリースパッケージ情報
					setControlReleaseConfirm( selectedControlNo, releaseComfirmBean );

					paramBean.setReleaseConfirmBean( releaseComfirmBean );
				}

			}

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(DmMessageId.DM004005F , e);
		}
	}

	/**
	 * 基本情報入力ビーンからリリース確認ビーンに値をセットする。
	 *
	 */
	private void setBaseInfoInputReleaseConfirm(
			BaseInfoInputBean baseInputBean, ReleaseConfirmBean releaseComfirmBean ) {

		releaseComfirmBean.setRelSummary	( baseInputBean.getRelSummary() );
		releaseComfirmBean.setRelContent	( baseInputBean.getRelContent() );
	}

	/**
	 * ロットの情報をリリース確認ビーンに値をセットする。
	 *
	 */
	private void setLotReleaseConfirm(
			String lotId, ReleaseConfirmBean releaseComfirmBean ) {

		ILotEntity pjtLotEntity = this.support.getAmFinderSupport().findLotEntity( lotId );

		LotViewBean lotViewBean = new LotViewBean();
		RmViewInfoAddonUtils.setLotViewBeanPjtLotEntity( lotViewBean, pjtLotEntity, this.support.getBmFinderSupport().findBldSrvEntityByController().getBldSrvId() );

		releaseComfirmBean.setLotViewBean( lotViewBean );
	}

	/**
	 * ロットの情報をリリース確認ビーンに値をセットする。
	 *
	 */
	private void setEnvReleaseConfirm(
			String envNo, ReleaseConfirmBean releaseComfirmBean ) {

		IBldEnvEntity envEntity = this.support.getBmFinderSupport().findBldEnvEntity( envNo );

		ConfigurationViewBean confViewBean = new ConfigurationViewBean();
		RmViewInfoAddonUtils.setConfViewBeanRelEnvEntity( confViewBean, envEntity );

		releaseComfirmBean.setConfViewBean( confViewBean );
	}

	/**
	 * リリースパッケージの情報をリリース確認ビーンに値をセットする。
	 *
	 */
	private void setControlReleaseConfirm(
			String controlNo, ReleaseConfirmBean releaseComfirmBean ) {

		IJdbcCondition condition =
			DBSearchConditionAddonUtil.getRpConditionByRelNo( controlNo );
		ISqlSort sort			= DBSearchSortAddonUtil.getRelUnitSortFromDesignDefineByRelEntryConfirm();

//		int selectedPageNo	=
//			(( 0 == releaseComfirmBean.getSelectPageNo() )? 1: releaseComfirmBean.getSelectPageNo() );
//		int maxPageNumber	=
//			DesignProjectUtil.getMaxPageNumber(
//					DesignProjectRelDefineId.maxPageNumberByRelList );

		int selectedPageNo	= 1;
		int maxPageNumber	= 0;

		IEntityLimit<IRpEntity> limit =
			this.support.getRpDao().find( condition.getCondition(), sort, selectedPageNo, maxPageNumber );

		//暫定
		releaseComfirmBean.setControlViewBean	(
				this.support.getControlViewBean( limit.getEntities().toArray(new IRpEntity[0]) ));

		//		releaseComfirmBean.setPageInfoView		(
//				AddonUtil.convertPageNoInfo( new PageNoInfo(), buildEntityLimit.getLimit() ));
//		releaseComfirmBean.setSelectPageNo		( selectedPageNo );
	}

//	/**
//	 * リリース登録中のリリースエンティティを取得する。
//	 *
//	 */
//	private IRelEntity[] getActiveRelEntity() {
//
//		ICondition condition = ListSearchAddonUtil.getActiveRelCondition();
//
//		IRelEntityLimit entityLimit = this.support.getRelDao().find( condition, null, 1, 0 );
//
//		return entityLimit.getEntity();
//	}
}
