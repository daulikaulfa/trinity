package jp.co.blueship.tri.dm.domain.dop;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.dm.DmFluentFunctionUtils;
import jp.co.blueship.tri.dm.constants.RelDistributeScreenID;
import jp.co.blueship.tri.dm.dao.dmdo.eb.DmDoCondition;
import jp.co.blueship.tri.dm.dao.dmdo.eb.IDmDoEntity;
import jp.co.blueship.tri.dm.domain.beans.dop.dto.ControlSelectBean;
import jp.co.blueship.tri.dm.domain.beans.dop.dto.ControlViewBean;
import jp.co.blueship.tri.dm.domain.dop.dto.FlowRelDistributeEntryServiceBean;
import jp.co.blueship.tri.dm.support.FlowRelDistributeEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.ItemCheckAddonUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.status.DmDoStatusId;
import jp.co.blueship.tri.fw.constants.status.RmRpStatusId;
import jp.co.blueship.tri.fw.dao.orm.DBSearchSortAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.DmMessageId;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.RpCondition;


/**
 * 配付資源登録・リリース選択画面の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public class FlowRelDistributeEntryServiceControlSelect implements IDomain<FlowRelDistributeEntryServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowRelDistributeEditSupport support = null;

	public void setSupport(FlowRelDistributeEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowRelDistributeEntryServiceBean> execute( IServiceDto<FlowRelDistributeEntryServiceBean> serviceDto ) {

		FlowRelDistributeEntryServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID	= paramBean.getReferer();
			String forwordID	= paramBean.getForward();
			String screenType	= paramBean.getScreenType();

			if ( true != RelDistributeScreenID.SELECT.equals( refererID ) &&
					true != RelDistributeScreenID.SELECT.equals( forwordID ) ){
				return serviceDto;
			}

			if( RelDistributeScreenID.SELECT.equals( refererID ) ) {

				String selectedControlNo = paramBean.getSelectedControlNo();

				if ( ScreenType.next.equals( screenType ) &&
						true != RelDistributeScreenID.SELECT.equals( forwordID ) ) {

					ItemCheckAddonUtils.checkRelNo( selectedControlNo );
					//this.support.checkReleasableRelUnit	( selectedBuildNo );
					IDmDoEntity dmDoEntity = this.support.getDmDoEntity( this.support.findRpEntity( selectedControlNo ) );
					if ( ! isRelCtlRegistrationEnabled( dmDoEntity )  ) {
						throw new BusinessException( DmMessageId.DM001001E );
					}
				}
			}

			if( RelDistributeScreenID.SELECT.equals( forwordID ) ) {

				String selectedLotNo = paramBean.getSelectedLotNo();
				String selectedEnvNo = paramBean.getSelectedEnvNo();
				// 画面遷移のために仮処理
//				ItemCheckAddonUtil.checkLotNo( selectedLotNo );

				// 指定されたロット番号を持ち、リリース可能なビルドパッケージ
				IRpEntity[] relEntityArray = this.getRelEntity( selectedEnvNo , selectedLotNo ) ;
				if ( true == TriStringUtils.isEmpty( relEntityArray ) ) {
					throw new BusinessException( DmMessageId.DM001000E );
				}

				ILotEntity lotEntity = this.support.getAmFinderSupport().findLotEntity(selectedLotNo);
				List<ControlViewBean> controlList = this.support.getControlViewBeanList( relEntityArray , lotEntity);
				if ( true == TriStringUtils.isEmpty( controlList ) ) {
					throw new BusinessException( DmMessageId.DM001000E );
				}
				ControlSelectBean controlSelectBean = paramBean.getControlSelectBean();
				controlSelectBean.setControlViewBeanList( controlList );
//				unitSelectBean.setPageInfoView		(
//						AddonUtil.convertPageNoInfo( new PageNoInfo(), buildEntityLimit.getLimit() ));
//				unitSelectBean.setSelectPageNo		( selectedPageNo );

				paramBean.setControlSelectBean( controlSelectBean );
			}

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(DmMessageId.DM005016S , e);
		}
	}

	/**
	 *
	 * @param envNo
	 * @param lotNo
	 * @param status
	 * @return
	 */
	private IRpEntity[] getRelEntity( String envNo , String lotId ) {

		RpCondition condition = new RpCondition();
		condition.setBldEnvId( envNo );
		condition.setLotId( lotId );
		condition.setStsIds( new String[] { RmRpStatusId.ReleasePackageCreated.getStatusId() } );

//		ISort sort			= DBSearchSortAddonUtil.getRelUnitSortFromDesignDefineByRelEntryReleaseUnitSelect();
		ISqlSort sort = DBSearchSortAddonUtil.getRelSortFromDesignDefineByRelHistoryList();


//		int selectedPageNo	=
//			(( 0 == unitSelectBean.getSelectPageNo() )? 1: unitSelectBean.getSelectPageNo() );
//		int maxPageNumber	=
//			DesignProjectUtil.getMaxPageNumber(
//				DesignProjectRelDefineId.maxPageNumberByRelList );

		// 対象外のRpId取得
		List<String> rpIds = getRpIdsFromDmDoStsIds();
		List<IRpEntity> rpEntities = new ArrayList<IRpEntity>();

		for ( IRpEntity rpEntity : this.support.getRpDao().find( condition.getCondition(), sort ) ) {
			if( !rpIds.contains( rpEntity.getRpId() ) ) {
				// 有効なRp情報のみ収集
				rpEntities.add(rpEntity);
			}
		}
		return rpEntities.toArray(new IRpEntity[0]) ;
	}

	private List<String> getRpIdsFromDmDoStsIds(){

		DmDoCondition dmDoCondition = new DmDoCondition();
		dmDoCondition.setStsIds(
				new String[] {
						DmDoStatusId.JobRegistered.getStatusId(),
						DmDoStatusId.TimerConfigured.getStatusId(),
						DmDoStatusId.JobCancelled.getStatusId() } );
		List<IDmDoEntity> dmDoEntityList = this.support.getDmDoDao().find( dmDoCondition.getCondition() );

		return FluentList.from(dmDoEntityList).map( DmFluentFunctionUtils.toRpIdFromDmDo ).asList();
	}

	/**
	 * この申請が資源登録可能かどうかを判定します。
	 *
	 * @param entity
	 * @return
	 */
	private static final boolean isRelCtlRegistrationEnabled( IDmDoEntity entity ) {
		if ( null == entity )
			return true;	// Rp登録済みでDo未登録

		String distBaseStatusId = entity.getStsId();

		if ( ! DmDoStatusId.JobRegistered.equals( distBaseStatusId )
			&& ! DmDoStatusId.TimerConfigured.equals( distBaseStatusId )
			&& ! DmDoStatusId.JobCancelled.equals( distBaseStatusId ) )
				return true;

		return false;
	}
}
