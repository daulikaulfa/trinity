package jp.co.blueship.tri.dm.domain.dop;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.CopyOnWriteArrayList;

import jp.co.blueship.tri.agent.dm.opt.beans.dto.DealAssetRegistrationHistoryBean;
import jp.co.blueship.tri.agent.dm.opt.beans.dto.DealAssetServiceBean;
import jp.co.blueship.tri.agent.dm.opt.beans.dto.DealAssetServiceResponseBean;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotCondition;
import jp.co.blueship.tri.bm.dao.bldenv.eb.BldEnvCondition;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvEntity;
import jp.co.blueship.tri.dm.DmDesignBusinessRuleUtils;
import jp.co.blueship.tri.dm.constants.DrmsApplyResultId;
import jp.co.blueship.tri.dm.constants.DrmsStatusId;
import jp.co.blueship.tri.dm.constants.RelDistributeCommercialShellPropertyDefineID;
import jp.co.blueship.tri.dm.constants.RelDistributeScreenID;
import jp.co.blueship.tri.dm.dao.dmdo.eb.DmDoCondition;
import jp.co.blueship.tri.dm.dao.dmdo.eb.IDmDoEntity;
import jp.co.blueship.tri.dm.domain.beans.CommercialShellID;
import jp.co.blueship.tri.dm.domain.beans.dop.dto.CommercialShellBean;
import jp.co.blueship.tri.dm.domain.beans.dop.dto.DistributeViewBean;
import jp.co.blueship.tri.dm.domain.dop.dto.FlowRelDistributeListServiceBean;
import jp.co.blueship.tri.dm.domain.dop.dto.FlowRelDistributeListServiceBean.ReleaseViewBean;
import jp.co.blueship.tri.dm.support.FlowRelDistributeEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.ItemCheckAddonUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.DmDesignBeanId;
import jp.co.blueship.tri.fw.constants.DmDesignEntryKeyByCommercialShell;
import jp.co.blueship.tri.fw.constants.RmDesignEntryKeyByRelease;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.status.DmDoStatusId;
import jp.co.blueship.tri.fw.constants.status.RmRpStatusId;
import jp.co.blueship.tri.fw.dao.orm.DBSearchConditionAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.DBSearchSortAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.DmMessageId;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.svc.beans.dto.PageNoInfo;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.RpCondition;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.ConfigurationSelectBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.ConfigurationViewBean;


/**
 * リリース配付・配付履歴一覧画面の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public class FlowRelDistributeListService implements IDomain<FlowRelDistributeListServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowRelDistributeEditSupport support = null;
	public void setSupport( FlowRelDistributeEditSupport support ) {
		this.support = support;
	}

	private List<IDomain<IGeneralServiceBean>> actions = new ArrayList<IDomain<IGeneralServiceBean>>();

	private List<IDomain<IGeneralServiceBean>> makeShellActions = new ArrayList<IDomain<IGeneralServiceBean>>();

	public void setActions(List<IDomain<IGeneralServiceBean>> actions) {
		this.actions = actions;
	}

	public void setMakeShellActions(List<IDomain<IGeneralServiceBean>> actions) {
		this.makeShellActions = actions;
	}

	@Override
	public IServiceDto<FlowRelDistributeListServiceBean> execute( IServiceDto<FlowRelDistributeListServiceBean> serviceDto ) {

		FlowRelDistributeListServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();
			String refererID = paramBean.getReferer();
			String forwordID = paramBean.getForward();
			String screenType = paramBean.getScreenType();

			if ( true != RelDistributeScreenID.LIST.equals( forwordID ) ) {
				return serviceDto;
			}

			// 選択コンボ用環境リスト
			IBldEnvEntity[] envAllEntities = this.getRelEnvEntity( 1 , 0 ) ;
			if ( TriStringUtils.isEmpty( envAllEntities ) ) {
				throw new BusinessException( RmMessageId.RM001020E );
			}

			if ( null != screenType && ScreenType.bussinessException.equals( screenType ) ) {
				return serviceDto;
			}

			// 活動中の全ロットを取得
			List<ILotEntity> accessableLotList = null;
			List<String> disableLinkLotNumbers	= new ArrayList<String>();
			{
				LotCondition lotCondition = (LotCondition)DBSearchConditionAddonUtil.getActiveLotCondition();

				accessableLotList = this.support.getAmFinderSupport().getAccessableLotNumbers( lotCondition, null, paramBean, disableLinkLotNumbers, true );
			}
			List<ILotDto> lotDtoEntities = this.support.getAmFinderSupport().findLotDto(accessableLotList);

			//リリース環境選択リストの設定
			IBldEnvEntity[] envEntities =
				this.support.getAmFinderSupport().getAccessableRelEnvEntity( envAllEntities, lotDtoEntities );
			{
				List<ConfigurationViewBean> confViewBeanList = this.support.makeConfigurationViewBeanList( envEntities ) ;

				ConfigurationSelectBean confSelectBean = paramBean.getConfSelectBean();
				confSelectBean.setConfViewBeanList	( confViewBeanList );
				paramBean.setConfViewBeanList(confViewBeanList);
			}

			String selectedEnvNo = paramBean.getSelectedEnvNo();
			String distributeStatus [] = null ;
			if ( null != refererID &&
					true != RelDistributeScreenID.LIST.equals( refererID ) ) {
				List<DistributeViewBean> distributeViewBeanList = new ArrayList<DistributeViewBean>();
				paramBean.setDistributeViewBeanList( distributeViewBeanList );
				return serviceDto;
			} else {
				distributeStatus = new String[] { DmDoStatusId.JobRegistered.getStatusId(),
						DmDoStatusId.TimerConfigured.getStatusId(),
						DmDoStatusId.JobCancelled.getStatusId() } ;
			}

			int selectedPageNo	= ( 0 == paramBean.getSelectPageNo() ) ? 1 : paramBean.getSelectPageNo() ;
			int maxPageNumber = sheet.intValue( RmDesignEntryKeyByRelease.maxPageNumberByHistoryList );

			IEntityLimit<IRpEntity> relLimit = this.getRelEntityLimit(
					selectedEnvNo,
					this.support.getAmFinderSupport().convertToLotNo(accessableLotList),
					distributeStatus,
					selectedPageNo,
					maxPageNumber );

			if ( ! ScreenType.bussinessException.equals( paramBean.getScreenType() ) ) {
				if ( null != screenType && "search".equals( screenType ) ) {
					if ( true == TriStringUtils.isEmpty( relLimit.getEntities() ) ) {
						paramBean.setInfoMessage( DmMessageId.DM001002E );
					}
				}
			}
			paramBean.setPageInfoView			(
					DmDesignBusinessRuleUtils.convertPageNoInfo( new PageNoInfo(), relLimit.getLimit() ));
			paramBean.setSelectPageNo			( selectedPageNo );

			// 環境選択チェック
			if ( true != TriStringUtils.isEmpty( selectedEnvNo ) ) {
				ItemCheckAddonUtils.checkConfNo( selectedEnvNo );
			}

			// 配付履歴確認実行
			List<DealAssetRegistrationHistoryBean> historyBeanList = null;

			List<Object> paramList = new CopyOnWriteArrayList<Object>() ;
			int returnCode = 0;

			if ( true != TriStringUtils.isEmpty( screenType ) &&
					"search".equals( screenType ) ) {

				if( true == TriStringUtils.isEmpty( selectedEnvNo ) ) {//リリース環境未選択で検索ボタン押下
					throw new BusinessException( RmMessageId.RM001021E ) ;
				}

				// 商用シェル出力
				if( !paramBean.isRmi() ) {
					this.outputCommercialShell( paramBean, selectedEnvNo ) ;
				}
				// RMI接続可否をチェック
				DealAssetServiceResponseBean resBean = null;
				if (paramBean.isRmi()) {
					DealAssetServiceBean dealBean = new DealAssetServiceBean();
					dealBean.setTargetEnvironment(selectedEnvNo);
					paramList.add(dealBean);

					IServiceDto<IGeneralServiceBean> innerServiceDto = new ServiceDto<IGeneralServiceBean>()
							.setServiceBean( paramBean )
							.setParamList( paramList );

					// 下層のアクションを実行する
					for ( IDomain<IGeneralServiceBean> action : actions ) {
						action.execute( innerServiceDto );
					}
					//戻り値のチェック
					resBean = this.support.getDealAssetServiceResponseBean( paramList ) ;
					if ( true == TriStringUtils.isEmpty( resBean ) ) {
						paramBean.setInfoMessage(DmMessageId.DM001006E);
					} else {
						returnCode = resBean.getReturnCode();
						historyBeanList = resBean.getRegistrationHistoryList();
					}

				}
				if (paramBean.isRmi() && returnCode != 0) {
					// エラーメッセージ出力
					String errMessage = this.support.makeRmiErrorMessage( resBean.getOutLog() , resBean.getErrLog() ) ;
					paramBean.setDrmsMessage( errMessage );
					if (returnCode == 1) {
						paramBean.setInfoMessage(DmMessageId.DM001007E);
					} else {
						paramBean.setInfoMessage(DmMessageId.DM001011E);
					}
				}
			}

			List<ReleaseViewBean> releaseViewBeanList = new ArrayList<ReleaseViewBean>();
			if (null != selectedEnvNo) {

				for ( IRpEntity relEntity : relLimit.getEntities() ) {
					// 画面表示設定
					IDmDoEntity dmDoEntity = this.support.getDmDoEntity(relEntity);
					// スタータスが取消の場合、取消できない。
					if ( RmRpStatusId.ReleasePackageRemoved.equals( relEntity.getProcStsId() ) ){
						continue;
					}
					ReleaseViewBean viewBean = paramBean.newReleaseViewBean();
					viewBean.setRelNo			( relEntity.getRpId() );
					viewBean.setDistNo			( relEntity.getRpId() );
					viewBean.setDrmsVer( relEntity.getRpId() );
					String lotId = relEntity.getLotId();
					ILotEntity lotEntity = this.support.getAmFinderSupport().findLotEntity( lotId );
					viewBean.setLinkNo( dmDoEntity.getMgtVer() );
					viewBean.setLotNo( lotId ) ;
					viewBean.setLotName(lotEntity.getLotNm());
					viewBean.setDistDateTimeDate ( dmDoEntity.getTimerSettingDate() ) ;
					viewBean.setDistDateTimeTime ( dmDoEntity.getTimerSettingTime() ) ;
					String dateTime =TriDateUtils.convertDateFormat( dmDoEntity.getTimerSettingDate() , dmDoEntity.getTimerSettingTime() ) ;
					String dateTimeLabel = TriDateUtils.convertViewDateFormat( dateTime ) ;
					viewBean.setDistDateTime( dateTimeLabel ) ;


					// 取り消されている場合は取消できない。
					if ( DmDoStatusId.JobCancelled.equals( dmDoEntity.getProcStsId() ) ){
						viewBean.setCancelView( false ) ;
					} else {
						// タイマーが設定されている場合は取消できない。
						if ( true == TriStringUtils.isEmpty( dmDoEntity.getTimerSettingDate() ) ||
								true == TriStringUtils.isEmpty( dmDoEntity.getTimerSettingTime() ) ) {
							viewBean.setCancelView( true ) ;
						}
					}

					if ( disableLinkLotNumbers.contains(lotId) ) {
						viewBean.setCancelView( false ) ;
						viewBean.setViewLinkEnabled( false );
					} else {
						viewBean.setViewLinkEnabled( true );
					}

					viewBean.setDistStatus		(
							sheet.getValue(
									DmDesignBeanId.distributeStatusId, dmDoEntity.getProcStsId() ));
					if (null != historyBeanList) {
						setLatest(historyBeanList, viewBean);
					}
					releaseViewBeanList.add( viewBean );

				}
			}

			paramBean.setReleaseViewBeanList	( releaseViewBeanList );

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(DmMessageId.DM005019S,e);
		}
	}




	/**
	 * 最新の適用リリースを取得する
	 * @param statusBeanList 配付履歴情報
	 * @return 資源情報
	 */
	private ReleaseViewBean  setLatest(List<DealAssetRegistrationHistoryBean> historyBeanList, ReleaseViewBean viewBean) {

		String latestDate = FlowRelDistributeEditSupport.TIMER_ALL_ZERO ;
		for( DealAssetRegistrationHistoryBean historyBean : historyBeanList) {
			// RC番号が一致
//			if (viewBean.getRelNo().equals(historyBean.getDrmsManagementNo())) {
			if (null != viewBean.getLinkNo()) {
				if (viewBean.getLinkNo().equals(historyBean.getDrmsManagementNo())) {
					String date = historyBean.getRegistrationDate();
					// 適用済み
					if (null != date) {
						// 登録日が新しい
						if (latestDate.compareTo(date) < 0) {
							latestDate = date;
							String inputDate = this.support.convertViewDateFormat(
									historyBean.getRegistrationDate(), TriDateUtils.getViewDateFormat() );
							viewBean.setDrmsInputDate(inputDate);
							String applyDate = this.support.convertViewDateFormat(
									historyBean.getDealDate(),  TriDateUtils.getViewDateFormat() );
							viewBean.setApplyDate(applyDate);
							if (null != historyBean.getStatus()) {
								String status = historyBean.getStatus();
								if ( DrmsApplyResultId.SUCCESS.getValue().equals( status ) ) {
									viewBean.setDrmsStatus( DrmsStatusId.APPLY .getValue() ) ;
								} else if ( DrmsApplyResultId.FAIL.getValue().equals( status ) ) {
									viewBean.setDrmsStatus( DrmsStatusId.APPLY_ERROR.getValue() ) ;
								}
							}
						}
					}
				}
			}
		}

		return viewBean;

	}


	/**
	 *
	 * @param envNo
	 */
	private void outputCommercialShell( FlowRelDistributeListServiceBean paramBean, String envNo ) {

		List<Object> paramList = new CopyOnWriteArrayList<Object>() ;

		CommercialShellBean shellBean = new CommercialShellBean();
		shellBean.setShellId(DmDesignEntryKeyByCommercialShell.shellRegHistory);
		BldEnvCondition condition = new BldEnvCondition();
		condition.setBldEnvId(envNo);
		IBldEnvEntity envEntity = this.support.getBmFinderSupport().getBldEnvDao().find( condition.getCondition() ).get(0);

		String shellFileName = this.support.makeShellFileName( CommercialShellID.SHELL_REG_HISTORY , envEntity.getBldEnvId() ) ;
		shellBean.setShellFileName(shellFileName);
		Properties properties = new Properties() ;
		properties.put( RelDistributeCommercialShellPropertyDefineID.GROUP_NAME,
				envEntity.getBldEnvNm() ) ;
		shellBean.setProperties(properties);
		paramList.add(shellBean);

		IServiceDto<IGeneralServiceBean> innerServiceDto = new ServiceDto<IGeneralServiceBean>()
				.setServiceBean( paramBean )
				.setParamList( paramList );

		for ( IDomain<IGeneralServiceBean> action : makeShellActions ) {
			action.execute( innerServiceDto );
		}
	}
	/**
	 *
	 * @param selectedPageNo
	 * @param maxPageNumber
	 * @return
	 */
	private IBldEnvEntity[] getRelEnvEntity( int selectedPageNo , int maxPageNumber ) {

		IJdbcCondition envCondition	= DBSearchConditionAddonUtil.getActivelRelEnvCondition();
		ISqlSort envSort			= DBSearchSortAddonUtil.getRelEnvSortFromDesignDefineByRelEntryConfigurationSelect();

		IEntityLimit<IBldEnvEntity> envEntityLimit =
			this.support.getBmFinderSupport().getBldEnvDao().find( envCondition.getCondition(), envSort, selectedPageNo, maxPageNumber );

		return envEntityLimit.getEntities().toArray(new IBldEnvEntity[0]);
	}
	/**
	 *
	 * @param envNo
	 * @param lotNoArrays アクセス可能なロット番号の配列
	 * @param distributeStatus
	 * @param selectedPageNo
	 * @param maxPageNumber
	 * @return
	 */
	private IEntityLimit<IRpEntity> getRelEntityLimit(
			String envNo,
			String[] lotNoArrays,
			String[] distributeStatus,
			int selectedPageNo,
			int maxPageNumber ) {


		IJdbcCondition condition =
			DBSearchConditionAddonUtil.getRpConditionByEnvNoRpIds( envNo, getRpIdsFromDisbuteStatus( distributeStatus ) );

		//アクセス可能なロット番号
		{
			if ( TriStringUtils.isEmpty( lotNoArrays ) ) {
				//ダミーを設定して、全件検索を防止する。
				((RpCondition)condition).setLotIds( new String[]{ "" });
			} else {

				((RpCondition)condition).setLotIds( lotNoArrays );
			}
		}

		ISqlSort sort =  DBSearchSortAddonUtil.getRelSortFromDesignDefineByRelDistributeList();

		IEntityLimit<IRpEntity> limit =
			this.support.getRpDao().find( condition.getCondition(), sort, selectedPageNo, maxPageNumber );

		return limit ;
	}

	private String[] getRpIdsFromDisbuteStatus( String[] distributeStatus ) {
		DmDoCondition condition = new DmDoCondition();
		condition.setProcStsIds( distributeStatus );
		List<IDmDoEntity> dmDoEntityList = this.support.getDmDoDao().find( condition.getCondition() );
		List<String> rpIds = new ArrayList<String>();
		for ( IDmDoEntity entity : dmDoEntityList ) {
			rpIds.add( entity.getRpId() );
		}
		return rpIds.toArray( new String[0] );
	}

}
