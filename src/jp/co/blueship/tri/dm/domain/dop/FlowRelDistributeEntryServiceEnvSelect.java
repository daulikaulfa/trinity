package jp.co.blueship.tri.dm.domain.dop;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotCondition;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.BpCondition;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.dm.constants.RelDistributeScreenID;
import jp.co.blueship.tri.dm.domain.dop.dto.FlowRelDistributeEntryServiceBean;
import jp.co.blueship.tri.dm.support.FlowRelDistributeEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.ItemCheckAddonUtils;
import jp.co.blueship.tri.fw.cmn.utils.RelCommonAddonUtil;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.dao.orm.DBSearchConditionAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.DBSearchSortAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.msg.DmMessageId;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.rm.RmViewInfoAddonUtils;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.ConfigurationSelectBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.ConfigurationViewBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.LotSelectBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.LotViewBean;

/**
 * リリース配布・ロット・環境選択画面の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */

public class FlowRelDistributeEntryServiceEnvSelect implements IDomain<FlowRelDistributeEntryServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowRelDistributeEditSupport support = null;
	public void setSupport(FlowRelDistributeEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowRelDistributeEntryServiceBean> execute( IServiceDto<FlowRelDistributeEntryServiceBean> serviceDto ) {

		FlowRelDistributeEntryServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID	= paramBean.getReferer();
			String forwordID	= paramBean.getForward();
			String screenType	= paramBean.getScreenType();

			if( true != RelDistributeScreenID.ENV_SELECT.equals( refererID ) &&
					true != RelDistributeScreenID.ENV_SELECT.equals( forwordID ) ){
				return serviceDto;
			}

			if( RelDistributeScreenID.ENV_SELECT.equals( refererID ) ) {

				String selectedConfNo = paramBean.getSelectedEnvNo();
				String selectedLotNo = paramBean.getSelectedLotNo();

				if ( ScreenType.next.equals( screenType ) &&
						true != RelDistributeScreenID.ENV_SELECT.equals( forwordID ) ) {
					ItemCheckAddonUtils.checkConfNo( selectedConfNo );
					ItemCheckAddonUtils.checkLotNo( selectedLotNo );

					// グループの存在チェック
					ILotDto dto = this.support.getAmFinderSupport().findLotDto( selectedLotNo );
					List<IGrpUserLnkEntity> groupUsers	= support.getUmFinderSupport().findGrpUserLnkByUserId( paramBean.getUserId() );

					RelCommonAddonUtil.checkAccessableGroup( dto, this.support.getUmFinderSupport().getGrpDao(), groupUsers );
				}

				paramBean.setSelectedEnvNo( selectedConfNo );
				paramBean.setSelectedLotNo( selectedLotNo );
			}


			if( RelDistributeScreenID.ENV_SELECT.equals( forwordID ) ) {

				if ( ! ScreenType.bussinessException.equals( screenType ) ) {

					// 環境一覧の取得
					IBldEnvEntity[] envAllEntities = this.getRelEnvEntity() ;
					if ( true == TriStringUtils.isEmpty( envAllEntities ) ) {
						throw new BusinessException( RmMessageId.RM001020E );
					}

					// 活動中の全ロットを取得
					List<ILotEntity> accessableLotList = null;
					{
						LotCondition lotCondition = (LotCondition)DBSearchConditionAddonUtil.getActiveLotCondition();
						List<String> disableLinkLotNumbers	= new ArrayList<String>();

						accessableLotList =
							this.support.getAmFinderSupport().getAccessableLotNumbers( lotCondition, null, paramBean, false, disableLinkLotNumbers, true );
					}
					List<ILotDto> lotDtoEntities = new ArrayList<ILotDto>();
					for ( ILotEntity entity : accessableLotList ) {
						ILotDto lotDto = support.getAmFinderSupport().findLotDto( entity );
						lotDtoEntities.add(lotDto);
					}

					// 「リリース可能なビルドパッケージ」を持つロット
					String[] lotNoArray = this.getLotNoByReleasableRelUnit( this.support.getAmFinderSupport().convertToLotNo(accessableLotList) );
					ILotEntity[] pjtLotEntityArray = this.getPjtLotEntityByReleasableRelUnit( lotNoArray ) ;

					//リリース環境選択一覧の設定
					IBldEnvEntity[] envEntities =
						this.support.getAmFinderSupport().getAccessableRelEnvEntity( envAllEntities, lotDtoEntities );
					{
						List<ConfigurationViewBean> confViewBeanList = this.support.makeConfigurationViewBeanList( envEntities ) ;
						ConfigurationSelectBean confSelectBean = paramBean.getConfSelectBean();
						confSelectBean.setConfViewBeanList	( confViewBeanList );
		//				confSelectBean.setPageInfoView		(
		//						AddonUtil.convertPageNoInfo( new PageNoInfo(), envEntityLimit.getLimit() ));
		//				confSelectBean.setSelectPageNo		( selectedPageNo );

						paramBean.setConfSelectBean( confSelectBean );
					}

					if ( true == TriStringUtils.isEmpty( pjtLotEntityArray ) ) {
						throw new BusinessException( BmMessageId.BM001000E );
					}
					List<LotViewBean> lotViewBeanList = this.makeLotViewBeanList( pjtLotEntityArray ) ;

					LotSelectBean lotSelectBean = paramBean.getLotSelectBean();
					lotSelectBean.setLotViewBeanList	( lotViewBeanList );
					paramBean.setLotSelectBean( lotSelectBean );
				}
			}

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( DmMessageId.DM005017S , e );
		}
	}

	/**
	 * リリース可能なビルドパッケージを持つロットのロット番号取得
	 * @param lotNoArrays アクセス可能なロット番号の配列
	 * @return ロット番号
	 */
	private String[] getLotNoByReleasableRelUnit( String[] lotNoArrays ) {

		// リリース登録可能なビルドパッケージ
		IJdbcCondition condition =
			DBSearchConditionAddonUtil.getBuildConditionByReleasableRelUnit();

		//アクセス可能なロット番号
		{
			if ( TriStringUtils.isEmpty( lotNoArrays ) ) {
				//ダミーを設定して、全件検索を防止する。
				((BpCondition)condition).setLotIds( new String[]{ "" });
			} else {

				((BpCondition)condition).setLotIds( lotNoArrays );
			}
		}

		IEntityLimit<IBpEntity> buildEntityLimit = this.support.getBmFinderSupport().getBpDao().find( condition.getCondition(), null, 1, 0 );

		// ロット番号を抽出
		HashSet<String> lotNoSet = new HashSet<String>();
		for ( IBpEntity entity : buildEntityLimit.getEntities() ) {
			lotNoSet.add( entity.getLotId() );
		}

		return (String[])lotNoSet.toArray( new String[0] );
	}
	/**
	 *
	 * @return
	 */
	private IBldEnvEntity[] getRelEnvEntity() {

		IJdbcCondition condition	= DBSearchConditionAddonUtil.getActivelRelEnvCondition();
		ISqlSort sort			= DBSearchSortAddonUtil.getRelEnvSortFromDesignDefineByRelEntryConfigurationSelect();

		IEntityLimit<IBldEnvEntity> envEntityLimit =
			this.support.getBmFinderSupport().getBldEnvDao().find( condition.getCondition(), sort, 1 , 0 );

		return envEntityLimit.getEntities().toArray(new IBldEnvEntity[0]);
	}
	/**
	 * 「リリース可能なビルドパッケージ」を持つロット情報を取得する<br>
	 * @param lotNoArray ロット番号の配列
	 * @return 「リリース可能なビルドパッケージ」を持つロット情報の配列
	 */
	private ILotEntity[] getPjtLotEntityByReleasableRelUnit( String[] lotNoArray ) {
		IJdbcCondition lotCondition	= DBSearchConditionAddonUtil.getActiveLotCondition( lotNoArray );
		ISqlSort lotSort			= DBSearchSortAddonUtil.getPjtLotSortFromDesignDefineByRelEntityLotSelect();

		int selectedPageNo	= 1;
		int maxPageNumber	= 0;
		IEntityLimit<ILotEntity> pjtLotEntityLimit =
			this.support.getAmFinderSupport().getLotDao().find( lotCondition.getCondition(), lotSort, selectedPageNo, maxPageNumber );

		return pjtLotEntityLimit.getEntities().toArray(new ILotEntity[0]) ;
	}
	/**
	 *
	 * @param pjtLotEntityArray
	 * @return
	 */
	private List<LotViewBean> makeLotViewBeanList( ILotEntity[] pjtLotEntityArray ) {
		List<LotViewBean> lotViewBeanList = new ArrayList<LotViewBean>();

		for ( ILotEntity entity : pjtLotEntityArray ) {
			LotViewBean viewBean = new LotViewBean();
			RmViewInfoAddonUtils.setLotViewBeanPjtLotEntity( viewBean, entity, this.support.getBmFinderSupport().findBldSrvEntityByController().getBldSrvId() );
			lotViewBeanList.add( viewBean );
		}
		return lotViewBeanList ;
	}
}
