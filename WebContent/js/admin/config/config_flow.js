/* ▼[F] Flow
/*--------------------------------------------------------------------------*/
var errMassage = "添付ファイルが不正です。";


function submitUcfSectionModify(formId,
							    sectionId, 
							    sectionIdValue
							   ) {
	document.getElementById(sectionId).value = sectionIdValue;
	submit(formId);
}

function submitUcfSectionDelete(formId,
							    sectionId, 
							    sectionIdValue
							   ) {
	document.getElementById(sectionId).value = sectionIdValue;
	submit(formId);
}


function submitUcfActionEntry(formID,
								actionId,actionIdValue,
								actionName,actionNameValue,
								contents,contentsValue
							){
	document.getElementById(actionId).value = actionIdValue;
	document.getElementById(actionName).value = actionNameValue;
	document.getElementById(contens).value = contentsValue;
}


function submitUcfActionModify(formId,
						       actionId, 
							   actionIdValue
							   ){
	document.getElementById(actionId).value = actionIdValue;
	submit(formId)
}

function submitUcfActionDelete(formId,
							   actionId,
							   actionIdValue
								){
	document.getElementById(actionId).value = actionIdValue;
	submit(formId)	
}

function submitUcfActionLockModify(formId,
							    actionId, 
							    actionIdValue
									){
	document.getElementById(actionId).value = actionIdValue;
	submit(formId);
}

function submitUcfRoleActionDelete(formId,
							    roleId, 
							    roleIdValue
									){
	document.getElementById(roleId).value = roleIdValue;
	submit(formId);
}

function submitUcfRoleActionModify(formId,
							    roleId, 
							    roleIdValue
									){
	document.getElementById(roleId).value = roleIdValue;
	submit(formId);
}

function submitUcfRoleDelete(formId,
							    roleId, 
							    roleIdValue
							   ) {
	document.getElementById(roleId).value = roleIdValue;
	submit(formId);
}

function submitUcfRoleModify(formId,
							    roleId, 
							    roleIdValue
							   ) {
	document.getElementById(roleId).value = roleIdValue;
	submit(formId);
}
						
function submitUcfUserModify(formId,
							 userId, 
							 userIdValue
							 ) {
	document.getElementById(userId).value = userIdValue;
	submit(formId);
}

function submitUcfUserDelete(formId,
							 userId, 
							 userIdValue
							 ) {
	document.getElementById(userId).value = userIdValue;
	submit(formId);
}

function submitUcfUserRoleModify(formId,
							     userId, 
							     userIdValue
							    ) {
	document.getElementById(userId).value = userIdValue;
	submit(formId);
}

function submitUcfUserRoleDelete(formId,
							     userId, 
							     userIdValue
							    ) {
	document.getElementById(userId).value = userIdValue;
	submit(formId);
}

function submitUcfGroupUserDelete(formId,
							    groupId, 
							    groupIdValue
									){
	document.getElementById(groupId).value = groupIdValue;
	submit(formId);
}

function submitUcfGroupUserModify(formId,
							    groupId, 
							    groupIdValue
									){
	document.getElementById(groupId).value = groupIdValue;
	submit(formId);
}

function submitUcfGroupDelete(formId,
							    groupId, 
							    groupIdValue
							   ) {
	document.getElementById(groupId).value = groupIdValue;
	submit(formId);
}

function submitUcfGroupModify(formId,
							    groupId, 
							    groupIdValue
							   ) {
	document.getElementById(groupId).value = groupIdValue;
	submit(formId);
}

function submitUcfProductModify(formId,
							    productIdKey, 
							    productIdValue,
							    computerNameKey, 
							    computerNameValue
							   ) {
	document.getElementById(productIdKey).value = productIdValue;
	document.getElementById(computerNameKey).value = computerNameValue;
	submit(formId);
}

function submitUcfProductDelete(formId,
							    productIdKey, 
							    productIdValue,
							    computerNameKey, 
							    computerNameValue
							   ) {
	document.getElementById(productIdKey).value = productIdValue;
	document.getElementById(computerNameKey).value = computerNameValue;
	submit(formId);
}
