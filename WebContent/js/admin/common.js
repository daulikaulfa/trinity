/*
/*--------------------------------------------------------------------------*/

//-----------------------------------------------
//--▼[F] getContextName--
//-----------------------------------------------
  function getContextName() {
  	var locationPath = window.location.pathname;
  	var slashIndex = locationPath.indexOf("/", 1);
  	var contextName = locationPath.substring(1, slashIndex);

  	return "/" + contextName;
  }

//-----------------------------------------------

//-----------------------------------------------
//--▼[F] getImageDirPath--
//-----------------------------------------------
  function getImageDirPath() {
  	return getContextName() + "/images";
  }

//-----------------------------------------------
//--▼[F] getImageDirPath--
//-----------------------------------------------
  function getSdImageDirPath() {
  	return getContextName() + "/images/desk";
  }

//-----------------------------------------------
//--▼[F] getImageDirPath--
//-----------------------------------------------
  function getIncImageDirPath() {
  	return getContextName() + "/images/incident";
  }

//-----------------------------------------------
//--▼[F] getImageDirPath--
//-----------------------------------------------
  function getProImageDirPath() {
  	return getContextName() + "/images/problem";
  }

//-----------------------------------------------
//--▼[F] getImageDirPath--
//-----------------------------------------------
  function getConfigImageDirPath() {
  	return getContextName() + "/images/config";
  }

//-----------------------------------------------
//--▼[F] adjustMessageList--
//-----------------------------------------------
  function adjustMessageList(obj) {
  	var message = "";
		var messageIdList = obj.messageList;
		var messageContentsList = obj.messageContents;
  	for(i=0;i<messageIdList.length;i++){
			message += messageIdList[i] +":"+ messageContentsList[i]+"<br>";
		}
  	return message;
  }


function submitConfirm(formId, message){
	if(nidooshi()){
		return;
	}
	if(ConfirmBox(message)){
    	document.getElementById(formId).submit();
    }
}

function submitPageNo(formId, hiddenId, selectedPageNo){
	if(nidooshi()){
		return;
	}
    document.getElementById(hiddenId).value = selectedPageNo;
    submit(formId);
}


var submitFlg = false;

//-----------------------------------------------
// 画面識別IDを設定してから送信を行う
// ※このメソッドを直接呼び出すと、通常のsubmitがキックされて
// 当メソッドが呼び出されない場合があります。
// 意図しない動作を避けるため、このメソッドではなく、submitServiceを
// 利用して下さい。
//-----------------------------------------------
function submit(formId){
	var formObj = document.getElementById(formId);

	var saveId = document.getElementById('trinityWindowsId');
	if ( saveId ) {
		var submitId = formObj.windowsId;
		if ( !submitId ) {
			var element = document.createElement('input');
			element.setAttribute('type', 'hidden');
			element.setAttribute('id', 'windowsId');
			element.setAttribute('name', 'windowsId');

			formObj.appendChild( element );
		}
		formObj.windowsId.value = saveId.value;
	}

	if(!submitFlg){
		submitFlg = true;
		formObj.submit();
	}
  	setTimeout("resetFlg()",3000);
}

//-----------------------------------------------
// 画面識別IDを設定してから送信を行う
//-----------------------------------------------
function submitService(formId){
	if(nidooshi()){
		return;
	}
	submit(formId);
}

//-----------------------------------------------
// actionを付け替えてから、送信を行う
//-----------------------------------------------
function submitChangeAction(formId, actionFormId){
	if(nidooshi()){
		return;
	}
 	var formObj = document.getElementById(formId);

	formObj.action = document.getElementById(actionFormId).action;
	submit(formId);
}

//-----------------------------------------------
// 画面識別IDを設定してから送信を行う
//-----------------------------------------------
function submitDialog(formId){
	if(nidooshi()){
		return;
	}
	var formObj = document.getElementById(formId);

	var submitId = formObj.windowsId;
	if ( !submitId ) {
		var element = document.createElement('input');
		element.setAttribute('type', 'hidden');
		element.setAttribute('id', 'windowsId');
		element.setAttribute('name', 'windowsId');

		formObj.appendChild( element );
	}
	formObj.windowsId.value = 'dialog';

	if(!submitFlg){
		submitFlg = true;
		formObj.submit();
	}
  	setTimeout("resetFlg()",3000);
}

//-----------------------------------------------
// 画面識別IDを設定してから送信を行う
//-----------------------------------------------
function submitDownload(formId){
	if(nidooshi()){
		return;
	}
	var formObj = document.getElementById(formId);

	var submitId = formObj.windowsId;
	if ( !submitId ) {
		var element = document.createElement('input');
		element.setAttribute('type', 'hidden');
		element.setAttribute('id', 'windowsId');
		element.setAttribute('name', 'windowsId');

		formObj.appendChild( element );
	}
	formObj.windowsId.value = 'download';

	if(!submitFlg){
		submitFlg = true;
		formObj.submit();
	}
  	setTimeout("resetFlg()",3000);
}

var win = new Array;
var winDialog;
var winIndex = 0;

function openSubWindow(formId, key){

	winIndex++;

	var saveId = document.getElementById('trinityWindowsId');
	if ( saveId ) {
		saveId.value = winIndex;
	}

	var width = 1024;
	var height = 900;
	var left = (screen.width - width) / 2;
	var top = 0;

	win[winIndex] = window.open('about:blank', winIndex, 'top=' + top + ',width=' + width + ',height=' + height + ',left=' + left + ',toolbar=no,location=no,directories=no,menubar=no,resizable=yes,scrollbars=yes,status=yes');
	win[winIndex].focus();

	document.getElementById(formId).target = winIndex;
	win[winIndex].focus();
	setTimeout("submit('" + formId + "')",1000);
}

function openSubWindowDialog(formId){
	var key = 'dialog';

	var width = 1024;
	var height = 900;
	var left = (screen.width - width) / 2;
	var top = 0;

	winDialog = window.open('about:blank', key, 'top=' + top + ',width=' + width + ',height=' + height + ',left=' + left + ',toolbar=no,location=no,directories=no,menubar=no,resizable=yes,scrollbars=yes,status=yes');
	winDialog.focus();
	document.getElementById(formId).target = key;
	setTimeout("submitDialog('" + formId + "')",1000);
}

function openSubWindow4Download(formId, url) {

	var width = 500;
	var height = 300;
	var left = (screen.width - width) / 2;
	var top = (screen.height - height) / 2;

	winDialog = window.open(url,'_download','top=' + top + ',left=' + left + ',width=' + width + ',height=' + height + ',toolbar=no,location=no,directories=no,menubar=no,resizable=no,scrollbars=no,status=no');
	winDialog.focus();
	document.getElementById(formId).target = '_download';
	setTimeout("submitDownload('" + formId + "')",1000);
}

function closeSubWindow(){
	var key;
    for (key in win) {
		if (win[key]) {
 			if (win[key].closed==false){
				win[key].close();
			}
		}
	}
}

function submitSelectAppNo(formId, hiddenId, selectAppNo){
	if(nidooshi()){
		return;
	}
    document.getElementById(hiddenId).value = selectAppNo;
    submit(formId);
}

function submitValidateCheck(formId, screenName){
	if(nidooshi()){
		return;
	}
	submit(formId);
}

function submitValidateConfirm(formId, message, scrname){
	if(nidooshi()){
		return;
	}
	var result = valiedateCheckSequence(scrname);

	if (!result){
		return;
	}
	if(ConfirmBox(message)){
		submit(formId, scrname);
    }
}

function setRequestParameter(selectList, dstIdList){
	var val = selectList.value;
	for (i = 0; i < selectList.length; i++) {
		if (selectList[i].checked) {
		val = selectList[i].value;
		}
	}
	for(var x=0;x<dstIdList.length;x++){
		document.all(dstIdList[x]).value = val;
//		alert(dstIdList[x] + ":" + document.all(dstIdList[x]).value);
	}
}

function defaultSet4OptionTag(optionId, targetValue) {
	if ( ! targetValue )
		return;

	var option = document.getElementById(optionId);
	var select = option.parentNode;
	for (i = 0; i < select.length; i++) {
		var element = select.options[i].value;
		if(element == targetValue){
			select.selectedIndex = i;
			break;
		}
	}
}

function getSelectedOptionTag(optionId) {
	var option = document.getElementById(optionId);
	var select = option.parentNode;
	for (i = 0; i < select.length; i++) {
		var element = select.options[i];
		if(true == element.selected){
			return element;
		}
	}
	return null;
}

function defaultSet4CheckBox(checkboxId, targetValue) {
	var checkbox = document.getElementsByName(checkboxId);
	var size = targetValue.length;
	var temp = targetValue.substring(1,size-1);
	var array = temp.split(", ");
	for (i = 0; i < checkbox.length; i++) {
		for (j = 0; j < array.length; j++) {
			if (array[j] == checkbox[i].value) {
				checkbox[i].checked = true;
				continue;
			}
		}
	}
}

function defaultSet4SingleCheckBox(checkboxName, targetValue) {
	var checkbox = document.getElementsByName(checkboxName);
	if (targetValue == 'on') {
		checkbox[0].checked = true;
	}
}

function defaultSet4Year(selectId, targetValue) {
	var select = document.getElementById(selectId);
	var year = new Date().getFullYear();
	var option = document.createElement("option");
	option.innerHTML = "----";
	option.value = "";
	select.appendChild(option);
	for (i = 1; i < 12; i++) {
		var setValue = year - 6 + i;
		var option = document.createElement("option");
		option.innerHTML = setValue;
		option.value = setValue;
		if (setValue == targetValue) {
			option.selected = true;
		}
		select.appendChild(option);
	}
	select.style.width = '55px'
}

function defaultSet4Month(selectId, targetValue) {
	var select = document.getElementById(selectId);
	var option = document.createElement("option");
	option.innerHTML = "--";
	option.value = "";
	select.appendChild(option);
	for (i = 1; i < 13; i++) {
		var setValue;
		if (i < 10) {
			setValue = "0" + i;
		} else {
			setValue = i;
		}
		var option = document.createElement("option");
		option.innerHTML = setValue;
		option.value = setValue;
		if (setValue == targetValue) {
			option.selected = true;
		}
		select.appendChild(option);
	}
	select.style.width = '40px'
}

function defaultSet4Day(selectId, targetValue) {
	var select = document.getElementById(selectId);
	var option = document.createElement("option");
	option.innerHTML = "--";
	option.value = "";
	select.appendChild(option);
	for (i = 1; i < 32; i++) {
		var setValue;
		if (i < 10) {
			setValue = "0" + i;
		} else {
			setValue = i;
		}
		var option = document.createElement("option");
		option.innerHTML = setValue;
		option.value = setValue;
		if (setValue == targetValue) {
			option.selected = true;
		}
		select.appendChild(option);
	}
	select.style.width = '40px'
}

function changeDisplay(divisionId, messageId) {

	if ( document.getElementById(divisionId).style.display == 'none' ){
		document.getElementById(divisionId).style.display = 'block';
		document.getElementById(messageId).innerHTML = '詳細非表示';
	} else {
		document.getElementById(divisionId).style.display = 'none';
		document.getElementById(messageId).innerHTML = '詳細表示';
	}
}

function resetFlg(){
	submitFlg = false;
}

function AllCheckBoxOn(checkName){
	var checkBox = document.getElementsByName(checkName);
	if (checkBox.checked == undefined) {
		// 複数の場合
		for (i=0; i<checkBox.length; i++) {
			checkBox[i].checked=true;
		}
    } else {
		// 単数の場合
		checkBox.checked=true;
    }
}

function AllCheckBoxOff(checkName){
	var checkBox = document.getElementsByName(checkName);
	if (checkBox.checked == undefined) {
		// 複数の場合
		for (i=0; i<checkBox.length; i++) {

			if (!checkBox[i].disabled) {
				checkBox[i].checked=false;
			}
		}
    } else {
		// 単数の場合
		if (!checkBox.disabled) {
			checkBox.checked=false;
		}
    }
}

/**
 * window の Load イベントを取得する。
 */
window.onload = window_Load;

function window_Load() {
  var i;

  // 全リンクのクリックイベントを submittableObject_Click で取得する。
  for (i = 0; i < document.links.length; i ++) {
    var item = document.links[i]
    Object.Aspect.around(item, "onclick", checkLoading);
  }

  // 全ボタンのクリックイベントを submittableObject_Click で取得する。
  for (i = 0; i < document.forms[0].elements.length; i ++) {
    var item = document.forms[0].elements[i]
    if (item.type == "button" ||
      item.type == "submit" ||
      item.type == "reset") {
      Object.Aspect.around(item, "onclick", checkLoading);

    }
  }

  return true;
}

/**
 *
 * 2度押し抑止アスペクト
 */
var checkLoading = function(invocation) {
	if(nidooshi()){
		return;
	}

  return invocation.proceed();
}

function nidooshi() {
	if (isDocumentLoading()) {
    alert("２度押しされました。処理中ですので少々お待ちください。");
    return true;
  }
}
/**
 *
 * 画面描画が終わったかどうか
 */
function isDocumentLoading() {
  return (document.readyState != null &&
          document.readyState != "complete");
}

/**
 *
 * アスペクト用
 */
Object.Aspect = {
  _around: function(target, methodName, aspect) {
    var method = target[methodName];
    target[methodName] = function() {
      var invocation = {
        "target" : this,
        "method" : method,
        "methodName" : methodName,
        "arguments" : arguments,
        "proceed" : function() {
          if (!method) {
            return true;
          }
          return method.apply(target, this.arguments);
        }
      };
      return aspect.apply(null, [invocation]);
    };
  },
  around: function(target, methodName, aspect) {
    this._around(target, methodName, aspect);
  }
}

/**
 *
 * 印刷ボタン用
 */
function printPage(){
	window.print();
	return false;
}

/**
 *
 * 不要キーの抑止
 */
window.document.onkeydown = function() {
	var KEY_BACK   = 8;
	var KEY_ESCAPE = 27;
	var KEY_LEFT   = 37;
	var KEY_RIGHT  = 39;
	var KEY_N      = 78;
	var KEY_R      = 82;
	var KEY_F5     = 116;
	var KEY_F11    = 122;
	var KEY_F12    = 123;
	var KEY_ENTER  = 13;

	// [1]テキスト入力域でのCtrl,Shift,BackSpaceは抑制しない
	// [2]セレクト領域でのCtrl,Shiftは抑制しない
	switch (event.srcElement.tagName) {
		case 'INPUT':
			var type = event.srcElement.type;
			if (type == 'text' || type == 'file' || type == 'password') {
				if (event.keyCode == KEY_BACK) return true;
				if (event.ctrlKey) return true;
				if (event.shiftKey) return true;
			}
			break;
		case 'TEXTAREA':
			if ( !event.srcElement.readOnly ) {
				if (event.keyCode == KEY_BACK) return true;
			}
			break;
		case 'SELECT':
			if( event.ctrlKey ) return true ;
			if( event.shiftKey ) return true ;
			break ;
	}

	// 押下キーの検証。無効化対象キーか否かを判定
	var nullifyKey = false;
	switch (event.keyCode) {
		case KEY_LEFT:
		case KEY_RIGHT:
			if (event.altKey) nullifyKey = true;
			break;
		case KEY_R:
        case KEY_N:
			if (event.ctrlKey) nullifyKey = true;
			break;
		case KEY_ENTER:
			if (event.ctrlKey) nullifyKey = true;
			if (event.shiftKey) nullifyKey = true;
			break;
		case KEY_BACK:
		case KEY_ESCAPE:
		case KEY_F5:
		case KEY_F11:
		case KEY_F12:
			nullifyKey = true;
			break;
	}

	// 無効化対象キーでなければtrueを返す
	if (!nullifyKey) return true;

	// 以下、無効化対象キーの処理
	if (event.altKey){
		// 検証の結果、Altとの組み合わせ（少なくとも左右矢印キーとの組み合わせ）の場合、
		// keyCodeにゼロを代入すると動作しないことが判明。そのためここでは何もしない。
		// ただしalertダイアログを出すなど、イベントバブルがキャンセルされるような動作を
		// 挿入すれば、keyCodeにゼロを代入しても可。
	} else {
		// Functionキーの押下を無効にする場合、keyCodeにゼロを代入しないと無効にならないので。
		event.keyCode = 0;
	}

	// キー押下による本来のイベントの発生を抑制
	event.returnValue = false;
	return false;
}

/**
 * 不要マウスイベントの抑止
 *
 **/
window.document.onmousedown = function() {

	// [1]セレクト領域でのCtrl,Shiftは抑制しない
	switch (event.srcElement.tagName) {
		case 'SELECT':
			if( event.ctrlKey ) return true ;
			if( event.shiftKey ) return true ;
			break ;
	}

	if(event.button != 1){
		alert("左ボタン以外のクリックは禁止です");
		event.button == null;
		return false ;
	}
	if (event.ctrlKey) {
		alert("ctrlキーを押してのクリックは無効です");
		return false ;

	}
	if (event.shiftKey ) {
		alert("shiftキーを押してのクリックは無効です");
		return false ;
	}
}

/**
 * 不要マウスイベントの抑止
 * (ドラッグ禁止)
 **/
window.document.ondragstart = function() {
	return false;
}

/**
 * コンテキストメニューの抑止
 *
 **/
window.document.oncontextmenu = function() {
	return false ;
}
