//-----------------------------------------------

//-----------------------------------------------
//--▼[F] sleep--
//-----------------------------------------------
  function sleep(time) {
    id = window.setTimeout("sleep()", time);
    window.clearTimeout(id);
  }

  function sleepWithFunc(callBack, time) {
    setTimeout(callBack, time);
  }

//-----------------------------------------------

//-----------------------------------------------
//--▼[F] reload--
//-----------------------------------------------
  var intervalId = 0;
  function reload(flow, interval) {
    reloadTarget(flow);
    intervalId = window.setInterval("reloadTarget('" + flow + "')", interval);
	//intervalId = window.setTimeout("reloadTarget('" + flow + "')", interval);
  }

//-----------------------------------------------
//--▼[F] reloadTarget--
//-----------------------------------------------
  function reloadTarget(flow) {

    //dwr.engine.setActiveReverseAjax(true);

    if('FlowChaRtnEntryBtn' == flow){
      dwr.engine.setErrorHandler(errh4Compile);

      var lotNoParam = dwr.util.getValue("lotNoParam");
      var applyNoParam = dwr.util.getValue("applyNoParam");
      var procIdParam = dwr.util.getValue("procIdParam");

      FlowChaRtnEntryBtn.start( lotNoParam , applyNoParam, procIdParam );
    }
  }

//-----------------------------------------------

//-----------------------------------------------
//--▼[F] DWR error handler--
//-----------------------------------------------
  function errh4Compile(message) {

	var regexp = message.match('^.*:', '');
	if(null != regexp)
      message = message.replace(regexp, '');
    else
      message = 'システム処理中に継続出来ないエラーが発生しました。：' + message;

    var tran = document.createElement('div');
    tran.innerHTML = message;
    if($('CompileMessage').hasChildNodes()){
      $('CompileMessage').removeChild($('CompileMessage').childNodes[0]);
    }
    $('CompileMessage').appendChild(tran);

    FlowChaRtnEntryBtn.invalidate();
	sleep(1000);
  }

//-----------------------------------------------
//-----------------------------------------------
//--▼[F] analyseCompileResult--
//-----------------------------------------------
  function analyseCompile(rtn,message) {
  }

//-----------------------------------------------
//--▼[F] analyseCompileResult--
//-----------------------------------------------
  function analyseCompileResult(rtn,message) {

    allCompleted = new Boolean(true);
  	var tmp = "";
  	var message = "";
  	var imageDirPath = getSeqImageDirPath() + "/";
  	var ext = ".gif";
  	var index1 = 0;
  	var index2 = 0;

  	if(rtn.imagePathList.length == 0){
		if(message == "") {
			message = '画像ファイルが取得できませんでした。';
		}

		var tran = document.createElement('div');
		tran.innerHTML = message;
		if($('CompileMessage').hasChildNodes()){
			$('CompileMessage').removeChild($('CompileMessage').childNodes[0]);
		}
		$('CompileMessage').appendChild(tran);
  		return;
  	}

  	rtn.imagePathList.each(
		function(obj){
			index1++;
			index2 = 0;
  			obj.each(
				function(obj){
					index2++;
					$('image' + index1 + index2).setAttribute("src", imageDirPath + obj);
				}
			);
		}
	);

//	if(rtn.buildStatusBeanList.length != 19){
//		allCompleted = false;
//	}
//	if(allCompleted) {
//		window.location.href = getContextName() + "/terminateBuild.do";
//	}

	if(message == "") {
		message = unescape('%u30D3%u30EB%u30C9%u3092%u5B9F%u884C%u4E2D%u3067%u3059%u3002');
	}

	var tran = document.createElement('div');
	tran.innerHTML = message;
	if($('CompileMessage').hasChildNodes()){
		$('CompileMessage').removeChild($('CompileMessage').childNodes[0]);
	}
	$('CompileMessage').appendChild(tran);
  }

//-----------------------------------------------

var dupAct = new Boolean(false);
var copyAct = new Boolean(false);
var extAct = new Boolean(false);
var pcmAct = new Boolean(false);
var charAct = new Boolean(false);
var lfAct = new Boolean(false);
var csAct = new Boolean(false);
//-----------------------------------------------
//--▼[F] analysePreCompileResultByExec--
//-----------------------------------------------
function analysePreCompileResultByExec(rtn){

	allCompleted = new Boolean(true);
 	var tmp = "";
 	var message = "";
 	var imageDirPath = getSeqImageDirPath() + "/";
 	var ext = ".gif";

 	var caAvailable = rtn.preCompileCallScriptAvailable;
 	if(! caAvailable) {
 		$('callScript').style.cssText = "display:none;";
 	}

 	var PROCESS_ACTIVE = 'ACTIVE';
 	var PROCESS_STOP = 'STOP';
 	var PROCESS_ERROR = 'ERROR';
	//PreCompileDoubleFileCheck//
	//processId=1:actionValidateReturnAssetDuplicate
 	var dupStatus = rtn.preCompileDuplicationCheckStatus;
		if(dupStatus == PROCESS_ACTIVE) {
			$('PreCompileDuplicationCheck').setAttribute("src", imageDirPath+"pci-nsch_act"+ext);
			allCompleted = false;
			dupAct = true;
		}
		if(dupStatus == PROCESS_STOP) {
			$('PreCompileDuplicationCheck').setAttribute("src", imageDirPath+"pci-nsch_die"+ext);
			if (!dupAct) allCompleted = false;
		}
		if(dupStatus == PROCESS_ERROR) {
			$('PreCompileDuplicationCheck').setAttribute("src", imageDirPath+"pci-nsch_err"+ext);
			FlowChaRtnEntryBtn.invalidate();
			sleep(1000);
		}
	//PreCompileReturnFileCopy//
	//processId=2:actionCopyToReturnAsset
	var copyStatus = rtn.preCompileReturnFileCopyStatus;
	if (dupAct) {
		if(copyStatus == PROCESS_ACTIVE) {
			$('PreCompileReturnFileCopy').setAttribute("src", imageDirPath+"pci-hfcp_act"+ext);
			allCompleted = false;
			copyAct = true;
		}
		if(copyStatus == PROCESS_STOP) {
			$('PreCompileReturnFileCopy').setAttribute("src", imageDirPath+"pci-hfcp_die"+ext);
			if (!copyAct) allCompleted = false;
		}
		if(copyStatus == PROCESS_ERROR) {
			$('PreCompileReturnFileCopy').setAttribute("src", imageDirPath+"pci-hfcp_err"+ext);
			allCompleted = false;
			FlowChaRtnEntryBtn.invalidate();
			sleep(1000);
		}
	}
	//PreCompileExtensionCheck//
	//processId=3:actionValidateReturnAssetFiles
	var extStatus = rtn.preCompileExtensionCheckStatus;
	if (dupAct && copyAct) {
		if(extStatus == PROCESS_ACTIVE) {
			$('PreCompileExtensionCheck').setAttribute("src", imageDirPath+"pci-kcch_act"+ext);
			allCompleted = false;
			extAct = true;
		}
		if(extStatus == PROCESS_STOP) {
			$('PreCompileExtensionCheck').setAttribute("src", imageDirPath+"pci-kcch_die"+ext);
			if (!extAct) allCompleted = false;
		}
		if(extStatus == PROCESS_ERROR) {
			$('PreCompileExtensionCheck').setAttribute("src", imageDirPath+"pci-kcch_err"+ext);
			allCompleted = false;
			FlowChaRtnEntryBtn.invalidate();
			sleep(1000);
		}
	}
	//PreCompileSyntaxCheck//
	//processId=4:actionPreCompiler
// 	var pcmStatus = rtn.preCompileSyntaxCheckStatus;
// 	if (dupAct && copyAct && extAct) {
//		if(pcmStatus == PROCESS_ACTIVE) {
//			$('PreCompileSyntaxCheck').setAttribute("src", imageDirPath+"pci-ckch_act"+ext);
//			allCompleted = false;
//			pcmAct = true;
//		}
//		if(pcmStatus == PROCESS_STOP) {
//			$('PreCompileSyntaxCheck').setAttribute("src", imageDirPath+"pci-ckch_die"+ext);
//			if (!pcmAct) allCompleted = false;
//		}
//		if(pcmStatus == PROCESS_ERROR) {
//			$('PreCompileSyntaxCheck').setAttribute("src", imageDirPath+"pci-ckch_err"+ext);
//			allCompleted = false;
//			FlowChaRtnEntryBtn.invalidate();
//			sleep(1000);
//		}
//	}
	//PreCompileCharsetCheck//
	//processId=5:actionPreCompiler
// 	var pcmStatus = rtn.preCompileCharsetCheckStatus;
// 	if (dupAct && copyAct && extAct) {
//		if(pcmStatus == PROCESS_ACTIVE) {
//			$('PreCompileCharsetCheck').setAttribute("src", imageDirPath+"pci-ccch_act"+ext);
//			allCompleted = false;
//			charAct = true;
//		}
//		if(pcmStatus == PROCESS_STOP) {
//			$('PreCompileCharsetCheck').setAttribute("src", imageDirPath+"pci-ccch_die"+ext);
//			if (!pcmAct) allCompleted = false;
//		}
//		if(pcmStatus == PROCESS_ERROR) {
//			$('PreCompileCharsetCheck').setAttribute("src", imageDirPath+"pci-ccch_err"+ext);
//			allCompleted = false;
//			FlowChaRtnEntryBtn.invalidate();
//			sleep(1000);
//		}
//	}
	//PreCompileLinefeedCheck//
	//processId=6:actionPreCompiler
// 	var pcmStatus = rtn.preCompileLinefeedCheckStatus;
// 	if (dupAct && copyAct && extAct && charAct) {
//		if(pcmStatus == PROCESS_ACTIVE) {
//			$('PreCompileLinefeedCheck').setAttribute("src", imageDirPath+"pci-lfch_act"+ext);
//			allCompleted = false;
//			lfAct = true;
//		}
//		if(pcmStatus == PROCESS_STOP) {
//			$('PreCompileLinefeedCheck').setAttribute("src", imageDirPath+"pci-lfch_die"+ext);
//			if (!pcmAct) allCompleted = false;
//		}
//		if(pcmStatus == PROCESS_ERROR) {
//			$('PreCompileLinefeedCheck').setAttribute("src", imageDirPath+"pci-lfch_err"+ext);
//			allCompleted = false;
//			FlowChaRtnEntryBtn.invalidate();
//			sleep(1000);
//		}
//	}
	//PreCompileCallScript//
	//processId=7:actionCallScriptReturnAsset
	var csStatus = rtn.preCompileCallScriptStatus;
	if (dupAct && copyAct && extAct && charAct && lfAct) {
		if(csStatus == PROCESS_ACTIVE) {
			$('PreCompileCallScript').setAttribute("src", imageDirPath+"pci-cscr_act"+ext);
			allCompleted = false;
			csAct = true;
		}
		if(csStatus == PROCESS_STOP) {
			$('PreCompileCallScript').setAttribute("src", imageDirPath+"pci-cscr_die"+ext);
			if (!csAct) allCompleted = false;
		}
		if(csStatus == PROCESS_ERROR) {
			$('PreCompileCallScript').setAttribute("src", imageDirPath+"pci-cscr_err"+ext);
			allCompleted = false;
			FlowChaRtnEntryBtn.invalidate();
			sleep(1000);
		}
	}

	message += rtn.preCompileMessage;
	if(allCompleted && rtn.completeStatus == '1') {
	  var curLoc = window.location.href;
	  if(null == curLoc.match(new RegExp(getContextName() + "/flowChaLibTop.do$"))) {
		  //返却申請画面対応。一旦はずす
		  submit('FlowChaLibRtnEntry');
//		  sleepWithFunc(function(){submit('FlowChaLibRtnEntry');}, 5000);
	  }
	}

	var tran = document.createElement('div');
	tran.innerHTML = message;
	if($('CompileMessage').hasChildNodes()){
		$('CompileMessage').removeChild($('CompileMessage').childNodes[0]);
	}
	$('CompileMessage').appendChild(tran);

 	if(! rtn.preCompileExecute){
		return;
 	}
 }

