/* ��[F] Flow
/*--------------------------------------------------------------------------*/

function submitSetForward(formId,forwardParamId,forwardParamValue){
	if(nidooshi()){
		return;
	}
	document.getElementById(forwardParamId).value = forwardParamValue;
    submit(formId);
}

function submitSetForwardAndScreenType(formId,forwardParamId,forwardParamValue,screenTypeId,screenTypeValue){
	if(nidooshi()){
		return;
	}
	document.getElementById(forwardParamId).value = forwardParamValue;
	document.getElementById(screenTypeId).value = screenTypeValue;
    submit(formId);
}

function submitApplyNo(formId,ApplyNoId,ApplyNoValue){
	if(nidooshi()){
		return;
	}
	document.getElementById(ApplyNoId).value = ApplyNoValue;
	submit(formId);
}

function submitApplyList(submitFormID,actionFormID){
	if(nidooshi()){
		return;
	}
	document.getElementById(submitFormID).action = document.getElementById(actionFormID).action;
	submit(submitFormID);
}

function submitForSelectDirectory(formId, selectDirectory){
	if(nidooshi()){
		return;
	}
	document.getElementById('selectDirectory').value = selectDirectory;
	submit(formId);
}

function submitLotDetailView(formId,
							 lotNoParamId,
							 lotNoParamValue
							 ) {
	if(nidooshi()){
		return;
	}
	document.getElementById(lotNoParamId).value = lotNoParamValue;
	submit(formId);
}

function submitLotModify(formId,
							 lotNoParamId,
							 lotNoParamValue
							 ) {
	if(nidooshi()){
		return;
	}
	document.getElementById(lotNoParamId).value = lotNoParamValue;
	submit(formId);
}

function submitLotModifyWithServerNo( formId, lotNoParamId, lotNoParamValue, serverNoParamId, serverNoParamValue ) {

	document.getElementById( serverNoParamId ).value = serverNoParamValue;
	submitLotModify( formId, lotNoParamId, lotNoParamValue );
}

function submitLotDetailViewWithServerNo( formId, lotNoParamId, lotNoParamValue, serverNoParamId, serverNoParamValue ) {

	document.getElementById( serverNoParamId ).value = serverNoParamValue;
	submitLotDetailView( formId, lotNoParamId, lotNoParamValue );
}

function submitApplyDetailView(formId,
							 applyNoParamId,
							 applyNoParamValue
							 ) {
	if(nidooshi()){
		return;
	}
	document.getElementById(applyNoParamId).value = applyNoParamValue;
	submit(formId);
}

function submitChaLibPjtEntry(formId,
							 lotNoParamId,
							 lotNoParamValue
							 ) {
	if(nidooshi()){
		return;
	}
	document.getElementById(lotNoParamId).value = lotNoParamValue;
	submit(formId);
}

function submitChaLibPjtConform(formId,
								actionFormId,
							 	changeCauseClassifyParamId,
							 	changeCauseClassifyId
							 ) {
	if(nidooshi()){
		return;
	}
 	var formObj = document.getElementById(formId);
 	var actionFormObj = document.getElementById(actionFormId);

	formObj.action = actionFormObj.action;
 	formObj.forward.value = actionFormObj.forward.value;
	formObj.screenType.value = actionFormObj.screenType.value;

 	if ( document.getElementById(changeCauseClassifyId) != null ) {
		document.getElementById(changeCauseClassifyParamId).value = getSelectedOptionTag(changeCauseClassifyId).value;
	}
	submit(formId);
}

function submitChaLibLotSelect(formId,
							 lotNoParamId, lotNoId
							 ) {
	if(nidooshi()){
		return;
	}
	if ( document.getElementById(lotNoId) != null ) {
		var formObj = document.getElementById(formId);
		if ( formObj ) {
			formObj.lotNo.value = getSelectedOptionTag(lotNoId).value;
		}
	}
	submit(formId);
}

function submitChaLibLotSelectWithServerNo( formId, lotNoParamId, lotNoId ) {

	if(nidooshi()){
		return;
	}

	if ( document.getElementById( lotNoId ) != null ) {
		var formObj = document.getElementById( formId );
		if ( formObj ) {

			var value = getSelectedOptionTag( lotNoId ).value;
			var values = value.split(",");

			formObj.lotNo.value = values[0];
			formObj.serverNo.value = values[1];
		}
	}
	submit(formId);
}

function submitChaLibServerSelect( formId, serverNoParamId, serverNoId ) {

	if(nidooshi()){
		return;
	}

	if ( document.getElementById( serverNoId ) != null ) {
		var formObj = document.getElementById( formId );
		if ( formObj ) {
			formObj.serverNo.value = getSelectedOptionTag( serverNoId ).value;
		}
	}
	submit( formId );
}

function submitChaLibPjtModify(formId,
							 pjtNoParamId,
							 pjtNoParamValue,
							 lotNoParamId,
							 lotNoParamValue
							 ) {
	if(nidooshi()){
		return;
	}
	document.getElementById(pjtNoParamId).value = pjtNoParamValue;
	document.getElementById(lotNoParamId).value = lotNoParamValue;
	submit(formId);
}

function submitChaLibLotClose(formId,
							 lotNoParamId,
							 lotNoParamValue
							 ) {
	if(nidooshi()){
		return;
	}
	document.getElementById(lotNoParamId).value = lotNoParamValue;
	submit(formId);
}

function submitChaLibLotCancel(formId,
							 lotNoParamId,
							 lotNoParamValue
							 ) {
	if(nidooshi()){
		return;
	}
	document.getElementById(lotNoParamId).value = lotNoParamValue;
	submit(formId);
}

function submitChaLibPjtApproveReject(formId,
							 applyNoParamId,
							 applyNoParamValue
							 ) {
	if(nidooshi()){
		return;
	}
	document.getElementById(applyNoParamId).value = applyNoParamValue;
	submit(formId);
}
function submitChaLibPjtApproveWaitAssetApplyDetailView(formId,
							 applyNoParamId,
							 applyNoParamValue
							 ) {
	if(nidooshi()){
		return;
	}
	document.getElementById(applyNoParamId).value = applyNoParamValue;
	submit(formId);
}

function submitChaLibPjtApproveDetailView(formId,
							 pjtNoParamId,
							 pjtNoParamValue
							 ) {
	if(nidooshi()){
		return;
	}
	document.getElementById(pjtNoParamId).value = pjtNoParamValue;
	submit(formId);
}

function submitChaLibPjtApproveAssetApplyDetailView(formId,
							 applyNoParamId,
							 applyNoParamValue
							 ) {
	if(nidooshi()){
		return;
	}
	document.getElementById(applyNoParamId).value = applyNoParamValue;
	submit(formId);
}

function submitChaLibPjtApproveCancel(formId,
							 pjtNoParamId1,
							 pjtNoParamValue1,
							 pjtNoParamId3,
							 pjtNoParamValue3
							 ) {
	if(nidooshi()){
		return;
	}
	document.getElementById(pjtNoParamId1).value = pjtNoParamValue1;
	document.getElementById(pjtNoParamId3).value = pjtNoParamValue3;
	submit(formId);
}

function submitChaLibPjtDetailView( formId, pjtNoParamId, pjtNoParamValue, checkBoxName ) {

	if(nidooshi()){
		return;
	}
	document.getElementById( pjtNoParamId ).value = pjtNoParamValue;
	saveCheckBox( formId, checkBoxName );

	submit( formId );
}

function submitPjtApproveSelection(formId) {
	if(nidooshi()){
		return;
	}
 	var formObj = document.getElementById(formId);

 	for (i = 0; i < document.FlowChaLibPjtApprove.elements.length; i++) {
		if( document.FlowChaLibPjtApprove.elements[i].checked ) {
			var param = document.getElementById('selectParam' + (i+1));
			if ( !param ) {
				var element = document.createElement('input');
				element.setAttribute('type', 'hidden');
				element.setAttribute('id', 'selectParam' + (i+1));
				element.setAttribute('name', 'selectedPjtNo');

				formObj.appendChild( element );
			}

			document.getElementById('selectParam' + (i+1)).value = document.FlowChaLibPjtApprove.elements[i].value;

		}
	}
	submit(formId);
}

function submitChaLibLotSavePageNo(formId, selectedPageNo, lotNoParamId, lotNoParamValue ){
	document.getElementById(lotNoParamId).value = lotNoParamValue;
    submitSavePageNo(formId, selectedPageNo);
}

function submitChaLibPjtSavePageNo(formId, selectedPageNo, pjtNoParamId, pjtNoParamValue ){
	document.getElementById(pjtNoParamId).value = pjtNoParamValue;
    submitSavePageNo(formId, selectedPageNo);
}

function submitChaLibConflictCheckBaselineList(formId,
							 lotNoParamId,
							 lotNoParamValue
							 ) {
	if(nidooshi()){
		return;
	}
	document.getElementById(lotNoParamId).value = lotNoParamValue;
	submit(formId);
}

function submitChaLibConflictCheckBaselineListWithServerNo(
	formId, lotNoParamId, lotNoParamValue, serverNoParamId, serverNoParamValue ) {

	document.getElementById( serverNoParamId ).value = serverNoParamValue;
	submitChaLibConflictCheckBaselineList( formId, lotNoParamId, lotNoParamValue );
}

function submitChaLibConflictCheckBaselineDetailView(formId,
							 buildNoParamId,
							 buildNoParamValue
							 ) {
	if(nidooshi()){
		return;
	}
	document.getElementById(buildNoParamId).value = buildNoParamValue;
	submit(formId);
}

function submitChaLibMergeReport(formId,
							 lotNoParamId,
							 lotNoParamValue,
							 buildNoParamId,
							 buildNoParamValue
							 ) {
	if(nidooshi()){
		return;
	}
	document.getElementById(lotNoParamId).value = lotNoParamValue;
	document.getElementById(buildNoParamId).value = buildNoParamValue;
	submit(formId);
}



function submitChaLibMergeBaselineDetailView(formId,
							 lotNoParamId,
							 lotNoParamValue
							 ) {
	if(nidooshi()){
		return;
	}
	document.getElementById(lotNoParamId).value = lotNoParamValue;
	submit(formId);
}

function submitChaLibMergeBaselineDetailViewWithServerNo(
	formId, lotNoParamId,  lotNoParamValue, serverNoParamId, serverNoParamValue ) {

	document.getElementById( serverNoParamId ).value = serverNoParamValue;
	submitChaLibMergeBaselineDetailView( formId, lotNoParamId, lotNoParamValue );
}

function submitChaLibMergePjtListDetailSelection(formId) {
	if(nidooshi()){
		return;
	}
 	var formObj = document.getElementById(formId);
 	for (i = 0; i < document.FlowChaLibMergePjtListDetail.elements.length; i++) {
		if( document.FlowChaLibMergePjtListDetail.elements[i].checked ) {
			var param = document.getElementById('selectParam' + (i+1));
			if ( !param ) {
				var element = document.createElement('input');
				element.setAttribute('type', 'hidden');
				element.setAttribute('id', 'selectParam' + (i+1));
				element.setAttribute('name', 'selectedPjtNo');

				formObj.appendChild( element );
			}

			document.getElementById('selectParam' + (i+1)).value = document.FlowChaLibMergePjtListDetail.elements[i].value;

		}
	}
	submit(formId);
}

function submitChaLibMergeHistoryBaselineList(formId,
							 lotNoParamId,
							 lotNoParamValue
							 ) {
	if(nidooshi()){
		return;
	}
	document.getElementById(lotNoParamId).value = lotNoParamValue;
	submit(formId);
}

function submitChaLibMergeHistoryBaselineListWithServerNo(
	formId, lotNoParamId, lotNoParamValue, serverNoParamId, serverNoParamValue ) {

	document.getElementById( serverNoParamId ).value = serverNoParamValue;
	submitChaLibMergeHistoryBaselineList( formId, lotNoParamId, lotNoParamValue );
}

function submitChaLibMergeHistoryBaselineDetailView(formId,
							 buildNoParamId,
							 buildNoParamValue
							 ) {
	if(nidooshi()){
		return;
	}
	document.getElementById(buildNoParamId).value = buildNoParamValue;
	submit(formId);
}

function submitWindowDialogSelectResource(formId,
							 resourceParamId,
							 resourceParamValue
							 ) {
	document.getElementById(resourceParamId).value = resourceParamValue;
	openSubWindowDialog(formId);
}

function submitWindowDialogSelectResourceDiff(formId,
							 resourceParamId,
							 resourceParamValue,
							 forwardParamId,
							 forwardParamValue
							 ) {
	document.getElementById(resourceParamId).value = resourceParamValue;
	document.getElementById(forwardParamId).value = forwardParamValue;
	openSubWindowDialog(formId);
}

function submitChaLibReportRegist( formId, windowId ){

	openSubWindow4Download( formId, 'about:blank', windowId );
}

function submitChaLibReportDownload( formId, paramId, paramValue, windowId ){

	document.getElementById( paramId ).value = paramValue;
	openSubWindow4Download( formId,'about:blank',windowId );
}

function changeConditionBySelectedItem( value ) {

	if ( '���|�[�g�敪' == value ) {

		document.getElementById("inputText").style.display = "none";
		document.getElementById("inputSelect").style.display = "block";

	} else {

		document.getElementById("inputText").style.display = "block";
		document.getElementById("inputSelect").style.display = "none";

	}
}

function submitConfirmUseMerge( confirmMessage, formId, useMerge ) {
	var editUseMerge = document.getElementById("editUseMerge");
	if ( editUseMerge.checked == true || ( useMerge == false && editUseMerge.checked == false ) ) {
		submitService( formId );
	} else {
		if ( confirm( confirmMessage ) == true ) {
			submitService( formId );
		}
	}
}
;
