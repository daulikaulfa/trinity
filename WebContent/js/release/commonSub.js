/* ▼[F] ButtonImages Swap
/*--------------------------------------------------------------------------*/
var img=new Object();

	img['btn_login_d']=new Image();

	img['btn_login_d'].src=getImageDirPath()+"/btn_login.png";

	img['btn_login_o']=new Image();

	img['btn_login_o'].src=getImageDirPath()+"/btn_login_o.png";

	img['btn_logout_o']=new Image();
	
	img['btn_logout_o'].src=getImageDirPath()+"/btn_logout_o.png";
	
	img['btn_logout_d']=new Image();
	
	img['btn_logout_d'].src=getImageDirPath()+"/btn_logout.png";

	img['btn_logout']=new Image();
	
	img['btn_logout'].src=getImageDirPath()+"/btn_logout.png";
	
	img['btn_henkou_o']=new Image();
	
	img['btn_henkou_o'].src=getImageDirPath()+"/btn_henkou_o.png";
	
	img['btn_henkou_d']=new Image();
	
	img['btn_henkou_d'].src=getImageDirPath()+"/btn_henkou.png";

	img['btn_henkou']=new Image();
	
	img['btn_henkou'].src=getImageDirPath()+"/btn_henkou.png";
	
	img['btn_navi_unit_entry_o']=new Image();

	img['btn_navi_unit_entry_o'].src=getImageDirPath()+"/btn_global_unit_entry_o.png";

	img['btn_navi_unit_entry_d']=new Image();

	img['btn_navi_unit_entry_d'].src=getImageDirPath()+"/btn_global_unit_entry.png";

	img['btn_navi_rel_apply_o']=new Image();

	img['btn_navi_rel_apply_o'].src=getImageDirPath()+"/btn_global_rel_apply_o.png";

	img['btn_navi_rel_apply_d']=new Image();

	img['btn_navi_rel_apply_d'].src=getImageDirPath()+"/btn_global_rel_apply.png";

	img['btn_navi_rel_entry_o']=new Image();

	img['btn_navi_rel_entry_o'].src=getImageDirPath()+"/btn_global_rel_entry_o.png";

	img['btn_navi_rel_entry_d']=new Image();

	img['btn_navi_rel_entry_d'].src=getImageDirPath()+"/btn_global_rel_entry.png";

	img['btn_navi_dist_entry_o']=new Image();

	img['btn_navi_dist_entry_o'].src=getImageDirPath()+"/btn_global_rel_distribute_o.png";

	img['btn_navi_dist_entry_d']=new Image();

	img['btn_navi_dist_entry_d'].src=getImageDirPath()+"/btn_global_rel_distribute.png";

	img['btn_navi_unit_close_o']=new Image();

	img['btn_navi_unit_close_o'].src=getImageDirPath()+"/btn_global_unit_close_o.png";

	img['btn_navi_unit_close_d']=new Image();

	img['btn_navi_unit_close_d'].src=getImageDirPath()+"/btn_global_unit_close.png";

	img['btn_navi_unit_cancel_o']=new Image();

	img['btn_navi_unit_cancel_o'].src=getImageDirPath()+"/btn_global_unit_cancel_o.png";

	img['btn_navi_unit_cancel_d']=new Image();

	img['btn_navi_unit_cancel_d'].src=getImageDirPath()+"/btn_global_unit_cancel.png";

	img['btn_navi_rel_cancel_o']=new Image();

	img['btn_navi_rel_cancel_o'].src=getImageDirPath()+"/btn_global_rel_cancel_o.png";

	img['btn_navi_rel_cancel_d']=new Image();

	img['btn_navi_rel_cancel_d'].src=getImageDirPath()+"/btn_global_rel_cancel.png";

	img['btn_navi_report_o']=new Image();

	img['btn_navi_report_o'].src=getImageDirPath()+"/btn_global_report_o.png";

	img['btn_navi_report_d']=new Image();

	img['btn_navi_report_d'].src=getImageDirPath()+"/btn_global_report.png";
	
	img['btn_tab_inspect_d']=new Image();

	img['btn_tab_inspect_d'].src=getImageDirPath()+"/btn_tab_latest.png";
	
	img['btn_tab_inspect_o']=new Image();

	img['btn_tab_inspect_o'].src=getImageDirPath()+"/btn_tab_latest_o.png";
	
	img['btn_tab_listinfo_d']=new Image();

	img['btn_tab_listinfo_d'].src=getImageDirPath()+"/btn_tab_listinfo.png";
	
	img['btn_tab_listinfo_o']=new Image();

	img['btn_tab_listinfo_o'].src=getImageDirPath()+"/btn_tab_listinfo_o.png";
	
	img['btn_tab_rirekiinfo_d']=new Image();

	img['btn_tab_rirekiinfo_d'].src=getImageDirPath()+"/btn_tab_rirekiinfo.png";
	
	img['btn_tab_rirekiinfo_o']=new Image();

	img['btn_tab_rirekiinfo_o'].src=getImageDirPath()+"/btn_tab_rirekiinfo_o.png";

	img['btn_mk_shinsei_d']=new Image();

	img['btn_mk_shinsei_d'].src=getImageDirPath()+"/btn_mk_shinsei.png";
	
	img['btn_mk_shinsei_o']=new Image();

	img['btn_mk_shinsei_o'].src=getImageDirPath()+"/btn_mk_shinsei_o.png";

	img['menu_top_d']=new Image();

	img['menu_top_d'].src=getRelUnitImageDirPath()+"/menu_top.png";
	
	img['menu_top_o']=new Image();

	img['menu_top_o'].src=getRelUnitImageDirPath()+"/menu_top_o.png";
	
	img['menu_relunit_d']=new Image();

	img['menu_relunit_d'].src=getRelUnitImageDirPath()+"/menu_relunit.png";
	
	img['menu_relunit_o']=new Image();

	img['menu_relunit_o'].src=getRelUnitImageDirPath()+"/menu_relunit_o.png";
	
	img['relunit_full_d']=new Image();

	img['relunit_full_d'].src=getRelUnitImageDirPath()+"/relunit_full.png";
	
	img['relunit_full_o']=new Image();

	img['relunit_full_o'].src=getRelUnitImageDirPath()+"/relunit_full_o.png";
	
	img['menu_generate_view_d']=new Image();

	img['menu_generate_view_d'].src=getRelUnitImageDirPath()+"/menu_generate_view.png";
	
	img['menu_generate_view_o']=new Image();

	img['menu_generate_view_o'].src=getRelUnitImageDirPath()+"/menu_generate_view_o.png";

	img['menu_relApply_top_d']=new Image();

	img['menu_relApply_top_d'].src=getRelApplyImageDirPath()+"/menu_top.png";
	
	img['menu_relApply_top_o']=new Image();

	img['menu_relApply_top_o'].src=getRelApplyImageDirPath()+"/menu_top_o.png";
	
	img['menu_relApply_approve_d']=new Image();

	img['menu_relApply_approve_d'].src=getRelApplyImageDirPath()+"/menu_approve.png";
	
	img['menu_relApply_approve_o']=new Image();

	img['menu_relApply_approve_o'].src=getRelApplyImageDirPath()+"/menu_approve_o.png";
	
	img['menu_relApply_close_d']=new Image();

	img['menu_relApply_close_d'].src=getRelApplyImageDirPath()+"/menu_close.png";
	
	img['menu_relApply_close_o']=new Image();

	img['menu_relApply_close_o'].src=getRelApplyImageDirPath()+"/menu_close_o.png";

	img['menu_rel_top_d']=new Image();

	img['menu_rel_top_d'].src=getRelCtlImageDirPath()+"/menu_top.png";
	
	img['menu_rel_top_o']=new Image();

	img['menu_rel_top_o'].src=getRelCtlImageDirPath()+"/menu_top_o.png";
	
	img['menu_rel_d']=new Image();

	img['menu_rel_d'].src=getRelCtlImageDirPath()+"/menu_rel.png";
	
	img['menu_rel_o']=new Image();

	img['menu_rel_o'].src=getRelCtlImageDirPath()+"/menu_rel_o.png";
	
	img['menu_rel_generate_view_d']=new Image();

	img['menu_rel_generate_view_d'].src=getRelCtlImageDirPath()+"/menu_generate_view.png";
	
	img['menu_rel_generate_view_o']=new Image();

	img['menu_rel_generate_view_o'].src=getRelCtlImageDirPath()+"/menu_generate_view_o.png";
	
	img['menu_relunit_can_top_d']=new Image();

	img['menu_relunit_can_top_d'].src=getRelUnitCancelImageDirPath()+"/menu_top.png";
	
	img['menu_relunit_can_top_o']=new Image();

	img['menu_relunit_can_top_o'].src=getRelUnitCancelImageDirPath()+"/menu_top_o.png";
	
	img['menu_rel_can_top_d']=new Image();

	img['menu_rel_can_top_d'].src=getRelCtlCancelImageDirPath()+"/menu_top.png";
	
	img['menu_rel_can_top_o']=new Image();

	img['menu_rel_can_top_o'].src=getRelCtlCancelImageDirPath()+"/menu_top_o.png";
	
	img['menu_relunit_close_top_d']=new Image();

	img['menu_relunit_close_top_d'].src=getRelUnitCloseImageDirPath()+"/menu_top.png";
	
	img['menu_relunit_close_top_o']=new Image();

	img['menu_relunit_close_top_o'].src=getRelUnitCloseImageDirPath()+"/menu_top_o.png";

	img['menu_reldist_top_d']=new Image();

	img['menu_reldist_top_d'].src=getRelDistributeImageDirPath()+"/menu_top.png";
	
	img['menu_reldist_top_o']=new Image();

	img['menu_reldist_top_o'].src=getRelDistributeImageDirPath()+"/menu_top_o.png";
	
	img['menu_reldist_entry_d']=new Image();

	img['menu_reldist_entry_d'].src=getRelDistributeImageDirPath()+"/menu_entry.png";
	
	img['menu_reldist_entry_o']=new Image();

	img['menu_reldist_entry_o'].src=getRelDistributeImageDirPath()+"/menu_entry_o.png";
	
	img['menu_reldist_timer_d']=new Image();

	img['menu_reldist_timer_d'].src=getRelDistributeImageDirPath()+"/menu_timer.png";
	
	img['menu_reldist_timer_o']=new Image();

	img['menu_reldist_timer_o'].src=getRelDistributeImageDirPath()+"/menu_timer_o.png";
	
	img['menu_relreport_top_d']=new Image();

	img['menu_relreport_top_d'].src=getRelReportImageDirPath()+"/menu_top.png";
	
	img['menu_relreport_top_o']=new Image();

	img['menu_relreport_top_o'].src=getRelReportImageDirPath()+"/menu_top_o.png";
	
function changeImage(id,objName){
	document.images[id].src=img[objName].src;

}
//-----------------------------------------------



//-----------------------------------------------
//--▼[F] StatusMsg--
//-----------------------------------------------
function StatusMsg(message){

	window.status=message;

}

//-----------------------------------------------



//-----------------------------------------------
//--▼[F] StatusMsg--
//-----------------------------------------------
function ConfirmBox(message){

	flag=confirm(message);

	if (flag) return true; else return false;

}

//-----------------------------------------------



//-----------------------------------------------
//--▼[F] CheckBox AllOn--
//-----------------------------------------------

function AllonCB(n){

	for (i=1; i<=n; i++) document.getElementById('FormAppSelectConfirmation').elements["myCHK"+i].checked=true;

}

/*表示されるCheckBoxの数をnに代入*/
/*各CheckBoxのname="myCHK*"にも連番を。*/

//-----------------------------------------------



//-----------------------------------------------
//--▼[F] CheckBox AllOff--
//-----------------------------------------------

function AlloffCB(n){

	for (i=1; i<=n; i++) document.getElementById('FormAppSelectConfirmation').elements["myCHK"+i].checked=false;

}

/*表示されるCheckBoxの数をnに代入*/
/*各CheckBoxのname="myCHK*"にも連番を。*/

//-----------------------------------------------
