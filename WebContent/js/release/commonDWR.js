//-----------------------------------------------

//-----------------------------------------------
//--▼[F] sleep--
//-----------------------------------------------
  function sleep(time) {
    id = window.setTimeout("sleep()", time);
    window.clearTimeout(id);
  }

//-----------------------------------------------

//-----------------------------------------------
//--▼[F] reload--
//-----------------------------------------------
  var intervalId = 0;
  function reload(flow, interval) {
    reloadTarget(flow);
    intervalId = window.setInterval("reloadTarget('" + flow + "')", interval);
    //intervalId = window.setTimeout("reloadTarget('" + flow + "')", interval);
  }

//-----------------------------------------------
//--▼[F] reloadTarget--
//-----------------------------------------------
  function reloadTarget(flow) {

    //dwr.engine.setActiveReverseAjax(true);

	if('FlowRelUnitGenerateDetailView' == flow){
	  dwr.engine.setErrorHandler(errh4Compile);
      var buildNoParam = dwr.util.getValue("buildNoParam");
	  FlowRelUnitGenerateDetailViewDWR.start(buildNoParam);
	}
	if('FlowRelGenerateDetailView' == flow){
	  dwr.engine.setErrorHandler(errh4Release);
      var relNoParam = dwr.util.getValue("relNoParam");
	  FlowRelGenerateDetailViewDWR.start(relNoParam);
	}

  }

//-----------------------------------------------

//-----------------------------------------------
//--▼[F] notifyServerOnPageUnload--
//-----------------------------------------------
  function notifyServerOnPageUnload(flow) {

    //dwr.engine.setActiveReverseAjax(true);

	if('FlowRelUnitGenerateDetailView' == flow){
	  dwr.engine.setErrorHandler(errh4Compile);
	  FlowRelUnitGenerateDetailViewDWR.invalidate();
	}
	if('FlowRelGenerateDetailView' == flow){
	  dwr.engine.setErrorHandler(errh4Release);
	  FlowRelGenerateDetailViewDWR.invalidate();
	}

	// 画面遷移する前にサーバ側の処理をしてくれるように少し待つ
	sleep(1000);
  }

//-----------------------------------------------

//-----------------------------------------------
//--▼[F] analyseCompileResult--
//-----------------------------------------------
  function analyseCompile(rtn,message) {
  }

//-----------------------------------------------
//--▼[F] analyseCompileResult--
//-----------------------------------------------
  function analyseCompileResult(rtn) {

    if(rtn.buildStatusMsg != undefined){
    var stateMsg = rtn.buildStatusMsg;
    var tran = document.createElement('div');
    tran.innerHTML = stateMsg;
		if($('CompileMessage').hasChildNodes()){
			$('CompileMessage').removeChild($('CompileMessage').childNodes[0]);
		}
    $('CompileMessage').appendChild(tran);
    }

    if(rtn.releaseStatusMsg != undefined){
    var stateMsg = rtn.releaseStatusMsg;
    var tran = document.createElement('div');
    tran.innerHTML = stateMsg;
		if($('releaseStatusMsg').hasChildNodes()){
			$('releaseStatusMsg').removeChild($('releaseStatusMsg').childNodes[0]);
		}
    $('releaseStatusMsg').appendChild(tran);
    }

    allCompleted = new Boolean(true);
  	var tmp = "";
  	var message = "";
  	var imageDirPath = getSeqImageDirPath() + "/";
  	var ext = ".gif";
  	var index0 = 0;
  	var index1 = 0;
  	var index2 = 0;

    if(rtn.imagePathList.length == 0){
		if(message == "") {
			message = '画像ファイルが取得できませんでした。';
		}

		var tran = document.createElement('div');
		tran.innerHTML = message;
		if( $('message') != null ) {
			if($('message').hasChildNodes()){
				$('message').removeChild($('message').childNodes[0]);
			}
			$('message').appendChild(tran);
		}
  		return;
  	}

	rtn.timelineList.each(
		function(obj){
			//タイムラインの背景色
			var bgColor = '#f8f8f8' ;
			if( 'ACTIVE' == obj ) {
				bgColor = 'LightBlue' ;
			} else if( 'COMPLETE' == obj ) {
				bgColor = 'LightGreen' ;
			} else if( 'ERROR' == obj ) {
				bgColor = 'tomato' ;
			}
			$('processTimeline' + index0 ).setAttribute( "bgColor" , bgColor );

			//タイムラインの処理中画像
//			var imageDirPath = "/trinity_release/images/sequence" ; //"./../../images/sequence";
			var imageActive = "arrow45-002.gif";
			var imageInactive = "clear.gif";
			var image = ( 'ACTIVE' == obj ) ? imageActive : imageInactive ;

			$('activeImage' + index0 ).setAttribute( "src" , imageDirPath + '/' + image );
			$('activeImage' + index0 ).setAttribute( "alt" , image );
			$('activeImage' + index0 ).setAttribute( "align" , 'center' );
			$('activeImage' + index0 ).setAttribute( "valign" , 'middle' );

			index0++;
		}
	);

  	rtn.imagePathList.each(
		function(obj){
			index2 = 0;
  			obj.each(
				function(obj){
					$('processImage' + index1 + ":" + index2).setAttribute("src", imageDirPath + obj);

					$('processImage' + index1 + ":" + index2).setAttribute( "class" , 'tooltipsBtn' );
					$('processImage' + index1 + ":" + index2).setAttribute( "title" , rtn.imageMsgList[index1][index2] );

					index2++;
				}
			);
			index1++;
		}
	);

  }

//-----------------------------------------------

//-----------------------------------------------
//--▼[F] DWR error handler--
//-----------------------------------------------
  function errh4Compile(message) {

	var regexp = message.match('^.*:', '');
	if(null != regexp)
      message = message.replace(regexp, '');
    else
      message = 'システム処理中に継続出来ないエラーが発生しました。再度ログインを行ってください。：' + message;

    var tran = document.createElement('div');
    tran.innerHTML = message;
    if($('CompileMessage').hasChildNodes()){
      $('CompileMessage').removeChild($('CompileMessage').childNodes[0]);
    }
    $('CompileMessage').appendChild(tran);

  }

//-----------------------------------------------

//-----------------------------------------------
//--▼[F] DWR error handler--
//-----------------------------------------------
  function errh4Release(message) {

	var regexp = message.match('^.*:', '');
	if(null != regexp)
      message = message.replace(regexp, '');
    else
      message = 'システム処理中に継続出来ないエラーが発生しました。再度ログインを行ってください。：' + message;

    var tran = document.createElement('div');
    tran.innerHTML = message;
    if($('releaseStatusMsg').hasChildNodes()){
      $('releaseStatusMsg').removeChild($('releaseStatusMsg').childNodes[0]);
    }
    $('releaseStatusMsg').appendChild(tran);

  }

