var targetId = "screenTitle";

var isIE = document.all;
var positionTimer;
var viewPosition;
var leftPosition;
var trinityWidth = 900;
var trinityDialogWidth = 488;
var trinityTitle = 280;

function PositionSet( positionWidth ){

	var targetStyle;
	if(isIE){
		targetStyle = document.all(targetId).style;
		viewPosition = document.body.scrollTop;
		//leftPosition = document.body.clientWidth / 2 - (trinityTitle / 2);
		leftPosition = (document.body.clientWidth - positionWidth) / 2;
	}
	else{
		targetStyle = document.getElementById(targetId).style;
		viewPosition = self.pageYOffset;
		//leftPosition = window.innerWidth / 2 - (trinityTitle / 2);
		leftPosition = (window.innerWidth - positionWidth) / 2;
	}

	if(0 > leftPosition){
		leftPosition=0;
	}

	leftPosition = leftPosition + positionWidth - trinityTitle;

	targetStyle.top = viewPosition + "px";
	targetStyle.left = leftPosition + "px";
}

function initPosition(){
	PositionSet( trinityWidth );
	if (isIE){
		document.body.onresize=function(){PositionSet( trinityWidth )};
		document.body.onscroll=function(){PositionSet( trinityWidth )};
	}else{
		window.onresize=function(){PositionSet( trinityWidth )};
		window.onscroll=function(){PositionSet( trinityWidth )};
	}
}

function initPositionDialogWindow(){
	PositionSet( trinityDialogWidth );
	if (isIE){
		document.body.onresize=function(){PositionSet( trinityDialogWidth )};
		document.body.onscroll=function(){PositionSet( trinityDialogWidth )};
	}else{
		window.onresize=function(){PositionSet( trinityDialogWidth )};
		window.onscroll=function(){PositionSet( trinityDialogWidth )};
	}
}

function setCookie(key, val){
	var date = new Date() ;
	time = date.getTime() ;
	time += 60 * 1000 ;//60秒後にセット
    date.setTime( time ) ;
    document.cookie = key + "=" + val + ";expires=" + date.toUTCString() ;
}

function getCookie(key){
	var tmp = document.cookie + ";";
	var index1 = tmp.indexOf(key, 0);
	if(index1 != -1){
		tmp = tmp.substring(index1,tmp.length);
		var index2 = tmp.indexOf("=",0) + 1;
		var index3 = tmp.indexOf(";",index2);
		return(unescape(tmp.substring(index2,index3)));
	}
	return("");
}

function onLoadMakeTopicPath(topicpath,topicLinkContent){

	document.getElementById(topicpath).innerHTML ="";
	for ( var cnt=0; cnt<topicLinkContent.length; cnt++) {
		document.getElementById(topicpath).innerHTML += topicLinkContent[cnt];
	}
}