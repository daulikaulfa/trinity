/* ��[F] Flow
/*--------------------------------------------------------------------------*/

function submitSetForward(formId,forward){
	if(nidooshi()){
		return;
	}
    document.getElementById('forward').value = forward;
    submit(formId);
}

function submitPjtSelection( formId ) {

	if(nidooshi()){
		return;
	}
	
 	var formObj = document.getElementById( formId );  

 	for ( i = 0; i < formObj.elements.length; i++ ) {
 	
		if( formObj.elements[i].checked ) {
		
			var param = document.getElementById('selectParam' + (i+1));
			if ( !param ) {
				var element = document.createElement('input');
				element.setAttribute('type', 'hidden');
				element.setAttribute('id', 'selectParam' + (i+1));
				element.setAttribute('name', formObj.elements[i].name );
		
				formObj.appendChild( element );
			}

			document.getElementById('selectParam' + (i+1)).value = formObj.elements[i].value;
			
		}
	}
	submit( formId );
}

function defaultSet4CheckBoxPjtSelection(checkboxId, targetValue) {
	var checkbox = document.getElementsByName(checkboxId);
	for (i = 0; i < checkbox.length; i++) {
		if ( targetValue.match(checkbox[i].value + ',') != null || targetValue.match(checkbox[i].value + ']') != null ) {
			checkbox[i].checked = true;
			targetValue = targetValue.replace(checkbox[i].value,'');
		}
	}
}

function submitHistoryList(formId,
							 lotNoParamId, 
							 lotNoParamValue
							 ) {
	if(nidooshi()){
		return;
	}
	document.getElementById(lotNoParamId).value = lotNoParamValue;
	submit(formId);
}

function submitHistoryListWithServerNo(
	formId, lotNoParamId, lotNoParamValue, serverNoParamId, serverNoParamValue ) {
	
	document.getElementById( serverNoParamId ).value = serverNoParamValue;
	submitHistoryList( formId, lotNoParamId, lotNoParamValue );
}

function submitRelUnitPjtDetailView(formId,
							 pjtNoParamId,
							 pjtNoParamValue,
							 closedFlagParamId,
							 closedFlagParamValue
							 ) {
	document.getElementById(pjtNoParamId).value = pjtNoParamValue;
	document.getElementById(closedFlagParamId).value = closedFlagParamValue;
	openSubWindow4Release(formId);
}

function submitRelUnitHistoryList(formId,
							 lotNoParamId, 
							 lotNoParamValue
							 ) {
	if(nidooshi()){
		return;
	}
	document.getElementById(lotNoParamId).value = lotNoParamValue;
	submit(formId);
}

function submitRelUnitHistoryListWithServerNo(
	formId, lotNoParamId, lotNoParamValue, serverNoParamId, serverNoParamValue ) {
	
	document.getElementById( serverNoParamId ).value = serverNoParamValue;
	submitRelUnitHistoryList( formId, lotNoParamId, lotNoParamValue );
}

function submitRelUnitSelect(formId,
							 buildNoParamId, 
							 buildNoParamValue
							 ) {
	if(nidooshi()){
		return;
	}
	document.getElementById(buildNoParamId).value = buildNoParamValue;
	submit(formId);
}

function submitRelUnitSelectWithServerNo(
	formId, buildNoParamId, buildNoParamValue, serverNoParamId, serverNoParamValue ) {
	
	document.getElementById( serverNoParamId ).value = serverNoParamValue;
	submitRelUnitSelect( formId, buildNoParamId, buildNoParamValue );
}

function submitRelUnitLotSelect(formId,
							 lotNoParamId, 
							 lotNoParamValue,
							 buildNoParamId, 
							 buildNoParamValue
							 ) {
	if(nidooshi()){
		return;
	}
	document.getElementById(lotNoParamId).value = lotNoParamValue;
	document.getElementById(buildNoParamId).value = buildNoParamValue;
	submit(formId);
}

function submitRelUnitLotSelectWithServerNo(
	formId, lotNoParamId, lotNoParamValue, serverNoParamId, serverNoParamValue, buildNoParamId, buildNoParamValue ) {
	
	document.getElementById( serverNoParamId ).value = serverNoParamValue;
	submitRelUnitLotSelect( formId, lotNoParamId, lotNoParamValue, buildNoParamId, buildNoParamValue );
}

function submitRelUnitHistoryDetailView( formId, unitNoParamId, unitNoParamValue, checkBoxName ) {

	if(nidooshi()){
		return;
	}
	document.getElementById( unitNoParamId ).value = unitNoParamValue;
	saveCheckBox( formId, checkBoxName );
	
	submit( formId );
}

function submitRelPackageLotSelect(formId,
							 lotNoParamId,
							 lotNoId
							 ) {
	if(nidooshi()){
		return;
	}
	if ( document.getElementById(lotNoId) != null ) {
		var formObj = document.getElementById(formId);  
		if ( formObj ) {
			formObj.lotNo.value = getSelectedOptionTag(lotNoId).value;
		}
	}
	submit(formId);
}

function submitRelPackageLotSelectWithServerNo( formId, lotNoParamId, lotNoId ) {

	if ( document.getElementById( lotNoId ) != null ) {
	
		var formObj = document.getElementById( formId );
		if ( formObj ) {
		
			var value = getSelectedOptionTag( lotNoId ).value;
			var values = value.split(",");
		
			formObj.lotNo.value = values[0];
			formObj.serverNo.value = values[1];
		}
	}
	submit( formId );
}