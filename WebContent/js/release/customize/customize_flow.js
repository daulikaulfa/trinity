/* ��[F] Flow
/*--------------------------------------------------------------------------*/

function submitRelHistoryDetailView( formId, relNoParamId, relNoParamValue, checkBoxName ) {

	if(nidooshi()){
		return;
	}
	document.getElementById( relNoParamId ).value = relNoParamValue;
	saveCheckBox( formId, checkBoxName );
	
	submit( formId );
}

function submitRelDistributeModify( formId, distNoParamId, distNoParamValue ) {

	if(nidooshi()){
		return;
	}
	document.getElementById( distNoParamId ).value = distNoParamValue;
	
	submit( formId );
}

function submitRelDistributeCancel( formId, relNoParamId, relNoParamValue ) {

	if(nidooshi()){
		return;
	}
	document.getElementById( relNoParamId ).value = relNoParamValue;
	
	submit( formId );
}

function submitRelReleaseHistoryList(formId,
							envNoParamId,
							envNoParamValue
							){
	if(nidooshi()){
		return;
	}
	document.getElementById(envNoParamId).value = envNoParamValue;
	submit(formId);
}

function submitSetForward(formId,forwardParamId,forwardParamValue){
	if(nidooshi()){
		return;
	}
	document.getElementById(forwardParamId).value = forwardParamValue;
    submit(formId);
}

function submitRelUnitDetailView(formId,
							 buildNoParamId, 
							 buildNoParamValue
							 ) {
	if(nidooshi()){
		return;
	}
	document.getElementById(buildNoParamId).value = buildNoParamValue;
	submitNW(formId);
}

function submitChaLibPjtDetailView( formId, pjtNoParamId, pjtNoParamValue, checkBoxName ) {

	if(nidooshi()){
		return;
	}
	document.getElementById( pjtNoParamId ).value = pjtNoParamValue;
	saveCheckBox( formId, checkBoxName );
	
	submit( formId );
}

function submitEnvSelect(formId,
							 envNoParamId, envNoId
							 ) {
	if(nidooshi()){
		return;
	}
	if ( document.getElementById(envNoId) != null ) {
		document.getElementById(envNoParamId).value = getSelectedOptionTag(envNoId).value;
	}
	submit(formId);
}

function submitEnvListSelect(formId,
							 envNoParamId, envNoId, checkBoxName
							 ) {
	if(nidooshi()){
		return;
	}
	if ( document.getElementById(envNoId) != null ) {
		document.getElementById(envNoParamId).value = getSelectedOptionTag(envNoId).value;
	}
	saveCheckBox( formId, checkBoxName );
	submit(formId);
}

function submitRelDistributeComp( formId, checkBoxName ) {

	if(nidooshi()){
		return;
	}
	saveCheckBox( formId, checkBoxName );
	
	submit( formId );
}

function submitEnvSavePageNo(formId, selectedPageNo,
							envNoParamId,
							envNoId,
							checkBoxName
							 ){
	if(nidooshi()){
		return;
	}
	if ( document.getElementById(envNoId) != null ) {
		document.getElementById(envNoParamId).value = getSelectedOptionTag(envNoId).value;
	}
    document.getElementById('trinitySavedPageNo').value = selectedPageNo;
 	saveCheckBox( formId, checkBoxName );

    submit(formId);
}

function defaultSet4ShellCheckBox(checkBoxName) {
	var checkBox = document.getElementsByName( checkBoxName );
	checkBox[0].checked = true;

}
