/* ��[F] Flow
/*--------------------------------------------------------------------------*/

function submitRelHistoryDetailView( formId, relNoParamId, relNoParamValue, checkBoxName ) {

	if(nidooshi()){
		return;
	}
	document.getElementById( relNoParamId ).value = relNoParamValue;
	saveCheckBox( formId, checkBoxName );
	
	submit( formId );
}

function submitRelHistoryDetailViewWithServerNo(
	formId, relNoParamId, relNoParamValue, lotNoParamId, lotNoParamValue, serverNoParamId, serverNoParamValue, checkBoxName ) {

	document.getElementById( lotNoParamId ).value = lotNoParamValue;
	document.getElementById( serverNoParamId ).value = serverNoParamValue;
	
	submitRelHistoryDetailView( formId, relNoParamId, relNoParamValue, checkBoxName );
}

function submitRelCtlHistoryList(formId,
							envNoParamId,
							envNoParamValue
							){
	if(nidooshi()){
		return;
	}
	document.getElementById(envNoParamId).value = envNoParamValue;
	submit(formId);
}

function submitSetForward(formId,forwardParamId,forwardParamValue){
	if(nidooshi()){
		return;
	}
	document.getElementById(forwardParamId).value = forwardParamValue;
    submit(formId);
}

function submitRelUnitDetailView(formId,
							 buildNoParamId, 
							 buildNoParamValue
							 ) {
	document.getElementById(buildNoParamId).value = buildNoParamValue;
	openSubWindow4Release(formId);
}

function submitChaLibPjtDetailView( formId, pjtNoParamId, pjtNoParamValue, checkBoxName ) {

	if(nidooshi()){
		return;
	}
	document.getElementById( pjtNoParamId ).value = pjtNoParamValue;
	saveCheckBox( formId, checkBoxName );
	
	submit( formId );
}

function submitReleaseSelect(formId,
							 envNoParamId, 
							 envNoParamValue
							 ) {
	if(nidooshi()){
		return;
	}
	document.getElementById(envNoParamId).value = envNoParamValue;
	submit(formId);
}

function submitReleaseLotSelect(formId,
							 lotNoParamId,
							 lotNoId
							 ) {
	if(nidooshi()){
		return;
	}
	if ( document.getElementById(lotNoId) != null ) {
		var formObj = document.getElementById(formId);  
		if ( formObj ) {
			formObj.lotNo.value = getSelectedOptionTag(lotNoId).value;
		}
	}
	submit(formId);
}

function submitReleaseLotSelectWithServerNo( formId, lotNoParamId, lotNoId ) {

	if ( document.getElementById( lotNoId ) != null ) {
	
		var formObj = document.getElementById( formId );
		if ( formObj ) {
		
			var value = getSelectedOptionTag( lotNoId ).value;
			var values = value.split(",");
		
			formObj.lotNo.value = values[0];
			formObj.serverNo.value = values[1];
		}
	}
	submit( formId );
}