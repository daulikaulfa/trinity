/*
 * trinity.finder.js JavaScript Library v2.0
 *
 * Copyright 2008, (c) Blueship co., ltd.
 *
 * Includes jQuery JavaScript Library v1.8.1
 *   http://jquery.com/
 *   Copyright 2012 jQuery Foundation and other contributors
 *   Released under the MIT license
 *   http://jquery.org/license
 *
 *
 *--------------------------------------------------------------------------*/

//-----------------------------------------------
// 初期化
//-----------------------------------------------
function finderInit(){
	jQuery.noConflict();
}

//-----------------------------------------------
// jQueryのラッパー関数
//-----------------------------------------------
function jQueryFinder( fn ){
	return jQuery( fn );
}

function jQueryFinderSmoothdivscroll(){
	// None of the options are set
	jQueryFinder("div#makeMeScrollable").smoothDivScroll({
		
	});
}
