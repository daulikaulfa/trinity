/* ��[F] Flow
/*--------------------------------------------------------------------------*/

function submitRelApplyDetailView( formId,
							 relApplyNoParamValue
							 ) {

	if(nidooshi()){
		return;
	}
	setValueAll( document.getElementsByName('searchRelApplyNo'), relApplyNoParamValue );

	submitService(formId);
}

function submitRelApplyModify( formId,
							 relApplyNoParamValue
							 ) {

	if(nidooshi()){
		return;
	}
	setValueAll( document.getElementsByName('searchRelApplyNo'), relApplyNoParamValue );

	submitService(formId);
}

function submitRelApplyEntrySaveComp(formId,
							 selectedGroupIdParamIdFrom,
							 selectedRelEnvNoParamIdFrom,
							 selectedRelWishDateParamIdFrom,
							 relApplyUserParamIdFrom,
							 relApplyPersonChargeParamIdFrom,
							 relatedNoParamIdFrom,
							 remarksParamIdFrom
							 ) {
	if(nidooshi()){
		return;
	}

	var formObj = document.getElementById('FlowRelApplyEntryConfirm');

	formObj.selectedGroupIdParam.value		 = document.getElementById(selectedGroupIdParamIdFrom).value;
	formObj.selectedRelEnvNoParam.value		 = document.getElementById(selectedRelEnvNoParamIdFrom).value;
	formObj.selectedRelWishDateParam.value	 = document.getElementById(selectedRelWishDateParamIdFrom).value;
	formObj.relApplyUserParam.value			 = document.getElementById(relApplyUserParamIdFrom).value;
	formObj.relApplyPersonChargeParam.value	 = document.getElementById(relApplyPersonChargeParamIdFrom).value;
	formObj.relatedNoParam.value			 = document.getElementById(relatedNoParamIdFrom).value;
	formObj.remarksParam.value				 = document.getElementById(remarksParamIdFrom).value;

	submitChangeActionForward('FlowRelApplyEntryConfirm', formId);
}

function submitRelApplyEntryConfirm(formId,
							 selectedGroupIdParamIdFrom,
							 selectedRelEnvNoParamIdFrom,
							 selectedRelWishDateParamIdFrom,
							 relApplyUserParamIdFrom,
							 relApplyPersonChargeParamIdFrom,
							 relatedNoParamIdFrom,
							 remarksParamIdFrom
							 ) {
	if(nidooshi()){
		return;
	}

	setValueAll( document.getElementsByName('selectedGroupIdParam'), document.getElementById(selectedGroupIdParamIdFrom).value );
	setValueAll( document.getElementsByName('selectedRelEnvNoParam'), document.getElementById(selectedRelEnvNoParamIdFrom).value );
	setValueAll( document.getElementsByName('selectedRelWishDateParam'), document.getElementById(selectedRelWishDateParamIdFrom).value );
	setValueAll( document.getElementsByName('relApplyUserParam'), document.getElementById(relApplyUserParamIdFrom).value );
	setValueAll( document.getElementsByName('relApplyPersonChargeParam'), document.getElementById(relApplyPersonChargeParamIdFrom).value );
	setValueAll( document.getElementsByName('relatedNoParam'), document.getElementById(relatedNoParamIdFrom).value );
	setValueAll( document.getElementsByName('remarksParam'), document.getElementById(remarksParamIdFrom).value );

	submitChangeActionForward('FlowRelApplyEntryConfirm', formId);
}


function submitRelApplyEntryComp(formId
							 ) {
	if(nidooshi()){
		return;
	}

	submitService(formId);
}


function submitRelApplyModifySaveComp(formId,
							 selectedGroupIdParamIdFrom,
							 selectedRelEnvNoParamIdFrom,
							 selectedRelWishDateParamIdFrom,
							 relApplyUserParamIdFrom,
							 relApplyPersonChargeParamIdFrom,
							 relatedNoParamIdFrom,
							 remarksParamIdFrom
							 ) {
	if(nidooshi()){
		return;
	}

	var formObj = document.getElementById('FlowRelApplyModifyConfirm');

	formObj.selectedGroupIdParam.value		 = document.getElementById(selectedGroupIdParamIdFrom).value;
	formObj.selectedRelEnvNoParam.value		 = document.getElementById(selectedRelEnvNoParamIdFrom).value;
	formObj.selectedRelWishDateParam.value	 = document.getElementById(selectedRelWishDateParamIdFrom).value;
	formObj.relApplyUserParam.value			 = document.getElementById(relApplyUserParamIdFrom).value;
	formObj.relApplyPersonChargeParam.value	 = document.getElementById(relApplyPersonChargeParamIdFrom).value;
	formObj.relatedNoParam.value			 = document.getElementById(relatedNoParamIdFrom).value;
	formObj.remarksParam.value				 = document.getElementById(remarksParamIdFrom).value;

	submitChangeActionForward('FlowRelApplyModifyConfirm', formId);
}

function submitRelApplyModifyConfirm(formId,
							 selectedGroupIdParamIdFrom,
							 selectedRelEnvNoParamIdFrom,
							 selectedRelWishDateParamIdFrom,
							 relApplyUserParamIdFrom,
							 relApplyPersonChargeParamIdFrom,
							 relatedNoParamIdFrom,
							 remarksParamIdFrom
							 ) {
	if(nidooshi()){
		return;
	}

	setValueAll( document.getElementsByName('selectedGroupIdParam'), document.getElementById(selectedGroupIdParamIdFrom).value );
	setValueAll( document.getElementsByName('selectedRelEnvNoParam'), document.getElementById(selectedRelEnvNoParamIdFrom).value );
	setValueAll( document.getElementsByName('selectedRelWishDateParam'), document.getElementById(selectedRelWishDateParamIdFrom).value );
	setValueAll( document.getElementsByName('relApplyUserParam'), document.getElementById(relApplyUserParamIdFrom).value );
	setValueAll( document.getElementsByName('relApplyPersonChargeParam'), document.getElementById(relApplyPersonChargeParamIdFrom).value );
	setValueAll( document.getElementsByName('relatedNoParam'), document.getElementById(relatedNoParamIdFrom).value );
	setValueAll( document.getElementsByName('remarksParam'), document.getElementById(remarksParamIdFrom).value );

	submitChangeActionForward('FlowRelApplyModifyConfirm', formId);
}


function submitRelApplyModifyComp(formId
							 ) {
	if(nidooshi()){
		return;
	}

	submitService(formId);
}


function submitRelApplyNoSelect(formId,
							 relApplyNoParamId,
							 relApplyNoParamValue
							 ) {
	if(nidooshi()){
		return;
	}
	document.getElementById(relApplyNoParamId).value = relApplyNoParamValue;
	submitService(formId);
}

function showAppendFileRow(num, targetId, buttonId1, buttonId2, openDisplayId){
	var display = document.getElementById(openDisplayId);
	if(num == 1) {
		if(targetId != ''){
			document.getElementById(targetId).style.cssText = 'display:';
		}
		if(buttonId1 != ''){
			document.getElementById(buttonId1).style.cssText = 'display:none';
		}
		if(buttonId2 != ''){
			document.getElementById(buttonId2).style.cssText = 'display:none';
		}
		display.value = '1';
	}

	if(num == 0) {
		if(targetId != ''){
			document.getElementById(targetId).style.cssText = 'display:none';
		}
		if(buttonId1 != ''){
			document.getElementById(buttonId1).style.cssText = 'display:';
		}
		if(buttonId2 != ''){
			document.getElementById(buttonId2).style.cssText = 'display:';
		}

		display.value = '0';
	}
}

function appendFileMode(divisionId, openDisplayId, openDisplay) {
	var display = document.getElementById(openDisplayId);

	if ( openDisplay == '0' ) {
		document.getElementById(divisionId).style.cssText = 'display:none';
		if ( display ) {
			display.value = '0';
		}
	} else {
		document.getElementById(divisionId).style.cssText = 'display:';
		if ( display ) {
			display.value = '1';
		}
	}
}

function adjustAppendBtn() {
	document.getElementById('appendFileBtn1').style.cssText = 'display:';
	document.getElementById('appendFileBtn2').style.cssText = 'display:';
	document.getElementById('appendFileBtn3').style.cssText = 'display:';
	document.getElementById('appendFileBtn4').style.cssText = 'display:';

	document.getElementById('deleteFileBtn2').style.cssText = 'display:';
	document.getElementById('deleteFileBtn3').style.cssText = 'display:';
	document.getElementById('deleteFileBtn4').style.cssText = 'display:';
	document.getElementById('deleteFileBtn5').style.cssText = 'display:';

	var display = document.getElementById('showAppendFile2');
	if ( display.value == '1' ) {
		document.getElementById('appendFileBtn1').style.cssText = 'display:none';
	}

	display = document.getElementById('showAppendFile3');
	if ( display.value == '1' ) {
		document.getElementById('appendFileBtn2').style.cssText = 'display:none';
		document.getElementById('deleteFileBtn2').style.cssText = 'display:none';
	}

	display = document.getElementById('showAppendFile4');
	if ( display.value == '1' ) {
		document.getElementById('appendFileBtn3').style.cssText = 'display:none';
		document.getElementById('deleteFileBtn3').style.cssText = 'display:none';
	}

	display = document.getElementById('showAppendFile5');
	if ( display.value == '1' ) {
		document.getElementById('appendFileBtn4').style.cssText = 'display:none';
		document.getElementById('deleteFileBtn4').style.cssText = 'display:none';
	}
}

function appendFileCheckBox(checkboxId, appendFileName) {
	var checkbox = document.getElementById(checkboxId);

	if ( !checkbox ) return;

	if ( appendFileName && appendFileName != '' ) {
		checkbox.disabled = false;
	} else {
		checkbox.disabled = true;
	}
}

//-----------------------------------------------
//--��[F] setValueAll--
//-----------------------------------------------
function setValueAll( array, val ) {
	for(var i = 0 ; i < array.length ; i++) {

		array[i].value = val;
	}

}

