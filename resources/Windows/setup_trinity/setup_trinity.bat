@echo off
setlocal

rem ENVIRONMENT VARIABLE
set PUBLIC=C:\trinity\web
set HOME=C:\home

cd /d %~dp0setup_admin
call .\setup_admin.bat

cd /d %~dp0setup_changec
call .\setup_changec.bat

cd /d %~dp0setup_release
call .\setup_release.bat

cd /d %~dp0

xcopy %HOME%\conf\trinity_admin\ %HOME%\conf\trinity\ /Y /E /C /H /S /EXCLUDE:.\EXCLUDE.TXT
xcopy %HOME%\trinity_changec %HOME%\trinity\ /Y /E /C /H /S /EXCLUDE:.\EXCLUDE.TXT
xcopy %HOME%\trinity_release %HOME%\trinity\ /Y /E /C /H /S /EXCLUDE:.\EXCLUDE.TXT

xcopy .\home %HOME%\ /Y /E /C /H /S /EXCLUDE:.\EXCLUDE.TXT
xcopy .\manual %PUBLIC%\manual\ /Y /E /C /H /S /EXCLUDE:.\EXCLUDE.TXT

endlocal