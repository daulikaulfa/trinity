@echo off
setlocal

rem @param1 配付対象グループ（環境）
SET GROUP_NAME={@GROUP_NAME}
rem @param2 リリースコントロール番号
SET RC_NO={@RC_NO}
rem @param3 資源転送先ディレクトリパス
SET DIR_PATH={@DIR_PATH}

rem コマンド格納先
SET CMD_PATH=C:\defs\dealAssetDevShell
rem シェル
SET CMD=%CMD_PATH%\to_assetReg.bat %GROUP_NAME% %RC_NO% %DIR_PATH%

:loop
cls
    echo ************************************************
    echo *                                              *
    echo * 配付資源登録                                 *
    echo *                                              *
    echo ************************************************
    echo ＤＲＭＳ管理ファイルに対して資源登録を行います。
    echo.
    echo 以下の条件で登録処理を実行します。
    echo # 配付対象グループ（環境）：%GROUP_NAME%
    echo # リリースコントロール番号：%RC_NO%
    echo.
    set /p input=実行しますか？(yes/no)
    if not defined input goto loop
    if /i %input%==YES (
            goto execute
    )

    if /i %input%==NO (
        echo ***** 登録処理を中止します *****
        exit /B 1
    )
goto loop

:execute
rem シェル呼び出し
echo.
CMD /Q /C %CMD%
SET RETVAL=%ERRORLEVEL%
if %RETVAL%==0 (
        echo.
        echo 正常終了 復帰値=%RETVAL%
        echo.
        exit /B %RETVAL%
    ) else (
        echo.
        echo 異常終了 復帰値=%RETVAL%
        echo.
        exit /B %RETVAL%
    )
