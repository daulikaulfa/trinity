#!/bin/bash

#@param1 配付対象グループ（環境）
GROUP_NAME="{@GROUP_NAME}"
#@param2 リリースコントロール番号
RC_NO="{@RC_NO}"
#@param3 資源転送先ディレクトリパス
DIR_PATH="{@DIR_PATH}"

#コマンド格納先
CMD_PATH=""
#資源登録シェル
CMD="${CMD_PATH}/to_assetReg.sh ${GROUP_NAME} ${RC_NO} ${DIR_PATH}"

clear
while true
do
    echo "************************************************"
    echo "*                                              *"
    echo "* 配付資源登録                                 *"
    echo "*                                              *"
    echo "************************************************"
    echo "ＤＲＭＳ管理ファイルに対して資源登録を行います。"
    echo ""
    echo "以下の条件で登録処理を実行します。"
    echo "# 配付対象グループ（環境）：${GROUP_NAME}"
    echo "# リリースコントロール番号：${RC_NO}"
    echo ""
    echo -n "実行しますか？(YES/NO) > "
    read ans
    if [ ${ans} == "YES" ];
        then
            break 1
    fi

    if [ ${ans} == "NO" ];
        then
            echo "***** 登録処理を中止します *****"
        exit 1
    fi
    clear
done

echo ""
echo "************************************************"

#登録シェル呼び出し
STR=$(bash ${CMD})
ret=$?

tmp=$(echo ${STR} | sed -e 's/,/ ) /g')
echo "* メッセージID ) メッセージ                    *"
echo "************************************************"
echo "${tmp}"

#メッセージ整形(input メッセージID,文言)
if [ ${ret} == "0" ];
    then 
        echo ""
        echo "[正常終了(cd=${ret})]"
        echo ""
        exit ${ret}
    else
        echo ""
        echo "[異常終了(cd=${ret})]"
        echo ""
        exit ${ret}
fi
