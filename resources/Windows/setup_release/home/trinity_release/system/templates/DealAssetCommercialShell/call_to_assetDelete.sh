#!/bin/bash

#@param1 配付対象グループ（環境）
GROUP_NAME="{@GROUP_NAME}"
#@param2 リリースコントロール番号
RC_NO="{@RC_NO}"

#コマンド格納先
CMD_PATH=""
#資源削除シェル
CMD="${CMD_PATH}/to_assetDelete.sh ${GROUP_NAME} ${RC_NO}"

clear
while true
do
    echo "************************************************"
    echo "*                                              *"
    echo "* 配付資源削除                                 *"
    echo "*                                              *"
    echo "************************************************"
    echo "ＤＲＭＳ管理ファイルから資源の削除を行います。"
    echo ""
    echo "以下の条件で削除処理を実行します。"
    echo "# 配付対象グループ（環境）：${GROUP_NAME}"
    echo "# リリースコントロール番号：${RC_NO}"
    echo ""
    echo -n "実行しますか？(YES/NO) > "
    read ans
    if [ ${ans} == "YES" ];
        then
            break 1
    fi

    if [ ${ans} == "NO" ];
        then
            echo "***** 削除処理を中止します *****"
        exit 1
    fi
    clear
done

echo ""
echo "************************************************"

#削除シェル呼び出し
STR=$(bash ${CMD})
ret=$?

tmp=$(echo ${STR} | sed -e 's/,/) /g')
echo "* メッセージID ) メッセージ                    *"
echo "************************************************"
echo "${tmp}"

#メッセージ整形(input メッセージID,文言)
if [ ${ret} == "0" ];
    then 
        echo ""
        echo "[正常終了(cd=${ret})]"
        echo ""
        exit ${ret}
    else
        echo ""
        echo "[異常終了(cd=${ret})]"
        echo ""
        exit ${ret}
fi
