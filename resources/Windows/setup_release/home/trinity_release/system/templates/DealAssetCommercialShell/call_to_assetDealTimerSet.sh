#!/bin/bash

#@param1 配付対象グループ（環境）
GROUP_NAME="{@GROUP_NAME}"
#@param2 リリースコントロール番号
RC_NO="{@RC_NO}"
#@param3 配付日時
TIMER="{@TIMER}"
#@param4 新規/変更フラグ
SET_FLG="{@SET_FLG}"

#コマンド格納先
CMD_PATH=""
#資源登録シェル
CMD="${CMD_PATH}/to_assetDealTimerSet.sh ${GROUP_NAME} ${RC_NO} ${TIMER} ${SET_FLG}"

clear
while true
do
    echo "************************************************"
    echo "*                                              *"
    echo "* タイマー設定                                 *"
    echo "*                                              *"
    echo "************************************************"
    echo "ＤＲＭＳ管理ファイルへ登録された資源に対し、配付"
    echo "日時を設定します。"
    echo ""
    echo "以下の条件で設定処理を実行します。"
    echo "# 配付対象グループ（環境）：${GROUP_NAME}"
    echo "# リリースコントロール番号：${RC_NO}"
    echo "# 配付日時                ：${TIMER}"
    echo "# 新規/変更               ：${SET_FLG}"
    echo ""
    echo -n "実行しますか？(YES/NO) > "
    read ans
    if [ ${ans} == "YES" ];
        then
            break 1
    fi

    if [ ${ans} == "NO" ];
        then
            echo "***** 設定処理を中止します *****"
        exit 1
    fi
    clear
done

echo ""
echo "************************************************"

#タイマー設定シェル呼び出し
STR=$(bash ${CMD})
ret=$?

#メッセージ整形(input メッセージID,文言)
if [ ${ret} == "0" ];
    then 
        msg=$(echo ${STR} | awk -F',' '{print $1}')
        drmsver=$(echo ${STR} | awk -F',' '{print $2}')
        setdate=$(echo ${STR} | awk -F',' '{print $3}')
        echo "* メッセージ ( DRMS管理バージョン:設定日時 )   *"
        echo "************************************************"
        echo "${msg} ( ${drmsver}:${setdate} )"
        echo ""
        echo "[正常終了(cd=${ret})]"
        echo ""
        exit ${ret}
    else
        tmp=$(echo ${STR} | sed -e 's/,/ ) /g')
        echo "* メッセージID ) メッセージ                    *"
        echo "************************************************"
        echo "${tmp}"
        echo ""
        echo "[異常終了(cd=${ret})]"
        echo ""
        exit ${ret}
fi
