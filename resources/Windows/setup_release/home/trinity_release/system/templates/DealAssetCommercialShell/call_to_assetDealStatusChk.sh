#!/bin/bash

#@param1 配付対象グループ（環境）
GROUP_NAME="{@GROUP_NAME}"

#コマンド格納先
CMD_PATH=""
#資源登録シェル
CMD="${CMD_PATH}/to_assetDealStatusChk.sh"

clear
while true
do
    echo "************************************************"
    echo "*                                              *"
    echo "* 全環境最新情報取得                           *"
    echo "*                                              *"
    echo "************************************************"
    echo "全環境の最新情報を取得します。"
    echo ""
    echo -n "実行しますか？(YES/NO) > "
    read ans
    if [ ${ans} == "YES" ];
        then
            break 1
    fi

    if [ ${ans} == "NO" ];
        then
            echo "***** 取得処理を中止します *****"
        exit 1
    fi
    clear
done

echo ""
echo "***************************************************"

#配付履歴確認処理
STR=$(bash ${CMD})
ret=$?

#メッセージ整形
if [ ${ret} == "0" ];
    then 
        echo ${STR} | grep ":" > /dev/null
        if [ $? -ne 0 ];
        then
            MSG=$(echo ${STR} | sed -e 's/,/ - /g')
        else
            tmp=$(echo ${STR} | sed -e 's/,/ - /g')
            MSG=$(echo ${tmp} | awk 'gsub(/:/,ORS)')
        fi
        echo "* 環境 - DRMS管理バージョン - 登録日時 - 配付日時 *"
        echo "***************************************************"
        echo "${MSG}"
        echo ""
        echo "[正常終了(cd=${ret})]"
        echo ""
        exit ${ret}
    else
        tmp=$(echo ${STR} | sed -e 's/,/ ) /g')
        echo "* メッセージID ) メッセージ                       *"
        echo "***************************************************"
        echo "${tmp}"
        echo ""
        echo "[異常終了(cd=${ret})]"
        echo ""
        exit ${ret}
fi
