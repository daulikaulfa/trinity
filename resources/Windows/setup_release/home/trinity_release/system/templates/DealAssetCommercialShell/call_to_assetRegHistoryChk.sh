#!/bin/bash

#@param1 配付対象グループ（環境）
GROUP_NAME="{@GROUP_NAME}"

#コマンド格納先
CMD_PATH=""
#履歴確認シェル
CMD="${CMD_PATH}/to_assetRegHistoryChk.sh ${GROUP_NAME}"

clear
while true
do
    echo "************************************************"
    echo "*                                              *"
    echo "* 特定環境履歴情報取得                         *"
    echo "*                                              *"
    echo "************************************************"
    echo "ＤＲＭＳ管理ファイルから資源登録履歴を取得します。"
    echo ""
    echo "以下の条件で取得処理を実行します。"
    echo "# 配付対象グループ（環境）：${GROUP_NAME}"
    echo ""
    echo -n "実行しますか？(YES/NO) > "
    read ans
    if [ ${ans} == "YES" ];
        then
            break 1
    fi

    if [ ${ans} == "NO" ];
        then
            echo "***** 削除処理を中止します *****"
        exit 1
    fi
    clear
done

echo ""
echo "**********************************************************"

#削除シェル呼び出し
STR=$(bash ${CMD})
ret=$?

#メッセージ整形
if [ ${ret} == "0" ];
    then 
        echo ${STR} | grep ":" >/dev/null
        if [ $? -ne 0 ];
        then
            MSG=$(echo ${STR} | sed -e 's/,/ => /g')
        else
            tmp=$(echo ${STR} | sed -e 's/,/ => /g')
            MSG=$(echo ${tmp} | awk 'gsub(/:/,ORS)')
        fi
        echo "* DRMS管理バージョン => 登録日時 => 配付日時 => 成功可否 *"
        echo "**********************************************************"
        echo "${MSG}"
        echo ""
        echo "[正常終了(cd=${ret})]"
        echo ""
        exit ${ret}
    else
        tmp=$(echo ${STR} | sed -e 's/,/ ) /g')
        echo "* メッセージID ) エラーメッセージ                        *"
        echo "**********************************************************"
        echo "${tmp}"
        echo ""
        echo "[異常終了(cd=${ret})]"
        echo ""
        exit ${ret}
fi
