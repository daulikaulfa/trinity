#!/bin/bash

#@param1 配付対象グループ（環境）
GROUP_NAME="{@GROUP_NAME}"
#@param2 リリースコントロール番号
RC_NO="{@RC_NO}"

#コマンド格納先
CMD_PATH=""
#資源登録シェル
CMD="${CMD_PATH}/to_assetDealTimerCancel.sh ${GROUP_NAME} ${RC_NO}"

clear
while true
do
    echo "************************************************"
    echo "*                                              *"
    echo "* タイマー取り消し                             *"
    echo "*                                              *"
    echo "************************************************"
    echo "資源に設定されたタイマーを取り消します。"
    echo ""
    echo "以下の条件で取り消し処理を実行します。"
    echo "# 配付対象グループ（環境）：${GROUP_NAME}"
    echo "# リリースコントロール番号：${RC_NO}"
    echo ""
    echo -n "実行しますか？(YES/NO) > "
    read ans
    if [ ${ans} == "YES" ];
        then
            break 1
    fi

    if [ ${ans} == "NO" ];
        then
            echo "*** 取り消し処理を中止します ***"
        exit 1
    fi
    clear
done

echo ""
echo "************************************************"

#タイマー取り消しシェル呼び出し
STR=$(bash ${CMD})
ret=$?

#メッセージ整形
if [ ${ret} == "0" ];
    then 
        tmp=$(echo ${STR} | sed -e 's/しました,/しました ( /g')
        MSG=$(echo "${tmp} )" | sed -e 's/:/ )/g')
        echo "* メッセージ ( DRMS管理バージョン)             *"
        echo "************************************************"
        echo "${MSG}"
        echo ""
        echo "[正常終了(cd=${ret})]"
        echo ""
        exit ${ret}
    else
        tmp=$(echo ${STR} | sed -e 's/,/ ) /g')
        MSG=$(echo ${tmp} | sed -e 's/:/\n/g')
        echo "* メッセージID ) メッセージ                    *"
        echo "************************************************"
        echo "${MSG}"
        echo ""
        echo "[異常終了(cd=${ret})]"
        echo ""
        exit ${ret}
fi
