@echo off
setlocal

rem コマンド格納先
SET CMD_PATH=C:\defs\dealAssetDevShell
rem シェル
SET CMD=%CMD_PATH%\to_assetDealStatusChk.bat

:loop
cls
    echo ************************************************
    echo *                                              *
    echo * 全環境最新情報取得                           *
    echo *                                              *
    echo ************************************************
    echo 全環境の最新情報を取得します。
    echo.
    set /p input=実行しますか？(yes/no)
    if not defined input goto loop
    if /i %input%==YES (
            goto execute
    )

    if /i %input%==NO (
        echo ***** 取得処理を中止します *****
        exit /B 1
    )
goto loop

:execute
rem シェル呼び出し
echo.
CMD /Q /C %CMD%
SET RETVAL=%ERRORLEVEL%
if %RETVAL%==0 (
        echo.
        echo 正常終了 復帰値=%RETVAL%
        echo.
        exit /B %RETVAL%
    ) else (
        echo.
        echo 異常終了 復帰値=%RETVAL%
        echo.
        exit /B %RETVAL%
    )
