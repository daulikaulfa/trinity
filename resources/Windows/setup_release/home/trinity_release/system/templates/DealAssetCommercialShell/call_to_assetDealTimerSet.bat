@echo off
setlocal

rem @param1 配付対象グループ（環境）
SET GROUP_NAME={@GROUP_NAME}
rem @param2 リリースコントロール番号
SET RC_NO={@RC_NO}
rem @param3 配付日時
SET TIMER={@TIMER}
rem @param4 新規/変更フラグ
SET SET_FLG={@SET_FLG}

rem コマンド格納先
SET CMD_PATH=C:\defs\dealAssetDevShell
rem シェル
SET CMD=%CMD_PATH%\to_assetDealTimerSet.bat %GROUP_NAME% %RC_NO% %TIMER% %SET_FLG%

:loop
cls
    echo ************************************************
    echo *                                              *
    echo * タイマー設定                                 *
    echo *                                              *
    echo ************************************************
    echo ＤＲＭＳ管理ファイルへ登録された資源に対し、配付
    echo 日時を設定します。
    echo.
    echo 以下の条件で設定処理を実行します。
    echo # 配付対象グループ（環境）：%GROUP_NAME%
    echo # リリースコントロール番号：%RC_NO%
    echo # 配付日時                ：%TIMER%
    echo # 新規/変更               ：%SET_FLG%
    echo.
    set /p input=実行しますか？(yes/no)
    if not defined input goto loop
    if /i %input%==YES (
            goto execute
    )

    if /i %input%==NO (
        echo ***** 設定処理を中止します *****
        exit /B 1
    )
goto loop

:execute
rem シェル呼び出し
echo.
CMD /Q /C %CMD%
SET RETVAL=%ERRORLEVEL%
if %RETVAL%==0 (
        echo.
        echo 正常終了 復帰値=%RETVAL%
        echo.
        exit /B %RETVAL%
    ) else (
        echo.
        echo 異常終了 復帰値=%RETVAL%
        echo.
        exit /B %RETVAL%
    )
