@echo off
setlocal

rem @param1 配付対象グループ（環境）
SET GROUP_NAME={@GROUP_NAME}

rem コマンド格納先
SET CMD_PATH=C:\defs\dealAssetDevShell
rem シェル
SET CMD=%CMD_PATH%\to_assetRegHistoryChk.bat %GROUP_NAME%

:loop
cls
    echo ************************************************
    echo *                                              *
    echo * 特定環境履歴情報取得                         *
    echo *                                              *
    echo ************************************************
    echo ＤＲＭＳ管理ファイルから資源登録履歴を取得します。
    echo.
    echo 以下の条件で取得処理を実行します。
    echo # 配付対象グループ（環境）：%GROUP_NAME%
    echo.
    set /p input=実行しますか？(yes/no)
    if not defined input goto loop
    if /i %input%==YES (
            goto execute
    )

    if /i %input%==NO (
        echo ***** 取得処理を中止します *****
        exit /B 1
    )
goto loop

:execute
rem シェル呼び出し
echo.
CMD /Q /C %CMD%
SET RETVAL=%ERRORLEVEL%
if %RETVAL%==0 (
        echo.
        echo 正常終了 復帰値=%RETVAL%
        echo.
        exit /B %RETVAL%
    ) else (
        echo.
        echo 異常終了 復帰値=%RETVAL%
        echo.
        exit /B %RETVAL%
    )
