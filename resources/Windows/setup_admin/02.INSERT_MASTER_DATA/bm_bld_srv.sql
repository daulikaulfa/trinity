-- ------------------------------------------------------------
-- bm_bld_srv
-- ------------------------------------------------------------

INSERT INTO trinity.bm_bld_srv (bld_srv_id , bld_srv_nm, is_agent, sort_odr, os_typ, srv_sts_id_off, srv_sts_id_active, srv_sts_id_err, del_sts_id) VALUES('BPSV_TRINITY', 'Main Server', false, 1, 'win2008R2', 'server_off', 'server_act', 'server_err', false);
INSERT INTO trinity.bm_bld_srv (bld_srv_id , bld_srv_nm, is_agent, sort_odr, os_typ, srv_sts_id_off, srv_sts_id_active, srv_sts_id_err, del_sts_id) VALUES('BPSV_AGENT_UNI', 'Agent Unix Server', true, 3, 'linux', 'linux_off', 'linux_act', 'linux_err', false);
INSERT INTO trinity.bm_bld_srv (bld_srv_id , bld_srv_nm, is_agent, sort_odr, os_typ, srv_sts_id_off, srv_sts_id_active, srv_sts_id_err, del_sts_id) VALUES('BPSV_AGENT_WIN', 'Agent Windows Server', true, 2, 'win7', 'server_off', 'server_act', 'server_err', false);

