-- ------------------------------------------------------------
-- bm_bld_env_srv
-- ------------------------------------------------------------

INSERT INTO trinity.bm_bld_env_srv (bld_env_id, bld_srv_id, del_sts_id) VALUES ('BPENV_TRINITY', 'BPSV_TRINITY', false);
INSERT INTO trinity.bm_bld_env_srv (bld_env_id, bld_srv_id, del_sts_id) VALUES ('BPENV_TRINITY', 'BPSV_AGENT_UNI', false);
INSERT INTO trinity.bm_bld_env_srv (bld_env_id, bld_srv_id, del_sts_id) VALUES ('BPENV_TRINITY', 'BPSV_AGENT_WIN', false);

INSERT INTO trinity.bm_bld_env_srv (bld_env_id, bld_srv_id, del_sts_id) VALUES ('BPENV_TRINITY_F', 'BPSV_TRINITY', false);
INSERT INTO trinity.bm_bld_env_srv (bld_env_id, bld_srv_id, del_sts_id) VALUES ('BPENV_TRINITY_F', 'BPSV_AGENT_UNI', false);
INSERT INTO trinity.bm_bld_env_srv (bld_env_id, bld_srv_id, del_sts_id) VALUES ('BPENV_TRINITY_F', 'BPSV_AGENT_WIN', false);

INSERT INTO trinity.bm_bld_env_srv (bld_env_id, bld_srv_id, del_sts_id) VALUES ('RPENV_TRINITY_PDT', 'BPSV_TRINITY', false);
INSERT INTO trinity.bm_bld_env_srv (bld_env_id, bld_srv_id, del_sts_id) VALUES ('RPENV_TRINITY_PDT', 'BPSV_AGENT_UNI', false);
INSERT INTO trinity.bm_bld_env_srv (bld_env_id, bld_srv_id, del_sts_id) VALUES ('RPENV_TRINITY_PDT', 'BPSV_AGENT_WIN', false);

