-- ------------------------------------------------------------
-- bm_bld_timeline
-- ------------------------------------------------------------

INSERT INTO trinity.bm_bld_timeline (bld_env_id, bld_line_no, del_sts_id) VALUES ('BPENV_TRINITY', 100, false);
INSERT INTO trinity.bm_bld_timeline (bld_env_id, bld_line_no, del_sts_id) VALUES ('BPENV_TRINITY', 200, false);
INSERT INTO trinity.bm_bld_timeline (bld_env_id, bld_line_no, del_sts_id) VALUES ('BPENV_TRINITY', 300, false);
INSERT INTO trinity.bm_bld_timeline (bld_env_id, bld_line_no, del_sts_id) VALUES ('BPENV_TRINITY', 400, false);
INSERT INTO trinity.bm_bld_timeline (bld_env_id, bld_line_no, del_sts_id) VALUES ('BPENV_TRINITY', 500, false);
INSERT INTO trinity.bm_bld_timeline (bld_env_id, bld_line_no, del_sts_id) VALUES ('BPENV_TRINITY', 600, false);
INSERT INTO trinity.bm_bld_timeline (bld_env_id, bld_line_no, del_sts_id) VALUES ('BPENV_TRINITY', 700, false);
INSERT INTO trinity.bm_bld_timeline (bld_env_id, bld_line_no, del_sts_id) VALUES ('BPENV_TRINITY', 800, false);
INSERT INTO trinity.bm_bld_timeline (bld_env_id, bld_line_no, del_sts_id) VALUES ('BPENV_TRINITY', 900, false);
INSERT INTO trinity.bm_bld_timeline (bld_env_id, bld_line_no, del_sts_id) VALUES ('BPENV_TRINITY', 1000, false);
INSERT INTO trinity.bm_bld_timeline (bld_env_id, bld_line_no, del_sts_id) VALUES ('BPENV_TRINITY', 1100, false);
INSERT INTO trinity.bm_bld_timeline (bld_env_id, bld_line_no, del_sts_id) VALUES ('BPENV_TRINITY', 1200, false);

INSERT INTO trinity.bm_bld_timeline (bld_env_id, bld_line_no, del_sts_id) VALUES ('BPENV_TRINITY_F', 100, false);
INSERT INTO trinity.bm_bld_timeline (bld_env_id, bld_line_no, del_sts_id) VALUES ('BPENV_TRINITY_F', 200, false);
INSERT INTO trinity.bm_bld_timeline (bld_env_id, bld_line_no, del_sts_id) VALUES ('BPENV_TRINITY_F', 300, false);
INSERT INTO trinity.bm_bld_timeline (bld_env_id, bld_line_no, del_sts_id) VALUES ('BPENV_TRINITY_F', 400, false);
INSERT INTO trinity.bm_bld_timeline (bld_env_id, bld_line_no, del_sts_id) VALUES ('BPENV_TRINITY_F', 500, false);
INSERT INTO trinity.bm_bld_timeline (bld_env_id, bld_line_no, del_sts_id) VALUES ('BPENV_TRINITY_F', 600, false);
INSERT INTO trinity.bm_bld_timeline (bld_env_id, bld_line_no, del_sts_id) VALUES ('BPENV_TRINITY_F', 700, false);
INSERT INTO trinity.bm_bld_timeline (bld_env_id, bld_line_no, del_sts_id) VALUES ('BPENV_TRINITY_F', 800, false);
INSERT INTO trinity.bm_bld_timeline (bld_env_id, bld_line_no, del_sts_id) VALUES ('BPENV_TRINITY_F', 900, false);
INSERT INTO trinity.bm_bld_timeline (bld_env_id, bld_line_no, del_sts_id) VALUES ('BPENV_TRINITY_F', 1000, false);
INSERT INTO trinity.bm_bld_timeline (bld_env_id, bld_line_no, del_sts_id) VALUES ('BPENV_TRINITY_F', 1100, false);
INSERT INTO trinity.bm_bld_timeline (bld_env_id, bld_line_no, del_sts_id) VALUES ('BPENV_TRINITY_F', 1200, false);

INSERT INTO trinity.bm_bld_timeline (bld_env_id, bld_line_no, del_sts_id) VALUES ('RPENV_TRINITY_PDT', 100, false);
INSERT INTO trinity.bm_bld_timeline (bld_env_id, bld_line_no, del_sts_id) VALUES ('RPENV_TRINITY_PDT', 200, false);
INSERT INTO trinity.bm_bld_timeline (bld_env_id, bld_line_no, del_sts_id) VALUES ('RPENV_TRINITY_PDT', 300, false);
INSERT INTO trinity.bm_bld_timeline (bld_env_id, bld_line_no, del_sts_id) VALUES ('RPENV_TRINITY_PDT', 400, false);
INSERT INTO trinity.bm_bld_timeline (bld_env_id, bld_line_no, del_sts_id) VALUES ('RPENV_TRINITY_PDT', 500, false);
INSERT INTO trinity.bm_bld_timeline (bld_env_id, bld_line_no, del_sts_id) VALUES ('RPENV_TRINITY_PDT', 600, false);
INSERT INTO trinity.bm_bld_timeline (bld_env_id, bld_line_no, del_sts_id) VALUES ('RPENV_TRINITY_PDT', 700, false);
INSERT INTO trinity.bm_bld_timeline (bld_env_id, bld_line_no, del_sts_id) VALUES ('RPENV_TRINITY_PDT', 800, false);
INSERT INTO trinity.bm_bld_timeline (bld_env_id, bld_line_no, del_sts_id) VALUES ('RPENV_TRINITY_PDT', 900, false);
INSERT INTO trinity.bm_bld_timeline (bld_env_id, bld_line_no, del_sts_id) VALUES ('RPENV_TRINITY_PDT', 1000, false);

