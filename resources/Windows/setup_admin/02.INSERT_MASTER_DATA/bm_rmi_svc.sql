-- ------------------------------------------------------------
-- bm_rmi_svc
-- ------------------------------------------------------------

INSERT INTO trinity.bm_rmi_svc (bld_srv_id, rmi_svc_id, rmi_host_nm, rmi_svc_port, rmi_reg_port, del_sts_id) VALUES ('BPSV_AGENT_UNI', 'IBuildTaskService', 'xxx.xxx.xxx.xxx', 45030, 45031, false);
INSERT INTO trinity.bm_rmi_svc (bld_srv_id, rmi_svc_id, rmi_host_nm, rmi_svc_port, rmi_reg_port, del_sts_id) VALUES ('BPSV_AGENT_UNI', 'IReleaseTaskService', 'xxx.xxx.xxx.xxx', 45040, 45041, false);

INSERT INTO trinity.bm_rmi_svc (bld_srv_id, rmi_svc_id, rmi_host_nm, rmi_svc_port, rmi_reg_port, del_sts_id) VALUES ('BPSV_AGENT_WIN', 'IBuildTaskService', 'xxx.xxx.xxx.xxx', 45030, 45031, false);
INSERT INTO trinity.bm_rmi_svc (bld_srv_id, rmi_svc_id, rmi_host_nm, rmi_svc_port, rmi_reg_port, del_sts_id) VALUES ('BPSV_AGENT_WIN', 'IReleaseTaskService', 'xxx.xxx.xxx.xxx', 45040, 45041, false);

