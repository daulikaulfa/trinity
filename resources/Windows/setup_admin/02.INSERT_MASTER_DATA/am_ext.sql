-- ------------------------------------------------------------
-- am_ext
-- ------------------------------------------------------------

INSERT INTO trinity.am_ext (ext , ext_nm, ext_typ, del_sts_id ) VALUES('bat', 'batファイル', 'ASCII', false);
INSERT INTO trinity.am_ext (ext , ext_nm, ext_typ, del_sts_id ) VALUES('classpath', 'classpathファイル', 'ASCII', false);
INSERT INTO trinity.am_ext (ext , ext_nm, ext_typ, del_sts_id ) VALUES('db', 'dbファイル', 'ASCII', false);
INSERT INTO trinity.am_ext (ext , ext_nm, ext_typ, del_sts_id ) VALUES('txt', 'txtファイル', 'ASCII', false);
INSERT INTO trinity.am_ext (ext , ext_nm, ext_typ, del_sts_id ) VALUES('gif', 'gifファイル', 'BINARY', false);
INSERT INTO trinity.am_ext (ext , ext_nm, ext_typ, del_sts_id ) VALUES('html', 'htmlファイル', 'ASCII', false);
INSERT INTO trinity.am_ext (ext , ext_nm, ext_typ, del_sts_id ) VALUES('java', 'javaファイル', 'ASCII', false);
INSERT INTO trinity.am_ext (ext , ext_nm, ext_typ, del_sts_id ) VALUES('jpg', 'jpgファイル', 'BINARY', false);
INSERT INTO trinity.am_ext (ext , ext_nm, ext_typ, del_sts_id ) VALUES('log', 'logファイル', 'ASCII', false);
INSERT INTO trinity.am_ext (ext , ext_nm, ext_typ, del_sts_id ) VALUES('none', '拡張子なしファイル', 'BINARY', false);
INSERT INTO trinity.am_ext (ext , ext_nm, ext_typ, del_sts_id ) VALUES('pch', 'pchファイル', 'BINARY', false);
INSERT INTO trinity.am_ext (ext , ext_nm, ext_typ, del_sts_id ) VALUES('png', 'pngファイル', 'BINARY', false);
INSERT INTO trinity.am_ext (ext , ext_nm, ext_typ, del_sts_id ) VALUES('project', 'projectファイル', 'ASCII', false);
INSERT INTO trinity.am_ext (ext , ext_nm, ext_typ, del_sts_id ) VALUES('properties', 'propertiesファイル', 'ASCII', false);
INSERT INTO trinity.am_ext (ext , ext_nm, ext_typ, del_sts_id ) VALUES('sh', 'shファイル', 'ASCII', false);
INSERT INTO trinity.am_ext (ext , ext_nm, ext_typ, del_sts_id ) VALUES('ttf', 'ttfファイル', 'ASCII', false);
INSERT INTO trinity.am_ext (ext , ext_nm, ext_typ, del_sts_id ) VALUES('xls', 'xlsファイル', 'BINARY', false);
INSERT INTO trinity.am_ext (ext , ext_nm, ext_typ, del_sts_id ) VALUES('xlsx', 'xlsxファイル', 'BINARY', false);
INSERT INTO trinity.am_ext (ext , ext_nm, ext_typ, del_sts_id ) VALUES('xml', 'xmlファイル', 'ASCII', false);


