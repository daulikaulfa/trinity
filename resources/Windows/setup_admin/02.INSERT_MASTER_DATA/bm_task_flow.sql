-- ------------------------------------------------------------
-- bm_task_flow
-- ------------------------------------------------------------

INSERT INTO trinity.bm_task_flow (task_flow_id, task, del_sts_id) VALUES ('BPTF_TRINITY', '  <?xml version="1.0" encoding="UTF-8"?>
  <task workflowNo="BPTF_TRINITY" xmlns="http://www.blueship.co.jp/tri/fw/schema/beans/task">
  
  <property name="sysDrive" value="C:"/>
  <property name="userDrive" value="C:"/>
  <property name="defsDrive" value="C:"/>

  <property name="workdir" value="{@workspace}/rp"/>

  <property name="concentrated" value="{@workdir}/cnc"/>

  <property name="intensive" value="{@workdir}/int"/>

  <property name="intensive_N_M" value="{@intensive}/N_M"/>

  <property name="intensive_D" value="{@intensive}/D"/>

  <property name="compiledir" value="{@workdir}/cpl"/>

  <property name="logdir" value="{@workdir}/log"/>

  <property name="historydir" value="{@historyPath}/rp"/>

  <property name="ftpRootTrinity_win" value="{@sysDrive}/home/tri/rel"/>

  <property name="ftpRootTrinity_uni" value="/home/tri/rel"/>

  <property name="remotebasedir_win" value="{@ftpRootTrinity_win}/rp/{@lotNo}"/>

  <property name="remotebasedir_uni" value="{@ftpRootTrinity_uni}/rp/{@lotNo}"/>

  <property name="remoteworkdir_win" value="{@remotebasedir_win}/ws"/>

  <property name="remoteworkdir_uni" value="{@remotebasedir_uni}/ws"/>

  <property name="lastTimeCplDir" value="{@sysDrive}/sys/{@lotNo}/LasttimeCpl"/>

  <property name="remoteworkdir_win" value="/rp/{@lotNo}/ws"/>
  
  <property name="remoteworkdir_uni" value="{@ftpRootTrinity}/rp/{@lotNo}/ws"/>

  <property name="tmpDir" value="{@workdir}/tmp"/>

  <target doSimulate="0" forceExecute="0" name="初期処理" sequenceNo="000100">

    <delete doDetailSnap="0" doNotExistSkip="1" doSimulate="0" sequence="1">
      <param src="{@workdir}"/>
    </delete>

    <mkdir doDetailSnap="0" doExistSkip="1" sequence="2">
      <param src="{@workdir}"/>
      <param src="{@intensive_N_M}"/>
      <param src="{@intensive_D}"/>
      <param src="{@historyRelUnitDSLPath}"/>
      <param src="{@lastTimeCplDir}"/>
      <param src="{@tmpDir}"/>
    </mkdir>

    <result/>
    <resource off="" active="" error=""/>
  </target>
  <target doSimulate="0" forceExecute="0" name="資産集約処理" sequenceNo="000200">

    <mkdir doDetailSnap="0" doExistSkip="0" sequence="1">
      <param src="{@concentrated}"/>
    </mkdir>

    <concentrate concentrateDir="{@concentrated}" sequence="2">
      <param applyNo="{@applyNo}"/>
      <param applyNo="{@delApplyNo}"/>
    </concentrate>

    <diff destDeleteFilePath="{@intensive_D}" destModifiedFilePath="{@intensive_N_M}" destNewFilePath="{@intensive_N_M}" diffPath="{@workPath}" doCopy="1" doDetailSnap="0" doNeglect="1" doReverse="1" holdTimestamp="1" sequence="3" srcPath="{@concentrated}"/>

    <mkdir doDetailSnap="0" doExistSkip="0" sequence="4">
      <param src="{@workdir}/MW"/>
    </mkdir>

    <copy doDetailSnap="0" doNeglect="1" doNotExistSkip="1" holdTimestamp="1" sequence="5">
      <fileset dest="{@workdir}/MW" src="{@workPath}"/>
      <overwriteInfo doAdd="1" doUpdateNew="1" doUpdateOld="1"/>
    </copy>

  </target>

  <target doSimulate="0" forceExecute="0" name="資産転送SEND" sequenceNo="000300">

    <zip dest="{@workdir}/MASTER_WORK_UTF-8.zip" doDetailSnap="0" doNotExistSkip="1" encoding="UTF-8" sequence="1">
      <param src="{@workdir}/MW"/>
    </zip>

    <zip dest="{@workdir}/MASTER_WORK.zip" doDetailSnap="0" doNotExistSkip="1" encoding="MS932" sequence="2">
      <param src="{@workdir}/MW"/>
    </zip>

    <delete doDetailSnap="0" doNotExistSkip="1" doSimulate="0" sequence="3">
      <param src="{@workdir}/MW"/>
    </delete>

    <zip dest="{@workdir}/intensive_UTF-8.zip" doDetailSnap="0" doNotExistSkip="1" encoding="UTF-8" sequence="4">
      <param src="{@intensive}"/>
    </zip>

    <zip dest="{@workdir}/intensive.zip" doDetailSnap="0" doNotExistSkip="1" encoding="MS932" sequence="5">
      <param src="{@intensive}"/>
    </zip>

    <ftp doDetailSnap="0" doPassivemode="0" doSimulate="0" encoding="SJIS" port="21" server="RP_AGENT_WIN" userid="user" sequence="6" >
      <put fileType="2" sequence="1">
        <param dest="{@remoteworkdir_win}" src="{@workdir}/MASTER_WORK.zip"/>
      </put>
      <put fileType="2" sequence="2">
        <param dest="{@remoteworkdir_win}" src="{@workdir}/intensive.zip"/>
      </put>
      <put fileType="2" sequence="3">
        <param dest="{@remoteworkdir_win}" src="{@lastTimeCplDir}/cpl_AGENT_WIN.zip"/>
      </put>
    </ftp>

    <ftp doDetailSnap="0" doPassivemode="0" doSimulate="0" encoding="UTF-8" port="21" server="RP_AGENT_UNI" userid="root" sequence="7">
      <put fileType="2" sequence="1">
        <param dest="{@remoteworkdir_uni}" src="{@workdir}/MASTER_WORK_UTF-8.zip"/>
      </put>
      <put fileType="2" sequence="2">
        <param dest="{@remoteworkdir_uni}" src="{@workdir}/intensive.zip"/>
      </put>
      <put fileType="2" sequence="3">
        <param dest="{@remoteworkdir_uni}" src="{@lastTimeCplDir}/cpl_AGENT_UNI.zip"/>
      </put>
    </ftp>

  </target>

  <target doSimulate="0" name="資産転送GET(LM)" sequenceNo="000900">

    <ftp doDetailSnap="0" doPassivemode="0" doSimulate="0" encoding="SJIS" port="21" server="RP_AGENT_WIN" userid="user" sequence="1">
      <get fileType="2" sequence="1">
        <param dest="{@workdir}" src="{@remoteworkdir_win}/artifact_AGENT_WIN.zip"/>
      </get>
      <get fileType="2" sequence="2">
        <param dest="{@tmpDir}" src="{@remoteworkdir_win}/cpl_AGENT01.zip"/>
      </get>
    </ftp>

    <ftp doDetailSnap="0" doPassivemode="0" doSimulate="0" encoding="UTF-8" port="21" server="RP_AGENT_UNI" userid="root" sequence="2">
      <get fileType="2" sequence="1">
        <param dest="{@workdir}" src="{@remoteworkdir_uni}/artifact_AGENT_UNI.zip"/>
      </get>
      <get fileType="2" sequence="2">
        <param dest="{@tmpDir}" src="{@remoteworkdir_uni}/cpl_AGENT_UNI.zip"/>
      </get>
    </ftp>

  </target>

  <target doSimulate="0" forceExecute="1" name="資産転送GET(log)" sequenceNo="001000">

    <ftp doDetailSnap="0" doPassivemode="0" doSimulate="0" encoding="SJIS" port="21" server="RP_AGENT_WIN" userid="user" sequence="1">
      <get fileType="2" sequence="1">
        <param dest="{@workdir}" src="{@remoteworkdir_win}/log_AGENT_WIN.zip"/>
      </get>
    </ftp>
    <ftp doDetailSnap="0" doPassivemode="0" doSimulate="0" encoding="UTF-8" port="21" server="RP_AGENT_UNI" userid="root" sequence="2">
      <get fileType="2" sequence="1">
        <param dest="{@workdir}" src="{@remoteworkdir_uni}/log_AGENT_UNI.zip"/>
      </get>
    </ftp>

  </target>

  <target doSimulate="0" forceExecute="0" name="アーカイブ" sequenceNo="001100">

    <copy doDetailSnap="0" doNeglect="1" doNotExistSkip="1" holdTimestamp="1" sequence="1">
      <fileset dest="{@historyRelUnitDSLPath}/artifact_AGENT_WIN.zip" src="{@workdir}/artifact_AGENT_WIN.zip"/>
      <fileset dest="{@historyRelUnitDSLPath}/artifact_AGENT_UNI.zip" src="{@workdir}/artifact_AGENT_UNI.zip"/>
      <overwriteInfo doAdd="1" doUpdateNew="1" doUpdateOld="1"/>
    </copy>

    <copy doDetailSnap="0" doNeglect="1" doNotExistSkip="1" holdTimestamp="1" sequence="2">
      <fileset dest="{@lastTimeCplDir}/cpl_AGENT_WIN.zip" src="{@tmpDir}/cpl_AGENT_WIN.zip"/>
      <overwriteInfo doAdd="1" doUpdateNew="1" doUpdateOld="1"/>
    </copy>

    <copy doDetailSnap="0" doNeglect="1" doNotExistSkip="1" holdTimestamp="1" sequence="3">
      <fileset dest="{@lastTimeCplDir}/cpl_AGENT_UNI.zip" src="{@tmpDir}/cpl_AGENT_UNI.zip"/>
      <overwriteInfo doAdd="1" doUpdateNew="1" doUpdateOld="1"/>
    </copy>

    <delete doDetailSnap="0" doNotExistSkip="1" doSimulate="0" sequence="5">
      <param src="{@tmpDir}"/>
    </delete>

  </target>
  <target doSimulate="0" forceExecute="1" name="終了処理" sequenceNo="001200">

    <unzip doDetailSnap="0" doNotExistSkip="1" sequence="1">
      <param dest="{@historyRelUnitDSLPath}" encoding="MS932" src="{@workdir}/log_AGENT_WIN.zip"/>
    </unzip>
    <unzip doDetailSnap="0" doNotExistSkip="1" sequence="2">
      <param dest="{@historyRelUnitDSLPath}" encoding="UTF-8" src="{@workdir}/log_AGENT_UNI.zip"/>
    </unzip>

  </target>

</task>', false);

INSERT INTO trinity.bm_task_flow (task_flow_id, task, del_sts_id) VALUES ('BPTF_AGENT_UNI', '  <?xml version="1.0" encoding="UTF-8"?>
  <task workflowNo="BPTF_AGENT_UNI" xmlns="http://www.blueship.co.jp/tri/fw/schema/beans/task">

  <property name="ftpRootTrinity" value="/home/tri/rel"/>

  <property name="remotebasedir" value="{@ftpRootTrinity}/rp/{@lotNo}"/>
  <property name="remoteworkdir" value="{@remotebasedir}/ws"/>

  <property name="compiledir" value="/libw/{@lotNo}/cpl"/>

  <property name="masterdir" value="{@remoteworkdir}/MW"/>
  <property name="intensive" value="{@remoteworkdir}/int"/>
  <property name="intensive_N_M" value="{@intensive}/N_M"/>
  <property name="intensive_D" value="{@intensive}/D"/>
  <property name="artifact" value="{@remoteworkdir}/atf"/>
  <property name="artifact_N_M" value="{@artifact}/N_M"/>
  <property name="artifact_D" value="{@artifact}/D"/>
  <property name="logdir" value="{@remoteworkdir}/log"/>

  <target doSimulate="0" forceExecute="0" name="初期処理" sequenceNo="000100">

    <delete doDetailSnap="0" doNotExistSkip="1" doSimulate="0" sequence="1">
      <param src="{@remoteworkdir}"/>
      <param src="{@compiledir}"/>
    </delete>

    <mkdir doDetailSnap="0" doExistSkip="1" sequence="2">
      <param src="{@remoteworkdir}"/>
      <param src="{@compiledir}"/>
      <param src="{@logdir}"/>
      <param src="{@artifact_N_M}"/>
      <param src="{@artifact_D}"/>
    </mkdir>
    <result/>
    <resource off="" active="" error=""/>
  </target>

  <target doSimulate="0" forceExecute="0" name="プレコンパイル準備" sequenceNo="000400">

    <unzip doDetailSnap="0" doNotExistSkip="1" sequence="1">
      <param dest="{@remoteworkdir}" encoding="UTF-8" src="{@remoteworkdir}/MASTER_WORK_UTF-8.zip"/>
    </unzip>

    <delete doDetailSnap="0" doNotExistSkip="1" doSimulate="0" sequence="2">
      <param src="{@remoteworkdir}/MASTER_WORK_UTF-8.zip"/>
    </delete>

    <unzip doDetailSnap="0" doNotExistSkip="1" sequence="3">
      <param dest="{@remoteworkdir}" encoding="UTF-8" src="{@remoteworkdir}/intensive_UTF-8.zip"/>
    </unzip>

    <delete doDetailSnap="0" doNotExistSkip="1" doSimulate="0" sequence="4">
      <param src="{@remoteworkdir}/intensive_UTF-8.zip"/>
    </delete>

    <unzip doDetailSnap="0" doNotExistSkip="1" sequence="5">
      <param dest="{@remoteworkdir}" encoding="UTF-8" src="{@remoteworkdir}/cpl_AGENT_UNI.zip"/>
    </unzip>

    <delete doDetailSnap="0" doNotExistSkip="1" doSimulate="0" sequence="6">
      <param src="{@remoteworkdir}/cpl_AGENT_UNI.zip"/>
    </delete>

    <copy doDetailSnap="0" doNeglect="1" doNotExistSkip="1" holdTimestamp="0" sequence="7">
      <fileset dest="{@compiledir}" src="{@masterdir}/"/>
      <overwriteInfo doAdd="1" doUpdateNew="1" doUpdateOld="1"/>
    </copy>

    <copy doDetailSnap="0" doNeglect="1" doNotExistSkip="1" holdTimestamp="1" sequence="8">
      <fileset dest="{@compiledir}" src="{@intensive_N_M}/"/>
      <overwriteInfo doAdd="1" doUpdateNew="1" doUpdateOld="1"/>
    </copy>

    <delete doDetailSnap="0" doNotExistSkip="1" sequence="9">
      <param src="{@compiledir}/{@masterDeletePath}"/>
    </delete>

    <delete doDetailSnap="0" doNotExistSkip="1" sequence="10">
      <param src="{@remoteworkdir}/cpl_AGENT_UNI/{@masterBinaryDeletePath}"/>
    </delete>

    <changePermission doDetailSnap="0" sequence="11">
      <param doRecursion="1" path="{@compiledir}" permission="755"/>
    </changePermission>

  </target>

  <target doSimulate="0" forceExecute="0" name="差分コンパイルA" sequenceNo="000500">
    <cBuild errLogPath="{@logdir}/cplB.log" executeShell="{@compiledir}/trinity/lib/Redship/groupA/apl/Java2D/src/java2d/compile.sh" outLogPath="{@logdir}/cplB.err" sequence="1">
      <buildPath>
        <src path="{@compiledir}/trinity/lib/Redship/groupA/apl/Java2D/"/>
        <intensiveSrc path="{@intensive_N_M}/trinity/lib/Redship/groupA/"/>
        <deleteSrc path="{@intensive_D}/trinity/lib/Redship/groupA/"/>
      </buildPath>
    </cBuild>

  </target>

  <target doSimulate="0" forceExecute="0" name="差分抽出" sequenceNo="000600">

    <diff destDeleteFilePath="{@artifact_D}" destModifiedFilePath="{@artifact_N_M}" destNewFilePath="{@artifact_N_M}" diffPath="{@remoteworkdir}/cpl_AGENT_UNI" doCopy="1" doDetailSnap="0" doNeglect="1" doReverse="1" holdTimestamp="1"  srcPath="{@compiledir}" sequence="1"/>

    <delete doDetailSnap="0" doNotExistSkip="1" sequence="2">
      <param src="{@remoteworkdir}/cpl_AGENT_UNI"/>
    </delete>

    <copy doDetailSnap="0" doNeglect="1" doNotExistSkip="1" holdTimestamp="1" sequence="3">
      <fileset dest="{@remoteworkdir}/cpl_AGENT_UNI/" src="{@compiledir}/"/>
      <overwriteInfo doAdd="1" doUpdateNew="1" doUpdateOld="1"/>
    </copy>

    <zip dest="{@remoteworkdir}/cpl_AGENT_UNI.zip" doDetailSnap="0" doNotExistSkip="1" encoding="UTF-8" sequence="4">
      <param src="{@remoteworkdir}/cpl_AGENT_UNI/"/>
    </zip>

  </target>

  <target doSimulate="0" forceExecute="0" name="資産転送準備(LM)" sequenceNo="000700">

    <zip dest="{@remoteworkdir}/artifact_AGENT_UNI.zip" doDetailSnap="0" doNotExistSkip="1" encoding="UTF-8" sequence="1">
      <param src="{@artifact}"/>
    </zip>

  </target>

  <target doSimulate="0" forceExecute="1" name="資産転送準備(LOG)" sequenceNo="000800">
    <zip dest="{@remoteworkdir}/log_AGENT_UNI.zip" doDetailSnap="0" doNotExistSkip="1" encoding="UTF-8" sequence="1">
      <param src="{@logdir}"/>
    </zip>

  </target>
</task>', false);

INSERT INTO trinity.bm_task_flow (task_flow_id, task, del_sts_id) VALUES ('BPTF_AGENT_WIN', '  <?xml version="1.0" encoding="UTF-8"?>
  <task workflowNo="BPTF_TRINITY_WIN" xmlns="http://www.blueship.co.jp/tri/fw/schema/beans/task">

  <property name="workDrive" value="C:"/>

  <property name="ftpRootTrinity" value="{@workDrive}/home/tri/rel"/>

  <property name="remotebasedir" value="{@ftpRootTrinity}/rp/{@lotNo}"/>

  <property name="remoteworkdir" value="{@remotebasedir}/ws"/>

  <property name="compiledir" value="{@workDrive}/libw/{@lotNo}/cpl"/>

  <property name="masterdir" value="{@remoteworkdir}/MW"/>

  <property name="intensive" value="{@remoteworkdir}/int"/>

  <property name="intensive_N_M" value="{@intensive}/N_M"/>

  <property name="intensive_D" value="{@intensive}/D"/>

  <property name="artifact" value="{@remoteworkdir}/atf"/>

  <property name="artifact_N_M" value="{@artifact}/N_M"/>

  <property name="artifact_D" value="{@artifact}/D"/>

  <property name="logdir" value="{@remoteworkdir}/log"/>

  <target doSimulate="0" forceExecute="0" name="初期処理" sequenceNo="000100">

    <delete doDetailSnap="0" doNotExistSkip="1" doSimulate="0" sequence="1">
      <param src="{@remoteworkdir}"/>
      <param src="{@compiledir}"/>
    </delete>

    <mkdir doDetailSnap="0" doExistSkip="1" sequence="2">
      <param src="{@remoteworkdir}"/>
      <param src="{@compiledir}"/>
      <param src="{@logdir}"/>
      <param src="{@artifact_N_M}"/>
      <param src="{@artifact_D}"/>
    </mkdir>

    <result/>
    <resource off="" active="" error=""/>
  </target>
  <target doSimulate="0" forceExecute="0" name="差分コンパイル準備" sequenceNo="000400">

    <unzip doDetailSnap="0" doNotExistSkip="1" sequence="1">
      <param dest="{@remoteworkdir}" encoding="MS932" src="{@remoteworkdir}/MASTER_WORK.zip"/>
    </unzip>

    <delete doDetailSnap="0" doNotExistSkip="1" doSimulate="0" sequence="2">
      <param src="{@remoteworkdir}/MASTER_WORK.zip"/>
    </delete>

    <unzip doDetailSnap="0" doNotExistSkip="1" sequence="3">
      <param dest="{@remoteworkdir}" encoding="MS932" src="{@remoteworkdir}/intensive.zip"/>
    </unzip>

    <delete doDetailSnap="0" doNotExistSkip="1" doSimulate="0" sequence="4">
      <param src="{@remoteworkdir}/intensive.zip"/>
    </delete>

    <unzip doDetailSnap="0" doNotExistSkip="1" sequence="5">
      <param dest="{@remoteworkdir}" encoding="MS932" src="{@remoteworkdir}/cpl_AGENT_WIN.zip"/>
    </unzip>

    <delete doDetailSnap="0" doNotExistSkip="1" doSimulate="0" sequence="6">
      <param src="{@remoteworkdir}/cpl_AGENT_WIN.zip"/>
    </delete>

    <copy doDetailSnap="0" doNeglect="1" doNotExistSkip="1" holdTimestamp="1" sequence="7">
      <fileset dest="{@compiledir}" src="{@masterdir}/"/>
      <overwriteInfo doAdd="1" doUpdateNew="1" doUpdateOld="1"/>
    </copy>

    <copy doDetailSnap="0" doNeglect="1" doNotExistSkip="1" holdTimestamp="1" sequence="8">
      <fileset dest="{@compiledir}" src="{@intensive_N_M}/"/>
      <overwriteInfo doAdd="1" doUpdateNew="1" doUpdateOld="1"/>
    </copy>

    <delete doDetailSnap="0" doNotExistSkip="1" sequence="9">
      <param src="{@compiledir}/{@masterDeletePath}"/>
    </delete>

    <delete doDetailSnap="0" doNotExistSkip="1" sequence="10">
      <param src="{@remoteworkdir}/cpl_AGENT_WIN/{@masterBinaryDeletePath}"/>
    </delete>


  </target>

  <target doSimulate="0" forceExecute="0" name="差分コンパイルA" sequenceNo="000500">
    <cBuild errLogPath="{@logdir}/cplA.log" executeShell="{@compiledir}/trinity/lib/Blueship/groupA/apl/Java2D/compile.bat" outLogPath="{@logdir}/cplA.err" sequence="1">
      <buildPath>
        <src path="{@compiledir}/trinity/lib/Blueship/groupA/apl/Java2D/"/>
        <intensiveSrc path="{@intensive_N_M}/trinity/lib/Blueship/groupA/"/>
        <deleteSrc path="{@intensive_D}/trinity/lib/Blueship/groupA/"/>
      </buildPath>
    </cBuild>

  </target>

  <target doSimulate="0" forceExecute="0" name="差分抽出" sequenceNo="000600">

    <diff destDeleteFilePath="{@artifact_D}" destModifiedFilePath="{@artifact_N_M}" destNewFilePath="{@artifact_N_M}" diffPath="{@remoteworkdir}/cpl_AGENT_WIN" doCopy="1" doDetailSnap="0" doNeglect="1" doReverse="1" holdTimestamp="1"  srcPath="{@compiledir}" sequence="1"/>

    <delete doDetailSnap="0" doNotExistSkip="1" sequence="2">
      <param src="{@remoteworkdir}/cpl_AGENT_WIN/"/>
    </delete>

    <copy doDetailSnap="0" doNeglect="1" doNotExistSkip="1" holdTimestamp="1" sequence="3">
      <fileset dest="{@remoteworkdir}/cpl_AGENT_WIN/" src="{@compiledir}/"/>
      <overwriteInfo doAdd="1" doUpdateNew="1" doUpdateOld="1"/>
    </copy>

    <zip dest="{@remoteworkdir}/cpl_AGENT_WIN.zip" doDetailSnap="0" doNotExistSkip="1" encoding="MS932" sequence="4">
      <param src="{@remoteworkdir}/cpl_AGENT_WIN/"/>
    </zip>

  </target>

  <target doSimulate="0" forceExecute="0" name="資産転送準備(LM)" sequenceNo="000700">
    <zip dest="{@remoteworkdir}/artifact_AGENT_WIN.zip" doDetailSnap="0" doNotExistSkip="1" encoding="MS932" sequence="1">
      <param src="{@artifact}"/>
    </zip>

  </target>

  <target doSimulate="0" forceExecute="1" name="資産転送準備(LOG)" sequenceNo="000800">

    <zip dest="{@remoteworkdir}/log_AGENT_WIN.zip" doDetailSnap="0" doNotExistSkip="1" encoding="MS932" sequence="1">
      <param src="{@logdir}"/>
    </zip>

  </target>
</task>', false);

INSERT INTO trinity.bm_task_flow (task_flow_id, task, del_sts_id) VALUES ('BPTF_TRINITY_F', '  <?xml version="1.0" encoding="UTF-8"?>
  <task workflowNo="BPTF_TRINITY_F" xmlns="http://www.blueship.co.jp/tri/fw/schema/beans/task">

  <property name="sysDrive" value="C:"/>
  <property name="userDrive" value="C:"/>
  <property name="defsDrive" value="C:"/>

  <property name="workdir" value="{@workspace}/rp"/>

  <property name="logdir" value="{@workdir}/log"/>

  <property name="masterdir" value="{@workPath}"/>
  
  <property name="historydir" value="{@historyPath}/rp"/>
  
  <property name="ftpRootTrinity_win" value="{@sysDrive}/home/trinity/trinity_release"/>

  <property name="ftpRootTrinity_uni" value="/home/tri/rel"/>

  <property name="remotebasedir_win" value="{@ftpRootTrinity_win}/rp/{@lotNo}"/>

  <property name="remotebasedir_uni" value="{@ftpRootTrinity_uni}/rp/{@lotNo}"/>

  <property name="remoteworkdir_win" value="{@remotebasedir_win}/ws"/>

  <property name="remoteworkdir_uni" value="{@remotebasedir_uni}/ws"/>
  
  <target forceExecute="0" name="初期処理" sequenceNo="000100">
  

    <delete doDetailSnap="0" doNotExistSkip="1" doSimulate="0" sequence="1">
      <param src="{@workdir}"/>
    </delete>
  
    <mkdir doDetailSnap="0" doExistSkip="0" sequence="2">
      <param src="{@workdir}"/>
      <param src="{@historyRelUnitDSLPath}"/>
      <param src="{@workdir}/MW"/>
    </mkdir>
  
  </target>
  
  <target doSimulate="0" forceExecute="0" name="資産集約処理" sequenceNo="000200">
    
    <copy doDetailSnap="0" doNeglect="1" doNotExistSkip="1" holdTimestamp="1" sequence="1">
      <fileset dest="{@workdir}/MW" src="{@masterdir}"/>
      <overwriteInfo doAdd="1" doUpdateNew="1" doUpdateOld="1"/>
    </copy>
  
  </target>
  
  <target doSimulate="0" forceExecute="0" name="資産転送SEND" sequenceNo="000300">

    <zip dest="{@workdir}/MASTER_WORK_UTF-8.zip" doDetailSnap="0" doNotExistSkip="1" encoding="UTF-8" sequence="1">
      <param src="{@workdir}/MW"/>
    </zip>

    <zip dest="{@workdir}/MASTER_WORK.zip" doDetailSnap="0" doNotExistSkip="1" encoding="MS932" sequence="2">
      <param src="{@workdir}/MW"/>
    </zip>

    <delete doDetailSnap="0" doNotExistSkip="1" doSimulate="0" sequence="3">
      <param src="{@workdir}/MW"/>
    </delete>

    <ftp doDetailSnap="0" doPassivemode="0" doSimulate="0" encoding="SJIS" port="21" server="RP_AGENT_WIN" userid="user" sequence="4" >
      <put fileType="2" sequence="1">
        <param dest="{@remoteworkdir_win}" src="{@workdir}/MASTER_WORK.zip"/>
      </put>
    </ftp>

    <ftp doDetailSnap="0" doPassivemode="0" doSimulate="0" encoding="UTF-8" port="21" server="RP_AGENT_UNI" userid="root" sequence="5">
      <put fileType="2" sequence="1">
        <param dest="{@remoteworkdir_uni}" src="{@workdir}/MASTER_WORK_UTF-8.zip"/>
      </put>
    </ftp>

  </target>

 <target doSimulate="0" name="資産転送GET(LM)" sequenceNo="000900">

    <ftp doDetailSnap="0" doPassivemode="0" doSimulate="0" encoding="SJIS" port="21" server="RP_AGENT_WIN" userid="user" sequence="1">
      <get fileType="2" sequence="1">
        <param dest="{@workdir}" src="{@remoteworkdir_win}/artifact_AGENT_WIN.zip"/>
      </get>
    </ftp>

    <ftp doDetailSnap="0" doPassivemode="0" doSimulate="0" encoding="UTF-8" port="21" server="RP_AGENT_UNI" userid="root" sequence="2">
      <get fileType="2" sequence="1">
        <param dest="{@workdir}" src="{@remoteworkdir_uni}/artifact_AGENT_UNI.zip"/>
      </get>
    </ftp>

  </target>

  <target doSimulate="0" forceExecute="1" name="資産転送GET(log)" sequenceNo="001000">

    <ftp doDetailSnap="0" doPassivemode="0" doSimulate="0" encoding="SJIS" port="21" server="RP_AGENT_WIN" userid="user" sequence="1">
      <get fileType="2" sequence="1">
        <param dest="{@workdir}" src="{@remoteworkdir_win}/log_AGENT_WIN.zip"/>
      </get>
    </ftp>
    <ftp doDetailSnap="0" doPassivemode="0" doSimulate="0" encoding="UTF-8" port="21" server="RP_AGENT_UNI" userid="root" sequence="2">
      <get fileType="2" sequence="1">
        <param dest="{@workdir}" src="{@remoteworkdir_uni}/log_AGENT_UNI.zip"/>
      </get>
    </ftp>

  </target>

  <target doSimulate="0" forceExecute="0" name="アーカイブ" sequenceNo="001100">

    <copy doDetailSnap="0" doNeglect="1" doNotExistSkip="1" holdTimestamp="1" sequence="1">
      <fileset dest="{@historyRelUnitDSLPath}/artifact_AGENT_WIN.zip" src="{@workdir}/artifact_AGENT_WIN.zip"/>
      <fileset dest="{@historyRelUnitDSLPath}/artifact_AGENT_UNI.zip" src="{@workdir}/artifact_AGENT_UNI.zip"/>
      <overwriteInfo doAdd="1" doUpdateNew="1" doUpdateOld="1"/>
    </copy>

  </target>

  <target doSimulate="0" forceExecute="1" name="終了処理" sequenceNo="001200">

    <unzip doDetailSnap="0" doNotExistSkip="1" sequence="1">
      <param dest="{@historyRelUnitDSLPath}" encoding="MS932" src="{@workdir}/log_AGENT_WIN.zip"/>
    </unzip>
    <unzip doDetailSnap="0" doNotExistSkip="1" sequence="2">
      <param dest="{@historyRelUnitDSLPath}" encoding="UTF-8" src="{@workdir}/log_AGENT_UNI.zip"/>
    </unzip>

  </target>
</task>', false);

INSERT INTO trinity.bm_task_flow (task_flow_id, task, del_sts_id) VALUES ('BPTF_AGENT_UNI_F', '  <?xml version="1.0" encoding="UTF-8"?>
  <task workflowNo="BPTF_AGENT_UNI_F" xmlns="http://www.blueship.co.jp/tri/fw/schema/beans/task">

  <property name="ftpRootTrinity" value="/home/trinity/trinity_release"/>
  <property name="remotebasedir" value="{@ftpRootTrinity}/rpbuild_r/{@lotNo}"/>
  <property name="remoteworkdir" value="{@remotebasedir}/workspace"/>

  <property name="compiledir" value="/libw/{@lotNo}/cpl"/>
  
  <property name="masterdir" value="{@remoteworkdir}/MW"/>
  <property name="intensive" value="{@remoteworkdir}/int"/>
  <property name="intensive_N_M" value="{@intensive}/N_M"/>
  <property name="intensive_D" value="{@intensive}/D"/>
  
  <property name="artifact" value="{@remoteworkdir}/atf"/>
  <property name="artifact_N_M" value="{@artifact}/N_M"/>
  <property name="artifact_D" value="{@artifact}/D"/>
  
  <property name="logdir" value="{@remoteworkdir}/log"/>
  
  <target doSimulate="0" forceExecute="0" name="初期処理" sequenceNo="000100">

    <delete doDetailSnap="0" doNotExistSkip="1" doSimulate="0" sequence="1">
      <param src="{@remoteworkdir}"/>
      <param src="{@compiledir}"/>
    </delete>

    <mkdir doDetailSnap="0" doExistSkip="1" sequence="2">
      <param src="{@remoteworkdir}"/>
      <param src="{@compiledir}"/>
      <param src="{@artifact_N_M}"/>
      <param src="{@artifact_D}"/>
    </mkdir>

  </target>

  <target doSimulate="0" forceExecute="0" name="全量コンパイル準備" sequenceNo="000400">
    
    <unzip doDetailSnap="0" doNotExistSkip="1" sequence="1">
      <param dest="{@remoteworkdir}" src="{@remoteworkdir}/MASTER_WORK_UTF-8.zip" encoding="UTF-8"/>
    </unzip>
  
    <delete doDetailSnap="0" doNotExistSkip="1" doSimulate="0" sequence="2">
      <param src="{@remoteworkdir}/MASTER_WORK_UTF-8.zip"/>
    </delete>
  
    <copy doDetailSnap="0" doNeglect="1" doNotExistSkip="1" holdTimestamp="1" sequence="3">
      <fileset dest="{@compiledir}" src="{@masterdir}"/>
      <overwriteInfo doAdd="1" doUpdateNew="0" doUpdateOld="0"/>
    </copy>
  
  </target>
    
  <target doSimulate="0" forceExecute="0" name="全量コンパイルB" sequenceNo="000500">
  
    <cBuild errLogPath="{@logdir}/errB_N_M.err" executeShell="{@compiledir}/lib/groupA/apl/SwingSet/compile.bat" outLogPath="{@logdir}/outB_N_M.out" sequence="1">

      <buildPath>
        <intensiveSrc path="{@compiledir}/lib/groupA/apl/SwingSet/src"/>
      </buildPath>

    </cBuild>
  
  </target>
  
  <target doSimulate="0" forceExecute="0" name="資産転送準備(LM)" sequenceNo="000700">
  
    <copy doDetailSnap="0" doNeglect="1" doNotExistSkip="1" holdTimestamp="1" sequence="1">
      <fileset dest="{@artifact_N_M}/lib" src="{@compiledir}/lib"/>
      <overwriteInfo doAdd="1" doUpdateNew="0" doUpdateOld="0"/>
    </copy>
  
    <zip dest="{@remoteworkdir}/artifact_AGENT_UNI.zip" doDetailSnap="0" doNotExistSkip="1" encoding="UTF-8" sequence="2">
      <param src="{@artifact}"/>
    </zip>
  
  </target>
  
  <target doSimulate="0" forceExecute="1" name="資産転送準備(LOG)" sequenceNo="000900">
  
    <zip dest="{@remoteworkdir}/log_AGENT_UNI.zip" doDetailSnap="0" doNotExistSkip="1" encoding="SJIS" sequence="1">
      <param src="{@logdir}"/>
    </zip>
  
  </target>

</task>', false);

INSERT INTO trinity.bm_task_flow (task_flow_id, task, del_sts_id) VALUES ('BPTF_AGENT_WIN_F', '  <?xml version="1.0" encoding="UTF-8"?>
  <task workflowNo="BPTF_AGENT_WIN_F" xmlns="http://www.blueship.co.jp/tri/fw/schema/beans/task">

  <property name="workDrive" value="C:"/>

  <property name="ftpRootTrinity" value="{@workDrive}/home/tri/rel"/>

  <property name="remotebasedir" value="{@ftpRootTrinity}/rp/{@lotNo}"/>

  <property name="remoteworkdir" value="{@remotebasedir}/ws"/>

  <property name="compiledir" value="{@workDrive}/libw/{@lotNo}/cpl"/>

  <property name="masterdir" value="{@remoteworkdir}/MW"/>

  <property name="intensive" value="{@remoteworkdir}/int"/>

  <property name="intensive_N_M" value="{@intensive}/N_M"/>

  <property name="intensive_D" value="{@intensive}/D"/>

  <property name="artifact" value="{@remoteworkdir}/atf"/>

  <property name="artifact_N_M" value="{@artifact}/N_M"/>

  <property name="artifact_D" value="{@artifact}/D"/>

  <property name="logdir" value="{@remoteworkdir}/log"/>

  
  <target doSimulate="0" forceExecute="0" name="初期処理" sequenceNo="000100">

    <delete doDetailSnap="0" doNotExistSkip="1" doSimulate="0" sequence="1">
      <param src="{@remoteworkdir}"/>
      <param src="{@compiledir}"/>
    </delete>

    <mkdir doDetailSnap="0" doExistSkip="1" sequence="2">
      <param src="{@remoteworkdir}"/>
      <param src="{@compiledir}"/>
      <param src="{@artifact_N_M}"/>
      <param src="{@artifact_D}"/>
    </mkdir>

  </target>

  <target doSimulate="0" forceExecute="0" name="全量コンパイル準備" sequenceNo="000400">
  
    <unzip doDetailSnap="0" doNotExistSkip="1" sequence="1">
      <param dest="{@remoteworkdir}" src="{@remoteworkdir}/MASTER_WORK.zip" encoding="SJIS"/>
    </unzip>
  
    <delete doDetailSnap="0" doNotExistSkip="1" doSimulate="0" sequence="2">
      <param src="{@remoteworkdir}/MASTER_WORK.zip"/>
    </delete>
  
    <copy doDetailSnap="0" doNeglect="1" doNotExistSkip="1" holdTimestamp="1" sequence="3">
      <fileset dest="{@compiledir}" src="{@masterdir}"/>
      <overwriteInfo doAdd="1" doUpdateNew="0" doUpdateOld="0"/>
    </copy>
  
  </target>
  
  <target doSimulate="0" forceExecute="0" name="全量コンパイルA" sequenceNo="000500">
  
    <cBuild errLogPath="{@logdir}/errA_N_M.err" executeShell="{@compiledir}/lib/groupA/apl/Java2D/compile.bat" outLogPath="{@logdir}/outA_N_M.out" sequence="1">
  
      <buildPath>
        <intensiveSrc path="{@compiledir}/lib/groupA/apl/Java2D/src/java2d"/>
      </buildPath>
  
    </cBuild>
  
  </target>
  
  <target doSimulate="0" forceExecute="0" name="資産転送準備(LM)" sequenceNo="000800">
  
    <copy doDetailSnap="0" doNeglect="1" doNotExistSkip="1" holdTimestamp="1" sequence="1">
      <fileset dest="{@artifact_N_M}/lib" src="{@compiledir}/lib"/>
      <overwriteInfo doAdd="1" doUpdateNew="0" doUpdateOld="0"/>
    </copy>
  
    <zip dest="{@remoteworkdir}/artifact_AGENT_WIN.zip" doDetailSnap="0" doNotExistSkip="1" encoding="MS932" sequence="2">
      <param src="{@artifact}"/>
    </zip>
  
  </target>
  
  <target doSimulate="0" forceExecute="1" name="資産転送準備(LOG)" sequenceNo="000900">
  
    <zip dest="{@remoteworkdir}/log_AGENT_WIN.zip" doDetailSnap="0" doNotExistSkip="1" encoding="MS932" sequence="1">
      <param src="{@logdir}"/>
    </zip>
  
  </target>

</task>', false);

INSERT INTO trinity.bm_task_flow (task_flow_id, task, del_sts_id) VALUES ('RPTF_TRINITY_PDT', '  <?xml version="1.0" encoding="UTF-8"?>
  <task workflowNo="RPTF_TRINITY_PDT" xmlns="http://www.blueship.co.jp/tri/fw/schema/beans/task">

  <property name="sysDrive" value="C:"/>
  <property name="userDrive" value="C:"/>
  <property name="defsDrive" value="C:"/>
  <property name="workdir" value="{@workspace}/rc/{@envNo}"/>
  <property name="artifact" value="{@workdir}/atf"/>
  <property name="dst" value="{@workdir}/dst"/>
  <property name="new_mod" value="{@dst}/n_m"/>
  <property name="del" value="{@dst}/del"/>
  <property name="ftpRootTrinity_uni" value="/home/tri/rel"/>
  <property name="remotebasedir_uni" value="{@ftpRootTrinity_uni}/rp/{@lotNo}"/>
  <property name="remoteworkdir_uni" value="{@remotebasedir_uni}/ws"/>

  <property name="rphistorydir" value="{@historyRelUnitDSLPath}"/>
  <property name="rphistorydir_F" value="{@historyPath}/rp/"/>

  <property name="rchistorydir" value="{@historyRelDSLPath}"/>
  <property name="relhistorydir" value="{@historyPath}/rel/{@envNo}"/>

  <property name="defdir" value="{@homePath}/defs"/>

  <property name="def_reform_N_M" value="{@defdir}/reform/{@envNo}/N_M/reform_{@envNo}_N_M.deflist"/>
  <property name="def_reform_D" value="{@defdir}/reform/{@envNo}/D/reform_{@envNo}_DEL.deflist"/>

  <target doSimulate="0" forceExecute="0" name="初期処理" sequenceNo="000100">

    <delete doDetailSnap="0" doNotExistSkip="1" doSimulate="0" sequence="1">
      <param src="{@workdir}"/>
    </delete>
    <mkdir doDetailSnap="0" doExistSkip="0" sequence="2">
      <param src="{@workdir}"/>
      <param src="{@rchistorydir}"/>
      <param src="{@relhistorydir}"/>
      <param src="{@new_mod}"/>
      <param src="{@del}"/>
    </mkdir>

  </target>

  <target doSimulate="0" forceExecute="0" name="リフォーム準備" sequenceNo="000200">

    <copy doNeglect="1" holdTimestamp="1" sequence="1">
      <fileset dest="{@workdir}/artifact_AGENT_WIN.zip" src="{@rphistorydir}/artifact_AGENT_WIN.zip"/>
      <overwriteInfo doAdd="1" doUpdateNew="1" doUpdateOld="1"/>
    </copy>

    <copy doNeglect="1" holdTimestamp="1" sequence="2">
      <fileset dest="{@workdir}/artifact_AGENT_UNI.zip" src="{@rphistorydir}/artifact_AGENT_UNI.zip"/>
      <overwriteInfo doAdd="1" doUpdateNew="1" doUpdateOld="1"/>
    </copy>

    <unzip doDetailSnap="0" doNotExistSkip="1" sequence="3">
      <param dest="{@workdir}" src="{@workdir}/artifact_AGENT_WIN.zip" encoding="MS932"/>
    </unzip>

    <unzip doDetailSnap="0" doNotExistSkip="1" sequence="4">
      <param dest="{@workdir}" src="{@workdir}/artifact_AGENT_UNI.zip" encoding="UTF-8"/>
    </unzip>

    <delete doDetailSnap="0" doNotExistSkip="1" doSimulate="0" sequence="5">
      <param src="{@workdir}/artifact_AGENT_WIN.zip"/>
      <param src="{@workdir}/artifact_AGENT_UNI.zip"/>
    </delete>

  </target>

  <target doSimulate="0" forceExecute="0" name="リフォーム処理" sequenceNo="000300">

    <mkdir doExistSkip="1" sequence="1">

    </mkdir>

    <copy defineFilePath="{@def_reform_N_M}" doDetailSnap="0" doNeglect="1" doNotExistSkip="1" holdTimestamp="1" sequence="2">
      <overwriteInfo doAdd="1" doUpdateNew="1" doUpdateOld="1"/>
    </copy>

    <copy defineFilePath="{@def_reform_D}" doDetailSnap="0" doNeglect="1" doNotExistSkip="1" holdTimestamp="1" sequence="3">
      <overwriteInfo doAdd="1" doUpdateNew="1" doUpdateOld="1"/>
    </copy>

  </target>

  <target doSimulate="0" forceExecute="0" name="圧縮処理" sequenceNo="000400">
    <zip dest="{@workdir}/artifact.zip" doDetailSnap="0" doNotExistSkip="1" sequence="1">
      <param src="{@dst}"/>
    </zip>
  </target>

  <target doSimulate="0" forceExecute="0" name="資産転送SEND" sequenceNo="000500">

    <ftp doDetailSnap="0" doPassivemode="0" doSimulate="0" encoding="UTF-8" port="21" server="RC_AGENT" userid="root" sequence="1">
      <put fileType="2" sequence="1">
        <param dest="{@remoteworkdir_uni}" src="{@workdir}/artifact.zip"/>
      </put>
    </ftp>

  </target>

  <target doSimulate="0" forceExecute="0" name="資産転送GET" sequenceNo="000800">

    <ftp doDetailSnap="0" doPassivemode="0" doSimulate="0" encoding="UTF-8" port="21" server="RC_AGENT" userid="root" sequence="1">
      <get fileType="2" sequence="1">
        <param dest="{@workdir}" src="{@remoteworkdir_uni}/artifact.zip"/>
      </get>
    </ftp>

  </target>

  <target doSimulate="0" forceExecute="0" name="アーカイブ" sequenceNo="000900">

    <copy doNeglect="1" doNotExistSkip="1" sequence="1">
      <fileset dest="{@rchistorydir}/{@envNo}.zip" src="{@workdir}/artifact.zip"/>
      <overwriteInfo doAdd="1" doUpdateNew="1" doUpdateOld="1"/>
    </copy>


  </target>

  <target doSimulate="0" forceExecute="1" name="終了処理" sequenceNo="001000">
    <result/>
    <resource off="" active="" error=""/>
  </target>
</task>', false);


INSERT INTO trinity.bm_task_flow (task_flow_id, task, del_sts_id) VALUES ('RPTF_AGENT_UNI_PDT', '  <?xml version="1.0" encoding="UTF-8"?>
  <task workflowNo="RPTF_AGENT_UNI_PDT" xmlns="http://www.blueship.co.jp/tri/fw/schema/beans/task">

  <property name="ftpRootTrinity_uni" value="/home/tri/rel"/>
  <property name="remotebasedir_uni" value="{@ftpRootTrinity_uni}/rp/{@lotNo}"/>
  <property name="remoteworkdir_uni" value="{@remotebasedir_uni}/ws"/>
  <property name="dist" value="{@remoteworkdir_uni}/dst"/>
  <property name="defdir" value="{@ftpRootTrinity_uni}/defs"/>
  <property name="def_perm" value="{@defdir}/perm/{@envNo}/permission_{@envNo}.deflist"/>
  <property name="compress" value="{@remoteworkdir}/compress"/>

  <target doSimulate="0" forceExecute="0" name="初期処理" sequenceNo="000100">

    <delete doDetailSnap="0" doNotExistSkip="1" doSimulate="0" sequence="1">

      <param src="{@remoteworkdir_uni}"/>
      <param src="{@compress}"/>

    </delete>

    <mkdir doDetailSnap="0" doExistSkip="0" sequence="2">
      <param src="{@remoteworkdir_uni}"/>
      <param src="{@compress}"/>
    </mkdir>

  </target>

  <target doSimulate="0" forceExecute="0" name="パーミッション処理" sequenceNo="000500">

    <unzip doDetailSnap="0" doNotExistSkip="1" sequence="1">
      <param dest="{@remoteworkdir_uni}" src="{@remoteworkdir_uni}/artifact.zip" encoding="UTF-8"/>
    </unzip>

    <changePermission defineFilePath="{@def_perm}" doDetailSnap="0" doSimulate="0" sequence="2"/>
 
   <delete doDetailSnap="0" doNotExistSkip="1" doSimulate="0" sequence="3">
      <param src="{@remoteworkdir_uni}/artifact.zip"/>
   </delete>

  </target>

  <target doSimulate="0" forceExecute="0" name="圧縮処理" sequenceNo="000600">

    <copy doNeglect="1" doNotExistSkip="1" sequence="1">
      <fileset dest="{@compress}/dist" src="{@remoteworkdir_uni}"/>
    </copy>

    <exe workDir="{@compress}" command="{@compress}/compress.sh" log="compress.log" sequence="2">
      <param env="{@lotNo}"/>
    </exe>

 </target>
</task>', false);

