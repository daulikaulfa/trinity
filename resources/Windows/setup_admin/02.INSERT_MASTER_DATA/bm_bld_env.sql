-- ------------------------------------------------------------
-- bm_bld_env
-- ------------------------------------------------------------

INSERT INTO trinity.bm_bld_env (bld_env_id, svc_id, bld_env_nm, sts_id, del_sts_id) VALUES ('BPENV_TRINITY', 'FlowRelUnitEntryService', 'ビルド環境_差分', '1100', false);
INSERT INTO trinity.bm_bld_env (bld_env_id, svc_id, bld_env_nm, sts_id, del_sts_id) VALUES ('BPENV_TRINITY_F', 'FlowRelUnitEntryService', 'ビルド環境_全量', '1100', false);
INSERT INTO trinity.bm_bld_env (bld_env_id, svc_id, bld_env_nm, sts_id, del_sts_id) VALUES ('RPENV_TRINITY_PDT', 'FlowRelCtlEntryService', 'リリース環境_本番', '1100', false);

