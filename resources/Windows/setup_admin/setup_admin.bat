@echo off
setlocal

rem ENVIRONMENT VARIABLE
set HOME=C:\home

xcopy .\home %HOME%\ /Y /S /E /C /H /EXCLUDE:.\EXCLUDE.TXT
copy .\pgpass.conf "%APPDATA%\postgresql\pgpass.conf"

rem set PGCLIENTENCODING=SJIS
rem set PGPORT=5432
set DB_NAME=trinity
set USER_ID=trinity

rem CREATE TABLE
psql %DB_NAME% %USER_ID% < ./01.CREATE/create_tables_v3.sql

rem INSERT MASTER DATA
psql %DB_NAME% %USER_ID% < ./02.INSERT_MASTER_DATA/am_ext.sql
psql %DB_NAME% %USER_ID% < ./02.INSERT_MASTER_DATA/am_head.sql
psql %DB_NAME% %USER_ID% < ./02.INSERT_MASTER_DATA/am_vcs_mdl.sql
psql %DB_NAME% %USER_ID% < ./02.INSERT_MASTER_DATA/am_vcs_repos.sql
psql %DB_NAME% %USER_ID% < ./02.INSERT_MASTER_DATA/bm_bld_env.sql
psql %DB_NAME% %USER_ID% < ./02.INSERT_MASTER_DATA/bm_bld_env_srv.sql
psql %DB_NAME% %USER_ID% < ./02.INSERT_MASTER_DATA/bm_bld_srv.sql
psql %DB_NAME% %USER_ID% < ./02.INSERT_MASTER_DATA/bm_bld_timeline.sql
psql %DB_NAME% %USER_ID% < ./02.INSERT_MASTER_DATA/bm_bld_timeline_agent.sql
psql %DB_NAME% %USER_ID% < ./02.INSERT_MASTER_DATA/bm_rmi_svc.sql
psql %DB_NAME% %USER_ID% < ./02.INSERT_MASTER_DATA/bm_task_flow.sql
psql %DB_NAME% %USER_ID% < ./02.INSERT_MASTER_DATA/rm_cal.sql
psql %DB_NAME% %USER_ID% < ./02.INSERT_MASTER_DATA/sm_auto_numbering.sql
psql %DB_NAME% %USER_ID% < ./02.INSERT_MASTER_DATA/sm_func_svc.sql
psql %DB_NAME% %USER_ID% < ./02.INSERT_MASTER_DATA/sm_sys_def.sql
psql %DB_NAME% %USER_ID% < ./02.INSERT_MASTER_DATA/um_dept.sql
psql %DB_NAME% %USER_ID% < ./02.INSERT_MASTER_DATA/um_grp.sql
psql %DB_NAME% %USER_ID% < ./02.INSERT_MASTER_DATA/um_grp_role_lnk.sql
psql %DB_NAME% %USER_ID% < ./02.INSERT_MASTER_DATA/um_grp_user_lnk.sql
psql %DB_NAME% %USER_ID% < ./02.INSERT_MASTER_DATA/um_lock_svc.sql
psql %DB_NAME% %USER_ID% < ./02.INSERT_MASTER_DATA/um_project.sql
psql %DB_NAME% %USER_ID% < ./02.INSERT_MASTER_DATA/um_role.sql
psql %DB_NAME% %USER_ID% < ./02.INSERT_MASTER_DATA/um_role_svc_ctg_lnk.sql
psql %DB_NAME% %USER_ID% < ./02.INSERT_MASTER_DATA/um_svc_ctg.sql
psql %DB_NAME% %USER_ID% < ./02.INSERT_MASTER_DATA/um_svc_ctg_svc_lnk.sql
psql %DB_NAME% %USER_ID% < ./02.INSERT_MASTER_DATA/um_user.sql
psql %DB_NAME% %USER_ID% < ./02.INSERT_MASTER_DATA/um_user_pass_hist.sql
psql %DB_NAME% %USER_ID% < ./02.INSERT_MASTER_DATA/um_user_role_lnk.sql

endlocal